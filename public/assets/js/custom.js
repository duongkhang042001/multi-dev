function callNotify(type, message, heading = null) {
    let bg
    let title
    switch (type) {
        case 'success':
            bg = '#5ba035'
            title = "Thành Công!"
            break;
        case 'status':
            bg = '#5ba035'
            title = "Thành Công!"
            break;
        case 'warning':
            bg = '#da8609'
            title = "Cảnh Báo!"
            break;
        case 'info':
            bg = '#3b98b5'
            title = "Thông Báo!"
            break;
        case 'danger':
            bg = '#bf441d'
            title = "Thất Bại!"
            break;
        default:
            bg = '#bf441d';
            title = "Thất Bại!"
            break;
    }
    $.toast({
        heading: heading ? heading : title,
        text: message,
        position: "top-right",
        loaderBg: bg,
        icon: type,
        hideAfter: 3e3,
        stack: 1
    })
}


function getLoading() {
    $('#fullDiv').show();
    $('body').addClass('onScroll');
}
function deleteData(formId) {
    Swal.fire({
        title: "Bạn có muốn xóa ?",
        text: "Bạn sẽ không thể hoàn tác điều này!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Đồng ý",
        cancelButtonText: "Hủy",
        confirmButtonClass: "btn btn-primary mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if(t.value) {
            Swal.fire({
                title: "Đã Xóa",
                text: "Dữ liệu đã bị xóa.",
                type: "success"
            }).then(function (){
                document.getElementById(formId).submit()
            })
        }
        if(t.dismiss === Swal.DismissReason.cancel){
            Swal.fire({
                title: "Đã Hủy",
                text: "Hãy cẩn thận nếu bạn xóa, dữ liệu sẽ bị mất hoàn toàn!",
                type: "error"
            })
        }
        return false
    })
}
function removeLoading() {
    $('#fullDiv').hide();
    $('body').removeClass('onScroll');
  }
function exemptionSuccess(userId)
{
    Swal.fire({
        title: "Bạn có muốn xác nhận miễn thực tập </br> cho sinh viên ?",
        text: "Bạn sẽ không thể hoàn tác điều này!",
        type: 'question',
        showCancelButton: !0,
        confirmButtonText: "Đồng ý",
        cancelButtonText: "Hủy",
        confirmButtonClass: "btn btn-primary mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if(t.value) {
            Swal.fire({
                title: "Đã xác nhận thành công",
                text: "Sinh viên đã được miễn giảm thực tập",
                type: "success"
            }).then(function (){
                document.getElementById(userId).submit()
            })
        }
        if(t.dismiss === Swal.DismissReason.cancel){
            Swal.fire({
                title: "Đã Hủy",
                text: "Bạn nên cân nhắc thật kỷ trước khi đưa ra quyết định",
                type: "error"
            })
        }
        return false
    })
}
function exemptionCancel(userId) {
    Swal.fire({
        title: "Bạn có muốn huỷ miễn thực tập </br> cho sinh viên ?",
        text: "Bạn sẽ không thể hoàn tác điều này!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Đồng ý",
        cancelButtonText: "Hủy",
        confirmButtonClass: "btn btn-primary mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if(t.value) {
            Swal.fire({
                title: "Đã huỷ",
                text: "Bạn đã huỷ miễn thực tập cho sinh viên",
                type: "success"
            }).then(function (){
                document.getElementById(userId).submit()
            })
        }
        if(t.dismiss === Swal.DismissReason.cancel){
            Swal.fire({
                title: "Đã Hủy",
                text: "Hãy cẩn thận nếu bạn xóa, dữ liệu sẽ bị mất hoàn toàn!",
                type: "error"
            })
        }
        return false
    })
}
function internshipPass(userId)
{
    Swal.fire({
        title: "Bạn có muốn xác nhận thực tập lại </br> cho sinh viên ?",
        text: "Bạn sẽ không thể hoàn tác điều này!",
        type: 'question',
        showCancelButton: !0,
        confirmButtonText: "Đồng ý",
        cancelButtonText: "Hủy",
        confirmButtonClass: "btn btn-primary mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if(t.value) {
            Swal.fire({
                title: "Đã xác nhận thành công",
                text: "Sinh viên đã được thực tập lại",
                type: "success"
            }).then(function (){
                document.getElementById(userId).submit()
            })
        }
        if(t.dismiss === Swal.DismissReason.cancel){
            Swal.fire({
                title: "Đã Hủy",
                text: "Bạn nên cân nhắc thật kỷ trước khi đưa ra quyết định",
                type: "error"
            })
        }
        return false
    })
}
function internshipFail(userId) {
    Swal.fire({
        title: "Bạn có muốn huỷ thực tập lại </br> cho sinh viên ?",
        text: "Bạn sẽ không thể hoàn tác điều này!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Đồng ý",
        cancelButtonText: "Hủy",
        confirmButtonClass: "btn btn-primary mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if(t.value) {
            Swal.fire({
                title: "Đã huỷ",
                text: "Bạn đã huỷ yêu cầu thực tập lại cho sinh viên",
                type: "success"
            }).then(function (){
                document.getElementById(userId).submit()
            })
        }
        if(t.dismiss === Swal.DismissReason.cancel){
            Swal.fire({
                title: "Đã Hủy",
                text: "Hãy cẩn thận nếu bạn xóa, dữ liệu sẽ bị mất hoàn toàn!",
                type: "error"
            })
        }
        return false
    })
}