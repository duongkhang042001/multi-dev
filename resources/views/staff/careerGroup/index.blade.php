@extends('layouts.dashboard')
@section('page-title', 'Quản lí nhóm ngành | Tổng quan')
@section('title', 'Quản lí doanh nghiệp')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Quản lý nhóm ngành</li>
                </ol>
            </div>
            <h4 class="page-title">Quản lý nhóm ngành</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <form method="POST" onsubmit="getLoading()" action="{{ route('staff.careerGroup.store') }}">
                @csrf
                @method('POST')
                <div class="row">
                    <label class="col-sm-2 col-form-label">Tên Nhóm Ngành :</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @error('name')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label label-checkbox"></label>
                    <div class="col-sm-3 col-sm-offset-1 checkbox-radios">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="is_education" checked> Nhóm
                                Ngành Của Trường
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <label class="col-sm-2 col-form-label label-checkbox"></label>
                    <div class="col-sm-4 col-sm-offset-1 checkbox-radios">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="is_active" checked> Hoạt Động
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Thêm</button>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-responsive">
                <table class="table table-striped table-bordered nowrap dataTable no-footer dtr-inline" id="datatable">
                    <thead class="text-primary">
                        <tr>
                            <th class="text-center">#</th>
                            <th>Tên Nhóm Ngành</th>
                            <th class="text-center">Trạng Thái</th>
                            <th class="text-right">Chức năng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($careerGroup as $key => $row)

                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class="col">{{ $row->name }}</td>
                            <td class="text-center">
                                @if ($row->is_active == 1) <p class="text-success">Hoạt động</p>
                                @else <p class="text-danger">Không hoạt động</p>
                                @endif
                            </td>
                            <td class="text-right">
                                <a href="{{ route('staff.careerGroup.edit', $row->id) }}" class="btn btn-outline-primary"><i class="fas fa-pencil-alt"></i></a>
                                <a href="javascript:void(deleteData('deletedRow{{ $row->id }}'))" class="btn btn-outline-secondary"><i class="fas fa-trash"></i></a>
                                <form action="{{ route('staff.careerGroup.destroy', $row->id) }}" method="POST" id="deletedRow{{$row->id}}" class="d-none">
                                    @method('DELETE')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')

<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {

        $('#datatable').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)");
    });
</script>
@endpush