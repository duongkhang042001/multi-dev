@extends('layouts.dashboard')
@section('page-title', 'Quản Lý doanh nghiệp | Tổng quan')
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách báo cáo thực tập</li>
                </ol>
            </div>
            <h4 class="page-title">Yêu cầu hủy thực tập</h4>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs border-0" data-tabs="tabs">
                    <li class="nav-item">
                        <a href="#profile2" data-toggle="tab" aria-expanded="true" class="nav-link active">
                            Danh sách
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#home1" data-toggle="tab" aria-expanded="false" class="nav-link">
                            Sinh viên
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#profile1" data-toggle="tab" aria-expanded="true" class="nav-link">
                            Doanh nghiệp
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-box">
            <div class="tab-content">
                <div class="tab-pane show active" id="profile2">
                    <table id="datatable1" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class='text-primary'>
                            <tr>
                                <th>Họ và Tên</th>
                                <th>Tên Doanh Nghiệp</th>
                                <th>Đối tượng</th>
                                <th>Trạng thái</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($service as $row)
                            <tr>
                                <td class="col">{{$row->user->name}}</td>
                                <td>{{$row->report->company->name}}</td>
                                <td>
                                    @if($row->roleUser->code == STUDENT_ROLE_CODE) <p class="text-info">Sinh viên</p>
                                    @else($row->roleUser->code == COMPANY_ROLE_CODE) <p class="text-info">Doanh nghiệp</p>
                                    @endif
                                </td>
                                <td>
                                    @if($row->status == SERVICE_STATUS_APPROVED) <p class="text-danger">Đã hủy thực tập</p>
                                    @elseif($row->status == SERVICE_STATUS_DENINED)<p class="text-success">Từ chối hủy thực tập</p>
                                    @else<p class="text-warning">Chờ xử lý</p>
                                    @endif

                                </td>
                                <td >
                                    @if($row->status == SERVICE_STATUS_PENDING) <button class="btn btn-warning" onclick="company({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl"><i class="fas fa-edit"></i> Duyệt xử lý</button>
                                    @elseif($row->status == SERVICE_STATUS_DENINED)<button class="btn btn-info " onclick="company({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl"><i class="fas fa-edit"></i> Xem xử lý</button>
                                    @else($row->status == SERVICE_STATUS_APPROVED)<button class="btn btn-info" onclick="company({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl"><i class="fas fa-edit"></i> Xem xử lý</button>
                                    @endif
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="home1">
                    <table id="datatable2" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class='text-primary'>
                            <tr>
                                <th >Họ và Tên</th>
                                <th >Tên Doanh Nghiệp</th>
                                <th >Lý do hủy thực tập</th>
                                <th >Trạng thái</th>
                                <th >Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($serviceStudent as $row)
                            <tr>
                                <td class="col"><a onclick="company({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl">{{$row->user->name}}</a></td>
                                <td >{{$row->report->company->name}}</td>

                                <td >@if(!empty($row->description))  {!! Str::limit($row->description,50) !!} @else <span class="font-italic">(không có)</span> @endif</td>
                                <td >
                                    @if($row->status == SERVICE_STATUS_APPROVED) <p class="text-danger">Đã hủy thực tập</p>
                                    @elseif($row->status == SERVICE_STATUS_DENINED)<p class="text-success">Từ chối hủy thực tập</p>
                                    @else<p class="text-warning">Chờ xử lý</p>
                                    @endif
                                </td>
                                <td >
                                    @if($row->status == SERVICE_STATUS_PENDING) <button class="btn btn-warning " onclick="company({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl"><i class="fas fa-edit"></i> Duyệt xử lý</button>
                                    @elseif($row->status == SERVICE_STATUS_DENINED)<button class="btn btn-info " onclick="company({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl"><i class="fas fa-edit"></i> Xem xử lý</button>
                                    @else($row->status == SERVICE_STATUS_APPROVED)<button class="btn btn-info " onclick="company({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl"><i class="fas fa-edit"></i> Xem xử lý</button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="profile1">
                    <table id="datatable3" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class='text-primary'>
                            <tr>

                                <th >Tên Doanh Nghiệp</th>
                                <th >Sinh viên</th>
                                <th >Lý do hủy thực tập</th>
                                <th >Trạng thái</th>
                                <th >Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($serviceCompany as $row)
                            <tr>
                                <td ><a onclick="company({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl">{{$row->report->company->name}}</a></td>
                                <td >{{$row->report->user->name}}</td>
                                <td >{!! Str::limit($row->description) !!}</td>
                                <td >
                                    @if($row->status == SERVICE_STATUS_APPROVED) <p class="text-danger">Đã hủy thực tập</p>
                                    @elseif($row->status == SERVICE_STATUS_DENINED)<p class="text-success">Từ chối hủy thực tập</p>
                                    @else<p class="text-warning">Chờ xử lý</p>
                                    @endif
                                </td>
                                <td >
                                    @if($row->status == SERVICE_STATUS_PENDING) <button class="btn btn-warning " onclick="company({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl"><i class="fas fa-edit"></i> Duyệt xử lý</button>
                                    @elseif($row->status == SERVICE_STATUS_DENINED)<button class="btn btn-info " onclick="company({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl"><i class="fas fa-edit"></i> Xem xử lý</button>
                                    @else($row->status == SERVICE_STATUS_APPROVED)<button class="btn btn-info " onclick="company({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl"><i class="fas fa-edit"></i> Xem xử lý</button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div id="companyModel" class="modal-content">

            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
<!-- Required datatable js -->
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datatable1").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)")
    });
    $(document).ready(function() {
        $("#datatable2").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)")
    });
    $(document).ready(function() {
        $("#datatable3").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable3_wrapper .col-md-6:eq(0)")
    });

    function company(id) {
        $("#companyModel").html(`
                <div class="modal-header">
                    <h4 class="modal-title text-warning" id="myExtraLargeModalLabel">Đang tải ... <div class="spinner-border "></div> </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            `)
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: `{{route('ajax.report')}}`,
            data: {
                id: id
            },
            success: function(data) {
                $("#companyModel").html(data)
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
</script>
@endpush
