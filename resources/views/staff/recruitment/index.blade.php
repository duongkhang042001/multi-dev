@extends('layouts.dashboard')
@section('page-title', 'Quản lý Đăng Tuyển | Tổng quan')
@section('title', 'Danh Sách Đăng Tuyển')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách đăng tuyển</li>
                </ol>
            </div>
            <h4 class="page-title">Bài đăng tuyển</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs border-0" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link border-0  active" href="#tab1" data-toggle="tab">
                            Tất cả
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 " href="#tab2" data-toggle="tab">
                            Lịch phỏng vấn
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="card-box">
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <div class="table-responsive">
                        <h4 class="header-title mt-1"><b>Danh Sách Đăng Tuyển</b></h4>
                        <p class="sub-header">
                            Bao gồm tất cả bài đăng tuyển hoạt động trên hệ thống. (Mặc định sắp xếp theo bài mới nhất)
                        </p>
                        <table id="datatable1" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class="text-primary">
                                <tr>
                                    {{-- <th>#</th> --}}
                                    <th>Tiêu Đề Đăng Tuyển</th>
                                    <th>Doanh nghiệp</th>
                                    <th>Tạo/Cập nhật</th>
                                    <th>Trạng thái</th>
                                    <th>Lượt xem</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($recruitments as $key => $row)
                                <tr>
                                    {{-- <td> {{ $loop->iteration }} </td> --}}
                                    <td class="col"> {{ Str::limit($row->post->title, 30) }} </td>
                                    <td>
                                        <a href="{{ route('staff.company.show', $row->company_id) }}" data-toggle="tooltip" title="{{ $row->company->name }}">
                                            {{ Str::limit($row->company->short_name, 15) }}
                                        </a>
                                    </td>
                                    <td>
                                        <span class="font-weight-bold">{{ date('d/m/Y H:i:s', strtotime($row->post->created_at)) }}</span>
                                        <br>
                                        <span>{{ date('d/m/Y H:i:s', strtotime($row->post->updated_at)) }}</span>
                                    </td>
                                    <td>
                                        @if (!$row->post->is_active)
                                        <span class="text-danger">Buộc dừng</span>
                                        @elseif(!$row->is_active)
                                        <span class="text-warning">Dừng ứng tuyển</span>
                                        @elseif(!$row->post->is_show)
                                        <span class="text-warning">Không hiển thị</span>
                                        @else
                                        <span class="text-success">Đang hoạt động</span>
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $row->post->view }}</td>
                                    <td class="">
                                        <a href="{{ route('staff.recruitment.show', $row->id) }}" class="btn btn-outline-info mr-1" data-toggle="tooltip" title="" data-original-title="Xem chi tiết ">
                                            <i class="far fa-sticky-note"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>


                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <div class="table-responsive">
                        <h4 class="header-title mt-1"><b>Lịch Phỏng Vấn</b></h4>
                        <p class="sub-header">
                            Thông tin lịch trình các buổi phỏng vấn giữa sinh viên và doanh nghiệp.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/switchery/switchery.min.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')

<!-- Required datatable js -->
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script>

<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<!--switchery-->
<script src="assets/libs/switchery/switchery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('[data-plugin="switchery"]').each(function(a, n) {
            new Switchery($(this)[0], $(this).data())
        })
        $("#datatable1").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)")
        $("#datatable2").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)")
        $("#datatable3").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable3_wrapper .col-md-6:eq(0)")
        $("#datatable4").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable4_wrapper .col-md-6:eq(0)")
    });
</script>
@endpush