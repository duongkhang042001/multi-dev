@extends('layouts.dashboard')
@section('page-title', 'Quản lý Bài Viết | Sửa')
@section('title', 'Sửa Bài Viết')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{route('staff.recruitment.index')}}">Danh sách đăng
                            tuyển</a></li>
                    <li class="breadcrumb-item active">Chỉnh sửa</li>
                </ol>
            </div>
            <h4 class="page-title">Chỉnh sửa bài đăng tuyển</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <form method="POST" onsubmit="getLoading()" action="{{ route('staff.recruitment.update', $recruitment->id) }}" class="form-horizontal">
                @csrf
                @method('PUT')
                <div class="row">
                    <label class="col-sm-2 col-form-label">Tiêu Đề Đăng Tuyển</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="title" value="{{$recruitment->Post->title }}">
                            @error('title')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Đường dẫn</label>
                    <div class="col-sm-10">
                        <div class="form-group has-muted bmd-form-group">
                            <label for="exampleInput3" class="bmd-label-floating">https://fptinternship.vn/</label>
                            <input type="text" class="form-control" id="exampleInput3" name="slug" value="{{ $recruitment->Post->slug }}">
                            @error('slug')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Doanh nghiệp</label>
                    <div class="col-sm-10">
                        <select name="company_id" class="form-control">
                            <option>Vui lòng chọn doanh nghiệp</option>
                            @foreach ($companies as $row)
                            <option value="{{ $row->id }}" @if($row->id == $recruitment->company_id) selected @endif>{{ $row->name }}</option>
                            @endforeach
                        </select>
                        @error('company_id')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Mô Tả</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="description" value="{!!$recruitment->post->description!!}">
                            @error('description')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>


                <div class="row">
                    <label class="col-sm-2 col-form-label">Nội Dung Bài Viết</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <textarea class="form-control" id="content" name="content" rows="10">{{$recruitment->post->content}}</textarea>
                            @error('content')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Loại Ngành</label>
                    <div class="col-sm-10">
                        <select name="career" class="form-control">
                            @foreach ($careers as $row)
                            <option value="{{ $row->id }}" @if($row->id == $recruitment->career_id) selected @endif>{{ $row->name }}</option>
                            @endforeach
                        </select>
                        @error('career')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Loại Bài Viết</label>
                    <div class="col-sm-10">
                        <select name="post_type" value="{{ old('post_type') }}" class="form-control">

                            @foreach ($postType as $row)
                            <option value="{{ $row->id }}" @if($row->id == $recruitment->Post->post_type_id) selected @endif >{{ $row->name }}</option>
                            @endforeach
                        </select>
                        @error('post_type')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Số Lượng</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="quantity" value="{{ $recruitment->quantity }}">
                            @error('quantity')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Ngày bắt đầu</label>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <input type="date" class="form-control" name="date_start" value="{{ date('Y-m-d', strtotime($recruitment->date_start)) }}">
                            @error('date_start')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <label class="col-sm-2 col-form-label">Ngày kết thúc</label>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <input type="date" class="form-control" name="date_end" value="{{ date('Y-m-d', strtotime($recruitment->date_end)) }}">
                            @error('date_end')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Lương thấp</label>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <input type="text" class="form-control" name="salary_min" value="{{ $recruitment->salary_min }}">
                            @error('salary_min')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <label class="col-sm-2 col-form-label">Lương cao nhất</label>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <input type="text" class="form-control" name="salary_max" value="{{ $recruitment->salary_max }}">
                            @error('salary_max')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label label-checkbox">Phúc Lợi</label>
                    <div class="col-sm-10 checkbox-radios">
                        @foreach ($welfares as $welfares)
                        <div class="form-check form-check-inline col-sm-2">
                            <label class="form-check-label">
                                <input class="form-check-input" name="welfare[]" type="checkbox" value="{{$welfares->id}}" <?php
                                                                                                                            $recruitmentWelfare = json_decode($recruitment->welfares);
                                                                                                                            ?> @foreach ($recruitmentWelfare as $row) @if ($row==$welfares->id)
                                checked
                                @endif
                                @endforeach
                                > {{$welfares->name}}
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        @endforeach

                        @error('welfare')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>

                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label label-checkbox">Giới Tính</label>

                    <div class="col-sm-1 checkbox-radios">
                        <div class="form-check ">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="gender" value="1" @if ($recruitment->gender==1) checked @endif> Nam
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-1 checkbox-radios">
                        <div class="form-check ">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="gender" value="0" @if ($recruitment->gender==0) checked @endif> Nữ
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3 checkbox-radios">
                        <div class="form-check ">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="gender" value="-1" @if ($recruitment->gender==-1) checked @endif> Không yêu cầu
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                    @error('gender')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label label-checkbox">Comment</label>

                    <div class="col-sm-2 checkbox-radios">
                        <div class="form-check ">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="comment" value="1" @if ($recruitment->post->is_comment==1) checked @endif> Cho Phép
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <label class="col-sm-2 col-form-label label-checkbox">Trạng Thái</label>
                    <div class="col-sm-2 checkbox-radios">
                        <div class="form-check ">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="active" value="1" @if ($recruitment->post->is_active==1) checked @endif> Hoạt Động
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer ml-auto mr-auto">
                    <button type="submit" class="btn btn-rose">Sửa Bài Viết</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection
