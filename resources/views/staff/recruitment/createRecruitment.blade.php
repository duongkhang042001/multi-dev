@extends('layouts.dashboard')
@section('page-title', 'Quản lý Bài Viết | Tạo mới')
@section('title', 'Thêm Bài Viết')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{route('staff.recruitment.index')}}">Danh sách đăng
                            tuyển</a></li>
                    <li class="breadcrumb-item active">Tạo mới</li>
                </ol>
            </div>
            <h4 class="page-title">Viết bài đăng tuyển</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <form method="POST" onsubmit="getLoading()" action="{{ route('staff.recruitment.store') }}" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="card-header">
                    <div class="card-text">
                        <h4 class="d-inline-block">Tạo bài đăng tuyển</h4>
                        <div class="float-right">
                            <button type="reset" class="btn btn-outline-info"><i class="fe-refresh-cw mr-1"></i> Làm
                                mới</button>
                            <button type="submit" class="btn btn-success"><i class="fa fa-save mr-1"></i> Lưu</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Tiêu đề đăng tuyển</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}">
                                @error('title')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Đường dẫn</label>
                        <div class="col-sm-10">
                            <div class="form-group has-muted bmd-form-group">
                                <input type="text" class="form-control" id="slug" placeholder="tieu-de-bai-dang-tuyen" name="slug" value="{{ old('slug') }}">
                                @error('slug')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Doanh Nghiệp</label>
                        <div class="col-sm-10">
                            <select name="company_id" class="form-control">
                                <option value="">Vui lòng chọn Doanh Nghiệp</option>
                                @foreach ($companies as $row)
                                <option value="{{ $row->id }}" @if ($row->id == old('company_id')) selected @endif>
                                    {{ $row->name }}
                                </option>
                                @endforeach
                            </select>
                            @error('company_id')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                    <div class="row mt-2">
                        <label class="col-sm-2 col-form-label">Mô Tả</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="description" value="{{ old('description') }}">
                                @error('description')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Nội dung bài viết</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <textarea class="form-control" id="content" name="content" rows="10">{{ old('content') }}</textarea>
                                <script>
                                    CKEDITOR.replace('content');
                                </script>
                                @error('content')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Yêu cầu bài Viết</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <textarea class="form-control" id="requirement" name="requirement" rows="10">{{ old('requirement') }}</textarea>
                                <script>
                                    CKEDITOR.replace('requirement');
                                </script>
                                @error('requirement')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Loại Ngành</label>
                        <div class="col-sm-10">
                            <select name="career" class="form-control">
                                <option value="">Vui lòng chọn ngành</option>
                                @foreach ($careers as $row)
                                <option value="{{ $row->id }}" @if ($row->id == old('career')) selected @endif>
                                    {{ $row->name }}
                                </option>
                                @endforeach
                            </select>
                            @error('career')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-2">
                        <label class="col-sm-2 col-form-label">Loại Bài Viết</label>
                        <div class="col-sm-10">
                            <select name="post_type" value="{{ old('post_type') }}" class="form-control">
                                @foreach ($postType as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                            @error('post_type')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-2">
                        <label class="col-sm-2 col-form-label">Số Lượng</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="quantity" value="{{ old('quantity') }}">
                                @error('quantity')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Ngày bắt đầu</label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="date" class="form-control" name="date_start" value="{{ old('date_start') }}">
                                @error('date_start')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <label class="col-sm-2 col-form-label">Ngày kết thúc</label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="date" class="form-control" name="date_end" value="{{ old('date_end') }}">
                                @error('date_end')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Lương thấp</label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="salary_min" value="{{ old('salary_min') }}">
                                @error('salary_min')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <label class="col-sm-2 col-form-label">Lương cao nhất</label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="salary_max" value="{{ old('salary_max') }}">
                                @error('salary_max')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <label class="col-sm-2 col-form-label label-checkbox">Phúc Lợi</label>
                        <div class="col-sm-10 checkbox-radios">
                            @foreach ($welfares as $welfares)
                            <div class="form-check form-check-inline col-sm-2">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="welfare[]" type="checkbox" value="{{ $welfares->id }}" @if (old('welfare')) @foreach (old('welfare') as $row) @if ($row==$welfares->id)
                                    checked
                                    @endif
                                    @endforeach
                                    @endif

                                    > {{ $welfares->name }}
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            @endforeach

                            @error('welfare')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                    <div class="row mt-3">
                        <label class="col-sm-2 col-form-label label-checkbox">Giới Tính</label>
                        <div class="col-sm-1 checkbox-radios">
                            <div class="form-check ">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gender" value="{{ MAN }}" checked> Nam
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-1 checkbox-radios">
                            <div class="form-check ">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gender" value="{{ WOMEN }}" checked> Nữ
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-3 checkbox-radios">
                            <div class="form-check ">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gender" value="-1" checked>
                                    Khác
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        @error('gender')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label label-checkbox mt-1">Cho phép bình luận</label>
                        <div class="col-sm-2 checkbox-radios">
                            <div class="form-check  mt-0">
                                <label class="form-check-label ">
                                    <input class="form-check-input" type="checkbox" name="comment" value="1">
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>

                            </div>
                        </div>
                        <label class="col-sm-2 col-form-label label-checkbox mt-1">Hoạt động</label>
                        <div class="col-sm-2 checkbox-radios">
                            <div class="form-check  mt-0">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="active" value="1" checked>
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="reset" class="btn btn-outline-info"><i class="fe-refresh-cw mr-1"></i> Làm
                        mới</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-save mr-1"></i> Lưu</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('script')
<script type="text/javascript">
    $("#title").keyup(function() {
            var Text = $(this).val();
            $slug = slugify(Text);
            $("#slug").val($slug);
        });

        function slugify(string) {
            const a = 'àáäâãåăæąçćčđďèéěėëêęğǵḧìíïîįłḿǹńňñòóöôœøṕŕřßşśšșťțùúüûǘůűūųẃẍÿýźžż·/_,:;'
            const b = 'aaaaaaaaacccddeeeeeeegghiiiiilmnnnnooooooprrsssssttuuuuuuuuuwxyyzzz------'
            const p = new RegExp(a.split('').join('|'), 'g')
            return string.toString().toLowerCase()
                .replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a')
                .replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e')
                .replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i')
                .replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o')
                .replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u')
                .replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y')
                .replace(/đ/gi, 'd')
                .replace(/\s+/g, '-')
                .replace(p, c => b.charAt(a.indexOf(c)))
                .replace(/&/g, '-and-')
                .replace(/[^\w\-]+/g, '')
                .replace(/\-\-+/g, '-')
                .replace(/^-+/, '')
                .replace(/-+$/, '')
        }
</script>
@endpush
