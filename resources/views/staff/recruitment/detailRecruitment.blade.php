@extends('layouts.dashboard')
@section('page-title', 'Quản lý Đăng Tuyển | Tổng quan')
@section('title', 'Chi Tiết Đăng Tuyển')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{route('staff.recruitment.index')}}">Danh sách đăng tuyển</a></li>
                    <li class="breadcrumb-item active">Chi tiết</li>
                </ol>
            </div>
            <h4 class="page-title">Chi tiết đăng tuyển</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box text-dark" style="font-size: 15px;">
            <div class="row">
                <div class="col-sm-3">
                    <img src="{{$recruitment->company->getAvatar()}}" onerror="this.src='assets/images/1.png'" class="w-100" />
                </div>
                <div class="col-sm-9">
                    <h3 class="font-weight-bold text-info">{{ $recruitment->post->title }}</h3>
                    <div class="row ml-0">
                        <p class="text-dark"><i class="fas fa-link text-purple" title="đường dẫn"></i> {{env('APP_URL').'/'.$recruitment->post->slug }}</p>
                    </div>
                    <div class="row ml-0">
                        <p>
                            <i class="fas fa-hotel text-success"></i> <span>{{$recruitment->company->name}}</span>
                        </p>
                    </div>
                    <div class="row ml-0">
                        <div class="col-sm-3">
                            @if ($recruitment->post->is_comment == 1)
                            <div class="row">
                                <p class="text-dark">
                                    <i class="fas fa-check-circle text-success"></i> <span> Mở bình luận</span>
                                </p>
                            </div>
                            @else
                            <div class="row">
                                <p class="text-dark"> <i class="fas fa-times-circle text-danger"></i> <span class="text-dark">Tắt bình luận</span></p>
                            </div>
                            @endif
                        </div>
                        <div class="col-sm-3">
                            @if ($recruitment->post->is_active == 1)
                            <div class="row">
                                <p class="text-dark">
                                    <i class="fas fa-check-circle text-success"></i> <span>Đang hoạt động</span>
                                </p>
                            </div>
                            @else
                            <div class="row">
                                <p class="text-dark">
                                    <i class="fas fa-times-circle text-danger"></i><span>Không hoạt động</span>
                                </p>
                            </div>
                            @endif
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <p class="text-dark"><i class="fas fa-user-edit text-info" title="người đăng"></i> {{$recruitment->post->user->name}}</p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <p class="text-dark"><i class="far fa-eye text-pink" title="Lượt xem"></i> {{$recruitment->post->view}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card-box">
                    <h3 class="font-weight-bold mt-2">Thông tin ứng tuyển</h3>
                        <blockquote class="card-bodyquote">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p> <label class="col-sm-2">Số lượng: </label> {{$recruitment->quantity }}</p>
                                    <p> <label class="col-sm-2">Giới tính: </label>
                                        @if($recruitment->gender==0) Nam
                                        @elseif ($recruitment->gender==1) Nữ
                                        @else Không yêu cầu giới tính
                                        @endif</p>
                                    <p> <label class="col-sm-2">Lương: </label>
                                        {{number_format( $recruitment->salary_min)}} - {{number_format( $recruitment->salary_max)}}
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <p> <label class="col-sm-2">Lĩnh vực: </label> {{ $nameCareer->name }}
                                    </p>
                                    <p> <label class="col-sm-2">Thời gian: </label>
                                        {{ date('d/m/Y',strtotime($recruitment->date_start)) }} -
                                        {{ date('d/m/Y',strtotime($recruitment->date_end))}}
                                    </p>
                                    <p> <label class="col-sm-2">Địa chỉ: </label>
                                        {{ $recruitment->company->getAddress()}}
                                    </p>
                                </div>
                            </div>
                            <div class="row ml-0">
                                <div class="col-12"><label>Phúc lợi:</label></div>
                                @foreach (json_decode($recruitment->welfares) as $row)
                                @foreach ($welfares as $row1)
                                @if ($row==$row1->id)
                                <span class="col-3">{{$row1->name}}</span>
                                @endif
                                @endforeach
                                @endforeach
                            </div>
                        </blockquote>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="font-weight-bold">
                        Chi tiết đăng tuyển
                    </h3>
                </div>
                <div class="col-sm-12">
                    <p class="text-break">{!! $recruitment->post->content !!} </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="font-weight-bold">
                        Yêu cầu đăng tuyển
                    </h3>
                </div>
                <div class="col-sm-12">
                    <p class="text-break">{!! $recruitment->requirement !!} </p>
                </div>
            </div>
            <div class="row">
                <a href="{{ route('staff.recruitment.edit', $recruitment->id) }}" class="btn btn-outline-primary mr-1"><i class="fas fa-pencil-alt"></i> Chỉnh bài đăng</a>
                <a href="javascript: document.getElementById('deletedRow{{$recruitment->id}}').submit();" onclick="return confirm('Bạn Có muốn xóa???');" class="btn btn-outline-secondary"><i class="fas fa-trash"></i> Xóa bài đăng</a>
                <form action="{{ route('staff.recruitment.destroy', $recruitment->id) }}" method="POST" id="deletedRow{{$recruitment->id}}" class="d-none">
                    @method('DELETE')
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<!-- Script You Need -->
<script>
    const numberFormat = new Intl.NumberFormat('vi-VN', {
        style: 'currency',
        currency: 'VND',
    });
</script>
@endpush