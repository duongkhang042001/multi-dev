@extends('layouts.dashboard')
@section('page-title', 'Quản lý Phúc Lợi | Tạo mới')
@section('title', 'Thêm Phúc Lợi')
@section('content')
@if(session()->has('status'))
<div class="col-sm-6 ml-3 alert alert-success">
    {{ session()->get('status') }}
</div>
@endif
<div class="content">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-icon card-header-rose">
                            <div class="card-icon">
                                <i class="material-icons">assignment</i>
                            </div>
                            <h4 class="card-title ">Thêm Phúc Lợi</h4>

                        </div>
                        <div class="card-body ">
                            <form method="POST" onsubmit="getLoading()" action="{{route('staff.welfare.store')}}" class="form-horizontal">
                                @csrf
                                @method('POST')
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Mã Phúc Lợi</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="code" value="{{ old('code') }}">
                                            @error('code')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Tên Phúc Lợi</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                            @error('name')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer ml-auto mr-auto">
                                    <button type="submit" class="btn btn-rose">Tạo Phúc Lợi</button>
                                </div>
                            </form>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('script')

@endsection