@extends('layouts.dashboard')
@section('page-title', 'Quản lý Phúc Lợi | Tạo mới')
@section('title', 'Chỉnh Phúc Lợi')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('staff.post.index') }}">Danh sách phúc lợi</a></li>
                    <li class="breadcrumb-item active">Chỉnh sửa phúc lợi</li>
                </ol>
            </div>
            <h4 class="page-title">Chỉnh sửa phúc lợi</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <form method="POST" onsubmit="getLoading()" action="{{route('staff.welfare.update',[$welfares->id])}}" class="form-horizontal">
                @csrf
                @method('PUT')
                <div class="row">
                    <label class="col-sm-2 col-form-label">Mã Phúc Lợi</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="code" value="{{$welfares->code}}">
                            @error('code')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Tên Phúc Lợi</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" value="{{$welfares->name}}">
                            @error('name')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
             
                    <button type="reset" class="btn btn-outline-info"><i class="fe-refresh-cw mr-1"></i> Làm
                        mới</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-save mr-1"></i> Lưu</button>
             
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection