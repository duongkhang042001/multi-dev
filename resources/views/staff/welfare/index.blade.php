@extends('layouts.dashboard')
@section('page-title', 'Quản lý Phúc Lợi | Admin')
@section('title', 'Phúc Lợi')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách phúc lợi</li>
                </ol>
            </div>
            <h4 class="page-title">Danh sách phúc lợi</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <form method="POST" onsubmit="getLoading()" action="{{route('staff.welfare.store')}}" class="form-horizontal">
                @csrf
                @method('POST')
                <div class="row">
                    <label class="col-sm-2 col-form-label">Mã Phúc Lợi</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="code" value="{{ old('code') }}">
                            @error('code')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Tên Phúc Lợi</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @error('name')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Thêm</button>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="">
                <table class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline" id="datatable">
                    <thead class="text-primary">
                        <tr>
                            <th class="text-center">Mã</th>
                            <th class="text-center">Tên Phúc Lợi</th>
                            <th class="text-center">Trạng Thái</th>
                            <th class="text-center">Chức Năng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($welfares as $key => $row)
                        <tr>
                            <td class="text-center">{{ $row->code }}</td>
                            <td class="text-center">{{ Str::limit($row->name, 80) }}</td>
                            <td class="text-center">
                                <div class="togglebutton">
                                    <label>
                                        <input type="checkbox" @if ($row->is_active) checked @endif>
                                        <span class="toggle" onclick="isActive('{{ $row->id }}','WELFARE')"></span>
                                    </label>
                                </div>
                            </td>
                            <td class="text-center">
                                <a href="{{ route('staff.welfare.edit', $row->id) }}" class="btn btn-outline-primary"><i class="fas fa-pencil-alt"></i></a>
                                <a href="javascript: document.getElementById('deletedRow{{ $row->id }}').submit();" onclick="return confirm('Bạn Có muốn xóa???');" class="btn btn-outline-secondary"><i class="fas fa-trash"></i></a>
                                <form action="{{ route('staff.welfare.destroy', $row->id) }}" method="POST" id="deletedRow{{ $row->id }}" class="d-none">
                                    @method('DELETE')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/switchery/switchery.min.css" rel="stylesheet" type="text/css" />
@endpush

@push('script')
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<script src="assets/libs/switchery/switchery.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script src="/assets/js/plugins/sweetalert2.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)");
    });

    function isActive(id, model) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: `{{ route('ajax.status') }}`,
            type: 'PUT',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
                id: id,
                model: model,
            },
            success: function(response) {
                if (response)
                    swal({
                        title: "Cho phép hoạt động!",
                        text: "Bạn đã vừa thay đổi trạng thái hoạt động!",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-success",
                        type: "success"
                    }).catch(swal.noop)
                else {
                    swal({
                        title: "Đã tắt hoạt động!",
                        text: "Bạn đã vừa thay đổi trạng thái hoạt động!",
                        icon: 'error',
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-warning",
                        type: 'warning',
                    }).catch(swal.noop)
                }
                // showNotification ('top','center','success', 'Thay đổi trạng thái thành công!',1000);
            },
            error: function(response) {
                swal({
                    title: "Lỗi hệ thống!",
                    text: "Đã xảy ra sự cố vui lòng thử lại sau!",
                    icon: 'error',
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-danger",
                    type: 'error',
                }).catch(swal.noop)
            }
        });
    }
</script>
@endpush