@extends('layouts.dashboard')
@section('page-title', 'Sinh viên miễn giảm thực tập | Danh sách')
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách cấp bảng điểm sinh viên</li>
                </ol>
            </div>
            <h4 class="page-title">Danh sách cấp bảng điểm sinh viên</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs border-0" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link border-0  active" href="#all" data-toggle="tab">
                            Tất Cả
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 " href="#new" data-toggle="tab">
                            Chưa xử lý
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 " href="#not-active" data-toggle="tab">
                            Đã xử lý
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-box">
            <div class="tab-content">
                <div class="tab-pane active" id="all">
                    <div class="table-responsive">
                        <table id="datatable1" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th class="text-center">Mã số sinh viên</th>
                                    <th class="text-center">Họ và Tên</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Ngày gửi yêu cầu</th>
                                    <th class="text-center">Ngày phản hồi</th>
                                    <th class="text-center">Trạng thái</th>
                                    <th class="text-center">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($transcript as $row)
                                <tr>
                                    <td class="text-center">{{$row->user->code}}</td>
                                    <td class="text-center">{{$row->user->name}}</td>
                                    <td class="text-center">{{$row->user->email}}</td>
                                    <td class="text-center">{{date('d-m-Y h:i:s', strtotime($row->created_at))}}</td>
                                    <td class="text-center">@if(!empty($row->getFile)) {{date('d-m-Y h:i:s', strtotime($row->getFile->created_at))}} @else <span class="font-italic">Chưa có cập nhật</span> @endif</td>
                                    <td class="text-center">
                                        @if($row->status == SERVICE_STATUS_PENDING) <p class="text-warning">Chờ xử lý</p>
                                        @else($row->status == SERVICE_STATUS_APPROVED)<p class="text-success">Đã xử lý</p>
                                        @endif

                                    </td>
                                    <td class="text-center">
                                        @if($row->status == SERVICE_STATUS_PENDING ) <button class="btn btn-warning" onclick="uploadTranscript({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fas fa-edit"></i></button>
                                        @else($row->status == SERVICE_STATUS_APPROVED) <a href="{{ \App\Http\Controllers\Controller::getFilePdf($row->file) }}" type="button" target="_blank" class="preview btn btn-primary" title="Xem hồ sơ"> <i class="far fa-address-card"></i></a>
                                        @endif
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="new">
                    <div class="table-responsive">
                        <table id="datatable2" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th class="text-center">Mã số sinh viên</th>
                                    <th class="text-center">Họ và Tên</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Trạng thái</th>
                                    <th class="text-center">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($transcriptRequest as $row)
                                <tr>
                                    <td class="text-center">{{$row->user->code}}</td>
                                    <td class="text-center">{{$row->user->name}}</td>
                                    <td class="text-center">{{$row->user->email}}</td>
                                    <td class="text-center">
                                        <p class="text-warning">Chờ xử lý</p>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-warning" onclick="uploadTranscript({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fas fa-edit"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="not-active">
                    <div class="table-responsive">
                        <table id="datatable3" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th class="text-center">Mã số sinh viên</th>
                                    <th class="text-center">Họ và Tên</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Trạng thái</th>
                                    <th class="text-center">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($transcriptRespone as $row)
                                <tr>
                                    <td class="text-center">{{$row->user->code}}</td>
                                    <td class="text-center">{{$row->user->name}}</td>
                                    <td class="text-center">{{$row->user->email}}</td>
                                    <td class="text-center">
                                        <p class="text-success">Đã xử lý</p>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ \App\Http\Controllers\Controller::getFilePdf($row->transcript_issued) }}" type="button" target="_blank" class="preview btn btn-primary" title="Xem hồ sơ"> <i class="far fa-address-card"></i></a>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div id="student-modal" class="modal-content">

        </div>
    </div>
</div>
@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/switchery/switchery.min.css" rel="stylesheet" type="text/css" />
@endpush

@push('script')
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<script src="assets/libs/switchery/switchery.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
@endpush

@push('script')
<script>
    $(document).ready(function() {
        $('#datatable1').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable2').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable3').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable3_wrapper .col-md-6:eq(0)");
    });

    function uploadTranscript(id) {
        $("#student-modal").html(`
                <div class="modal-header">
                    <h4 class="modal-title text-warning" id="myExtraLargeModalLabel">Đang tải ... <div class="spinner-border "></div> </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            `);
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: `{{ route('ajax.transcript') }}`,
            data: {
                id: id
            },
            success: function(data) {
                $("#student-modal").html(data);
            },
            error: function(data) {
                console.log(data);
            }
        });

    }
</script>
@endpush