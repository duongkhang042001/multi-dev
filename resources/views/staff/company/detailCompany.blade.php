@extends('layouts.dashboard')
@section('page-title', 'Quản Lý Doanh Nghiệp | Hồ Sơ')
@section('title', 'Hồ Sơ Doanh Nghiệp')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{route('staff.company.index')}}">Danh sách doanh nghiệp</a></li>
                    <li class="breadcrumb-item active">Chi tiết</li>
                </ol>
            </div>
            <h4 class="page-title">Chi tiết doanh Nghiệp</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="row" style='height: 50vh; object-fit:cover; overflow: hidden;'>
                <div class="col-sm-12">
                    <img src="{{FILE_URL .$company->banner}}" onerror="this.src='assets/img/default-banner.jpg'" class="w-100" />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3" style="margin-top:-140px; margin-left: 20px;">
                    <img src="{{FILE_URL .$company->avatar}}" onerror="this.src='assets/img/default-avatar-company.jpg'" class="w-100" />
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="text-dark" style='text-align: center;margin-top: 25px;'>{{$company->name}}</h2>
                        </div>
                        @if ($company->status == 'APPROVED')
                        <div class="row text-success" style="margin: 0 auto;">
                            <i class="fas fa-check-circle text-success mr-2 mt-1" style="font-size: 20px;"></i>
                            <p style="font-size:20px;">Đang hoạt động</p>
                        </div>
                        @elseif($company->status == 'WAITING')
                        <div class="row text-danger" style="margin: 0 auto;">
                            <span class="material-icons mr-2">
                                radio_button_checked
                            </span>
                            <p style="font-size:20px;">Chưa hoạt động</p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">

                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <ul class="nav nav-tabs" data-tabs="tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#profile" data-toggle="tab">
                                        Doanh Nghiệp
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#messages" data-toggle="tab">
                                        Nhân Viên
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#settings" data-toggle="tab">
                                        Bài Viết
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#students" data-toggle="tab">
                                        Sinh Viên
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="profile">
                            <div class="row" style="margin-top: 30px;">
                                <div class="col-sm-6">
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-sm-3">
                                            <div class="staff-input">
                                                <label class="mt-2 text-dark">Số Điện Thoại:</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="satff-info">
                                                <input type="text" class="form-control text-center" value="{{$company->phone}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-sm-3">
                                            <div class="staff-input">
                                                <label class="mt-2 text-dark">Email:</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="satff-info">
                                                <input type="text" class="form-control text-center" value="{{$company->email}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-sm-3">
                                            <div class="staff-input">
                                                <label class="mt-2 text-dark">Địa Chỉ Website:</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="satff-info">
                                                <input type="text" class="form-control text-center" @foreach(json_decode($company->url) as $key => $url)
                                                @if($key == 'url')
                                                value="{{$url}}"
                                                @endif
                                                @endforeach disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-sm-4">
                                            <div class="staff-input">
                                                <label class="mt-2 text-dark">Mã Doanh Nghiệp:</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="satff-info">
                                                <input type="text" class="form-control text-center" value="{{$company->code}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-sm-4">
                                            <div class="staff-input">
                                                <label class="mt-2 text-dark">Ngày Thành Lập:</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="satff-info">
                                                <input type="text" class="form-control text-center" value="{{date('d-m-Y', strtotime($company->founded_at))}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-sm-4">
                                            <div class="staff-input">
                                                <label class="mt-2 text-dark">Mã Số Thuế:</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="satff-info">
                                                <input type="text" class="form-control text-center" value="{{$company->tax_number}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-sm-2">
                                            <div class="staff-input">
                                                <label class="mt-2 text-dark">Địa Chỉ:</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="satff-info">
                                                <input type="text" class="form-control text-center" value="{{$company->getAddress()}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <form action="{{ route('staff.company.destroy', $company->id) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger ml-2 mt-3" onclick="return confirm('Bạn Có muốn xóa???');">Xóa Doanh Nghiệp</button>
                                </form>
                            </div>
                        </div>
                        <!-- Danh Sách Nhân Viên Doanh Nghiệp Và Hồ Sơ Nhân Viên-->
                        <div class="tab-pane" id="messages">
                            @if(empty($company->manager))
                            <div class="row">
                                <div class="col-sm-12">
                                    <h2 class="txtdeepshadow">
                                        Chưa Có Nhân Viên Trong Doanh Nghiệp
                                    </h2>
                                </div>
                            </div>
                            @else
                            <div class="table-responsive">
                                <table class="table" id="datatable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center col-sm-2">Mã Nhân Viên</th>
                                            <th class="text-center col-sm-3">Tên Nhân Viên</th>
                                            <th class="text-center col-sm-3">Email</th>
                                            <th class="text-center col-sm-4">Chức năng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($company->staffs as $key => $row)
                                        <tr>
                                            <td class="text-center">{{$loop->iteration}}</td>
                                            <td class="text-center col-sm-2">{{$row->code}}</td>
                                            <td class="text-center col-sm-3">{{$row->name}}</td>
                                            <td class="text-center col-sm-3">{{$row->email}}</td>
                                            <td class="text-center">
                                                <a href="{{route('staff.staff-companys.show',$row->id)}}" title="Chi tiết nhân viên" class="btn  btn-sm btn-info mr-1"><i class=" fas fa-id-card"></i></a>
                                                {{-- <a href="javascript: document.getElementById('deletedRow{{ $row->id }}').submit();" onclick="return confirm('Bạn Có muốn xóa???');" class="btn  btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                                                <form action="{{route('staff.staff-company.edit',$row->id)}}" method="POST" id="deletedRow{{ $row->id }}" class="d-none">
                                                    @method('DELETE')
                                                    @csrf
                                                </form> --}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <a class="btn btn-ml btn-info" href="javascript: document.getElementById('staffCreate').submit();">Tạo Nhân Viên</a>
                                <form id="staffCreate" action="{{route('staff.staff-company.create')}}" method="GET" class="d-none">
                                    <input type='hidden' name="company_id" value='{{$company->id}}'>
                                </form>
                            </div>
                            @endif
                        </div>
                        <div class="tab-pane" id="settings">
                            @if(count($postCompany))
                            <div class="table-responsive">
                                <table class="table" id="datatable1">
                                    <thead class="text-primary text-primary">
                                        <tr>
                                            <th>#</th>
                                            <th>Tiêu Đề </th>
                                            <th class="text-center">Trạng Thái</th>
                                            <th class="text-center">Bình Luận</th>
                                            <th class="text-center">Thời gian tạo</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($postCompany as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <p class="mb-0">
                                                    <a href="{{ route('staff.post-companys.show', $row->post_id) }}" class=" text-primary"> {{Str::limit($row->post->title, 100)}}</a>
                                                </p>
                                                <small class="text-muted">{{Str::limit($row->post->slug, 150)}}</small>
                                            </td>
                                            <td class="text-center">
                                                @if($row->is_active == 1)
                                                <div class="togglebutton">
                                                    <label>
                                                        <input type="checkbox" checked="">
                                                        <span class="toggle"></span>
                                                    </label>
                                                </div>
                                                @else
                                                <div class="togglebutton">
                                                    <label>
                                                        <input type="checkbox">
                                                        <span class="toggle"></span>
                                                    </label>
                                                </div>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($row->is_comment == 1)
                                                <div class="togglebutton">
                                                    <label>
                                                        <input type="checkbox" checked="">
                                                        <span class="toggle"></span>
                                                    </label>
                                                </div>
                                                @else
                                                <div class="togglebutton">
                                                    <label>
                                                        <input type="checkbox">
                                                        <span class="toggle"></span>
                                                    </label>
                                                </div>
                                                @endif
                                            </td>
                                            <td class="text-dark text-center">
                                                {{date('d/m/Y H:i:s',strtotime($row->created_at))}}
                                                <br>
                                                {{$row->company->manager->name}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 class="txtdeepshadow">
                                        Chưa Có Bài Viết Của Doanh Nghiệp
                                    </h4>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="tab-pane" id="students">
                            <div class="table-responsive">
                                <table class="table" id="datatable2">
                                    <thead class='text-primary'>
                                        <tr>
                                            <div class="row">
                                                <th class="text-center">#</th>
                                                <th class="text-center col-sm-4">MSSV</th>
                                                <th class="text-center col-sm-3">Tên Sinh Viên</th>
                                                <th class="text-center col-sm-3">Email</th>
                                                <th class="text-center col-sm-4">Trạng thái</th>
                                            </div>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($student as $row)
                                        <tr>
                                            <div class="row">
                                                <td class="text-center">{{$loop->iteration}}</td>
                                                <td class="text-center col-sm-3">{{$row->user->code}}</td>
                                                <td class="text-center col-sm-3">{{$row->user->name}}</td>
                                                <td class="text-center col-sm-2">{{$row->user->email}}</td>
                                                <td class="text-center">
                                                    @if($row->status == REPORT_PENDING) <p class="text-warning">Đang thực tập</p>
                                                    @elseif($row->status == REPORT_PASS) <p class="text-success"> Đạt thực tập</p>
                                                    @elseif($row->status == REPORT_FAIL) <p class="text-danger"> Không đạt thực tập</p>
                                                    @endif
                                                </td>
                                            </div>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-2 ml-2 col-sm-6">
                @if($company->status == 'WAITING')
                <form action="{{ route('staff.company.approve', $company->id) }}" method="POST">
                    @method('POST')
                    @csrf
                    <button class="btn btn-success" style=" min-width:200px;max-width: 200px;" onclick="return confirm('Bạn có muốn duyệt???');">
                        <i class="material-icons">check</i> Duyệt danh nghiệp
                    </button>
                </form>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/switchery/switchery.min.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<script src="assets/libs/switchery/switchery.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script src="/assets/js/plugins/sweetalert2.js"></script>
<script src="assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#datatable').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable1').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable2').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)");
    });
</script>
@endpush
