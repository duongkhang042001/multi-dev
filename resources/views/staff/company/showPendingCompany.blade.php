@extends('layouts.dashboard')
@section('page-title', 'Quản Lý doanh nghiệp | Xét duyệt')
@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="/admin">Danh sách </a></li>
                        <li class="breadcrumb-item active">Xét duyệt doanh nghiệp</li>
                    </ol>
                </div>
                <h4 class="page-title">Xét duyệt doanh nghiệp</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <div class="table-responsive">
                    <h4 class="card-title text-primary">Kiểm Duyệt Thông Tin Doanh nghiệp</h4>

                    <div class="sub-header">
                        <p>Hình thức đăng ký:</p>
                        <ul>
                            <li>Sinh viên đăng ký báo cáo doanh nghiệp chưa tồn tại trên hệ thống</li>
                            <li>Doanh nghiệp đăng ký mới để hoạt động </li>
                        </ul>
                    </div>
                    <table class="table table-striped  table-hover  dt-responsive nowrap dataTable table-bordered"
                        id="datatable">
                        <thead class=" text-primary">
                            <tr>
                                <th class="text-center">#</th>
                                <th>Tên Doanh Nghiệp</th>
                                <th>Hình thức Đăng Ký</th>
                                <th>Email</th>
                                <th>Số điện thoại</th>
                                <th>Mã số thuế</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pendingCompany as $key => $row)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td class="col">
                                        <a href="" class="text-primary" onclick="company({{ $row->id }})"
                                            data-toggle="modal"
                                            data-target=".bs-example-modal-xl">{{ Str::limit($row->name,30)}}</a>
                                    </td>
                                    <td class="text-center">
                                        @if (empty($row->manager_id))
                                            <span class="text-warning">Sinh Viên</span>
                                        @else
                                            <span class="text-info">Doanh nghiệp</span>
                                        @endif
                                    </td>
                                    <td>{{ $row->email }}</td>
                                    <td>{{ $row->phone }}</td>
                                    <td>{{ $row->tax_number }}</td>
                                    <td>
                                        <a href="{{ route('staff.company.show', $row->id) }}"
                                            class="btn btn-outline-info mr-1" data-toggle="modal"
                                            data-target=".bs-example-modal-xl" onclick="company({{ $row->id }})">
                                            <i class="far fa-address-card"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-company" class="modal fade bs-example-modal-xl" tabindex="-1" role="dialog"
        aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div id="companyModel" class="modal-content">

            </div>
        </div>
    </div>
@endsection
@push('css')
    <!-- third party css -->
    <link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
    <!-- Required datatable js -->
    <script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        if ({{ count($pendingCompany) }} > 10) {
            $(document).ready(function() {
                $('#datatable').DataTable({
                    'aoColumnDefs': [{
                        'bSortable': false,
                        'aTargets': [-1] /* 1st one, start by the right */
                    }]
                });
            });
        }

        function company(id) {
            $("#companyModel").html(`
                <div class="modal-header">
                    <h4 class="modal-title text-warning" id="myExtraLargeModalLabel">Đang tải ... <div class="spinner-border "></div> </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            `)
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: `{{ route('ajax.company') }}`,
                data: {
                    id: id
                },
                success: function(data) {
                    $("#companyModel").html(data)
                },
                error: function() {
                    $('#modal-company').modal('toggle')
                    Swal.fire({
                        type: "error",
                        title: "Lỗi ..!",
                        text: "Hệ thống đang gặp sự cố, vui lòng thử lại sau!",
                        confirmButtonClass: "btn btn-confirm mt-2",
                        footer: '<a href="/admin">Quay về trang chủ</a>'
                    })
                }
            });
        }
    </script>

@endpush
@push('css')
    <style>
        .modal-body img {
            max-height: 250px !important;
            object-fit: cover;
        }

    </style>
@endpush
