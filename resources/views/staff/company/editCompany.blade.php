@extends('layouts.dashboard')
@section('page-title', 'Quản Lý doanh nghiệp | Chỉnh sửa thông tin')
@section('title', 'Quản Lý doanh nghiệp')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('staff.company.index') }}">Danh sách doanh nghiệp</a>
                        </li>
                        <li class="breadcrumb-item active">Chỉnh sửa</li>
                    </ol>
                </div>
                <h4 class="page-title">Chỉnh sửa Doanh Nghiệp</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">

                <form method="POST"  onsubmit="getLoading()" action="{{ route('staff.company.update', [$company->id]) }}"
                    enctype='multipart/form-data'>
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Tên Doanh Nghiệp</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name"
                                    value="{{ !empty(old('name')) ? old('name') : $company->name }}">
                                @error('name')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Mã Doanh Nghiệp</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="code"
                                    value="{{ !empty(old('code')) ? old('code') : $company->code }}">
                                @error('code')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Số Điện Thoại</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone"
                                    value="{{ !empty(old('phone')) ? old('phone') : $company->phone }}">
                                @error('phone')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email"
                                    value="{{ !empty(old('email')) ? old('email') : $company->email }}">
                                @error('email')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Địa Chỉ Website Doanh Nghiệp</label>
                        <div class="col-sm-10">
                          @php
                          $url = json_decode($company->url);
                          @endphp
                            <div class="form-group">
                                <input type="text" class="form-control" name="url"
                                    value="{{ !empty(old('url')) ? old('url') : $url->url }}">
                                @error('url')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Ngày Thành Lập Doanh Nghiệp</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="date" class="form-control" name="founded_at"
                                    value="{{ date('Y-m-d', strtotime($company->founded_at)) }}">
                                @error('founded_at')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Số Thuế</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="tax_number"
                                    value="{{ !empty(old('tax_number')) ? old('tax_number') : $company->tax_number }}">
                                @error('tax_number')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <label class="col-sm-2 col-form-label">Banner Doanh Nghiệp</label>
                        <div class="col-md-10">
                            <div class="form-group" id="banner">
                                @error('thumbnail')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                                <input type="file" class="dropify" name="banner" value="{{ old('thumbnail') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2" >
                        <label class="col-sm-2 col-form-label">Hình Doanh Nghiệp</label>
                        <div class="col-sm-10" id="avatar">
                            <input type="file" class="dropify" name="avatar"
                                value="{{ !empty(old('avatar')) ? old('avatar') : $company->avatar }}">
                            @error('avatar')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    @livewire('change-address-custom', ['cityId' => $company->city_id,'districtId' =>
                    $company->district_id,'wardId' => $company->ward_id])

                    <button type="reset" class="btn btn-outline-info"><i class="fe-refresh-cw mr-1"></i> Làm
                        mới</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-save mr-1"></i> Lưu</button>

                </form>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- form Uploads -->
    <link href="assets/libs/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endpush

@push('script')
    <script src="assets/libs/fileuploads/js/dropify.min.js"></script>
    <script type="text/javascript">
        $('.dropify').dropify({
            defaultFile: "assets/images/1.png",
        });
        let urlImageBanner = `{{ FILE_URL . $company->banner }}`
        if (urlImageBanner) $("#banner").find('img').attr("src", urlImageBanner);
        let urlImageAvatar = `{{ FILE_URL . $company->avatar }}`
        if (urlImageAvatar) $("#avatar img").attr("src", urlImageAvatar);
    </script>

@endpush
