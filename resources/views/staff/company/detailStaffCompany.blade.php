@extends('layouts.dashboard')
@section('page-title', 'Quản lý nhân viên | Tổng quan')
@section('title', 'Quản lý Nhân Viên Doanh Nghiệp')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{route('staff.company.index')}}">Danh sách doanh nghiệp</a></li>
                    <li class="breadcrumb-item active">Chi tiết</li>
                </ol>
            </div>
            <h4 class="page-title">Chi tiết nhân viên</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">

        <div class="card">
            <div class="card-header card-header-icon card-header-primary">
                <h4 class="card-title">Hồ sơ cá nhân <a href="" class="ml-auto">
                    </a></h4>
            </div>
            <div class="card-body">
                <form autocomplete="off">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Họ và tên:</label>
                                <input type="text" class="form-control" name="full_name" value="{{$user->profile->full_name}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">Họ:</label>
                                <input type="text" class="form-control" name="first_name" value="{{$user->profile->first_name}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="bmd-label-floating">Tên:</label>
                                <input type="text" class="form-control" name="last_name" value="{{$user->profile->last_name}}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Email:</label>
                                <input type="name" class="form-control" name="name" disabled value="{{$user->email}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Email cá nhân:</label>
                                <input type="name" class="form-control" name="name" disabled value="{{$user->profile->email_personal}}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Ngày sinh:</label>
                                <input type="name" class="form-control" name="name" disabled value="{{date('d-m-Y',strtotime($user->profile->birthday)) }}">
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Số điện thoại:</label>
                                <input type="text" class="form-control" name="phone" value="{{$user->profile->phone}}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="bmd-label-floating">CMND / CCCD:</label>
                                <input type="text" class="form-control" name="indo" value="{{$user->profile->indo}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="bmd-label-floating">Giới tính:</label>
                                <input type="name" class="form-control" name="name" disabled value="{{$user->profile->gender ? 'Nữ' : 'Nam' }}">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Địa chỉ:</label>
                                <input type="text" class="form-control" name="address" value="{{ $user->profile->getAddress() }}" disabled>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><i class="fas fa-image mr-1"></i> Hình Ảnh</h4>
            </div>

            <div class="card-body">
                <h4 class="header-title m-t-0 m-b-30">Ảnh Đại Diện</h4>
                <img src="{{$user->getAvatar()}}" onerror="this.src='assets/clients/img/avt-default.png'" class="w-100" />
                <h6 class="mt-3 text-primary"><i class=" fas fa-user-graduate"> </i> {{$user->role->name}} </h6>
                @if ($user->is_logged)
                <h6 class="text-success"><i class="fas fa-dot-circle"> </i> <span>Đang hoạt động</span></h6>
                @elseif (!$user->is_active)
                <h6 class="text-danger"><i class="fas fa-dot-circle"> </i> <span>Ngừng hoạt động</span></h6>
                @else
                <h6 class="text-danger"><i class="fas fa-dot-circle"> </i> <span>Chưa tham gia hệ thống</span></h6>
                @endif
                <h3 class="font-weight-bold"> {{$user->name}} </h3>
                <p class="text-primary">Email: {{$user->email}}</p>
                <p class="card-description mt-3 font-italic">
                    {{!empty($user->profile->description) ? $user->profile->description : 'Chưa có thông tin tiểu sử' }}
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<!-- Script You Need -->
@endsection