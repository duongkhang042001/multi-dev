@extends('layouts.dashboard')
@section('page-title', 'Quản Lý doanh nghiệp | Tạo Mới')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{route('staff.company.index')}}">Danh sách doanh nghiệp</a></li>
                        <li class="breadcrumb-item active">Tạo mới</li>
                    </ol>
                </div>
                <h4 class="page-title">Thêm Doanh Nghiệp</h4>
            </div>
        </div>
    </div>
    <form method="POST"  onsubmit="getLoading()" action="{{route('staff.company.store')}}" enctype="multipart/form-data">
        @csrf
        @method('POST')
        <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header">
                    <div class="card-text">
                        <h4 class="d-inline-block">Thêm Doanh Nghiệp</h4>
                        <div class="float-right">
                            <button type="reset" class="btn btn-outline-info"><i class="fe-refresh-cw mr-1"></i> Làm mới</button>
                            <button type="submit" class="btn btn-success"><i class="fa fa-save mr-1"></i> Lưu</button>
                        </div>
                    </div>
                </div>
                <div class="card-body ">

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Tên Doanh Nghiệp</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                    @error('name')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Mã Doanh Nghiệp</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="code" value="{{old('code')}}">
                                    @error('code')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Số Điện Thoại</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="phone" value="{{old('phone')}}">
                                    @error('phone')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                    <input type="email" class="form-control" name="email" value="{{old('email')}}">
                                    @error('email')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Địa Chỉ Website Doanh Nghiệp</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="url" value="{{old('url')}}"
                                           placeholder="https://www.magezon.com">
                                    @error('url')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Ngày Thành Lập Doanh Nghiệp</label>
                            <div class="col-sm-10">
                                    <input type="date" class="form-control" name="founded_at" value="{{old('founded_at')}}">
                                    @error('founded_at')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Mã Số Thuế</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="tax_number" value="{{old('tax_number')}}">
                                    @error('tax_number')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Địa Chỉ</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" name="address" value="{{old('address')}}">
                                    @error('address')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                            </div>
                        </div>
                        <livewire:change-address-custom/>
                </div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Logo</h4>
                    @error('avatar')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                    <input type="file" class="dropify" value="{{old('avatar')}}" />
                </div>
            </div>
            <div class="col-sm-8">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Banner </h4>
                    @error('banner')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                    <input type="file" class="dropify"  value="{{old('banner')}}"/>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('css')
    <!-- form Uploads -->
    <link href="assets/libs/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
    <!-- Script You Need -->
    <livewire:scripts/>
    <!-- file uploads js -->
    <script src="assets/libs/fileuploads/js/dropify.min.js"></script>
    <script type="text/javascript">
        $('.dropify').dropify({
            messages: {
                'default': 'Kéo và thả tệp vào đây hoặc nhấp vào',
                'replace': 'Kéo và thả hoặc nhấp để thay thế',
                'remove': 'Thay đổi',
                'error': 'Rất tiếc, đã xảy ra sự cố.'
            },
            error: {
                'fileSize': 'Kích thước tệp quá lớn (tối đa 1M).'
            }
        });
    </script>
@endpush
