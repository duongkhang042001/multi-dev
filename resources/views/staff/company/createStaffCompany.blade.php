@extends('layouts.dashboard')
@section('page-title', 'Quản Lý nhân viên | Tạo mới')
@section('title', 'Quản Lý nhân viên doanh nghiệp')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{route('staff.company.index')}}">Danh sách doanh nghiệp</a></li>
                    <li class="breadcrumb-item active">Tạo mới</li>
                </ol>
            </div>
            <h4 class="page-title">Thêm nhân viên</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <form method="POST" onsubmit="getLoading()" action="{{route('staff.staff-company.store')}}">
                <input type='hidden' name="company_id" value='{{$companyId}}'>
                @csrf
                @method('POST')
                <div class="row">
                    <label class="col-sm-2 col-form-label">Họ Và Tên</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @error('name')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Mã Nhân Viên</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="code" value="{{ old('code') }}">
                            @error('code')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            @error('email')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Email Cá Nhân</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="email" class="form-control" name="email_personal" value="{{ old('email_personal') }}">
                            @error('email_personal')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Số Điện Thoại</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                            @error('phone')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">CMND</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="indo" value="{{ old('indo') }}">
                            @error('indo')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Ngày Sinh</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="date" class="form-control" id="birthday" name="birthday" value="{{old('birthday')}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label label-checkbox">Giới Tính</label>
                    <div class="col-sm-10 checkbox-radios">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" value="0" id="gender" name="gender" checked> Nam
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" value="1" id="gender" name="gender"> Nữ
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 col-form-label">Địa Chỉ</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                            @error('address')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <livewire:change-address-custom />
                <button type="reset" class="btn btn-outline-info"><i class="fe-refresh-cw mr-1"></i> Làm
                    mới</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-save mr-1"></i> Lưu</button>

            </form>
        </div>
    </div>
</div>
@endsection

@push('script')
<livewire:scripts />
@endpush