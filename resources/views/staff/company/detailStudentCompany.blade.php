@extends('layouts.dashboard')
@section('page-title', 'Quản lý Sinh Viên | Tổng quan')
@section('title', 'Quản lý sinh viên doanh nghiệp')
@section('content')
    @if ($errors->any())
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    @if (session()->has('status'))
        <div class="col-sm-6 ml-3 alert alert-success">
            {{ session()->get('status') }}
        </div>
    @endif
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-5 bg-white">
                    <div class="card-header">
                        <div class="row mt-3">
                            <div class="col-4">
                                <img src="{{$user->avatar}}" onerror="this.src='assets/images/1.png'" class="rounded" id="wizardPicturePreview" title="" width="100" />
                                <!-- <input type="file" id="wizard-picture"> -->
                            </div>
                            <div class="col-7">
                                <div class="user-info">
                                    <a>{{$user->name}}</a>
                                </div>
                                <span style="font-size: 12px;">Nhân Viên</span>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-5">
                                <div class="user-info">
                                    <span>Email:</span>
                                </div>
                                <div class="user-info mt-2">
                                    <span>Mã Số Sinh Viên:</span>
                                </div>
                                <div class="user-info mt-2">
                                    <span>Kỳ Học:</span>
                                </div>
                                <div class="user-info mt-2">
                                    <span>Trạng Thái:</span>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="user-info">
                                    <a>{{$user->email}}</a>
                                </div>
                                <div class="user-info mt-2">
                                    <a>{{$user->code}}</a>
                                </div>
                                <div class="user-info mt-2">
                                    <a>{{$student->current_semester}}</a>
                                </div>
                                <div class="user-info mt-2">
                                    <a>@if ($user->is_logged == 0) Tài khoản chưa kích hoạt
                                        @elseif ($user->is_logged == 1) Tài khoản đã kích hoạt @endif
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3 ml-2 col-sm-4">
                        <form action="{{ route('staff.student-company.destroy', $user->id) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-danger" onclick="return confirm('Bạn Có muốn xóa???');">Xóa</button>
                        </form>
                    </div>
                </div>
                <div class="col-6 bg-white ml-auto">
                    <div class="card-header">
                        Thông Tin Cá Nhân
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="staff-input">
                                    <label class="mt-2">Họ Và Tên:</label>
                                </div>
                                <div class="staff-input">
                                    <label class="mt-3">Email Cá Nhân:</label>
                                </div>
                                <div class="staff-input">
                                    <label class="mt-3">Số Điện Thoại:</label>
                                </div>
                                <div class="staff-input">
                                    <label class="mt-3">CMDN/Căn Cước:</label>
                                </div>
                                <div class="staff-input">
                                    <label class="mt-3">Giới Tính:</label>
                                </div>
                                <div class="staff-input">
                                    <label class="mt-3">Ngày Sinh:</label>
                                </div>
                                <div class="staff-input">
                                    <label class="mt-3">Địa Chỉ:</label>
                                </div>
                                <div class="staff-input">
                                    <label class="mt-3">Phường/Xã:</label>
                                </div>
                                <div class="staff-input">
                                    <label class="mt-3">Quận/Huyện:</label>
                                </div>
                                <div class="staff-input">
                                    <label class="mt-3">Thành Phố/Tỉnh:</label>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="satff-info">
                                    <input type="text" class="form-control text-center" readonly="readonly" value="{{$user->name}}">
                                </div>
                                <div class="satff-info">
                                    <input type="text" class="form-control text-center mt-2" readonly="readonly" value="{{$profile->email_personal}}">
                                </div>
                                <div class="satff-info">
                                    <input type="text" class="form-control text-center mt-2" readonly="readonly" value="{{$profile->phone}}">
                                </div>
                                <div class="satff-info">
                                    <input type="text" class="form-control text-center mt-2" readonly="readonly" value="{{$profile->indo}}">
                                </div>
                                <div class="satff-info">
                                    <input type="text" class="form-control text-center mt-2" readonly="readonly" value="@if($profile->gender==0) Nam @elseif ($profile->gender==1) Nữ @endif ">
                                </div>
                                <div class="satff-info">
                                    <input type="text" class="form-control text-center mt-2" readonly="readonly" value="{{date('d-m-Y', strtotime($profile->birthday))}}">
                                </div>
                                <div class="satff-info">
                                    <input type="text" class="form-control text-center mt-2" readonly="readonly" value="{{$profile->address}}">
                                </div>
                                <div class="satff-info">
                                    <input type="text" class="form-control text-center mt-2" readonly="readonly" value="{{$profile->ward_name}}">
                                </div>
                                <div class="satff-info">
                                    <input type="text" class="form-control text-center mt-2" readonly="readonly" value="{{$profile->district_name}}">
                                </div>
                                <div class="satff-info">
                                    <input type="text" class="form-control text-center mt-2" readonly="readonly" value="{{$profile->city_name}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- Script You Need -->
@endsection
