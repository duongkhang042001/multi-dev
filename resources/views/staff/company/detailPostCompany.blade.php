@extends('layouts.dashboard')
@section('page-title', 'Quản lý bài viết | Tổng quan')
@section('title', 'Chi tiết bài viết')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{route('staff.company.index')}}">Danh sách doanh nghiệp</a></li>
                    <li class="breadcrumb-item active">Chi tiết</li>
                </ol>
            </div>
            <h4 class="page-title">Chi tiết bài đăng tuyển</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card-box text-dark" style="font-size: 15px;">
            <div class="row">
                <div class="col-sm-3 mt-4 text-center">
                    <div style="width: 250px; height: 250px;">
                        <img src="{{$post->recruitmentPost->company->getAvatar()}}" onerror="this.src='assets/images/1.png'" class="w-100" />
                    </div>
                </div>
                <div class="col-sm-9">
                    <h3 class="font-weight-bold mt-5 text-info">{{ $post->title }}</h3>
                    <div class="row ml-0">
                        <p class="text-dark"><i class="fas fa-link text-purple" title="đường dẫn"></i> {{env('APP_URL').'/'.$post->slug }}</p>
                    </div>
                    <div class="row ml-0">
                        <div class="col-sm-3">
                            @if ($post->is_comment == 1)
                            <div class="row">
                                <p class="text-dark">
                                    <i class="fas fa-check-circle text-success"></i> <span class="font-italic"> Mở bình luận</span>
                                </p>
                            </div>
                            @else

                            <div class="row">
                                <p class="text-dark"> <i class="fas fa-times-circle text-danger"></i> <span class="text-dark">Tắt bình luận</span></p>
                            </div>

                            @endif
                        </div>
                        <div class="col-sm-3">
                            @if ($post->is_active == 1)
                            <div class="row">
                                <p class="text-dark">
                                    <i class="fas fa-check-circle text-success"></i> <span>Đang hoạt động</span>
                                </p>
                            </div>
                            @else
                            <div class="row">
                                <p class="text-dark">
                                    <i class="fas fa-times-circle text-danger"></i><span>Không hoạt động</span>
                                </p>
                            </div>
                            @endif
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <p class="text-dark"><i class="fas fa-user-edit text-info" title="người đăng"></i> {{$post->user->name}}</p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <p class="text-dark"><i class="far fa-eye text-pink" title="Lượt xem"></i> {{$post->view}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <h3 class="font-weight-bold">
                        Mô tả bài viết
                    </h3>
                </div>
                <div class="col-sm-12">
                    <p class="text-justify">{!! $post->description !!}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="font-weight-bold">
                        Chi tiết bài viết
                    </h3>
                </div>
                <div class="col-sm-12">
                    <p class="text-break">{!! $post->content !!} </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="font-weight-bold">
                        Yêu cầu bài đăng tuyển
                    </h3>
                </div>
                <div class="col-sm-12">
                    <p class="text-justify">{!! $post->recruitmentPost->requirement !!}</p>
                </div>
            </div>
            <div class="row">
                <a href="{{ route('staff.post-company.edit', $post->id) }}" class="btn btn-ml btn-warning mr-1"><i class="fas fa-pencil-alt"></i> Chỉnh sửa</a>
                <a href="javascript: document.getElementById('deletedRow{{$post->id}}').submit();" onclick="return confirm('Bạn Có muốn xóa???');" class="btn btn-ml btn-danger"><i class="fas fa-trash"></i> Xóa bài viết</a>
                <form action="{{ route('staff.post-company.destroy', $post->id) }}" method="POST" id="deletedRow{{$post->id}}" class="d-none">
                    @method('DELETE')
                    @csrf
                </form>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<!-- Script You Need -->
@endsection