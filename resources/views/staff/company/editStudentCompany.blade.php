@extends('layouts.dashboard')
@section('page-title', 'Quản Lý sinh viên | Chỉnh sửa thông tin')
@section('title', 'Quản lý sinh viên doanh nghiệp')
@section('content')
<div class="col-md-12">
  <div class="card ">
    <div class="card-header card-header-primary card-header-text">
      <div class="card-text">
        <h4 class="card-title">Sửa Sinh Viên</h4>
      </div>
    </div>
    <div class="card-body ">
      <form method="POST" onsubmit="getLoading()" action="{{route('staff.student-company.update',[$user->id])}}">
        @csrf
        @method('PUT')
        <div class="row">
          <label class="col-sm-2 col-form-label">Họ Và Tên</label>
          <div class="col-sm-10">
            <div class="form-group">
              <input type="text" class="form-control" name="name" value="{{!empty(old('name')) ? old('name') : $user->name}}">
              @error('name')
              <span class="text-danger small font-italic">{{ $message }}</span>
              @enderror
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">MSSV</label>
          <div class="col-sm-10">
            <div class="form-group">
              <input type="text" class="form-control" name="code" value="{{!empty(old('code')) ? old('code') : $user->code}}">
              @error('code')
              <span class="text-danger small font-italic">{{ $message }}</span>
              @enderror
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Học Kỳ</label>
          <div class="col-sm-10">
              <select name="semester" value="{{$students->current_semester}}" class="form-control">
                <option value="0" selected>Vui lòng chọn kỳ học</option>
                @for($i=1;$i<=7;$i++) @if($i==$students->current_semester)
                  <option value="{{$i}}" selected>Kỳ {{$i}}</option>
                  @else
                  <option value="{{$i}}">Kỳ {{$i}}</option>
                  @endif
                  @endfor
              </select>
              @error('semester')
              <span class="text-danger small font-italic">{{ $message }}</span>
              @enderror
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Email</label>
          <div class="col-sm-10">
            <div class="form-group">
              <input type="email" class="form-control" name="email" placeholder="Email Trường FPT" value="{{!empty(old('email')) ? old('email') : $user->email}}">
              @error('email')
              <span class="text-danger small font-italic">{{ $message }}</span>
              @enderror
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Email Cá Nhân</label>
          <div class="col-sm-10">
            <div class="form-group">
              <input type="email" class="form-control" name="email_personal" placeholder="Email Cá Nhân" value="{{!empty(old('email_personal')) ? old('email_personal') : $profile->email_personal}}">
              @error('email_personal')
              <span class="text-danger small font-italic">{{ $message }}</span>
              @enderror
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Số Điện Thoại</label>
          <div class="col-sm-10">
            <div class="form-group">
              <input type="text" class="form-control" name="phone" value="{{!empty(old('phone')) ? old('phone') : $profile->phone}}">
              @error('phone')
              <span class="text-danger small font-italic">{{ $message }}</span>
              @enderror
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">CMND</label>
          <div class="col-sm-10">
            <div class="form-group">
              <input type="text" class="form-control" name="indo" value="{{!empty(old('indo')) ? old('indo') : $profile->indo}}">
              @error('indo')
              <span class="text-danger small font-italic">{{ $message }}</span>
              @enderror
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Ngày Sinh</label>
          <div class="col-sm-10">
            <div class="form-group">
              <input type="date" class="form-control" id="birthday" name="birthday" value="{{!empty(old('birthday')) ? old('birthday') : date('Y-m-d', strtotime($profile->birthday))}}">
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Giới Tính</label>
          <div class="col-sm-10">
            <select name="gender" class="form-control form-control-lg mt-3">
              <option value="0" @if($profile->gender==0) selected @endif>Nam</option>

              <option value="1" @if($profile->gender==1) selected @endif>Nữ</option>
            </select>
          </div>
        </div>
        <div class="row">
          <label class="col-sm-2 col-form-label">Địa Chỉ</label>
          <div class="col-sm-10">
            <div class="form-group">
              <input type="text" class="form-control" name="address" value="{{!empty(old('address')) ? old('address') : $profile->address}}">
              @error('address')
              <span class="text-danger small font-italic">{{ $message }}</span>
              @enderror
            </div>
          </div>
        </div>
        @livewire('change-address-custom', ['cityId' => $profile->city_id,'districtId' => $profile->district_id,'wardId' => $profile->ward_id])
        <button class="btn btn-primary" type="submit">Sửa</button>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
<livewire:scripts />
@endsection