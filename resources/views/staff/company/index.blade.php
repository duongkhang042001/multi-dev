@extends('layouts.dashboard')
@section('page-title', 'Quản Lý doanh nghiệp | Tổng quan')
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách doanh nghiệp</li>
                </ol>
            </div>
            <h4 class="page-title">Danh sách doanh nghiệp</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs border-0" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link border-0  active" href="#all" data-toggle="tab">
                            Doanh Nghiệp đang hoạt động

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 " href="#new" data-toggle="tab">
                            Doanh Nghiệp từ chối hoạt động
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-box">
            <div class="tab-content">
                <div class="tab-pane active" id="all">
                    <div class="table-responsive">
                        <h4 class="header-title mt-1"><b>Danh sách sinh viên</b></h4>
                        <p class="sub-header">
                            Bao gồm tất cả sinh viên đang hoạt động trên hệ thống.
                        </p>
                        <table id="datatable1" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th class="col">Tên Doanh Nghiệp</th>
                                    <th>Người quản lý</th>
                                    <th>Trạng thái</th>
                                    <th>Email</th>
                                    <th>Số điện thoại</th>
                                    <th>Mã số thuế</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($company as $key =>$row)

                                <tr>
                                    <td class="col">{{$row->name}}</td>
                                    <td>@if($row->manager)<a href="#" data-toggle="tooltip" title="{{$row->manager->name}}">{{$row->manager->profile->last_name ?? ''}}</a>@else <span class="text-warning font-italic small">(Trống)</span>@endif</td>
                                    <td>
                                        @if($row->is_active)
                                        @if(!$row->on_system)
                                        <span class="text-info">Ngoài hệ thống</span>
                                        @elseif(!$row->manager_id)
                                        <span class="text-warning">Không có quản lý</span>
                                        @else
                                        <span class="text-success">Đang hoạt động</span>
                                        @endif
                                        @else
                                        <span class="text-danger">Ngừng hoạt động</span>
                                        @endif
                                    </td>
                                    <td>{{$row->email}}</td>
                                    <td>{{$row->phone}}</td>
                                    <td>{{$row->tax_number}}</td>
                                    <td class="">
                                        <a href="{{route('staff.company.show',$row->id)}}" class="btn btn-outline-info mr-1" data-toggle="tooltip" title="Xem thông tin chi tiết của doanh nghiệp">
                                            <i class="far fa-address-card"></i>
                                        </a>
                                        <a href="{{ route('staff.company.edit',$row->id) }}" class="btn btn-primary mr-1" data-toggle="tooltip" title="Sửa thông tin doanh nghiệp">
                                            <i class="far fa-edit"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="new">
                    <div class="table-responsive">
                        <h4 class="header-title mt-1"><b>Mới tạo</b></h4>
                        <p class="sub-header">
                            Danh sách sinh viên vừa tạo mới.
                        </p>
                        <table id="datatable2" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class="text-primary">
                                <tr>
                                    <th class="col">Tên Doanh Nghiệp</th>
                                    <th>Người quản lý</th>
                                    <th>Trạng thái</th>
                                    <th>Email</th>
                                    <th>Số điện thoại</th>
                                    <th>Mã số thuế</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($companyDenied as $key =>$row)
                                <tr>
                                    <td class="col">{{$row->name}}</td>
                                    <td>@if($row->manager)<a href="#" data-toggle="tooltip" title="{{$row->manager->name}}">{{$row->manager->profile->last_name ?? ''}}</a>@else <span class="text-warning font-italic small">(Trống)</span>@endif</td>
                                    <td>
                                        @if($row->is_active)
                                        @if(!$row->on_system)
                                        <span class="text-info">Ngoài hệ thống</span>
                                        @elseif(!$row->manager_id)
                                        <span class="text-warning">Không có quản lý</span>
                                        @endif
                                        @endif
                                    </td>
                                    <td>{{$row->email}}</td>
                                    <td>{{$row->phone}}</td>
                                    <td>{{$row->tax_number}}</td>
                                    <td class="">
                                        <a href="{{route('staff.company.show',$row->id)}}" class="btn btn-outline-info mr-1" data-toggle="tooltip" title="Xem thông tin chi tiết của doanh nghiệp">
                                            <i class="far fa-address-card"></i>
                                        </a>
                                        <a href="{{ route('staff.company.edit',$row->id) }}" class="btn btn-primary mr-1" data-toggle="tooltip" title="Sửa thông tin doanh nghiệp">
                                            <i class="far fa-edit"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->

@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
<!-- Required datatable js -->
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datatable1").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)")
    });
    $(document).ready(function() {
        $("#datatable2").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)")
    });
</script>
@endpush
