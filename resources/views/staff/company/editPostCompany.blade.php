@extends('layouts.dashboard')
@section('page-title', 'Quản lý Bài Viết | Chỉnh sửa')
@section('title', 'Chỉnh Bài Viết')
@section('content')
    @if (session()->has('status'))
        <div class="col-sm-6 ml-3 alert alert-success">
            {{ session()->get('status') }}
        </div>
    @endif
    <div class="content">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card ">
                            <div class="card-header card-header-icon card-header-primary">
                                <div class="card-icon">
                                    <i class="material-icons">assignment</i>
                                </div>
                                <h4 class="card-title ">Chỉnh Bài Viết</h4>
                            </div>
                            <div class="card-body ">
                                <form method="POST" onsubmit="getLoading()" action="{{ route('staff.post-company.update', [$post->id]) }}"
                                    class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Tiêu Đề Bài Viết</label>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="title" value="{{!empty(old('title')) ? old('title') : $post->title}}">
                                                @error('title')
                                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Đường dẫn</label>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="slug" value="{{!empty(old('slug')) ? old('slug') : $post->slug}}">
                                                @error('slug')
                                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Mô Tả Bài Viết</label>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <textarea class="form-control" id="description" name="description" rows="10">{{ $post->description }}</textarea>
                                                <script>
                                                    CKEDITOR.replace('description');
                                                </script>
                                                @error('description')
                                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Nội Dung Bài Viết</label>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <textarea class="form-control" id="content" name="content"
                                                    rows="10">{{ $post->content }}</textarea>
                                                <script>
                                                    CKEDITOR.replace('content');
                                                </script>
                                                @error('content')
                                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Hình Ảnh Bài Viết</label>
                                        <div class="col-sm-10 mt-2">
                                            <input type="file" class="form-control-file" name="thumbnail"
                                                value="{{ $post->thumbnail }}">
                                            <img src="{{ FILE_URL . $post->thumbnail }}" class="img-fluid" width="200">
                                            @error('thumbnails')
                                                <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Loại Bài Viết</label>
                                        <div class="col-sm-10"> 
                                            <select name="post-type" value="{{!empty(old('post-type')) ? old('post-type') : $post->{'post-type'} }}"
                                                class="form-control">
                                                <option value="0">Vui lòng chọn loại bài viết</option>
                                                @foreach ($postTypes as $postType)
                                                    <option value="{{ $postType->id }}" @if ($postType->id == $post->post_type_id) selected @endif>
                                                        {{ $postType->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="card-footer ml-auto mr-auto">
                                        <button type="submit" class="btn btn-primary">Chỉnh Bài Viết</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        CKEDITOR.replace('description', 'content', {
            width: "700px",
            height: "400px",
            filebrowserUploadMethod: "form",
        });
    </script>
@endsection
