@extends('layouts.dashboard')
@section('page-title', 'Quản Lý doanh nghiệp | Tổng quan')
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách thông báo</li>
                </ol>
            </div>
            <h4 class="page-title">Danh sách thông báo</h4>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    @php
    $msPerMinute = 60 ;
    $msPerHour = $msPerMinute * 60;
    $msPerDay = $msPerHour * 24;
    $msPerMonth = $msPerDay * 30;
    $msPerYear = $msPerDay * 365;
    @endphp
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover table-bordered dt-responsive nowrap"
                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class='text-primary'>
                        <tr>
                            <th>#</th>
                            <th class="col">Tiêu Đề</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($notifyDetail as $key =>$row)
                        <tr>
                            <td class="text-center">{{$loop->iteration}}</td>
                            <td>
                                <a href="" @if ($row->notify->content)
                                    data-toggle="modal" data-target=".bs-example-modal-lg"
                                    onclick="notify({{$row->id}})"
                                    @else 
                                    onclick="return false;" style="cursor:default"
                                    @endif >{{$row->notify->title}}</a>
                                    <br>
                                    <?php 
                                    $elapsed = time() - strtotime($row->created_at);
                                    if ($elapsed < $msPerMinute) {
                                        echo ceil($elapsed).' giây trước';   
                                    }
                                    else if ($elapsed < $msPerHour) {
                                        echo ceil($elapsed/$msPerMinute).' phút trước';   
                                    }
                                    else if ($elapsed < $msPerDay ) {
                                        echo ceil($elapsed/$msPerHour ).' giờ trước';   
                                    }
                                    else if ($elapsed < $msPerMonth) {
                                        echo ceil($elapsed/$msPerDay).' Ngày trước';   
                                    }
                                    else if ($elapsed < $msPerYear) {
                                        echo ceil($elapsed/$msPerMonth).' tháng trước';   
                                    }
                                    else {
                                        echo ceil($elapsed/$msPerYear ).' năm trước';   
                                    }
                          ?>
                            </td>
                            <td>
                                <a href="/" class="btn btn-outline-info mr-1"
                                @if ($row->notify->content)
                                data-toggle="modal" data-target=".bs-example-modal-lg"
                                onclick="notify({{$row->id}})"
                                @else 
                                onclick="return false;"
                                @endif
                                   >
                                    <i class="far fa-address-card"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div id="notifyModel" class="modal-content">

        </div>
    </div>
</div>
@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
<!-- Required datatable js -->
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script>
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
            $("#datatable1").DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)")
        });
</script>
<script type="text/javascript">
   function notify(id) {
        $("#notifyModel").html(`
                <div class="modal-header">
                    <h4 class="modal-title text-warning" id="myExtraLargeModalLabel">Đang tải ... <div class="spinner-border "></div> </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            `)
        $.ajax({
            type: 'POST', //THIS NEEDS TO BE GET
            url: `{{route('ajax.staffGetNotify')}}`,
            data: {
                _token: '{{csrf_token()}}',
                id: id
            },
            success: function(data) {
                console.log(data);
                $("#notifyModel").html(`<div class="modal-header">
                        <h4 class="modal-title text-success text-center" id="myExtraLargeModalLabel">${data['notify']['title']}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-12" style="font-size: 15px;">
                                                <span class="text-dark">
                                                    <h4>Nội dung thông báo</h4>${data['notify']['content']}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                `)
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
</script>
@endpush