@extends('layouts.dashboard')
@section('page-title', 'Quản Lý doanh nghiệp | Tổng quan')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Thông báo chi tiết</li>
                </ol>
            </div>
            <h4 class="page-title">Thông báo chi tiết</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="text-left col-sm-12" style="border: 1px dashed #ccc; padding: 15px 25px;">
                <h4 class="col-12 text-primary text-center mt-2">{{$notifyDetail->notify->title}}</h4>
                <div class="education-label">
                    <span class="study-year font-weight-bold ">Nội dung thông báo:</span>
                    <div class="warning-report mt-3">
                        {!!$notifyDetail->notify->content!!}
                    </div>
                </div>
                <br>
                <div class="float-right"><small>( Thời gian: {{ date('d/m/Y H:i:s' ,
                        strtotime($notifyDetail->created_at)) }} )</small></div><br>
            </div>
        </div>
    </div>
</div>
@endsection