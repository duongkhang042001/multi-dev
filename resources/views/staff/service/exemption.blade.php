@extends('layouts.dashboard')
@section('page-title', 'Sinh viên miễn giảm thực tập | Danh sách')
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách sinh viên miễn giảm thực tập</li>
                </ol>
            </div>
            <h4 class="page-title">Danh sách sinh viên miễn giảm thực tập</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs border-0" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link border-0  active" href="#all" data-toggle="tab">
                            Tất Cả
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 " href="#new" data-toggle="tab">
                            Chưa xử lý
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 " href="#not-active" data-toggle="tab">
                            Đã xử lý
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 " href="#cancel" data-toggle="tab">
                            Sinh viên huỷ đăng ký
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-box">
            <div class="tab-content">
                <div class="tab-pane active" id="all">
                    <div class="table-responsive">
                        <table id="datatable1" class="table table-striped table-hover text-nowrap nowrap table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class='text-primary'>
                                    <tr>
                                        <th class="col-sm-1">MSSV</th>
                                        <th class="col-sm-2">Họ và Tên</th>
                                        <th class="col-sm-2">Email</th>
                                        <th class="col-sm-3">Nội dung</th>
                                        <th class="col-sm-2">Trạng thái</th>
                                        <th class="col-sm-2">Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($allExemption as $row)
                                        @php
                                            $item = json_decode($row->file);
                                        @endphp
                                        <tr>
                                            <td>{{ $row->user->code }}</td>
                                            <td class="col">{{ $row->user->name }}</td>
                                            <td>{{ $row->user->email }}</td>
                                            <td>@if($row->description) {{ $row->description }} @else Không có @endif</td>
                                            <td>
                                                @if (!empty($row->status))
                                                    @if ($row->status == SERVICE_STATUS_PENDING)
                                                    <p class="text-warning">Chưa xử lý</p>
                                                    @elseif($row->status == SERVICE_STATUS_APPROVED)
                                                    <p class="text-primary">Chấp nhận yêu cầu</p>
                                                    @elseif($row->status == SERVICE_STATUS_DENINED)
                                                    <p class="text-danger">Huỷ yêu cầu</p>
                                                    @elseif($row->status == SERVICE_STATUS_CANCEL)
                                                    <p class="text-danger">Sinh viên huỷ yêu cầu</p>
                                                    @endif
                                                @endif
                                            </td>
                                            <td class="col-sm-2">
                                                @php
                                                $item = json_decode($row->file);
                                                @endphp
                                                @if($row->status == SERVICE_STATUS_PENDING)
                                                <div class="d-flex">
                                                    <a href="{{ \App\Http\Controllers\Controller::getFilePdf($item->code) }}" target="_blank" class="btn btn-outline-info mr-1" title="Xem hồ sơ sinh viên"><i class="far fa-file-alt"></i></a>
                                                    <a href="javascript:void(exemptionSuccess('updateRow{{ $row->id }}'))" class="btn btn-outline-success mr-1" title="Chấp nhận yêu cầu sinh viên"><i class="far fa-check-circle"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-outline-danger mr-1" title="Huỷ yêu cầu sinh viên" onclick="denined('{{$row->id}}')"><i class="far fa-times-circle"></i></a>
                                                </div>
                                                @else
                                                <a href="{{ \App\Http\Controllers\Controller::getFilePdf($item->code) }}" target="_blank" class="btn btn-info text-center" title="Xem hồ sơ sinh viên"><i class="far fa-file-alt"></i></a>
                                                @endif
                                                <form action="{{ route('staff.exemption.success',$row->id) }}" method="POST" id="updateRow{{ $row->id }}" class="d-none" onclick="getLoading()">
                                                    @method('PUT')
                                                    @csrf
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="new">
                    <div class="table-responsive">
                        <table id="datatable2" class="table table-striped table-hover table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th class="col-sm-1">MSSV</th>
                                    <th class="col-sm-2">Họ và Tên</th>
                                    <th class="col-sm-2">Email</th>
                                    <th class="col-sm-3">Nội dung</th>
                                    <th class="col-sm-2">Trạng thái</th>
                                    <th class="col-sm-2">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($waitingExemption as $row)
                                @php
                                    $item = json_decode($row->file);
                                @endphp
                                <tr>
                                    <td class="col-sm-1">{{ $row->user->code }}</td>
                                    <td class="col-sm-2">{{ $row->user->name }}</td>
                                    <td class="col-sm-2">{{ $row->user->email }}</td>
                                    <td class="col-sm-3">@if($row->description) {{ $row->description }} @else  Không có  @endif</td>
                                    <td class="col-sm-2">
                                        <p class="text-warning">Chưa xử lý</p>
                                    </td>
                                    <td class="col-sm-2">
                                        @php
                                        $item = json_decode($row->file);
                                        @endphp
                                        <div class="d-flex">
                                            <a href="{{ \App\Http\Controllers\Controller::getFilePdf($item->code) }}" target="_blank" class="btn btn-outline-info mr-1" title="Xem hồ sơ sinh viên"><i class="far fa-file-alt"></i></a>
                                            <a href="javascript:void(exemptionSuccess('updateRow{{ $row->id }}'))" class="btn btn-outline-success mr-1" title="Chấp nhận yêu cầu sinh viên"><i class="far fa-check-circle"></i></a>
                                            <a href="javascript:void(0)" class="btn btn-outline-danger mr-1" title="Huỷ yêu cầu sinh viên" onclick="denined('{{$row->id}}')"><i class="far fa-times-circle"></i></a>
                                            <form action="{{ route('staff.exemption.success',$row->user_id) }}" method="POST" id="updateRow{{ $row->user_id }}" class="d-none">
                                                @method('PUT')
                                                @csrf
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="not-active">
                    <div class="table-responsive">
                        <table id="datatable3" class="table table-striped table-hover table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th class="col-sm-1">MSSV</th>
                                    <th class="col-sm-2">Họ và Tên</th>
                                    <th class="col-sm-2">Email</th>
                                    <th class="col-sm-3">Nội dung</th>
                                    <th class="col-sm-2">Trạng thái</th>
                                    <th class="col-sm-2">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($approvedExemption as $row)
                                @php
                                    $item = json_decode($row->file);
                                @endphp
                                <tr>
                                    <td class="col-sm-1">{{ $row->user->code }}</td>
                                    <td class="col-sm-2">{{ $row->user->name }}</td>
                                    <td class="col-sm-2">{{ $row->user->email }}</td>
                                    <td class="col-sm-3">@if($row->description) {{ $row->description }} @else  Không có  @endif</td>
                                    <td class="col-sm-2">
                                        @if (!empty($row->status))
                                            @if($row->status == SERVICE_STATUS_APPROVED)
                                            <p class="text-primary">Chấp nhận yêu cầu</p>
                                            @elseif($row->status == SERVICE_STATUS_DENINED)
                                            <p class="text-danger">Huỷ yêu cầu</p>
                                            @endif
                                        @endif
                                    </td>
                                    <td class="col-sm-2">
                                    <a href="{{ \App\Http\Controllers\Controller::getFilePdf($item->code) }}" target="_blank" class="btn btn-outline-info mr-1" title="Xem hồ sơ sinh viên"><i class="far fa-address-card"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="cancel">
                    <div class="table-responsive">
                        <table id="datatable4" class="table table-striped table-hover table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th class="col-sm-1">MSSV</th>
                                    <th class="col-sm-2">Họ và Tên</th>
                                    <th class="col-sm-2">Email</th>
                                    <th class="col-sm-3">Nội dung</th>
                                    <th class="col-sm-2">Trạng thái</th>
                                    <th class="col-sm-2">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cancelExemption as $row)
                                    @php
                                        $item = json_decode($row->file);
                                    @endphp
                                    <tr>
                                        <td class="col-sm-1">{{ $row->user->code }}</td>
                                        <td class="col-sm-2">{{ $row->user->name }}</td>
                                        <td class="col-sm-2">{{ $row->user->email }}</td>
                                        <td class="col-sm-3">@if($row->description) {{ $row->description }} @else  Không có  @endif</td>
                                        <td class="col-sm-2">
                                            <p class="text-danger">Sinh viên huỷ yêu cầu</p>
                                        </td>
                                        <td class="col-sm-2">
                                        <a href="{{ \App\Http\Controllers\Controller::getFilePdf($item->code) }}" target="_blank" class="btn btn-info mr-1" title="Xem hồ sơ sinh viên"><i class="far fa-file-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div id="student-modal" class="modal-content">

        </div>
    </div>
</div>
@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/switchery/switchery.min.css" rel="stylesheet" type="text/css" />
@endpush

@push('script')
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<script src="assets/libs/switchery/switchery.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
@endpush

@push('script')
<script>
    $(document).ready(function() {
        $('#datatable1').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable2').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable3').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable3_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable4').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable4_wrapper .col-md-6:eq(0)");
    });
    function getStudent(id) {
        $("#student-modal").html(`
                <div class="modal-header">
                    <h4 class="modal-title text-warning" id="myExtraLargeModalLabel">Đang tải ... <div class="spinner-border "></div> </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            `);
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: `{{ route('ajax.student.reset-report') }}`,
            data: {
                id: id
            },
            success: function(data) {
                $("#student-modal").html(data);
            },
            error: function(data) {
                console.log(data);
            }
        });

    }
    function denined(id) {
        Swal.fire({
            title: "Lý do để bạn từ chối đăng ký </br> thực tập lại là gì ?",
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Đồng ý',
            cancelButtonText: 'Đóng',
            showLoaderOnConfirm: true,
            preConfirm: (reason) => {
                return reason;
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((reason) => {
            if (reason.value === "" || reason.value) {
               getLoading();
                $.ajax({
                    type: "PUT",
                    url: "{{ route('staff.exemption.fail') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        data: {
                            id: id,
                            reason: reason.value,
                        },
                    },
                    success: function(data) {
                        removeLoading();
                        if (data['error']) {
                            Swal.fire({
                                type: 'error',
                                title: 'Vui lòng nhập lý do !',
                                text: 'Bạn phải nhập lý do để từ chối đăng ký !',
                            })
                        } else {
                            swal.fire({
                                title: "Đã huỷ đăng ký thực tập lại !",
                                text: "Bạn đã huỷ đăng ký thực tập lại của sinh viên",
                                buttonsStyling: false,
                                confirmButtonClass: "btn btn-success",
                                type: "success",
                                confirmButtonText: 'Đồng ý !',
                            }).then(() => window.location.reload())
                            .catch(swal.noop)
                        }
                    },
                    error: function(data) {
                        swal({
                            title: "Hệ thống đã xảy ra lỗi !",
                            text: "Vui lòng quay lại sau !",
                            buttonsStyling: false,
                            confirmButtonClass: "btn btn-warning",
                            type: "warning",
                            confirmButtonText: 'Đồng ý !',
                        }).catch(swal.noop)

                    }
                });
            }

        });

    }
</script>
@endpush
