@extends('layouts.dashboard')
@section('page-title', 'Sinh viên đăng ký thực tập lại | Danh sách')
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách sinh viên đăng ký thực tập lại</li>
                </ol>
            </div>
            <h4 class="page-title">Danh sách sinh viên đăng ký thực tập lại</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs border-0" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link border-0  active" href="#all" data-toggle="tab">
                            Tất Cả
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 " href="#new" data-toggle="tab">
                            Chưa xử lý
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 " href="#not-active" data-toggle="tab">
                            Đã xử lý
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 " href="#denined" data-toggle="tab">
                            Sinh viên huỷ đăng ký
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-box">
            <div class="tab-content">
                <div class="tab-pane active" id="all">
                    <div class="table-responsive">
                        <table id="datatable1" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th class="text-center">Mã số sinh viên</th>
                                    <th class="text-center">Họ và Tên</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Thời gian đăng ký</th>
                                    <th class="text-center">Trạng thái</th>
                                    <th class="text-center">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($allInternshipAgain as $row)
                                <tr>
                                    <td class="text-center">{{ $row->user->code }}</td>
                                    <td class="text-center">{{ $row->user->name }}</td>
                                    <td class="text-center">{{ $row->user->email }}</td>
                                    <td class="text-center">{{ $row->created_at  }}</td>
                                    <td class="text-center">
                                        @if (!empty($row->status))
                                        @if ($row->status == SERVICE_STATUS_PENDING)
                                        <p class="text-warning">Chưa xử lý</p>
                                        @elseif($row->status == SERVICE_STATUS_APPROVED)
                                        <p class="text-success">Chấp nhận yêu cầu</p>
                                        @elseif($row->status == SERVICE_STATUS_DENINED)
                                        <p class="text-danger">Huỷ yêu cầu</p>
                                        @endif
                                        @endif
                                    </td>
                                    @if($row->status == SERVICE_STATUS_PENDING)
                                    <td class="text-center">
                                        <a href="javascript:void(internshipPass('updateRow{{ $row->user_id }}'))" class="btn btn-outline-success mr-1" title="Chấp nhận yêu cầu sinh viên"><i class="far fa-check-square"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-outline-danger mr-1" title="Huỷ yêu cầu sinh viên" onclick="denined('{{$row->id}}')"><i class="far fa-times-circle"></i></a>
                                        <form action="{{ route('staff.internship-again.pass',$row->id) }}" method="POST" id="updateRow{{ $row->user_id }}" class="d-none">
                                            @method('PUT')
                                            @csrf
                                        </form>
                                    </td>
                                    @else
                                    <td class="text-center"></td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="new">
                    <div class="table-responsive">
                        <table id="datatable2" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th class="text-center">Mã số sinh viên</th>
                                    <th class="text-center">Họ và Tên</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Thời gian đăng ký</th>
                                    <th class="text-center">Trạng thái</th>
                                    <th class="text-center">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($waitingInternshipAgain as $row)
                                <tr>
                                    <td class="text-center">{{ $row->user->code }}</td>
                                    <td class="text-center">{{ $row->user->name }}</td>
                                    <td class="text-center">{{ $row->user->email }}</td>
                                    <td class="text-center">{{ $row->created_at  }}</td>
                                    <td class="text-center">
                                        <p class="text-warning">Chưa xử lý</p>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:void(internshipPass('updateRow{{ $row->user_id }}'))" class="btn btn-outline-success mr-1" title="Chấp nhận yêu cầu sinh viên"><i class="far fa-check-square"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-outline-danger mr-1" title="Huỷ yêu cầu sinh viên" onclick="denined('{{$row->id}}')"><i class="far fa-times-circle"></i></a>
                                        <form action="{{ route('staff.internship-again.pass',$row->id) }}" method="POST" id="updateRow{{ $row->user_id }}" class="d-none">
                                            @method('PUT')
                                            @csrf
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="not-active">
                    <div class="table-responsive">
                        <table id="datatable3" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th class="text-center">Mã số sinh viên</th>
                                    <th class="text-center">Họ và Tên</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Thời gian đăng ký</th>
                                    <th class="text-center">Trạng thái</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($approvedInternshipAgain as $row)
                                <tr>
                                    <td class="text-center">{{ $row->user->code }}</td>
                                    <td class="text-center">{{ $row->user->name }}</td>
                                    <td class="text-center">{{ $row->user->email }}</td>
                                    <td class="text-center">{{ $row->created_at  }}</td>
                                    <td class="text-center">
                                        <p class="text-primary">Chấp nhận yêu cầu</p>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="denined">
                    <div class="table-responsive">
                        <table id="datatable4" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th class="text-center">Mã số sinh viên</th>
                                    <th class="text-center">Họ và Tên</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Thời gian đăng ký</th>
                                    <th class="text-center">Trạng thái</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($deninedInternshipAgain as $row)
                                <tr>
                                    <td class="text-center text-danger">{{ $row->user->code }}</td>
                                    <td class="text-center text-danger">{{ $row->user->name }}</td>
                                    <td class="text-center text-danger">{{ $row->user->email }}</td>
                                    <td class="text-center text-danger">{{ $row->created_at  }}</td>
                                    <td class="text-center text-danger">
                                        <p class="text-danger">Sinh viên huỷ đăng ký</p>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div id="student-modal" class="modal-content">

        </div>
    </div>
</div>
@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/switchery/switchery.min.css" rel="stylesheet" type="text/css" />
@endpush

@push('script')
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<script src="assets/libs/switchery/switchery.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
@endpush

@push('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable1').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable2').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable3').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable3_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable4').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable4_wrapper .col-md-6:eq(0)");
    });

    function getStudent(id) {
        $("#student-modal").html(`
                <div class="modal-header">
                    <h4 class="modal-title text-warning" id="myExtraLargeModalLabel">Đang tải ... <div class="spinner-border "></div> </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            `);
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: `{{ route('ajax.student.reset-report') }}`,
            data: {
                id: id
            },
            success: function(data) {
                $("#student-modal").html(data);
            },
            error: function(data) {
                console.log(data);
            }
        });

    }

    function denined(id) {
        Swal.fire({
            title: "Lý do để bạn từ chối đăng ký </br> thực tập lại là gì ?",
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Đồng ý',
            cancelButtonText: 'Đóng',
            showLoaderOnConfirm: true,
            preConfirm: (reason) => {
                return reason;
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((reason) => {
            if (reason.value === "" || reason.value) {
               getLoading();
                $.ajax({
                    type: "PUT",
                    url: "{{ route('staff.internship-again.fail') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        data: {
                            id: id,
                            reason: reason.value,
                        },
                    },
                    success: function(data) {
                        removeLoading();
                        if (data['error']) {
                            Swal.fire({
                                type: 'error',
                                title: 'Vui lòng nhập lý do !',
                                text: 'Bạn phải nhập lý do để từ chối đăng ký !',
                            })
                        } else {
                            swal.fire({
                                title: "Đã huỷ đăng ký thực tập lại !",
                                text: "Bạn đã huỷ đăng ký thực tập lại của sinh viên",
                                buttonsStyling: false,
                                confirmButtonClass: "btn btn-success",
                                type: "success",
                                confirmButtonText: 'Đồng ý !',
                            }).then(() => window.location.reload())
                            .catch(swal.noop)
                        }
                    },
                    error: function(data) {
                        swal({
                            title: "Hệ thống đã xảy ra lỗi !",
                            text: "Vui lòng quay lại sau !",
                            buttonsStyling: false,
                            confirmButtonClass: "btn btn-warning",
                            type: "warning",
                            confirmButtonText: 'Đồng ý !',
                        }).catch(swal.noop)

                    }
                });
            }

        });

    }
</script>
@endpush