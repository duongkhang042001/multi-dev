@extends('layouts.dashboard')
@section('page-title', 'Quản Lý Sinh Viên | Danh sách')
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách phản hồi</li>
                </ol>
            </div>
            @if($feedback->type == FEEDBACK_STUDENT)
            <h4 class="page-title">Phản hồi từ sinh viên</h4>
            @elseif($feedback->type == FEEDBACK_COMPANY)
            <h4 class="page-title">Phản hồi từ doanh nghiệp</h4>
            @else
            <h4 class="page-title">Phản hồi từ người dùng</h4>
            @endif
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-sm-12 col-lg-12 col-md-12">
        <div class="card-box">
            <div class="mt-4">
                <h4>{{$feedback->title}}</h4>
                <hr />
                <div class="media mb-3 mt-1">
                    <img class="d-flex mr-3 rounded-circle avatar-sm" onerror="this.src='assets/images/1.png'" src="{{!empty($feedback->user->avatar) ? $feedback->user->getAvatar() : null }}">
                    <div class="media-body">
                        <span class="float-right">Ngày phản hồi: {{date('d/m/Y H:i:s',strtotime($feedback->created_at))}}</span>
                        <div class="row">
                            <div class="col-sm-4">
                                <h5 class="m-0 mb-1">{{!empty($feedback->user->name) ? $feedback->user->name : $feedback->full_name}}</h5>
                                <span><i class="fas fa-envelope"> </i> {{!empty($feedback->user->email) ? $feedback->user->email : $feedback->email}}</span> <br>
                            </div>
                            <div class="col-sm-8">
                                <span>
                                    @if($feedback->type == FEEDBACK_STUDENT)
                                    <i class="fas fa-user-alt"></i> Sinh Viên
                                    @elseif($feedback->type == FEEDBACK_COMPANY)
                                    <i class="fas fa-suitcase"></i> Tên Doanh Nghiệp: {{$feedback->company->name}}
                                    @else($feedback->type == FEEDBACK_ALL)
                                    <i class="fas fa-user-secret"></i> Đối tượng: Ẩn danh
                                    @endif
                                </span> <br>
                                <span><i class="fas fa-phone-volume"> </i> {{!empty($feedback->user->profile->phone) ? $feedback->user->profile->phone : $feedback->phone}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="ml-2"><b>Xin chào nhà trường...</b></p>
                <p class="ml-2">{{$feedback->content}}</p>
            </div> <!-- card-box -->

            @if($feedback->type == FEEDBACK_ALL)
            <div class="col-sm-12 mt-4">
                <a id="hideshow">
                    <button type="button" rel="tooltip" class="btn btn-outline-primary mr-1">
                        <i class="fas fa-pencil-alt"></i> Gửi phản hồi
                    </button>
                </a>
                <a href="javascript: document.getElementById('deletedRow{{$feedback->id}}').submit();" onclick="return confirm('Bạn Có muốn xóa?');" title="xóa">

                    <button type="button" rel="tooltip" class="btn btn-outline-secondary">
                        <i class="fas fa-trash"></i> Xóa phản hồi
                    </button>
                </a>
                <form action="{{ route('staff.feedback.destroy', $feedback->id) }}" method="POST" id="deletedRow{{$feedback->id}}" class="d-none">
                    @method('DELETE')
                    @csrf
                </form>
                    @php
                        $email = !empty($feedback->user->email) ? $feedback->user->email : $feedback->email;
                    @endphp
                <div id="form1" class="mt-2" style="display: none;">

                    <form action="{{route('staff.feedback.sendMailStudent',$email)}}" method="post">
                        @method('POST')
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" id="content" name="content" rows="10" value="{{old('content')}}"></textarea>
                                </div>
                            </div>
                            <button rel="tooltip" class="btn btn-outline-primary ml-2">
                                <i class="fas fa-paper-plane"></i> Gửi Email
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            @elseif($feedback->type == FEEDBACK_STUDENT)
            <div class="media mb-0 mt-5">
                <img class="d-flex mr-3 rounded-circle avatar-sm" src="{{Auth::user()->getAvatar()}}" onerror="this.src='assets/images/1.png'" alt="Generic placeholder image">
                <div class="media-body">
                    <div class="summernote">
                        <h5>{{Auth::user()->name}}</h5>
                        @if($feedback->content_return == NULL)
                        <form action="{{ route('staff.feedback.reply',$feedback->id)}}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" id="content" name="content" rows="10" value="{{old('content')}}"></textarea>
                                    @error('description')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row ml-3">
                                <button rel="tooltip" class="btn btn-info mr-2">
                                    <i class="fab fa-telegram-plane"></i> Gửi Phản Hồi
                                </button>
                                <a href="javascript: document.getElementById('deletedRow{{$feedback->id}}').submit();" onclick="return confirm('Bạn Có muốn xóa?');" title="xóa">
                                    <button type="button" rel="tooltip" class="btn btn-outline-secondary">
                                        <i class="fas fa-trash"></i> Xóa phản hồi
                                    </button>
                                </a>
                            </div>

                        </form>
                        @else
                        <p id="feedbackReply">{!!$feedback->content_return!!}</p>
                        <br>
                        <button id="btn" class="btn btn-outline-primary">
                            <i class=" fas fa-pencil-alt"></i>
                            <span>Sửa phản hồi</span>
                        </button>
                        <button id="btn" class="btn btn-outline-secondary">
                            <i class="far fa-trash-alt"></i>
                            <span>Xóa phản hồi</span>
                        </button>

                        @endif
                        <div id="form" class="mt-2" style="display: none;">
                            <form action="{{ route('staff.feedback.reply',$feedback->id)}}" method="post">
                                @method('PUT')
                                @csrf
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- <input type="hidden" value="{{$feedback->id}}"> -->
                                        <div class="form-group">
                                            <textarea class="form-control" id="content" name="content" rows="10" value="{{old('content')}}">{!!$feedback->content_return!!}</textarea>
                                        </div>
                                    </div>
                                    <div class="row ml-3">
                                        <button rel="tooltip" class="btn btn-outline-primary">
                                            <i class="fas fa-paper-plane"></i> Gửi phản hồi
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            @else
            <div class="media mb-0 mt-5">
                <img class="d-flex mr-3 rounded-circle avatar-sm" src="{{Auth::user()->getAvatar()}}" onerror="this.src='assets/images/1.png'" alt="Generic placeholder image">
                <div class="media-body">
                    <div class="summernote">
                        <h5>{{Auth::user()->name}}</h5>
                        @if($feedback->content_return == NULL)
                        <form action="{{ route('staff.feedback.reply',$feedback->id)}}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" id="content" name="content" rows="10" value="{{old('content')}}"></textarea>
                                    @error('description')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row ml-3">
                                <button rel="tooltip" class="btn btn-outline-primary mr-2">
                                    <i class="fab fa-telegram-plane"></i> Gửi Phản Hồi
                                </button>
                                <a href="javascript: document.getElementById('deletedRow{{$feedback->id}}').submit();" onclick="return confirm('Bạn Có muốn xóa?');" title="xóa">
                                    <button type="button" rel="tooltip" class="btn btn-outline-secondary">
                                        <i class="fas fa-trash"></i> Xóa phản hồi
                                    </button>
                                </a>
                            </div>

                        </form>
                        @else
                        <p id="feedbackReply">{!!$feedback->content_return!!}</p>
                        <br>
                        <button id="btn" class="btn btn-outline-primary">
                            <i class=" fas fa-pencil-alt"></i>
                            <span>Sửa phản hồi</span>
                        </button>
                        <button id="btn" class="btn btn-outline-secondary">
                            <i class="far fa-trash-alt"></i>
                            <span>Xóa phản hồi</span>
                        </button>

                        @endif
                        <div id="form" class="mt-2" style="display: none;">
                            <form action="{{ route('staff.feedback.reply',$feedback->id)}}" method="post">
                                @method('PUT')
                                @csrf
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- <input type="hidden" value="{{$feedback->id}}"> -->
                                        <div class="form-group">
                                            <textarea class="form-control" id="content" name="content" rows="10" value="{{old('content')}}">{!!$feedback->content_return!!}</textarea>
                                        </div>
                                    </div>
                                    <div class="row ml-3">
                                        <button rel="tooltip" class="btn btn-outline-primary mr-2">
                                            <i class="fas fa-paper-plane"></i> Gửi phản hồi
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            @endif
        </div>
    </div>
</div>

@endsection
@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')

<!-- Required datatable js -->
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#btn').on('click', function(event) {
            jQuery('#form').toggle('show');
        });
    });

    jQuery(document).ready(function() {
        jQuery('#hideshow').on('click', function(event) {
            jQuery('#form1').toggle('show');
        });
    });
</script>
@endpush
