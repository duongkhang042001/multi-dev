@extends('layouts.dashboard')
@section('page-title', 'Quản Lý Sinh Viên | Danh sách')
@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item active">Danh sách phản hồi</li>
                    </ol>
                </div>
                <h4 class="page-title">Danh sách phản hồi</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs border-0" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link border-0  active" href="#all" data-toggle="tab">
                                Tất Cả

                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link border-0 " href="#new" data-toggle="tab">
                                Doanh Nghiệp
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link border-0 " href="#not-active" data-toggle="tab">
                                Sinh Viên
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-box">
                <div class="tab-content">
                    <div class="tab-pane active" id="all">
                        <div class="table-responsive">
                            <h4 class="header-title mt-1"><b>Danh sách phản hồi từ người dùng ẩn danh</b></h4>
                            <table id="datatable1" class="table table-striped table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="">
                                    <tr>
                                        <th>#</th>
                                        <th class="text-center">Nội Dung</th>
                                        <th class="text-center">Trạng Thái</th>
                                        <th class="text-center">Ngày gửi</th>
                                        <th class="text-center">Người gửi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($feedbackAll as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>

                                            <td>

                                                <a href="{{ route('staff.feedback.show', [$row->id]) }}"
                                                    class="">
                                                    <div class="row">
                                                        {{ Str::limit($row->content, 30) }}
                                                    </div>
                                                </a>

                                            </td>

                                            <td>
                                                <div class="row" style="margin-left: 30%;">
                                                    @if ($row->is_active == 1)
                                                        <p class="font-italic text-success">Đã xem</p>
                                                    @else
                                                        <p class="text-warning"> Chưa xem</p>
                                                    @endif
                                                </div>

                                            </td>
                                            <td class="font-italic text-center">
                                                {{ date('d/m/Y H:i:s', strtotime($row->created_at)) }}
                                                <br>
                                            </td>
                                            <td class="text-center">
                                                <a href="">
                                                    {{ $row->full_name }}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="tab-pane" id="new">
                        <div class="table-responsive">
                            <h4 class="header-title mt-1"><b>Phản hổi từ Doanh Nghiệp</b></h4>
                            <table id="datatable2" class="table table-striped table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="">
                                    <tr>
                                        <th>#</th>
                                        <th class="text-center">Nội Dung</th>
                                        <th class="text-center">Trạng Thái</th>
                                        <th class="text-center">Phản hồi</th>
                                        <th class="text-center">Ngày Gửi</th>
                                        <th class="text-center">Người Gửi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($feedbackCompany as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>

                                            <td>
                                                <div class="row">
                                                    <a href="{{ route('staff.feedback.show', [$row->id]) }}"
                                                        class="">
                                                        {{ Str::limit($row->content, 50) }}
                                                    </a>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="row" style="margin-left: 30%;">
                                                    @if ($row->is_active == 1)
                                                        <p class="font-italic">Đã xem</p>
                                                    @else
                                                        <p> Chưa xem</p>
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                @if ($row->user_return == null)
                                                    <p class="font-italic text-warning">Chưa phản hồi</p>
                                                @else
                                                    <p class="font-italic text-success">Đã phản hồi</p>
                                                @endif
                                            </td>
                                            <td class="font-italic text-center">
                                                {{ date('d/m/Y H:i:s', strtotime($row->created_at)) }}
                                                <br>
                                            </td>
                                            <td class="text-center">
                                                <a href="">
                                                    {{ $row->user->name }}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="not-active">
                        <div class="table-responsive">
                            <h4 class="header-title mt-1"><b>Phản hổi từ sinh viên</b></h4>
                            <table id="datatable3" class="table table-striped table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="">
                                    <tr>
                                        <th>#</th>
                                        <th class="text-center">Nội Dung</th>
                                        <th class="text-center">Trạng Thái</th>
                                        <th class="text-center">Phản hồi</th>
                                        <th class="text-center">Ngày Gửi</th>
                                        <th class="text-center">Người Gửi</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($feedbackStudent as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>

                                            <td>
                                                <div class="row">
                                                    <a href="{{ route('staff.feedback.show', [$row->id]) }}"
                                                        class="">
                                                        {{ Str::limit($row->content, 50) }}
                                                    </a>
                                                </div>
                                            </td>


                                            <td>
                                                <div class="row" style="margin-left: 30%;">
                                                    @if ($row->is_active == 1)
                                                        <p class="font-italic text-success">Đã xem</p>
                                                    @else
                                                        <p class="text-warning">Chưa xem</p>
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                @if ($row->user_return == null)
                                                    <p class="font-italic text-warning">Chưa phản hồi</p>
                                                @else
                                                    <p class="font-italic text-success">Đã phản hồi</p>
                                                @endif
                                            </td>
                                            <td class="font-italic  text-center">
                                                {{ date('d/m/Y H:i:s', strtotime($row->created_at)) }}
                                                <br>
                                            </td>
                                            <td class="text-center">
                                                <a href="">
                                                    {{ $row->user->name }}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('css')
    <!-- third party css -->
    <link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')

    <!-- Required datatable js -->
    <script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
    <script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
    <script src="assets/libs/jszip/jszip.min.js"></script>
    <script src="assets/libs/pdfmake/pdfmake.min.js"></script>
    <script src="assets/libs/pdfmake/vfs_fonts.js"></script>
    <script src="assets/libs/datatables/buttons.html5.min.js"></script>
    <script src="assets/libs/datatables/buttons.print.min.js"></script> b
    <!-- Responsive examples -->
    <script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
    <script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#datatable1").DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)")
        });
        $(document).ready(function() {
            $("#datatable2").DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)")
        });
        $(document).ready(function() {
            $("#datatable3").DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable3_wrapper .col-md-6:eq(0)")
        });
    </script>
@endpush
