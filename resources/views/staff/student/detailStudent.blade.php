@extends('layouts.dashboard')
@section('page-title', 'Hồ sơ sinh viên | Quản lý ')
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{route('staff.student.index')}}">Danh sách</a></li>
                    <li class="breadcrumb-item active">Hồ sơ sinh viên</li>
                </ol>
            </div>
            <h4 class="page-title">Hồ sơ sinh viên</h4>
        </div>
    </div>
</div>
<div class="nav-tabs-navigation">
    <div class="nav-tabs-wrapper">
        <ul class="nav nav-tabs border-0" data-tabs="tabs">
            <li class="nav-item">
                <a class="nav-link border-0  active" href="#tab1" data-toggle="tab">
                    Thông tin cơ bản
                </a>
            </li>
            @if(!empty($user->reportDetail))
            <li class="nav-item">
                <a class="nav-link border-0 " href="#tab2" data-toggle="tab">
                    Báo cáo thực tập
                </a>
            </li>
            @endif
            @if(!empty($user->reportDetail->serviceCancel) || !empty($user->serviceExemption))
            <li class="nav-item">
                <a class="nav-link border-0 " href="#tab3" data-toggle="tab">
                    Đăng ký dịch vụ
                </a>
            </li>
            @endif
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="tab-content p-0">
            <div class="tab-pane active" id="tab1">
                <div class="card">
                    <div class="card-header card-header-icon card-header-primary">
                        <h4 class="card-title">Hồ sơ cá nhân <a href="" class="ml-auto"></a></h4>
                    </div>
                    <div class="card-body">
                        <form autocomplete="off">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Họ và tên:</label>
                                        <input type="text" class="form-control" name="full_name" value="{{$user->profile->full_name}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Họ:</label>
                                        <input type="text" class="form-control" name="first_name" value="{{$user->profile->first_name}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Tên:</label>
                                        <input type="text" class="form-control" name="last_name" value="{{$user->profile->last_name}}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Email:</label>
                                        <input type="name" class="form-control" name="name" disabled value="{{$user->email}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Email cá nhân:</label>
                                        <input type="name" class="form-control" name="name" disabled value="{{$user->profile->email_personal}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Ngày sinh:</label>
                                        <input type="name" class="form-control" name="name" disabled value="{{date('d-m-Y',strtotime($user->profile->birthday)) }}">
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Số điện thoại:</label>
                                        <input type="text" class="form-control" name="phone" value="{{$user->profile->phone}}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">CMND / CCCD:</label>
                                        <input type="text" class="form-control" name="indo" value="{{$user->profile->indo}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Giới tính:</label>
                                        <input type="name" class="form-control" name="name" disabled value="{{$user->profile->gender ? 'Nam' : 'Nữ' }}">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Địa chỉ:</label>
                                        <input type="text" class="form-control" name="address" value="{{ $user->profile->address }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2 mb-4">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="{{ $user->profile->city_name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="{{ $user->profile->district_name }}" disabled>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="{{ $user->profile->ward_name }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab2">
                @if(!empty($user->reportDetail))
                <div class="card">
                    <div class="card-header card-header-icon card-header-primary">
                        <h4 class="card-title">Danh sách báo cáo thực tập</h4>
                    </div>
                    <div class="card-body">
                        @livewire('report-detail',['idUser'=> $user->id])
                    </div>
                </div>
                @endif
            </div>
            <div class="tab-pane" id="tab3">
                @if(!empty($user->reportDetail->serviceCancel) || !empty($user->serviceExemption))
                <div class="card">
                    <div class="card-header card-header-icon card-header-primary">
                        <h4 class="card-title">Danh sách sinh viên đăng ký dịch vụ</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="datatable1" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class='text-primary'>
                                    <tr>
                                        <th class="text-center">Họ và Tên</th>
                                        <th class="text-center">Loại dịch vụ</th>
                                        <th class="text-center">Thời gian đăng ký</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($service as $row)
                                    <tr>
                                        <td class="text-center">{{$row->user->name}}</td>
                                        <td class="text-center">
                                            @if($row->type == SERVICE_TYPE_EXEMPTION)
                                                <span class="text-primary">Miễn giảm thực tập</span>
                                            @else
                                                <span class="text-success">Huỷ thực tập</span>
                                            @endif
                                        </td>
                                        <td class="text-center">{{$row->created_at}}</td>
                                        <td class="text-center">
                                            @if($row->status == SERVICE_STATUS_PENDING) <span class="text-warning">Chờ xác nhận</span>
                                            @elseif($row->status ==SERVICE_STATUS_APPROVED) <span class="text-success">Đăng ký thành công</span>
                                            @elseif($row->status == SERVICE_STATUS_DENINED) <span class="text-danger">Đăng ký thất bại</span>
                                            @elseif($row->status == SERVICE_STATUS_CANCEL) <span class="text-danger">Huỷ đăng ký</span>
                                            @else <span class="text-danger">Đăng ký nhưng </br> chưa nộp hồ sơ</span>
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            @if($row->status == SERVICE_STATUS_WAITING)
                                            @else
                                            <a href="{{route('staff.student.service',$row->id)}}"  class="btn btn-info" title="Xem thông tin chi tiết báo cáo sinh viên">
                                                <i class="far fa-address-card"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><i class="fas fa-image mr-1"></i> Hình Ảnh</h4>
            </div>
            <div class="card-body">
                <h4 class="header-title m-t-0 m-b-30">Ảnh Đại Diện</h4>
                <input type="file" class="dropify" disabled />
                <h6 class="mt-3 text-primary"><i class=" fas fa-user-graduate"> </i> {{$user->role->name}} </h6>
                @if ($user->is_logged)
                <h6 class="text-success"><i class="fas fa-dot-circle"> </i> <span>Đang hoạt động</span></h6>
                @elseif (!$user->is_active)
                <h6 class="text-danger"><i class="fas fa-dot-circle"> </i> <span>Ngừng hoạt động</span></h6>
                @else
                <h6 class="text-danger"><i class="fas fa-dot-circle"> </i> <span>Chưa tham gia hệ thống</span></h6>
                @endif
                <h3 class="font-weight-bold"> {{$user->name}} </h3>
                <p class="text-primary">Email: {{$user->email}}</p>
                <p class="card-description mt-3 font-italic">
                    {{!empty($user->profile->descricption) ? $user->profile->descricption : 'Chưa có thông tin tiểu sử' }}
                </p>
                <table class="table mt-3">
                    <tbody>
                        <tr>
                            <th>Hiện tại:</th>
                            <th>Kỳ {{$user->student->current_semester }}</th>
                        </tr>
                        <tr>
                            <th>Ngành học:</th>
                            <th>{{$user->student->career->name }}</th>
                        </tr>
                        @if($user->report)
                        <tr>
                            <th>Ngày đăng ký:</th>
                            <td>
                                {{date('d-m-Y',strtotime($user->report->created_at))}}
                            </td>
                        </tr>
                        <tr>
                            <th>Loại đăng ký:</th>
                            <td>
                                @if($user->report->is_outside) <span class="text-info">Đăng ký bên ngoài</span> @else
                                <span class="text-pink">Đăng ký trên hệ thống</span> @endif
                            </td>
                        </tr>
                        @endif
                        <tr>
                            <th>Trạng thái:</th>
                            <td>
                                @if($user->student->study_status == 1)
                                <span class="green">HDI ( Học đi )</span>
                                @else
                                <span class="text-danger">BLUU ( Bảo lưu )</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Thực tập:</th>
                            <th>@if($user->student->is_accept == 1) Chưa có nơi thực tập @else Đã có nơi thực tập @endif</th>
                        </tr>
                        <tr>
                            <th>CV hiển thị:</th>
                            @if(!empty($cv))
                            <td><a href="{{$cv['view']}}" target="_blank">{{$cv['name']}}</a></td>
                            @else
                            <td>Chưa có</td>
                            @endif
                        </tr>
                        @if($user->student->is_accept == 0)
                        <tr>
                            <th>Doanh Nghiệp:</th>
                            <td>
                                @if($user->company_id)
                                {{$user->company->name}}
                                @else
                                Chưa có
                                @endif
                            </td>
                        </tr>
                        @if($user->report)
                        <tr>
                            <th>Báo cáo kỳ:</th>
                            <td>
                                {{$user->report->semester->name}}
                            </td>
                        </tr>
                        @endif
                        <tr>
                            <th>Báo cáo thực tập</th>
                            @if($user->report)
                            <td>
                                @if($user->report->status == REPORT_PENDING)
                                Đang viết báo cáo
                                @elseif($user->report->status == REPORT_CANCEL)
                                Huỷ thực tập
                                @elseif($user->report->status == REPORT_PASS)
                                Đậu thực tập
                                @elseif($user->report->status == REPORT_FAIL)
                                Rớt thực tập
                                @elseif($user->report->status == REPORT_DONE)
                                Đã hoàn thành báo cáo
                                @else
                                Doanh nghiệp đã đánh giá
                                @endif
                            </td>
                            @else
                            <td>Chưa có báo cáo</td>
                            @endif
                        </tr>
                        <tr>
                            <th>Kết quả thực tập</th>
                            @if($user->student == NULL)
                            <td>Chưa có kết quả</td>
                            @else
                            <td>
                                @if($user->student->status == STUDENT_STATUS_PASS)
                                Thực tập thành công
                                @elseif($user->student->status == STUDENT_STATUS_FAIL)
                                Thực tập thất bại
                                @else
                                Chưa đánh giá
                                @endif
                            </td>
                            @endif
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<!-- form Uploads -->
<link href="assets/libs/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
<!-- Script You Need -->
<!-- file uploads js -->
<script src="assets/libs/fileuploads/js/dropify.min.js"></script>
<script type="text/javascript">
    $('.dropify').dropify({
        defaultFile: "assets/images/1.png",
    });
    let urlImage = `{{$user->getAvatar()}}`
    if (urlImage) $(".dropify-render img").attr("src", urlImage);
</script>
@endpush
