@extends('layouts.dashboard')
@section('page-title', 'Quản Lý sinh viên | Chỉnh sửa thông tin')
@section('title', 'Quản Lý sinh viên')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('staff.student.index') }}">Danh Sách</a></li>
                    <li class="breadcrumb-item active">Chỉnh sửa sinh viên</li>
                </ol>
            </div>
            <h4 class="page-title">Chỉnh sửa sinh viên</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="card ">
            <div class="card-header">
                <h3 class="header-title">Cập Nhật Thông Tin Sinh Viên</h3>
            </div>
            <div class="card-body">
                <form method="POST" onsubmit="getLoading()" action="{{ route('staff.student.update', [$user->id]) }}">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <label class="col-md-2 col-form-label">Họ Và Tên</label>
                        <div class="col-md-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                                @error('name')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">MSSV</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="code" value="{{ $user->code }}">
                                @error('code')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Học Kỳ Hiện Tại</label>
                        <div class="col-sm-10">
                            <div class="form-group ">
                                <select name="semester" value="{{ $user->student->current_semester }}" class="form-control">
                                    <option value="0" selected>Vui lòng chọn kỳ học</option>
                                    @for ($i = 1; $i <= 11; $i++) @if ($i==$user->student->current_semester)
                                        <option value="{{ $i }}" selected>Kỳ {{ $i }}
                                        </option>
                                        @else
                                        <option value="{{ $i }}">Kỳ {{ $i }}</option>
                                        @endif
                                        @endfor
                                </select>
                                @error('semester')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-2 col-form-label">Ngành học</label>
                        <div class="col-md-10">
                            <select name="career_id" value="{{ $user->student->career_id }}" type="text" class="form-control">
                                <option value="" selected>Vui lòng chọn ngành nghề</option>
                                @foreach ($careers as $career)
                                @if ($career->id == $user->student->career_id)
                                <option value="{{ $career->id }}" selected>{{ $career->name }}</option>
                                @else
                                <option value="{{ $career->id }}">{{ $career->name }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <label class="col-sm-2 col-form-label">Email </label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                @if ($user->is_logged == 1)
                                <input type="email" class="form-control" name="email" value="{{ $user->email }}" placeholder="emailEduFPT@fpt.edu.vn" readonly>
                                @elseif($user->is_logged == 0)
                                <input type="email" class="form-control" name="email" value="{{ $user->email }}" placeholder="emailEduFPT@fpt.edu.vn">
                                @endif
                                @error('email')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <label class="col-sm-2 col-form-label">Email Cá Nhân</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email_personal" placeholder="Email Cá Nhân" value="{{ $user->profile->email_personal }}">
                                @error('email_personal')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-4 col-form-label">Ngày Sinh</label>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="date" class="form-control" id="birthday" name="birthday" value="{{ date('Y-m-d', strtotime($user->profile->birthday)) }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row ">
                                <label class="col-sm-4 col-form-label">Giới Tính</label>
                                <div class="col-sm-8 checkbox-radios">
                                    <div class="form-check form-check-inline mt-2 ml-3">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" value="{{ MAN }}" id="gender" name="gender" @if (!$user->profile->gender) checked @endif> Nam
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" value="{{ WOMEN }}" id="gender" name="gender" @if ($user->profile->gender) checked @endif> Nữ
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-sm-4 col-form-label">CMND</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="indo" value="{{ $user->profile->indo }}">
                                        @error('indo')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-sm-4 col-form-label">Số Điện Thoại</label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="phone" value="{{ $user->profile->phone }}">
                                        @error('phone')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-sm-5">
                        <label class="col-sm-2 col-form-label">Địa Chỉ</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="address" value="{{ $user->profile->address }}">
                                @error('address')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    @livewire('change-address-custom', ['cityId' => $user->profile->city_id,'districtId' =>
                    $user->profile->district_id,'wardId' => $user->profile->ward_id])
                    <div class="row mt-5">
                        <div class="col-2"></div>
                        <div class="col-10">
                            <button class="btn btn-outline-warning h6 mr-3" type="submit">Hủy Thay Đổi
                            </button>
                            <button class="btn btn-primary  h6" type="submit">Cập Nhật</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card ">
            <div class="card-header text-center">
                <h3 class="header-title">Hồ Sơ Sinh Viên</h3>
            </div>
            <div class="card-body table-responsive">
                <div class="text-center">
                    <input type="file" class="dropify" data-default-file="assets/images/default-avatar.png" />

                    <div class="mt-5">
                        <h3 class="font-weight-bold">{{ $user->name }}</h3>
                        @if ($user->logged_at)
                        <span class="text-success font-weight-bold"><i class="fa fa-dot-circle-o "></i> Đang hoạt
                            động</span>
                        <br>
                        <span class="text-gray font-italic">Đăng nhập gần nhất:
                            {{ date('d-m-Y H:i:s', strtotime($user->logged_at)) }}</span>
                        @elseif(!$user->is_active)
                        <span><i class="fa fa-dot-circle-o text-warning font-weight-bold"></i> Ngừng hoạt
                            động</span>
                        <span class="text-gray font-italic">Lần cuối đăng nhập: {{ $user->logged_at }}</span>
                        @else
                        <span><i class="fa fa-dot-circle-o text-danger font-weight-bold"></i> Không hoạt
                            động</span>
                        @endif
                    </div>
                </div>
                <table class="table mt-3">
                    <tbody>
                        <tr>
                            <th>Thực tập:</th>
                            <th>@if($user->student->is_accept == 1) Chưa có nơi thực tập @else Đã có nơi thực tập  @endif</th>
                        </tr>
                        <tr>
                            <th>CV hiển thị:</th>
                            @if(!empty($cv))
                                <td><a href="{{$cv['view']}}" target="_blank">{{$cv['name']}}</a></td>
                            @else
                                <td>Chưa có</td>
                            @endif
                        </tr>
                        @if($user->student->is_accept == 0)
                        <tr>
                            <th>Doanh Nghiệp:</th>
                            <td>
                                @if($user->company_id)
                                {{$user->company->name}}
                                @else
                                Chưa có
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Báo cáo thực tập</th>
                            @if($user->report)
                            <td>
                                @if($user->report->status == REPORT_PENDING)
                                Đang viết báo cáo
                                @elseif($user->report->status == REPORT_CANCEL)
                                Huỷ thực tập
                                @elseif($user->report->status == REPORT_PASS)
                                Đậu thực tập
                                @elseif($user->report->status == REPORT_FAIL)
                                Rớt thực tập
                                @elseif($user->report->status == REPORT_DONE)
                                Đã hoàn thành báo cáo
                                @else
                                Doanh nghiệp đã đánh giá
                                @endif
                            </td>
                            @else
                            <td>Chưa có báo cáo</td>
                            @endif
                        </tr>
                        <tr>
                            <th>Kết quả thực tập</th>
                            @if($user->student == NULL)
                            <td>Chưa có kết quả</td>
                            @else
                            <td>
                                @if($user->student->status == STUDENT_STATUS_PASS)
                                Thực tập thành công
                                @elseif($user->student->status == STUDENT_STATUS_FAIL)
                                Thực tập thất bại
                                @else
                                Chưa đánh giá
                                @endif
                            </td>
                            @endif
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
@endsection

@push('css')
<link href="assets/libs/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
<!-- Script You Need -->
<livewire:scripts />
<!-- file uploads js -->
<script src="assets/libs/fileuploads/js/dropify.min.js"></script>
<script type="text/javascript">
    $('.dropify').dropify({
        messages: {
            'default': 'Kéo và thả tệp vào đây hoặc nhấp vào',
            'replace': 'Kéo và thả hoặc nhấp để thay thế',
            'remove': 'Thay đổi',
            'error': 'Rất tiếc, đã xảy ra sự cố.'
        },
        error: {
            'fileSize': 'Kích thước tệp quá lớn (tối đa 1M).'
        }
    });
    let urlImage = `{{$user->getAvatar() ? $user->getAvatar() : 'assets/images/1.png'}}`
    if (urlImage) $(".dropify-render img").attr("src", urlImage);
</script>
@endpush
