@extends('layouts.dashboard')
@section('page-title', 'Sinh Viên | Import File')
@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{route('staff.student.index')}}">Danh Sách</a></li>
                        <li class="breadcrumb-item active">Import sinh viên</li>
                    </ol>
                </div>
                <h4 class="page-title">Import Excel - Sinh Viên</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <form method="POST" action="{{ route('staff.import.student') }}" enctype="multipart/form-data"
          class="form-horizontal" onsubmit="getLoading()">
        @csrf
        @method('POST')
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header card-header-icon card-header-primary">

                        <h4 class="card-title ">Thêm Sinh Viên - Import File</h4>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-6 ">
                                <label class="form-label" for="customFile">Chọn file sinh viên</label>
                                <div class="form-group mb-0">
                                    <p>File tải lên phải là tệp excel (.xlsx, .xls)</p>
                                    <input type="file" name="file" class="filestyle" data-text="Chọn file Import"
                                           data-btnClass="btn-primary">
                                </div>
                                @error('file')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <button type="submit" class="btn btn-danger">Bắt đầu Import File</button>
                    </div>
                </div>
                @if(Session::has('failures'))
                    <div class="card-box">
                        <table class="table table-hover table-bordered dt-responsive nowrap text-nowrap table-danger">
                            <thead>
                            <tr>
                                <th>DÒNG</th>
                                <th>STT</th>
                                <th>LỖI DỮ LIỆU</th>
                            </tr>
                            </thead>
                            @foreach (session()->get('failures') as  $validation)
                                <tbody>
                                <tr>
                                    <td>{{$validation['row']}}</td>
                                    <td>{{$validation['values'][0]}}</td>
                                    <td class="col">
                                        <ul>
                                            @foreach ($validation['errors'] as $e)
                                                <li>{{$e}}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>
                                </tbody>
                            @endforeach
                        </table>
                    </div>
                @endif
                @if(Session::has('headerError'))
                    <div class="card-box">
                        <h5 class="text-danger">Tệp excel phải có dạng</h5>
                        <table class="table table-striped table-hover table-bordered dt-responsive nowrap">
                            <tr class="table-danger">
                                @foreach(Session::get('headerError') as $value)
                                    <th>{{$value}}</th>
                                @endforeach
                            </tr>
                            <tr>
                                @foreach(Session::get('headerError') as $value)
                                    <td></td>
                                @endforeach
                            </tr>
                        </table>
                    </div>
                @endif
                <div class="card-box">
                    <h4 class="card-title d-sm-inline">Mẫu File - Import Sinh viên</h4>
                    <div class="float-right">
                        <a class="btn btn-primary" type="button" href="{{FILE_URL}}j8RapX01I"><i
                                class="fas fa-download mr-2"></i> Tải Mẫu</a>
                    </div>
                    <p class="card-text"> Bạn bắt buộc phải dựa theo mẫu để thêm sinh viên, các trường này là bắt buộc.
                        Dữ liệu nhiều hay ít dựa vào số dòng bạn đưa vào tối đa là 30.000 dòng.</p>
                    <img src="assets/images/imports/student-import.png" alt="" class="w-100">
                </div>
            </div>
        </div>
    </form>
@endsection

@push('script')
    <!-- Script You Need -->
    <script src="assets/libs/bootstrap-filestyle2/bootstrap-filestyle.min.js"></script>
@endpush
