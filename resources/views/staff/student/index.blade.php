@extends('layouts.dashboard')
@section('page-title', 'Quản Lý Sinh Viên | Danh sách')
@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item active">Danh sách sinh viên</li>
                    </ol>
                </div>
                <h4 class="page-title">Danh sách sinh viên</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs border-0" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link border-0  active" href="#all" data-toggle="tab">
                                Tất Cả
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link border-0 " href="#reports" data-toggle="tab">
                                Đã & Đang Thực Tập
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link border-0 " href="#new" data-toggle="tab">
                                Mới Tạo
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link border-0 " href="#not-active" data-toggle="tab">
                                Không Hoạt Động
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-box">
                <div class="tab-content">
                    <div class="tab-pane active" id="all">
                        <div class="table-responsive">
                            <h4 class="header-title mt-1"><b>Danh sách sinh viên</b></h4>
                            <p class="sub-header">
                                Bao gồm tất cả sinh viên đang hoạt động trên hệ thống.
                            </p>
                            <table id="datatable1" class="table table-striped table-bordered dt-responsive nowrap"
                                   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class='text-primary'>
                                <tr>
                                    <th>#</th>
                                    <th>MSSV</th>
                                    <th class="col">Họ Tên</th>
                                    <th>Email</th>
                                    <th>Kỳ</th>
                                    <th>Trạng thái</th>
                                    <th>Ngày khởi tạo</th>
                                    <th></th>
                                </tr>
                                </thead>


                                <tbody>
                                @foreach($students as $key =>$row)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$row->code}}</td>
                                        <td class="col">{{$row->name}}</td>
                                        <td>{{$row->email}}</td>
                                        <td> {{($row->student->current_semester)}} </td>
                                        <td>
                                            @if ($row->is_logged)
                                                <span class="text-success">Đang hoạt động</span>
                                            @elseif (!$row->is_active)
                                                <span class="text-danger">Ngừng hoạt động</span>
                                            @else
                                                <span class="text-warning">Chưa hoạt động</span>
                                            @endif
                                        </td>
                                        <td>{{date('d/m/Y',strtotime($row->created_at))}}</td>
                                        <td class="">
                                            <a href="{{route('staff.student.show',$row->id)}}"
                                               class="btn btn-outline-info mr-1"
                                               data-toggle="tooltip" title="Xem thông tin chi tiết của sinh viên">
                                                <i class="far fa-address-card"></i>
                                            </a>
                                            <a href="{{ route('staff.student.edit',$row->id) }}"
                                               class="btn btn-primary mr-1"
                                               data-toggle="tooltip" title="Sửa thông tin sinh viên">
                                                <i class="far fa-edit"></i></a>
                                            @if(!$row->logged_at && $row->is_active)
                                                <a href="{{ route('staff.student.send',$row->id) }}"
                                                   class="btn btn-twitter mr-1"
                                                   onclick="return confirm('Bạn Có muốn gửi email???');"
                                                   data-toggle="tooltip" title="Yêu cầu sinh viên đăng nhập lần đầu!">
                                                    <i class="fab fa-telegram-plane"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="tab-pane" id="new">
                        <div class="table-responsive">
                            <h4 class="header-title mt-1"><b>Mới tạo</b></h4>
                            <p class="sub-header">
                                Danh sách sinh viên vừa tạo mới.
                            </p>
                            <table id="datatable2" class="table table-striped table-bordered dt-responsive nowrap"
                                   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="text-primary">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>MSSV</th>
                                    <th>Họ Tên</th>
                                    <th>Email</th>
                                    <th>Kỳ</th>
                                    <th>Ngày khởi tạo</th>
                                    <th class="text-center">Chức năng</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($newStudents as $key =>$row)
                                    <tr>
                                        <td class="text-center">{{$loop->iteration}}</td>
                                        <td>{{$row->code}}</td>
                                        <td class="col">{{$row->name}}</td>
                                        <td>{{$row->email}}</td>
                                        <td> {{($row->student->current_semester)}} </td>
                                        <td>{{date('d/m/Y',strtotime($row->created_at))}}</td>
                                        <td class="">
                                            <a href="{{route('staff.student.show',$row->id)}}"
                                               class="btn btn-outline-info mr-1"
                                               data-toggle="tooltip" title="Xem thông tin chi tiết của sinh viên">
                                                <i class="far fa-address-card"></i>
                                            </a>
                                            <a href="{{ route('staff.student.edit',$row->id) }}"
                                               class="btn btn-primary mr-1"
                                               data-toggle="tooltip" title="Sửa thông tin sinh viên">
                                                <i class="far fa-edit"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="not-active">
                        <div class="table-responsive">
                            <h4 class="header-title mt-1"><b>Không hoạt động</b></h4>
                            <p class="sub-header">
                                Sinh viên không hoạt động trong kỳ thực tập, kỳ hiện tại trở về trước.
                            </p>
                            <table id="datatable3" class="table table-striped table-bordered dt-responsive nowrap"
                                   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="text-primary">
                                <tr>
                                    <div class="row">
                                        <th class="text-center">#</th>
                                        <th class="">MSSV</th>
                                        <th class="">Họ Tên</th>
                                        <th class="">Email</th>
                                        <th class="">Kỳ</th>
                                        <th>Ngày khởi tạo</th>
                                        <th class=""></th>
                                    </div>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($studentsNoActive as $key =>$row)
                                    <tr class="text-danger">
                                        <div class="row">
                                            <td class="text-center">{{$loop->iteration}}</td>
                                            <td class="">{{$row->code}}</td>
                                            <td class="col">{{$row->name}}</td>
                                            <td class="">{{$row->email}}</td>
                                            <td> {{($row->student->current_semester)}} </td>
                                            <td>{{date('d/m/Y',strtotime($row->created_at))}}</td>
                                            <td class="">
                                                <a href="{{route('staff.student.show',$row->id)}}"
                                                   class="btn btn-outline-info mr-1"
                                                   data-toggle="tooltip" title="Xem thông tin chi tiết của sinh viên">
                                                    <i class="far fa-address-card"></i>
                                                </a>
                                                <a href="{{ route('staff.student.edit',$row->id) }}"
                                                   class="btn btn-primary mr-1"
                                                   data-toggle="tooltip" title="Sửa thông tin sinh viên">
                                                    <i class="far fa-edit"></i></a>
                                                <a href="{{ route('staff.student.send',$row->id) }}"
                                                   class="btn btn-twitter mr-1"
                                                   onclick="return confirm('Bạn Có muốn gửi email???');"
                                                   data-toggle="tooltip" title="Yêu cầu sinh viên đăng nhập lần đầu!">
                                                    <i class="fab fa-telegram-plane"></i></a>
                                            </td>
                                        </div>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="reports">
                        <div class="table-responsive">
                            <h4 class="header-title mt-1"><b>Sinh viên thực tập</b></h4>
                            <p class="sub-header">
                                Danh sách sinh viên đã và đang thực tập trong kỳ.
                            </p>
                            <table id="datatable4" class="table table-striped table-bordered dt-responsive nowrap"
                                   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="text-primary">
                                <tr>
                                    <div class="row">
                                        <th class="text-center">#</th>
                                        <th class="">MSSV</th>
                                        <th class="col">Họ Tên</th>
                                        <th class="">Email</th>
                                        <th class="col">Kỳ</th>
                                        <th class="">Trạng Thái</th>
                                        <th>Ngày khởi tạo</th>
                                        <th class=""></th>
                                    </div>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($studentInterns as $key =>$row)
                                    <tr class="">
                                        <div class="row">
                                            <td class="text-center">{{$loop->iteration}}</td>
                                            <td class="">{{$row->code}} </td>
                                            <td class="col">{{$row->name}} </td>
                                            <td class="">{{$row->email}}</td>
                                            <td> {{($row->student->current_semester)}} </td>
                                            <td class="">
                                                @switch(!empty($row->report) ? $row->report->status : null)
                                                    @case(REPORT_PENDING)
                                                    <span class="text-info">Đang Thực Tập</span>
                                                    @break
                                                    @case(REPORT_PASS)
                                                    <span class="text-success">Đạt Thực Tập</span>
                                                    @break
                                                    @case(REPORT_FAIL)
                                                    <span class="text-danger">Rớt Thực Tập</span>
                                                    @break
                                                    @case(REPORT_FINISHED)
                                                    <span class="text-primary">Đã Hoàn Thành Thực Tập</span>
                                                    @break
                                                    @case(REPORT_CANCEL)
                                                    <span class="text-warning">Đã Hủy Thực Tập</span>
                                                    @break
                                                    @case(REPORT_DONE)
                                                    <span class="text-purple">Đợi Đánh Giá Thực Tập</span>
                                                    @break
                                                    @default
                                                    @if ($row->student->status == STUDENT_STATUS_PASS)
                                                        <span class="text-purple">Miễn giảm thực tập</span>
                                                    @elseif($row->student->status == STUDENT_STATUS_FAIL)
                                                        <span class="text-danger">Rớt Thực Tập</span>
                                                    @else
                                                        <span class="text-info">Đang Thực Tập</span>
                                                    @endif
                                                @endswitch

                                            </td>
                                            <td>{{date('d/m/Y',strtotime($row->created_at))}}</td>

                                            <td class="td-actions d-block text-center">
                                                <a href="{{route('staff.student.show',$row->id)}}"
                                                   class="btn btn-outline-info mr-1"
                                                   data-toggle="tooltip" title="Xem thông tin chi tiết của sinh viên">
                                                    <i class="far fa-address-card"></i>
                                                </a>
                                                <a href="{{ route('staff.student.edit',$row->id) }}"
                                                   class="btn btn-primary mr-1"
                                                   data-toggle="tooltip" title="Sửa thông tin sinh viên">
                                                    <i class="far fa-edit"></i></a>
                                            </td>
                                        </div>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fe-box float-right"></i>
                <h5 class="text-muted text-uppercase mb-3 mt-0">Tổng số</h5>
                <h3 class="mb-3" data-plugin="counterup">{{count($students)}}</h3>
                <span class="text-muted vertical-middle">Tổng số sinh viên đang hoạt động trên hệ thống</span>
            </div>
        </div>

        <div class="col-md-6 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fe-layers float-right"></i>
                <h5 class="text-muted text-uppercase mb-3 mt-0">Hoàn thành thực tập</h5>
                <h3 class="mb-3"><span data-plugin="counterup">{{count($countPassStudents)}}</span></h3>
                <span class="text-muted ml-2 vertical-middle">Sinh viên hoàn thành thực trên tổng số</span>
            </div>
        </div>

        <div class="col-md-6 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fe-tag float-right"></i>
                <h5 class="text-muted text-uppercase mb-3 mt-0">Đang hoạt động</h5>
                <h3 class="mb-3"><span data-plugin="counterup">{{count($studentsActive)}}</span></h3>
                <span class="text-muted ml-2 vertical-middle">From previous period</span>
            </div>
        </div>

        <div class="col-md-6 col-xl-3">
            <div class="card-box tilebox-one">
                <i class="fe-briefcase float-right"></i>
                <h5 class="text-muted text-uppercase mb-3 mt-0">Không hoạt động</h5>
                <h3 class="mb-3" data-plugin="counterup">{{count($studentsNoActive)}}</h3>
                <span class="text-muted ml-2 vertical-middle">Sinh viên chưa đăng nhập lần nào</span>
            </div>
        </div>
    </div>

@endsection
@push('css')
    <!-- third party css -->
    <link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css"/>
    <link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css"/>
    <link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css"/>
@endpush
@push('script')

    <!-- Required datatable js -->
    <script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
    <script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
    <script src="assets/libs/jszip/jszip.min.js"></script>
    <script src="assets/libs/pdfmake/pdfmake.min.js"></script>
    <script src="assets/libs/pdfmake/vfs_fonts.js"></script>
    <script src="assets/libs/datatables/buttons.html5.min.js"></script>
    <script src="assets/libs/datatables/buttons.print.min.js"></script> b
    <!-- Responsive examples -->
    <script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
    <script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#datatable1").DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)")
            $("#datatable2").DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)")
            $("#datatable3").DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable3_wrapper .col-md-6:eq(0)")
            $("#datatable4").DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable4_wrapper .col-md-6:eq(0)")
        });
    </script>
@endpush
