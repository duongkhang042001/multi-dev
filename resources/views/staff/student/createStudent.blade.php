@extends('layouts.dashboard')
@section('page-title', 'Quản Lý sinh viên | Thêm mới')
@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{route('staff.student.index')}}">Danh Sách</a></li>
                        <li class="breadcrumb-item active">Thêm sinh viên</li>
                    </ol>
                </div>
                <h4 class="page-title">Thêm sinh viên</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <form method="POST" onsubmit="getLoading()" action="{{route('staff.student.store')}}">
        @csrf
        @method('POST')
        <div class="row">
            <div class="col-md-8">
                <div class="card ">
                    <div class="card-header card-header-primary card-header-text">
                        <div class="card-text">
                            <h4 class="card-title"><i class="fas fa fa-address-card mr-1"></i> Thông tin sinh viên</h4>
                        </div>
                    </div>
                        <div class="card-body ">
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Họ Và Tên <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name"
                                               placeholder="Nguyễn Văn A" value="{{ old('name') }}">
                                        @error('name')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                </div>

                            </div>
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">MSSV <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                        <input type="text" class="form-control" name="code" placeholder="PS11322"
                                               value="{{ old('code') }}">
                                        @error('code')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                </div>

                            </div>
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Email <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                        <input type="text" class="form-control" name="email"
                                               placeholder="nguyenvana@fpt.edu.vn" value="{{ old('email') }}">
                                        @error('email')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                </div>

                            </div>
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Email Cá Nhân</label>
                                <div class="col-sm-10">
                                        <input type="text" class="form-control" name="email_personal"
                                               placeholder="nguyenvana@gmail.com"
                                               value="{{ old('email_personal') }}">
                                        @error('email_personal')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                </div>

                            </div>
                            <div class="row form-group mt-5">
                                <label class="col-sm-2 col-form-label">Số Điện Thoại <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                        <input type="text" class="form-control" name="phone"
                                               placeholder="0123456789" value="{{ old('phone') }}">
                                        @error('phone')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                </div>

                            </div>
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">CMND <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                        <input type="text" class="form-control" name="indo" placeholder="123456789"
                                               value="{{ old('indo') }}">
                                        @error('indo')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Ngày Sinh <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" id="birthday" name="birthday"
                                           value="{{old('birthday')}}">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label label-checkbox">Giới Tính <span class="text-danger">*</span></label>
                                <div class="col-sm-10 checkbox-radios">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" value="0" id="gender"
                                                   name="gender" checked> Nam
                                            <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" value="1" id="gender"
                                                   name="gender"> Nữ
                                            <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group mt-5">
                                <label class="col-sm-2 col-form-label">Học Kỳ <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <select name="semester" class="form-control" value="{{ old('semester') }}">
                                        <option value="0" selected>Vui lòng chọn kỳ học</option>
                                        @for($i=5;$i<=11;$i++) @if($i==old('semester'))
                                            <option value="{{$i}}" selected>Kỳ {{$i}}</option>
                                        @else
                                            <option value="{{$i}}">Kỳ {{$i}}</option>
                                        @endif
                                        @endfor
                                    </select>
                                    @error('semester')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            @livewire('change-group-career',['is_education'=>true , 'careerId' => old('career') , 'careerGroupId' => old('group_career')])
                            <div class="row form-group mt-5">
                                <label class="col-sm-2 col-form-label">Địa Chỉ <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                        <input type="text" class="form-control" name="address"
                                               placeholder="123 Nguyễn Phúc Chu" value="{{ old('address') }}">
                                        @error('address')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                </div>

                            </div>
                            @livewire('change-address-custom', ['cityId' => old('city'),'districtId' => old('district'),'wardId'=>old('ward')])
                            <div class="row form-group text-sm-right mt-5 pt-sm-5 d-flex align-items-center">
                                <div class="col-sm-9 checkbox-radios">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label text-primary">
                                            <input class="form-check-input" type="checkbox" value="1" id="account"
                                                   name="account" {{old('account') ? "checked" : null}}> Gửi Thông Tin Tài Khoản Cho Sinh Viên
                                            <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-primary btn-block h5" type="submit">Thêm</button>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" >
                    <div class="card-header">
                        <h4 class="card-title"><i class="fas fa-image mr-1"></i> Hình Ảnh</h4>
                    </div>

                    <div class="card-body">
                        <h4 class="header-title m-t-0 m-b-30">Ảnh Đại Diện</h4>
                        <input type="file" class="dropify" data-default-file="assets/images/1.png" />
                        <p class="font-italic mt-3"><span>Lưu ý: </span> Đây là hình ảnh mặc định của hệ thống, bạn có thể cập nhật hình ảnh cho sinh viên tại đây.</p>
                    </div>
                </div>
            </div>
        </div>
    </form>


@endsection

@push('css')
    <!-- form Uploads -->
    <link href="assets/libs/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endpush

@push('script')
    <!-- file uploads js -->
    <script src="assets/libs/fileuploads/js/dropify.min.js"></script>
    <script type="text/javascript">
        $('.dropify').dropify({
            messages: {
                'default': 'Kéo và thả tệp vào đây hoặc nhấp vào',
                'replace': 'Kéo và thả hoặc nhấp để thay thế',
                'remove': 'Thay đổi',
                'error': 'Rất tiếc, đã xảy ra sự cố.'
            },
            error: {
                'fileSize': 'Kích thước tệp quá lớn (tối đa 1M).'
            }
        });
    </script>
@endpush
