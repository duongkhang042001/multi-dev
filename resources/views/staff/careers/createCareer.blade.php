@extends('layouts.dashboard')
@section('page-title', 'Quản lí nhóm ngành | Tạo mới')
@section('title', 'Quản lí sinh viên')
@section('content')
<div class="col-md-12">
    <div class="card ">
        <div class="card-header card-header-rose card-header-text">
            <div class="card-text">
                <h4 class="card-title">Thêm Nhóm Ngành</h4>
            </div>
        </div>
        <div class="card-body ">
            <form method="POST" onsubmit="getLoading()" action="{{ route('staff.careers.store') }}">
                @csrf
                @method('POST')
                <div class="row">
                    <label class="col-sm-2 col-form-label">Tên Nhóm Ngành :</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @error('name')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror

                        </div>
                    </div>
                </div>

                <div class=" row">
                    <label class="col-sm-2 col-form-label">Nhóm Ngành</label>
                    <div class="col-sm-4">
                        <select name="career_group_id" type="text" class="form-control">
                            <option selected>Vui lòng chọn nhóm ngành</option>
                            @foreach ($careerGroups as $careerGroup)
                            <option value="{{ $careerGroup['id'] }}">{{ $careerGroup['name'] }}</option>
                            @endforeach
                        </select>
                        @error('career_group_id')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>

                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label label-checkbox"></label>
                    <div class="col-sm-4 col-sm-offset-1 checkbox-radios">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="is_education" checked> Ngành Của
                                Trường
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>


                <button class="btn btn-rose" type="submit">Thêm</button>
            </form>
        </div>
        @endsection

        @section('script')

        @endsection