@extends('layouts.dashboard')
@section('page-title', 'Quản lí nhóm ngành | Tạo mới')
@section('title', 'Quản lí sinh viên')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('staff.careers.index') }}">Quản lý ngành</a></li>
                        <li class="breadcrumb-item active">Chỉnh nghành</li>
                    </ol>
                </div>
                <h4 class="page-title">Chỉnh nghành</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <form method="POST" onsubmit="getLoading()" action="{{ route('staff.careers.update', $career->id) }}">
                    @csrf
                    @method('put')
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Tên Ngành :</label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" value="{{ $career->name }}">
                                @error('name')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Nhóm Ngành</label>
                        <div class="col-sm-4">
                            <select name="career_group_id" type="text" class="form-control">
                                <option>Vui lòng chọn nhóm ngành</option>
                                @foreach ($careerGroups as $careerGroup)
                                    <option value="{{ $careerGroup->id }}" @if ($careerGroup->id == $career->career_group_id) selected @endif>{{ $careerGroup->name }}
                                    </option>
                                @endforeach
                            </select>
                            @error('career_group_id')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <label class="col-sm-2 col-form-label label-checkbox"></label>
                        <div class="col-sm-4 col-sm-offset-1 checkbox-radios">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="is_education"
                                        @if ($career->is_education == 1) value="1" checked @endif> Ngành Của Trường
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-info" type="submit">Sửa</button>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')

@endsection
