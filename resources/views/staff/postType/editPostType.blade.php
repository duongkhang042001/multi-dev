@extends('layouts.dashboard')
@section('page-title', 'Quản lý Loại Bài Viết | Tạo mới')
@section('title', 'Chỉnh Loại Bài Viết')
@section('content')
@if(session()->has('status'))
<div class="col-sm-6 ml-3 alert alert-success">
    {{ session()->get('status') }}
</div>
@endif
<div class="content">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-icon card-header-rose">
                            <div class="card-icon">
                                <i class="material-icons">assignment</i>
                            </div>
                            <h4 class="card-title ">Chỉnh Loại Bài Viết</h4>

                        </div>
                        <div class="card-body ">
                            <form method="POST" action="{{route('staff.post-type.update',[$postType->id])}}" class="form-horizontal">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Tên Loại Bài Viết</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" value="{{$postType->name}}">
                                            @error('name')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Đường dẫn</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="slug" value="{{$postType->slug}}">
                                            @error('slug')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Mô Tả Loại Bài Viết</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <textarea class="form-control" id="description" name="description" rows="10">{{$postType->description}}</textarea>
                                            <script>
                                                CKEDITOR.replace('description');
                                            </script>
                                            @error('description')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer ml-auto mr-auto">
                                    <button type="submit" class="btn btn-rose">Chỉnh Loại Bài Viết</button>
                                </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    CKEDITOR.replace('description', {
        width: "700px",
        height: "400px",
        filebrowserUploadMethod: "form",
    });
</script>
@endsection