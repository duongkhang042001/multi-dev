@extends('layouts.dashboard')
@section('page-title', 'Quản lý Loại Bài Viết | Tổng quan')
@section('title', 'Danh Sách Loại Bài Viết')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách loại bài viết</li>
                </ol>
            </div>
            <h4 class="page-title">Danh sách loại bài viết</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
                <table id="datatable2" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                    <thead class="text-primary">
                        <tr>
                            <th>#</th>
                            <th>Tên Loại Bài Viết</th>
                            <th>Mô Tả</th>
                            <th class="text-center">Trạng Thái</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($postType as $key => $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->name }}</td>
                            <td>
                                <span data-toggle="popover" data-content="{{$row->description}}">
                                    {{Str::limit($row->description, 50)}}
                                </span>
                            </td>

                            <td class="text-center">
                                @if($row->is_active == 1)
                                <i class="fas fa-check-circle text-success"></i>
                                @else
                                <div class="togglebutton">
                                    <label>
                                        <input type="checkbox">
                                        <span class="toggle"></span>
                                    </label>
                                </div>
                                @endif
                            </td>
                            @if($row->slug == POST_TYPE_NORMAL_SLUG || $row->slug == POST_TYPE_FAQ_SLUG
                            || $row->slug == POST_TYPE_COMPANY_SLUG || $row->slug == POST_TYPE_RECRUITMENT_SLUG)
                            @else
                            <td class="td-actions text-center">
                                <a href="{{ route('staff.post-type.edit', $row->id) }}" rel="tooltip" class="btn btn-sm btn-success">

                                    <i class="material-icons">edit</i>
                                </a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
@endsection

@push('css')
    <!-- third party css -->
    <link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')

    <!-- Required datatable js -->
    <script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
    <script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
    <script src="assets/libs/jszip/jszip.min.js"></script>
    <script src="assets/libs/pdfmake/pdfmake.min.js"></script>
    <script src="assets/libs/pdfmake/vfs_fonts.js"></script>
    <script src="assets/libs/datatables/buttons.html5.min.js"></script>
    <script src="assets/libs/datatables/buttons.print.min.js"></script> b
    <!-- Responsive examples -->
    <script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
    <script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
             $("#datatable1").DataTable({
                 'aoColumnDefs': [{
                     'bSortable': false,
                     'aTargets': [-1] /* 1st one, start by the right */
                 }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)")
            $("#datatable2").DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)")
            $("#datatable3").DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable3_wrapper .col-md-6:eq(0)")
            $("#datatable4").DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                lengthChange: !1,
                buttons: ["copy", "excel"]
            }).buttons().container().appendTo("#datatable4_wrapper .col-md-6:eq(0)")
        });
    </script>
@endpush
