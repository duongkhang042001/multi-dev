@extends('layouts.dashboard')
@section('page-title', 'Quản lý Bài Viết | Tổng quan')
@section('title', 'Chi Tiết Loại Bài viết')
@section('content')
    @if ($errors->any())
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    @if (session()->has('status'))
        <div class="col-sm-6 ml-3 alert alert-success">
            {{ session()->get('status') }}
        </div>
    @endif
    <div class="content">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card ">
                            <div class="card-header ">
                                <div class="row">
                                    <div class="col-9">
                                        <h4 class="card-title text-center text-danger font-weight-bold">
                                            {{ $postType->name }}
                                        </h4>
                                        <h5 class="font-italic text-center"> Đường dẫn: {{ $postType->slug }} </h5>
                                    </div>
                                    <div class="col-3">
                                        @if ($postType->is_active == 1)
                                            <button rel="tooltip" class="btn btn-success">
                                                <i class="material-icons">check</i>Đang hoạt động
                                            @else
                                                <button rel="tooltip" class="btn btn-success">
                                                    <i class="material-icons">close</i>Không hoạt động
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4 class="font-weight-bold">
                                            Mô tả bài viết
                                        </h4>
                                    </div>
                                    <div class="col-lg-10">

                                        {!! $postType->description !!}
                                        <p class="font-italic mb-0">Người đăng:{{ $postType->user->name }}</p>
                                        <p class="font-italic">Ngày
                                            đăng:{{ date('d/m/Y H:i:s', strtotime($postType->created_at)) }}</p>

                                        <form action="{{ route('staff.post-type.destroy', $postType->id) }}"
                                            method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button rel="tooltip" class="btn btn-outline-secondary"
                                                onclick="return confirm('Bạn Có muốn xóa???');">
                                                <i class="material-icons">close</i>Xóa Bài Viết
                                        </form>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- Script You Need -->
@endsection
