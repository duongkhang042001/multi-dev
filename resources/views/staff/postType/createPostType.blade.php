@extends('layouts.dashboard')
@section('page-title', 'Quản lý Loại Bài Viết | Tạo mới')
@section('title', 'Thêm Loại Bài Viết')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{route('staff.post-type.index')}}">Danh Sách Loại bài viết</a></li>
                    <li class="breadcrumb-item active">Tạo loại bài viết</li>
                </ol>
            </div>
            <h4 class="page-title">Tạo loại bài viết</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <form method="POST" action="{{route('staff.post-type.store')}}" class="form-horizontal">
                @csrf
                @method('POST')
                <div class="row">
                    <label class="col-sm-2 col-form-label">Tên Loại Bài Viết</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @error('name')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Đường Dẫn</label>
                    <div class="col-sm-10">
                        <div class="form-group has-muted bmd-form-group">
                            <label for="exampleInput3" class="bmd-label-floating">https://fptinternship.vn/</label>
                            <input type="text" class="form-control" id="exampleInput3" name="slug" value="{{old('slug')}}">
                            <span class="form-control-feedback">
                                <i class="material-icons">clear</i>
                            </span>
                            @error('slug')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Mô Tả Bài Viết</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="description" value="{{ old('description') }}">
                            @error('description')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer ml-auto mr-auto">
                    <button type="submit" class="btn btn-rose">Tạo Loại Bài Viết</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection