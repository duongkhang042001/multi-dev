@extends('layouts.dashboard')
@section('page-title', 'Quản Lý doanh nghiệp | Tổng quan')
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách báo cáo thực tập</li>
                </ol>
            </div>
            <h4 class="page-title">Danh sách báo cáo thực tập</h4>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs border-0" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link border-0  active" href="#all" data-toggle="tab">
                            Tất Cả
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 " href="#new" data-toggle="tab">
                            Chờ xử lý
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-box">
            <div class="tab-content">
                <div class="tab-pane active" id="all">
                    @php $penCom = \App\Models\Report::where('status',REPORT_FINISHED)->where('is_active',1)->count(); @endphp
                    <ul class="nav nav-pills navtab-bg nav-justified">
                        <li class="nav-item">
                            <a href="#home1" data-toggle="tab" aria-expanded="true" class="nav-link @if (!$penCom) active @endif">
                                Tất cả báo cáo
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#profile1" data-toggle="tab" aria-expanded="false" class="nav-link @if ($penCom) active @endif">
                                Chờ xử lý   @if ($penCom) <span class="badge badge-warning badge-pill float-right">{{ $penCom }}</span> @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#messages1" data-toggle="tab" aria-expanded="false" class="nav-link">
                                Đạt báo cáo
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#settings1" data-toggle="tab" aria-expanded="false" class="nav-link">
                                Không đạt báo cáo
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane @if (!$penCom) active @endif" id="home1">
                            <div class="table-responsive">
                                <table id="datatable1" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class='text-primary'>
                                        <tr>
                                            <th class="text-center">Mã số sinh viên</th>
                                            <th class="text-center">Họ và Tên</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">Trạng thái</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($report as $row)
                                        <tr>
                                            <td class="text-center">{{$row->user->code}}</td>
                                            <td class="text-center">{{$row->user->name}}</td>
                                            <td class="text-center">{{$row->user->email}}</td>
                                            <td class="text-center">
                                                @if($row->status == REPORT_PENDING) <span class="text-info">Đang thực tập</span>
                                                @elseif($row->status == REPORT_FINISHED) <span class="text-warning">Chờ xử lý</span>
                                                @elseif($row->status ==REPORT_DONE) <span class="text-pink">Đang hoàn tất</span>
                                                @elseif($row->status == REPORT_PASS) <span  class="text-success">Đạt </span>
                                                @elseif($row->status == REPORT_FAIL) <span class="text-danger">Không đạt</span>
                                                @endif

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane @if ($penCom) active @endif" id="profile1">
                            <div class="table-responsive">
                                <table id="datatable2" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class='text-primary'>
                                        <tr>
                                            <th>Họ và Tên</th>
                                            <th>Tên Doanh Nghiệp</th>
                                            <th>Nghành học</th>
                                            <th>Email</th>
                                            <th>Xử lý</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($reportDone as $row)
                                        <tr>
                                            <td>{{$row->user->name}}</td>
                                            <td>{{$row->company->name}}</td>
                                            <td>{{$row->user->student->career->name}}</td>
                                            <td>{{$row->user->email}}</td>
                                            <td class="text-center">
                                                <a href="{{route('staff.report.show',$row->id)}}" class="btn btn-info" title="Xem thông tin chi tiết báo cáo sinh viên">
                                                    <i class="far fa-address-card"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="messages1">
                            <div class="table-responsive">
                                <table id="datatable3" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class='text-primary'>
                                        <tr>
                                            <th>Họ và Tên</th>
                                            <th>Tên Doanh Nghiệp</th>
                                            <th>Nghành học</th>
                                            <th>Email</th>
                                            <th>Nhận xét</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($reportPass as $row)
                                        <tr>
                                            <td>{{$row->user->name}}</td>
                                            <td>{{$row->company->name}}</td>
                                            <td>{{$row->user->student->career->name}}</td>
                                            <td>{{$row->user->email}}</td>
                                            <td>
                                                @if($row->status == REPORT_PASS) <p class="text-success">Đạt</p>
                                                @elseif($row->status == REPORT_FAIL) <P class="text-danger">Không đạt</P>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="settings1">
                            <div class="table-responsive">
                                <table id="datatable4" class="table table-striped table-hover table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class='text-primary'>
                                        <tr>
                                            <th>Họ và Tên</th>
                                            <th>Tên Doanh Nghiệp</th>
                                            <th>Nghành học</th>
                                            <th>Email</th>
                                            <th>Nhận xét</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($reportFail as $row)
                                        <tr>
                                            <td>{{$row->user->name}}</td>
                                            <td>{{$row->company->name}}</td>
                                            <td>{{$row->user->student->career->name}}</td>
                                            <td>{{$row->user->email}}</td>
                                            <td>
                                                @if($row->status == REPORT_PASS) <p class="text-success">Đạt</p>
                                                @elseif($row->status == REPORT_FAIL) <P class="text-danger">Không đạt</P>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="new">
                    <div class="table-responsive">
                        <table id="datatable5" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class='text-primary'>
                                <tr>
                                    <th>Họ và Tên</th>
                                    <th>Tên Doanh Nghiệp</th>
                                    <th>Nghành học</th>
                                    <th>Email</th>
                                    <th>Xử lý</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($reportDone as $row)
                                <tr>
                                    <td>{{$row->user->name}}</td>
                                    <td>{{$row->company->name}}</td>
                                    <td>{{$row->user->student->career->name}}</td>
                                    <td>{{$row->user->email}}</td>
                                    <td class="text-center">
                                        <a href="{{route('staff.report.show',$row->id)}}" class="btn btn-info" title="Xem thông tin chi tiết báo cáo sinh viên">
                                            <i class="far fa-address-card"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
<!-- Required datatable js -->
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datatable1").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)")

        $("#datatable2").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)")

        $("#datatable3").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable3_wrapper .col-md-6:eq(0)")

        $("#datatable4").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable4_wrapper .col-md-6:eq(0)")

        $("#datatable5").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable5_wrapper .col-md-6:eq(0)")
    });

</script>
@endpush
