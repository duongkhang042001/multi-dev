@extends('layouts.dashboard')
@section('page-title', 'Quản Lý doanh nghiệp | Tổng quan')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a>
                        <li class="breadcrumb-item"><a href="{{route('staff.recruitment.index')}}">Báo cáo thực tập</a>
                        <li class="breadcrumb-item active">Chi tiết báo cáo
                    </ol>
                </div>
                <h4 class="page-title">Báo Cáo Thực Tập Của Sinh Viên</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="card" style="min-height: 100%">
                <div class="card-header">
                    <h3 class="card-title"><i class=" fas fa-file-alt mr-1"></i> Nội dung báo cáo thực tập</h3>
                    <span class="sub-header">Bao gồm tất cả thông về báo cáo thực tập của sinh viên, doanh nghiệp, nội dung báo cáo,...</span>
                </div>
                <div class="card-body">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <ul class="nav nav-tabs" data-tabs="tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#all" data-toggle="tab">
                                        Thông tin
                                    </a>

                                <li class="nav-item">
                                    <a class="nav-link" href="#faq" data-toggle="tab">
                                        Đơn vị thực tập
                                    </a>

                                <li class="nav-item">
                                    <a class="nav-link" href="#company" data-toggle="tab">
                                        Báo cáo hằng tuần
                                    </a>

                                <li class="nav-item">
                                    <a class="nav-link" href="#done" data-toggle="tab">
                                        Kết quả thực tập
                                    </a>

                                <li class="nav-item">
                                    <a class="nav-link" href="#reponse" data-toggle="tab">
                                        Đánh giá thực tập
                                    </a>
                            </ul>
                        </div>
                    </div>
                    <div class="border border-top-0 p-3" style="border-bottom: 1px solid #dee2e6;">
                        <div class="tab-content">
                            <div class="tab-pane active" id="all">
                                <h4><i class="far fa-address-book mr-2"></i> Thông Tin Sinh Viên</h4>
                                <div class="card-box mb-5">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped mb-0 text-nowrap">
                                            <tbody>
                                            <tr class="table-success">
                                                <th class="text-nowrap" scope="row">Họ Tên</th>
                                                <th colspan="5">{{$report->user->name}}</th>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Chuyên Ngành</th>
                                                <td colspan="4">{{$report->user->student->career->name}}</td>
                                                <th>Kỳ {{$report->user->student->current_semester}}</th>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Email</th>
                                                <td colspan="5">{{$report->user->email}}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Ngày sinh</th>
                                                <td colspan="2">{{date('d-m-Y',strtotime($report->user->profile->birthday))}}</td>
                                                <td class="bg-table-info"></td>
                                                <th>Giới tính</th>
                                                <td>{{$report->user->profile->gender ? "Nữ" : "Nam"}}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Địa chỉ</th>
                                                <td colspan="5">{{$report->user->profile->getAddress()}}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Báo cáo của kỳ</th>
                                                <td colspan="2">{{$report->semester->name}}</td>
                                                <td class="bg-table-info"></td>
                                                <th>Ngày đăng ký</th>
                                                <td colspan="2">{{date('d-m-Y',strtotime($report->registed_at))}}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Hình thức thực tập</th>
                                                <td colspan="5">
                                                    @if($report->user->student->current_semester < 6 )
                                                        <span class="text-success">Sinh viên thực tập</span>
                                                    @elseif($report->user->student->current_semester < 8 )
                                                        <span class="text-info">Sinh viên thực tập đúng hạn</span>
                                                    @else
                                                        <span class="text-warning">Sinh viên thực tập trễ hạn</span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Hình thức đăng ký</th>
                                                <td colspan="5">
                                                    @if($report->is_outside)
                                                        <span
                                                            class="text-warning">Đăng ký thực tập bên ngoài hệ thống</span>
                                                    @else
                                                        <span class="text-info">Đăng ký thực tập trên hệ thống</span>
                                                    @endif
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <h4><i class="fas fa-building mr-2"></i>Thông Tin Doanh Nghiệp</h4>
                                <div class="card-box">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped mb-0 text-nowrap">
                                            <tbody>
                                            <tr class="table-success">
                                                <th class="" scope="row">Doanh nghiệp</th>
                                                <th colspan="5"><a target="_blank"
                                                                   href="{{route('staff.company.show',$report->company_id)}}">{{$report->company->name}}</a>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Tên viết tắt</th>
                                                <td colspan="5">{{$report->company->short_name}}</td>

                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Website</th>
                                                <td colspan="5"><a href="{{json_decode($report->company->url)->url}}"
                                                                   target="_blank">{{json_decode($report->company->url)->url}}</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Lĩnh vực</th>
                                                <td colspan="2">{{$report->company->careerGroup->name}}</td>
                                                <td></td>
                                                <th class="text-nowrap" scope="row">Số điện thoại</th>
                                                <td colspan="2">{{$report->company->phone}}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Ngày thành lập</th>
                                                <td colspan="2">{{date('d-m-Y',strtotime($report->company->founded_at))}}</td>
                                                <td class="bg-table-info"></td>
                                                <th>Mã số thuế</th>
                                                <td>{{$report->company->tax_number}}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Địa chỉ</th>
                                                <td colspan="5">{{$report->company->getAddress()}}
                                                    @if(!empty($report->company->lat) && !empty($report->company->lng))
                                                        <a class="float-right"
                                                           href="https://maps.google.com/maps/?q={{$report->company->lat}},{{$report->company->lng}}"
                                                           target="_blank"><i class="fas fa-map-marked-alt"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Trạng thái hoạt động</th>
                                                <td colspan="5">
                                                    @if($report->company->on_system)
                                                        <span class="text-success">Hoạt động trên hệ thống</span>
                                                    @else
                                                        <span class="text-warning">Hoạt động bên ngoài hệ thống</span>
                                                    @endif
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                        <table class="table table-bordered table-striped mb-0 text-nowrap mt-3">
                                            <tbody>
                                            <tr class="table-success">
                                                <th class="text-nowrap" scope="row">Người hướng dẫn</th>
                                                <th colspan="5">{{ !empty($report->tutor_name) ? $report->tutor_name :"Chưa có dữ liệu!" }}</th>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Email</th>
                                                <td colspan="5">{{ !empty($report->tutor_email) ? $report->tutor_email :"Chưa có dữ liệu!" }}</td>

                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Vị trí</th>
                                                <td colspan="2">{{$report->tutor_position}}</td>
                                                <td></td>
                                                <th class="text-nowrap" scope="row">Số điện thoại</th>
                                                <td colspan="2">{{ !empty($report->tutor_phone) ? $report->tutor_phone :"Chưa có dữ liệu!" }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-nowrap" scope="row">Ngày thành lập</th>
                                                <td colspan="2">{{date('d-m-Y',strtotime($report->company->founded_at))}}</td>
                                                <td class="bg-table-info"></td>
                                                <th>Mã số thuế</th>
                                                <td>{{$report->company->tax_number}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="faq">
                                <div class="card shadow-sm">
                                    <h4 class="card-header"><i class="fas fa-chalkboard-teacher text-info mr-2"></i> GIỚI THIỆU KHÁI QUÁT VỀ ĐƠN VỊ THỰC TẬP</h4>
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <h5 class="card-title"><i class=" fas fa-align-left mr-2"></i>Tóm lược quá trình hình thành và phát triển</h5>
                                            <div class="p-sm-2">{!! !empty($report->about) ? $report->about :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                        <div class="mb-3">
                                            <h5 class="card-title"><i class=" fas fa-align-left mr-2"></i> Chức năng và lĩnh vực hoạt động</h5>
                                            <div class="p-sm-2">{!! !empty($report->function_area_activities) ? $report->function_area_activities :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                        <div class="mb-3">
                                            <h5 class="card-title"><i class=" fas fa-align-left mr-2"></i> Sản phẩm và dịch vụ</h5>
                                            <div class="p-sm-2">{!! !empty($report->product_services) ? $report->product_services :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                        <div class="mb-3">
                                            <h5 class="card-title"><i class=" fas fa-align-left mr-2"></i> Tổ chức quản lý hành chính, nhân sự</h5>
                                            <div class="p-sm-2">{!! !empty($report->strategy_future) ? $report->strategy_future :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                        <div class="mb-3">
                                            <h5 class="card-title"><i class=" fas fa-align-left mr-2"></i> Chiến lược và phương hướng phát triển của đơn vị trong tương lai</h5>
                                            <div class="p-sm-2">{!! !empty($report->strategy_future) ? $report->strategy_future :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card shadow-sm">
                                    <h4 class="card-header"><i class="fas fa-book-reader text-warning mr-2"></i> BÁO CÁO NỘI DUNG CÔNG VIỆC THỰC TẬP</h4>
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <h5 class="card-title"><i class=" fas fa-align-left mr-2"></i>Giới thiệu tóm tắt các hoạt động/công việc tại đơn vị thực tập</h5>
                                            <div class="p-sm-2">{!! !empty($report->summary_activities) ? $report->summary_activities :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                        <div class="mb-3">
                                            <h5 class="card-title"><i class=" fas fa-align-left mr-2"></i>Mô tả các công việc được phân công thực hiện hoặc được tham gia tại đơn vị </h5>
                                            <div class="p-sm-2">{!! !empty($report->work) ? $report->work :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="company">
                                <div class="mb-3">
                                    <ul class="nav nav-tabs">
                                        @php $loopArr =[]; @endphp
                                        @foreach ($allWeek as $key => $week)
                                            @if ($loop->iteration > 10)
                                                @break
                                            @endif
                                            @php
                                                $arr = collect($arrDate)->filter(function($value) use($week){
                                                    if(strtotime($value) <= strtotime($week['end_week']) && strtotime($value) >= strtotime($week['start_week'])){
                                                        return $value;
                                                    }
                                                });
                                            @endphp
                                            <li class="nav-item">
                                                @if (!count($arr))
                                                    <a href="javascript:void(0)"  class="nav-link text-danger" data-toggle="tooltip"  title="Không có dữ liệu" class="text-danger"> Tuần {{$key}} </a>
                                                @else
                                                    @php(array_push($loopArr,$week))
                                                    <a class="nav-link @if(head($loopArr)==$week) active @endif" href="#privacy{{$key}}" id="privacy-tab{{$key}}" data-toggle="tab" aria-expanded="false" >
                                                        Tuần {{$key}}
                                                    </a>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content py-3">
                                        @foreach ($allWeek as $key => $week)

                                            <div class="tab-pane @if(head($loopArr)==$week) active @endif" id="privacy{{$key}}" role="tabpanel" aria-labelledby="privacy-tab{{$key}}" >
                                                <h5>
                                                    <i class=" far fa-calendar-alt mr-2"></i>Báo cáo thực tập
                                                    <span class="font-italic">
                                                {{date('d/m/Y', strtotime($week['start_week']))}}
                                                -
                                                {{date('d/m/Y', strtotime($week['end_week']))}}
                                            </span>
                                                </h5>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered text-nowrap">
                                                        <thead class="thead-light ">
                                                        <th class=""></th>
                                                        <th>
                                                            <h5>Sáng</h5>
                                                        </th>
                                                        <th>
                                                            <h5>Chiều</h5>
                                                        </th>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($reportDetail as $item)
                                                            @if (strtotime($item->date) <= strtotime($week['end_week']) && strtotime($item->date) >= strtotime($week['start_week']))
                                                                <tr>
                                                                    <th class="text-center">
                                                                        {{DAYOFWEEK[\Carbon\Carbon::parse($item->date)->dayOfWeek]}}
                                                                        <br>
                                                                        ({{date('d/m/Y', strtotime($item->date))}})
                                                                    </th>
                                                                    <td >{{ $item->content_morning }}</td>
                                                                    <td >{{ $item->content_afternoon }} </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                @if(count($allWeek) >10)
                                    <div class="mb-3">
                                        <ul class="nav nav-tabs">
                                            @foreach ($allWeek as $key => $week)
                                                @if ($loop->iteration <= 10)
                                                    @continue
                                                @endif
                                                <?php
                                                    $arr = collect($arrDate)->filter(function($value) use($week){
                                                        if(strtotime($value) <= strtotime($week['end_week']) && strtotime($value) >= strtotime($week['start_week'])){
                                                            return $value;
                                                        }
                                                    });
                                                ?>
                                                <li class="nav-item">
                                                    @if (!count($arr))
                                                        <a href="javascript:void(0)"  class="nav-link text-danger" data-toggle="tooltip"  title="Không có dữ liệu" class="text-danger"> Tuần {{$key}} </a>
                                                    @else
                                                        @php(array_push($loopArr,$week))
                                                        <a class="nav-link @if(head($loopArr)==$week) active @endif" href="#privacy{{$key}}" id="privacy-tab{{$key}}" data-toggle="tab" aria-expanded="false" >
                                                            Tuần {{$key}}
                                                        </a>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                        <div class="tab-content py-3">
                                            @foreach ($allWeek as $key => $week)
                                                @if ($loop->iteration <= 10)
                                                    @continue
                                                @endif
                                                <div class="tab-pane @if(head($loopArr)==$week) active @endif" id="privacy{{$key}}" role="tabpanel" aria-labelledby="privacy-tab{{$key}}" >
                                                    <h5>
                                                        <i class=" far fa-calendar-alt mr-2"></i>Báo cáo thực tập
                                                        <span class="font-italic">
                                                            {{date('d/m/Y', strtotime($week['start_week']))}}
                                                            -
                                                            {{date('d/m/Y', strtotime($week['end_week']))}}
                                                        </span>
                                                    </h5>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered text-nowrap">
                                                            <thead class="thead-light ">
                                                            <th class=""></th>
                                                            <th>
                                                                <h5>Sáng</h5>
                                                            </th>
                                                            <th>
                                                                <h5>Chiều</h5>
                                                            </th>
                                                            </thead>
                                                            <tbody>
                                                            @foreach ($reportDetail as $item)
                                                                @if (strtotime($item->date) <= strtotime($week['end_week']) && strtotime($item->date) >= strtotime($week['start_week']))
                                                                    <tr>
                                                                        <th class="text-center">
                                                                            {{DAYOFWEEK[\Carbon\Carbon::parse($item->date)->dayOfWeek]}}
                                                                            <br>
                                                                            ({{date('d/m/Y', strtotime($item->date))}})
                                                                        </th>
                                                                        <td >{{ $item->content_morning }}</td>
                                                                        <td >{{ $item->content_afternoon }} </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="tab-pane" id="done">
                                <div class="card shadow-sm">
                                    <h5 class="card-header text-uppercase"><i class="fab fa-get-pocket text-success mr-2"></i> Kết quả đạt được trong thời gian thực tập</h5>
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <div class="p-sm-2">{!! !empty($report->result) ? $report->result :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card shadow-sm">
                                    <h5 class="card-header text-uppercase"><i class=" fas fa-times-circle text-danger mr-2"></i> Những điều chưa thực hiện được</h5>
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <div class="p-sm-2">{!! !empty($report->work_unfinished) ? $report->work_unfinished :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card shadow-sm">
                                    <h5 class="card-header text-uppercase"><i class=" fas fa-search text-info mr-2"></i> Các vấn đề cần tiếp tục nghiên cứu, phát triển</h5>
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <div class="p-sm-2">{!! !empty($report->problem_develop) ? $report->problem_develop :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card shadow-sm">
                                    <h5 class="card-header text-uppercase"><i class="fab fa-get-pocket text-success mr-2"></i> Kết quả đạt được trong thời gian thực tập</h5>
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <div class="p-sm-2">{!! !empty($report->result) ? $report->result :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card shadow-sm">
                                    <h5 class="card-header text-uppercase"><i class="fab fa-get-pocket text-success mr-2"></i> Nhận xét chung</h5>
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <h5 class="card-title"><i class=" fas fa-align-left mr-2"></i> Nhận xét chung</h5>
                                            <div class="p-sm-2"> {!! !empty($report->general_comment) ? json_decode($report->general_comment)->general :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                        <div class="mb-3">
                                            <h5 class="card-title"><i class=" fas fa-align-left mr-2"></i> Thuận lợi</h5>
                                            <div class="p-sm-2">{!! !empty($report->general_comment) ? json_decode($report->general_comment)->advantage :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                        <div class="mb-3">
                                            <h5 class="card-title"><i class=" fas fa-align-left mr-2"></i> Khó khăn</h5>
                                            <div class="p-sm-2">{!! !empty($report->propose) ? $report->propose :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card shadow-sm">
                                    <h5 class="card-header text-uppercase"><i class="fab fa-get-pocket text-success mr-2"></i> ĐỀ XUẤT – KIẾN NGHỊ VỚI ĐƠN VỊ THỰC TẬP</h5>
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <div class="p-sm-2">{!! !empty($report->general_comment) ? json_decode($report->general_comment)->difficult :"Chưa có dữ liệu!" !!}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="reponse">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 ><i class=" fas fa-bookmark text-success mr-2"></i>NHẬN XÉT CỦA CƠ QUAN THỰC TẬP</h4>
                                        <p class="sub-header">Hình thức nhận xét thực tập {{ $report->is_outside ? 'thông qua email' : 'trực tiếp trên hệ thống' }}</p>
                                        <div class="table-responsive">
                                            <table class="table table-bordered  table-background-light text-nowrap">
                                                @if($report->is_outside && !empty($rateFile))
                                                    <tr>
                                                        <th>Tệp đánh giá</th>
                                                        <td class="col"><a href="{{$rateFile['view']}}" target="_blank">{{$rateFile['name']}}</a></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Ngày gửi file</th>
                                                        <td>{{$report->rateFile->created_at}}</td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <th>Ưu điểm</th>
                                                        <td class="col">{{ !empty($report->reportRate->advantages) ? $report->reportRate->advantages :"Chưa có dữ liệu!"}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Hạn chế:</th>
                                                        <td>{{ !empty($report->reportRate->defect) ? $report->reportRate->defect :"Chưa có dữ liệu!" }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Đề xuất, góp ý :</th>
                                                        <td>{{ !empty($report->reportRate->content) ? $report->reportRate->content :"Chưa có dữ liệu!" }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Thái độ, ý thức: </th>
                                                        <td>{{ !empty($report->reportRate->attitude_point) ? $report->reportRate->attitude_point.'điểm' :"Chưa có dữ liệu!" }} </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Kết quả công việc:</th>
                                                        <td>{{ !empty($report->reportRate->work_point) ? $report->reportRate->work_point.'điểm' :"Chưa có dữ liệu!" }} </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Đánh giá cuối cùng:</th>
                                                        <td>{{isset($report->reportRate->status) ? ($report->reportRate->status == 0 ? 'Đạt' : 'Không đạt') :"Chưa có dữ liệu!" }}</td>
                                                    </tr>

                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-box bg-soft-info">
                                    <h4 ><i class=" fas fa-bookmark text-success mr-2"></i>ĐÁNH GIÁ TỪ NHÀ TRƯỜNG</h4>
                                    <p class="sub-header">Đưa ra kết quả thực tập cuối cho sinh viên!</p>
                                    @if(in_array($report->status,[REPORT_FAIL,REPORT_PASS]))
                                        <div class="table-responsive">
                                            <table class="table table-bordered text-nowrap">
                                                <tr>
                                                    <th>Kết quả</th>
                                                    <td class="col">
                                                        @if($report->status == REPORT_PASS)
                                                            <span class="text-success">Đạt</span>
                                                        @else
                                                            <span class="text-danger">Không Đạt</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Lý do</th>
                                                    <td>
                                                        {{!empty($report->status_reason) ? $report->status_reason : '(không có)'}}
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    @else
                                        <form action="{{route('staff.report.update',$report->id)}}" method="post">
                                            @csrf
                                            @method('PUT')
                                            <h5 class=" mt-3 mb-2">Kết quả thực tập</h5>
                                            <div class="px-2    ">
                                                <div class="radio radio-success form-check-inline">
                                                    <input type="radio" id="radio_1" name="status" value="PASS" onclick="myFunction()" checked>
                                                    <label for="radio_1">Đạt</label>
                                                </div>
                                                <div class="radio radio-danger form-check-inline">
                                                    <input type="radio" id="radio_2" name="status" value="FAIL" onclick="myFunction()" >
                                                    <label for="radio_2">Không đạt</label>
                                                </div>
                                            </div>
                                            <div id="text" style="display: none" >
                                                <span class="sub-header">Lí do không đạt <span class="text-danger"> (*) </span></span>
                                                <input type="text" class="form-control" name="status_reason">
                                            </div>
                                            <button class="btn btn-info btn-ml mt-3">Đánh giá báo cáo</button>
                                        </form>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><i class=" far fa-address-book mr-1"></i> Thông tin cơ bản</h3>
                    <span class="sub-header">Hình ảnh, thông tin sinh viên.</span>
                </div>
                <div class="card-body">
                    <h4 class="header-title m-t-0 m-b-30">Ảnh Đại Diện</h4>
                    <input type="file" class="dropify" data-default-file="assets/images/default-avatar.png" disabled/>

                    <h6 class="card-category text-primary mt-3"><i
                            class="mdi mdi-shield-account"> </i> {{$report->user->role->name}} </h6>
                    <h3 class="font-weight-bold"> {{$report->user->name}} </h3>
                    <p class="text-primary">Email: {{$report->user->email}}</p>
                    <div class="">
                        @if ($report->user->logged_at)
                            <span class="text-success font-weight-bold"><i class="fa fa-check-circle "></i> Đang hoạt
                                    động</span>
                            <br>
                            <span class="text-gray font-italic">Đăng nhập gần nhất:
                                    {{ date('d-m-Y H:i:s', strtotime($report->user->logged_at)) }}</span>
                        @elseif(!$report->user->is_active)
                            <span><i class="fa fa-check-circle text-warning font-weight-bold"></i> Ngừng hoạt
                                    động</span>
                            <span
                                class="text-gray font-italic">Lần cuối đăng nhập: {{ $report->user->logged_at }}</span>
                        @else
                            <span><i class="fa fa-check-circle text-danger font-weight-bold"></i> Không hoạt
                                    động</span>
                        @endif
                    </div>
                    <p class="card-description mt-3 font-italic">
                        {{!empty($report->user->profile->descricption) ? $report->user->profile->descricption : 'Chưa có thông tin tiểu sử' }}
                    </p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-4">Hiện tại:</div>
                                <div class="col-8">Kỳ {{$report->user->student->current_semester }}</div>
                            </div>

                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-4">Ngành học</div>
                                <div class="col-8">{{$report->user->student->career->name }}</div>
                            </div>

                        </li>

                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-4">CV hiển thị:</div>
                                <div class="col-8">
                                    @if(!empty($cv))
                                        <a href="{{$cv['view']}}" target="_blank">{{$cv['name']}}</a>
                                    @else
                                        Chưa có
                                    @endif
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-4">Báo cáo</div>
                                <div class="col-8">
                                    <span class="text-success">Đã nộp</span>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-4">Báo cáo kỳ</div>
                                <div class="col-8">
                                    <span class="text-info">{{$report->semester->name}}</span>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-4">Ngày đăng ký</div>
                                <div class="col-8">
                                    <span>{{date('d-m-Y',strtotime($report->created_at))}}</span>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-4">Loại đăng ký</div>
                                <div class="col-8">
                                    @if(!$report->is_outside) <span class="text-info">Đăng ký trên hệ thống</span> @else
                                        <span class="text-pink">Đăng ký bên ngoài</span> @endif
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-4">Trạng thái</div>
                                <div class="col-8">
                                    <span>HỌC ĐI</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- form Uploads -->
    <link href="assets/libs/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css"/>
@endpush
@push('script')
    <script src="assets/libs/fileuploads/js/dropify.min.js"></script>

    <script type="text/javascript">
        function myFunction() {
            var chkYes = document.getElementById("radio_2");
            var dvPassport = document.getElementById("text");
            text.style.display = chkYes.checked ? "block" : "none";
        }

        $('.dropify').dropify({
            messages: {
                'default': 'Kéo và thả tệp vào đây hoặc nhấp vào',
                'replace': 'Kéo và thả hoặc nhấp để thay thế',
                'remove': 'Thay đổi',
                'error': 'Rất tiếc, đã xảy ra sự cố.'
            },
            error: {
                'fileSize': 'Kích thước tệp quá lớn (tối đa 1M).'
            }
        });

        let urlImage = `{{$report->user->getAvatar()}}`
        if (urlImage) $(".dropify-render img").attr("src", urlImage);
        $(".dropify-infos").addClass('d-none');
    </script>
@endpush
