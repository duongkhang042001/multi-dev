@extends('layouts.dashboard')
@section('page-title', 'Quản lý Bài Viết | Chỉnh sửa')
@section('title', 'Chỉnh Bài Viết')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{route('staff.post.index')}}">Danh sách bài viết</a></li>
                    <li class="breadcrumb-item active">Chỉnh bài viết</li>
                </ol>
            </div>
            <h4 class="page-title">Chỉnh bài viết</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">

            <form method="POST" onsubmit="getLoading()" action="{{ route('staff.post.update', [$post->id]) }}" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <label class="col-sm-2 col-form-label">Tiêu Đề Bài Viết</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="title" value="{{ $post->title }}">
                            @error('title')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Đường dẫn</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="slug" value="{{ $post->slug }}">
                            @error('slug')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Loại Bài Viết</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <select name="post-type" value="{{ old('post-type') }}" class="form-control">
                                <option value="0">Vui lòng chọn loại bài viết</option>
                                @foreach ($postTypes as $postType)
                                    <option value="{{ $postType->id }}" @if ($postType->id == $post->post_type_id) selected @endif>
                                        {{ $postType->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                </div>
                <div class="form-group row ">
                    <label class="col-sm-2 col-form-label">Ảnh thu nhỏ</label>
                    <div class="col-md-6">
                        <div class="form-group">
                            @error('thumbnail')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                            <input type="file" class="dropify" name="thumbnail" data-default-file="assets/images/default-avatar.png"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Mô Tả Bài Viết</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <textarea class="form-control ckeditor" id="description" name="description" rows="10">{{ $post->description }}</textarea>
                            @error('description')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Nội Dung Bài Viết</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <textarea class="form-control ckeditor" id="content" name="content" rows="10">{{ $post->content }}</textarea>
                            @error('content')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="card-footer text-right">
                    <a href="{{route('staff.post.show',$post->id)}}" class="btn btn-secondary"><i class="fas fa-ban mr-1"></i> Hủy</a>
                    <button type="submit" class="btn btn-success px-3"><i class="fa fa-save mr-1"></i> Lưu</button>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection


@push('css')  <!-- form Uploads -->
<link href="assets/libs/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
    <script>
        CKEDITOR.replace('content',{
            height: 500
        })

    </script>
    <!-- file uploads js -->
    <script src="assets/libs/fileuploads/js/dropify.min.js"></script>
    <script type="text/javascript">
        $('.dropify').dropify({
            messages: {
                'default': 'Kéo và thả tệp vào đây hoặc nhấp vào',
                'replace': 'Kéo và thả hoặc nhấp để thay thế',
                'remove': 'Thay đổi',
                'error': 'Rất tiếc, đã xảy ra sự cố.'
            },
            error: {
                'fileSize': 'Kích thước tệp quá lớn (tối đa 1M).'
            }
        });
        let urlImage = `{{$post->getThumbnail()}}`
        if(urlImage) $(".dropify-render img").attr("src",urlImage) ;
    </script>
@endpush()
