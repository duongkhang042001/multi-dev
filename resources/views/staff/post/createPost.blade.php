@extends('layouts.dashboard')
@section('page-title', 'Quản lý Bài Viết | Tạo mới')
@section('title', 'Thêm Bài Viết')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('staff.post.index') }}">Danh sách bài viết</a></li>
                        <li class="breadcrumb-item active">Tạo bài viết</li>
                    </ol>
                </div>
                <h4 class="page-title">Tạo bài viết</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form method="POST" onsubmit="getLoading()" action="{{ route('staff.post.store') }}" class="form-horizontal"
                    enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <div class="card-header">
                        <div class="card-text">
                            <h4 class="d-inline-block">Tạo bài viết</h4>
                            <div class="float-right">
                                <button type="reset" class="btn btn-outline-info"><i class="fe-refresh-cw mr-1"></i> Làm
                                    mới</button>
                                <button type="submit" class="btn btn-success"><i class="fa fa-save mr-1"></i> Lưu</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Tiêu Đề Bài Viết</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="title" name="title"
                                        value="{{ old('title') }}">
                                    @error('title')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Đường dẫn</label>
                            <div class="col-sm-10">
                                <div class="form-group ">
                                    <input type="text" class="form-control" id="slug" name="slug"
                                        value="{{ old('slug') }}" placeholder="bai-viet-huong-dan-thuc-tap">
                                    @error('slug')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Loại Bài Viết</label>
                            <div class="col-sm-10">
                                <div class="form-group ">
                                    <select name="post-type" value="{{ old('post-type') }}" class="form-control">
                                        <option value="0" selected>Vui lòng chọn loại bài viết</option>
                                        @foreach ($postType as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('post-type')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-sm-2 col-form-label">Ảnh thu nhỏ</label>
                            <div class="col-md-10">
                                <div class="form-group">
                                    @error('thumbnail')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                    <input type="file" class="dropify" name="thumbnail"
                                        value="{{ old('thumbnail') }}" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Mô Tả Bài Viết</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <textarea class="form-control ckeditor"
                                        name="description">{{ old('description') }}</textarea>
                                    @error('description')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nội Dung Bài Viết</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <textarea class="form-control ckeditor" id="content"
                                        name="content">{{ old('content') }}</textarea>
                                    @error('content')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer text-right">
                        <button type="reset" class="btn btn-outline-info"><i class="fe-refresh-cw mr-1"></i> Làm
                            mới</button>
                        <button type="submit" class="btn btn-success"><i class="fa fa-save mr-1"></i> Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- form Uploads -->
    <link href="assets/libs/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endpush

@push('script')
    <!-- Script You Need -->
    <!-- file uploads js -->
    <script src="assets/libs/fileuploads/js/dropify.min.js"></script>
    <script type="text/javascript">
        $('.dropify').dropify({
            messages: {
                'default': 'Kéo và thả tệp vào đây hoặc nhấp vào',
                'replace': 'Kéo và thả hoặc nhấp để thay thế',
                'remove': 'Thay đổi',
                'error': 'Rất tiếc, đã xảy ra sự cố.'
            },
            error: {
                'fileSize': 'Kích thước tệp quá lớn (tối đa 1M).'
            }
        });
        
        $("#title").keyup(function() {
            var Text = $(this).val();
            $slug = slugify(Text);
            $("#slug").val($slug);
        });

        function slugify(string) {
            const a = 'àáäâãåăæąçćčđďèéěėëêęğǵḧìíïîįłḿǹńňñòóöôœøṕŕřßşśšșťțùúüûǘůűūųẃẍÿýźžż·/_,:;'
            const b = 'aaaaaaaaacccddeeeeeeegghiiiiilmnnnnooooooprrsssssttuuuuuuuuuwxyyzzz------'
            const p = new RegExp(a.split('').join('|'), 'g')
            return string.toString().toLowerCase()
                .replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a')
                .replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e')
                .replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i')
                .replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o')
                .replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u')
                .replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y')
                .replace(/đ/gi, 'd')
                .replace(/\s+/g, '-')
                .replace(p, c => b.charAt(a.indexOf(c)))
                .replace(/&/g, '-and-')
                .replace(/[^\w\-]+/g, '')
                .replace(/\-\-+/g, '-')
                .replace(/^-+/, '')
                .replace(/-+$/, '')
        }
    </script>
@endpush
