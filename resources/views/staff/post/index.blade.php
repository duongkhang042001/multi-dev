@extends('layouts.dashboard')
@section('page-title', 'Quản lý bài viết | Tổng quan')
@section('title', 'Quản lý bài viết')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách bài viết</li>
                </ol>
            </div>
            <h4 class="page-title">Danh sách bài viết</h4>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#all" data-toggle="tab">
                            Bài Viết
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#faq" data-toggle="tab">
                            Câu Hỏi Thường Gặp
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#company" data-toggle="tab">
                            Bài Viết Doanh Nghiệp
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-box">
            <div class="tab-content">
                <div class="tab-pane active" id="all">
                    <table class="table table-striped table-bordered nowrap dataTable no-footer" id="datatable1">
                        <thead class="text-primary">
                            <tr>
                                <th>Tiêu Đề</th>
                                <th>Trạng Thái</th>
                                <th>Bình Luận</th>
                                <th>Thời gian tạo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($postNormal as $row)
                            <tr>
                                <td class="col"><a href=" {{route('staff.post.show',$row->id)}} "> {{Str::limit($row->title, 100)}} </a></td>
                                <td>
                                    @if (!$row->is_active)
                                        <span class="text-danger">Ngừng hoạt động</span>
                                    @elseif(!$row->is_show)
                                        <span class="text-warning">Không hiển thị</span>
                                    @else
                                        <span class="text-success">Đang hoạt động</span>
                                    @endif
                                </td>
                                <td>
                                    @if (!$row->is_comment)
                                        <span class="text-warning">Đã tắt</span>
                                    @elseif(empty($row->comments))
                                        <span class="text-warning">Chưa có</span>
                                    @else
                                        <span class="text-success">Đang hoạt động</span>
                                    @endif
                                </td>
                                <td>
                                    {{ date('d/m/Y H:i:s', strtotime($row->created_at)) }}
                                    <br>
                                    {{ $row->user->profile->last_name }}
                                </td>
                                <td>
                                    <a href=" {{route('staff.post.show',$row->id)}} " class="btn btn-outline-info mr-1" data-toggle="tooltip" title="" data-original-title="Xem chi tiết ">
                                        <i class="far fa-sticky-note"></i>
                                    </a>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="faq">
                    <table class="table table-striped table-bordered nowrap dataTable no-footer" id="datatable2">
                        <thead class="text-primary">
                            <tr>
                                <th>Tiêu Đề</th>
                                <th>Trạng Thái</th>
                                <th>Bình Luận</th>
                                <th>Thời gian tạo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($postFAQ as $row)
                                <tr>
                                    <td class="col"><a href=" {{route('staff.post.show',$row->id)}} "> {{Str::limit($row->title, 100)}} </a></td>
                                    <td>
                                        @if (!$row->is_active)
                                            <span class="text-danger">Ngừng hoạt động</span>
                                        @elseif(!$row->is_show)
                                            <span class="text-warning">Không hiển thị</span>
                                        @else
                                            <span class="text-success">Đang hoạt động</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if (!$row->is_comment)
                                            <span class="text-warning">Đã tắt</span>
                                        @elseif(empty($row->comments))
                                            <span class="text-warning">Chưa có</span>
                                        @else
                                            <span class="text-success">Đang hoạt động</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{ date('d/m/Y H:i:s', strtotime($row->created_at)) }}
                                        <br>
                                        {{ $row->user->profile->last_name }}
                                    </td>
                                    <td>
                                        <a href=" {{route('staff.post.show',$row->id)}} " class="btn btn-outline-info mr-1" data-toggle="tooltip" title="" data-original-title="Xem chi tiết ">
                                            <i class="far fa-sticky-note"></i>
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="company">
                    <table class="table table-striped table-bordered nowrap dataTable no-footer" id="datatable3">
                        <thead class="text-primary">
                            <tr>
                                <th>Tiêu Đề </th>
                                <th >Trạng Thái</th>
                                <th >Bình Luận</th>
                                <th >Thời gian tạo</th>
                                <th ></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($postCompany as $row)
                                <tr>
                                    <td class="col"><a href=" {{route('staff.post.show',$row->id)}} "> {{Str::limit($row->title, 100)}} </a></td>
                                    <td>
                                        @if (!$row->is_active)
                                            <span class="text-danger">Ngừng hoạt động</span>
                                        @elseif(!$row->is_show)
                                            <span class="text-warning">Không hiển thị</span>
                                        @else
                                            <span class="text-success">Đang hoạt động</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if (!$row->is_comment)
                                            <span class="text-warning">Đã tắt</span>
                                        @elseif(empty($row->comments))
                                            <span class="text-warning">Chưa có</span>
                                        @else
                                            <span class="text-success">Đang hoạt động</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{ date('d/m/Y H:i:s', strtotime($row->created_at)) }}
                                        <br>
                                        {{ $row->user->profile->last_name }}
                                    </td>
                                    <td>
                                        <a href=" {{route('staff.post.show',$row->id)}} " class="btn btn-outline-info mr-1" data-toggle="tooltip" title="" data-original-title="Xem chi tiết ">
                                            <i class="far fa-sticky-note"></i>
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div id="notifyModel" class="modal-content">

            </div>
        </div>
    </div>
</div>

@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush

@push('script')
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script>
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#datatable1').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable2').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable3').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable3_wrapper .col-md-6:eq(0)");
    });
</script>
@endpush
