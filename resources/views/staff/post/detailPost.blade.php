@extends('layouts.dashboard')
@section('page-title', 'Quản lý bài viết | Tổng quan')
@section('title', 'Chi tiết bài viết')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('staff.post.index') }}">Danh sách bài viết</a>
                        </li>
                        <li class="breadcrumb-item active">Chi tiết bài viết</li>
                    </ol>
                </div>
                <h4 class="page-title">Chi tiết bài viết</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="mb-3">
                        <h3 class="font-weight-bold">{{$post->title}}</h3>
                        <div class="px-3">
                            <a href="{{route('postDetail',$post->slug)}}" target="_blank"><i
                                    class="fas fa-link text-purple"></i> {{env('APP_URL').'/post/'.$post->slug}}</a>
                            <div class="d-flex flex-wrap">
                                <span class="mr-3"><i
                                        class="fas fa-user-edit text-info"></i> {{$post->user->name}}</span>
                                <span class="mr-3"> @if($post->is_active && $post->is_show)<i
                                        class="fas fa-check-circle text-success"></i> Đang hoạt động @else <i
                                        class="fas fa-times-circle text-danger"></i> Không hoạt động  @endif</span>
                                <span class="mr-3"> <i class="far fa-eye text-pink"></i> {{$post->view}}</span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-body">

                    <div class="mb-3">
                        <h5>Mô tả bài viết</h5>
                        <div class="px-md-5">
                            {!! $post->description !!}
                        </div>
                    </div>
                    <div class="mb-3">
                        <h5>Chi Tiết Bài Viết</h5>
                        <div class="px-md-5">
                            {!! $post->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card-box">
                <h4 class="header-title mt-1"><b>Thông Tin</b></h4>
                <p class="sub-header">Bao gồm thông tin người đăng, loại bài viết ...</p>

                <div class="card-body px-0">
                    <div class="mb-3">
                        <h5>Ảnh thu nhỏ</h5>
                        <img src="{{$post->getThumbnail()}}" onerror="this.src='assets/clients/img/avt-default.png'"
                             class="w-100">
                    </div>
                    <ul class="list-group list-group-flush">
                        <h5>Thông tin chi tiết</h5>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-3">Tác giả:</div>
                                <div class="col-9"> {{$post->user->name}} </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-3">Ngày tạo:</div>
                                <div class="col-9"> {{date('Y-m-d H:i:s',strtotime($post->created_at))}} </div>
                            </div>
                            @if(!empty($post->updated_at))
                                <div class="row">
                                    <div class="col-3">Cập nhật:</div>
                                    <div class="col-9"> {{date('Y-m-d H:i:s',strtotime($post->updated_at))}} </div>
                                </div>
                            @endif
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-3">Trạng thái:</div>
                                <div class="col-9">
                                    @if ($post->is_active)
                                        <span class="text-success"> <i class="fas fa-check-circle  mr-1"></i> Đang hoạt động</span>
                                    @else
                                        <span class="text-danger"><i class="fas fa-times-circle  mr-1"></i> Không hoạt động</span>
                                    @endif
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-3">Hiển thị:</div>
                                <div class="col-9">
                                    @if ($post->is_show)
                                        <span class="text-success"> <i class="fas fa-check-circle  mr-1"></i> Đang hiển thị</span>
                                    @else
                                        <span class="text-danger"><i class="fas fa-times-circle  mr-1"></i> Đã tắt hiển thị</span>
                                    @endif
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-3">Bình luận:</div>
                                <div class="col-9">
                                    @if ($post->is_comment)
                                        <span class="text-success"> <i class="fas fa-check-circle mr-1"></i> Cho phép bình luận</span>
                                    @else
                                        <span class="text-danger"><i class="fas fa-times-circle mr-1"></i> Đã tắt chức năng bình luận</span>
                                    @endif
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-3">Lượt xem:</div>
                                <div class="col-9">
                                    <i class="fas fa-eye mr-1"></i> {{$post->view}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-3">Đường dẫn:</div>
                                <div class="col-9">
                                    <a href="{{route('post',$post->slug)}}">{{env('APP_URL').'/post/'.Str::limit($post->slug,45)}}</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-box">
                <h4 class="card-header">Chức năng</h4>
                <div class="card-box">
                    <div class="row form-group">
                        <div class="col">
                            <a href="javascript:void(isActive('{{$post->id}}','POST_ACTIVE'))"
                               @if($post->is_active)
                                    class="btn btn-info  btn-block">Đang hoạt động</a>
                                @else
                                    class="btn btn-danger  btn-block">Ngừng hoạt động</a>
                               @endif
                        </div>
                        <div class="col">
                            <a href="javascript:void(isActive('{{$post->id}}','RECRUITMENT_SHOW_COMPANY'))"
                               @if($post->is_show)
                                   class="btn btn-info  btn-block">Đang hiển thị</a>
                                @else
                                    class="btn btn-warning  btn-block">Không hiển thị</a>
                                @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <a href="javascript:void(isActive('{{$post->id}}','POST_COMMENT'))"
                               @if($post->is_comment)
                               class="btn btn-info  btn-block">Cho phép bình luận</a>
                            @else
                                class="btn btn-secondary  btn-block">Không cho phép bình luận</a>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <a href="{{ route('staff.post.edit', $post->id) }}" class="btn btn-info  btn-block">Chỉnh
                                Sửa</a>
                        </div>
                        <div class="col">
                            <a href="javascript:void(deleteData('deletedRow{{$post->id}}'))"
                               class="btn btn-outline-danger btn-block">Xóa</a>
                            <form action="{{ route('staff.post.destroy', $post->id) }}" method="POST"
                                  id="deletedRow{{$post->id}}" class="d-none">
                                @method('DELETE')
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        function isActive(id, model) {
            $.ajax({
                url: `{{route('ajax.status')}}`,
                type: 'PUT',
                dataType: 'json',
                data: {
                    _token: '{{csrf_token()}}',
                    id: id,
                    model: model,
                },
                success: function (response) {
                    let text
                    switch (model) {
                        case 'RECRUITMENT_SHOW_COMPANY':    text = 'hiển thị!';break;
                        case 'POST_ACTIVE':                 text = 'hoạt động!';break;
                        case 'POST_COMMENT':                text = 'bình luận!';break;
                    }
                    if (response) {
                        Swal.fire({
                            title: 'Đã bật '+ text + '!',
                            text: "Bạn đã vừa thay đổi trạng thái bài viết!",
                            type: "success",
                            confirmButtonClass: "btn btn-confirm mt-2"
                        })
                    }else {
                        Swal.fire({
                            title: 'Đã tắt '+ text + '!',
                            text: "Bạn đã vừa thay đổi trạng thái bài viết!",
                            type: 'warning',
                            confirmButtonClass: "btn btn-confirm mt-2",
                        })
                    }
                },
                error: function (response) {
                    Swal.fire({
                        type: "error",
                        title: "Lỗi ..!",
                        text: "Hệ thống đang gặp sự cố, vui lòng thử lại sau!",
                        confirmButtonClass: "btn btn-confirm mt-2",
                        footer: '<a href="/admin">Quay về trang chủ</a>'
                    })
                }
            }).done(function (data){
                window.location.reload();
            });
        }
    </script>
@endpush
