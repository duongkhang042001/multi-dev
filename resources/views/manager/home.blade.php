@extends('layouts.dashboard')
@section('page-title', 'Trang chủ')
@section('content')

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">FPT-Internship</a></li>
                    <li class="breadcrumb-item active">Tổng Quan</li>
                </ol>
            </div>
            <h4 class="page-title">FPT-Internship</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-md-6 col-xl-3">
        <div class="card-box tilebox-one">
            <i class="fe-users float-right"></i>
            <h5 class="text-muted text-uppercase mb-3 mt-0">Tổng nhân viên</h5>
            <h3 class="mb-3" data-plugin="counterup">{{$staff}}</h3>

        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card-box tilebox-one">
            <i class="fe-layers float-right"></i>
            <h5 class="text-muted text-uppercase mb-3 mt-0">Tổng doanh nghiệp</h5>
            <h3 class="mb-3"><span data-plugin="counterup">{{$company}}</span></h3>

        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card-box tilebox-one">
            <i class="fe-briefcase float-right"></i>
            <h5 class="text-muted text-uppercase mb-3 mt-0">Tổng sinh viên</h5>
            <h3 class="mb-3"><span data-plugin="counterup">{{$student}}</span></h3>

        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card-box tilebox-one">
            <i class="fe-bell float-right"></i>
            <h5 class="text-muted text-uppercase mb-3 mt-0">Tổng số thông báo</h5>
            <h3 class="mb-3" data-plugin="counterup">{{$notify}}</h3>

        </div>
    </div>
</div>


<div class="row">
    <div class="col-xl-8">
        <div class="card-box">
        <h4 class="header-title">Doanh nghiệp có lượng sinh viên thực tập</h4>
            <table class="tablesaw table mb-0"  data-tablesaw-sortable data-tablesaw-sortable-switch>
                <thead>
                    <tr>
                        <th class="text-center" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="3">Thứ Tự</th>
                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Tên Doanh Nghiệp</th>
{{--                        <th class="text-center" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Sinh viên đang thực tập</th>--}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($companyTopRecruitment as $row)
                    <tr>
                        <td  class="text-center">{{ $loop->iteration }}</td>
                        <td>{{$row->name}}</td>
{{--                        <td class="text-center">--}}
{{--                            {{$row->reports_count}}--}}
{{--                        </td>--}}

                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="card-box">
            <h4 class="header-title">Biểu đồ sinh viên thực tập</h4>

            <canvas id="doughnut" height="450" class="mt-4"></canvas>

        </div>
    </div>
</div>
</div> <!-- end container-fluid -->

@endsection

@push('script')
<script src="assets/libs/chart-js/Chart.bundle.min.js"></script>
<script src="assets/libs/tablesaw/tablesaw.js"></script>
<script type="text/javascript">
const data = {
  labels: [
    'Đạt thực tập',
    'Đang thực tập',
    'Không đạt thực tập',
    'Đã nộp báo cáo',
    'Hủy thực tập',
  ],
  datasets: [{
    label: 'My First Dataset',
    data: [ {{count( $countPassStudents )}}, {{count( $countPendingStudents )}},
    {{count( $countFailStudents )}},{{count( $countDoneStudents )}},{{count($countCancelStudents)}}],
    backgroundColor: [
      'rgb(64, 170, 72)',
      'rgb(255, 205, 86)',
      'rgb(255 ,11, 28)',
      'rgb(10 ,196, 253)',
      'rgb(158 ,148, 150)',

    ],
    hoverOffset: 5
  }]
};
const config = {
  type: 'doughnut',
  data: data,
};
const myChart = new Chart(
    document.getElementById('doughnut'),
    config
  );
</script>
@endpush
@push('css')
<link href="assets/libs/tablesaw/tablesaw.css" rel="stylesheet" type="text/css" />
@endpush
