@extends('layouts.dashboard')
@section('page-title', 'Quản lý thống báo | Tổng quan')
@section('title', 'Quản lý thông báo')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách thông báo</li>
                </ol>
            </div>
            <h4 class="page-title">Danh sách thông báo</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#all" data-toggle="tab">
                            Tất Cả

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#staff" data-toggle="tab">
                            Nhân Viên

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#company" data-toggle="tab">
                            Doanh Nghiệp

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#student" data-toggle="tab">
                            Sinh Viên

                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="card-box">
            <div class="tab-content">
                <div class="tab-pane active" id="all">
                    <table class="table" id="datatable1">
                        @if(count($notifyAll))
                        <thead class=" text-primary">
                            <tr>
                                <th>#</th>
                                <th class="text-center">Tiêu Đề Thông Báo</th>
                                <th class="text-center">Trạng Thái</th>
                                <th class="text-center">Gửi Thông Báo</th>
                                <th>Chức Năng</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($notifyAll as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <a onclick="notify({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl">
                                        {{Str::limit($row->title, 20)}}
                                    </a>
                                </td>
                                <td class="text-center">
                                    @if($row->is_active == 1)
                                    <div class="togglebutton">
                                        <label>
                                            <input type="checkbox" checked="">
                                            <span class="toggle"></span>
                                        </label>
                                    </div>
                                    @else
                                    <div class="togglebutton">
                                        <label>
                                            <input type="checkbox">
                                            <span class="toggle"></span>
                                        </label>
                                    </div>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($row->is_active == 0)
                                    <form action="{{ route('manager.sendNotify', $row->id) }}" method="POST">
                                        @method('POST')
                                        @csrf
                                        <button rel="tooltip" class="btn btn-sm btn-info" onclick="return confirm('Bạn Có muốn gửi???');">
                                            <i class="fab fa-telegram-plane" title="Gửi thông báo"></i>
                                        </button>
                                    </form>
                                    @else
                                    <i class="fas fa-check-circle text-success" title="Đã gửi thông báo"></i>
                                    @endif
                                </td>
                                <td class="td-actions">
                                    @if($row->is_active == 0)
                                    <a href="{{ route('manager.notify.edit', $row->id) }}">
                                        <button type="button" rel="tooltip" class="btn btn-outline-primary mr-1">
                                            <i class="fas fa-pencil-alt"></i>
                                        </button>
                                    </a>
                                    <a href="javascript: document.getElementById('deletedRow{{$row->id}}').submit();" onclick="return confirm('Bạn Có muốn xóa?');" title="xóa">

                                        <button type="button" rel="tooltip" class="btn btn-outline-secondary">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </a>
                                    <form action="{{ route('manager.notify.destroy', $row->id) }}" method="POST" id="deletedRow{{$row->id}}" class="d-none">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <h4>Chưa có dữ liệu!</h4>
                            @endif

                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="staff">
                    <table class="table" id="datatable2">
                        @if(count($notifyStaff))
                        <thead class=" text-primary">
                            <tr>
                                <th>#</th>
                                <th class="text-center">Tiêu Đề Thông Báo</th>
                                <th class="text-center">Trạng Thái</th>
                                <th class="text-center">Gửi Thông Báo</th>
                                <th>Chức Năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($notifyStaff as $row)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                    <a onclick="notify({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl">
                                        {{Str::limit($row->title, 20)}}
                                    </a>
                                </td>
                                <td class="text-center">
                                    @if($row->is_active == 1)
                                    <div class="togglebutton">
                                        <label>
                                            <input type="checkbox" checked="">
                                            <span class="toggle"></span>
                                        </label>
                                    </div>
                                    @else
                                    <div class="togglebutton">
                                        <label>
                                            <input type="checkbox">
                                            <span class="toggle"></span>
                                        </label>
                                    </div>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($row->is_active == 0)
                                    <form action="{{ route('manager.sendNotify', $row->id) }}" method="POST">
                                        @method('POST')
                                        @csrf
                                        <button rel="tooltip" class="btn btn-sm btn-info" onclick="return confirm('Bạn Có muốn gửi???');">
                                            <i class="fab fa-telegram-plane" title="Gửi thông báo"></i>
                                        </button>
                                    </form>
                                    @else
                                    <i class="fas fa-check-circle text-success" title="Đã gửi thông báo"></i>
                                    @endif
                                </td>
                                <td class="td-actions">
                                    @if($row->is_active == 0)
                                    <a href="{{ route('manager.notify.edit', $row->id) }}">
                                        <button type="button" rel="tooltip" class="btn btn-outline-primary mr-1">
                                            <i class="fas fa-pencil-alt"></i>
                                        </button>
                                    </a>
                                    <a href="javascript: document.getElementById('deletedRow{{$row->id}}').submit();" onclick="return confirm('Bạn Có muốn xóa?');" title="xóa">

                                        <button type="button" rel="tooltip" class="btn btn-outline-secondary">
                                            <i class=" fas fa-trash"></i>
                                        </button>
                                    </a>
                                    <form action="{{ route('manager.notify.destroy', $row->id) }}" method="POST" id="deletedRow{{$row->id}}" class="d-none">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                    @endif
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                        @else
                        <h4>Chưa có dữ liệu!</h4>
                        @endif
                    </table>
                </div>
                <div class="tab-pane" id="company">
                    <table class="table" id="datatable3">
                        @if(count($notifyCompany))
                        <thead class=" text-primary">
                            <tr>
                                <th>#</th>
                                <th class="text-center">Tiêu Đề Thông Báo</th>
                                <th class="text-center">Trạng Thái</th>
                                <th class="text-center">Gửi Thông Báo</th>
                                <th>Chức Năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($notifyCompany as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <a onclick="notify({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl">
                                        {{Str::limit($row->title, 20)}}
                                    </a>
                                </td>
                                <td class="text-center">
                                    @if($row->is_active == 1)
                                    <div class="togglebutton">
                                        <label>
                                            <input type="checkbox" checked="">
                                            <span class="toggle"></span>
                                        </label>
                                    </div>
                                    @else
                                    <div class="togglebutton">
                                        <label>
                                            <input type="checkbox">
                                            <span class="toggle"></span>
                                        </label>
                                    </div>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($row->is_active == 0)
                                    <form action="{{ route('manager.sendNotify', $row->id) }}" method="POST">
                                        @method('POST')
                                        @csrf
                                        <button rel="tooltip" class="btn btn-sm btn-info" onclick="return confirm('Bạn Có muốn gửi???');">
                                            <i class="fab fa-telegram-plane" title="Gửi thông báo"></i>
                                        </button>
                                    </form>
                                    @else
                                    <i class="fas fa-check-circle text-success" title="Đã gửi thông báo"></i>
                                    @endif
                                </td>
                                <td class="td-actions">
                                    @if($row->is_active == 0)
                                    <a href="{{ route('manager.notify.edit', $row->id) }}">
                                        <button type="button" rel="tooltip" class="btn btn-outline-primary mr-1">
                                            <i class="fas fa-pencil-alt"></i>
                                        </button>
                                    </a>
                                    <a href="javascript: document.getElementById('deletedRow{{$row->id}}').submit();" onclick="return confirm('Bạn Có muốn xóa?');" title="xóa">

                                        <button type="button" rel="tooltip" class="btn btn-outline-secondary">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </a>
                                    <form action="{{ route('manager.notify.destroy', $row->id) }}" method="POST" id="deletedRow{{$row->id}}" class="d-none">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                    @endif
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                        @else
                        <h4>Chưa có dữ liệu!</h4>
                        @endif
                    </table>
                </div>
                <div class="tab-pane" id="student">
                    <table class="table" id="datatable4">
                        @if(count($notifyStudent))
                        <thead class=" text-primary">
                            <tr>
                                <th>#</th>
                                <th class="text-center">Tiêu Đề Thông Báo</th>
                                <th class="text-center">Trạng Thái</th>
                                <th class="text-center">Gửi Thông Báo</th>
                                <th>Chức Năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($notifyStudent as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <a onclick="notify({{$row->id}})" data-toggle="modal" data-target=".bs-example-modal-xl">
                                        {{Str::limit($row->title, 20)}}
                                    </a>
                                </td>
                                <td class="text-center">
                                    @if($row->is_active == 1)
                                    <div class="togglebutton">
                                        <label>
                                            <input type="checkbox" checked="">
                                            <span class="toggle"></span>
                                        </label>
                                    </div>
                                    @else
                                    <div class="togglebutton">
                                        <label>
                                            <input type="checkbox">
                                            <span class="toggle"></span>
                                        </label>
                                    </div>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($row->is_active == 0)
                                    <form action="{{ route('manager.sendNotify', $row->id) }}" method="POST">
                                        @method('POST')
                                        @csrf
                                        <button rel="tooltip" class="btn btn-sm btn-info" onclick="return confirm('Bạn Có muốn gửi???');">
                                            <i class="fab fa-telegram-plane" title="Gửi thông báo"></i>
                                        </button>
                                    </form>
                                    @else
                                    <i class="fas fa-check-circle text-success" title="Đã gửi thông báo"></i>
                                    @endif
                                </td>
                                <td class="td-actions">
                                    @if($row->is_active == 0)
                                    <a href="{{ route('manager.notify.edit', $row->id) }}">
                                        <button type="button" rel="tooltip" class="btn btn-outline-primary mr-1">
                                            <i class="fas fa-pencil-alt"></i>
                                        </button>
                                    </a>
                                    <a href="javascript: document.getElementById('deletedRow{{$row->id}}').submit();" onclick="return confirm('Bạn Có muốn xóa?');" title="xóa">

                                        <button type="button" rel="tooltip" class="btn btn-outline-secondary">
                                            <i class=" fas fa-trash"></i>
                                        </button>
                                    </a>
                                    <form action="{{ route('manager.notify.destroy', $row->id) }}" method="POST" id="deletedRow{{$row->id}}" class="d-none">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                    @endif
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                        @else
                        <h4>Chưa có dữ liệu</h4>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div id="notifyModel" class="modal-content">

        </div>
    </div>
</div>
@endsection

@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush

@push('script')
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {

        $('#datatable1').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable2').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable2_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable3').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable3_wrapper .col-md-6:eq(0)");
    });
    $(document).ready(function() {
        $('#datatable4').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable4_wrapper .col-md-6:eq(0)");
    });

    function notify(id) {
        $("#notifyModel").html(`
                <div class="modal-header">
                    <h4 class="modal-title text-warning" id="myExtraLargeModalLabel">Đang tải ... <div class="spinner-border "></div> </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            `)
        $.ajax({
            type: 'POST', //THIS NEEDS TO BE GET
            url: `{{route('ajax.notify')}}`,
            data: {
                _token: '{{csrf_token()}}',
                id: id
            },
            success: function(data) {
                $("#notifyModel").html(data)
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
</script>

@endpush