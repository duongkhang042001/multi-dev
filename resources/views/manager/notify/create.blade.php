@extends('layouts.dashboard')
@section('page-title', 'Quản lý thông báo | Tạo mới')
@section('title', 'Thêm Thông Báo')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('manager.notify.index') }}">Danh sách thông báo</a>
                        </li>
                        <li class="breadcrumb-item active">Tạo thông báo</li>
                    </ol>
                </div>
                <h4 class="page-title">Tạo thông báo</h4>
            </div>
        </div>
    </div>
    @push('css')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.bootstrap4.css"
            integrity="sha512-WJ1jnnij6g+LY1YfSmPDGxY0j2Cq/I6PPA7/s4QJ/5sRca5ypbHhFF+Nam0TGfvpacrw9F0OGeZa0ROdNAsaEQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer" />
    @endpush
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-icon card-header-primary">
                            <div class="card-icon">
                                <h4 class="card-title ">Thêm Thông Báo</h4>
                            </div>
                        </div>
                        <div class="card-body ">
                            <form method="POST" onsubmit="getLoading()" action="{{ route('manager.notify.store') }}" class="form-horizontal">
                                @csrf
                                @method('POST')
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Tiêu Đề</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="title"
                                                value="{{ old('title') }}">
                                            @error('title')
                                                <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Nội Dung Thông Báo</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <textarea class="form-control ckeditor" id="content" name="content" rows="10"
                                                value="{{ old('content') }}"></textarea>

                                            @error('content')
                                                <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Đối Tượng</label>
                                    <div class="col-sm-10">
                                        <select name="object" class="form-control" id="ccc" onchange="addressz(this);">
                                            <option value="{{ NOTIFY_ALL }}">Toàn Hệ Thống</option>
                                            <option value="{{ NOTIFY_STAFF }}">Nhân Viên</option>
                                            <option value="{{ NOTIFY_COMPANY }}">Doanh Nghiệp</option>
                                            <option value="{{ NOTIFY_STUDENT }}">Sinh Viên</option>
                                            @error('object')
                                                <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </select>
                                    </div>
                                </div>
                                <div class="row mt-2" id="object_detail">

                                </div>
                                <div class="row form-group text-sm-right pt-sm-5 d-flex align-items-center">
                                    <div class="col-sm-9 checkbox-radios">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label text-primary">
                                                <input class="form-check-input" type="checkbox" value="1" name="is_active">
                                                Gửi Thông Báo Ngay Bây Giờ
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <button class="btn btn-primary btn-block h5" type="submit">Tạo Thông Báo</button>
                                    </div>
                                </div>
                        </div>
                        </form>
                    </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/selectize@0.12.2/dist/js/standalone/selectize.min.js"></script>
    <script type="text/javascript">
        function addressz(a) {
            console.log(a.value);
            moreInformation = document.getElementById('object_detail');
            moreInformation.innerHTML = '';
            if (a.value == {{ NOTIFY_ALL }}) {
                moreInformation.innerHTML = `
                        `;
            } else if (a.value == {{ NOTIFY_STAFF }}) {
                moreInformation.innerHTML = ` <label class="col-sm-2 col-form-label">Đối Tượng Cụ Thể</label>
                                        <div class="col-sm-10">
                                            <select name="object_detail" id="infor" class="form-control" value="{{ old('object_detail') }}">
                                            <option value="0">Tất Cả Nhân Viên</option>
                                            <?php foreach ($staff as $row) {
                                                echo '<option value="' . $row->id . '">' . $row->name . '</option>';
                                            } ?>
                                        </select>
                                            </div>
            `;
            } else if (a.value == {{ NOTIFY_COMPANY }}) {
                moreInformation.innerHTML = ` <label class="col-sm-2 col-form-label">Đối Tượng Cụ Thể</label>
                                        <div class="col-sm-10">
                                            <select name="object_detail" id="infor" class="form-control" value="{{ old('object_detail') }}">
                                            <option value="0">Tất Cả Doanh Nghiệp</option>
                                            <?php foreach ($company as $row) {
                                                echo '<option value="' . $row->id . '">' . $row->name . '</option>';
                                            } ?>
                                        </select>
                                            </div>
            `;
            } else if (a.value == {{ NOTIFY_STUDENT }}) {
                moreInformation.innerHTML = `  
                                    <label class="col-sm-2 col-form-label">Đối Tượng Cụ Thể</label>
                                    <div class="col-sm-10">
                                        <select name="object_detail" id="infor" class="form-control" value="{{ old('object_detail') }}">
                                            <option value="0">Tất Cả Sinh Viên</option>
                                            <option value="1">Sinh viên Đang Thực Tập</option>
                                            <option value="2">Sinh viên Chưa Có Nơi Thực Tập</option>
                                            <option value="3">Sinh Viên Trong kỳ Thực Tập</option>
                                            <option value="4">Sinh Viên Sắp Thực Tập</option>
                                            @error('object_detail')
                                                <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </select>

                                    </div>
                        `;
            }
            $("#infor").selectize({
                create: false,
                sortField: "text",
            });
        }
    </script>
@endpush
