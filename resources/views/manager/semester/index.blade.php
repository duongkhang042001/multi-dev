@extends('layouts.dashboard')
@section('page-title', 'Quản lý kỳ học | Tổng quan')
@section('title', 'Quản lý kỳ học')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Danh sách kỳ học</li>
                </ol>
            </div>
            <h4 class="page-title">Danh sách kỳ học</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-responsive">
                <table id="datatable1" class="table">
                    <thead class=" text-primary">
                        <tr>
                            <th>#</th>
                            <th>Tên Kỳ</th>
                            <th>Băt Đầu Kỳ</th>
                            <th>Kết Thúc Kỳ</th>
                            <th>Mô Tả</th>
                            <th>Chức Năng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($semesters as $key => $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ date('d/m/Y', strtotime($row->from)) }}</td>
                            <td>{{ date('d/m/Y', strtotime($row->to)) }}</td>
                            <td>{{ $row->description }}</td>
                            <td class="td-actions ">
                                <div class="row text-center">
                                @if ($row->to >= now())
                                <a href="{{ route('manager.semester.edit', $row->id) }}" type="button" rel="tooltip" class="btn btn-outline-primary mr-1">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                @endif
                                <?php
                                if ($row->from <= now()) {
                                    $testTime = 0;
                                    $test = 1;
                                } else {
                                    $testTime = 1;
                                    if (isset($test)) {
                                        $testTime = 0;
                                        $test = null;
                                    }
                                }
                                ?>
                                @if ($testTime == 1) 
                              
                                 
                                    <a href="javascript: document.getElementById('deletedRow{{$row->id}}').submit();" onclick="return confirm('Bạn sẽ xóa luôn kỳ thực tập. bạn có chắc chắn không ?')" title="xóa">

                                        <button type="button" rel="tooltip" class="btn btn-outline-secondary">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </a>
                                    <form action="{{ route('manager.semester.destroy', $row->id) }}" method="POST" id="deletedRow{{$row->id}}" class="d-none">
                                        @method('DELETE')
                                        @csrf
                                    </form>

                                </div>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $("#datatable1").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)")
    });
</script>
@endpush