@extends('layouts.dashboard')
@section('page-title', 'Kỳ Học | Tạo mới')
@section('title', 'Trang quản trị')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('manager.semester.index') }}">Danh Sách kỳ học</a>
                        </li>
                        <li class="breadcrumb-item active">Thêm kỳ học</li>
                    </ol>
                </div>
                <h4 class="page-title">Thêm kỳ học</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <form method="POST"  onsubmit="getLoading()" action="{{ route('manager.semester.store') }}">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Tên Kỳ học</label>
                        <div class="col-sm-10">
                            <div class="form-group has-muted bmd-form-group">
                                <input type="text" class="form-control" id="exampleInput3" name="name"
                                    value="{{ old('name') }}">
                                @error('name')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Mô tả</label>
                        <div class="col-sm-10">
                            <div class="form-group has-muted bmd-form-group">
                                <input type="text" class="form-control" id="exampleInput3" name="description"
                                    placeholder="Mô tả kỳ học" value="{{ old('description') }}">
                                @error('description')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Ngày bắt đầu kỳ mới</label>
                        <div class="col-sm-4">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="from" value="{{ old('from') }}">
                                    @error('from')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <label class="col-sm-2 col-form-label">Ngày kết thúc kỳ</label>
                        <div class="col-sm-4">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="to" value="{{ old('to') }}">
                                    @error('to')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Thêm giời gian thực tập</button>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $a = document.getElementsByClassName('bmd-form-group').c
    </script>
@endsection
