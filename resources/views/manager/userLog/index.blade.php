@extends('layouts.dashboard')
@section('page-title', 'Quản lý lịch sử | Tổng quan')
@section('title', 'Lịch sử hoạt động')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Nhật ký hoạt động</li>
                </ol>
            </div>
            <h4 class="page-title">Nhật ký hoạt động</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-responsive">
                <table id="datatable1" class="table">
                    <thead class=" text-primary">
                        <tr>
                            <th>#</th>
                            <th>Nhân Viên</th>
                            <th>Thao Tác</th>
                            <th>Thời Gian</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($userLogs as $key => $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->user->name }}</td>
                            <td>{{$row->action=="POST" ? 'Tạo mới' : ($row->action=="PUT" ? 'Thay đổi' : 'Xóa') }}: {{ $row->target }}</td>
                            <td>
                                @if (date('d/m/Y', strtotime($row->created_at )) == now()->format('d/m/Y'))
                                Hôm nay
                                @else
                                {{date('d/m/Y', strtotime($row->created_at ))}}
                                @endif
                                {{date('H:i', strtotime($row->created_at ))}}
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $("#datatable1").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)")
    });
</script>
@endpush