@extends('layouts.dashboard')
@section('page-title', 'Thực Tập | Tạo mới')
@section('title', 'Trang quản trị')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('manager.timeline.index') }}">Quản lý thời gian thực
                                tập</a></li>
                        <li class="breadcrumb-item active">Tạo thời gian thưc tập</li>
                    </ol>
                </div>
                <h4 class="page-title">Tạo thời gian thực tập</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <form method="POST"  onsubmit="getLoading()" action="{{ route('manager.timeline.store') }}">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <label class="col-sm-3 col-form-label">Kỳ học</label>
                        <div class="col-sm-4">
                            <select name="semester_id" value="" type="text" class="form-control ml-2">
                                <option selected>Vui lòng chọn kỳ</option>
                                @foreach ($semeters as $semeters)
                                    <option value="{{ $semeters['id'] }}">{{ $semeters['name'] }}</option>
                                @endforeach
                            </select>
                            @error('semester_id')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <label class="col-sm-3 col-form-label">Ngày bắt đầu tìm doanh nghiệp</label>
                        <div class="col-sm-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="start_find"
                                        value="{{ old('start_find') }}">
                                    @error('start_find')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <label class="col-sm-3 col-form-label">Ngày kết thúc tìm doanh nghiệp</label>
                        <div class="col-sm-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="end_find"
                                        value="{{ old('end_find') }}">
                                    @error('end_find')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-3 col-form-label">Ngày bắt đầu hỗ trợ</label>
                        <div class="col-sm-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="start_support"
                                        value="{{ old('start_support') }}">
                                    @error('start_support')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <label class="col-sm-3 col-form-label">Ngày kết thúc hỗ trợ</label>
                        <div class="col-sm-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="end_support"
                                        value="{{ old('end_support') }}">
                                    @error('end_support')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                    </div>



                    <div class="row">
                        <label class="col-sm-3 col-form-label">Thông báo danh sách lần 1</label>
                        <div class="col-sm-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="first_notify"
                                        value="{{ old('first_notify') }}">
                                    @error('first_notify')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <label class="col-sm-3 col-form-label">Thông báo danh sách lần 2</label>
                        <div class="col-sm-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="second_notify"
                                        value="{{ old('second_notify') }}">
                                    @error('second_notify')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-3 col-form-label">Bắt đầu thực tập</label>
                        <div class="col-sm-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="start" value="{{ old('start') }}">
                                    @error('start')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror

                                </div>
                            </div>

                        </div>
                        <label class="col-sm-3 col-form-label">Kết thúc thực tập</label>
                        <div class="col-sm-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="date" class="form-control" name="end" value="{{ old('end') }}">
                                    @error('end')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Tạo thời gian thực tập</button>
                </form>
            </div>
        </div>
    @endsection

    @section('script')
        <script>
            $a = document.getElementsByClassName('bmd-form-group').c
        </script>
    @endsection
