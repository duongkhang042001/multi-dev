@extends('layouts.dashboard')
@section('page-title', 'Quản lý timeline | Tổng quan')
@section('title', 'Quản lý thực tập')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item active">Quản lý thời gian thực tập</li>
                </ol>
            </div>
            <h4 class="page-title">Quản lý thời gian thực tập</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-responsive">
                <table id="datatable1" class="table">
                    <thead class=" text-primary">
                        <tr>
                            <th>Tên Kỳ</th>
                            <th>Băt Đầu Tìm Việc</th>
                            <th>Kết Thúc Tìm Việc</th>
                            <th>Băt Đầu Hỗ Trợ</th>
                            <th>Kết Thúc Hỗ Trợ</th>
                            <th>Ngày Băt Đầu</th>
                            <th>Ngày Kết Thúc</th>
                            <th>Chức năng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($timelines as $key => $row)
                        <tr>
                            <td>{{ $row->semester->name }}</td>
                            <td>{{ date('d/m/Y', strtotime($row->start_find)) }}</td>
                            <td>{{ date('d/m/Y', strtotime($row->end_find)) }}</td>
                            <td>{{ date('d/m/Y', strtotime($row->start_support)) }}</td>
                            <td>{{ date('d/m/Y', strtotime($row->end_support)) }}</td>
                            <td>{{ date('d/m/Y', strtotime($row->start)) }}</td>
                            <td>{{ date('d/m/Y', strtotime($row->end)) }}</td>
                            <td class="td-actions">
                                <div class="row text-center">
                                    @if ($row->semester->to >= now())
                                    <a href="{{ route('manager.timeline.edit', $row->id) }}" type="button" rel="tooltip" class="btn btn-outline-primary mr-1">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>
                                    @endif
                                    <?php
                                    if ($row->semester->from <= now()) {
                                        $testTime = 0;
                                        $test = 1;
                                    } else {
                                        $testTime = 1;
                                        if (isset($test)) {
                                            $testTime = 0;
                                            $test = null;
                                        }
                                    }
                                    ?>
                                    @if ($testTime == 1)
                                    <a href="javascript: document.getElementById('deletedRow{{$row->id}}').submit();" onclick="return confirm('Bạn Có muốn xóa?');" title="xóa">

                                        <button type="button" rel="tooltip" class="btn btn-outline-secondary">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </a>
                                    <form action="{{ route('manager.timeline.destroy', $row->id) }}" method="POST" id="deletedRow{{$row->id}}" class="d-none">
                                        @method('DELETE')
                                        @csrf
                                    </form>

                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush

@push('script')
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $("#datatable1").DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            lengthChange: !1,
            buttons: ["copy", "excel"]
        }).buttons().container().appendTo("#datatable1_wrapper .col-md-6:eq(0)")
    });
</script>
@endpush