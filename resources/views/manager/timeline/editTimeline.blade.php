@extends('layouts.dashboard')
@section('page-title', 'Admin | Thực Tập')
@section('title', 'Trang quản trị')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('manager.timeline.index') }}">Quản lý thời thực tập</a></li>
                    <li class="breadcrumb-item active">Chỉnh sửa thời gian thực tập </li>
                </ol>
            </div>
            <h4 class="page-title">Chỉnh sửa thời gian thực tập</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
        <form method="POST"  onsubmit="getLoading()" action="{{route('manager.timeline.update',[$timeline->id])}}">
            @csrf
            @method('PUT')
            <div class="mb-3 ">
                <label class="form-label">Tên kỳ thực tập</label>
                <input type="text" class="form-control" value="{{ $timeline->semester->name }}" disabled>
            </div>
            <div class="mb-3 ">
                <label class="form-label">Ngày bắt đầu tìm doanh nghiệp</label>
                <input type="date" class="form-control" name="start_find" value="{{ date('Y-m-d', strtotime($timeline->start_find)) }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Ngày kết thúc tìm doanh nghiệp</label>
                <input type="date" class="form-control" name="end_find" value="{{ date('Y-m-d', strtotime($timeline->end_find)) }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Bắt đầu hỗ trợ</label>
                <input type="date" class="form-control" name="start_support" value="{{ date('Y-m-d', strtotime($timeline->start_support)) }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Kết thúc hỗ trợ</label>
                <input type="date" class="form-control" name="end_support" value="{{ date('Y-m-d', strtotime($timeline->end_support)) }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Thông báo danh sách lần 1</label>
                <input type="date" class="form-control" name="first_notify" value="{{ date('Y-m-d', strtotime($timeline->first_notify)) }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Thông báo danh sách lần 2</label>
                <input type="date" class="form-control" name="second_notify" value="{{ date('Y-m-d', strtotime($timeline->second_notify)) }}">
            </div>
            <div class="mb-3">
                <label class="form-label">Bắt đầu thực tập</label>
                <input type="date" class="form-control" name="start" value="{{ date('Y-m-d', strtotime($timeline->start))}}">
            </div>
            <div class="mb-3">
                <label class="form-label">Kết thúc thực tập</label>
                <input type="date" class="form-control" name="end" value="{{ date('Y-m-d', strtotime($timeline->end))}}">
            </div>
            <button type="submit" class="btn btn-primary">Chỉnh sửa</button>

        </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<!-- Script You Need -->
@endsection