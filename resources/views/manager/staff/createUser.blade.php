@extends('layouts.dashboard')
@section('page-title', 'Quản lý nhân viên | Tạo mới')
@section('title', 'Quản lý nhân viên')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('manager.staff.index') }}">Danh Sách</a></li>
                        <li class="breadcrumb-item active">Thêm nhân viên</li>
                    </ol>
                </div>
                <h4 class="page-title">Thêm nhân viên</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <form method="POST"  onsubmit="getLoading()" action="{{ route('manager.staff.store') }}" class="form-horizontal">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Họ Và Tên <span class="text-danger"> (*) </span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                                @error('name')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Mã Nhân viên <span class="text-danger"> (*)
                            </span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" id="code" name="code" value="{{ old('code') }}">
                                @error('code')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Email <span class="text-danger"> (*) </span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email"
                                    value="{{ old('email') }}">
                                @error('email')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Email Cá Nhân</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="email" class="form-control" id="email_personal" name="email_personal"
                                    value="{{ old('email_personal') }}">
                                @error('email_personal')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Số Điện Thoại <span class="text-danger"> (*)
                            </span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" id="phone" name="phone"
                                    value="{{ old('phone') }}">
                                @error('phone')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Căn Cước Công Dân <span class="text-danger"> (*)
                            </span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" id="indo" name="indo"
                                    value="{{ old('indo') }}">
                                @error('indo')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Ngày Sinh <span class="text-danger"> (*) </span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="date" class="form-control" id="birthday" name="birthday"
                                    value="{{ old('birthday') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label label-checkbox">Giới Tính <span class="text-danger"> (*)
                            </span></label>
                        <div class="col-sm-10 checkbox-radios">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" value="0" id="gender" name="gender"
                                        checked> Nam
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" value="1" id="gender" name="gender"> Nữ
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Địa Chỉ</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" id="address" name="address"
                                    value="{{ old('address') }}">
                            </div>
                        </div>
                    </div>
                    @livewire('change-address-custom', ['cityId' => old('city'),'districtId' => old('district'),'wardId'
                    =>old('ward')])

                    <button type="submit" class="btn btn-primary">Thêm nhân viên</button>

                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Script You Need -->
    <livewire:scripts />

@endsection
