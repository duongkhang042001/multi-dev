@extends('layouts.dashboard')
@section('page-title', 'Nhân viên | Chỉnh sửa thông tin')
@section('title', 'Trang quản trị')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('manager.staff.index') }}">Danh Sách</a></li>
                        <li class="breadcrumb-item active">Chỉnh sửa nhân viên</li>
                    </ol>
                </div>
                <h4 class="page-title">Chỉnh sửa nhân viên</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <form method="POST" onsubmit="getLoading()" action="{{ route('manager.staff.update', $user->id) }}"
                    class="form-horizontal">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Họ Và Tên</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                                @error('name')
                                    <span class=" text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Mã Nhân viên</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" id="code" name="code" value="{{ $user->code }}">
                                @error('code')
                                    <span class=" text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email"
                                    value="{{ $user->email }}">
                                @error('email')
                                    <span class=" text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Email Cá Nhân</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email_personal"
                                    value="{{ $user->profile->email_personal }}">
                                @error('email_personal')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Số Điện Thoại</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone"
                                    value="{{ $user->profile->phone }}">
                                @error('phone')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">CMND/CCCD </label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" id="indo" name="indo"
                                    value="{{ $user->profile->indo }}">
                                @error('indo')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Ngày Sinh</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="date" class="form-control" id="birthday" name="birthday"
                                    value="{{ date('Y-m-d', strtotime($user->profile->birthday)) }}">
                                @error('birthday')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Giới Tính</label>
                        <div class="col-sm-10">
                            <select name="gender" class="form-control">
                                <option value="0" @if ($user->profile->gender == 0) selected @endif>Nam</option>
                                <option value="1" @if ($user->profile->gender == 1) selected @endif>Nữ</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <label class="col-sm-2 col-form-label">Địa Chỉ</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" id="address" name="address"
                                    value="{{ $user->profile->address }}">
                                @error('address')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    @livewire('change-address-custom', ['cityId' =>$user->profile->city_id,'districtId'
                    =>$user->profile->district_id,'wardId'=>$user->profile->ward_id])
                    <div class="ml-auto mr-auto">
                        <button type="submit" class="btn btn-primary">Thay đổi thông tin</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Script You Need -->
@endsection
