@extends('layouts.dashboard')
@section('page-title', 'Hồ sơ cá nhân | Hồ sơ')
@section('title', 'Hồ sơ nhân viên')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('manager.staff.index') }}">Danh Sách</a></li>
                        <li class="breadcrumb-item active">Chi tiết nhân viên</li>
                    </ol>
                </div>
                <h4 class="page-title">Chi tiết nhân viên</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-5 bg-white">
            <div class="card-header">
                <div class="row mt-3">
                    <div class="col-sm-4">
                        <img src="{{ $staff->avatar }}" onerror="this.src='assets/images/1.png'" class="rounded"
                            id="wizardPicturePreview" title="" width="100" />
                    </div>
                    <div class="col-sm-7">
                        <div class="row text-danger mt-2">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <h4>{{ $staff->name }}</h4>
                                    </div>
                                    <div class="col-sm-2 mt-2">
                                        <i class="fas fa-check-circle text-success"></i>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <span class="font-weight-bold" style="font-size: 12px;">Nhân viên</span>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="font-weight-bold">
                            <span>Email</span>
                        </div>
                        <div class="font-weight-bold mt-2">
                            <span>Mã nhân viên</span>
                        </div>
                        <div class="font-weight-bold mt-4">
                            <span>Trạng thái</span>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="staff-info">
                            <a>{{ $staff->email }}</a>
                        </div>
                        <div class="staff-info mt-2">
                            <a>{{ $staff->code }}</a>
                        </div>
                        <div class="staff-info mt-2">
                            @if ($staff->is_logged == 0)
                                <button class="btn btn-warning btn-round mt-2">Chưa kích hoạt</button>
                            @else
                                <button class="btn btn-success btn-round mt-2">Đã kích hoạt</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3 ml-2 col-sm-4">
                <form action="{{ route('manager.staff.destroy', $staff->id) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button class="btn btn-danger" onclick="return confirm('Bạn Có muốn xóa???');">Xóa Nhân Viên</button>
                </form>
            </div>
        </div>
        <div class="col-6 bg-white ml-auto">
            <div class="card-header text-dark font-weight-bold">
                Thông Tin Cá Nhân
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="staff-input">
                            <label class="mt-2 text-dark">Họ Và Tên:</label>
                        </div>
                        <div class="staff-input">
                            <label class="mt-3 text-dark">Email Cá Nhân:</label>
                        </div>
                        <div class="staff-input">
                            <label class="mt-3 text-dark">Số Điện Thoại:</label>
                        </div>
                        <div class="staff-input">
                            <label class="mt-3 text-dark">CMDN/Căn Cước:</label>
                        </div>
                        <div class="staff-input">
                            <label class="mt-3 text-dark">Giới Tính:</label>
                        </div>
                        <div class="staff-input">
                            <label class="mt-3 text-dark">Ngày Sinh:</label>
                        </div>
                        <div class="staff-input">
                            <label class="mt-3 text-dark">Địa Chỉ:</label>
                        </div>
                        <div class="staff-input">
                            <label class="mt-3 text-dark">Phường/Xã:</label>
                        </div>
                        <div class="staff-input">
                            <label class="mt-3 text-dark">Quận/Huyện:</label>
                        </div>
                        <div class="staff-input">
                            <label class="mt-3 text-dark">Thành Phố/Tỉnh:</label>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="satff-info">
                            <input type="text" class="form-control text-center" value="{{ $staff->name }}" disabled>
                        </div>
                        <div class="satff-info">
                            <input type="text" class="form-control text-center mt-2" value="{{ $profile->email_personal }}"
                                disabled>
                        </div>
                        <div class="satff-info">
                            <input type="text" class="form-control text-center mt-2" value="{{ $profile->phone }}" disabled>
                        </div>
                        <div class="satff-info">
                            <input type="text" class="form-control text-center mt-2" value="{{ $profile->indo }}" disabled>
                        </div>
                        <div class="satff-info">
                            <input type="text" class="form-control text-center mt-2" value="@if ($profile->gender == 0) Nam @elseif ($profile->gender==1) Nữ @endif "
                                disabled>
                        </div>
                        <div class="satff-info">
                            <input type="text" class="form-control text-center mt-2"
                                value="{{ date('d-m-Y', strtotime($profile->birthday)) }}" disabled>
                        </div>
                        <div class="satff-info">
                            <input type="text" class="form-control text-center mt-2" value="{{ $profile->address }}"
                                disabled>
                        </div>
                        <div class="satff-info">
                            <input type="text" class="form-control text-center mt-2" value="{{ $profile->ward_name }}"
                                disabled>
                        </div>
                        <div class="satff-info">
                            <input type="text" class="form-control text-center mt-2" value="{{ $profile->district_name }}"
                                disabled>
                        </div>
                        <div class="satff-info">
                            <input type="text" class="form-control text-center mt-2" value="{{ $profile->city_name }}"
                                disabled>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Script You Need -->
@endsection
