<span style="color: black;">Thư trả lời của P.QHDN đến {{$email}}</span>
<p style="font-size: 5px; line-height: 100%; text-align: center;color:black">&nbsp;</p>

<span style="color: black;">Chào bạn,</span>
<p style="font-size: 5px; line-height: 100%; text-align: center;color:black">&nbsp;</p>

<span style="color: black;">Cảm ơn bạn đã liên hệ đến P.QHDN.</span>
<p style="font-size: 5px; line-height: 100%; text-align: center;color:black">&nbsp;</p>
<span style="color: black;">Nhà trường xin trả lời thắc mắc của bạn :</span>
<p style="font-size: 5px; line-height: 100%; text-align: center;color:black">&nbsp;</p>
<span style="color: black;">{{$content}}</span>
<p style="font-size: 5px; line-height: 100%; text-align: center;color:black">&nbsp;</p>

<span style="color: black;">Thân mến,</span>
<p style="font-size: 5px; line-height: 100%; text-align: center;color:black">&nbsp;</p>
<span style="color: black;">internshipfpt2021@gmail.com</span>
