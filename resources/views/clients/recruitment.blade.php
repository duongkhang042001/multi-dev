@extends('layouts.client_guest')
@section('page-title', $recruitment->post->title)
@section('content')
<!-- Breadcrumb -->
<div class="alice-bg padding-top-70 padding-bottom-70">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="breadcrumb-area">
                    <h1>Chi tiết bài đăng tuyển</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                            <li class="breadcrumb-item"><a href="{{route('recruitment.list')}}">Danh sách</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Chi tiết bài đăng tuyển</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Breadcrumb End -->
<div class="alice-bg section-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="job-listing-details">
                    <div class="job-title-and-info">
                        <div class="title">

                            <div class="thumb">
                                <img src="{{ FILE_URL . $recruitment->company->avatar }}" onerror="this.src='assets/clients/img/job/company-logo-1.png'" class="w-100 h-100" alt="">
                            </div>
                            <div class="title-body">
                                <h4 class="text-primary"><a href="{{ route('recruitment', $recruitment->post->slug) }}">{{ Str::limit($recruitment->post->title, 58) }}</a>
                                </h4>
                                <p class="text-secondary mt-n3" style="font-weight: bold;font-size: 15px;"><a href="{{ route('company.detail', $recruitment->company->slug) }}">{{ Str::limit($recruitment->company->name, 95) }}</a>
                                </p>
                                <div class="info mt-2">
                                    <span class="company"><a><i data-feather="clock"></i>{{ date('d-m-Y', strtotime($recruitment->created_at)) }}</a></span>
                                    <span class="office-location"><a><i data-feather="map-pin"></i>{{ $recruitment->city_name }}</a></span>
                                    @if($recruitment->salary_max)
                                    <span class="job-type full-time"><a><i data-feather="dollar-sign"></i>{{ floatval(number_format($recruitment->salary_min)) }}-{{ floatval(number_format($recruitment->salary_max)) }}
                                            triệu</a></span>
                                    @endif
                                    <span class="office-location"><a><i data-feather="eye"></i>{{ $recruitment->post->view }}</a></span>
                                </div>
                                @if(strtotime($recruitment->date_end) < time())
                                    <div class="info mt-2">
                                        <p class="text-danger ml-2">Bài viết này đã hết hạn ứng tuyển</p>
                                    </div>
                                @endif
                            </div>
                        </div>

                        @if(!Auth::guard('company')->check() && !Auth::guard('staff')->check() && !Auth::guard('manager')->check() && strtotime($recruitment->date_end) > time())
                            @if(Auth::guard('student')->check())
                                @if (Auth::guard('student')->user()->student->is_accept == 1)
                                    <div class="buttons">
                                        @if ($recruitment->is_active == 1)
                                            @if ($recruitmentPostId)
                                                <button class="apply bg-info" onclick="applied()">Đã ứng tuyển</button>
                                            @else
                                                <button class="apply"
                                                    @auth('student')
                                                        @if(!\App\Http\Controllers\Controller::checkTimeLine('find'))
                                                            onclick="warningTimeline()"
                                                        @elseif (!empty(Auth::guard('student')->user()->student->getCV()))
                                                            data-toggle="modal" data-target="#modal-qualification"
                                                        @else
                                                            onclick="checkCV()"
                                                        @endif
                                                    @else onclick="checkLogin()"
                                                    @endauth id="modal-apply">Ứng tuyển ngay
                                                </button>
                                            @endif
                                        @endif
                                    </div>
                                @else
                                    <div class="buttons">
                                        <button class="apply" onclick="checkIsAccept()" id="modal-apply">Ứng tuyển ngay</button>
                                    </div>
                                @endif
                            @else
                                <div class="buttons">
                                    <button class="apply" onclick="checkLogin()" id="modal-apply">Ứng tuyển ngay</button>
                                </div>
                            @endif
                        @endif
                    </div>

                    <div class="details-information section-padding-60">
                        <div class="row">
                            <div class="col-xl-7 col-lg-8">
                                <div class="description details-section">
                                    <h4><i data-feather="align-left"></i>Mô tả công việc</h4>
                                    <p>{!! $recruitment->post->content !!}</p>
                                </div>
                                <div class="edication-and-experience details-section">
                                    <h4><i data-feather="book"></i>Yêu cầu ứng viên</h4>
                                    <p>{!! $recruitment->requirement !!}</p>
                                </div>
                                <div class="other-benifit details-section">
                                    <h4><i data-feather="gift"></i>Phúc lợi</h4>
                                    <ul class="row">
                                        @foreach ($welfares as $key => $welfares)
                                        <?php
                                        $recruitmentWelfare = json_decode($recruitment->welfares);
                                        ?>
                                        @foreach ($recruitmentWelfare as $row)
                                        @if ($row == $welfares->id)
                                        <li class="col-sm-6">{{ $welfares->name }}</li>
                                        @endif
                                        @endforeach
                                        @endforeach
                                    </ul>
                                </div>
                                @if(!Auth::guard('company')->check() && !Auth::guard('staff')->check() && !Auth::guard('manager')->check() && strtotime($recruitment->date_end) > time())
                                    @if(Auth::guard('student')->check())
                                        @if (Auth::guard('student')->user()->student->is_accept == 1)
                                            @if ($recruitment->is_active == 1)
                                                <div class="job-apply-buttons other-benifit details-section">
                                                    <h4><i data-feather="send"></i>Cách thức ứng tuyển</h4>
                                                    <p class="mt-3">Ứng viên nộp hồ sơ trực tuyến bằng cách bấm <span class="text-info" style="font-size: 2.5vh;">Ứng tuyển
                                                            ngay</span>
                                                        dưới đây.</p>
                                                    <div class="buttons">
                                                        @if ($recruitmentPostId)
                                                            <a class="mr-2 bg-info text-white pointer" onclick="applied()">Đã ứng tuyển</a>
                                                        @else
                                                            <a class="bg-primary text-white mr-2 pointer"
                                                                @auth('student')
                                                                    @if(!\App\Http\Controllers\Controller::checkTimeLine('find'))
                                                                        onclick="warningTimeline()"
                                                                    @elseif (!empty(Auth::guard('student')->user()->student->getCV()))
                                                                        data-toggle="modal" data-target="#modal-qualification"
                                                                    @else
                                                                        onclick="checkCV()"
                                                                    @endif
                                                                @else onclick="checkLogin()"
                                                                @endauth id="modal-apply">Ứng tuyển ngay
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <p class="mt-4">Hạn nộp hồ sơ :
                                                        {{ date('d-m-Y', strtotime($recruitment->date_end)) }}
                                                    </p>
                                                </div>
                                            @endif
                                        @else
                                            <div class="job-apply-buttons other-benifit details-section">
                                                <h4><i data-feather="send"></i>Cách thức ứng tuyển</h4>
                                                <p class="mt-3">Ứng viên nộp hồ sơ trực tuyến bằng cách bấm <span class="text-info" style="font-size: 2.5vh;">Ứng tuyển
                                                        ngay</span>
                                                    dưới đây.</p>
                                                <div class="buttons">
                                                    @if ($recruitmentPostId)
                                                    <a class="mr-2 bg-info text-white pointer" onclick="applied()">Đã ứng tuyển</a>
                                                    @else
                                                        <a class="bg-primary text-white mr-2 pointer" onclick="checkIsAccept()" id="modal-apply">Ứng tuyển ngay
                                                        </a>
                                                    @endif
                                                </div>
                                                <p class="mt-4">Hạn nộp hồ sơ :
                                                    {{ date('d-m-Y', strtotime($recruitment->date_end)) }}
                                                </p>
                                            </div>
                                        @endif
                                    @else
                                        <div class="job-apply-buttons other-benifit details-section">
                                            <h4><i data-feather="send"></i>Cách thức ứng tuyển</h4>
                                            <p class="mt-3">Ứng viên nộp hồ sơ trực tuyến bằng cách bấm <span class="text-info h4">Ứng tuyển
                                                    ngay</span>
                                                dưới đây.</p>
                                            <div class="buttons">
                                                @if ($recruitmentPostId)
                                                <a class="mr-2 bg-info text-white pointer" onclick="applied()">Đã ứng tuyển</a>
                                                @else
                                                    <a class="bg-primary text-white mr-2 pointer" onclick="checkLogin()" id="modal-apply">Ứng tuyển ngay
                                                    </a>
                                                @endif
                                            </div>
                                            <p class="mt-4">Hạn nộp hồ sơ :
                                                {{ date('d-m-Y', strtotime($recruitment->date_end)) }}
                                            </p>
                                        </div>
                                    @endif
                                @endif
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12">
                                        <div class="company-information details-section">
                                            @php
                                            $slug = $recruitment->company->slug;
                                            @endphp
                                            <h4><i data-feather="briefcase"></i>Thông tin :<a href="{{ route('company.detail', $slug) }}">
                                                    {{ $recruitment->company->name }}</a></h4>
                                            <ul>
                                                <li><span>Email:</span>{{ $recruitment->company->email }}</li>
                                                <li><span>Số điện thoại:</span>{{ $recruitment->company->phone }}</li>
                                                <li><span>Địa chỉ:</span>{{ $recruitment->company->getAddress() }}</li>

                                                <li><span>Website:</span>
                                                    <a href="{{ json_decode($recruitment->company->url)->url }}">
                                                        {{ json_decode($recruitment->company->url)->url }}
                                                    </a>
                                                </li>
                                                <li><span>Giới thiệu:</span></li>
                                                <li>{!! $recruitment->company->description !!}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 offset-xl-1 col-lg-4">
                                <div class="information-and-share">
                                    <div class="job-summary">
                                        <h4>Thông tin chung</h4>
                                        <ul>
                                            <li><span>Ngành nghề:</span>{{ $recruitment->career->name }}</li>
                                            <li><span>Số lượng tuyển:</span>{{ $recruitment->quantity }}</li>
                                            <li><span>Hình thức làm việc:</span>@if ($recruitment->working_form == 0) Toàn thời gian @else Bán thời gian @endif</li>
                                            <li><span>Nơi làm việc:</span>{{ $recruitment->city_name }}</li>
                                            <li><span>Mức
                                                    lương:</span>@if($recruitment->salary_max) {{ floatval(number_format($recruitment->salary_min)) }}-{{ floatval(number_format($recruitment->salary_max)) }}
                                                triệu @else Thoả thuận @endif</li>
                                            <li><span>Giới tính:</span>@if ($recruitment->gender == 1) Nam @endif Nữ</li>
                                            <li><span>Hạn nộp hồ
                                                    sơ:</span>{{ date('d-m-Y', strtotime($recruitment->date_end)) }}
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="job-summary mt-3">
                                        <h4>Địa điểm làm việc</h4>
                                        <ul>
                                            <li><span>- Khu vực:</span>{{ $recruitment->city_name }}</li>
                                            <li><span>- Địa chỉ:</span>{{ $recruitment->getAddress() }}</li>
                                        </ul>
                                    </div>
                                    <div class="share-job-post">
                                        <span class="share"><i class="fas fa-share-alt"></i>Chia sẻ:</span>
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-linkedin-in"></i></a>
                                        <a href="#"><i class="fab fa-google-plus-g"></i></a>
                                        <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                        <a href="#" class="add-more"><span class="ti-plus"></span></a>
                                    </div>
                                    <!-- <div class="buttons">
                                        <a href="#" class="button print"><i data-feather="printer"></i>Print Job</a>
                                        <a href="#" class="button report"><i data-feather="flag"></i>Report Job</a>
                                    </div> -->
                                    <!-- <div class="job-location">
                                        <h4>Bản đồ</h4>
                                        <div id="map-area">
                                            <div class="cp-map" id="location" data-lat="40.713355" data-lng="-74.005535" data-zoom="10"></div> -->
                                            <!-- <div class="cp-map" id="location" data-lat="40.713355" data-lng="-74.005535" data-zoom="10"></div> -->
                                        <!-- </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if (count($relatedWork) > 0)
                <div class="job-listing-details mt-5">
                    <h5>Việc làm liên quan</h5>
                    <div class="details-information section-padding-60">
                        <div class="job-filter-result">
                            @livewire('list-relate-word',['idRecruitment' => $recruitment->id])
                        </div>
                    </div>
                </div>
                @endif

            </div>
        </div>
    </div>
</div>


<div class="special-qualification dashboard-section details-section">
    <!-- Modal -->
    <div class="modal fade" id="modal-qualification" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="title">
                        <h4><i data-feather="briefcase"></i>Ứng tuyển : {{ $recruitment->post->title }}</h4>
                    </div>
                    <div class="content">
                        @if (empty(Auth::guard('student')->check()))
                        @else
                        <div style="margin-top: 8px; border: 1px dashed #ccc; padding: 5px 10px;">
                            <div class="row">
                                <div class="col-sm-10">
                                    <table>
                                        <tbody>
                                            <tr>
                                                @if (!empty(
                                                Auth::guard('student')->user()->student->getCV()
                                                ))
                                                <th>CV</th>
                                                <td style="padding-left: 5px">: <a href="{{ $filePathPublic }}" target="blank" class="text-highlight text-info">{{ Auth::guard('student')->user()->student->getCV()->name }}<small><i>
                                                                (Click để xem)</i></small></a></td>
                                                @else
                                                <th>Bạn chưa có CV , Vui lòng tạo CV <a href="{{ route('student.resume') }}" class="text-primary">tại đây</a></th>
                                                @endif

                                            </tr>
                                            <tr>
                                                <th>Họ và Tên</th>
                                                <td style="padding-left: 5px">:
                                                    {{ Auth::guard('student')->user()->profile->full_name }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td style="padding-left: 5px">:
                                                    {{ Auth::guard('student')->user()->email }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Số điện thoại</th>
                                                <td style="padding-left: 5px">:
                                                    {{ Auth::guard('student')->user()->profile->phone }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @if (!empty(
                                Auth::guard('student')->user()->student->getCV()
                                ))
                                <div class="col-sm-2">
                                    <a href="{{ route('student.resume') }}" class="add-more ml-auto text-primary mt-2"><i data-feather="edit"></i>
                                        Thay đổi</a>
                                </div>
                                @endif
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="mt-3 form-group">
                        <label>Bạn có thể nhập nguyện vọng của mình khi ứng tuyển bài viết này ! </label></br>
                        <textarea rows="7" min="20" max="500" require class="form-control mt-3" style="outline: none;font-size: 1.5rem;" id="wish" placeholder="Vui lòng nhập nguyện vọng vào đây :"></textarea>
                    </div>
                    <div class="buttons text-right pt-5">
                        @if (!empty($recruitmentPostId))
                        <button class="btn btn-info" style="font-size: 15px;" onclick="applied()">Nộp CV</button>
                        @else
                        <button class="btn btn-info" style="font-size: 15px;" @auth('student') onclick="isActive('{{ $recruitment->id }}')" @else onclick="checkLogin()" @endauth">Nộp
                            CV</button>
                        @endif
                        <button class="btn btn-danger" style="font-size: 15px;" data-dismiss="modal">Đóng</button>
                    </div>
                    <div class="footer">
                        <div class="text-left" style="font-size: 12px; margin-top: 8px; border: 1px dashed #ccc; padding: 5px 10px;">
                            Lưu ý: <br>
                            1. Ứng viên nên lựa chọn ứng tuyển bằng CV Online &amp; viết thêm mong muốn tại phần thư
                            giới thiệu để được Nhà tuyển dụng xem CV sớm hơn. <br>
                            2. Nếu bạn gặp vấn đề khi ứng tuyển vui lòng tải trình duyệt Google Chrome mới nhất <a target="_blank" href="https://www.google.com/chrome/"><strong style="font-weight: bold">tại đây.</strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function isActive(id) {
        let wish = document.getElementById('wish').value;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "{{ route('student.recruitment.store') }}",
            data: {
                _token: '{{ csrf_token() }}',
                id: id,
                wish: wish,
            },
            dataType: "json",
            success: function(data) {
                swal({
                        title: "Ứng tuyển thành công!",
                        text: "Bạn vừa ứng tuyển bài viết này !",
                        buttonsStyling: false,
                        timer: 10000,
                        confirmButtonClass: "btn btn-success",
                        type: "success",
                        confirmButtonText: 'Đồng ý !',

                    }).then(() => window.location.reload())
                    .catch(swal.noop);
            },
            error: function(data) {
                swal({
                    title: "Bạn chưa có CV !",
                    text: "Vui lòng tạo CV để ứng tuyển bài viết này !",
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-warning",
                    type: "warning",
                    confirmButtonText: 'Đồng ý !',
                }).catch(swal.noop)
            }
        });
    }

    function warningTimeline() {
        Swal.fire({
            title: 'Trễ Hạn Thực Tập',
            text: "Bạn đã trễ hạn thực tập, nếu bạn tiếp tục báo cáo thực tập của bạn sẽ được nộp vào kỳ sau!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Tiếp tục",
            cancelButtonText: "Đóng",
            confirmButtonClass: "btn btn-lg btn-warning mt-2",
            cancelButtonClass: "btn btn-lg btn-secondary ml-2 mt-2",
            buttonsStyling: !1
        }).then(function(t) {
            if(t.value) {
                let check = "{{Auth::guard('student')->check() && !empty(Auth::guard('student')->user()->student->getCV())}}";
                check ? $('#modal-qualification').modal('show') : checkCV();
            }
        })
    }
</script>
@endsection
