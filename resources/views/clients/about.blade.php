@extends('layouts.client_guest')
@section('page-title', 'Giới thiệu | FPT Intern')
@section('content')
    <!-- Breadcrumb -->
    <div class="alice-bg padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="breadcrumb-area">
                        <h1>Giới thiệu</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Giới thiệu</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="breadcrumb-form">
                        <form action="#">
                            <input type="text" placeholder="Nhập từ khoá tìm kiếm...">
                            <button><i data-feather="search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <div class="alice-bg section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="white-bg others-block">
                        <div class="about-company">
                            <div class="row">
                                <div class="col-lg-6 order-lg-2">
                                    <div class="feature-thumb">
                                        <img src="/assets/clients/img/feature/thumb-1.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-6 order-lg-1">
                                    <div class="feature-content">
                                        <h3>Hơn 10,00,000 + Việc làm</h3>
                                        <p>
                                            Xã hội ngày càng phát triển, nhu cầu sử dụng các phần mềm công nghệ ngày càng
                                            cao và dường như không thể thiếu trong các tổ chức. Việc sử dụng các phần mềm đó
                                            giúp tăng hiệu quả trong việc xử lý thông tin phức tạp, đem đến sự tiện ích,
                                            nhanh chóng và hiệu quả trong việc điều khiển, quản lý các công việc, nghiệp vụ
                                            riêng của mỗi doanh nghiệp cũng như làm hẹp không gian lưu trữ, cụ thể hóa thông
                                            tin đáp ứng nhu cầu của người sử dụng. Dự án “HỆ THỐNG QUẢN LÝ THỰC TẬP SINH
                                            FPT” của chúng tôi ra đời nhằm đáp ứng những yêu cầu thực tiễn của trường nhằm
                                            khắc phục những hạn chế và nâng cao khả năng quản lý, tăng khả năng lưu trữ
                                            thông tin, thúc đẩy sự tham gia của các doanh nghiệp trong quá trình tìm kiếm
                                            nguồn nhân lực chất lượng và hỗ trợ tốt hơn trong quá trình triển khai các giai
                                            đoạn thực tập cho sinh viên trường “FPT Polytechnic”.
                                        </p>
                                        <a href="{{ route('home') }}" class="button">Bắt đầu ngay</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="infobox-wrap">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="info-box">
                                        <div class="icon">
                                            <i data-feather="cast"></i>
                                        </div>
                                        <h4>Quảng cáo một công việc</h4>
                                        <p>Khám phá nhiều trang web vẫn còn sơ khai. Nhiều phiên bản khác nhau đã phát triển
                                            những năm, đôi khi tình cờ.</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="info-box">
                                        <div class="icon">
                                            <i data-feather="layout"></i>
                                        </div>
                                        <h4>Hồ sơ nhà tuyển dụng</h4>
                                        <p>Khám phá nhiều trang web vẫn còn sơ khai. Nhiều phiên bản khác nhau đã phát triển
                                            những năm, đôi khi tình cờ.</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="info-box">
                                        <div class="icon">
                                            <i data-feather="compass"></i>
                                        </div>
                                        <h4>Tìm công việc mơ ước của bạn</h4>
                                        <p> Khám phá nhiều trang web vẫn còn sơ khai. Nhiều phiên bản khác nhau đã phát triển
                                            những năm, đôi khi tình cờ.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Fun Facts -->
                        <div class="padding-top-90 padding-bottom-60 fact-bg">
                            <div class="container">
                                <div class="row fact-items">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="fact">
                                            <div class="fact-icon">
                                                <i data-feather="briefcase"></i>
                                            </div>
                                            <p class="fact-number"><span class="count" data-form="0"
                                                    data-to="12376"></span></p>
                                            <p class="fact-name">Công việc</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="fact">
                                            <div class="fact-icon">
                                                <i data-feather="users"></i>
                                            </div>
                                            <p class="fact-number"><span class="count" data-form="0"
                                                    data-to="89562"></span></p>
                                            <p class="fact-name">Ứng viên</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="fact">
                                            <div class="fact-icon">
                                                <i data-feather="file-text"></i>
                                            </div>
                                            <p class="fact-number"><span class="count" data-form="0"
                                                    data-to="28166"></span></p>
                                            <p class="fact-name">Hồ sơ</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="fact">
                                            <div class="fact-icon">
                                                <i data-feather="award"></i>
                                            </div>
                                            <p class="fact-number"><span class="count" data-form="0"
                                                    data-to="1366"></span></p>
                                            <p class="fact-name">Doanh nghiệp</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
