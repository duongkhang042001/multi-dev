@extends('layouts.client')
@section('page-title', 'Thông báo')
@section('title', 'Thông báo')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Sinh viên</a></li>
            <li class="breadcrumb-item active" aria-current="page">Thông báo</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="dashboard-content-wrapper">
        <div class="dashboard-section dashboard-recent-activity">
            <?php
            $msPerMinute = 60;
            $msPerHour = $msPerMinute * 60;
            $msPerDay = $msPerHour * 24;
            $msPerMonth = $msPerDay * 30;
            $msPerYear = $msPerDay * 365;
            ?>
            <h4 class="title">
                Danh sách thông báo
            </h4>
            @if (!empty($notifyDetails) && count($notifyDetails) == 0)
                <p class="p-3 text-sm">Chưa có thông báo.</p>
            @else
                @foreach ($notifyDetails as $item)
                    <div class="activity-list pointer" onclick="javascript:location.href='{{ $item->notify->url }}'">
                        <i class="fas fa-bell"></i>
                        <div class="content">
                            <h5>{!! $item->notify->title !!}</h5>
                            <span class="time">
                                <?php
                                $elapsed = time() - strtotime($item->created_at);
                                if ($elapsed < $msPerMinute) {
                                    echo ceil($elapsed) . ' giây trước';
                                } elseif ($elapsed < $msPerHour) {
                                    echo ceil($elapsed / $msPerMinute) . ' phút trước';
                                } elseif ($elapsed < $msPerDay) {
                                    echo ceil($elapsed / $msPerHour) . ' giờ trước';
                                } elseif ($elapsed < $msPerMonth) {
                                    echo ceil($elapsed / $msPerDay) . ' ngày trước';
                                } elseif ($elapsed < $msPerYear) {
                                    echo ceil($elapsed / $msPerMonth) . ' tháng trước';
                                } else {
                                    echo ceil($elapsed / $msPerYear) . ' năm trước';
                                }
                                ?>
                            </span>
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="pagination-list text-center">
                <nav class="navigation pagination">
                    <div class="nav-links">
                        @if ($notifyDetails->lastPage() > 1)
                            <a class="prev page-numbers" href="{{ $notifyDetails->url(1) }}"><i
                                    class="fas fa-angle-left"></i></a>
                            @for ($i = 1; $i <= $notifyDetails->lastPage(); $i++)
                                <?php
                                $half_total_links = floor(5 / 2);
                                $from = $notifyDetails->currentPage() - $half_total_links;
                                $to = $notifyDetails->currentPage() + $half_total_links;
                                if ($notifyDetails->currentPage() < $half_total_links) {
                                    $to += $half_total_links - $notifyDetails->currentPage();
                                }
                                if ($notifyDetails->lastPage() - $notifyDetails->currentPage() < $half_total_links) {
                                    $from -= $half_total_links - ($notifyDetails->lastPage() - $notifyDetails->currentPage()) - 1;
                                }
                                ?>
                                @if ($from < $i && $i < $to)
                                    @if ($notifyDetails->currentPage() == $i)
                                        <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                    @else
                                        <a class="page-numbers"
                                            href="{{ $notifyDetails->url($i) }}">{{ $i }}</a>
                                    @endif


                                @endif
                            @endfor

                            <a class="next page-numbers" href="{{ $notifyDetails->url($notifyDetails->lastPage()) }}"><i
                                    class="fas fa-angle-right"></i></a>
                        @endif
                    </div>
                </nav>
            </div>
        </div>
    </div>
@endsection
@push('script')

@endpush
