@extends('layouts.client_guest')
@section('page-title', 'Danh sách tìm kiếm việc làm')
@section('content')
    <!-- Breadcrumb -->
    <div class="alice-bg padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="breadcrumb-area">
                        <h1><a href="{{ route('search-recruitment.list') }}">Danh sách tìm kiếm công việc</a></h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a
                                        href="{{ route('search-recruitment.list') }}">Danh sách tìm kiếm công việc</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="breadcrumb-form">
                        <a href="{{ route('home') }}" class="text-primary float-right d-flex align-items-center"><i
                                data-feather="arrow-left"></i> <span> Quay về trang chủ</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Breadcrumb End -->
    <!-- Job Listing -->
    <div class="alice-bg section-padding-bottom">
        <div class="container">
            <div class="row no-gutters">
                <div class="col">
                    <div class="filtered-job-listing-wrapper">
                        <div class="job-view-controller-wrapper">
                            <div class="job-view-controller">
                                <div class="controller list active">
                                    <i data-feather="menu"></i>
                                </div>
                                <div class="controller grid">
                                    <i data-feather="grid"></i>
                                </div>
                            </div>
                        </div>
                        <div class="job-filter-result">

                            @if ($searchRecruitment)
                                @foreach ($searchRecruitment as $key => $row)
                                    <div class="job-list">
                                        <div class="thumb">
                                            <a href="{{ route('recruitment', $row->post->slug) }}">
                                                <img src="{{ FILE_URL . $row->company->avatar }}"
                                                    onerror="this.src='assets/clients/img/job/company-logo-1.png'"
                                                    class="w-100 h-100" alt="">
                                            </a>
                                        </div>
                                        <div class="body">
                                            <div class="content">
                                                <h4><a
                                                        href="{{ route('recruitment', $row->post->slug) }}">{{ Str::limit($row->post->title, 80) }}</a>
                                                </h4>
                                                <div class="info">
                                                    <span class="office-location"><a href="#"><i
                                                                class="fas fa-map-marker-alt px-2"></i>{{ $row->city_name }}</a></span>
                                                    <span class="job-type temporary"><a href="#"><i
                                                                class="fas fa-dollar-sign px-2"></i>{{ floatval(number_format($row->salary_min)) }}-{{ floatval(number_format($row->salary_max)) }}
                                                            triệu</a></span>
                                                    <span class="office-location"><a href="#"><i
                                                                class="fas fa-eye px-2"></i>{{ $row->post->view }}</a></span>
                                                </div>
                                            </div>
                                            <div class="more">
                                                <div class="buttons">
                                                    <a href="{{ route('recruitment', $row->post->slug) }}"
                                                        class="button">Xem ngay</a>
                                                    <a href="#" class="favourite"><i
                                                            class="fas fa-heart text-danger"></i></i></a>
                                                </div>

                                                @if (strtotime($row->date_end) > time())
                                                    <p class="deadline">Hết hạn:
                                                        {{ date('d-m-Y', strtotime($row->date_end)) }}</p>
                                                @else
                                                    <p class="deadline text-danger mr-5">Đã hết hạn</p>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            @if (count($searchRecruitment) == 0)
                                <p class="font-weight-bold text-center mt-5">Không tìm thấy việc làm phù hợp với yêu cầu của
                                    bạn.</p>
                            @endif
                        </div>
                        <div class="pagination-list text-center">
                            <nav class="navigation pagination">
                                <div class="nav-links">
                                    @if ($searchRecruitment->lastPage() > 1)
                                        <a class="prev page-numbers" href="{{ $searchRecruitment->url(1) }}"><i
                                                class="fas fa-angle-left"></i></a>
                                        @for ($i = 1; $i <= $searchRecruitment->lastPage(); $i++)
                                            <?php
                                            $half_total_links = floor(5 / 2);
                                            $from = $searchRecruitment->currentPage() - $half_total_links;
                                            $to = $searchRecruitment->currentPage() + $half_total_links;
                                            if ($searchRecruitment->currentPage() < $half_total_links) {
                                                $to += $half_total_links - $searchRecruitment->currentPage();
                                            }
                                            if ($searchRecruitment->lastPage() - $searchRecruitment->currentPage() < $half_total_links) {
                                                $from -= $half_total_links - ($searchRecruitment->lastPage() - $searchRecruitment->currentPage()) - 1;
                                            }
                                            ?>
                                            @if ($from < $i && $i < $to)
                                                @if ($searchRecruitment->currentPage() == $i)
                                                    <span aria-current="page"
                                                        class="page-numbers current">{{ $i }}</span>
                                                @else
                                                    <a class="page-numbers"
                                                        href="{{ $searchRecruitment->url($i) }}">{{ $i }}</a>
                                                @endif


                                            @endif
                                        @endfor

                                        <a class="next page-numbers"
                                            href="{{ $searchRecruitment->url($searchRecruitment->lastPage()) }}"><i
                                                class="fas fa-angle-right"></i></a>
                                    @endif
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Job Listing End -->
@endsection
