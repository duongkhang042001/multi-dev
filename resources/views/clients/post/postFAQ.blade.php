@extends('layouts.client_guest')
@section('page-title', 'Câu hỏi thường gặp | FPT Intern')
@section('content')
<!-- Breadcrumb -->
<div class="alice-bg padding-top-70 padding-bottom-70">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="breadcrumb-area">
                    <h1>Câu hỏi thường gặp</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Câu hỏi thường gặp</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6">
                <div class="breadcrumb-form">
                    <form action="#">
                        <input type="text" placeholder="Nhập từ khoá tìm kiếm...">
                        <button><i data-feather="search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<div class="alice-bg section-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col">

                @foreach ($postFAQ as $row)
                <div class="blog-list">
                    <div class="info">
                        <img src="{{$row->getThumbnail()}}" width="150" height="100" onerror="this.src='/assets/clients/img/logo-default.png'">
                    </div>
                    <div class="content">
                        <h3><a href="{{ route('postDetail', $row->slug) }}">{{ Str::limit($row->title, 70) }}</a>
                        </h3>
                        <p>{!! Str::limit($row->description, 250) !!} </p>
                    </div>
                    <div class="read-more">
                        <a href="{{ route('postDetail', $row->slug) }}" class="button">Chi tiết</a>
                    </div>
                </div>
                @endforeach
                <div class="pagination-list text-center">
                    <nav class="navigation pagination">
                        <div class="nav-links">
                            @if ($postFAQ->lastPage() > 1)
                            <a class="prev page-numbers" href="{{ $postFAQ->url(1) }}"><i class="fas fa-angle-left"></i></a>
                            @for ($i = 1; $i <= $postFAQ->lastPage(); $i++)
                                <?php
                                $half_total_links = floor(5 / 2);
                                $from = $postFAQ->currentPage() - $half_total_links;
                                $to = $postFAQ->currentPage() + $half_total_links;
                                if ($postFAQ->currentPage() < $half_total_links) {
                                    $to += $half_total_links - $postFAQ->currentPage();
                                }
                                if ($postFAQ->lastPage() - $postFAQ->currentPage() < $half_total_links) {
                                    $from -= $half_total_links - ($postFAQ->lastPage() - $postFAQ->currentPage()) - 1;
                                }
                                ?>
                                @if ($from < $i && $i < $to) @if ($postFAQ->currentPage() == $i)
                                    <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                    @else
                                    <a class="page-numbers" href="{{ $postFAQ->url($i) }}">{{ $i }}</a>
                                    @endif


                                    @endif
                                    @endfor

                                    <a class="next page-numbers" href="{{ $postFAQ->url($postFAQ->lastPage()) }}"><i class="fas fa-angle-right"></i></a>
                                    @endif
                        </div>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection