@extends('layouts.client_guest')
@section('page-title', $postDetail->title)
@section('content')
    <!-- Breadcrumb -->
    <div class="alice-bg padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="breadcrumb-area">
                        <h1>Chi tiết bài viết</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Chi tiết bài viết</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="breadcrumb-form">
                        <form action="#">
                            <input type="text" placeholder="Nhập từ khoá tìm kiếm...">
                            <button><i data-feather="search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <div class="alice-bg section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="blog-post-details-container">
                        <div class="blog-details-wrapper">
                            <div class="info">
                                <span class="date"><i
                                        data-feather="calendar"></i>{{ date('d/m/Y', strtotime($postDetail->created_at)) }}</span>
                                <span class="author"><i
                                        data-feather="user"></i>{{ $postDetail->user->profile->last_name }}</a></span>
                                <span class="comments"><i
                                        data-feather="eye"></i>{{ number_format($postDetail->view) }}</span>
                            </div>
                            <div class="post-content">
                                <h2>{{ $postDetail->title }}</h2>
                                <p>{!! $postDetail->description !!}</p>
                                <p>{!! $postDetail->content !!}</p>
                            </div>
                        </div>

                        <div class="post-meta">
                            <div class="post-author">
                                <div class="avatar">
                                    <img src="{{ FILE_URL . $postDetail->user->avatar }}"
                                        onerror="this.src='assets/clients/img/avt-default.png'" class="img-fluid"
                                        alt="">
                                </div>
                                <div class="name">
                                    <p>Đăng bởi</p>
                                    <h5><a href="#">{{ $postDetail->user->name }}</a></h5>
                                </div>
                            </div>
                            <div class="post-tag">
                                <h6>Thẻ</h6>
                                <div class="tags">
                                    <a href="#">Công nghệ thông tin</a>
                                    <a href="#">Thiết kế</a>
                                    <a href="#">Marketing</a>
                                </div>
                            </div>
                            <div class="post-tag w-100">
                                <h6>Cộng đồng</h6>
                                <div class="social-buttons">
                                    <div class="fb-like" data-href="{{ url()->current() }}" data-width="300px"
                                        data-layout="button_count" data-action="like" data-size="large" data-share="true">
                                    </div>

                                </div>
                            </div>
                        </div>
                        @if ($postDetail->is_comment == 1)
                            @if (Auth::guard('student')->check() || Auth::guard('company')->check() || Auth::guard('staff')->check() || Auth::guard('manager')->check())
                                @livewire('comments',['idPost' => $postDetail->id])
                            @else
                                <div class="post-comment-block">
                                    <div class="comment-respond" style="min-height: 30px;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>Vui lòng <a href="javascript:void(checkAuth())"
                                                        class="font-weight-bold">đăng
                                                        nhập</a> để bình luận</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif



                        <div class="blog-details-wrapper">

                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div class="section-header">
                                            <h2>Bài viết liên quan</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="company-carousel owl-carousel">
                                            @if ($postDetail->post_type_id == 1)
                                                @foreach ($relatedPost as $key => $row)
                                                    <div class="company-wrap">
                                                        @php
                                                            $slug = $row->slug;
                                                        @endphp
                                                        <div class="thumb">
                                                            <a href="{{ route('postDetail', $row->slug) }}">
                                                                <img src="{{ $row->getThumbnail() }}"
                                                                    onerror="this.src='assets/clients/img/logo-default.jpg'"
                                                                    class="w-100" height="80" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="body">
                                                            <h4><a
                                                                    href="{{ route('postDetail', $row->slug) }}">{{ Str::limit($row->title, 30) }}</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @elseif($postDetail->post_type_id == 2)
                                                @foreach ($relatedPostFAQ as $key => $row)
                                                    <div class="company-wrap">
                                                        @php
                                                            $slug = $row->slug;
                                                        @endphp
                                                        <div class="thumb">
                                                            <a href="{{ route('postDetail', $row->slug) }}">
                                                                <img src="{{ $row->getThumbnail() }}"
                                                                    onerror="this.src='assets/clients/img/logo-default.jpg'"
                                                                    class="w-100" height="80" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="body">
                                                            <h4>
                                                                <a href="{{ route('postDetail', $row->slug) }}">{{ Str::limit($row->title, 30) }}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="fb-root"></div>
@endsection
@push('meta')
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $postDetail->title }}" />
    <meta property="og:description" content="{{ strip_tags($postDetail->description) }}" />
    <meta property="og:image" content="{{ $postDetail->getThumbnail() }}" />
@endpush
@push('script')
    <!-- Load Facebook SDK for JavaScript -->

    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v12.0"
        nonce="7nioKls3"></script>
@endpush
