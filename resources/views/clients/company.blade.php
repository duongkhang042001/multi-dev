@extends('layouts.client_guest')
@section('page-title', 'Thông tin doanh nghiệp và tin tuyển dụng từ doanh nghiệp | FPT Intern')
@section('content')

<div class="alice-bg padding-top-60 section-padding-bottom">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="candidate-details">
          <div class="title-and-info">
            <img src="{{FILE_URL .$company->banner}}" onerror="this.src='assets/clients/img/banner-default.jpg'" class="w-100 shadow-sm rounded-lg" style="height: 350px;object-fit: cover;" />
          </div>
          <div class="row align-content-end" style="margin-left: 1px;margin-right:1px;background-color: #f1f9fd;padding: 20px;">
            <div class="col-sm-2 d-sm-block" style="position: relative; width: 135px;height: 125px;margin-top: 30px;">
              <img src="{{FILE_URL .$company->avatar}}" onerror="this.src='assets/img/default-avatar.jpg'" class="w-100 position-absolute shadow-sm" style="object-position: 50% 50%;">
            </div>
            <div class="col-sm-10 pl-sm-5 text-left">
              <div class="text-dark ml-4">
                <h4 class="mt-4 text-dark">{{$company->name}}</h4>
                <p class="mt-4">
                  <a href="#" class="mr-5"><i class="fas fa-user-tie mr-3"></i>Người liên hệ : @if($company->manager) {{$company->manager->name}} @else không có @endif</a>
                  <a href="" class="mr-5"><i class="fas fa-user-friends mr-3"></i>Qui mô doanh nghiệp : {{count($countStaff)}} nhân viên</a>
                  <a href="" class="mr-5"><i class="fas fa-envelope mr-3"></i>Email : {{$company->email}}</a>
                </p>
                <p class="mt-4">
                  <a href="{{json_decode($company->url)->url}}" class="mr-5"><i class="fas fa-globe mr-3"></i>
                    Website : {{json_decode($company->url)->url}}
                  </a>
                  <a href="" class="mr-5"><i class="fas fa-phone mr-3"></i>Số điện thoại : {{$company->phone}}</a>
                  <a href="" class="mr-5"><i class="fas fa-book-open mr-3"></i>Số bài đăng tuyển : {{count($company->recruitmentPosts)}}</a>
                </p>
                <hr />
                <p class="mt-4">
                  <a href="#" class="mr-5" style="font-weight: bold;font-size: 1.7rem;"><i class="fas fa-map-marker-alt mr-3"></i>Địa chỉ : {{$company->getAddress()}}</a>
                </p>
              </div>
            </div>
          </div>
          <div class="details-information section-padding-60 box-company-info">
            <div class="about-details details-section">
              <h4><i data-feather="align-left"></i>GIỚI THIỆU DOANH NGHIỆP</h4>
              <p>{!!$company->description!!}</p>
            </div>
            <div class="edication-background details-section">
              <div class="row d-flex ml-2">
                <h4 class="mr-2 mt-1"><i data-feather="book"></i>VIỆC LÀM ĐANG TUYỂN</h4>
                <h4><span class="badge badge-info">{{count($company->recruitmentPosts)}}</span></h4>
              </div>
              @if($company->recruitmentPosts)
                @livewire('list-recruitment-company',['idCompany' => $company->id])
              @endif
              @if(count($company->recruitmentPosts) == 0)
                <p> ( Chưa có bài đăng tuyển ) </p>
              @endif

              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
