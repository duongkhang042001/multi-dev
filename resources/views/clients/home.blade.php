@extends('layouts.client_guest')
@section('page-title', 'FPT Internship')
@section('content')
    <!-- Banner -->
    <div class="banner banner-1 banner-1-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner-content">
                        <h1>58,246 Việc làm </h1>
                        <p>Hiên đang có rất nhiều Việc Làm & Cơ hội Nghề nghiệp xung quanh bạn. Hãy bắt đầu thể hiện mình đi
                            nào!</p>
                        <a href="{{ route('recruitment.list') }}" class="button">Bắt Đầu ngay!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner End -->

    <!-- Search and Filter -->
    <div class="searchAndFilter-wrapper">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="searchAndFilter-block">
                        <div class="searchAndFilter">
                            <form action="{{ route('search-recruitment.list') }}" class="search-form" method="POST">
                                @csrf
                                <input type="text" placeholder="Từ khóa tìm kiếm" name="keyword">
                                <select class="selectpicker" id="search-location" name="address">
                                    <option value="" selected>Địa điểm</option>
                                    @foreach ($cities as $row)
                                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                                    @endforeach
                                </select>
                                <select class="selectpicker" id="search-category" name="career">
                                    <option value="" selected>Nhóm ngành</option>
                                    @foreach ($careersEdu as $row)
                                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                                    @endforeach
                                </select>
                                <button class="button primary-bg"><i class="fas fa-search"></i>Tìm Kiếm</button>
                            </form>
                            <div class="filter-categories">
                                <h4>Ngành của trường</h4>
                                <ul>
                                    @foreach ($careersEdu as $row)
                                        <li><a href="{{ route('list-career', ['career', $row->slug]) }}"><i
                                                    data-feather="git-commit"></i>{{ $row->name }}
                                            </a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search and Filter End -->

    <!-- Jobs -->
    <div class="section-padding-bottom alice-bg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="nav nav-tabs job-tab" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="recent-tab" data-toggle="tab" href="#recent" role="tab"
                                aria-controls="recent" aria-selected="true">Gần Đây</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="feature-tab" data-toggle="tab" href="#feature" role="tab"
                                aria-controls="feature" aria-selected="false">Nổi Bật</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="recent" role="tabpanel" aria-labelledby="recent-tab">
                            <div class="row">
                                @foreach ($recruitmentRecently as $key => $row)
                                    <div class="col-lg-6">
                                        <div class="job-list half-grid">
                                            <div class="thumb">
                                                <a href="{{ route('recruitment', $row->post->slug) }}">
                                                    <img src="{{ $row->company->getAvatar() }}"
                                                        onerror="this.src='assets/clients/img/job/company-logo-1.png'"
                                                        class="w-100" alt="">
                                                </a>
                                            </div>
                                            <div class="body">
                                                <div class="content">
                                                    <h4><a
                                                            href="{{ route('recruitment', $row->post->slug) }}">{{ $row->post->title }}</a>
                                                    </h4>
                                                    <div class="info">
                                                        <span class="office-location"><a href="#"><i
                                                                    data-feather="map-pin"></i>{{ $row->city_name }}</a></span>
                                                        <span class="job-type temporary"><a href="#"><i
                                                                    data-feather="dollar-sign"></i>{{ floatval(number_format($row->salary_min)) }}-{{ floatval(number_format($row->salary_max)) }}
                                                                triệu</a></span>
                                                        <span class="office-location"><a href="#"><i
                                                                    data-feather="eye"></i>{{ $row->post->view }}</a></span>
                                                    </div>
                                                </div>
                                                <div class="more">
                                                    <div class="buttons">
                                                        <a href="#" class="button">Apply Now</a>
                                                        <a href="#" class="favourite"><i data-feather="heart"></i></a>
                                                    </div>
                                                    <p class="deadline">Deadline: Oct 31, 2018</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="tab-pane fade" id="feature" role="tabpanel" aria-labelledby="feature-tab">
                            <div class="row">
                                @foreach ($recruitmentHighlight as $key => $row)
                                    <div class="col-lg-6">
                                        <div class="job-list half-grid">
                                            <div class="thumb">
                                                <a href="{{ route('recruitment', $row->slug) }}">
                                                    <img src="{{ $row->recruitmentPost->company->getAvatar() }}"
                                                        onerror="this.src='assets/clients/img/job/company-logo-1.png'"
                                                        class="w-100" alt="">
                                                </a>
                                            </div>
                                            <div class="body">
                                                <div class="content">
                                                    <h4><a
                                                            href="{{ route('recruitment', $row->slug) }}">{{ $row->title }}</a>
                                                    </h4>
                                                    <div class="info">
                                                        <span class="office-location"><a href="#"><i
                                                                    data-feather="map-pin"></i>{{ $row->recruitmentPost->city_name }}</a></span>
                                                        <span class="job-type temporary"><a href="#"><i
                                                                    data-feather="dollar-sign"></i>{{ floatval(number_format($row->recruitmentPost->salary_min)) }}-{{ floatval(number_format($row->recruitmentPost->salary_max)) }}
                                                                triệu</a></span>
                                                        <span class="office-location"><a href="#"><i
                                                                    data-feather="eye"></i>{{ $row->view }}</a></span>
                                                    </div>
                                                </div>
                                                <div class="more">
                                                    <div class="buttons">
                                                        <a href="#" class="button">Apply Now</a>
                                                        <a href="#" class="favourite"><i data-feather="heart"></i></a>
                                                    </div>
                                                    <p class="deadline">Deadline: Oct 31, 2018</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Jobs End -->

    <!-- Top Companies -->
    <div class="section-padding-top padding-bottom-90">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section-header">
                        <h2>Các Doanh Nghiệp Hàng Đầu</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="company-carousel owl-carousel">
                        @foreach ($topCompany as $key => $row)
                            <div class="company-wrap">
                                @php
                                    $slug = $row->slug;
                                @endphp
                                <div class="thumb">
                                    <a href="{{route('company.detail',$slug)}}">
                                        <img src="{{$row->getAvatar()}}" class="w-100" height="70px" width="70px" alt="" style=" object-fit: contain;">
                                    </a>
                                </div>
                                <div class="body">
                                    @if (!empty($row->short_name))
                                        <h4><a
                                                href="{{ route('company.detail', $slug) }}">{{ Str::limit($row->short_name, 15) }}</a>
                                        </h4>
                                    @else
                                        <h4><a
                                                href="{{ route('company.detail', $slug) }}">{{ Str::limit($row->name, 15) }}</a>
                                        </h4>
                                    @endif
                                    <span>{{ Str::limit($row->getAddress(), 18) }}</span>
                                    <a href="{{ route('company.detail', $slug) }}"
                                        class="button">{{ count($row->recruitmentPosts) }} bài viết</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Top Companies End -->

    <!-- Fun Facts -->
    {{-- <div class="padding-top-90 padding-bottom-60 fact-bg">
        <div class="container">
            <div class="row fact-items">
                <div class="col-md-3 col-sm-6">
                    <div class="fact">
                        <div class="fact-icon">
                            <i data-feather="briefcase"></i>
                        </div>
                        <p class="fact-number"><span class="count" data-form="0" data-to="{{$recruitmentTotal}}"></span></p>
                        <p class="fact-name">Ứng tuyển</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="fact">
                        <div class="fact-icon">
                            <i data-feather="users"></i>
                        </div>
                        <p class="fact-number"><span class="count" data-form="0" data-to="89562"></span></p>
                        <p class="fact-name">Ứng viên</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="fact">
                        <div class="fact-icon">
                            <i data-feather="file-text"></i>
                        </div>
                        <p class="fact-number"><span class="count" data-form="0" data-to="28166"></span></p>
                        <p class="fact-name">CV</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="fact">
                        <div class="fact-icon">
                            <i data-feather="award"></i>
                        </div>
                        <p class="fact-number"><span class="count" data-form="0" data-to="1366"></span></p>
                        <p class="fact-name">Doanh nghiệp</p>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- Fun Facts End -->

    <!-- Registration Box -->
    {{-- <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="call-to-action-box candidate-box">
                        <div class="icon">
                            <img src="/assets/clients/img/register-box/1.png" alt="">
                        </div>
                        <span>Are You</span>
                        <h3>Candidate?</h3>
                        <a href="#" data-toggle="modal" data-target="#exampleModalLong2">Register Now <i
                                class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="call-to-action-box employer-register">
                        <div class="icon">
                            <img src="/assets/clients/img/register-box/2.png" alt="">
                        </div>
                        <span>Are You</span>
                        <h3>Employer?</h3>
                        <a href="#" data-toggle="modal" data-target="#exampleModalLong3">Register Now <i
                                class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- Registration Box End -->

    <!-- Testimonial -->
    {{-- <div class="section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="testimonial">
                        <div class="testimonial-quote">
                            <img src="/assets/clients/img/testimonial/quote.png" class="img-fluid" alt="">
                        </div>
                        <div class="testimonial-for">
                            <div class="testimonial-item">
                                <p>“On the other hand, we denounce with righteous indignation and dislike men who are so
                                    beguiled and demoralized by the charmsto send our denim shorts wardrob One”</p>
                                <h5>Maria Marlin @ Google</h5>
                            </div>
                            <div class="testimonial-item">
                                <p>“On the other hand, we denounce with righteous indignation and dislike men who are so
                                    beguiled and demoralized by the charmsto send our denim shorts wardrob Two”</p>
                                <h5>Laura Harper @ Amazon</h5>
                            </div>
                            <div class="testimonial-item">
                                <p>“On the other hand, we denounce with righteous indignation and dislike men who are so
                                    beguiled and demoralized by the charmsto send our denim shorts wardrob Three”</p>
                                <h5>John Doe @ Envato</h5>
                            </div>
                            <div class="testimonial-item">
                                <p>“On the other hand, we denounce with righteous indignation and dislike men who are so
                                    beguiled and demoralized by the charmsto send our denim shorts wardrob Four”</p>
                                <h5>Jenny Doe @ Dribbble</h5>
                            </div>
                            <div class="testimonial-item">
                                <p>“On the other hand, we denounce with righteous indignation and dislike men who are so
                                    beguiled and demoralized by the charmsto send our denim shorts wardrob Five”</p>
                                <h5>Michle Clark @ Apple</h5>
                            </div>
                            <div class="testimonial-item">
                                <p>“On the other hand, we denounce with righteous indignation and dislike men who are so
                                    beguiled and demoralized by the charmsto send our denim shorts wardrob Two”</p>
                                <h5>Laura Harper @ Amazon</h5>
                            </div>
                            <div class="testimonial-item">
                                <p>“On the other hand, we denounce with righteous indignation and dislike men who are so
                                    beguiled and demoralized by the charmsto send our denim shorts wardrob Three”</p>
                                <h5>John Doe @ Envato</h5>
                            </div>
                        </div>
                        <div class="testimonial-nav">
                            <div class="commenter-thumb">
                                <img src="/assets/clients/img/testimonial/thumb-1.jpg" class="img-fluid commenter" alt="">
                                <img src="/assets/clients/img/testimonial/1.png" class="comapnyLogo" alt="">
                            </div>
                            <div class="commenter-thumb">
                                <img src="/assets/clients/img/testimonial/thumb-2.jpg" class="img-fluid commenter" alt="">
                                <img src="/assets/clients/img/testimonial/2.png" class="comapnyLogo" alt="">
                            </div>
                            <div class="commenter-thumb">
                                <img src="/assets/clients/img/testimonial/thumb-3.jpg" class="img-fluid commenter" alt="">
                                <img src="/assets/clients/img/testimonial/3.png" class="comapnyLogo" alt="">
                            </div>
                            <div class="commenter-thumb">
                                <img src="/assets/clients/img/testimonial/thumb-4.jpg" class="img-fluid commenter" alt="">
                                <img src="/assets/clients/img/testimonial/4.png" class="comapnyLogo" alt="">
                            </div>
                            <div class="commenter-thumb">
                                <img src="/assets/clients/img/testimonial/thumb-5.jpg" class="img-fluid commenter" alt="">
                                <img src="/assets/clients/img/testimonial/5.png" class="comapnyLogo" alt="">
                            </div>
                            <div class="commenter-thumb">
                                <img src="/assets/clients/img/testimonial/thumb-2.jpg" class="img-fluid commenter" alt="">
                                <img src="/assets/clients/img/testimonial/2.png" class="comapnyLogo" alt="">
                            </div>
                            <div class="commenter-thumb">
                                <img src="/assets/clients/img/testimonial/thumb-3.jpg" class="img-fluid commenter" alt="">
                                <img src="/assets/clients/img/testimonial/3.png" class="comapnyLogo" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- Testimonial End -->

    <!-- NewsLetter -->
    {{-- <div class="newsletter-bg padding-top-90 padding-bottom-90">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6">
                    <div class="newsletter-wrap">
                        <h3>Thông tin ứng tuyển</h3>
                        <p>Nhận cập nhật qua e-mail về tin tức mới nhất và các bài tuyển dụng phù hợp. Chúng tôi không gửi thư rác nên đừng lo lắng.
                        </p>
                        <form action="#" class="newsletter-form form-inline">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Địa chỉ email...">
                            </div>
                            <button class="btn button">Xác nhận<i class="fas fa-caret-right"></i></button>
                            <p class="newsletter-error">0 - Please enter a value</p>
                            <p class="newsletter-success">Thank you for subscribing!</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- NewsLetter End -->

@endsection
