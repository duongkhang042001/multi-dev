@extends('layouts.client_guest')
@section('page-title', 'Thông báo')
@section('content')
    <div class="alice-bg padding-top-70 padding-bottom-70 ">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="white-bg others-block">
                        <div class="padding-top-60 padding-bottom-60">
                            <div class="container padding-top-60 padding-bottom-60 border">
                                <div class="row">
                                    <div class="col">
                                        <div class="section-header section-header-center">
                                            <h6>Hệ Thống FPT INTERNSHIP</h6>
                                            <h2 class="text-primary">Thông Báo</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    @if (Session::has('notify-content'))
                                        <p>{!!Session::get('notify-content')!!}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
