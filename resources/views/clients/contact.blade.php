@extends('layouts.client_guest')
@section('page-title', 'Liên hệ | FPT Intern')
@section('content')
<!-- Breadcrumb -->
<div class="alice-bg padding-top-70 padding-bottom-70">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="breadcrumb-area">
                    <h1>Liên hệ</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Liên hệ</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6">
                <div class="breadcrumb-form">
                    <form action="#">
                        <input type="text" placeholder="Nhập từ khoá tìm kiếm...">
                        <button><i data-feather="search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<div class="alice-bg section-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="contact-block">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="contact-address">
                                <h4>Thông tin liên hệ</h4>
                                <ul>
                                    <li><i data-feather="map-pin"></i>Toà nhà Innovation, lô 24, Công viên phần mềm
                                        Quang Trung, Quận 12, Hồ Chí Minh</li>
                                    <li><i data-feather="mail"></i>internshipfpt2021@gmail.com</li>
                                    <li><i data-feather="phone-call"></i>+8 246-345-0698</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-7 offset-lg-1">
                            <div class="contact-form">
                                <h4>Liên lạc với chúng tôi</h4>
                                <form action="{{route('feed-back.store')}}" method="POST" id="myForm">
                                    @csrf
                                    @method('POST')
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="full_name" placeholder="Họ và tên" value="{{ old('full_name') }}" >
                                                @error('full_name')
                                                <span class="text-danger small font-italic">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}">
                                                @error('email')
                                                <span class="text-danger small font-italic">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="phone" placeholder="Số điện thoại" value="{{ old('phone') }}">
                                                @error('phone')
                                                <span class="text-danger small font-italic">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="title" placeholder="Tiêu đề" value="{{ old('title') }}">
                                                @error('title')
                                                <span class="text-danger small font-italic">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <textarea class="form-control" name="content" placeholder="Nội dung">{{old('content')}}</textarea>
                                                @error('content')
                                                <span class="text-danger small font-italic">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="button">Gửi liên hệ</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div id="map-area" class="contact-map">
                                <div class="cp-map" id="location" data-lat="40.713355" data-lng="-74.005535" data-zoom="10"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
 <script>
     window.addEventListener("pageshow", () => {
     document.getElementById("myForm").reset();
});
 </script>
  
   
@endpush