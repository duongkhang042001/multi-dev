@extends('layouts.client')
@section('page-title', 'Thông báo')
@section('title', 'Thông báo')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Sinh viên</a></li>
        <li class="breadcrumb-item active" aria-current="page">Thông báo</li>
    </ol>
</nav>
@endsection
@section('content')
<div class="dashboard-content-wrapper">
    <div class="about-details details-section dashboard-section pl-0">
        <h4><i class="fas fa-list mr-3"></i>THÔNG BÁO {{$notifyDetail->notify->title}}</h4>

        <div class="input-block-wrap">
            <div class="text-left col-sm-12" style="font-size: 15px;  border: 1px dashed #ccc; padding: 15px 25px;">
                <h6 class="col-12 text-primary text-center mt-4">{{$notifyDetail->notify->title}}</h6>
                <div class="education-label">
                    <span class="study-year font-weight-bold ">Nội dung thông báo:</span>
                    <div class="warning-report mt-3">
                        {!!$notifyDetail->notify->content!!}
                    </div>
                </div>
                <br>
                <div class="float-right"><small>( Thời gian: {{ date('d/m/Y H:i:s' ,
                        strtotime($notifyDetail->created_at)) }} )</small></div><br>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')

@endpush