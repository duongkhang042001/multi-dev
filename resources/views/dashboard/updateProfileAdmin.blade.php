@extends('layouts.dashboard')
@section('page-title', 'Cập nhật hồ sơ')
@section('title', 'Cập nhật hồ sơ')
@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Trang chủ</a></li>
                        <li class="breadcrumb-item active">Cập nhật hồ sơ</li>
                    </ol>
                </div>
                <h4 class="page-title">Cập Nhật Hồ Sơ</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- end row -->
    <form autocomplete="off" action="{{Auth::guard('staff')->check() ?  route('staff.updateProfile') :  route('manager.updateProfile')}}" method="POST" enctype="multipart/form-data">
        @csrf
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><i class="far fa-address-card mr-1"></i> Hồ sơ cá nhân</h4>
                </div>
                <div class="row" style="width: 100%; height: 20px;"></div>
                <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Họ và tên:</label>
                                    <input type="text" class="form-control" name="full_name" value="{{ !empty(old('full_name'))? old('full_name') : $user->profile->full_name}}">
                                    @error('full_name')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="bmd-label-floating">Biệt danh: <span class="font-weight-light">(nếu có)</span></label>
                                <input type="text" class="form-control" name="short_name" value="{{ !empty(old('short_name'))? old('short_name') : $user->profile->short_name}}" disabled>
                                @error('short_name')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group ">
                                <label class="bmd-label-floating">Ngày Sinh:</label>
                                <input type="date" class="form-control" id="birthday" name="birthday" value="{{ date('Y-m-d', strtotime(!empty(old('birthday'))? old('birthday') : $user->profile->birthday)) }}">
                                @error('birthday')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3 pl-sm-3">
                            <div class="form-group">
                                <label class="">Giới Tính:</label>
                                <div class="d-flex flex-wrap">
                                    <div class="custom-control custom-radio  mr-2">
                                        <input type="radio" id="customRadio1" name="gender" value="{{MAN}}" class="custom-control-input" {{old('gender') == 0 || $user->profile->gender == 0 ? "checked" : ""}}>
                                        <label class="custom-control-label" for="customRadio1">Nam</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio2" name="gender" value="{{WOMEN}}" class="custom-control-input" {{old('gender') == 1 || $user->profile->gender == 1 ? "checked" : ""}}>
                                        <label class="custom-control-label" for="customRadio2">Nữ</label>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Email:</label>
                                    <input type="email" class="form-control"   value="{{$user->email}}" disabled>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Email cá nhân: <span class="font-weight-light">(nếu có)</span></label>
                                    <input type="email" class="form-control"  name="email_personal" value="{{!empty(old('email_personal')) ?  old('email_personal') : $user->profile->email_personal}}">
                                    @error('email_personal')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Số điện thoại:</label>
                                    <input type="text" class="form-control" name="phone" value="{{!empty(old('phone'))? old('phone') : $user->profile->phone}}">
                                    @error('phone')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="bmd-label-floating">CMND / CCCD:</label>
                                    <input type="text" class="form-control" name="indo" placeholder="Nhập số chứng minh thư của bạn" value="{{!empty(old('indo'))? old('indo') :$user->profile->indo}}">
                                    @error('indo')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Địa chỉ:</label>
                                    <input type="text" class="form-control" name="address" value="{{ !empty(old('address'))? old('address') : $user->profile->address }}">
                                </div>
                            </div>
                        </div>
                        @livewire('edit-address-admin',['cityCode' => !empty(old('city'))? old('city') : $user->profile->city_id,'districtCode' => !empty(old('district'))? old('district') : $user->profile->district_id,'wardsCode' => !empty(old('ward'))? old('ward') : $user->profile->ward_id ])
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tiểu sử:</label>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" name="description" > {{ !empty(old('description'))? old('description') : $user->profile->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Cập nhật hồ sơ</button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><i class="fas fa-image mr-1"></i> Hình Ảnh</h4>
                </div>

                <div class="card-body">
                        <h4 class="header-title m-t-0 m-b-30">Ảnh Đại Diện</h4>
                    <input type="file" name="avatar" class="dropify" data-default-file="assets/images/default-avatar.png" />
                    <h6 class="card-category text-primary"><i class="mdi mdi-shield-account">  </i> {{$user->role->name}} </h6>
                    <h3 class="font-weight-bold"> {{$user->name}} </h3>
                    <p class="text-primary">Email: {{$user->email}}</p>
                    <p class="card-description mt-3 font-italic">
                        {{!empty($user->profile->description) ? $user->profile->description : 'Chưa có thông tin tiểu sử' }}
                    </p>
                </div>
            </div>
            <div class="card-box font-italic d-none d-sm-block">
                    <h4 class="header-title "><u>Chú ý:</u></h4>
                    <p class="card-description  ">
                        Mỗi nhân viên trong một doanh nghiệp, dù là làm toàn thời gian hay bán thời gian, ở vị trí nhân viên hay quản lý đều phải có trách nhiệm hoàn thành các nhiệm vụ cụ thể ở vị trí đó. Trách nhiệm của nhân viên là thực hiện các công việc được giao một cách tốt nhất trong khi tuân thủ các chính sách và giao thức của doanh nghiệp, quản lý thời gian và hoà nhập với văn hoá doanh nghiệp. Trong khi đó, quản lý đảm bảo cung cấp môi trường làm việc tích cực và sẵn sàng giải đáp các thắc mắc của nhân viên cấp dưới.
                    </p>
            </div>
        </div>
    </div>
    </form>

@endsection

@push('css')
    <!-- form Uploads -->
    <link href="assets/libs/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
    <!-- Script You Need -->
    <livewire:scripts />
    <!-- file uploads js -->
    <script src="assets/libs/fileuploads/js/dropify.min.js"></script>
    <script type="text/javascript">
        $('.dropify').dropify({
            messages: {
                'default': 'Kéo và thả tệp vào đây hoặc nhấp vào',
                'replace': 'Kéo và thả hoặc nhấp để thay thế',
                'remove': 'Thay đổi',
                'error': 'Rất tiếc, đã xảy ra sự cố.'
            },
            error: {
                'fileSize': 'Kích thước tệp quá lớn (tối đa 1M).'
            }
        });
        let urlImage = `{{$user->getAvatar()}}`
        if(urlImage) $(".dropify-render img").attr("src",urlImage) ;
    </script>

@endpush
