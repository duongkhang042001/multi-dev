@extends('layouts.dashboard')
@section('page-title', 'Thông tin tài khoản')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="/admin">Tổng Quan</a></li>
                        <li class="breadcrumb-item active">Hồ sơ Cá Nhân</li>
                    </ol>
                </div>
                <h4 class="page-title">Hồ sơ Cá Nhân</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8" >
            <div class="card" >
                <div class="card-header card-header-icon card-header-primary d-flex justify-content-between">
                    <h4 class="card-title ">Hồ sơ cá nhân </h4>
                    <a href="{{Auth::guard('staff')->check() ?  route('staff.editProfile') : route('manager.editProfile')}}" class="font-italic">( Cập nhật )</a>
                </div>
                <div class="card-body">
                    <form autocomplete="off">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Họ và tên:</label>
                                    <input type="text" class="form-control" name="full_name"
                                           value="{{$user->profile->full_name}}" disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Họ:</label>
                                    <input type="text" class="form-control" name="first_name"
                                           value="{{$user->profile->first_name}}" disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Tên:</label>
                                    <input type="text" class="form-control" name="last_name"
                                           value="{{$user->profile->last_name}}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Email:</label>
                                    <input type="name" class="form-control" name="name" disabled value="{{$user->email}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Email cá nhân:</label>
                                    <input type="name" class="form-control" name="name" disabled value="{{$user->profile->email_personal}}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Ngày sinh:</label>
                                    <input type="name" class="form-control" name="name" disabled value="{{date('d-m-Y',strtotime($user->profile->birthday)) }}">
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Số điện thoại:</label>
                                    <input type="text" class="form-control" name="phone"
                                           value="{{$user->profile->phone}}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="bmd-label-floating">CMND / CCCD:</label>
                                    <input type="text" class="form-control" name="indo"
                                           value="{{$user->profile->indo}}" disabled>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Giới tính:</label>
                                    <input type="name" class="form-control" name="name" disabled value="{{$user->profile->gender ? 'Nam' : 'Nữ' }}">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Địa chỉ:</label>
                                    <input type="text" class="form-control" name="address"
                                           value="{{ $user->profile->address }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2 mb-4">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{ $user->profile->city_name }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{ $user->profile->district_name }}" disabled>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{ $user->profile->ward_name }}" disabled>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" >
                <div class="card-header">
                    <h4 class="card-title"><i class="fas fa-image mr-1"></i> Hình Ảnh</h4>
                </div>

                <div class="card-body">

                    <h4 class="header-title m-t-0 m-b-30">Ảnh Đại Diện</h4>

                    <input type="file" class="dropify" data-default-file="assets/images/default-avatar.png" />
                    <h6 class="card-category text-primary mt-3"><i class="mdi mdi-shield-account">  </i> {{$user->role->name}} </h6>
                    <h3 class="font-weight-bold"> {{$user->name}} </h3>
                    <p class="text-primary">Email: {{$user->email}}</p>
                    <p class="card-description mt-3 font-italic">
                        {{!empty($user->profile->description) ? $user->profile->description : 'Chưa có thông tin tiểu sử' }}
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('css')
    <!-- form Uploads -->
    <link href="assets/libs/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')
    <!-- file uploads js -->
    <script src="assets/libs/fileuploads/js/dropify.min.js"></script>
    <script type="text/javascript">
        $('.dropify').dropify({
            messages: {
                'default': 'Kéo và thả tệp vào đây hoặc nhấp vào',
                'replace': 'Kéo và thả hoặc nhấp để thay thế',
                'remove': 'Thay đổi',
                'error': 'Rất tiếc, đã xảy ra sự cố.'
            },
            error: {
                'fileSize': 'Kích thước tệp quá lớn (tối đa 1M).'
            }
        });
        let urlImage = `{{$user->getAvatar()}}`
        if(urlImage) $(".dropify-render img").attr("src",urlImage) ;
    </script>

@endpush
