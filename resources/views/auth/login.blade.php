@extends('layouts.auth_dashboard')
@section('content')
    <div class="card bg-pattern">

        <div class="card-body p-4">

            <div class="text-center w-75 m-auto">
                <a href="/admin">
                    <span><img src="assets/images/logo-2.png" alt="" height="50"></span>
                </a>
                <h5 class="text-uppercase text-center font-bold mt-4">Đăng nhập</h5>

            </div>

            <form method="post" action="{{ route('admin.check',['guard'=>'admin']) }}">
                @csrf
                <div class="form-group mb-3">
                    <label for="emailaddress">Email</label>
                    <input class="form-control" type="email" name="email" id="emailaddress" required="" placeholder="Nhập email của bạn">
                </div>

                <div class="form-group mb-3">
                    <a href="{{route('admin.forgot.show')}}" class="text-muted float-right"><small>Quên mật khẩu?</small></a>

                    <label for="password">Mật Khẩu</label>
                    <input class="form-control" type="password" name="password" required="" id="password" placeholder="Nhập mật khẩu của bạn">
                </div>

                <div class="form-group mb-3">
                    <div class="custom-control custom-checkbox checkbox-success">
                        <input type="checkbox" class="custom-control-input" id="checkbox-signin" checked>
                        <label class="custom-control-label" for="checkbox-signin">Lưu mật khẩu</label>
                    </div>
                </div>

                <div class="form-group mb-0 text-center">
                    <button class="btn btn-gradient btn-block" type="submit"> Đăng Nhập </button>
                </div>

            </form>

            <div class="row mt-4">
                <div class="col-sm-12 text-center">
                    <p class=" mb-0">
                        <a class="text-danger" href="{{route('social.redirect','google')}}">
                            <i class="mdi mdi-google-plus-box"></i>
                            Đăng nhập bằng Google
                        </a>
                    </p>
                </div>
            </div>


        </div> <!-- end card-body -->
    </div>
@endsection
