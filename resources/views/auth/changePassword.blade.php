@extends('layouts.auth_dashboard')
@section('content')
    <div class="card bg-pattern">

        <div class="card-body p-4">

            <div class="text-center m-auto">
                <a href="/admin">
                    <span><img src="assets/images/logo-2.png" alt="" height="50"></span>
                </a>
                <p class="text-muted sub-header mb-4 mt-3">Xác thực email thành công. Hãy cập nhật lại mật khẩu của bạn!</p>
            </div>

            <form method="post" action="{{ route('admin.change.update',['guard'=>'admin']) }}">
                @csrf
                <div class="form-group mb-3">
                    <label for="emailaddress">Mật khẩu mới</label>
                    <input type="password" name="password" class="form-control" placeholder="********">
                    @error('password')
                        <span class="text-danger font-italic"> {{$message}} </span>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="emailaddress">Nhập lại mật khẩu mới</label>
                    <input type="password" name="password_confirmation" class="form-control" placeholder="********">
                    @error('password_confirmation')
                    <span class="text-danger font-italic"> {{$message}} </span>
                    @enderror
                </div>
                <div class="form-group mb-0 text-center">
                    <button class="btn btn-gradient btn-block" type="submit">Cập Nhật</button>
                </div>

            </form>

        </div> <!-- end card-body -->
    </div>

@endsection
