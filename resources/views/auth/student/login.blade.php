@extends('layouts.auth_client')
@section('title','Sinh Viên')
@section('content')
    <header class="header-2 access-page-nav">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="header-top">
                        <div class="logo-area">
                            <a href="/"><img src="assets/clients/img/logo-2.png" alt=""></a>
                        </div>
                        <div class="top-nav">
                            <a href="{{ route('home') }}" class="account-page-link">QUAY LẠI TRANG CHỦ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="padding-top-90 padding-bottom-90 access-page-bg-student">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6">
                    <div class="access-form">
                        <div class="form-header">
                            <h5><i data-feather="user"></i>Đăng nhập</h5>
                        </div>
                        <form action="{{ route('company.check', ['guard' => 'student']) }}" method="post">
                            @if (Session::get('fail') || !empty(Session::has('errors')))
                                <div class="alert alert-danger">
                                    Sai thông tin đăng nhập!
                                </div>
                            @endif
                            @if(Session::has('confirm'))
                                    <div class="alert alert-info">
                                        Vui lòng xác thực email để đăng nhập!
                                     </div>
                            @endif
                            @csrf
                            <div class="form-group">
                                <input type="email" name="email" placeholder="Địa chỉ Email" class="form-control"
                                    value="{{ old('email') }}">
                                <span class="text-danger">@error('email'){{ $message }}@enderror</span>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" placeholder="Mật khẩu" class="form-control"
                                        value="{{ old('password') }}">
                                </div>
                                <div class="more-option">
                                    <div class="mt-0 terms">
                                        <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition">
                                        <label for="radio-4">
                                            <span class="dot"></span> Lưu mật khẩu
                                        </label>
                                    </div>
                                    <a href="#">Quên mật khẩu</a>
                                </div>
                                <button type="submit" class="button primary-bg btn-block">Đăng nhập</button>
                            </form>
                            <div class="shortcut-login">
                                <span>Hoặc đăng nhập với</span>
                                <div class="buttons">
                                    <a href="{{ url('/redirect/google  ') }}" class="google w-100"><i
                                            class="fab fa-google"></i>Google</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection


