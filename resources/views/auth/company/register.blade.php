@extends('layouts.auth_client')
@section('title','Đăng Ký Doanh Nghiệp')
@section('content')
    <header class="header-2 access-page-nav">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="header-top">
                        <div class="logo-area">
                            <a href="/"><img src="assets/clients/img/logo-2.png" alt=""></a>
                        </div>
                        <div class="top-nav">
                            <a href="{{route('company.login')}}" class="account-page-link">ĐĂNG NHẬP</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="padding-top-90 padding-bottom-90 access-page-bg">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6">
                    <div class="access-form">
                        <div class="form-header">
                            <h5><i data-feather="edit"></i>Đăng ký </h5>
                        </div>
                        <div class="account-type">
                            <label for="idRegisterCan">
                                <input id="idRegisterCan" type="radio" name="register">
                                <span>Quản lý </span>
                            </label>
                            <label for="idRegisterEmp">
                                <input id="idRegisterEmp" type="radio" name="register">
                                <span>Nhân viên</span>
                            </label>
                        </div>
                        <form action="{{route('company.register.create')}}" method="post">
                            @if (Session::has('fail') )
                                <div class="alert alert-danger">
                                    {{Session::get('fail')}}
                                </div>
                            @elseif (Session::has('confirm'))
                                <div class="alert alert-primary">
                                    {{Session::get('confirm')}}
                                </div>
                            @endif
                            @csrf
                            <div class="form-group">
                                <input type="email" name="email" placeholder="Email" class="form-control" value="{{old('email')}}">
                                @error('email') <span class="text-danger"> {{$message}} </span> @enderror
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" placeholder="Mật khẩu" class="form-control" value="{{old('password')}}">
                                @error('password') <span class="text-danger"> {{$message}} </span> @enderror
                            </div>
                            <div class="more-option terms">
                                <div class="mt-0 terms">
                                    <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition" checked>
                                    <label for="radio-4">
                                        <span class="dot"></span> Tôi chấp nhận <a href="#">tất cả điều khoản</a>
                                    </label>
                                </div>
                            </div>
                            <button class="button primary-bg btn-block">Đăng ký</button>
                        </form>
                        <div class="shortcut-login">
                            {{-- <span>Hoặc đăng ký với</span>
                            <div class="buttons">
                                <a href="#" class="facebook"><i class="fab fa-facebook-f"></i>Facebook</a>
                                <a href="#" class="google"><i class="fab fa-google"></i>Google</a>
                            </div> --}}
                            <p>Bạn đã có tài khoản ? <a href="{{route('company.login')}}">Đăng nhập</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
