@extends('layouts.auth_dashboard')
@section('content')
    <div class="card bg-pattern">

        <div class="card-body p-4">

            <div class="text-center m-auto">
                <a href="/admin">
                    <span><img src="assets/images/logo-2.png" alt="" height="50"></span>
                </a>
                <p class="text-muted sub-header mb-4 mt-3">Nhập địa chỉ email của bạn và chúng tôi sẽ gửi cho bạn một email kèm theo hướng dẫn để đặt lại mật khẩu của bạn.</p>
            </div>

            <form method="post" action="{{ route('admin.forgot.send',['guard'=>'admin']) }}">
                @csrf
                <div class="form-group mb-3">
                    <label for="emailaddress">Email của bạn</label>
                    <input class="form-control" type="email" name="email" id="emailaddress" required="" placeholder="quangbui@gmail.com">
                </div>

                <div class="form-group mb-0 text-center">
                    <button class="btn btn-gradient btn-block" type="submit"> Gửi Yêu Cầu! </button>
                </div>

            </form>

            <div class="row mt-4">
                <div class="col-sm-12 text-center">
                    <p class="text-muted mb-0">Quay lại <a href="{{route('admin.login')}}" class="text-dark ml-1"><b>Đăng Nhập</b></a></p>
                </div>
            </div>


        </div> <!-- end card-body -->
    </div>
@endsection
