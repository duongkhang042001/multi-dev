@extends('layouts.auth_dashboard')
@section('content')
    @if(Session::has('user') || Session::get('user')['guard'] == 'admin')
    <div class="card bg-pattern">
        <div class="card-body p-4">
            <div class="text-center w-75 m-auto">

                <a href="index.html">
                    <span><img src="assets/images/logo-2.png" alt="" height="50"></span>
                </a>
                <h5 class="text-center mt-4">Xin chào ! {{Session::get('user')['name']}}</h5>
            </div>

            <div class="account-content">
                <div class="text-center">
                    <div class="mb-3 mt-3">
                        <img src="{{Session::get('user')['avatar']}}" onerror="this.src='assets/images/default-avatar.png'" class="rounded-circle img-fluid img-thumbnail avatar-lg" alt="thumbnail">
                    </div>

                    <p class="text-muted font-14">Nhập mật khẩu của bạn để truy cập quản trị viên.</p>
                </div>
                <form class="form-horizontal mt-5"  method="post" action="{{ route('admin.check',['guard'=>'admin']) }}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="password">Mật khẩu</label>
                            <input class="form-control" type="password" name="password" id="password" placeholder="Nhập mật khẩu của bạn">
                            @error('password')
                            <span class="text-danger font-italic"> {{$message}} </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row text-center">
                        <div class="col-12">
                            <input  type="hidden" name="email"  value="{{Session::get('user')['email']}}">
                            <button class="btn btn-block btn-gradient waves-effect waves-light" type="submit">Đăng Nhập</button>
                        </div>
                    </div>

                </form>

                <div class="row mt-4">
                    <div class="col-sm-12 text-center">
                        <p class="text-muted mb-0">Không phải bạn? Quay lại<a href="{{route('admin.login')}}" class="text-dark ml-1"><b>Đăng Nhập</b></a></p>
                    </div>
                </div>

            </div>

        </div> <!-- end card-body -->
    </div>
    @else
        <script>window.location = "/admin/login";</script>
    @endif
@endsection
