<div class="dashboard-section dashboard-recent-activity" wire:poll.keep-alive>
    <h4 class="title">Sinh viên ứng tuyển mới</h4>
    @php
        $msPerMinute = 60;
        $msPerHour = $msPerMinute * 60;
        $msPerDay = $msPerHour * 24;
        $msPerMonth = $msPerDay * 30;
        $msPerYear = $msPerDay * 365;
    @endphp
    @foreach ($listRcmDetail as $item)
        <div class="activity-list">
            <i class="fas fa-arrow-circle-down"></i>
            <div class="content">
                <h5><a href="{{ route('company.recruitment-manager.index') }}"><b>{{ $item->user->name }}</b> ứng tuyển vào bài
                        viết
                        <b>{{ $item->recruitmentPost->post->title }}</b> .</h5>
                </a>
                <span class="time">
                    @php
                        $elapsed = time() - strtotime($item->created_at);
                        if ($elapsed < $msPerMinute) {
                            echo ceil($elapsed) . ' giây trước';
                        } elseif ($elapsed < $msPerHour) {
                            echo ceil($elapsed / $msPerMinute) . ' phút trước';
                        } elseif ($elapsed < $msPerDay) {
                            echo ceil($elapsed / $msPerHour) . ' giờ trước';
                        } elseif ($elapsed < $msPerMonth) {
                            echo ceil($elapsed / $msPerDay) . ' ngày trước';
                        } elseif ($elapsed < $msPerYear) {
                            echo ceil($elapsed / $msPerMonth) . ' tháng trước';
                        } else {
                            echo ceil($elapsed / $msPerYear) . ' năm trước';
                        }
                    @endphp
                </span>
            </div>
            {{-- <div class="close-activity">
                <i class="fas fa-times"></i>
            </div> --}}
        </div>
    @endforeach
</div>
