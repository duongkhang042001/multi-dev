@if (Auth::guard('company')->check() || Auth::guard('student')->check())
    <div class="">
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Tỉnh/Thành phố <span class="text-danger">*</span></label>
            <div class="col-sm-9">
                <select name="city" value="{{ old('city') }}" wire:model="cityId" type="text" class="form-control">
                    <option value="" selected>Vui lòng chọn Tỉnh / Thành phố</option>
                    @foreach ($cities as $city)
                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                    @endforeach
                </select>
                @error('city')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Quận/Huyện <span class="text-danger">*</span></label>
            <div class="col-sm-9">
                <select wire:model="districtId" name="district" @if (count($districts) == 0) disabled @endif type="text"
                    class="form-control">
                    <option value="" selected>Vui lòng chọn Quận / Huyện</option>
                    @foreach ($districts as $district)
                        <option value="{{ $district->id }}">{{ $district->name }}</option>
                    @endforeach
                </select>
                @error('district')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Phường/Xã <span class="text-danger">*</span></label>
            <div class="col-sm-9">
                <select type="text" name="ward" value="{{ old('ward') }}" class="form-control"
                    @if (count($wards) == 0 && count($districts) == 0) disabled @endif>
                    <option selected>Vui lòng chọn Phường / Xã</option>
                    @foreach ($wards as $ward)
                        <option @if ($ward->id == $wardId)
                            selected
                            @endif value="{{ $ward->id }}">{{ $ward->name }}</option>
                    @endforeach
                </select>
                @error('ward')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>
@else
    <div class="">
        <div class="form-group row">
            <label class="col-2 col-form-label">Thành Phố/Tỉnh <span class="text-danger">*</span></label>
            <div class="col-sm-10">
                <select name="city" value="{{ old('city') }}" wire:model="cityId" type="text" class="form-control">
                    <option value="" selected>Vui lòng chọn Tỉnh / Thành phố</option>
                    @foreach ($cities as $city)
                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 col-form-label">Quận/Huyện <span class="text-danger">*</span></label>
            <div class="col-sm-10">
                <select wire:model="districtId" name="district" value="{{ old('district') }}"
                        @if (count($districts) == 0) disabled @endif type="text" class="form-control">
                    <option value="" selected>Vui lòng chọn Quận / Huyện</option>
                    @foreach ($districts as $district)
                        <option value="{{ $district->id }}">{{ $district->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 col-form-label">Phường/Xã/Tỉnh <span class="text-danger">*</span></label>
            <div class="col-sm-10">
                <select type="text" name="ward" value="{{ old('ward') }}" class="form-control"
                        @if (count($wards) == 0 && count($districts) == 0) disabled @endif>
                    <option value="" selected>Vui lòng chọn Phường / Xã</option>
                    @foreach ($wards as $ward)
                        <option @if ($ward->id == $wardId)
                                selected
                                @endif value="{{ $ward->id }}">{{ $ward->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
@endif
