@auth('staff')
    <div class="">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Chọn nhóm ngành <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <select name="group_career" type="text" wire:model="careerGroupId" class="form-control">
                    <option value="" selected>Vui lòng chọn nhóm ngành</option>
                    @foreach ($groupCareers as $groupCareer)
                        <option value="{{ $groupCareer->id }}">{{ $groupCareer->name }}</option>
                    @endforeach
                </select>
                @error('group_career')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Chọn ngành <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <select name="career" type="text" @if (count($careers) == 0) disabled @endif class="form-control">
                    <option value="" selected>Vui lòng chọn ngành</option>
                    @foreach ($careers as $career)
                        <option @if ($career->id == $careerId)
                                selected
                                @endif
                                value="{{ $career->id }}">{{ $career->name }}</option>
                    @endforeach
                </select>
                @error('career')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>
@else
    <div class="">
        <div class="form-group row">
            <label class="col-sm-3 col-form-label"> Chọn nhóm ngành <span class="text-danger">*</span></label>
            <div class="col-sm-9">
                <select name="group_career" type="text" wire:model="careerGroupId" class="form-control">
                    <option value="" selected>Vui lòng chọn nhóm ngành</option>
                    @foreach ($groupCareers as $groupCareer)
                        <option value="{{ $groupCareer->id }}">{{ $groupCareer->name }}</option>
                    @endforeach
                </select>
                @error('group_career')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Chọn ngành <span class="text-danger">*</span></label>
            <div class="col-sm-9">
                <select name="career" type="text" @if (count($careers) == 0) disabled @endif class="form-control">
                    <option value="" selected>Vui lòng chọn ngành</option>
                    @foreach ($careers as $career)
                        <option @if ($career->id == $careerId)
                                selected
                                @endif
                                value="{{ $career->id }}">{{ $career->name }}</option>
                    @endforeach
                </select>
                @error('career')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>
@endauth
