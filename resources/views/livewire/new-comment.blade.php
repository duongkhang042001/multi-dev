<div class="p-3">
    <div wire:poll.visible class="inbox-widget">
        @foreach ($comments as $item)
            <div class="inbox-item">
                <p class="inbox-item-author"><a
                        href="{{ route('postDetail', $item->post->slug) }}" target="_blank">{{ Str::limit($item->post->title, 25) }}</a>
                </p>
                <p class="inbox-item-text">{{ Str::limit($item->content, 30) }}</p>
                <p class="inbox-item-date">{{ date('h:i:s', strtotime($item->created_at)) }}</p>
            </div>
        @endforeach
        @if (count($comments) == 0)
            Không có bình luận mới.
        @endif
    </div> <!-- end inbox-widget -->
</div>
