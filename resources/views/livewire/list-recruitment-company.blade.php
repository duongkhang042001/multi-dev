<div>

    <div class="job-filter-result row">
        @if($listRecruitmentCompany)
            @foreach($listRecruitmentCompany as $key => $row)
                <div class="col-sm-6">
                    <div class="job-list mb-5 border">                              
                        <div class="thumb">
                            <a href="{{route('recruitment',$row->post->slug)}}">
                                <img src="{{FILE_URL .$row->post->avatar}}" class="img-fluid" onerror="this.src='{{FILE_URL .$row->company->avatar}}'">
                            </a>
                        </div>
                        <div class="body">
                            <div class="content">
                                <h4><a href="{{route('recruitment',$row->post->slug)}}">{{Str::limit($row->post->title,37)}}</a></h4>
                                <div class="info">
                                <span class="company"><a><i class="fas fa-dollar-sign ml-2"></i>{{ floatval(number_format($row->salary_min)) }}-{{ floatval(number_format($row->salary_max)) }}triệu</a></span>
                                <span class="office-location"><a><i class="fas fa-calendar-alt px-2"></i>Hạn nộp : {{date('d-m-Y',strtotime($row->date_end))}}</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <hr />
            @endforeach
        @endif
    </div>
    <div class="d-felx justify-content-center mt-5">
 
    {{ $listRecruitmentCompany->links('livewire.pagination-recruitment-links') }}

    </div>
</div>