<div>
    <!-- Breadcrumb -->
    <div class="alice-bg padding-top-70 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="breadcrumb-area">
                        <h1><a href="{{route('recruitment.list')}}">Danh sách công việc</a></h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="{{route('recruitment.list')}}">Danh sách công việc</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="breadcrumb-form-gb">
                        <div class="form">
                            <input type="text" name="keywords" placeholder="Nhập từ khoá" wire:model="search">
                            <button type="submit"><i class="fas fa-search"></i></button>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Breadcrumb End -->
    <div class="alice-bg section-padding-bottom">
        <div class="container">
            <div class="row no-gutters">
                <div class="col">
                    <div class="filtered-job-listing-wrapper" style="min-height: 400px;">
                        <div class="job-view-controller-wrapper" style="box-shadow: 0px 15px 0px -20px lightgray;">
                            <div class="job-view-controller w-100 flex-wrap">
                                <div class="job-view-filter mb-2">
                                    <select wire:model="byCareer" class="filter-career-gb1">
                                        <option value="" selected>Ngành nghề</option>
                                        @foreach($careers as $row)
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="job-view-filter mb-2">
                                    <select wire:model="city" class="filter-career-gb1">
                                        <option value="" selected>Địa điểm</option>
                                        @foreach($cities as $row)
                                        <option value="{{$row->name}}">{{$row->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="job-view-filter mb-2">
                                    <select wire:model="gender" class="filter-career-gb1">
                                        <option value="" selected>Giới tính</option>
                                        <option value="0">Nam</option>
                                        <option value="1">Nữ</option>
                                        <option value="-1">Không giới tính</option>
                                    </select>
                                </div>
                                <div class="job-view-filter mb-2">
                                    <select wire:model="jobType" class="filter-career-gb1">
                                        <option value="" selected>Loại công việc</option>
                                        <option value="0">Toàn thời gian</option>
                                        <option value="1">Bán thời gian</option>
                                    </select>
                                </div>
                                <div class="ml-auto" class="filter-career-gb1">
                                    <div class="d-flex">
                                        <div class="controller list active">
                                            <i class="fas fa-bars"></i>
                                        </div>
                                        <div class="controller grid">
                                            <i class="fas fa-th-large"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="job-filter-result">
                            @if(count($recruitment) > 0)
                                @foreach($recruitment as $key => $row)
                                    @php
                                        $slugs = $row->post->slug;
                                    @endphp
                                    <div class="job-list">
                                        <div class="thumb">
                                            <a href="{{route('recruitment',$slugs)}}">
                                                <img src="{{FILE_URL .$row->company->avatar}}" onerror="this.src='assets/clients/img/job/company-logo-1.png'" class="w-100 h-100" alt="">
                                            </a>
                                        </div>
                                        <div class="body">
                                            <div class="content">
                                                <h4><a href="{{route('recruitment',$slugs)}}">{{Str::limit($row->post->title,80)}}</a></h4>
                                                <div class="info">
                                                    <span class="office-location"><a><i class="fas fa-map-marker-alt px-2"></i>{{$row->city_name}}</a></span>
                                                    <span class="job-type temporary"><a><i class="fas fa-dollar-sign px-2"></i>{{floatval(number_format($row->salary_min))}}-{{floatval(number_format($row->salary_max))}} triệu</a></span>
                                                    <span class="office-location"><a><i class="fas fa-eye px-2"></i>{{$row->post->view}}</a></span>
                                                </div>
                                            </div>
                                            <div class="more">
                                                <div class="buttons">
                                                    <a href="{{route('recruitment',$slugs)}}" class="button">Xem ngay</a>
                                                    <a class="favourite"><i class="fas fa-heart text-danger"></i></i></a>
                                                </div>

                                                @if(strtotime($row->date_end) > time())
                                                <p class="deadline">Hết hạn: {{date('d-m-Y',strtotime($row->date_end))}}</p>
                                                @else
                                                <p class="deadline text-danger mr-5">Đã hết hạn</p>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                            <p class="font-weight-bold text-center mt-5">Không tìm thấy việc làm phù hợp với yêu cầu của bạn.</p>
                            @endif
                        </div>
                        <div class="row">
                            @if(count($recruitment) > 0)
                            <div class="col-sm-12" style="margin: 0 auto;">
                                {{$recruitment->links('livewire.pagination-links')}}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('css')
    <style>
        .filter-career-gb1 {
            margin-left: 25px;
            font-size: 1.4rem;
            padding: 9px 15px;
            border: 1px solid rgba(0, 0, 0, 0.08);
            border-radius: 3px;
            -webkit-box-shadow: none;
            box-shadow: none;
            width: 150px;
            outline: none;
        }
    </style>
    @endpush
</div>