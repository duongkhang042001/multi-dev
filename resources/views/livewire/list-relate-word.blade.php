<div>
    @foreach ($relatedWork as $key => $row)
        <div class="job-list">
            <div class="thumb">
                <a href="{{route('recruitment',$row->post->slug)}}">
                    <img src="{{$row->company->getAvatar()}}" onerror="this.src='assets/clients/img/job/company-logo-1.png'" class="img-fluid" alt="">
                </a>
            </div>
            <div class="body">
                <div class="content">
                    @php
                        $slugs = $row->post->slug;
                    @endphp
                    <h4><a href="{{ route('recruitment', $slugs) }}">{{ Str::limit($row->post->title,80) }}</a></h4>
                    <div class="info">
                        <span class="company"><a href="{{ route('company.detail', $row->company->slug) }}"><i class="fe-briefcase mr-2"></i>{{ $row->company->short_name }}</a></span>
                        <span class="office-location"><i class="fe-map-pin mr-2"></i>{{ $row->city_name }}</span>
                    </div>
                </div>
                <div class="more">
                    <div class="buttons">
                        <a href="{{ route('recruitment', $slugs) }}" class="button">Xem ngay</a>
                        <a href="javascript:void(0)" class="favourite"><i class="fe-heart"></i></a>
                    </div>
                    <p class="deadline">Hết hạn: {{ date('d-m-Y', strtotime($row->created_at)) }}</p>
                </div>
            </div>
        </div>
    @endforeach

    <div class="d-felx justify-content-center mt-5">

    {{ $relatedWork->links('livewire.pagination-recruitment-links') }}

    </div>
</div>
