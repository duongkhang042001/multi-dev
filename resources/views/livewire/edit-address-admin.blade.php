<div class="row mt-2 mb-4">
    <div class="col-md-4">
        <div class="form-group">
            {{-- <label for="">Tỉnh, Thành phố:</label> --}}
            <select wire:model="cityCode" name="city" type="text" class="form-control">
                <option value="" selected>Vui lòng chọn Tỉnh / Thành phố</option>
                @foreach ($cities as $city)
                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                @endforeach
            </select>
        </div>
        @error('city')
            <br>
            <span class="text-danger small font-italic">{{ $message }}</span>
        @enderror
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <select wire:model="districtCode" name="district" type="text" class="form-control"
                @if (count($districts) == 0) disabled @endif>
                <option value="" selected>Vui lòng chọn Quận / Huyện</option>
                @foreach ($districts as $district)
                    <option value="{{ $district->id }}">{{ $district->name }}</option>
                @endforeach
            </select>
        </div>
        @error('district')
            <br>
            <span class="text-danger small font-italic">{{ $message }}</span>
        @enderror
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <select type="text" class="form-control" name="ward" @if (count($wards) == 0 && count($districts) == 0) disabled @endif>
                <option value="" selected>Vui lòng chọn Phường / Xã</option>
                @foreach ($wards as $ward)
                    <option value="{{ $ward->id }}" @if ($ward->id == $wardsCode)
                        selected
                @endif>{{ $ward->name }}</option>
                @endforeach
            </select>
        </div>
        @error('ward')
            <br>
            <span class="text-danger small font-italic">{{ $message }}</span>
        @enderror
    </div>

</div>
