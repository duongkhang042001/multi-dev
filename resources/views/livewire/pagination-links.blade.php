@if ($paginator->hasPages())
    <ul class="pagination pagination-rounded justify-content-center mt-4">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled mr-4"><a href="javascript:;" wire:click="previousPage" class="page-link" style="width: 40px;text-align:center;"><i class="fas fa-chevron-left"></i></a></li>
        @else
            <li class="page-item mr-4"><a href="javascript:;" wire:click="previousPage" rel="prev" class="page-link" style="width: 40px;text-align:center;"><i class="fas fa-chevron-left"></i></a></li>
        @endif

        {{-- Pagination Element Here --}}
        @foreach ($elements as $element)
            {{-- Make dots here --}}
            @if (is_string($element))
                <li class="page-item disabled"><a class="page-link"><span>{{ $element }}</span></a></li>
            @endif

            {{-- Links array Here --}}
            @if (is_array($element))
                @foreach ($element as $page=>$url)
                    @if ($page == $paginator->currentPage())
                    <li class="page-item active px-2" aria-current="page"><a href="javascript:;" wire:click="gotoPage({{$page}})" class="page-link" style="width: 40px;text-align:center;border-radius: 2px;"><span>{{ $page }}</span></a></li>
                    @else
                    <li class="page-item px-2"><a href="javascript:;" wire:click="gotoPage({{$page}})" class="page-link" style="width: 40px;text-align:center;border-radius: 2px;">{{$page}}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item ml-4"><a href="javascript:;" wire:click="nextPage" class="page-link" style="width: 40px;text-align:center;"><i class="fas fa-chevron-right"></i></a></li>
        @else
          <li class="page-item disabled ml-4"><a href="javascript:;" class="page-link" style="width: 40px;text-align:center;"><i class="fas fa-chevron-right"></i></a></li>
        @endif
    </ul>
@endif