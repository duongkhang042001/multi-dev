<div class="post-comment-block">
    <div class="comment-respond">
        <div>
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
        </div>
        @if ($updateMode)
            <h4>Chỉnh sửa bình luận</h4>
        @else
            <h4>Viết bình luận</h4>
        @endif
        <div class="row" wire:addComment.prevent="submit">
            <div class="col-md-12">
                @if ($updateMode)
                    <input type="hidden" wire:model="post_id">
                    <div class="form-group">
                        <textarea wire:model="content" class="form-control"></textarea>
                        @error('content')
                            <span style="color: red;">{{ $message }}</span>
                        @enderror
                    </div>
                    <button type="submit" class="button primary-bg" wire:click="update()">Chỉnh sửa</button>
                @else
                    <div class="form-group">
                        <textarea placeholder="Bình luận" wire:model="content" class="form-control"></textarea>
                        @error('content')
                            <span style="color: red;">{{ $message }}</span>
                        @enderror
                    </div>
                    <button type="submit" class="button primary-bg" wire:click="store()">Bình luận</button>
                @endif
            </div>
        </div>
    </div>
    <div class="comment-area" wire:poll.visible>
        <h4>{{ count($comments) }} bình luận</h4>
        <ul class="comments">
            @foreach ($comments as $comment)
                <li class="comment">
                    <div class="comment-wrap">
                        <div class="comment-info">
                            <div class="commenter-thumb">
                                <img src="{{ $comment->user->getAvatar() }}"
                                    onerror="this.src='https:\//fpt-internship.live:8443/file/AARjB70ux'" class="w-100"
                                    alt="">
                            </div>
                            <div class="commenter-info">
                                <span class="commenter-name">{{ $comment->user->name }}</span>
                                <span class="date">
                                    <?php
                                    $msPerMinute = 60;
                                    $msPerHour = $msPerMinute * 60;
                                    $msPerDay = $msPerHour * 24;
                                    $msPerMonth = $msPerDay * 30;
                                    $msPerYear = $msPerDay * 365;
                                    
                                    $elapsed = time() - strtotime($comment->created_at);
                                    if ($elapsed < $msPerMinute) {
                                        echo ceil($elapsed) . ' giây trước';
                                    } elseif ($elapsed < $msPerHour) {
                                        echo ceil($elapsed / $msPerMinute) . ' phút trước';
                                    } elseif ($elapsed < $msPerDay) {
                                        echo ceil($elapsed / $msPerHour) . ' giờ trước';
                                    } elseif ($elapsed < $msPerMonth) {
                                        echo ceil($elapsed / $msPerDay) . ' ngày trước';
                                    } elseif ($elapsed < $msPerYear) {
                                        echo ceil($elapsed / $msPerMonth) . ' tháng trước';
                                    } else {
                                        echo ceil($elapsed / $msPerYear) . ' năm trước';
                                    }
                                    ?>
                                </span>
                            </div>
                            <div class="reply">
                                @if ($comment->user->id == $userIdCurrent)
                                    <div class="dropdown dropleft">
                                        <p class="dropdown-toggle pointer" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-expanded="false" style="font-size: 18px;">
                                        </p>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="javascript:void(0)"
                                                wire:click="edit({{ $comment->id }})">Chỉnh sửa</a>
                                            <a class="dropdown-item" href="javascript:void(0)"
                                                wire:click="deleteComment({{ $comment->id }})">Xóa</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="comment-body">
                            <p>{{ $comment->content }}</p>
                        </div>
                    </div>

                </li>
            @endforeach
        </ul>
        <div class="view-all">
            @if ($total == 0)
                <a class="pointer">Chưa có bình luận</a>
            @elseif($total > $limit)
                <a class="pointer" wire:click="showLimtComment">Xem thêm</a>
            @endif
        </div>

    </div>
</div>
