<div class="modal-body">
    <div class="content">
        <form action="{{ route('company.report-rate.update.cancel',$report->id)}}" method="POST">
            @csrf
            @method('PUT')
            <h4><i data-feather="align-left"></i> Hủy thực tập</h4>
            <div id="job-description" class="row mt-4">
                <label class="col-md-2 col-form-label">Lý do hủy thực tập</label>
                <div class="col-md-10">
                    <textarea id="mytextarea" class="tinymce-editor tinymce-editor-1" name="status_reason">{{old('status_reason')}}</textarea>
                    @error('status_reason')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="buttons">
                <button class="bg-info text-white">Gửi yêu cầu</button>
                <button class="" data-dismiss="modal">Đóng</button>
            </div>
        </form>
    </div>
</div>