<div class="modal-body">
    <div class="content">
        <h4 class="mb-4"><i class="fe-user mr-2"></i>Thông tin sinh viên</h4>
        <div class="title-and-info">
            <div class="row title">
                <div class="col-2">
                    <img src="{{$student->getAvatar()}}" onerror="this.src='assets/clients/img/avt-default.png'" class="w-100">
                </div>
                <div class="col-10">
                    <div class="title-body">
                        <h4 class="py-3 text-success">{{$student->name}}</h4>
                        <div class="row info">
                            <div class="col-6">
                                <span class="candidate-location text-dark"><i class="mdi mdi-email-check-outline"></i>
                                    Ngày sinh: {{date('d/m/Y', strtotime($student->profile->birthday))}}
                                </span>
                                <br>
                                <span class="candidate-location text-dark"><i class="mdi mdi-email-check-outline"></i>
                                    Email: {{$student->email}}
                                </span>
                                <br>
                                <span class="candidate-location text-dark"><i class="mdi mdi-cellphone-nfc"></i>
                                    Điện thoại: {{$student->profile->phone}}
                                </span>
                                <br>
                                <span class="candidate-location text-dark"><i class="mdi mdi-file-document-box-check"></i>
                                    Ngành học: {{$student->student->career->name}}
                                </span>
                            </div>
                            <div class="col-6">
                                <span class="candidate-location text-dark"><i class="mdi mdi-closed-caption-outline"></i>
                                    CCCD/CMND: {{$student->profile->indo}}
                                </span>
                                <br>
                                <span class="candidate-location text-dark"><i class="mdi mdi-email-check-outline"></i>
                                    Email cá nhân: {{$student->profile->email_personal}}
                                </span>
                                <br>
                                <span class="candidate-location text-dark"><i class="mdi mdi-file-document-box-check"></i>
                                    Địa chỉ: {{$student->profile->getAddress()}}
                                </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label font-weight-bold">Giới thiệu bản thân</label>
                            <div class="col-sm-12">
                                <span>{{$student->profile->description}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>