@if($service->roleUser->code == COMPANY_ROLE_CODE)
<form action="{{route('staff.request-company.update',$service->id)}}" method="POST">
    @csrf
    @method('PUT')
    <div class="modal-header bg-info">
        @if($service->status == SERVICE_STATUS_PENDING)
        <h4 class="modal-title text-white" id="myExtraLargeModalLabel">YÊU CẦU HỦY THỰC TẬP TỪ DOANH NGHIỆP</h4>
        @else($service->status == SERVICE_STATUS_CANCEL)
        <h4 class="modal-title text-white" id="myExtraLargeModalLabel">CHẤP NHẬN HỦY THỰC TẬP TỪ DOANH NGHIỆP</h4>
        @endif
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div class="modal-body">
        <p class="sub-header">
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            @if($service->report->is_outside == 1)
            Doanh nghiệp bên ngoài hệ thống
            @else($service->report->is_outside == 0)
            Doanh nghiệp đang hoạt động trên hệ thống
            @endif
        </div>
        </p>
        <div class="row mb-3">
            <div class="col-sm-2">
                <h4>Nhãn hiệu</h4>
                <img src="{{$service->report->company->getAvatar()}}" onerror="this.src='assets/clients/img/avt-default.png'" height="150">
            </div>
            <div class="col-md-10">
                <h4>Thông tin doanh nghiệp</h4>
                <div class="border p-3 ">
                    <div class=" row">
                        <div class="col-sm-3"><label for="">Tên đầy đủ:</label></div>
                        <div class="col-sm-9"><b>{{$service->report->company->name}}</b></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Tên viết tắt:</label></div>
                        <div class="col-sm-9">{{$service->report->company->short_name}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Mã số thuế:</label></div>
                        <div class="col-sm-9">{{$service->report->company->tax_number}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Website</label></div>
                        <div class="col-sm-9"><a href="{{json_decode($service->report->company->url)->url}}">{{json_decode($service->report->company->url)->url}}</a></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Email:</label></div>
                        <div class="col-sm-9">{{$service->report->company->email}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Số điện thoại:</label></div>
                        <div class="col-sm-9">{{$service->report->company->phone}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Lĩnh vực hoạt động:</label></div>
                        <div class="col-sm-9">{{$service->report->company->careerGroup->name}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Địa chỉ:</label></div>
                        <div class="col-sm-9">{{$service->report->company->getAddress()}}</div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row mb-3">
            <div class="col-sm-2">
                <h4>Ảnh đại diện</h4>
                <div class="thumbnail p-1 border">
                    <img src="{{$service->report->user->getAvatar()}}" alt="Logo-dn" class="img-fluid" onerror="this.src='assets/clients/img/avt-default.png'">
                </div>
            </div>
            <div class="col-md-10">
                <h4>Thông tin sinh viên</h4>
                <div class="border p-3 ">
                    <div class=" row">
                        <div class="col-sm-3"><label for="">Họ và tên:</label></div>
                        <div class="col-sm-9"><b>{{$service->report->user->name}}</b> - ({{($service->report->user->profile->gender) ? "Nữ":"Nam"}})</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Email:</label></div>
                        <div class="col-sm-9">{{$service->report->user->email}}</div>
                    </div>
                    @if(empty($service->report->company->manager_id))
                    <div class="row">
                        <div class="col-sm-3"><label for="">Mã số sinh viên</label></div>
                        <div class="col-sm-9">{{$service->report->user->code}} - (Kỳ {{$service->report->user->student->semester_current}})</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Ngành</label></div>
                        <div class="col-sm-9">{{$service->report->user->student->career->name}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Trạng thái thực tập</label></div>
                        <div class="col-sm-9"> @if($service->report->user->student->status('find')) <span class="text-success">Đúng tiến độ</span> @else <span class="text-warning">Trễ tiến độ</span> @endif </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-3"><label for="">Email cá nhân:</label></div>
                        <div class="col-sm-9">{{$service->report->user->profile->email_personal ?? '---'}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Số điện thoại:</label></div>
                        <div class="col-sm-9">{{$service->report->user->profile->phone}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Ngày sinh:</label></div>
                        <div class="col-sm-9">{{date('d/m/Y', strtotime($service->report->user->profile->birthday))}} </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">CCCD/CMND:</label></div>
                        <div class="col-sm-9">{{$service->report->user->profile->indo ?? '---'}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Địa chỉ:</label></div>
                        <div class="col-sm-9">{{$service->report->user->profile->getAddress()}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Giới thiệu:</label></div>
                        <div class="col-sm-9">{{$service->report->user->profile->description ?? '---'}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 mt-2">
                <h5><span class="text-danger">*</span> Lý do doanh nghiệp<span class="font-weight-bold text-info"> {{$service->report->company->name}}</span> hủy thực tập sinh viên <span class="font-weight-bold text-info"> {{$service->user->name}}</span> </h5>
                <div class="row">
                    <div class="col-sm-12 ml-2">
                        <span class="text-dark">{!!$service->description!!}</span>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal-footer" class="modal-footer">
            @if($service->status == SERVICE_STATUS_PENDING)
            <div id="modal-footer" class="modal-footer">
                <input type="submit" name="cancel" class="btn btn-danger" value="Từ chối">
                <input type="submit" name="appoved" class="btn btn-primary" value="Chấp thuận">
            </div>
            @else($service->status == SERVICE_STATUS_CANCEL)
            @endif
        </div>
    </div>
</form>
@else($service->roleUser->code == STUDENT_ROLE_CODE)
<form action="{{route('staff.request-student.update',$service->id)}}" method="POST">
    @csrf
    @method('PUT')
    <div class="modal-header bg-info">
        @if($service->status == SERVICE_STATUS_PENDING)
        <h4 class="modal-title text-white" id="myExtraLargeModalLabel">YÊU CẦU HỦY THỰC TẬP TỪ SINH VIÊN</h4>
        @else($service->status == SERVICE_STATUS_CANCEL)
        <h4 class="modal-title text-white" id="myExtraLargeModalLabel">CHẤP NHẬN HỦY THỰC TẬP TỪ SINH VIÊN</h4>
        @endif
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div class="modal-body">
        <p class="sub-header">
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            @if($service->report->is_outside == 1)
            Doanh nghiệp bên ngoài hệ thống
            @else($service->report->is_outside == 0)
            Doanh nghiệp đang hoạt động trên hệ thống
            @endif
        </div>
        </p>
        <div class="row mb-3">
            <div class="col-sm-2">
                <h4>Nhãn hiệu</h4>
                <img src="{{$service->report->company->getAvatar()}}" onerror="this.src='assets/clients/img/avt-default.png'" height="150">
            </div>
            <div class="col-md-10">
                <h4>Thông tin doanh nghiệp</h4>
                <div class="border p-3 ">
                    <div class=" row">
                        <div class="col-sm-3"><label for="">Tên đầy đủ:</label></div>
                        <div class="col-sm-9"><b>{{$service->report->company->name}}</b></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Tên viết tắt:</label></div>
                        <div class="col-sm-9">{{$service->report->company->short_name}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Mã số thuế:</label></div>
                        <div class="col-sm-9">{{$service->report->company->tax_number}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Website</label></div>
                        <div class="col-sm-9"><a href="{{json_decode($service->report->company->url)->url}}">{{json_decode($service->report->company->url)->url}}</a></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Email:</label></div>
                        <div class="col-sm-9">{{$service->report->company->email}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Số điện thoại:</label></div>
                        <div class="col-sm-9">{{$service->report->company->phone}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Lĩnh vực hoạt động:</label></div>
                        <div class="col-sm-9">{{$service->report->company->careerGroup->name}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Địa chỉ:</label></div>
                        <div class="col-sm-9">{{$service->report->company->getAddress()}}</div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row mb-3">
            <div class="col-sm-2">
                <h4>Ảnh đại diện</h4>
                <div class="thumbnail p-1 border">
                    <img src="{{$service->report->user->getAvatar()}}" alt="Logo-dn" class="img-fluid mt-3" onerror="this.src='assets/clients/img/avt-default.png'">
                </div>
            </div>
            <div class="col-md-10">
                <h4>Thông tin sinh viên</h4>
                <div class="border p-3 ">
                    <div class=" row">
                        <div class="col-sm-3"><label for="">Họ và tên:</label></div>
                        <div class="col-sm-9"><b>{{$service->report->user->name}}</b> - ({{($service->report->user->profile->gender) ? "Nữ":"Nam"}})</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Email:</label></div>
                        <div class="col-sm-9">{{$service->report->user->email}}</div>
                    </div>
                    @if(empty($service->report->company->manager_id))
                    <div class="row">
                        <div class="col-sm-3"><label for="">Mã số sinh viên</label></div>
                        <div class="col-sm-9">{{$service->report->user->code}} - (Kỳ {{$service->report->user->student->semester_current}})</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Ngành</label></div>
                        <div class="col-sm-9">{{$service->report->user->student->career->name}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Trạng thái thực tập</label></div>
                        <div class="col-sm-9"> @if($service->report->user->student->status('find')) <span class="text-success">Đúng tiến độ</span> @else <span class="text-warning">Trễ tiến độ</span> @endif </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-3"><label for="">Email cá nhân:</label></div>
                        <div class="col-sm-9">{{$service->report->user->profile->email_personal ?? '---'}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Số điện thoại:</label></div>
                        <div class="col-sm-9">{{$service->report->user->profile->phone}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Ngày sinh:</label></div>
                        <div class="col-sm-9">{{date('d/m/Y', strtotime($service->report->user->profile->birthday))}} </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">CCCD/CMND:</label></div>
                        <div class="col-sm-9">{{$service->report->user->profile->indo ?? '---'}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Địa chỉ:</label></div>
                        <div class="col-sm-9">{{$service->report->user->profile->getAddress()}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><label for="">Giới thiệu:</label></div>
                        <div class="col-sm-9">{{$service->report->user->profile->description ?? '---'}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-2"></div>
            <div class="col-md-10">
                <h5><span class="text-danger">*</span> Lý do sinh viên <span class="font-weight-bold text-info"> {{$service->user->name}}</span> hủy thực tập tại <span class="font-weight-bold text-info"> {{$service->report->company->name}}</span> </h5>
                <div class="row">
                    <div class="col-sm-12 ml-2">
                        <span class="text-dark">{!!$service->description!!}</span>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal-footer" class="modal-footer">
            @if($service->status == SERVICE_STATUS_PENDING)
            <div id="modal-footer" class="modal-footer">
                <input type="submit" name="cancel" class="btn btn-danger" value="Từ chối">
                <input type="submit" name="appoved" class="btn btn-primary" value="Chấp thuận">
            </div>
            @else($service->status == SERVICE_STATUS_CANCEL)
            @endif
        </div>
    </div>
</form>
@endif
<script src="assets/js/vendor.min.js"></script>

<!-- Tost-->
<script src="assets/libs/jquery-toast/jquery.toast.min.js"></script>

<!-- toastr init js-->
<script src="assets/js/pages/toastr.init.js"></script>

<!-- App js -->
<script src="assets/js/app.min.js"></script>