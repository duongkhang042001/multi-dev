<div class="modal-header">
    <h4 class="modal-title text-info" id="myExtraLargeModalLabel"><i class="fa fa-building mr-2"></i>
        Thông tin sinh viên</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <div class="col-12">
        <div class="row">
            <div class="col-sm-12">
                <div>
                    <form class="form-horizontal" role="form">
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Mã số sinh viên</label>
                            <div class="col-10">
                                <input type="text" class="form-control" value="{{ $student->code }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Họ và tên sinh viên</label>
                            <div class="col-10">
                                <input type="text" class="form-control" value="{{ $student->name }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="example-email">Email</label>
                            <div class="col-10">
                                <input type="email" id="example-email" name="example-email" class="form-control"
                                    placeholder="Email" value="{{ $student->email }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Kỳ học hiện tại</label>
                            <div class="col-10">
                                <input type="text" class="form-control"
                                    value="{{ $student->student->current_semester }}">
                            </div>
                        </div>
                        @if (!empty($student->reportReset))
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Trạng thái thực tập</label>
                                <div class="col-10">
                                    @if (!empty($student->reportReset))
                                        @if ($student->reportReset->status == REPORT_PENDING)
                                            <input type="text" class="form-control" value="Đang chờ xử lý">
                                        @elseif($student->reportReset->status == REPORT_FINISHED)
                                            <input type="text" class="form-control" value="Hoàn thành báo cáo">
                                        @elseif($student->reportReset->status == REPORT_PASS)
                                            <input type="text" class="form-control" value="Hoàn thành thực tập">
                                        @elseif($student->reportReset->status == REPORT_FAIL)
                                            <input type="text" class="form-control" value="Không đạt thực tập">
                                        @elseif($student->reportReset->status == REPORT_CANCEL)
                                            <input type="text" class="form-control" value="Đã huỷ thực tập">
                                        @elseif($student->reportReset->status == REPORT_DONE)
                                            <input type="text" class="form-control" value="Đã nộp báo cáo cho doanh nghiệp">
                                        @endif
                                    @else
                                        <input type="text" class="form-control"
                                            value="Báo cáo thực tập đã bị huỷ">
                                    @endif  
                                </div>
                            </div>
                            <hr>
                            @php
                                $request = json_decode($student->reportReset->request_reset);
                            @endphp
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Yều cầu của sinh viên</label>
                                <div class="col-10">
                                    <input type="text" class="form-control"
                                        value="{{ PLAN_RESET_REPORT[$request->request] }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Ngày gửi yêu cầu</label>
                                <div class="col-10">
                                    <input type="date" class="form-control"
                                        value="{{ date('Y-m-d', strtotime($request->created_at)) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Trạng thái</label>
                                <div class="col-10">
                                    <input type="text" class="form-control"
                                        value="{{ $request->status == 0 ? 'Chưa xử lý' : 'Đã xử lý' }}">
                                </div>
                            </div>
                        @endif

                    </form>
                </div>
            </div>

        </div>

    </div>
</div>
<div id="modal-footer" class="modal-footer">
    <input type="submit" name="denied" class="btn btn-danger" data-dismiss="modal" value="Huỷ">
    @if (!empty($student->reportReset) && $request->status == 0)
        <a href="{{ route('staff.request-reset-report.handle', $student->reportReset->id) }}"
            class="btn btn-primary">Xét
            duyệt</a>
    @endif
</div>
