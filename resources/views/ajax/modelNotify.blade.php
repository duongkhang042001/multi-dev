<div class="modal-header">
    <h4 class="modal-title text-success" id="myExtraLargeModalLabel">{{$notify->title}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-2 text-center">
                    <img src="{{$notify->user->getAvatar()}}" onerror="this.src='assets/clients/img/avt-default.png'" height="150">
                </div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-12" style="font-size: 15px;">
                            <span class="text-dark">
                                <h4>Nội dung thông báo</h4>{!!$notify->content!!}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal-footer" class="modal-footer">
    @if($notify->is_active == 0)
    <form action="{{ route('manager.sendNotify', $notify->id) }}" method="POST">
        @method('POST')
        @csrf
        <button rel="tooltip" class="btn btn-info" onclick="return confirm('Bạn Có muốn gửi???');">
            <i class="fab fa-telegram-plane"></i> Gửi Thông Báo
        </button>
    </form>
    @else
    <button type="button" rel="tooltip" class="btn btn-success">
        <i class="fas fa-check"></i> Đã Gửi Thông Báo
    </button>
    @endif
    <!-- <input type="submit" name="pending" class="btn btn-danger" value="Từ chối">
    <input type="submit" name="cancel" class="btn btn-primary" value="Chấp thuận"> -->
</div>