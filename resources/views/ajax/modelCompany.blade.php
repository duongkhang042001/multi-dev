@if($getCompanyClient)
    <div class="modal-body">
        <form action="{{route('student.register.report')}}" method="post" onsubmit="getLoading()">
            @csrf
            <div class="title mb-0">
                <h4><i data-feather="briefcase"></i> Đây có phải doanh nghiệp bạn đang thực tập!</h4>
                <a href="#" class="add-more"><i class="fas fa-check"></i> Xác nhận</a>

            </div>
            <div class="text-left col-sm-12" style="border: 1px dashed #ccc;">
                <p class="">
                    Chú ý:
                    Vui lòng kiểm tra thông tin doanh nghiệp nếu thông tin chính xác vui lòng nhấn <a class="text-info" href="#" onclick="this.form.submit()">xác nhận</a></p>
            </div>
            <div class="content my-5">
                <div class="input-block-wrap">
                    <div class="form-group row">
                        <div class="col-sm-3 ">
                            <div class="thumbnail p-1 border">
                                <img src="{{$company->getAvatar()}}" onerror="this.src='assets/clients/img/avt-default.png'"  alt="" width="100%">
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Tên Doanh Nghiệp</div>
                                </div>
                                <input type="text" class="form-control" placeholder="{{$company->name}}" readonly>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Tên Viết Tắt</div>
                                </div>
                                <input type="text" class="form-control" placeholder="{{$company->short_name}}" readonly>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Số điện thoại</div>
                                </div>
                                <input type="text" class="form-control" placeholder="{{$company->phone}}" readonly>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Website</div>
                                </div>
                                <input type="text" class="form-control" placeholder="{{json_decode($company->url)->url}}" readonly>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Địa chỉ</div>
                                </div>
                                <input type="text" class="form-control" placeholder="{{$company->getAddress()}}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="offset-sm-3 col-sm-9">
                        <div class="buttons">
                            <input type="hidden" name="company_id" value="{{$company->id}}">
                            <button class="primary-bg getPendingApproved" type="submit">Xác nhận</button>
                            <button class="" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@else
    <form action="{{route('staff.company.approve',$company->id)}}" method="POST" onsubmit="getLoading()">
        @csrf
        <div class="modal-header bg-info">
            <h4 class="modal-title text-light" id="myExtraLargeModalLabel"><i class="fe-box mr-2"></i> {{$company->name}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <p class="sub-header">
                Thông tin doanh nghiệp từ form đăng ký của @if(empty($company->manager_id)) <span class="text-warning"> Sinh Viên</span> @else <span class="text-info"> Doanh Nghiệp</span>  @endif
            </p>
            <div class="row mb-3" >
                <div class="col-sm-2">
                    <h4>Nhãn hiệu</h4>
                    <img src="{{$company->getAvatar()}}" alt="Logo-dn" class="w-100"  onerror="this.src='assets/clients/img/logo-default.png'">
                </div>
                <div class="col-md-10">
                    <h4>Thông tin doanh nghiệp</h4>
                    <div class="border p-3 ">
                        <div class=" row">
                            <div class="col-sm-3"><label for="">Tên đầy đủ:</label></div>
                            <div class="col-sm-9"><b>{{$company->name}}</b></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Tên viết tắt:</label></div>
                            <div class="col-sm-9">{{$company->short_name}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Mã số thuế:</label></div>
                            <div class="col-sm-9">{{$company->tax_number}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Website</label></div>
                            <div class="col-sm-9"><a href="{{json_decode($company->url)->url}}">{{json_decode($company->url)->url}}</a></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Email:</label></div>
                            <div class="col-sm-9">{{$company->email}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Số điện thoại:</label></div>
                            <div class="col-sm-9">{{$company->phone}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Lĩnh vực hoạt động:</label></div>
                            <div class="col-sm-9">{{$company->careerGroup->name}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Địa chỉ:</label></div>
                            <div class="col-sm-9">{{$company->getAddress()}}</div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row mb-3" >
                <div class="col-sm-2">
                    <h4>Ảnh đại diện</h4>
                    <div class="thumbnail p-1 border">
                        <img src="{{$company->userRegister->getAvatar()}}" alt="Logo-dn" class="img-fluid"  @if(empty($company->manager_id)) onerror="this.src='assets/clients/img/avt-default.png'" @else  onerror="this.src='assets/clients/img/default-avatar.png'" @endif>
                    </div>
                </div>
                <div class="col-md-10">
                    <h4>{{empty($company->manager_id) ? 'Thông tin sinh viên' : 'Thông tin nhân viên quản lý'}}</h4>
                    <div class="border p-3 ">
                        <div class=" row">
                            <div class="col-sm-3"><label for="">Họ và tên:</label></div>
                            <div class="col-sm-9"><b>{{$company->userRegister->name}}</b> - ({{($company->userRegister->profile->gender) ? "Nữ":"Nam"}})</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Email:</label></div>
                            <div class="col-sm-9">{{$company->userRegister->email}}</div>
                        </div>
                        @if(empty($company->manager_id))
                            <div class="row">
                                <div class="col-sm-3"><label for="">Mã số sinh viên</label></div>
                                <div class="col-sm-9">{{$company->userRegister->code}}- (Kỳ {{$company->userRegister->student->current_semester}})</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3"><label for="">Ngành</label></div>
                                <div class="col-sm-9">{{$company->userRegister->student->career->name}}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3"><label for="">Trạng thái thực tập</label></div>
                                <div class="col-sm-9"> @if($company->userRegister->student->status('find')) <span class="text-success">Đúng tiến độ</span> @else <span class="text-warning">Trễ tiến độ</span> @endif </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-sm-3"><label for="">Email cá nhân:</label></div>
                            <div class="col-sm-9">{{$company->userRegister->profile->email_personal ?? '---'}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Số điện thoại:</label></div>
                            <div class="col-sm-9">{{$company->userRegister->profile->phone}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Ngày sinh:</label></div>
                            <div class="col-sm-9">{{date('d/m/Y', strtotime($company->userRegister->profile->birthday))}} </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">CCCD/CMND:</label></div>
                            <div class="col-sm-9">{{$company->userRegister->profile->indo ?? '---'}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Địa chỉ:</label></div>
                            <div class="col-sm-9">{{$company->getAddress()}}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"><label for="">Giới thiệu:</label></div>
                            <div class="col-sm-9">{{$company->userRegister->profile->description ?? '---'}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modal-footer" class="modal-footer">
                @if (empty($company->manager_id))
                    <input type="hidden" name="idStudent" value="{{$company->userRegister->id}}">
                @endif
                <input type="submit" name="denied" class="btn btn-danger" value="Từ chối">
                <input type="submit" name="approved" class="btn btn-primary" value="Chấp thuận">
            </div>
        </div>
    </form>
@endif
