@if ($ajaxStudent->status == RECRUITMENT_WAITING)
<h4 class=" "><i class="fe-user mr-2"></i>Thông tin sinh viên</h4>
<div class="title-and-info">
    <div class="title">
        <div class="thumb">
            <img src="{{FILE_URL.$ajaxStudent->user->avatar}}"
                onerror="this.src='assets/images/1.png'" class="img-fluid" alt="">
        </div>
        <div class="title-body">
            <h4 class="py-3">{{$ajaxStudent->user->name}}</h4>

            <div class="info">
                <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                    Ngày sinh: {{date('d/m/Y', strtotime($ajaxStudent->user->profile->birthday))}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                    Email: {{$ajaxStudent->user->email}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-cellphone-nfc"></i>
                    Điện thoại: {{$ajaxStudent->user->profile->phone}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-file-document-box-check"></i>
                    Ngành học: {{$ajaxStudent->user->student->career->name}}
                </span>
            </div>
        </div>
    </div>
    <div class="download-resume flex-column justify-content-end text-center">
        @if(!empty($viewCV))
        <a target="_blank" href="{{$viewCV}}" class="btn-block ">Xem CV <i class="mdi mdi-eye  ml-2"></i></a>
        @endif

        @if(!empty($downCV))
        <a href="{{$downCV}}" class="bg-info btn-block">Tải CV <i class="mdi mdi-cloud-download  ml-2"></i></a>
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label font-weight-bold">Nguyện vọng ứng tuyển</label>
    <div class="col-sm-9">
        <textarea style="border:0" class="form-control"
            readonly>{{($ajaxStudent->wish == null) ? 'Sinh viên không điền nguyện vọng' : $ajaxStudent->wish}}</textarea>
    </div>
</div>
<h4 class=" "><i class="fe-check-circle mr-2"></i>Xét duyệt ứng tuyển</h4>
<div class="form-group row">
    <label class="col-md-3 col-form-label">Chọn hình thức</label>
    <div class="col-md-9">
        <div class="package-select border-0">
            <div id="" class="package-select-inputs col-12">
                <div class="form-group">
                    <input class="custom-radio" type="radio" id="radio_1" name="status"
                        value="{{ RECRUITMENT_PENDING_APPROVE }}" checked>
                    <label for="radio_1">
                        <span class="dot"></span> <span class="package-type text-success">Tuyển thẳng</span> <span
                            class="d-block d-sm-inline">Chấp nhận ứng tuyển đợi sinh viên xác nhận</span>
                    </label>
                </div>
                <div class="form-group">
                    <input class="custom-radio" type="radio" id="radio_2" name="status" value="{{RECRUITMENT_PENDING}}">
                    <label for="radio_2">
                        <span class="dot"></span> <span class="package-type text-primary">Phỏng vấn</span> <span
                            class="d-block d-sm-inline">Đồng ý, sau khi sinh viên xác thực sẽ lên lịch phỏng vấn</span>
                    </label>
                </div>
                <div class="form-group">
                    <input class="custom-radio" type="radio" id="radio_3" name="status" value="{{RECRUITMENT_DENIED}}">
                    <label for="radio_3">
                        <span class="dot"></span> <span class="package-type text-danger">Từ chối</span> <span
                            class="d-block d-sm-inline">Từ chối ứng tuyển</span>
                    </label>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <input class="form-control col-sm-10" type="text" id="" name="reason" value=""
                        placeholder="lý do từ chối (nếu có)">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="offset-sm-3 col-sm-9">
        <div class="buttons">
            <input type="hidden" name="id" value="{{$ajaxStudent->id}}">
            <button type="submit" class="primary-bg">XÁC NHẬN</button>
            <button class="" data-dismiss="modal">TẠM ĐÓNG</button>
        </div>
    </div>
</div>
@elseif($ajaxStudent->status == RECRUITMENT_PENDING || $ajaxStudent->status == RECRUITMENT_PENDING_APPROVE || $ajaxStudent->status == RECRUITMENT_INTERVIEW_SUCCESS)
<h4 class=" "><i class="fe-user mr-2"></i>Thông tin sinh viên</h4>
<div class="title-and-info">
    <div class="title">
        <div class="thumb">
            <img src="{{FILE_URL.$ajaxStudent->user->avatar}}"
                onerror="this.src='assets/images/1.png'" class="img-fluid" alt="">
        </div>
        <div class="title-body">
            <h4 class="py-3">{{$ajaxStudent->user->name}}</h4>
            <div class="info">
                <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                    Ngày sinh: {{date('d/m/Y', strtotime($ajaxStudent->user->profile->birthday))}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                    Email: {{$ajaxStudent->user->email}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-cellphone-nfc"></i>
                    Điện thoại: {{$ajaxStudent->user->profile->phone}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-file-document-box-check"></i>
                    Ngành học: {{$ajaxStudent->user->student->career->name}}
                </span>
            </div>
        </div>
    </div>
    <div class="download-resume flex-column justify-content-end text-center">
        @if(!empty($viewCV))
        <a target="_blank" href="{{$viewCV}}" class="btn-block ">Xem CV <i class="mdi mdi-eye  ml-2"></i></a>
        @endif

        @if(!empty($downCV))
        <a href="{{$downCV}}" class="bg-info btn-block">Tải CV <i class="mdi mdi-cloud-download  ml-2"></i></a>
        @endif

    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label font-weight-bold">Nguyện vọng ứng tuyển</label>
    <div class="col-sm-9">
        <textarea class="form-control"
            readonly>{{($ajaxStudent->wish == null) ? 'Sinh viên không điền nguyện vọng' : $ajaxStudent->wish }}</textarea>
    </div>
</div>

<div class="row">
    <div class="offset-sm-3 col-sm-9">
        <div class="buttons">
            <input type="hidden" name="id" value="{{$ajaxStudent->id}}">
            <button class="" data-dismiss="modal">TẠM ĐÓNG</button>
        </div>
    </div>
</div>
@elseif($ajaxStudent->status == RECRUITMENT_INTERVIEW_WAITING)
<h4 class=" "><i class="fe-user mr-2"></i>Thông tin sinh viên</h4>
<div class="title-and-info">
    <div class="title">
        <div class="thumb">
            <img src="{{FILE_URL.$ajaxStudent->user->avatar}}"
                onerror="this.src='assets/images/1.png'" class="img-fluid" alt="">
        </div>
        <div class="title-body">
            <h4 class="py-3">{{$ajaxStudent->user->name}}</h4>
            <div class="info">
                <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                    Ngày sinh: {{date('d/m/Y', strtotime($ajaxStudent->user->profile->birthday))}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                    Email: {{$ajaxStudent->user->email}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-cellphone-nfc"></i>
                    Điện thoại: {{$ajaxStudent->user->profile->phone}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-file-document-box-check"></i>
                    Ngành học: {{$ajaxStudent->user->student->career->name}}
                </span>
            </div>
        </div>
    </div>
    <div class="download-resume flex-column justify-content-end text-center">
        @if(!empty($viewCV))
        <a target="_blank" href="{{$viewCV}}" class="btn-block ">Xem CV <i class="mdi mdi-eye  ml-2"></i></a>
        @endif

        @if(!empty($downCV))
        <a href="{{$downCV}}" class="bg-info btn-block">Tải CV <i class="mdi mdi-cloud-download  ml-2"></i></a>
        @endif

    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label font-weight-bold">Nguyện vọng ứng tuyển</label>
    <div class="col-sm-9">
        <textarea class="form-control"
            readonly>{{($ajaxStudent->wish == null) ? 'Sinh viên không điền nguyện vọng' : $ajaxStudent->wish }}</textarea>
    </div>
</div>
<h4 class=" "><i class="fe-check-circle mr-2"></i>Lên lịch phỏng vấn</h4>
<div id="details" class="row">
    <label class="col-md-3 col-form-label">Thông tin phỏng vấn: </label>
    <div class="row col-md-9">
        <label class="col-md-4 col-form-label">Thời gian bắt đầu</label>
        <div class="col-md-8">
            <div class="form-group">
                <input type="datetime-local" name="interview_start" class="form-control">
                <div id="error_interview_start"></div>
            </div>
        </div>
        <label class="col-md-4 col-form-label">Thời gian kêt thúc</label>
        <div class="col-md-8">
            <div class="form-group">
                <input type="datetime-local" name="interview_end" class="form-control">
                <div id="error_interview_end"></div>
            </div>
        </div>
        <label class="col-md-4 col-form-label">Chọn hình thức</label>
        <div class="form-group col-md-8">
            <select class="form-control" name="type_interview" id="selectBox" onchange="address(this);">
                <option value="">Chọn hình thức</option>
                <option value="meet">Google Meet</option>
                <option value="zoom">Zoom</option>
                <option value="offline">Trực tiếp</option>
            </select>
            <div id="error_type_interview"></div>
        </div>
        <div id="moreInformation" class="col-md-12 row">
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <input type="text" class="form-control" name="infor" placeholder="Thông tin thêm">
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="offset-sm-3 col-sm-9">
        <div class="buttons">
            <input type="hidden" name="id" value="{{$ajaxStudent->id}}">
            <input type="hidden" name="status" value="{{RECRUITMENT_INTERVIEW}}">
            <button type="submit" class="primary-bg">XÁC NHẬN</button>
            <button class="" data-dismiss="modal">TẠM ĐÓNG</button>
        </div>
    </div>
</div>
@elseif($ajaxStudent->status == RECRUITMENT_INTERVIEW && time() >= strtotime($ajaxStudent->interview_start))
<h4 class=" "><i class="fe-user mr-2"></i>Thông tin sinh viên</h4>
<div class="title-and-info">
    <div class="title">
        <div class="thumb">
            <img src="{{FILE_URL.$ajaxStudent->user->avatar}}"
                onerror="this.src='assets/images/1.png'" class="img-fluid" alt="">
        </div>
        <div class="title-body">
            <h4 class="py-3">{{$ajaxStudent->user->name}}</h4>
            <div class="info">
                <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                    Ngày sinh: {{date('d/m/Y', strtotime($ajaxStudent->user->profile->birthday))}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                    Email: {{$ajaxStudent->user->email}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-cellphone-nfc"></i>
                    Điện thoại: {{$ajaxStudent->user->profile->phone}}
                </span>
                <br>
                <span class="candidate-location"><i class="mdi mdi-file-document-box-check"></i>
                    Ngành học: {{$ajaxStudent->user->student->career->name}}
                </span>
            </div>
        </div>
    </div>
    <div class="download-resume flex-column justify-content-end text-center">
        @if(!empty($viewCV))
        <a target="_blank" href="{{$viewCV}}" class="btn-block ">Xem CV <i class="mdi mdi-eye  ml-2"></i></a>
        @endif

        @if(!empty($downCV))
        <a href="{{$downCV}}" class="bg-info btn-block">Tải CV <i class="mdi mdi-cloud-download  ml-2"></i></a>
        @endif
    </div>
</div>
<div id="details" class="row">
    <label class="col-md-3 col-form-label">Thông tin phỏng vấn: </label>
    <div class="row col-md-9">
        <label class="col-md-4 col-form-label">Hình thức phỏng vấn</label>
        @if (!json_decode($ajaxStudent->interview)->online)
        <div class="form-group col-md-8">
            Trực tiếp
        </div>
        @elseif(json_decode($ajaxStudent->interview)->app=='zoom')
        <div class="form-group col-md-8">
            Online ( Zoom )
        </div>
        @elseif(json_decode($ajaxStudent->interview)->app=='meet')
        <div class="form-group col-md-8">
            Online ( Google Meet )
        </div>
        @endif
        <label class="col-md-4 col-form-label">Thời gian bắt đầu:</label>
        <div class="col-md-8">
            <div class="form-group">
                {{date('d/m/Y H:i', strtotime($ajaxStudent->interview_start))}}
                <div id="error_interview_start"></div>
            </div>
        </div>
        <label class="col-md-4 col-form-label">Thời gian kết thúc:</label>
        <div class="col-md-8">
            <div class="form-group">
                {{date('d/m/Y H:i', strtotime($ajaxStudent->interview_end))}}
                <div id="error_interview_end"></div>
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label">Đánh giá kết quả phỏng vấn</label>
    <div class="col-md-9">
        <div class="package-select border-0">
            <div id="" class="package-select-inputs col-12">
                <div class="form-group">
                    <input class="custom-radio" type="radio" onclick="showReason(0)" id="radio_1" name="rate"
                        value="{{ RECRUITMENT_INTERVIEW_SUCCESS }}" checked>
                    <label for="radio_1">
                        <span class="dot"></span> <span class="package-type text-success">Thành công</span> <span
                            class="d-block d-sm-inline">Sinh viên đậu phỏng vấn</span>
                    </label>
                </div>
                <div class="form-group">
                    <input class="custom-radio" onclick="showReason(1)" type="radio" id="radio_3" name="rate"
                        value="{{RECRUITMENT_DENIED}}">
                    <label for="radio_3">
                        <span class="dot"></span> <span class="package-type text-danger">Từ chối</span> <span
                            class="d-block d-sm-inline">Sinh viên không đậu phỏng vấn</span>
                    </label>
                </div>
                <div class="form-group row" id="reason_fail">

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="offset-sm-3 col-sm-9">
        <div class="buttons">
            <input type="hidden" name="id" value="{{$ajaxStudent->id}}">
            <input type="hidden" name="status" value="{{RECRUITMENT_INTERVIEW_SUCCESS}}">
            <button type="submit" class="primary-bg">XÁC NHẬN</button>
            <button class="" data-dismiss="modal">TẠM ĐÓNG</button>
        </div>
    </div>
</div>
@elseif($ajaxStudent->status == RECRUITMENT_INTERVIEW && time() < strtotime($ajaxStudent->interview_start))
    <h4 class=" "><i class="fe-user mr-2"></i>Thông tin sinh viên</h4>
    <div class="title-and-info">
        <div class="title">
            <div class="thumb">
                <img src="{{FILE_URL.$ajaxStudent->user->avatar}}"
                    onerror="this.src='assets/images/1.png'" class="img-fluid" alt="">
            </div>
            <div class="title-body">
                <h4 class="py-3">{{$ajaxStudent->user->name}}</h4>
                <div class="info">
                    <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                        Ngày sinh: {{date('d/m/Y', strtotime($ajaxStudent->user->profile->birthday))}}
                    </span>
                    <br>
                    <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                        Email: {{$ajaxStudent->user->email}}
                    </span>
                    <br>
                    <span class="candidate-location"><i class="mdi mdi-cellphone-nfc"></i>
                        Điện thoại: {{$ajaxStudent->user->profile->phone}}  
                    </span>
                    <br>
                    <span class="candidate-location"><i class="mdi mdi-file-document-box-check"></i>
                        Ngành học: {{$ajaxStudent->user->student->career->name}}
                    </span>
                </div>
            </div>
        </div>
        <div class="download-resume flex-column justify-content-end text-center">
            <a target="_blank" href="{{FILE_URL.$ajaxStudent->viewCvfile}}" class="btn-block ">Xem CV <i
                    class="mdi mdi-eye  ml-2"></i></a>
            <a href="{{FILE_URL.$ajaxStudent->cvfile}}" class="bg-info btn-block">Tải CV <i
                    class="mdi mdi-cloud-download  ml-2"></i></a>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label font-weight-bold">Nguyện vọng ứng tuyển</label>
        <div class="col-sm-9">
            <textarea class="form-control"
                readonly>{{($ajaxStudent->wish == null) ? 'Sinh viên không điền nguyện vọng' : $ajaxStudent->wish }}</textarea>
        </div>
    </div>
    <h4 class=" "><i class="fe-check-circle mr-2"></i>Lịch phỏng vấn</h4>
    <div id="details" class="row">
        <label class="col-md-3 col-form-label">Thông tin phỏng vấn: </label>
        <div class="row col-md-9">
            <label class="col-md-4 col-form-label">Thời gian bắt đầu:</label>
            <div class="col-md-8">
                <div class="form-group">
                    <input type="datetime-local" name="interview_start" value="{{substr(date("c",strtotime($ajaxStudent->interview_start)), 0, 16); }}" class="form-control">
                    <div id="error_interview_start"></div>
                </div>
            </div>
            <label class="col-md-4 col-form-label">Thời gian kết thúc:</label>
            <div class="col-md-8">
                <div class="form-group">
                    <input type="datetime-local" name="interview_end" value="{{substr(date("c",strtotime($ajaxStudent->interview_end)), 0, 16); }}" class="form-control">
                    <div id="error_interview_end"></div>
                </div>
            </div>
            <label class="col-md-4 col-form-label">Chọn hình thức:</label>
            <div class="form-group col-md-8">
                <select class="form-control" name="type_interview" id="selectBox" onchange="address(this);">
                    <option value="">Chọn hình thức</option>
                    <option value="meet" @if (json_decode($ajaxStudent->interview)->app=='meet')
                        selected
                        @endif>Google Meet</option>
                    <option value="zoom" @if (json_decode($ajaxStudent->interview)->app=='zoom')
                        selected
                        @endif>Zoom</option>
                    <option value="offline" @if (!json_decode($ajaxStudent->interview)->online)
                        selected
                        @endif>Trực tiếp</option>
                </select>
                <div id="error_type_interview"></div>
            </div>

            <div id="moreInformation" class="col-md-12 row">@if (json_decode($ajaxStudent->interview)->app=='meet')
                <label class="col-md-4 col-form-label">Đường dẫn:</label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" name="url" value="{{json_decode($ajaxStudent->interview)->url}}"
                            class="form-control">
                        <div id="error_url"></div>
                    </div>
                </div>
                @elseif(json_decode($ajaxStudent->interview)->app=='zoom')
                <label class="col-md-4 col-form-label">ID cuộc họp:</label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" name="id_url" value="{{json_decode($ajaxStudent->interview)->room}}"
                            class="form-control">
                        <div id="error_id_url"></div>
                    </div>
                </div>
                <label class="col-md-4 col-form-label">Mật khẩu:</label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" name="pass_url" value="{{json_decode($ajaxStudent->interview)->password}}"
                            class="form-control">
                        <div id="error_pass_url"></div>
                    </div>
                </div>
                @elseif(!json_decode($ajaxStudent->interview)->online)
                <label class="col-md-4 col-form-label">Địa điểm:</label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" name="address" value="{{json_decode($ajaxStudent->interview)->location}}"
                            class="form-control">
                        <div id="error_address"></div>
                    </div>
                </div>
                @endif
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <input type="text" class="form-control" name="infor" value="{{$ajaxStudent->interview_information}}"
                        placeholder="Thông tin thêm">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="offset-sm-3 col-sm-9">
            <div class="buttons">
                <input type="hidden" name="id" value="{{$ajaxStudent->id}}">
                <input type="hidden" name="status" value="{{RECRUITMENT_INTERVIEW}}">
                <button type="submit" class="primary-bg">XÁC NHẬN</button>
                <button class="" data-dismiss="modal">TẠM ĐÓNG</button>
            </div>
        </div>
    </div>
    @elseif($ajaxStudent->status == RECRUITMENT_CANCEL)
    <h4 class=" "><i class="fe-user mr-2"></i>Thông tin sinh viên</h4>
    <div class="title-and-info">
        <div class="title">
            <div class="thumb">
                <img src="{{FILE_URL.$ajaxStudent->user->avatar}}"
                    onerror="this.src='assets/images/1.png'" class="img-fluid" alt="">
            </div>
            <div class="title-body">
                <h4 class="py-3">{{$ajaxStudent->user->name}}</h4>
                <div class="info">
                    <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                        Ngày sinh: {{date('d/m/Y', strtotime($ajaxStudent->user->profile->birthday))}}
                    </span>
                    <br>
                    <span class="candidate-location"><i class="mdi mdi-email-check-outline"></i>
                        Email: {{$ajaxStudent->user->email}}
                    </span>
                    <br>
                    <span class="candidate-location"><i class="mdi mdi-cellphone-nfc"></i>
                        Điện thoại: {{$ajaxStudent->user->profile->phone}}
                    </span>
                    <br>
                    <span class="candidate-location"><i class="mdi mdi-file-document-box-check"></i>
                        Ngành học: {{$ajaxStudent->user->student->career->name}}
                    </span>
                </div>
            </div>
        </div>
        <div class="download-resume flex-column justify-content-end text-center">
            @if(!empty($viewCV))
            <a target="_blank" href="{{$viewCV}}" class="btn-block ">Xem CV <i class="mdi mdi-eye  ml-2"></i></a>
            @endif
    
            @if(!empty($downCV))
            <a href="{{$downCV}}" class="bg-info btn-block">Tải CV <i class="mdi mdi-cloud-download  ml-2"></i></a>
            @endif
        </div>
    </div>
    <div id="details" class="row">
        <label class="col-md-3 col-form-label">Thông tin phỏng vấn: </label>
        <div class="row col-md-9">
            <label class="col-md-4 col-form-label">Hình thức phỏng vấn</label>
            @if (!json_decode($ajaxStudent->interview)->online)
            <div class="form-group col-md-8">
                Trực tiếp
            </div>
            @elseif(json_decode($ajaxStudent->interview)->app=='zoom')
            <div class="form-group col-md-8">
                Online ( Zoom )
            </div>
            @elseif(json_decode($ajaxStudent->interview)->app=='meet')
            <div class="form-group col-md-8">
                Online ( Google Meet )
            </div>
            @endif
            <label class="col-md-4 col-form-label">Thời gian bắt đầu:</label>
            <div class="col-md-8">
                <div class="form-group">
                    {{date('d/m/Y H:i', strtotime($ajaxStudent->interview_start))}}
                </div>
            </div>
            <label class="col-md-4 col-form-label">Thời gian kết thúc:</label>
            <div class="col-md-8">
                <div class="form-group">
                    {{date('d/m/Y H:i', strtotime($ajaxStudent->interview_end))}}
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-3 col-form-label">Lí do sinh viên hủy phỏng vấn:</label>
        <div class="col-md-9">
            <p class="text-danger">{{$ajaxStudent->reason}}</p>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-3 col-form-label">Phỏng vấn lại:</label>
        <div class="col-md-9">
            <div class="package-select border-0">
                <div id="" class="package-select-inputs  col-12">

                    <div class="form-group">
                        <input class="custom-radio" onclick="showinterviewagian()" type="radio" id="radio_3" name="rate"
                            value="{{RECRUITMENT_DENIED}}">
                        <label for="radio_3">
                            <span class="dot"></span> <span class="package-type text-success">Xác nhận</span> <span
                                class="d-block d-sm-inline">Lên lại lịch phỏng vấn cho sinh viên</span>
                        </label>
                    </div>
                    <div class="form-group row" id="reason_fail">
    
                    </div>
                </div>
                <div id="showinterviewagian"></div>
               
            </div>
            
        </div>
    </div>
    <div class="row">
        <div class="offset-sm-3 col-sm-9">
            <div class="buttons">
                <input type="hidden" name="id" value="{{$ajaxStudent->id}}">
                <input type="hidden" name="status" value="{{RECRUITMENT_INTERVIEW}}">
                <button type="submit" class="primary-bg" id="submitinterviewagain" hidden>XÁC NHẬN</button>
                <button class="" data-dismiss="modal">TẠM ĐÓNG</button>
            </div>
        </div>
    </div>
    @endif