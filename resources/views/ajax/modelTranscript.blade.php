<div class="modal-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title">{{$transcript->user->name}}</h4>
                <form action="{{route('staff.transcript.store',$transcript->user->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <p>Tệp bảng điểm sinh viên</p>
                        <input type="file" name="file" class="filestyle" data-btnClass="btn-primary">
                    </div>
               
                    <button type="submit" class="btn btn-info"> <i class="mdi mdi-send mr-1"></i>Cấp bảng điểm</button>
              
                </form>
                
            </div>
        </div>
    </div>
</div>
<script src="assets/js/vendor.min.js"></script>

<script src="assets/libs/switchery/switchery.min.js"></script>
<script src="assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
<script src="assets/libs/select2/select2.min.js"></script>
<script src="assets/libs/jquery-mockjax/jquery.mockjax.min.js"></script>
<script src="assets/libs/autocomplete/jquery.autocomplete.min.js"></script>
<script src="assets/libs/bootstrap-select/bootstrap-select.min.js"></script>
<script src="assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script src="assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="assets/libs/bootstrap-filestyle2/bootstrap-filestyle.min.js"></script>

<!-- Init js-->

<!-- App js -->
<script src="assets/js/app.min.js"></script>