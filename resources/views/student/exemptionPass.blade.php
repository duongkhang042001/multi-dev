@extends('layouts.client_guest')
@section('page-title', 'Sinh viên')
@section('title', 'Sinh viên')


@section('content')
<!-- <div class="alice-bg padding-top-60 section-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 m-auto" style="margin: auto;object-fit: cover;">
                <img src="assets/clients/img/exemption-success.jpg" class=""  alt="">
            </div>
            <div class="col-sm-6 text-left" style="width: 400px;margin: auto;">
                <h5 class=" p-4 text-primary">Chúc mừng bạn đã hoàn thành thực tập</h5>
                <div class="info" style="padding-left: 20px;">
                    <p>- Thân chào : {{$user->name}},</p>
                    <p>- Nhà trường xin chúc mừng bạn đã được miễn giảm thực tập thành công . Hồ sơ của bạn được chấp thuận vào ngày {{ date('d-m-Y h:i:s', strtotime($user->updated_at)) }} .</p>
                    <p>- Trân trọng cảm ơn bạn đã sử dụng yêu cầu miễn giảm thực tập và chúc bạn thật nhiều thành công .</p>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="alice-bg padding-top-60 section-padding-bottom">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="candidate-details" style="min-height: 800px;">
          <div class="row flex-column">
            <div class="col-lg-6 mx-auto">
              <img src="assets/clients/img/complete-internship.jpg" class="w-100" style="object-fit: cover;" />
              
            </div> 
          </div>
          <h1 class="text-center text-warning mt-3 d-none d-md-block">{{$user->name}}</h1>
          <h3 class="text-center text-warning mt-3 d-md-none">{{$user->name}}</h3>
          <div class="row flex-column mt-3">
            <div class="col-xl-1 col-sm-2 col-3 mx-auto">
              <img src="assets/clients/img/icon-internshipPass.png" class="w-100" style="object-fit: cover;" />
            </div>
          </div>
          <div class="mt-5">
            <div class="info text-center mt-3 h2">
                <p class="h3">Nhà trường xin chúc mừng bạn đã hoàn thành thực tập.</p>
                <p>Chúc bạn thật nhiều sức khoẻ và thành công .</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection