@extends('layouts.client')
@section('page-title', 'Đăng ký doanh nghiệp thực tập')
@section('title', 'Đăng ký doanh nghiệp')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tổng quan</a></li>
            <li class="breadcrumb-item active" aria-current="page">Đăng ký doanh nghiệp thực tập</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="dashboard-content-wrapper">
        <form action="{{ Route('student.company.store') }}" method="POST" class="dashboard-form"
              autocomplete="off" enctype="multipart/form-data">
            @csrf
            @method('POST')
            <div class="dashboard-section basic-info-input">
                <h4><i data-feather="user-check"></i>Thông tin doanh nghiệp thực tập</h4>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Tên doanh nghiệp <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" value="{{old('name')}}" class="form-control" placeholder="" name="name" >
                        @error('name')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Tên viết tắt <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" value="{{old('short_name')}}" class="form-control" placeholder="" name="short_name">
                        @error('short_name')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Mã số thuế <span
                            class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" value="{{old('tax_number')}}" class="form-control" placeholder="" name="tax_number" id="tax_number">
                        @error('tax_number')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Email <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" value="{{old('email')}}" class="form-control" placeholder="email@example.com" name="email">
                        @error('email')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Số điện thoại <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" value="{{old('phone')}}" class="form-control" placeholder="" name="phone">
                        @error('phone')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Website <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" value="{{old('url')}}" class="form-control" placeholder="website.com" name="url">
                        @error('url')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div id="" class="form-group row">
                    <label class="col-md-3 col-form-label">Lĩnh vực <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                            <select class="form-control" name="careerGroup">
                                <option value="">Lĩnh vực hoạt động</option>
                                @foreach ($careerGroups as $item)
                                    <option value="{{$item->id}}" @if(old('careerGroup') == $item->id) selected @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                            @error('careerGroup')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Ngày thành lập <span class="text-danger">*</span></label>
                    <div class="col-sm-6 col-md-3">
                        <input type="date" value="{{old('founded_at')}}" name="founded_at" class="form-control">
                        @error('founded_at')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div  class="form-group row">
                    <label class="col-md-3 col-form-label">Địa chỉ <span class="text-danger">*</span> </label>
                    <div class="col-md-9">
                            <input type="text" name="address" class="form-control" placeholder="123 Lạc Long Quân" value="{{ old('address') }}">
                            @error('address')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                    </div>
                </div>
                @livewire('change-address-custom', ['cityId' => old('city'),'districtId' => old('district'),'wardId'
                              =>old('ward')])
            </div>
            <div class="dashboard-section basic-info-input">
                <div class="row">
                    <div class="col-md-9 offset-md-3">
                        <div class="form-group mt-0 terms">
                            <input class="custom-radio" type="checkbox" id="radio" name="termsandcondition" required checked>
                            <label for="radio">
                                <span class="dot"></span> Đồng ý xác nhận thông tin doanh nghiệp thực tập.
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                        <button class="button">Lưu thông tin</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="experience dashboard-section details-section">
        <div class="modal fade modal-accept" id="modal-company" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" id="modal-content">
                </div>
            </div>
        </div>
    </div>

@endsection


@push('script')

    <script type="text/javascript">
       window.onload = () => {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.checkTimeline','find') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                },
                success: function(data) {
                    if(!data){
                        Swal.fire({
                            title: 'Trễ Hạn Thực Tập',
                            text: "Bạn đã trễ hạn thực tập, nếu bạn tiếp tục báo cáo thực tập của bạn sẽ được nộp vào kỳ sau!",
                            type: "warning",
                            showCancelButton: !0,
                            confirmButtonText: "Hủy đăng ký",
                            cancelButtonText: "Tiếp tục",
                            confirmButtonClass: "btn btn-lg btn-warning mt-2",
                            cancelButtonClass: "btn btn-lg btn-secondary ml-2 mt-2",
                            buttonsStyling: !1
                        }).then(function (t) {
                            if(t.value) window.history.back();
                        })
                    }
                },
                error: function() {
                    console.log('Something Error!')
                }
            })
        }

        $("#tax_number").focusout(function(){
            let taxNum = $(this).val().trim()
            if (/^\d+$/g.test(taxNum) && (taxNum.length == 10 || taxNum.length == 13)) {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.company') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        tax_number: taxNum,
                    },
                    success: function(data) {
                        if(data)
                        {
                            $('#modal-content').html(data)
                            $('#modal-company').modal('show')
                        }
                    },
                    error: function() {
                        console.log('Something Error!')
                    }
                })
            }
        });
    </script>
@endpush
@push('css')
    <style>
        #modal-content .input-group-text{
            width: 130px!important;
        }
    </style>
@endpush
