@extends('layouts.client')
@section('page-title', 'Báo cáo thực tập')
@section('title', 'Báo cáo thực tập')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">Báo cáo thực tập</li>
        </ol>
    </nav>
@endsection

@section('content')
    @if (empty($report))
        <div class="dashboard-content-wrapper">
            <div class="input-block-wrap">
                <div class="text-center col-sm-12" style="font-size: 15px;  border: 1px dashed #ccc; padding: 15px 10px;">
                    <h5 class="text-primary">Thông Báo!</h5>
                    <p class="">
                        Bạn chưa có doanh nghiệp thực tập.
                    </p>
                </div>
            </div>
        </div>
    @elseif(!empty($report->company) && $report->company->status == COMPANY_STATUS_WAITING)
        <div class="dashboard-content-wrapper dashboard-section">
            <div class="input-block-wrap">
                <div class="text-center col-sm-12" style="font-size: 15px;  border: 1px dashed #ccc; padding: 15px 10px;">
                    <h5 class="text-primary">Thông Báo!</h5>
                    <p class="">
                        Thông tin doanh nghiệp thực tập của bạn đang được phê duyệt!
                    </p>
                </div>
            </div>
            <div class="information-and-contact">
                <div class="information">
                    <h4>Thông Tin Đăng Ký</h4>
                    <ul>
                        <li><span>Tên doanh nghiệp:</span> {{$report->company->name}}</li>
                        <li><span>Tên viết tắt:</span> {{$report->company->short_name}}</li>
                        <li><span>Mã số thuế:</span> {{$report->company->tax_number}}</li>
                        <li><span>Số điện thoại:</span> {{$report->company->phone}}</li>
                        <li><span>Email:</span> {{$report->company->email}}</li>
                        <li><span>Thành phố:</span> {{$report->company->city_name}}</li>
                        <li><span>Website:</span> {{json_decode($report->company->url)->url}}</li>
                        <li><span>Địa chỉ:</span> {{$report->company->address.' '.$report->company->ward_name.' '.$report->company->district_name}}</li>
                    </ul>
                    <p class="mt-5 font-italic text-warning">(Đang chờ phê duyệt)</p>
                </div>
            </div>
        </div>
    @elseif($report->status == REPORT_CANCEL)
    <div class="dashboard-content-wrapper">
        <div class="input-block-wrap">
            <div class="text-center col-sm-12" style="font-size: 15px;  border: 1px dashed #ccc; padding: 15px 10px;">
                <h5 class="text-primary">Thông Báo!</h5>
                @if ($report->serviceCancel->roleUser->id == STUDENT_ROLE_ID && $report->serviceCancel->status == SERVICE_STATUS_APPROVED)
                <p class="">
                    Yêu cầu huỷ thực tập của bạn đã được nhà trường đồng ý!.
                </p>
                @elseif (!empty($report->serviceCancel) && $report->serviceCancel->roleUser->id == COMPANY_ROLE_ID && $report->serviceCancel->status == SERVICE_STATUS_APPROVED)
                <p class="">
                    Yêu cầu huỷ thực tập của doanh nghiệp đã được nhà trường đồng ý.
                </p>            
                @elseif(!empty($report->serviceCancel) && $report->serviceCancel->status == SERVICE_STATUS_DENINED)
                <p class="">
                    Yêu cầu huỷ thực tập của bạn không được chấp nhận. Liên hệ với nhà trường để biết thêm thông tin chi tiết!
                </p>
                @endif
            </div>
        </div>
    </div>
    @else
        <div class="dashboard-content-wrapper">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                        role="tab" aria-controls="nav-profile" aria-selected="false">Báo cáo thực tập</a>
                    <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                        aria-controls="nav-home" aria-selected="true">Thông tin thực tập</a>
                    <a class="nav-item nav-link" id="nav-report-tab" data-toggle="tab" href="#nav-report" role="tab"
                        aria-controls="nav-report" aria-selected="false">Báo cáo hàng tuần</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="dashboard-apply-area">
                        <div class="information-and-contact information my-5">
                            <div class="input-block-wrap">
                                <div class="row">
                                    <div class="text-left col-sm-12"
                                        style="font-size: 15px; margin-top: 8px; border: 1px dashed #ccc; padding: 5px 10px;">
                                        Lưu ý: <br>
                                        1. Sinh viên phải hoàn thành báo cáo trong thời gian quy định của nhà
                                        trường.<br>
                                        2. Sinh viên vui lòng suy nghĩ thật kỹ khi đưa ra quyết định. Sinh viên nên cân
                                        nhẳc điều này. <br>
                                        3. Sinh viên vui lòng suy nghĩ thật kỹ khi đưa ra quyết định. Sinh viên muốn <a
                                            href="{{ route('student.service.intershipCancle.index') }}"><span
                                                class="text-danger font-weight-bold">hủy thực tập</span></a> tại đây. <br>
                                        4. Thời gian nộp báo cáo trước ngày:
                                        {{ date('d/m/Y', strtotime($timeLine->end)) }} </br>
                                        <a href="{{route('postDetail','huong-dan-viet-bao-cao-thuc-tap')}}" target="_blank" class="text-primary">5. Hướng dẫn viết báo cáo thực tập</a>

                                    </div>
                                    <div class="mt-5">
                                        @if ($report->status == REPORT_PASS)
                                            <div class="mt-2 text-info">
                                                Chúc mừng bạn đã hoàn thành thực tập. Chúc bạn thành công trên con đường sắp
                                                tới!.
                                            </div>
                                        @elseif($report->status == REPORT_FAIL)
                                            <div class="text-danger">
                                                Bạn đã rớt thực tập. Vui lòng liên hệ nhà để được hướng dẫn thực tập lại vào
                                                kỳ sau.
                                            </div>
                                        @else
                                            @if ($report->status == REPORT_PENDING)
                                                <a href="{{ route('student.report-done') }}" class="btn btn-outline-info"
                                                    style="font-size: 14px;">Hoàn thành báo cáo
                                                </a>
                                            @elseif ($report->status == REPORT_DONE && $report->is_outside == 1)
                                                 <div class="mb-2 text-info">
                                                    Bạn đã hoàn thành báo cáo thực tập. Vui lòng tải lên file nhận xét của doanh nghiệp.
                                                </div>
                                            @elseif ($report->status == REPORT_DONE)
                                                <div class="mb-2 text-info">
                                                    Bạn đã hoàn thành báo cáo thực tập. Vui lòng đợi nhận xét của doanh
                                                    nghiệp và nhà trường.
                                                </div>
                                            @endif
                                            @if ($report->status == REPORT_DONE || $report->status == REPORT_FINISHED)
                                                @if (!$report->company->on_system || $report->is_outside)
                                                    @if (empty($report->rate_file))
                                                        <a class="btn btn-outline-danger" style="font-size: 14px;"
                                                            data-toggle="modal" data-target="#modal-upload">Tải lên nhận xét
                                                            của doanh nghiệp</a>
                                                    @endif
                                                @endif
                                            @endif
                                            @if ($report->status == REPORT_FINISHED)
                                                <div class="text-info">
                                                    Doanh nghiệp đã đánh giá quá trình thực tập của bạn. Vui lòng chờ đợi
                                                    kết quả thực tập
                                                    từ nhà trường.
                                                </div>
                                            @endif
                                            @if (!empty($report->rate_file))
                                                <div class="download-resume dashboard-section mt-2">
                                                    <a href="{{ \App\Http\Controllers\Controller::getFilePdf($report->rate_file) }}"
                                                        target="_blank">Xem nhận xét thực tập <span
                                                            data-feather="eye"></span></a>
                                                </div>
                                            @endif
                                        @endif
                                        @if (!empty($report) && !\App\Http\Controllers\Controller::checkTimeLine('report'))
                                            <div class="mt-4">
                                                Bạn đã hoàn thành muộn báo cáo thực tập. Vui lòng gửi <span
                                                    class="text-danger pointer" onclick="checkFinish(event)">yêu
                                                    cầu</span>
                                                xử lý cho nhà trường.<br>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @php
                            $reportRate = !empty($report->reportRate) ? $report->reportRate : null;
                        @endphp
                        @if (!empty($reportRate))
                            <div class="information-and-contact mb-4">
                                <div class="information">
                                    <h4>Nhận xét của doanh nghiệp</h4>
                                    <div class="px-0 col-sm-12">
                                        <table class="display nowrap table table-hover table-striped table-bordered"
                                            data-select2-id="5">
                                            <thead>
                                                <tr>
                                                    <th style="width: 200px;">Ưu điểm:</th>
                                                    <td>{{ $reportRate->advantages }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Hạn chế:</th>
                                                    <td>{{ $reportRate->defect }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Đề xuất, góp ý:</th>
                                                    <td>{{ $reportRate->content }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Thái độ, ý thức:</th>
                                                    <td>{{ $reportRate->attitude_point }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Kết quả công việc:</th>
                                                    <td>{{ $reportRate->work_point }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Đánh giá cuối cùng:</th>
                                                    <td>
                                                        {{ $reportRate->status == 1 ? 'Không đạt' : 'Đạt' }}
                                                    </td>
                                                </tr>
                                            </thead>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="information-and-contact mb-4">
                            <div class="information">
                                <h4>Thông tin thực tập</h4>
                                <div class="px-0 col-sm-12">
                                    <ul class="row">
                                        <div class="px-0 col-sm-6">
                                            <li><span>Sinh
                                                    viên:</span>{{ !empty($report->user->name) ? $report->user->name : null }}
                                            </li>
                                            <li><span>Vị
                                                    trí:</span>{{ !empty($report->intern_position) ? $report->intern_position : null }}
                                            </li>
                                            <li><span>Điện
                                                    thoại:</span>{{ !empty($report->user->profile->phone) ? $report->user->profile->phone : null }}
                                            </li>
                                            <li><span>Email:</span>{{ !empty($report->user->email) ? $report->user->email : null }}
                                            </li>
                                        </div>
                                        <div class="px-0 col-sm-6">
                                            <li><span>Người hướng
                                                    dẫn:</span>{{ !empty($report->tutor_name) ? $report->tutor_name : null }}
                                            </li>
                                            <li><span>Vị
                                                    trí:</span>{{ !empty($report->tutor_position) ? $report->tutor_position : null }}
                                            </li>
                                            <li><span>Điện
                                                    thoại:</span>{{ !empty($report->tutor_phone) ? $report->tutor_phone : null }}
                                            </li>
                                            <li><span>Email:</span>{{ !empty($report->tutor_email) ? $report->tutor_email : null }}
                                            </li>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="edication-background details-section dashboard-section my-5">
                            <h4><i data-feather="align-left"></i>GIỚI THIỆU KHÁI QUÁT VỀ ĐƠN VỊ THỰC TẬP</h4>
                            @if (!empty($report->about) && !empty($report->function_area_activities) && !empty($report->product_services) && !empty($report->organization) && !empty($report->strategy_future))
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Tóm lược quá trình hình thành và phát triển</h5>
                                    </span>
                                    {!! !empty($report->about) ? $report->about : 'Chưa có thông tin!' !!}
                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Chức năng và lĩnh vực hoạt động</h5>
                                    </span>
                                    {!! !empty($report->function_area_activities) ? $report->function_area_activities : 'Chưa có thông tin!' !!}
                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Sản phẩm và dịch vụ</h5>
                                    </span>
                                    {!! !empty($report->product_services) ? $report->product_services : 'Chưa có thông tin!' !!}
                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Tổ chức quản lý hành chính, nhân sự</h5>
                                    </span>
                                    {!! !empty($report->organization) ? $report->organization : 'Chưa có thông tin!' !!}
                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Chiến lược và phương hướng phát triển của đơn vị trong tương lai</h5>
                                    </span>
                                    {!! !empty($report->strategy_future) ? $report->strategy_future : 'Chưa có thông tin!' !!}
                                </div>
                            @else
                                <div class="education-label">
                                    Chưa đủ thông tin!
                                </div>
                            @endif

                        </div>
                        <div class="edication-background details-section dashboard-section">
                            <h4><i data-feather="align-left"></i>BÁO CÁO NỘI DUNG CÔNG VIỆC THỰC TẬP</h4>
                            @if (!empty($report->summary_activities) && !empty($report->work))
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Giới thiệu tóm tắt các hoạt động/công việc tại đơn vị thực tập</h5>
                                    </span>
                                    {!! !empty($report->summary_activities) ? $report->summary_activities : 'Chưa có thông tin!' !!}
                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Mô tả các công việc được phân công thực hiện hoặc được tham gia tại đơn vị </h5>
                                    </span>
                                    {!! !empty($report->work) ? $report->work : 'Chưa có thông tin!' !!}
                                </div>
                            @else
                                <div class="education-label">
                                    Chưa đủ thông tin!
                                </div>
                            @endif

                        </div>
                        <div class="experience dashboard-section details-section">
                            <h4><i data-feather="briefcase"></i>BÁO CÁO CÔNG TÁC THEO TUẦN</h4>
                            @if (!empty($reportDetail->count()))
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    @foreach ($allWeek as $key => $week)
                                        <li class="nav-item">
                                            <a class="nav-link @if ($key == 1) active @endif"
                                                id="privacy-tab{{ $key }}" href="#privacy{{ $key }}"
                                                data-toggle="tab" role="tab" aria-controls="privacy{{ $key }}"
                                                aria-selected="true"> Tuần
                                                {{ $key }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content" id="myTabContent" style="font-size: 14px">
                                    @foreach ($allWeek as $key => $week)
                                        <div class="tab-pane fade @if ($key == 1) active show @endif" id="privacy{{ $key }}"
                                            role="tabpanel" aria-labelledby="privacy-tab{{ $key }}"
                                            class="mb-0">
                                            <div class="tab-body mt-3">
                                                <table class="table table-bordered" style="font-size: ">
                                                    <thead>
                                                        <th style="width: 30%; text-align: center;">
                                                            {{ date('d/m/Y', strtotime($week['start_week'])) }} -
                                                            {{ date('d/m/Y', strtotime($week['end_week'])) }}
                                                        </th>
                                                        <th style="width: 35%; text-align: center;">
                                                            <span>Sáng</span>
                                                        </th>
                                                        <th style="width: 35%; text-align: center;">
                                                            <span>Chiều</span>
                                                        </th>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($reportDetail as $item)
                                                            @if (strtotime($item->date) <= strtotime($week['end_week']) && strtotime($item->date) >= strtotime($week['start_week']))
                                                                <tr>
                                                                    <td class="text-center">
                                                                        {{ DAYOFWEEK[\Carbon\Carbon::parse($item->date)->dayOfWeek] }}
                                                                        <br>
                                                                        ({{ date('d/m/Y', strtotime($item->date)) }})
                                                                    </td>
                                                                    <td>{{ $item->content_morning }}</td>
                                                                    <td>{{ $item->content_afternoon }} </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @else
                                <div class="education-label">
                                    Chưa đủ thông tin!
                                </div>
                            @endif

                        </div>
                        <div class="professonal-skill dashboard-section details-section">
                            <h4><i data-feather="feather"></i>KẾT QUẢ ĐẠT ĐƯỢC TRONG THỜI GIAN THỰC TẬP</h4>
                            {!! !empty($report->result) ? $report->result : 'Chưa có thông tin!' !!}
                        </div>
                        <div class="special-qualification dashboard-section details-section">
                            <h4><i data-feather="gift"></i>NHỮNG ĐIỀU CHƯA THỰC HIỆN ĐƯỢC</h4>
                            {!! !empty($report->work_unfinished) ? $report->work_unfinished : 'Chưa có thông tin!' !!}
                        </div>
                        <div class="professonal-skill dashboard-section details-section">
                            <h4><i data-feather="feather"></i>CÁC VẤN ĐỀ CẦN NGHIÊN CỨU VÀ PHÁT TRIỂN</h4>
                            {!! !empty($report->problem_develop) ? $report->problem_develop : 'Chưa có thông tin!' !!}
                        </div>
                        <div class="edication-background details-section dashboard-section">
                            <h4><i data-feather="book"></i>NHẬN XÉT CHUNG, NHỮNG THUẬN LỢI VÀ KHÓ KHĂN TRONG QUÁ TRÌNH THỰC
                                TẬP</h4>
                            <div class="education-label">
                                <span class="study-year">
                                    <h5>Nhận xét chung</h5>
                                </span>
                                {!! !empty($report->general_comment) && !empty(json_decode($report->general_comment)->general) ? json_decode($report->general_comment)->general : 'Chưa có thông tin!' !!}
                            </div>
                            <div class="education-label">
                                <span class="study-year">
                                    <h5>Thuận lợi</h5>
                                </span>
                                {!! !empty($report->general_comment) && !empty(json_decode($report->general_comment)->advantage) ? json_decode($report->general_comment)->advantage : 'Chưa có thông tin!' !!}
                            </div>
                            <div class="education-label">
                                <span class="study-year">
                                    <h5>Khó khăn</h5>
                                </span>
                                {!! !empty($report->general_comment) && !empty(json_decode($report->general_comment)->difficult) ? json_decode($report->general_comment)->difficult : 'Chưa có thông tin!' !!}
                            </div>
                        </div>
                        <div class="portfolio dashboard-section details-section">
                            <h4><i data-feather="align-left"></i>ĐỀ XUẤT – KIẾN NGHỊ VỚI ĐƠN VỊ THỰC TẬP</h4>
                            {!! !empty($report->propose) ? $report->propose : 'Chưa có thông tin!' !!}
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <form id="info-report" enctype="multipart/form-data">
                        <div class="dashboard-apply-area job-post-form">
                            <div class="edication-background details-section dashboard-section my-5">
                                <h4><i data-feather="align-left"></i>Thông tin người hướng dẫn
                                </h4>
                                <div id="job-title" class="form-group row">
                                    <label class="col-md-3 col-form-label text-dark">Người hướng dẫn <span
                                            class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="tutor_name"
                                            placeholder="Họ và tên người hướng dẫn"
                                            value="{{ !empty($report->tutor_name) ? $report->tutor_name : null }}">
                                        <div id="tutor_name"></div>
                                    </div>
                                </div>
                                <div id="job-summery" class="row">
                                    <label class="col-md-3 col-form-label text-dark">Thông tin <span
                                            class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="tutor_email"
                                                        placeholder="Địa chỉ email"
                                                        value="{{ !empty($report->tutor_email) ? $report->tutor_email : null }}">
                                                    <div id="tutor_email"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="tutor_phone"
                                                        placeholder="Số điện thoại"
                                                        value="{{ !empty($report->tutor_phone) ? $report->tutor_phone : null }}">
                                                    <div id="tutor_phone"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="tutor_position"
                                                        placeholder="Vị trí trong doanh nghiệp"
                                                        value="{{ !empty($report->tutor_position) ? $report->tutor_position : null }}">
                                                    <div id="tutor_position"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="edication-background details-section dashboard-section my-5">
                                <h4><i data-feather="align-left"></i>Thông tin thực tập của sinh viên
                                </h4>
                                <div id="job-title" class="form-group row">
                                    <label class="col-md-3 col-form-label text-dark">Vị trí thực tập <span
                                            class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="intern_position"
                                            placeholder="Vị trí thực tập"
                                            value="{{ !empty($report->intern_position) ? $report->intern_position : null }}">
                                        <div id="intern_position"></div>
                                    </div>
                                </div>
                                <div id="job-summery" class="row">
                                    <label class="col-md-3 col-form-label text-dark">Ngày bắt đầu thực tập <span
                                            class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <div class="form-group datepicker">
                                            <input type="date" class="form-control" name="start_date"
                                                value="{{ !empty($report->start) ? date('Y-m-d', strtotime($report->start)) : null }}">
                                            <div id="start_date"></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="job-summery" class="row">
                                    <label class="col-md-3 col-form-label text-dark">Ngày kết thúc thực tập</label>
                                    <div class="col-md-9">
                                        <div class="form-group datepicker">
                                            <input type="date" class="form-control" name="end_date"
                                                value="{{ !empty($report->end) ? date('Y-m-d', strtotime($report->end)) : null }}">
                                            <div id="end_date"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="edication-background details-section dashboard-section my-5">

                                <h4><i data-feather="align-left"></i>Giới thiệu về đơn vị thực tập</h4>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Tóm lược quá trình hình thành và phát triển</h5>
                                    </span>
                                    <textarea class="tinymce-editor-2" name="about_content"
                                        placeholder="Description text here">{{ !empty($report->about) ? $report->about : null }}</textarea>
                                    <div id="about_content"></div>

                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Chức năng và lĩnh vực hoạt động</h5>
                                    </span>
                                    <textarea class="tinymce-editor-2" name="function_area_activities"
                                        placeholder="Description text here">{{ !empty($report->function_area_activities) ? $report->function_area_activities : null }}</textarea>
                                    <div id="function_area_activities"></div>
                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Sản phẩm và dịch vụ</h5>
                                    </span>
                                    <textarea class="tinymce-editor-2" name="product_services"
                                        placeholder="Description text here">{{ !empty($report->product_services) ? $report->product_services : null }}</textarea>
                                    <div id="product_services"></div>
                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Tổ chức quản lý hành chính, nhân sự</h5>
                                    </span>
                                    <textarea class="tinymce-editor-2" name="organization"
                                        placeholder="Description text here">{{ !empty($report->organization) ? $report->organization : null }}</textarea>
                                    <div id="organization"></div>
                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Chiến lược và phương hướng phát triển của đơn vị trong tương lai</h5>
                                    </span>
                                    <textarea class="tinymce-editor-2" name="strategy_future"
                                        placeholder="Description text here">{{ !empty($report->strategy_future) ? $report->strategy_future : null }}</textarea>
                                    <div id="strategy_future"></div>
                                </div>
                            </div>
                            <div class="edication-background details-section dashboard-section">
                                <h4><i data-feather="align-left"></i>Báo cáo các nội dung công việc thực tập</h4>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Giới thiệu tóm tắt các hoạt động/công việc tại đơn vị thực tập</h5>
                                    </span>
                                    <textarea class="tinymce-editor-2" name="summary_activities"
                                        placeholder="Description text here">{{ !empty($report->summary_activities) ? $report->summary_activities : null }}</textarea>
                                    <div id="summary_activities"></div>
                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Mô tả các công việc được phân công thực hiện hoặc được tham gia tại đơn vị </h5>
                                    </span>
                                    <textarea class="tinymce-editor-2" name="work"
                                        placeholder="Description text here">{{ !empty($report->work) ? $report->work : null }}</textarea>
                                    <div id="work"></div>
                                </div>
                            </div>
                            <div class="professonal-skill dashboard-section details-section">
                                <h4><i data-feather="feather"></i>Kết quả đạt được trong thời gian thực tập</h4>
                                <textarea class="tinymce-editor-2" name="result"
                                    placeholder="Description text here">{{ !empty($report->result) ? $report->result : null }}</textarea>
                                <div id="result"></div>
                            </div>
                            <div class="special-qualification dashboard-section details-section">
                                <h4><i data-feather="gift"></i>Những điều chưa thực hiện được</h4>
                                <textarea class="tinymce-editor-2" name="work_unfinished"
                                    placeholder="Description text here">{{ !empty($report->work_unfinished) ? $report->work_unfinished : null }}</textarea>
                                <div id="work_unfinished"></div>
                            </div>
                            <div class="professonal-skill dashboard-section details-section">
                                <h4><i data-feather="feather"></i>Các vấn đề cần tiếp tục nghiên cứu, phát triển</h4>
                                <textarea class="tinymce-editor-2" name="problem_develop"
                                    placeholder="Description text here">{{ !empty($report->problem_develop) ? $report->problem_develop : null }}</textarea>
                                <div id="problem_develop"></div>
                            </div>
                            <div class="edication-background details-section dashboard-section">
                                <h4><i data-feather="book"></i>Nhận xét chung, những thuận lợi, khó khăn trong suốt quá
                                    trình
                                    thực
                                    tập</h4>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Thuận lợi</h5>
                                    </span>
                                    <textarea name="advantage_comment" class="tinymce-editor-2"
                                        placeholder="Description text here">{{ !empty($report->general_comment) ? json_decode($report->general_comment)->advantage : null }}</textarea>
                                    <div id="advantage_comment"></div>
                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Khó khăn</h5>
                                    </span>
                                    <textarea name="difficult_comment" class="tinymce-editor-2"
                                        placeholder="Description text here">{{ !empty($report->general_comment) ? json_decode($report->general_comment)->difficult : null }}</textarea>
                                    <div id="difficult_comment"></div>
                                </div>
                                <div class="education-label">
                                    <span class="study-year">
                                        <h5>Nhận xét chung</h5>
                                    </span>
                                    <textarea name="general_comment" class="tinymce-editor-2"
                                        placeholder="Description text here">{{ !empty($report->general_comment) ? json_decode($report->general_comment)->general : null }}</textarea>
                                    <div id="general_comment"></div>
                                </div>
                            </div>
                            <div class="portfolio dashboard-section details-section">
                                <h4><i data-feather="align-left"></i>Đề xuất - Kiến nghị với đơn vị thực tập</h4>
                                <textarea name="propose" class="tinymce-editor-2"
                                    placeholder="Description text here">{{ !empty($report->propose) ? $report->propose : null }}</textarea>
                                <div id="propose"></div>
                            </div>
                            <div class="portfolio dashboard-section details-section">
                                <button class="btn btn-primary p-4" style="font-size: 10pt">CẬP NHẬT BÁO CÁO</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="nav-report" role="tabpanel" aria-labelledby="nav-report-tab">
                    <div class="row about-details details-section dashboard-section d-block">
                        <!-- Button trigger modal -->
                        <button type="button" onclick="clearInput()" class="btn btn-primary add-report-detail"
                            data-toggle="modal" data-target="#modal-report" title="Viết báo cáo">
                            <i data-feather="edit-2"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade modal-pro-skill" id="modal-report" tabindex="-1" role="dialog"
                            aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="title">
                                            <h4><i data-feather="feather"></i>Viết báo cáo ngày</h4>
                                        </div>
                                        <div class="content">
                                            <form id="report-detail">
                                                <div class="input-block-wrap">
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 col-form-label">Nội dung làm việc buổi
                                                            sáng</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group">
                                                                <textarea name="content_morning" class="form-control"
                                                                    name="" id="content_morning"></textarea>
                                                            </div>
                                                            <div id="content_morning_error"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="input-block-wrap">
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 col-form-label">Nội dung làm việc buổi
                                                            chiều</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group">
                                                                <textarea name="content_afternoon" class="form-control"
                                                                    id="content_afternoon"></textarea>
                                                            </div>
                                                            <div id="content_afternoon_error"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="input-block-wrap">
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 col-form-label">Ngày làm việc</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group">
                                                                <input type="date" id="date" name="date"
                                                                    class="form-control">
                                                            </div>
                                                            <div id="date_error"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="input-block-wrap d-none">
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 col-form-label">Ngày làm việc</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group">
                                                                <input type="hidden" id="id" name="id"
                                                                    class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="offset-sm-3 col-sm-9">
                                                        <div class="buttons">
                                                            <button class="primary-bg" id='btn-report'>Thêm báo
                                                                cáo</button>
                                                            <button class=""
                                                                data-dismiss="modal">Huỷ</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-apply-area manage-job-container my-5">
                        @if (!empty($allWeek))
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                @foreach ($allWeek as $key => $week)
                                    <li class="nav-item">
                                        <a class="nav-link @if ($key == 1) active @endif" id="privacy-tab{{ $key . 'exm' }}"
                                            href="#privacy{{ $key . 'exm' }}" data-toggle="tab" role="tab"
                                            aria-controls="privacy{{ $key . 'exm' }}" aria-selected="true"> Tuần
                                            {{ $key }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="tab-content" id="myTabContent" style="font-size: 14px">
                                @foreach ($allWeek as $key => $week)
                                    <div class="tab-pane fade @if ($key == 1) active show @endif" id="privacy{{ $key . 'exm' }}"
                                        role="tabpanel" aria-labelledby="privacy-tab{{ $key . 'exm' }}"
                                        class="mb-0">
                                        <div class="tab-body mt-3">
                                            <table class="table table-bordered text-dark">
                                                <thead class="border-bottom-none">
                                                    <th style="width: 25%; text-align: center;">
                                                        {{ date('d/m/Y', strtotime($week['start_week'])) }} -
                                                        {{ date('d/m/Y', strtotime($week['end_week'])) }}
                                                    </th>
                                                    <th style="width: 30%; text-align: center;">
                                                        <span>Sáng</span>
                                                    </th>
                                                    <th style="width: 30%; text-align: center;">
                                                        <span>Chiều</span>
                                                    </th>
                                                    <th style="width: 10%; text-align: center;">
                                                        <span>Hành động</span>
                                                    </th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($reportDetail as $item)
                                                        @if (strtotime($item->date) <= strtotime($week['end_week']) && strtotime($item->date) >= strtotime($week['start_week']))
                                                            <tr>
                                                                <td class="text-center">
                                                                    {{ DAYOFWEEK[\Carbon\Carbon::parse($item->date)->dayOfWeek] }}
                                                                    <br>
                                                                    ({{ date('d/m/Y', strtotime($item->date)) }})
                                                                </td>
                                                                <td class="p-3">{{ $item->content_morning }}
                                                                </td>
                                                                <td class="p-3">
                                                                    {{ $item->content_afternoon }}
                                                                </td>
                                                                <td class="action text-center">
                                                                    <a class="edit pointer"
                                                                        onclick="editReportDetail('{{ $item->id }}')"
                                                                        title="Edit"><i data-feather="edit"></i></a>
                                                                    <a class="remove pointer"
                                                                        onclick="deleteRportDetail('{{ $item->id }}')"
                                                                        title="Delete"><i data-feather="trash-2"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            Bạn chưa bổ sung thông tin thực tập. Vui lòng nhập ngay để tiến hành viết báo cáo hoạt động hàng
                            ngày.
                        @endif

                    </div>
                </div>
            </div>
        </div>
    @endif
    @if (!empty($report) && !empty($report->company) && $report->company->status == COMPANY_STATUS_APPROVED && empty($report->tutor_name) && empty($report->email) && empty($report->tutor_position) && empty($report->tutor_phone) && empty($report->start) && empty($report->end))
        <div class="row about-details details-section dashboard-section d-block">
            <div class="modal fade modal-pro-skill" id="modal-first" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="title">
                                <h4><i data-feather="feather"></i>Thông tin thực tập</h4>
                            </div>
                            <div class="content">
                                <form id="info-report-modal" enctype="multipart/form-data" method="POST">
                                    <div class="input-block-wrap">
                                        <div id="job-title" class="form-group row">
                                            <label class="col-md-3 col-form-label text-dark">Người hướng dẫn <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="tutor_name"
                                                    placeholder="Họ và tên người hướng dẫn"
                                                    value="{{ !empty($report->tutor_name) ? $report->tutor_name : null }}">
                                                <div id="tutor_name_modal"></div>
                                            </div>
                                        </div>
                                        <div id="job-summery" class="row">
                                            <label class="col-md-3 col-form-label text-dark">Thông tin <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="tutor_email"
                                                                placeholder="Địa chỉ email"
                                                                value="{{ !empty($report->tutor_email) ? $report->tutor_email : null }}">
                                                            <div id="tutor_email_modal"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="tutor_phone"
                                                                placeholder="Số điện thoại"
                                                                value="{{ !empty($report->tutor_phone) ? $report->tutor_phone : null }}">
                                                            <div id="tutor_phone_modal"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="tutor_position"
                                                                placeholder="Vị trí trong doanh nghiệp"
                                                                value="{{ !empty($report->tutor_position) ? $report->tutor_position : null }}">
                                                            <div id="tutor_position_modal"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="job-title" class="form-group row">
                                            <label class="col-md-3 col-form-label text-dark">Vị trí thực tập <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="intern_position"
                                                    placeholder="Vị trí thực tập"
                                                    value="{{ !empty($report->intern_position) ? $report->intern_position : null }}">
                                                <div id="intern_position_modal"></div>
                                            </div>
                                        </div>
                                        <div id="job-summery" class="row">
                                            <label class="col-md-3 col-form-label text-dark">Ngày bắt đầu thực tập <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-md-9">
                                                <div class="form-group datepicker">
                                                    <input type="date" class="form-control" name="start_date"
                                                        value="{{ !empty($report->start) ? date('Y-m-d', strtotime($report->start)) : '' }}">
                                                    <div id="start_date_modal"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="job-summery" class="row">
                                            <label class="col-md-3 col-form-label text-dark">Thời gian thực tập</label>
                                            <div class="col-md-9">
                                                <div class="form-group datepicker">

                                                    <select class="form-control" name="end_date">
                                                        <option value="0">2,5 tháng ( 10 tuần )</option>
                                                        <option value="1">3 tháng</option>
                                                        <option value="2">4 tháng</option>
                                                    </select>
                                                </div>
                                                <div id="end_date_modal"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="offset-sm-3 col-sm-9">
                                            <div class="buttons">
                                                <button class="primary-bg">Cập nhật thông tin</button>
                                                <button class="" data-dismiss="modal">Huỷ</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if ((!empty($report) && !$report->is_accept) || !\App\Http\Controllers\Controller::checkTimeLine('report'))
        <div class="row about-details details-section dashboard-section d-block">
            <div class="modal fade modal-pro-skill" id="modal-reset-request" tabindex="-1" role="dialog"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="title">
                                <h4><i data-feather="feather"></i>Gửi yêu cầu xử lý thực tập trễ tiến độ</h4>
                            </div>
                            <div class="content">
                                <div class="text-left text-danger col-sm-12 mb-5"
                                    style="font-size: 15px; margin-top: 8px; border: 1px dashed #ccc; padding: 5px 10px;">
                                    Thông tin sinh viên: <br>
                                    1. Bạn đã trễ thời gian hoàn thành báo cáo thực tập. Sinh viên vui lòng gửi yêu cầu về
                                    nhà
                                    trường để được xử lý.<br>
                                    2. Sinh viên vui lòng suy nghĩ thật kỹ khi đưa ra quyết định. Sinh viên nên cân
                                    nhẳc điều này. <br>
                                </div>
                                <form id="reset-report-modal" enctype="multipart/form-data" method="POST">
                                    <div class="row">
                                        <div class="form-group col-sm-7">
                                            <select class="form-control" name="request">
                                                <option value="">Vui lòng chọn phương án yêu cầu hổ trợ</option>
                                                @foreach (PLAN_RESET_REPORT as $key => $item)
                                                    <option value="{{ $key }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="buttons float-right">
                                                <button class="primary-bg">Gửi yêu cầu hổ trợ</button>
                                                <button class="" data-dismiss="modal">Huỷ</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- Modal -->
    <div class="row about-details details-section dashboard-section d-block">
        <div class="modal fade modal-pro-skill" id="modal-upload" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="title">
                            <h4><i data-feather="feather"></i>Nộp nhận xét thực tập</h4>
                        </div>
                        <div class="content">
                            <div class="text-left col-sm-12 mb-5"
                                style="font-size: 15px; margin-top: 8px; border: 1px dashed #ccc; padding: 5px 10px;">
                                Thực hiện theo 02 cách sau: <br>
                                Cách 1: In file biểu mẫu gửi lãnh đạo nhận xét, ký và ghi rõ họ tên, đóng dấu DN (mộc tròn).
                                Dùng phần mềm scan lấy file (định dạng .pdf). Các phần mềm cho thể dùng như CamScanner,
                                Microsoft Lens,… tải ứng dụng từ CH Play/ Appstore. <br>
                                Cách 2: Sinh viên copy và paste nội dung biểu mẫu NXTT gửi email trực tiếp cho DN nhận xét
                                sau đó xuất file theo định dạng pdf để tải lên tại đây. <br>
                                Lưu ý: Nếu chưa có biểu mẫu nhận xét sinh
                                viên vui lòng tải xuống <a href="{{ FILE_URL . '5KMTJwRyi' }}"
                                    class="text-info">tại
                                    đây</a>.
                            </div>
                            <form action="{{ route('student.report.upload-rate') }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                @method('POST')
                                <div class="form-group ">
                                    <input type="file" name="file" class="filestyle"
                                        data-text="Chọn File nhận xét (PDF)" data-btnClass="btn-lg btn-primary"
                                        value="{{ old('file') }}">
                                    @error('file')
                                        <div class="text text-danger mt-3">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="buttons mt-4">
                                    <button class="bg-primary text-white">Nộp nhận xét thực tập</button>
                                    <button class="delete-button" data-dismiss="modal">Huỷ</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    <style>
        .input-group {
            align-items: end;
        }

    </style>
@endpush
@push('script')
    @if ($errors->any())
        <script>
            $('#modal-upload').modal('show')
        </script>
    @endif
    <script src="assets/libs/bootstrap-filestyle2/bootstrap-filestyle.min.js"></script>
    <script>
        var onloadWindow = window.onload;
        window.onload = function() {
            if (onloadWindow) {
                onloadWindow();
            }
            let flag_page = localStorage.getItem('flag-page');
            if (flag_page === 'report-detail') {
                activaTab('nav-report');
            }
            localStorage.clear();
            $('#modal-first').modal('show');
        };


        function clearInput() {
            $('#content_morning').val('');
            $('#content_afternoon').val('');
            $('#date').val('');
            $('#id').val('');
            $('#date').removeClass('readonly-div');
            $("#btn-report").html('Thêm báo cáo');
            document.getElementById('content_afternoon_error').innerHTML = '';
            document.getElementById('content_morning_error').innerHTML = '';
            document.getElementById('date_error').innerHTML = '';
        }
        $("#info-report").submit(function(event) {
            event.preventDefault();
            tinyMCE.triggerSave();
            let data = $('form#info-report').serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});
            getLoading();
            $.ajax({
                url: '{{ route('student.report.store') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    data: data
                },
                success: function(response) {
                    removeLoading();
                    if (response['error']) {
                        if (response['error']['tutor_name']) {
                            error = document.getElementById('tutor_name');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['tutor_name']}</span>`
                        }
                        if (response['error']['tutor_phone']) {
                            error = document.getElementById('tutor_phone');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['tutor_phone']}</span>`
                        }
                        if (response['error']['tutor_email']) {
                            error = document.getElementById('tutor_email');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['tutor_email']}</span>`
                        }
                        if (response['error']['tutor_position']) {
                            error = document.getElementById('tutor_position');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['tutor_position']}</span>`
                        }
                        if (response['error']['intern_position']) {
                            error = document.getElementById('intern_position');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['intern_position']}</span>`
                        }
                        if (response['error']['start_date']) {
                            error = document.getElementById('start_date');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['start_date']}</span>`
                        }
                        if (response['error']['end_date']) {
                            error = document.getElementById('end_date_modal');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['end_date']}</span>`
                        }
                        if (response['error']['about_content']) {
                            error = document.getElementById('about_content');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['about_content']}</span>`
                        }
                        if (response['error']['function_area_activities']) {
                            error = document.getElementById('function_area_activities');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['function_area_activities']}</span>`
                        }
                        if (response['error']['product_services']) {
                            error = document.getElementById('product_services');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['product_services']}</span>`
                        }
                        if (response['error']['organization']) {
                            error = document.getElementById('organization');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['organization']}</span>`
                        }
                        if (response['error']['strategy_future']) {
                            error = document.getElementById('strategy_future');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['strategy_future']}</span>`
                        }
                        if (response['error']['summary_activities']) {
                            error = document.getElementById('summary_activities');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['summary_activities']}</span>`
                        }
                        if (response['error']['work']) {
                            error = document.getElementById('work');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['work']}</span>`
                        }
                        if (response['error']['result']) {
                            error = document.getElementById('result');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['result']}</span>`
                        }
                        if (response['error']['work_unfinished']) {
                            error = document.getElementById('work_unfinished');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['work_unfinished']}</span>`
                        }
                        if (response['error']['problem_develop']) {
                            error = document.getElementById('problem_develop');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['problem_develop']}</span>`
                        }
                        if (response['error']['propose']) {
                            error = document.getElementById('propose');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['propose']}</span>`
                        }
                        if (response['error']['difficult_comment']) {
                            error = document.getElementById('difficult_comment');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['difficult_comment']}</span>`
                        }
                        if (response['error']['general_comment']) {
                            error = document.getElementById('general_comment');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['general_comment']}</span>`
                        }
                        if (response['error']['advantage_comment']) {
                            error = document.getElementById('advantage_comment');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['advantage_comment']}</span>`
                        }
                    } else {
                        swal({
                                title: "Cập nhập thành công!",
                                text: "Bạn vừa Icập nhật thông tin doanh nghiệp thành công !",
                                buttonsStyling: false,
                                timer: 3000,
                                confirmButtonClass: "btn btn-lg btn-success",
                                type: "success",
                                confirmButtonText: 'Đồng ý !',
                            }).then(() => window.location.reload())
                            .catch(swal.noop)
                    }
                },
                error: function(response) {
                    swal({
                        title: "Hệ thống gặp sự cố!",
                        text: "Vui lòng thử lại sau !",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-lg btn-warning",
                        type: "warning",
                        confirmButtonText: 'Đồng ý !',
                    }).catch(swal.noop)
                }
            });
        });
        $("#info-report-modal").submit(function(event) {
            event.preventDefault();
            tinyMCE.triggerSave();
            let data = $('form#info-report-modal').serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});
            $.ajax({
                url: '{{ route('student.report.store') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    data: data
                },
                success: function(response) {
                    if (response['error']) {
                        if (response['error']['tutor_name']) {
                            error = document.getElementById('tutor_name_modal');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['tutor_name']}</span>`
                        }
                        if (response['error']['tutor_phone']) {
                            error = document.getElementById('tutor_phone_modal');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['tutor_phone']}</span>`
                        }
                        if (response['error']['tutor_email']) {
                            error = document.getElementById('tutor_email_modal');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['tutor_email']}</span>`
                        }
                        if (response['error']['tutor_position']) {
                            error = document.getElementById('tutor_position_modal');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['tutor_position']}</span>`
                        }
                        if (response['error']['intern_position']) {
                            error = document.getElementById('intern_position_modal');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['intern_position']}</span>`
                        }
                        if (response['error']['start_date']) {
                            error = document.getElementById('start_date_modal');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['start_date']}</span>`
                        }
                        if (response['error']['end_date']) {
                            error = document.getElementById('end_date_modal');
                            error.innerHTML =
                                `<span class="text-danger small font-italic">${response['error']['end_date']}</span>`
                        }
                    } else {
                        swal({
                                title: "Cập nhập thành công!",
                                text: "Bạn vừa Icập nhật thông tin doanh nghiệp thành công !",
                                buttonsStyling: false,
                                timer: 3000,
                                confirmButtonClass: "btn btn-lg btn-success",
                                type: "success",
                                confirmButtonText: 'Đồng ý !',
                            }).then(() => window.location.reload())
                            .catch(swal.noop)
                    }
                },
                error: function(response) {
                    swal({
                        title: "Hệ thống gặp sự cố!",
                        text: "Vui lòng thử lại sau !",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-lg btn-warning",
                        type: "warning",
                        confirmButtonText: 'Đồng ý !',
                    }).catch(swal.noop)
                }
            });
        });

        function editReportDetail(id) {
            $('#modal-report').modal('show');
            $("#btn-report").html('Chỉnh sửa');
            document.getElementById('content_afternoon_error').innerHTML = '';
            document.getElementById('content_morning_error').innerHTML = '';
            document.getElementById('date_error').innerHTML = '';
            $.ajax({
                url: '{{ route('student.report-detail.edit') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id
                },
                success: function(response) {
                    let currentDate = new Date(response['date']);
                    let date = moment(currentDate, 'MM-DD-YYYY HH:mm:ss', true).format("YYYY-MM-DD");
                    $('#content_morning').val(response['content_morning']);
                    $('#content_afternoon').val(response['content_afternoon']);
                    $('#date').val(date);
                    $('#id').val(response['id']);
                    $('#date').addClass('readonly-div');
                },
                error: function(response) {
                    swal({
                        title: "Hệ thống gặp sự cố!",
                        text: "Vui lòng thử lại sau !",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-lg btn-warning",
                        type: "warning",
                        confirmButtonText: 'Đồng ý !',
                    }).catch(swal.noop)
                }
            });
        }

        $("#report-detail").submit(function(event) {
            event.preventDefault();
            let data = $('form#report-detail').serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});
            $.ajax({
                url: '{{ route('student.report-detail.store') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    data: data,
                },
                success: function(response) {
                    console.log(response);
                    if (response['checkReport']) {
                        swal({
                                title: "Bạn không thể viết báo cáo ngày!",
                                text: "Sinh viên đã hoàn thành báo cáo nên không thể tiếp tục!",
                                buttonsStyling: false,
                                timer: 10000,
                                confirmButtonClass: "btn btn-lg btn-success",
                                type: "warning",
                                confirmButtonText: 'Đồng ý !',
                            })
                            .then(() => window.location.reload())
                            .catch(swal.noop);
                    } else {
                        if (response['error']) {
                            console.log(response['error']);
                            if (response['error']['content_afternoon']) {
                                error = document.getElementById('content_afternoon_error');
                                error.innerHTML =
                                    `<span class="text-danger small font-italic">${response['error']['content_afternoon']}</span>`
                            }
                            if (response['error']['content_morning']) {
                                error = document.getElementById('content_morning_error');
                                error.innerHTML =
                                    `<span class="text-danger small font-italic">${response['error']['content_morning']}</span>`
                            }
                            if (response['error']['date']) {
                                error = document.getElementById('date_error');
                                error.innerHTML =
                                    `<span class="text-danger small font-italic">${response['error']['date']}</span>`
                            }
                        } else {

                            localStorage.setItem('flag-page', 'report-detail');
                            swal({
                                    title: "Thành công!",
                                    text: "Bạn vừa viết bào cáo ngày thành công !",
                                    buttonsStyling: false,
                                    timer: 10000,
                                    confirmButtonClass: "btn btn-lg btn-success",
                                    type: "success",
                                    confirmButtonText: 'Đồng ý !',
                                })
                                .then(() => window.location.reload())
                                .catch(swal.noop);

                        }
                    }

                },
                error: function(response) {
                    console.log(response);
                    swal({
                        title: "Hệ thống gặp sự cố!",
                        text: "Vui lòng thử lại sau !",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-lg btn-warning",
                        type: "warning",
                        confirmButtonText: 'Đồng ý !',
                    }).catch(swal.noop)
                }
            });
        })

        function deleteRportDetail(id) {
            Swal.fire({
                title: 'Bạn có chắc chắn muốn xoá báo cáo hàng ngày không?',
                text: "Mọi hành động sau khi đồng ý sẽ không thể khôi phục lại!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Đồng ý!',
                cancelButtonText: 'Huỷ!',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '{{ route('student.report-detail.destroy') }}',
                        type: 'DELETE',
                        data: {
                            _token: '{{ csrf_token() }}',
                            id: id,
                        },
                        success: function(response) {
                            localStorage.setItem('flag-page', 'report-detail');
                            swal({
                                    title: "Thành công!",
                                    text: "Bạn vừa xoá báo cáo hàng ngày thành công!",
                                    buttonsStyling: false,
                                    timer: 10000,
                                    confirmButtonClass: "btn btn-lg btn-success",
                                    type: "success",
                                    confirmButtonText: 'Đồng ý !',
                                })
                                .then(() => window.location.reload())
                                .catch(swal.noop);
                        },
                        error: function(response) {
                            swal({
                                title: "Hệ thống gặp sự cố!",
                                text: "Vui lòng thử lại sau !",
                                buttonsStyling: false,
                                confirmButtonClass: "btn btn-lg btn-warning",
                                type: "warning",
                                confirmButtonText: 'Đồng ý !',
                            }).catch(swal.noop)
                        }
                    });
                }
            });

        }

        function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };

        function checkFinish(event) {
            event.preventDefault();
            $('#modal-reset-request').modal('show');
        }

        $("#reset-report-modal").submit(function(event) {
            event.preventDefault();
            $('#modal-reset-request').modal('hide');
            getLoading();
            let data = $('form#reset-report-modal').serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});
            $.ajax({
                type: "POST",
                url: `{{ route('student.report.reset') }}`,
                data: {
                    _token: '{{ csrf_token() }}',
                    data: data
                },
                success: function(response) {
                    removeLoading();
                    swal({
                            title: "Thành công!",
                            text: "Bạn vừa gửi yêu cầu hỗ trợ thành công!",
                            buttonsStyling: false,
                            timer: 10000,
                            confirmButtonClass: "btn btn-lg btn-success",
                            type: "success",
                            confirmButtonText: 'Đồng ý !',
                        })
                        .then(() => window.location.reload())
                        .catch(swal.noop);
                },
                error: function(response) {
                    swal({
                        title: "Hệ thống gặp sự cố!",
                        text: "Vui lòng thử lại sau !",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-lg btn-warning",
                        type: "warning",
                        confirmButtonText: 'Đồng ý !',
                    }).catch(swal.noop)
                }
            });
        });
    </script>

@endpush
