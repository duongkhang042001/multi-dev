@extends('layouts.client_guest')
@section('page-title', 'CV | FPT Intern')
@section('title', 'Cập nhật thông tin tài khoản')
@section('content')
    <div class="alice-bg padding-top-70 padding-bottom-70 ">
        <div class="container">
            <div class="dashboard-content-wrapper">
                <iframe src="{{ $filePathPublic }}" frameBorder="0" scrolling="auto" height="1200px"
                    width="100%"></iframe>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@endsection
