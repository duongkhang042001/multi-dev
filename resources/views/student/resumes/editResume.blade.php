@extends('layouts.client')
@section('page-title', 'Chỉnh sửa hồ sơ cá nhân | FPT Intern')
@section('title', 'Chỉnh sửa hồ sơ cá nhân')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Sinh viên</a></li>
            <li class="breadcrumb-item active" aria-current="page">Chỉnh sửa hồ sơ cá nhân</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="dashboard-content-wrapper">
        <div class="about-details details-section dashboard-section">
            <h4><i data-feather="align-left"></i>Giới thiệu bản thân</h4>
            {!! $resume->introduce !!}
            <div class="information-and-contact">
                <div class="information">
                    <h4>Thông tin cơ bản</h4>
                    <ul>
                        <li><span>Họ và
                                tên:</span>{{ !empty(Auth::user()->profile) ? Auth::user()->profile->full_name : null }}
                        </li>
                        <li><span>Email:</span> {{ !empty(Auth::user()) ? Auth::user()->email : null }}</li>
                        <li><span>Ngành học:</span> {{ Auth::user()->student->career->name }}</li>
                        <li><span>Nơi ở:</span> {{ Auth::user()->profile->getAddress() }}</li>
                        <li><span>Năm kinh nghiệm:</span>{{ $resume->experience_year }}</li>
                        <li><span>Trình độ chuyên môn:</span> {{ $resume->qualification }}</li>
                        <li><span>Giới tính:</span>
                            {{ !empty(Auth::user()->profile ? Auth::user()->profile->gender : null) == MAN ? 'Nam' : 'Nữ' }}
                        </li>
                        <li><span>Tuổi:</span> {{ Auth::user()->profile->getAge() }}</li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="about-details details-section dashboard-section">
            <h4><i data-feather="align-left"></i>Mục tiêu</h4>
            {!! $resume->target !!}
        </div>
        <div class="edication-background details-section dashboard-section">
            <h4><i data-feather="book"></i>Học vấn</h4>
            @php
                $edu = json_decode($resume->education);
            @endphp
            <div class="education-label">
                <span class="study-year">{{ $edu->time }}</span>
                <h5>{{ Auth::user()->student->career->name }}<span>@ {{ $edu->name }}</span></h5>
                <p>{{ $edu->des }}</p>
            </div>

        </div>
        <div class="experience dashboard-section details-section">
            <h4><i data-feather="briefcase"></i>Kinh nghiệm làm việc</h4>
            @foreach (json_decode($resume->experience) as $item)
                <div class="experience-section">
                    <span class="service-year">{{ $item->timeExperience }}</span>
                    <h5>{{ $item->positionExperience }}<span>@ {{ $item->nameExperience }}</span></h5>
                    <p>{{ $item->desExperience }}</p>
                </div>
            @endforeach

        </div>
        <div class="professonal-skill dashboard-section details-section">
            <h4><i data-feather="feather"></i>Kỹ năng chuyên môn</h4>
            <div class="progress-group">
                @foreach (json_decode($resume->skill) as $item)
                    <div class="progress-item">
                        <div class="progress-head">
                            <p class="progress-on">{{ $item->name }}</p>
                        </div>
                        <div class="progress-body">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ $item->value }}"
                                    aria-valuemin="0" aria-valuemax="100" style="width: 0;"></div>
                            </div>
                            <p class="progress-to">{{ $item->value }}%</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{-- <div class="special-qualification dashboard-section details-section">
            <h4><i data-feather="gift"></i>Chứng chỉ</h4>
            <ul>
                <li>5 years+ experience designing and building products.</li>
                <li>Skilled at any Kind Design Tools.</li>
                <li>Passion for people-centered design, solid intuition.</li>
                <li>Hard Worker & Quick Lerner.</li>
            </ul>
        </div> --}}
    </div>
@endsection
@section('scripts')

@endsection
