@extends('layouts.client')
@section('page-title', 'Danh sách hồ sơ sinh viên')
@section('title', 'Danh sách hồ sơ sinh viên')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Sinh viên</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tạo hồ sơ sinh viên</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="dashboard-content-wrapper">
        <div class="download-resume dashboard-section">
            <a href="{{ Route('student.resume.create') }}" class="d-none" onclick="">Tạo hồ sơ<i
                    data-feather="plus-circle"></i></a>
            <a href="" data-toggle="modal" data-target="#model-upload">Tải hồ sơ<i data-feather="upload"></i></a>
        </div>
        <div class="input-block-wrap mt-5">
            <div class="text-left col-sm-12" style="font-size: 15px;  border: 1px dashed #ccc; padding: 15px 10px;">
                <p class=""><span>Lưu ý:</span>
                    Nếu sinh viên đã nộp ứng tuyển vào doanh nghiệp thì không thể thay đổi trạng thái của CV được nữa !
                </p>
            </div>
        </div>
        <div class="manage-job-container mt-5">
            @if (count(json_decode($listCV)) != 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tên hồ sơ</th>
                            <th>Thời gian tạo</th>
                            <th>Trạng thái</th>
                            <th class="action">Hành động</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach (json_decode($listCV) as $index => $objItem)
                            @php
                                $item = json_decode($objItem);
                            @endphp
                            <tr class="job-items">
                                <td class="title">
                                    <h5><a
                                            href="{{ Route('student.resume.detail', $item->code) }}">{{ $item->name }}</a>
                                    </h5>
                                    <div class="info">
                                        <span class="office-location">
                                            <a href="#">
                                                <i data-feather="archive"></i>
                                                Người tạo: {{ $item->type == CV_SYSTEM ? 'Hệ thống' : 'Cá nhân' }}
                                            </a>
                                        </span>
                                    </div>
                                </td>
                                <td class="deadline">{{ date('d-m-Y h:i:s', strtotime($item->created_at)) }}</td>

                                <td class="status">
                                    <div class="switchToggle">
                                        <input type="checkbox" id="switch{{ $index }}"
                                            onclick="changeActive('{{ $item->code }}')" @if ($item->is_active) checked @endif>
                                        <label for="switch{{ $index }}"></label>
                                    </div>
                                </td>
                                <td class="action">
                                    <a href="{{ \App\Http\Controllers\Controller::getFilePdf($item->code) }}"
                                        target="_blank" class="preview" title="Preview"><i
                                            data-feather="eye"></i></a>
                                    @if ($item->type == CV_SYSTEM)
                                        <a href="#" class="edit" title="Edit"><i data-feather="edit"></i></a>
                                    @endif
                                    <a href="javascript: document.getElementById('deletedRow{{ $item->code }}').submit();"
                                        onclick="return confirm('Bạn Có muốn xóa???');" class="remove"
                                        title="Delete"><i data-feather="trash-2"></i></a>
                                </td>
                                <form action="{{ route('student.resume.delete-upload', $item->code) }}" method="POST"
                                    id="deletedRow{{ $item->code }}" class="d-none">
                                    @method('DELETE')
                                    @csrf
                                </form>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                Bạn chưa có hồ sơ nào. Upload ngay!
            @endif
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade modal-delete" id="model-upload" tabindex="-1" role="dialog" aria-hidden="true">
        <div class=" modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4><i data-feather="upload"></i>Tải hồ sơ</h4>
                    <p>Bạn có chắc chắn muốn tải lên hồ sơ của bạn!</p>
                    <form action="{{ Route('student.resume.store-upload') }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" value="{{ old('name') }}"
                                placeholder="Tên hồ sơ">
                            @error('name')
                                <div class="text text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group ">
                            <input type="file" name="file" class="filestyle" data-text="Chọn CV (PDF)"
                                data-btnClass="btn-lg btn-primary" value="{{ old('file') }}">
                            @error('file')
                                <div class="text text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="buttons mt-4">
                            <button class="">Tải lên</button>
                            <button class="delete-button" data-dismiss="modal">Huỷ</button>
                        </div>
                        <div class="form-group form-check">
                            <input id="check" type="checkbox" class="form-check-input" name="check" checked>
                            <label for="check" class="form-check-label">Bạn có đồng ý với <a href="#">Chính sách và Quyền
                                    lợi</a> và <a href="#">Bảo mật của chúng tôi.</a></label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    <style>
        .input-group {
            align-items: end;
        }

    </style>
@endpush
@section('scripts')
    <script src="assets/libs/bootstrap-filestyle2/bootstrap-filestyle.min.js"></script>
    <script>
        function changeActive(code) {
            $.ajax({
                url: '{{ route('student.resume.change-active') }}',
                type: 'PUT',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    code: code,
                },
                success: function(response) {
                    console.log(response);
                    if (response) {
                        var msg = 'Thay đổi trạng thái hồ sơ thành công!';
                        callNotify('info', msg);
                    } else {
                        var msg = 'Thay đổi trạng thái hồ sơ thất bại!';
                        callNotify('error', msg);
                    }
                },
                error: function(response) {
                    callNotify('error', 'Lỗi hệ thống!');
                }
            });
            setTimeout(() => {
                window.location.reload();
            }, 2000);
        };
    </script>
    @if ($errors->any())
        <script>
            $('#model-upload').modal('show')
        </script>
    @endif
@endsection
