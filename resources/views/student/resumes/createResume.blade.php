@extends('layouts.client')
@section('page-title', 'Tạo hồ sơ sinh viên')
@section('title', 'Tạo hồ sơ sinh viên')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Sinh viên</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tạo hồ sơ sinh viên</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="dashboard-content-wrapper">
        <div class="post-content-wrapper p-0">
            <form action="{{ Route('student.resume.store') }}" class="job-post-form" method="POST">
                @csrf
                @method('POST')
                <div class="basic-info-input">
                    <h4><i data-feather="plus-circle"></i>Tạo hồ sơ sinh viên</h4>
                    <div id="full-name" class="form-group row">
                        <label class="col-md-3 col-form-label">Họ và tên </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control"
                                value="{{ !empty(Auth::user()->profile) ? Auth::user()->profile->full_name : null }}"
                                readonly placeholder="Họ và tên">
                        </div>
                    </div>
                    <div id="information" class="row">
                        <label class="col-md-3 col-form-label">Thông tin cơ bản</label>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control" disabled>
                                            <option>Chọn ngành học</option>
                                            @foreach ($careers as $career)
                                                @if ($career->id == Auth::user()->student->career_id)
                                                    <option value="{{ $career->id }}" selected>{{ $career->name }}
                                                    </option>
                                                @else
                                                    <option value="{{ $career->id }}">{{ $career->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <i class="fa fa-caret-down"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control" disabled>
                                            <option>Giới tính</option>
                                            <option @if (Auth::user()->profile->gender == MAN)
                                                selected
                                                @endif>Nam</option>
                                            <option @if (Auth::user()->profile->gender == WOMEN)
                                                selected
                                                @endif>Nữ</option>
                                        </select>
                                        <i class="fa fa-caret-down"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="date"
                                            value="{{ !empty(Auth::user()->profile) ? date('Y-m-d', strtotime(Auth::user()->profile->birthday)) : null }}"
                                            class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group datepicker">
                                        <input type="text" class="form-control"
                                            value="{{ !empty(Auth::user()) ? Auth::user()->email : null }}"
                                            placeholder="Email" readonly>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group datepicker">
                                        <input type="text" class="form-control" placeholder="Địa chỉ" readonly
                                            value="{{ Auth::user()->profile->getAddress() }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="information" class="row">
                        <div class="col-md-9 ml-auto">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control" name="qualification">
                                            <option>Trình độ chuyên môn</option>
                                            <option>Sinh viên</option>
                                            <option>Tốt nghiệp đại học</option>
                                            <option>Tốt nghiệp cao đẳng</option>
                                        </select>
                                        <i class="fa fa-caret-down"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control" name="experience_year">
                                            <option>Số năm kinh nghiệm</option>
                                            <option>Dưới 1 năm kinh nghiệm</option>
                                            <option>2 năm kinh nghiệm</option>
                                            <option>3 năm kinh nghiệm</option>
                                            <option>4 năm kinh nghiệm</option>
                                            <option>Trên 5 năm kinh nghiệm</option>
                                        </select>
                                        <i class="fa fa-caret-down"></i>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div id="about" class="row">
                        <label class="col-md-3 col-form-label">Giới thiệu bản thân <span
                                class="text text-danger">*</span></label>
                        <div class="col-md-9">
                            <textarea name="introduce" class="tinymce-editor-1" placeholder=""></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 col-form-label">Mục tiêu <span class="text text-danger">*</span></label>
                        <div class="col-md-9">
                            <textarea name="target" class="tinymce-editor-1" placeholder=""></textarea>
                        </div>
                    </div>
                    <div id="about" class="row">
                        <label class="col-md-3 col-form-label">Sở thích <span class="text text-danger">*</span></label>
                        <div class="col-md-9">
                            <textarea name="hobby" class="tinymce-editor-1" placeholder=""></textarea>
                        </div>
                    </div>
                    <div id="about" class="row">
                        <label class="col-md-3 col-form-label">Hoạt động <span class="text text-danger">*</span></label>
                        <div class="col-md-9">
                            <textarea name="activity" class="tinymce-editor-1" placeholder=""></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 col-form-label">Học vấn <span class="text text-danger">*</span></label>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" name="education[name]" class="form-control" placeholder="Tên trường">
                            </div>
                            <div class="form-group">
                                <input type="text" name="education[time]" class="form-control"
                                    placeholder="Thời gian học">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="education[des]"
                                    placeholder="Mô tả ( không bắt buộc )"></textarea>
                            </div>
                        </div>
                    </div>
                    <div id="experience">
                        <div class="row">
                            <label class="col-md-3 col-form-label">Kinh nghiệm <span
                                    class="text text-danger">*</span></label>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" name="exp[exp0][nameExperience]" class="form-control"
                                        placeholder="Tên doanh nghiệp">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="exp[exp0][positionExperience]" class="form-control"
                                        placeholder="Vị trí">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="exp[exp0][timeExperience]" class="form-control"
                                        placeholder="Khoảng thời gian">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="exp[exp0][desExperience]"
                                        placeholder="Mô tả (không bắt buộc)"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-md-9 ml-auto">
                            <button type="button" onclick="addRowExperience()" class="add-new-field">+ Thêm kinh nghiệm
                                làm việc</button>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 col-form-label">Mô tả kỹ năng</label>
                        <div class="col-md-9">
                            <div class="form-group">
                                <textarea type="text" class="form-control" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 col-form-label">Kỹ năng <span class="text text-danger">*</span></label>
                        <div class="col-md-9">
                            <div id="skill">
                                <div class="box-skill">
                                    <div class="form-group">
                                        <input type="text" name="skill[skill0][name]" class="form-control"
                                            placeholder="Tên kĩ năng">
                                    </div>
                                    <div class="form-group">
                                        <input type="range" onInput="$('#rangeval0').html($(this).val())"
                                            name="skill[skill0][value]" class="custom-range"
                                            placeholder="Giá trị ( Phần trăm )" min="0" max="100" step="5">
                                        <span id="rangeval0">50</span>%
                                    </div>
                                </div>
                            </div>
                            <button type="button" onclick="addRowSkill()" class="add-new-field">+ Thêm kỹ năng</button>
                        </div>
                    </div>
                    <div id="profile" class="row">
                        <label class="col-md-3 col-form-label">Mạng xã hội</label>
                        <div class="col-md-9">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text dropdown-label">
                                            <select class="form-control" disabled>
                                                <option selected>Facebook</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" id="inlineFormInputGroup"
                                        name="url[facebook]" placeholder="Liên kết đến trang cá nhân">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text dropdown-label">
                                            <select class="form-control" disabled>
                                                <option selected>Twitter</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" id="inlineFormInputGroup"
                                    name="url[twitter]"
                                        placeholder="Liên kết đến trang cá nhân">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text dropdown-label">
                                            <select class="form-control" disabled>
                                                <option selected>Linkedin</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" id="inlineFormInputGroup"
                                        name="url[linkedin]" placeholder="Liên kết đến trang cá nhân">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text dropdown-label">
                                            <select class="form-control" disabled>
                                                <option selected>Instagram</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" id="inlineFormInputGroup"
                                        name="url[instagram]" placeholder="Liên kết đến trang cá nhân">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text dropdown-label">
                                            <select class="form-control" disabled>
                                                <option selected>Github</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" id="inlineFormInputGroup"
                                        name="url[github]" placeholder="Liên kết đến trang cá nhân">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9 offset-md-3">
                            <div class="form-group mt-0 terms">
                                <input class="custom-radio" value="1" type="checkbox" id="radio-4" name="check">
                                <label for="radio-4">
                                    <span class="dot"></span> Bạn chấp nhận các <a href="#">Điều khoản và
                                        Chính sách</a>
                                    và <a href="#">Bảo mật</a> của chúng tôi.
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"></label>
                        <div class="col-md-9">
                            <button class="button">Tạo hồ sơ</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
@endsection
@section('scripts')
    <script>
        let temp = 0;
        let tempExp = 0;

        function addRowSkill() {
            let boxSkill = document.getElementById('skill');
            temp++;
            let input = document.createElement('div');
            input.innerHTML = `<div class="box-skill">
                                <div class="form-group d-flex">
                                    <input type="text" name="skill[skill` + temp + `][name]" class="form-control col-11" placeholder="Tên kĩ năng">
                                    <p class="remove col-1 pointer" onclick="deleteRowSkill()" title="Delete"><i class="fa fa-trash-alt text-danger" aria-hidden="true"></i></p>
                                </div>
                                <div class="form-group">
                                    <input onInput="$('#rangeval` + temp +
                `').html($(this).val())" type="range" name="skill[skill` + temp + `][value]" class="custom-range"
                                        placeholder="Giá trị ( Phần trăm )" min="0" max="100" step="5">
                                    <span id="rangeval` + temp + `">50<!-- Default value --></span>%
                                </div>
                            </div>`;
            boxSkill.appendChild(input);
        }



        function addRowExperience() {
            let boxSkill = document.getElementById('experience');
            tempExp++;
            let input = document.createElement('div');
            input.innerHTML = `<div class="row">
                                    <label class="col-md-3 col-form-label">Kinh nghiệm ` + tempExp + `</label>
                                    <div class="col-md-9">
                                        <div class="form-group d-flex">
                                            <input type="text" name="exp[exp` + tempExp + `][nameExperience]" class="form-control col-11"
                                                placeholder="Tên doanh nghiệp">
                                            <p class="remove col-1 pointer" onclick="deleteRowExperience()" title="Delete"><i class="fa fa-trash-alt text-danger" aria-hidden="true"></i></p>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="exp[exp` + tempExp + `][positionExperience]" class="form-control"
                                                placeholder="Vị trí">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="exp[exp` + tempExp + `][timeExperience]" class="form-control"
                                                placeholder="Khoảng thời gian">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" name="exp[exp` + tempExp + `][desExperience]"
                                                placeholder="Mô tả (không bắt buộc)"></textarea>
                                        </div>
                                    </div>
                                </div>`;
            boxSkill.appendChild(input);
        }

        function deleteRowExperience(e) {
            let row = document.querySelector(".remove");
            let deleteRow = row.parentElement.parentElement.parentElement;
            deleteRow.remove();
        }

        function deleteRowSkill(e) {
            let row = document.querySelector(".remove");
            let deleteRow = row.parentElement.parentElement;
            deleteRow.remove();
        }
    </script>
@endsection
