@extends('layouts.client')
@section('page-title', 'Hồ sơ cá nhân | FPT Intern')
@section('title', 'Hồ sơ cá nhân')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Sinh viên</a></li>
            <li class="breadcrumb-item active" aria-current="page">Hồ sơ cá nhân</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="dashboard-content-wrapper">
        <form action="{{ Route('student.initial.update') }}" method="POST" class="dashboard-form" autocomplete="off"
            enctype="multipart/form-data">
            @csrf
            @method('POST')
            <div class="dashboard-section upload-profile-photo">
                <div class="update-photo">
                    <img class="image" src="{{ FILE_URL . Auth::user()->avatar }}"
                        onerror="this.src='assets/clients/img/default-avatar.png'" alt="">
                </div>
                <div class="file-upload">
                    <input type="file" class="file-input" name="avatar">Thay đổi
                </div>
            </div>
            @error('avatar')
                <div class="jy-alert danger-alert my-5">
                    <div class="icon">
                        <i class="fas fa-check-circle"></i>
                    </div>
                    <p>{{ $message }}</p>
                    <div class="close_">
                        <a href="#"><i class="fas fa-times"></i></a>
                    </div>
                </div>
            @enderror
            <div class="dashboard-section basic-info-input">
                <h4><i data-feather="user-check"></i>Thông tin cơ bản</h4>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Họ và tên <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text"
                            value="{{ !empty(Auth::user()->profile) ? Auth::user()->profile->full_name : null }}"
                            class="form-control" readonly placeholder="Họ và tên" name="full_name">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Mã số sinh viên <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" value="{{ !empty(Auth::user()) ? Auth::user()->code : null }}" readonly
                            class="form-control" placeholder="PS11411" name="code">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Chuyên ngành <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" value="{{ Auth::user()->student->career->name }}" readonly
                            class="form-control" placeholder="PS11411" name="code">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Email <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input value="{{ !empty(Auth::user()) ? Auth::user()->email : null }}" readonly type="text"
                            class="form-control" placeholder="email@example.com" name="email">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Ngày sinh <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input
                            value="{{ !empty(Auth::user()->profile) ? date('Y-m-d', strtotime(Auth::user()->profile->birthday)) : null }}"
                            type="date" readonly name="birthday" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Giới tính <span class="text-danger">*</span></label>
                    <div class="col-3 col-md-1">
                        <input readonly class="custom-radio" type="radio" id="radio_1" name="gender" @if (!empty(Auth::user()->profile))
                        @if (Auth::user()->profile->gender == MAN)
                            checked
                        @endif
                        @endif
                        value="{{ MAN }}">
                        <label for="radio_1">
                            <span class="dot"></span> <span class="package-type">Nam</span>
                        </label>
                    </div>
                    <div class="col-3 col-sm-1">
                        <input readonly class="custom-radio" type="radio" id="radio_2" name="gender" @if (!empty(Auth::user()->profile))
                        @if (Auth::user()->profile->gender == WOMEN)
                            checked
                        @endif
                        @endif
                        value="{{ WOMEN }}">
                        <label for="radio_2">
                            <span class="dot"></span> <span class="package-type">Nữ</span>
                        </label>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">CMND/CCCD <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input readonly type="text" class="form-control"
                            value="{{ !empty(Auth::user()->profile) ? Auth::user()->profile->indo : null }}" readonly
                            placeholder="Chứng minh nhân dân / Căn cước công dân" name="indo">

                    </div>
                </div>
            </div>
            <div class="dashboard-section basic-info-input">
                <h4><i data-feather="lock"></i>THÔNG TIN CÁ NHÂN</h4>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Email cá nhân</label>
                    <div class="col-sm-9">
                        <input value="{{ !empty(Auth::user()->profile) ? Auth::user()->profile->email_personal : null }}"
                            type="text" class="form-control" placeholder="email@example.com" name="email_personal">

                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Số điện thoại <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input
                            value="{{ !empty(Auth::user()->profile) ? Auth::user()->profile->phone : Auth::user()->phone }}"
                            type="text" class="form-control" placeholder="000 0000 000" name="phone">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Địa chỉ</label>
                    <div class="col-sm-9">
                        @if (old('address'))
                            <input type="text" value="{{ old('address') }}" class="form-control"
                                placeholder="13, TP. Hồ Chí Minh" name="address">
                        @else
                            <input type="text"
                                value="{{ !empty(Auth::user()->profile) ? Auth::user()->profile->address : null }}"
                                class="form-control" placeholder="13, TP. Hồ Chí Minh" name="address">
                        @endif

                    </div>
                </div>
                @php
                    if (!empty(Auth::user()->profile) || !empty(Auth::user())) {
                        $cityId = Auth::user()->profile->city_id ?? Auth::user()->city_id;
                        $districtId = Auth::user()->profile->district_id ?? Auth::user()->district_id;
                        $wardId = Auth::user()->profile->ward_id ?? Auth::user()->ward_id;
                    }
                @endphp
                @livewire('change-address-custom', ['cityId' => $cityId,'districtId' => $districtId,'wardId'
                =>$wardId])
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Giới thiệu </label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description"
                            placeholder="Giới thiệu bản thân">{{ !empty(Auth::user()->profile) ? Auth::user()->profile->description : null }}</textarea>
                    </div>
                </div>
                <div class="dashboard-section basic-info-input">
                    <h4><i data-feather="lock"></i>THAY ĐỔI MẬT KHẨU</h4>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Mật Khẩu Cũ</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="password" placeholder="Mật khẩu cũ">
                            @error('password')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Mật Khẩu Mới</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="password_new" placeholder="Mật khẩu mới">
                            @error('password_new')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nhập Lại Mật Khẩu</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="password_confirmation"
                                placeholder="Nhập lại mật khẩu">
                            @error('password_confirmation')
                                <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>
                        <input type="hidden" name="flag" value="1" id="">
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label"></label>
                        <div class="col-sm-9">
                            <button class="button">Lưu Thay Đổi</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')

@endsection
