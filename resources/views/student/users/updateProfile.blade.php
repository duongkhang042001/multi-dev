@extends('layouts.client_guest')
@section('page-title', 'Cập nhật thông tin tài khoản | FPT Intern')
@section('title', 'Cập nhật thông tin tài khoản')
@section('content')
    <div class="alice-bg padding-top-70 padding-bottom-70 ">
        <div class="container">
            <div class="dashboard-content-wrapper">
                <form action="{{ Route('student.initial.update') }}" method="POST" class="dashboard-form"
                    autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <div class="dashboard-section upload-profile-photo">
                        <div class="update-photo">
                            <img class="image" src="{{ FILE_URL . Auth::user()->avatar }}"
                                onerror="this.src='assets/clients/img/default-avatar.png'" alt="">
                        </div>
                        <div class="file-upload">
                            <input type="file" class="file-input" name="avatar">Thay đổi
                        </div>
                    </div>


                    @error('avatar')
                        <div class="jy-alert danger-alert my-5">
                            <div class="icon">
                                <i class="fas fa-check-circle"></i>
                            </div>
                            <p>{{ $message }}</p>
                            <div class="close_">
                                <a href="#"><i class="fas fa-times"></i></a>
                            </div>
                        </div>
                    @enderror

                    <div class="dashboard-section basic-info-input">
                        <h4><i data-feather="user-check"></i>Thông tin cơ bản</h4>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Họ và tên <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="text"
                                    value="{{ !empty(Auth::user()->profile) ? Auth::user()->profile->full_name : null }}"
                                    class="form-control" readonly placeholder="Họ và tên" name="full_name">
                                @error('full_name')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Mã số sinh viên <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" value="{{ !empty(Auth::user()) ? Auth::user()->code : null }}" readonly
                                    class="form-control" placeholder="PS11411" name="code">
                                @error('code')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Chuyên ngành <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="text"
                                    value="{{ !empty(Auth::user()) ? Auth::user()->student->career->name : null }}"
                                    readonly class="form-control" placeholder="PS11411">
                            </div>
                        </div>
                        <div class="readonly-div form-group row">
                            <label class="col-sm-3 col-form-label">Giới tính <span class="text-danger">*</span></label>
                            <div class="col-3 col-md-1">
                                <input class="custom-radio" type="radio" id="radio_1" name="gender" @if (!empty(Auth::user()->profile))
                                @if (Auth::user()->profile->gender == MAN)
                                    checked
                                @endif
                                @endif
                                value="{{ MAN }}">
                                <label for="radio_1">
                                    <span class="dot"></span> <span class="package-type">Nam</span>
                                </label>
                            </div>
                            <div class="col-3 col-sm-1">
                                <input class="custom-radio" type="radio" id="radio_2" name="gender" @if (!empty(Auth::user()->profile))
                                @if (Auth::user()->profile->gender == WOMEN)
                                    checked
                                @endif
                                @endif
                                value="{{ WOMEN }}">
                                <label for="radio_2">
                                    <span class="dot"></span> <span class="package-type">Nữ</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Email <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input value="{{ !empty(Auth::user()) ? Auth::user()->email : null }}" readonly
                                    type="text" class="form-control" placeholder="email@example.com" name="email">
                                @error('email')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Ngày sinh <span class="text-danger">*</span></label>
                            <div class="col-sm-6 col-md-3">
                                <input
                                    value="{{ !empty(Auth::user()->profile) ? date('Y-m-d', strtotime(Auth::user()->profile->birthday)) : null }}"
                                    type="date" readonly name="birthday" class="form-control">
                                @error('birthday')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-section basic-info-input">
                        <h4><i data-feather="lock"></i>BỔ SUNG THÔNG TIN</h4>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Email cá nhân</label>
                            <div class="col-sm-9">
                                <input value="{{ old('email_personal') ?? Auth::user()->profile->email_personal }}"
                                    type="text" class="form-control" placeholder="email@example.com"
                                    name="email_personal">
                                @error('email_personal')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">CMND/CCCD <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control"
                                    value="{{ old('indo') ?? Auth::user()->profile->indo }}"
                                    placeholder="Chứng minh nhân dân / Căn cước công dân" name="indo">
                                @error('indo')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Số điện thoại <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input
                                    value="{{ !empty(Auth::user()->profile) ? Auth::user()->profile->phone : Auth::user()->phone }}"
                                    type="text" class="form-control" placeholder="000 0000 000" name="phone">
                                @error('phone')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Địa chỉ</label>
                            <div class="col-sm-9">
                                @if (old('address'))
                                    <input type="text" value="{{ old('address') }}" class="form-control"
                                        placeholder="13, TP. Hồ Chí Minh" name="address">
                                @else
                                    <input type="text"
                                        value="{{ !empty(Auth::user()->profile) ? Auth::user()->profile->address : null }}"
                                        class="form-control" placeholder="13, TP. Hồ Chí Minh" name="address">
                                @endif
                                @error('address')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        @php
                            if (!empty(Auth::user()->profile) || !empty(Auth::user())) {
                                $cityId = Auth::user()->profile->city_id ?? Auth::user()->city_id;
                                $districtId = Auth::user()->profile->district_id ?? Auth::user()->district_id;
                                $wardId = Auth::user()->profile->ward_id ?? Auth::user()->ward_id;
                            }
                        @endphp
                        @if (!empty(old('ward')) || !empty(old('district')) || !empty(old('city')))
                            @livewire('change-address-custom', ['cityId' => old('city'),'districtId' =>
                            old('district'),'wardId'
                            =>old('ward')])
                        @else
                            @livewire('change-address-custom', ['cityId' => $cityId,'districtId' => $districtId,'wardId'
                            =>$wardId])
                        @endif
                        {{-- @php
                            if (!empty(Auth::user()->student)) {
                                $career_id = Auth::user()->student->career_id;
                                $career_group_id = Auth::user()->student->career->career_group_id;
                            }
                        @endphp
                        @livewire('change-group-career',['careerGroupId'=>$career_group_id, 'careerId'=>$career_id]) --}}
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Giới thiệu </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="description"
                                    placeholder="Giới thiệu bản thân">{{ !empty(Auth::user()->profile) ? Auth::user()->profile->description : null }}</textarea>
                                @error('description')
                                    <span class="text-danger small font-italic">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9 offset-md-3">
                                <div class="form-group mt-0 terms"
                                    style="font-size: 15px; margin-top: 8px; border: 1px dashed #ccc; padding: 5px 10px;">
                                    Lưu ý: <br>
                                    Sinh viên vui lòng kiểm tra và cập nhật đầy đủ thông tin cá nhân. Trường hợp sai thông
                                    tin sinh viên vui lòng liên hệ với nhà trường để được cập nhật thông tin.
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button class="button">Lưu thay đổi</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>

    </script>
@endsection
