@extends('layouts.client')
@section('page-title', 'Đăng ký dịch vụ')
@section('title', 'Đăng ký dịch vụ')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Tổng quan</a></li>
        <li class="breadcrumb-item"><a href="#">Dịch vụ</a></li>
        <li class="breadcrumb-item active" aria-current="page">Cấp bảng điểm</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-5">
                <span class="text-light p-4" style="background-color: #0abb87;"> YÊU CẦU HỦY THỰC TẬP </span>
            </div>

            <div class="mt-4">
                <h5>Lưu ý :</h5>
                <p>1. Trước khi nộp hồ sơ miễn giảm thực tập , bạn có thể xem và sửa lại hồ sơ của mình.</p>
                <p>2. Sau khi xem xong hồ sơ miễn giảm thực tập , bạn vui lòng nhấn nút <span class="text-primary">Nộp hồ sơ</span> bên dưới để nhà trường xét duyệt .</p>
                <p>3. Sau khi nộp hồ sơ miễn giảm thực tập , bạn vui lòng đợi nhà trường xét duyệt của bạn.</p>
            </div>
            @if(empty($oldrequestCancle) || (!empty($oldrequestCancle) && $oldrequestCancle->status != SERVICE_STATUS_PENDING))
            <div class="mt-3">
                <form action="{{route('student.service.intershipCancle.store')}}" method="POST">
                    @csrf
                    @method('POST')
                    <label for="">Ghi chú</label>
                    <div class="form-group">
                        <textarea name="description" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-danger btn-lg">Gửi yêu cầu hủy thực tập</button>
                </form>
            </div>
            @endif
            @if(count($requestCancle))
            <div class="history" style="margin-top: 100px;">
                <hr />
                <div class="download-resume dashboard-section mb-5" style="font-weight: bold;font-size: 20px;">
                    Lịch sử hoạt động
                </div>
                <div class="manage-job-container mt-5">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Tên sinh viên</th>
                                <th>Thời gian yêu cầu hủy</th>
                                <th>Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($requestCancle as $key => $row)
                            <tr class="job-items">
                                <td class="title">
                                    <h5>{{ $row->user->name }}</h5>
                                </td>
                                <td class="deadline">{{ date('d-m-Y h:i:s', strtotime($row->created_at)) }}</td>
                                <td class="status">
                                    @if($row->status == SERVICE_STATUS_PENDING)
                                    <p class="text-warning">Chờ xác nhận</p>
                                    @elseif($row->status == SERVICE_STATUS_APPROVED)
                                    <p class="text-success">Hủy thực thành công</p>
                                    @else($row->status == SERVICE_STATUS_DENINED)
                                    <p class="text-danger">Từ chối hủy thực tập</p>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            @endif



        </div>

    </div>
</div>

@endsection