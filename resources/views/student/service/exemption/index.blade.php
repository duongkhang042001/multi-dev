@extends('layouts.client')
@section('page-title', 'Đăng ký dịch vụ')
@section('title', 'Đăng ký dịch vụ')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Sinh viên</a></li>
        <li class="breadcrumb-item active" aria-current="page">Yêu cầu miễn giảm thực tập</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
    @if(!$service || $service->status == SERVICE_STATUS_CANCEL)
    <div class="download-resume dashboard-section">

        @if($userStudent->is_accept == 1)
        <a href="" data-toggle="modal" data-target="#model-upload">Tải hồ sơ miễn giảm thực tập<i data-feather="upload"></i></a>
        @else
        <a onclick="checkIsAcceptExemption()">Tải hồ sơ miễn giảm thực tập<i data-feather="upload"></i></a>
        @endif
        <p>Lưu ý: Nếu sinh viên huỷ đăng ký hoặc đăng ký không thành công . Sinh viên có thể đăng ký miễn giảm thực tập lại.</p>
    </div>
    @endif
    @if(count($userHistory) > 0)
        @if($service->status == SERVICE_STATUS_WAITING)
        <div class="mt-5">
            <h5 class="mb-2">Lưu ý :</h5>
            <p>1. Trước khi nộp hồ sơ miễn giảm thực tập , bạn có thể xem và sửa lại hồ sơ của mình.</p>
            <p>2. Sau khi xem xong hồ sơ miễn giảm thực tập , bạn vui lòng nhấn nút <span class="text-primary">Nộp hồ sơ</span> bên dưới để nhà trường xét duyệt .</p>
            <p>3. Sau khi nộp hồ sơ miễn giảm thực tập , bạn vui lòng đợi nhà trường xét duyệt.</p>
            <div class="row">
                <div class="col-sm-12">
                    @if (json_decode($service->file))
                        @php
                        $item = json_decode($service->file);
                        @endphp
                        <div class="about-details dashboard-section">
                            <div class="information-and-contact">
                                <div class="information">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h4>Thông tin hồ sơ miễn giảm thực tập</h4>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <a href="" data-toggle="modal" data-target="#model-edit"><i class="fas fa-edit"></i></a>
                                        </div>
                                    </div>
                                    <ul>
                                        <li><span>Tên hồ sơ:</span> {{$item->name}}</li>
                                        <li><span>Thời gian nộp:</span> {{ date('d-m-Y h:i:s', strtotime($item->created_at)) }}</li>
                                        <li class="d-flex"><span>Trạng thái:</span>
                                            <span>Chưa nộp hồ sơ</span>
                                        </li>
                                        <li><span>Nội dung:</span>{{$service->description}}</li>
                                        <li>
                                            <span>Xem hồ sơ:</span>
                                            <a href="{{ \App\Http\Controllers\Controller::getFilePdf($item->code) }}" target="_blank" class="preview mr-4" title="Xem hồ sơ">{{$item->name}}.pdf</a>
                                            <a href="javascript:void(deleteData('deletedRow{{ $service->id }}'))" class="remove text-danger" title="Xoá hồ sơ"><i>( Xoá )</i></a>
                                            <form action="{{ route('student.service-exemption-student.destroy', $service->id) }}" method="POST" id="deletedRow{{ $service->id }}" class="d-none">
                                                @method('DELETE')
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                    <p class="mt-5"><a href="{{ Route('student.service-exemption-student.edit',$service->id) }}" type="button" class="btn px-3 btn-primary" style="font-size: 13px;">Nộp hồ sơ</a></p>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @elseif($service->status == SERVICE_STATUS_PENDING)
        <div class="row">
            <div class="col-sm-12">
                <div class="input-block-wrap">
                    <div class="text-left col-sm-12" style="font-size: 15px;  border: 1px dashed #ccc; padding: 15px 10px;">
                        <h5 class="p-4 text-primary">Vui lòng chờ đợi nhà trường xét duyệt miễn giảm thực tập của bạn !</h5>
                    </div>
                </div>
            </div>
        </div>
        @elseif($service->status == SERVICE_STATUS_DENINED)
        <div class="row">
            <div class="col-sm-12">
                <div class="input-block-wrap mb-5">
                    <div class="text-left col-sm-12" style="font-size: 15px;  border: 1px dashed #ccc; padding: 15px 10px;">
                        <h5 class="text-left p-4 text-danger">Nộp hồ sơ miễn giảm thực tập không thành công</h5>
                        <p>- Nhà trường không chấp nhận yêu cầu của bạn vì lý do {{$service->reason}}.</p>
                        <p>- Bạn có thể nộp hồ sơ miễn giảm thực tập ở bên dưới !</p>
                        <div class="download-resume dashboard-section">
                            <a href="" data-toggle="modal" data-target="#model-upload">Tải hồ sơ miễn giảm thực tập<i data-feather="upload"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    @else
        <div class="mt-5">
            <h5 class="mb-2">Lưu ý : điều kiện được xét miễn thực tập :</h5>
            <p>- Có hợp đồng lao động (hợp pháp, còn hiệu lực) có xác định thời hạn từ 02 tháng trở lên. </p>
            <p>- Nội dung công việc ghi trong hợp đồng phải phù hợp với chuyên ngành đào tạo.</p>
            <p>- Có xác nhận về thái độ và tinh thần của lãnh đạo doanh nghiệp.</p>
            <p>- Có đơn xin miễn thực tập.</p>
            <p>- Link tải mẫu đơn: <a href="https://drive.google.com/file/d/1H6xTgyBAp16G_w8FhG6mMyDPOTdACO3j/view?fbclid=IwAR0ZvCPSj99jWargKCjVdFp-S7mIBb3PFeyPw9wpjdLHbPLCAyI4gfVgJCE" target="_blank" class="text-info">tại đây</a></p>
        </div>
    @endif
    @if(count($userHistory) > 0)
    <div class="history" style="margin-top: 100px;">
        <hr />
        <div class="download-resume dashboard-section mb-5" style="font-weight: bold;font-size: 20px;">
                Lịch sử đã đăng ký
        </div>
        <div class="manage-job-container mt-5">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tên sinh viên</th>
                            <th>Thời gian đăng ký</th>
                            <th>Trạng thái</th>
                            @foreach ($userHistory as $key => $row)
                            @if($row->status == SERVICE_STATUS_PENDING || $row->status == SERVICE_STATUS_WAITING)
                            <th class="action">Hành động</th>
                            @endif
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($userHistory as $key => $row)
                            <tr class="job-items">
                                <td class="title">
                                    <h5>{{ $row->user->name }}</h5>
                                </td>
                                <td class="deadline">{{ date('d-m-Y h:i:s', strtotime($row->created_at)) }}</td>
                                <td class="status">
                                    @if($row->status == SERVICE_STATUS_WAITING)
                                    <p class="text-warning">Chưa nộp hồ sơ</p>
                                    @elseif($row->status == SERVICE_STATUS_PENDING)
                                    <p class="text-warning">Chờ xác nhận</p>
                                    @elseif($row->status == SERVICE_STATUS_APPROVED)
                                    <p class="text-success">Đăng ký thành công</p>
                                    @elseif($row->status == SERVICE_STATUS_DENINED)
                                    <p class="text-danger">Đăng ký thất bại</p>
                                    @else($row->status == SERVICE_STATUS_CANCEL)
                                    <p class="text-danger">Huỷ đăng ký</p>
                                    @endif
                                </td>
                                <td class="action">
                                    @if($row->status == SERVICE_STATUS_PENDING || $row->status == SERVICE_STATUS_WAITING)

                                            <a href="javascript:void(deleteData('deletedRow{{ $service->id }}'))" type="button" class="btn btn-danger mr-4">Huỷ</button>

                                        <form action="{{ route('student.service-exemption-student.delete', $service->id) }}" method="POST" id="deletedRow{{ $service->id }}" class="d-none">
                                                @method('DELETE')
                                                @csrf
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

        </div>
    </div>
    @endif

</div>

<!-- Modal UPLOAD EXEMPTION -->
<div class="edication-background details-section dashboard-section">
    <div class="modal fade modal-education" id="model-upload" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="title">
                        <h4><i class="fas fa-upload mr-2"></i>Nộp hồ sơ miễn giảm thực tập </h4>
                    </div>
                    <div class="content">
                        <form action="{{ Route('student.service-exemption-student.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('POST')
                            <div class="input-block-wrap">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Thông tin hồ sơ</label>
                                    <div class="col-sm-9">
                                        <div class="input-group row ">
                                            <div class="input-group-prepend col-sm-3">
                                                <div class="input-group-text">Tên hồ sơ</div>
                                            </div>
                                            <input type="text" name="name" class="form-control col-sm-9" value="{{ old('name') }}">
                                            @error('name')
                                            <div class="text text-danger col-sm-9 float-center">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="offset-sm-3 col-sm-9">
                                        <div class="input-group row">
                                            <div class="input-group-prepend col-sm-3">
                                                <div class="input-group-text">Nội dung</div>
                                            </div>
                                            <textarea class="form-control col-sm-9" name="description" value="{{old('description')}}" placeholder="Bạn có thể ghi chú tại đây"></textarea>
                                            @error('description')
                                            <div class="text text-danger col-sm-8 float-center">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-3 col-sm-9">
                                        <div class="input-group row">
                                            <input type="file" name="file" class="filestyle" data-text="Chọn hồ sơ (PDF)" data-buttonBefore="true" data-btnClass="btn-lg btn-primary" value="{{ old('file') }}">
                                            @error('file')
                                            <div class="text text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input-block-wrap">
                                <div class="form-group form-check text-center">
                                    <input id="check" type="checkbox" class="form-check-input mt-2" name="check" checked>
                                    <label for="check" class="form-check-label ml-2 pb-4">Bạn có đồng ý với Chính sách và Quyền
                                        lợi và Bảo mật của chúng tôi.</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="offset-sm-3 col-sm-9">
                                    <div class="buttons">
                                        <button class="primary-bg">Tải lên</button>
                                        <button class="bg-danger text-white" data-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(count($userHistory) > 0)
<!-- Modal EDIT EXEMPTION -->
<div class="edication-background details-section dashboard-section">
    <div class="modal fade modal-education" id="model-edit" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="title">
                        <h4><i class="far fa-edit mr-2"></i>Thay đổi hồ sơ miễn giảm thực tập </h4>
                    </div>
                    <div class="content">

                        <form action="{{ Route('student.service-exemption-student.update',$service->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="input-block-wrap">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Thông tin hồ sơ</label>
                                    <div class="col-sm-9">
                                        <div class="input-group row ">
                                            <div class="input-group-prepend col-sm-3">
                                                <div class="input-group-text">Tên hồ sơ</div>
                                            </div>
                                            @php
                                            $items = json_decode($service->file);
                                            @endphp

                                            <input type="text" name="name" class="form-control col-sm-9" value="@if($items) {{!empty(old('name')) ? old('name') : $items->name}} @endif">
                                            @error('name')
                                            <div class="text text-danger col-sm-9 float-center">{{ $message }}</div>
                                            @enderror

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="offset-sm-3 col-sm-9">
                                        <div class="input-group row">
                                            <div class="input-group-prepend col-sm-3">
                                                <div class="input-group-text">Nội dung</div>
                                            </div>
                                            <textarea class="form-control col-sm-9" name="description">@if($service->description) {{!empty(old('description')) ? old('description') : $service->description}} @endif</textarea>
                                            @error('description')
                                            <div class="text text-danger col-sm-8 float-center">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-3 col-sm-9">
                                        <div class="input-group row">
                                            <input type="file" data-placeholder="Bạn có muốn thay đổi hồ sơ của mình không ?" name="file" class="filestyle" data-text="Chọn hồ sơ (PDF)" data-buttonBefore="true" data-btnClass="btn-lg btn-primary" value="{{ old('file') }}">
                                            @error('file')
                                            <div class="text text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="offset-sm-3 col-sm-9">
                                    <div class="buttons">
                                        <button class="primary-bg">Chỉnh sửa</button>
                                        <button class="bg-danger text-white" data-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
@push('css')
<style>
    .input-group {
        align-items: end;
    }
</style>
@endpush
@section('scripts')
<script src="assets/libs/bootstrap-filestyle2/bootstrap-filestyle.min.js"></script>

@if ($errors->any())
<script>
    $('#model-upload').modal('show')
</script>
@endif
@endsection
