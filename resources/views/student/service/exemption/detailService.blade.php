@extends('layouts.client_guest')
@section('page-title', 'Hồ sơ miễn giảm thực tập | FPT Intern')
@section('title', 'Hồ sơ miễn giảm thực tập')
@section('content')
    <div class="alice-bg padding-top-70 padding-bottom-70 ">
        <div class="container">
            <div class="dashboard-content-wrapper">
                <iframe src="{{ $filePathPublic }}" frameBorder="0" scrolling="auto" height="1200px"
                    width="100%"></iframe>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@endsection
