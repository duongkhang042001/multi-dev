@extends('layouts.client')
@section('page-title', 'Đăng ký dịch vụ')
@section('title', 'Đăng ký thực tập lại')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Sinh viên</a></li>
        <li class="breadcrumb-item active" aria-current="page">Yêu cầu thực tập lại</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
    @if($checkService && $checkService->status == SERVICE_STATUS_PENDING)
    <div class="row">
        <div class="col-sm-12">
            <div class="input-block-wrap">
                <div class="text-left col-sm-12" style="font-size: 15px;  border: 1px dashed #ccc; padding: 15px 10px;">
                    <h5 class="p-4 text-primary">Vui lòng chờ đợi nhà trường xét duyệt đăng ký thực tập lại của bạn !</h5>
                </div>
            </div>
        </div>
    </div>

    @elseif($checkService && $checkService->status == SERVICE_STATUS_APPROVED)
    <div class="row">
        <div class="col-sm-12">
            <div class="thumb">
                <img src="assets/clients/img/exemption-success.jpg" class="w-100" alt="">
            </div>
            <div class="body">
                <h5 class="text-center p-4 text-primary">Chúc mừng bạn đã đăng ký thực tập lại thành công</h5>
                <div class="info text-center text-primary">
                    <p>Bạn có thể nộp ứng tuyển bài đăng tuyển để được thực tập</p>
                </div>
                <div class="info text-center text-primary">
                    <p>Chúc bạn thành công</p>
                </div>
            </div>
        </div>
    </div>
    @elseif($checkService && $checkService->status == SERVICE_STATUS_DENINED)
    <div class="row">
        <div class="col-sm-12">
            <div class="input-block-wrap mb-5">
                <div class="text-left col-sm-12" style="font-size: 15px;  border: 1px dashed #ccc; padding: 15px 10px;">
                    <h5 class="text-left p-4 text-danger">Đăng ký thực tập lại không thành công</h5>
                    <p>- Nhà trường không chấp nhận yêu cầu của bạn vì lý do {{$checkService->reason}}.</p>
                    <p>- Bạn có thể đăng ký thực tập lại ở bên dưới !</p>
                </div>
            </div>
            <div class="download-resume dashboard-section mb-5" style="font-weight: bold;font-size: 20px;">
            Thông tin sinh viên
            </div>
            <table class="display nowrap table table-hover table-striped table-bordered" data-select2-id="5">
                <thead>
                    <tr>
                        <th style="width: 200px;">Họ và tên:</th>
                        <td>{{$userStudent->name}}</td>
                    </tr>
                    <tr>
                        <th>Mã sinh viên:</th>
                        <td>{{$userStudent->code}}</td>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <td>{{$userStudent->email}}</td>
                    </tr>
                    <tr>
                        <th>Số điện thoại:</th>
                        <td>{{$userStudent->profile->phone}}</td>
                    </tr>
                    <tr>
                        <th>Kỳ thứ:</th>
                        <td>{{$userStudent->student->current_semester}}</td>
                    </tr>
                    <tr>
                        <th>Trạng thái:</th>
                        <td>
                            
                            @if($checkService && $checkService->status == SERVICE_STATUS_APPROVED)
                                Đã đăng ký thực tập lại thành công
                            @else
                                @if($userStudent->student->study_status == 1)
                                <span class="green">HDI ( Học đi )</span>
                                @else
                                <span class="text-danger">BLUU ( Bảo lưu )</span>
                                @endif 
                            @endif 
                        </td>
                    </tr>
                    <tr>
                        <th>Loại dịch vụ:</th>
                        <td>
                            Đăng ký thực tập lại
                        </td>
                    </tr>
                    
                        @if($checkService == NULL || $checkService->status == SERVICE_STATUS_CANCEL || $checkService->status == SERVICE_STATUS_DENINED)
                        <tr>
                            <td class=""></td>
                            <td colspan="">
                                <form action="{{ Route('student.service-internship-again-student.store') }}" method="POST">
                                    @csrf
                                    @method('POST')
                                    <button type="submit" class="btn btn-success btn-lg">Đăng ký</button> 
                                </form>
                            </td>
                        </tr>
                        @endif
                    

                </thead>   
            </table>
        </div>
    </div>
    @else
    <div class="download-resume dashboard-section mb-5" style="font-weight: bold;font-size: 20px;">
            Thông tin sinh viên
    </div>
    <table class="display nowrap table table-hover table-striped table-bordered" data-select2-id="5">
        <thead>
            <tr>
                <th style="width: 200px;">Họ và tên:</th>
                <td>{{$userStudent->name}}</td>
            </tr>
            <tr>
                <th>Mã sinh viên:</th>
                <td>{{$userStudent->code}}</td>
            </tr>
            <tr>
                <th>Email:</th>
                <td>{{$userStudent->email}}</td>
            </tr>
            <tr>
                <th>Số điện thoại:</th>
                <td>{{$userStudent->profile->phone}}</td>
            </tr>
            <tr>
                <th>Kỳ thứ:</th>
                <td>{{$userStudent->student->current_semester}}</td>
            </tr>
            <tr>
                <th>Trạng thái:</th>
                <td>
                    
                    @if($checkService && $checkService->status == SERVICE_STATUS_APPROVED)
                        Đã đăng ký thực tập lại thành công
                    @else
                        @if($userStudent->student->study_status == 1)
                        <span class="green">HDI ( Học đi )</span>
                        @else
                        <span class="text-danger">BLUU ( Bảo lưu )</span>
                        @endif 
                    @endif 
                </td>
            </tr>
            <tr>
                <th>Loại dịch vụ:</th>
                <td>
                    Đăng ký thực tập lại
                </td>
            </tr>
            
                @if($checkService == NULL || $checkService->status == SERVICE_STATUS_CANCEL || $checkService->status == SERVICE_STATUS_DENINED)
                <tr>
                    <td class=""></td>
                    <td colspan="">
                        <form action="{{ Route('student.service-internship-again-student.store') }}" method="POST">
                            @csrf
                            @method('POST')
                            <button type="submit" class="btn btn-success btn-lg">Đăng ký</button> 
                        </form>
                    </td>
                </tr>
                @endif
            

        </thead>   
    </table>
    @endif
    @if(count($userHistory) == 0)
   
    @else
    <div class="history" style="margin-top: 100px;">
        <hr />
        <div class="download-resume dashboard-section mb-5" style="font-weight: bold;font-size: 20px;">
                Lịch sử đã đăng ký
        </div>
        <div class="manage-job-container mt-5">    
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tên sinh viên</th>
                            <th>Thời gian đăng ký</th>
                            <th>Trạng thái</th>
                            @foreach ($userHistory as $key => $row)
                            @if($row->status == SERVICE_STATUS_PENDING)
                            <th class="action">Hành động</th>
                            @endif
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($userHistory as $key => $row)
                            <tr class="job-items">
                                <td class="title">
                                    <h5>{{ $row->user->name }}</h5>
                                </td>
                                <td class="deadline">{{ date('d-m-Y h:i:s', strtotime($row->created_at)) }}</td>
                                <td class="status">
                                    @if($row->status == SERVICE_STATUS_PENDING)
                                    <p class="text-warning">Chờ xác nhận</p>
                                    @elseif($row->status == SERVICE_STATUS_APPROVED)
                                    <p class="text-success">Đăng ký thành công</p>
                                    @elseif($row->status == SERVICE_STATUS_DENINED)
                                    <p class="text-danger">Đăng ký thất bại</p>
                                    @else($row->status == SERVICE_STATUS_CANCEL)
                                    <p class="text-danger">Huỷ đăng ký</p>
                                    @endif
                                </td>
                                <td class="action">
                                    @if($row->status == SERVICE_STATUS_PENDING)
                                        <a href="javascript:void(deleteData('deletedRow{{ $row->id }}'))" type="button" class="btn btn-danger mr-4">Huỷ</button>

                                        <form action="{{route('student.service-internship-again-student.destroy',$row->id)}}" method="POST" id="deletedRow{{ $row->id }}" class="d-none">
                                            @method('DELETE')
                                            @csrf
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
          
        </div>
    </div>
    @endif
</div>

@endsection
@push('css')
<style>
    .input-group {
        align-items: end;
    }
</style>
@endpush
@section('scripts')
<script src="assets/libs/bootstrap-filestyle2/bootstrap-filestyle.min.js"></script>

@endsection