@extends('layouts.client')
@section('page-title', 'Đăng ký dịch vụ')
@section('title', 'Đăng ký dịch vụ')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
    <li class="breadcrumb-item active" aria-current="page">Đăng ký dịch vụ</li>
  </ol>
</nav>
@endsection

@section('content')
<!-- How It Work -->
<div class="dashboard-content-wrapper">
  <div class="dashboard-applied">

    <div class="how-it-work">
      <div class="row">
        <!-- <div class="col-lg-6 order-lg-2">
                  <div class="working-process-thumb">
                    <img src="images/feature/thumb-2.jpg" class="img-fluid" alt="">
                  </div>
                </div> -->
        <div class="col-lg-12 order-lg-1">
          <h5 class="mb-5 p-2 font-weight-bold">Danh sách các dịch vụ trực tuyến</h5>
          <div class="working-process">
            <h3><span>#01.</span>Yêu cầu miễn giảm thực tập</h3>
            <p class="font-weight-bold">Điều kiện</p>
            <p>- Có hợp đồng lao động (hợp pháp, còn hiệu lực) có xác định thời hạn từ 02 tháng trở lên. </p>
            <p>- Nội dung công việc ghi trong hợp đồng phải phù hợp với chuyên ngành đào tạo.</p>
            <p>- Có xác nhận về thái độ và tinh thần của lãnh đạo doanh nghiệp.</p>
            <p>- Có đơn xin miễn thực tập.</p>
            <p>- Link tải mẫu đơn: <a href="https://drive.google.com/file/d/1H6xTgyBAp16G_w8FhG6mMyDPOTdACO3j/view?fbclid=IwAR0ZvCPSj99jWargKCjVdFp-S7mIBb3PFeyPw9wpjdLHbPLCAyI4gfVgJCE" target="_blank" class="text-info">tại đây</a></p>
            <p class="text-warning"><a href="{{route('student.service-exemption-student.create')}}"><i class="fas fa-check-double mr-2"></i>Đăng ký dịch vụ</a></p>
          </div>
          <div class="working-process">
            <h3><span>#02.</span>Yêu cầu cấp bảng điểm</h3>
            <p class="font-weight-bold">Điều kiện</p>
            <p>- Sinh viên yêu cầu cấp bảng điểm có mộc đỏ vui lòng liên hệ nhà trường đóng phí </p>
            <p>- Sinh viên cấp bảng điểm tối đa 3 lần trong 1 kỳ học.</p>
            <p>- Sinh viên khi cấp bảng điểm vui lòng điền ý do cấp bảng điểm.</p>
            <p class="text-warning"><a href="{{route('student.transcript.index')}}"><i class="fas fa-check-double mr-2"></i>Đăng ký dịch vụ</a></p>
          </div>
          <div class="working-process">
            <h3><span>#03.</span>Yêu cầu đăng ký thực tập lại</h3>
            <p class="font-weight-bold">Điều kiện</p>
            <p>- Đã thực tập trong các kỳ trước nhưng bị Fail </p>
            <p>- Đủ điều kiện thực tập nhưng KHÔNG đạt hoặc CHƯA đăng ký thực tập ở các kỳ trước đó.</p>
            <p class="text-warning">
              @if($report && $report->status == REPORT_FAIL)
              <a class="pointer" onclick="checkReport()"><i class="fas fa-check-double mr-2"></i>Đăng ký dịch vụ</a>
              @else
                <a href="{{route('student.service-internship-again-student.index')}}"><i class="fas fa-check-double mr-2"></i>Đăng ký dịch vụ</a>
              @endif
            </p>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- How It Work End -->
@endsection