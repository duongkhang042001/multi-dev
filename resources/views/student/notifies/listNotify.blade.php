@extends('layouts.client')
@section('page-title', 'Thông báo')
@section('title', 'Thông báo')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Sinh viên</a></li>
            <li class="breadcrumb-item active" aria-current="page">Thông báo</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="dashboard-content-wrapper">
        <div class="dashboard-section dashboard-recent-activity">
            <h4 class="title">
                Danh sách thông báo
            </h4>
            @foreach ($notifyDetail as $item)
                <div class="activity-list">
                    <i class="fas fa-bell"></i>
                    <div class="content">
                        <h5>{{ $item->notify->title }}</h5>
                        <span class="time">5 giờ trước</span>
                    </div>
                </div>
            @endforeach


        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

@endpush
