@extends('layouts.client')
@section('page-title', 'Sinh viên')
@section('title', 'Sinh viên')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Sinh viên</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tổng quan</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="dashboard-content-wrapper">
        <div class="about-details details-section dashboard-section pl-0">
            <h4><i class="fas fa-list mr-3"></i>Tổng quan</h4>
            <div class="home-student information mt-0">
                <h4>Thông tin kỳ học</h4>
                <ul class="row">
                    <div class="px-0 col-sm-6">
                        <li><span>Kỳ học:</span> {{ $semester->name }}
                        </li>
                        <li>
                            <span>Ngày bắt đầu tìm nơi thực tập: </span> {{ date('d/m/Y', strtotime($semester->timeline->start_find)) }}
                        </li>
                    </div>
                    <div class="px-0 col-sm-6">
                        <li>
                            <span>Thời gian:</span>{{ date('d/m/Y', strtotime($semester->from)) . ' - ' . date('d/m/Y', strtotime($semester->to)) }}
                        </li>
                        <li>
                            <span>Ngày kết thúc tìm nơi thực tập:</span>{{ date('d/m/Y', strtotime($semester->timeline->end_find)) }}
                        </li>
                    </div>
                    @if (!empty($report))
                        <div class="px-0 col-sm-6 mt-3">
                            <li>
                                <span>Hình thức thực tập:</span>
                                {!! $report->is_outside ? '<span class="text-info">Doanh nghiệp ngoài hệ thống.</span>' : '<span class="text-info">Doanh nghiệp trong hệ thống.</span>' !!}
                            </li>
                        </div>
                    @endif
                    <div class="px-0 col-sm-6 mt-3">
                        <li><span>Trạng thái thực tập:</span>
                            @if (empty($report))
                                <span class="text-info">
                                    Bạn chưa có nơi thực tập
                                </span>
                            @elseif (!empty($report->company) && $report->company == COMPANY_STATUS_WAITING)
                                <span class="text-warning">
                                    Chờ phê duyệt đăng ký
                                </span>
                            @else
                                @switch($report->status)
                                    @case(REPORT_PENDING)
                                        <span class="text-info">
                                            Đang viết báo cáo thực tập.
                                        </span>
                                    @break
                                    @case(REPORT_DONE)
                                        @if (empty($report->reportRate) || !$report->reportRate->is_active)
                                            <span class="text-info">
                                                Doanh nghiệp chưa nhận xét báo cáo thực tập.
                                            </span>
                                        @elseif($report->reportRate->is_active)
                                            <span class="text-info">
                                                Doanh nghiệp đã nhận xét báo cáo thực tập.
                                            </span>
                                        @endif
                                    @break
                                    @case(REPORT_FINISHED)
                                        @if (!empty($report->request_reset))
                                            <span class="text-info">
                                                Yêu cầu của bạn đã được gửi đến nhà trường. Vui lòng chờ xét duyệt.
                                            </span>
                                        @else
                                            <span class="text-info">
                                                Đã nộp, chờ xử lý.
                                            </span>
                                        @endif
                                    @break
                                    @case(REPORT_CANCEL)
                                        <span class="text-warning">
                                            Huỷ thực tập bởi
                                            {{ $report->serviceCancel->roleUser->code == STUDENT_ROLE_CODE ? 'sinh viên' : 'doanh nghiệp' }}.
                                        </span>
                                    @break
                                    @case(REPORT_FAIL)
                                        <span class="text-danger">
                                            Rớt thực tập.
                                        </span>
                                    @break
                                    @case(REPORT_PASS)
                                        <span class="text-success">
                                            Hoàn thành thực tập.
                                        </span>
                                    @break
                                    @default
                                        <span class="text-info">
                                            Đang chờ xử lý.
                                        </span>
                                @endswitch
                            @endif

                        </li>
                    </div>
                    <div class="px-0 col-sm-12 mt-3">
                        @if (!$checkTimeline)
                            <li>
                                <span class="text-danger">Bạn đã trễ hạn thực tập ở học kỳ {{ $semester->name }}. Vui
                                    lòng liên hệ với nhà trường để xử lý.</span>
                            </li>
                        @endif
                    </div>
                </ul>
            </div>
        </div>
        <div class="edication-background details-section dashboard-section pl-0 pb-5">
            <h4 class=""><i class="fas fa-question mr-3"></i>Những điều cần lưu ý</h4>

            <div class="input-block-wrap">
                <div class="text-left col-sm-12" style="font-size: 15px;  border: 1px dashed #ccc; padding: 15px 10px;">
                    <div class="education-label">
                        <span class="study-year font-weight-bold">Sinh viên ứng tuyển trên hệ thống</span>
                        <div class="warning-report">
                            1. Sinh viên cần bổ sung đầy đủ thông tin của báo cáo thực tập<br>
                            2. Hạn nộp trước ngày kết thúc thực tập của kỳ<br>
                            3. Sinh viên cân nhắc hủy thực tập hoặc thay đổi nơi thực tập, nếu thay đổi trong quá trình thực
                            tập sẽ bị đánh rớt và báo cáo thực tập sẽ được bổ sung vào kỳ sau<br>
                        </div>
                    </div>
                    <div class="education-label">
                        <span class="study-year font-weight-bold">Sinh viên tự tìm nơi thực tập ngoài hệ thống</span>
                        <div class="warning-report">
                            1. Sinh viên tự tìm nơi thực tập bên ngoài hệ thống, vui lòng <a
                                href="{{ route('student.company.create') }}" class="text-info font-weight-bold">đăng ký
                                doanh nghiệp và thực tập</a><br>
                            2. Sinh viên bổ sung thông tin và báo cáo ngày trên hệ thống, và tải lên file đánh giá của doanh
                            nghiệp.<br>
                            3. Sinh viên lưu ý thời hạn đăng ký và nộp báo cáo thực tập!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    <style>
        .warning-report {
            color: dimgray;
        }

    </style>
@endpush
@push('script')
    <script type="text/javascript">
        function checkInitalCV() {
            Swal.fire({
                title: "Bạn đã có doanh ngiệp thực tập?",
                text: "Nếu sinh viên đã có nơi thực tập vui lòng đăng ký doanh nghiệp với nhà trường.",
                type: "question",
                showCancelButton: true,
                confirmButtonText: "Đăng ký",
                cancelButtonText: "Tạo hồ sơ",
                confirmButtonClass: "btn btn-lg btn-success mt-2",
                cancelButtonClass: "btn btn-lg btn-info ml-2 mt-2",
                buttonsStyling: false
            }).then(function(t) {
                if (t.value) {
                    window.location.href = "student/company/create";
                }
                if (t.dismiss === Swal.DismissReason.cancel) {
                    window.location.href = "student/resume";
                }
            })
        }
        function checkInitalApply() {
            Swal.fire({
                title: "Bạn đã có doanh ngiệp thực tập?",
                text: "Nếu sinh viên đã có nơi thực tập vui lòng đăng ký doanh nghiệp với nhà trường.",
                type: "question",
                showCancelButton: true,
                confirmButtonText: "Đăng ký",
                cancelButtonText: "Tìm kiếm doanh nghiệp",
                confirmButtonClass: "btn btn-lg btn-success mt-2",
                cancelButtonClass: "btn btn-lg btn-info ml-2 mt-2",
                buttonsStyling: false
            }).then(function(t) {
                if (t.value) {
                    window.location.href = "student/company/create";
                }
                if (t.dismiss === Swal.DismissReason.cancel) {
                    window.location.href = "recruitment";
                }
            })
        }
    </script>
    @if (Auth::guard('student')->user()->student->is_accept && empty(Auth::guard('student')->user()->student->getCV()))
        <script>
            checkInitalCV();
        </script>
    @elseif(empty(Auth::guard('student')->user()->report) || !empty($checkRcm))
    @elseif(empty(Auth::guard('student')->user()->report))
        <script>
            checkInitalApply();
        </script>
    @endif

@endpush
