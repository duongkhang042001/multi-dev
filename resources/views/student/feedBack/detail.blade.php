@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
    <div class="row">
        <div class="col-sm-12 col-lg-12 col-md-12">
            <h5 class="text-info">{{$feedback->title}}</h5>
            <hr />
            <div class="padding-top-60">
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="about-details details-section">
                            <h4><i data-feather="align-left"></i>Nội dung</h4>
                            <p>{{$feedback->content}}</p>
                            <span class="font-italic">Ngày phản hồi: {{date('d/m/Y H:i:s',strtotime($feedback->created_at))}} </span>
                        </div>
                    </div>
                </div>
            </div>
            @if(!empty($feedback->content_return))
            <div class="padding-top-60">
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="about-details details-section">
                            <h4><i data-feather="align-left"></i>Nhà trường phản hồi</h4>
                            <p>{{$feedback->content_return}}</p>
                            <span class="font-italic">Phản hồi bởi: Phòng quan hệ doanh nghiệp </span> <br>
                            <span class="font-italic">Ngày phản hồi: {{date('d/m/Y H:i:s',strtotime($feedback->updated_at))}} </span>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
@push('css')
<!-- third party css -->
<link href="assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
@endpush
@push('script')

<!-- Required datatable js -->
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/jszip/jszip.min.js"></script>
<script src="assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script> b
<!-- Responsive examples -->
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#btn').on('click', function(event) {
            jQuery('#form').toggle('show');
        });
    });

    jQuery(document).ready(function() {
        jQuery('#hideshow').on('click', function(event) {
            jQuery('#form1').toggle('show');
        });
    });
</script>
@endpush