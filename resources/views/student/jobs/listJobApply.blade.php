@extends('layouts.client')
@section('page-title', 'Công việc đã ứng tuyển | FPT Intern')
@section('title', 'Công việc đã ứng tuyển')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Sinh viên</a></li>
        <li class="breadcrumb-item active" aria-current="page">Công việc đã ứng tuyển</li>
    </ol>
</nav>
@endsection

@section('content')

<div class="dashboard-content-wrapper">
    <div class="dashboard-applied">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link d-flex align-items-start {{Request::has('pendings')||empty(Request::all()) ? 'active' : ''}}" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Chờ xử lý @if(count($pending)) <span class="badge badge-primary badge-pill ml-1 ">{{count($pending)}}</span>@endif</a>
                <a class="nav-item nav-link d-flex align-items-start {{Request::has('interviews') ? 'active' : ''}}" id="nav-interview-tab" data-toggle="tab" href="#nav-interview" role="tab" aria-controls="nav-interview" aria-selected="true">Tình trạng phỏng vấn @if(count($interview))<span class="badge badge-primary badge-pill ml-1">{{count($interview)}}</span>@endif</a>
                <a class="nav-item nav-link d-flex align-items-start {{Request::has('waitings') ? 'active' : ''}}" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Đang ứng tuyển @if(count($waiting))<span class="badge badge-primary badge-pill ml-1">{{count($waiting)}}</span>@endif</a>
                <a class="nav-item nav-link d-flex align-items-start {{Request::has('cancels') ? 'active' : ''}}" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Hoàn tất ứng tuyển @if(count($cancel))<span class="badge badge-primary badge-pill  ">{{count($cancel)}}</span>@endif</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade {{Request::has('pendings')||empty(Request::all()) ? 'show active' : ''}}" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="dashboard-apply-area">
                    @if(!empty($pendings))
                    <div class="manage-candidate-container mt-5 " style="overflow-x:hidden">
                        <table class="table">
                            <tbody>
                                @foreach($pendings as $row)
                                <tr>
                                    <td class="title">
                                        <div class="thumb">
                                            <a href="#">
                                                <img src="{{$row->recruitmentPost->company->getAvatar()}}" onerror="this.src='assets/clients/img/job/company-logo-1.png'" class="w-100" alt="">
                                            </a>
                                        </div>
                                        <div class="body">
                                            <h5>
                                                <a href="{{route('recruitment',$row->post->slug)}}">{{Str::limit($row->post->title,45)}}</a>
                                            </h5>
                                            <div class="info">
                                                <span class="designation"><a href="#"><i data-feather="check-square"></i>{{
                                                date('d/m/Y H:i:s', strtotime($row->created_at))}}</a></span>
                                            </div>
                                        </div>
                                    </td>
                                    @if($row->status == RECRUITMENT_PENDING)
                                    <td class="status text-center">
                                        <p class="text-primary">Chờ xác nhận </br> để được phỏng vấn</p>
                                    </td>
                                    <td class="status text-center">
                                        <a class="text-primary pointer" onclick="statusInterview('{{$row->id}}')"><i data-feather="check-circle"></i></a>
                                    </td>
                                    @elseif($row->status == RECRUITMENT_PENDING_APPROVE)
                                    <td class="status text-center">
                                        <p class="text-success">Chờ xác nhận </br> để vào doanh nghiệp</p>
                                    </td>
                                    <td class="status text-center">
                                        <a class="text-success pointer" onclick="getPending('{{$row->recruitmentPost->company_id}}','{{$row->recruitmentPost->post_id}}','{{$row->id}}')" data-toggle="modal" data-target="#modal-accept"><i data-feather="eye"></i></a>
                                    </td>
                                    @elseif($row->status == RECRUITMENT_INTERVIEW_SUCCESS)
                                    <td class="status text-center">
                                        <p class="text-info">Phỏng vấn </br> thành công</p>
                                    </td>
                                    <td class="status text-center">
                                        <a class="text-info pointer" onclick="statusInterviewSuccess('{{$row->id}}')"><i data-feather="check-circle"></i></a>
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-list text-center">
                            <nav class="navigation pagination">
                                <div class="nav-links">
                                    @if ($pendings->lastPage() > 1)
                                    <a class="prev page-numbers" href="{{ $pendings->url(1) }}"><i class="fas fa-angle-left"></i></a>
                                    @for ($i = 1; $i <= $pendings->lastPage(); $i++)
                                        <?php
                                        $half_total_links = floor(5 / 2);
                                        $from = $pendings->currentPage() - $half_total_links;
                                        $to = $pendings->currentPage() + $half_total_links;
                                        if ($pendings->currentPage() < $half_total_links) {
                                            $to += $half_total_links - $pendings->currentPage();
                                        }
                                        if ($pendings->lastPage() - $pendings->currentPage() < $half_total_links) {
                                            $from -= $half_total_links - ($pendings->lastPage() - $pendings->currentPage()) - 1;
                                        }
                                        ?>
                                        @if ($from < $i && $i < $to) @if ($pendings->currentPage() == $i)
                                            <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                            @else
                                            <a class="page-numbers" href="{{ $pendings->url($i) }}">{{ $i }}</a>
                                            @endif


                                            @endif
                                            @endfor

                                            <a class="next page-numbers" href="{{ $pendings->url($pendings->lastPage()) }}"><i class="fas fa-angle-right"></i></a>
                                            @endif
                                </div>
                            </nav>
                        </div>
                    </div>
                    @endif
                    @if(count($pendings) == 0)
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="mt-5 text-center text-dark">
                                ( Chưa có dữ liệu !! )
                            </p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="tab-pane fade {{Request::has('interviews') ? 'show active' : ''}}" id="nav-interview" role="tabpanel" aria-labelledby="nav-interview-tab">
                <div class="dashboard-apply-area">
                    @if(!empty($interviews))
                    <div class="manage-candidate-container mt-5 " style="overflow-x:hidden">
                        <table class="table">
                            <tbody>
                                @foreach($interviews as $row)
                                <tr>
                                    <td class="title">
                                        <div class="thumb">
                                            <a href="#">
                                                <img src="{{$row->recruitmentPost->company->getAvatar()}}" onerror="this.src='assets/clients/img/job/company-logo-1.png'" class="w-100" alt="">
                                            </a>
                                        </div>
                                        <div class="body">
                                            <h5>
                                                <a href="{{route('recruitment',$row->post->slug)}}">{{Str::limit($row->post->title,45)}}</a>
                                            </h5>
                                            <div class="info">
                                                <span class="designation"><a href="#"><i data-feather="check-square"></i>{{
                                                date('d/m/Y H:i:s', strtotime($row->created_at))}}</a></span>
                                            </div>
                                        </div>
                                    </td>

                                    <td class="status text-center">
                                        @if($row->status == RECRUITMENT_INTERVIEW)<p class="text-primary">Doanh nghiệp đã lên lịch</br> phỏng vấn với bạn</p> @endif
                                    </td>
                                    <td class="status text-center">
                                        <a class="text-info pointer" onclick="getCompany('{{$row->recruitmentPost->company_id}}','{{$row->id}}')" data-toggle="modal" data-target="#modal-experience"><i data-feather="eye"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-list text-center">
                            <nav class="navigation pagination">
                                <div class="nav-links">
                                    @if ($interviews->lastPage() > 1)
                                    <a class="prev page-numbers" href="{{ $interviews->url(1) }}"><i class="fas fa-angle-left"></i></a>
                                    @for ($i = 1; $i <= $interviews->lastPage(); $i++)
                                        <?php
                                        $half_total_links = floor(5 / 2);
                                        $from = $interviews->currentPage() - $half_total_links;
                                        $to = $interviews->currentPage() + $half_total_links;
                                        if ($interviews->currentPage() < $half_total_links) {
                                            $to += $half_total_links - $interviews->currentPage();
                                        }
                                        if ($interviews->lastPage() - $interviews->currentPage() < $half_total_links) {
                                            $from -= $half_total_links - ($interviews->lastPage() - $interviews->currentPage()) - 1;
                                        }
                                        ?>
                                        @if ($from < $i && $i < $to) @if ($interviews->currentPage() == $i)
                                            <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                            @else
                                            <a class="page-numbers" href="{{ $interviews->url($i) }}">{{ $i }}</a>
                                            @endif


                                            @endif
                                            @endfor

                                            <a class="next page-numbers" href="{{ $interviews->url($interviews->lastPage()) }}"><i class="fas fa-angle-right"></i></a>
                                            @endif
                                </div>
                            </nav>
                        </div>
                    </div>
                    @endif
                    @if(count($interviews) == 0)
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="mt-5 text-center text-dark">
                                ( Chưa có dữ liệu !! )
                            </p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="tab-pane fade {{Request::has('cancels') ? 'show active' : ''}}" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="dashboard-apply-area">
                    @if(!empty($cancels))
                    <div class="manage-candidate-container mt-5 " style="overflow-x:hidden">
                        <table class="table">
                            <tbody>
                                @foreach($cancels as $row)
                                <tr class="row">
                                    @if($row->status == RECRUITMENT_APPROVED)
                                        <td class="title">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="{{$row->recruitmentPost->company->getAvatar()}}" onerror="this.src='assets/clients/img/job/company-logo-1.png'" class="w-100" alt="">
                                                </a>
                                            </div>
                                            <div class="body">
                                                <h5>
                                                    <a href="{{route('recruitment',$row->post->slug)}}">{{Str::limit($row->post->title,45)}}</a>
                                                </h5>
                                                <div class="info">
                                                    <span class="designation"><a href="#"><i data-feather="check-square"></i>{{
                                                            date('d/m/Y H:i:s', strtotime($row->created_at))}}</a></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="status text-center">
                                            <p class="text-success">Ứng tuyển thành công</p>
                                        </td>
                                    @elseif($row->status == RECRUITMENT_DENIED)

                                        <td class="title col-sm-9">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="{{$row->recruitmentPost->company->getAvatar()}}" onerror="this.src='assets/clients/img/job/company-logo-1.png'" class="w-100" alt="">
                                                </a>
                                            </div>
                                            <div class="body">
                                                <h5>
                                                    <a href="{{route('recruitment',$row->post->slug)}}">{{Str::limit($row->post->title,45)}}</a>
                                                </h5>
                                                <div class="info">
                                                    <p><span class="text-danger">Lý do</span>: {{$row->reason}}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="status text-center col-sm-3">
                                            <p class="text-danger">Ứng tuyển thất bại</p>
                                        </td>

                                    @elseif($row->status == RECRUITMENT_CANCEL)

                                        <td class="title col-sm-9">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="{{$row->recruitmentPost->company->getAvatar()}}" onerror="this.src='assets/clients/img/job/company-logo-1.png'" class="w-100" alt="">
                                                </a>
                                            </div>
                                            <div class="body">
                                                <h5>
                                                    <a href="{{route('recruitment',$row->post->slug)}}">{{Str::limit($row->post->title,45)}}</a>
                                                </h5>
                                                <div class="info">
                                                    <p><span class="text-danger">Lý do</span>: {{$row->reason}}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="status text-center col-sm-3">
                                            <p class="text-warning">Huỷ phỏng vấn | </br> Từ chối ứng tuyển</p>
                                        </td>

                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="pagination-list text-center">
                            <nav class="navigation pagination">
                                <div class="nav-links">
                                    @if ($cancels->lastPage() > 1)
                                    <a class="prev page-numbers" href="{{ $cancels->url(1) }}"><i class="fas fa-angle-left"></i></a>
                                    @for ($i = 1; $i <= $cancels->lastPage(); $i++)
                                        <?php
                                        $half_total_links = floor(5 / 2);
                                        $from = $cancels->currentPage() - $half_total_links;
                                        $to = $cancels->currentPage() + $half_total_links;
                                        if ($cancels->currentPage() < $half_total_links) {
                                            $to += $half_total_links - $cancels->currentPage();
                                        }
                                        if ($cancels->lastPage() - $cancels->currentPage() < $half_total_links) {
                                            $from -= $half_total_links - ($cancels->lastPage() - $cancels->currentPage()) - 1;
                                        }
                                        ?>
                                        @if ($from < $i && $i < $to) @if ($cancels->currentPage() == $i)
                                            <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                            @else
                                            <a class="page-numbers" href="{{ $cancels->url($i) }}">{{ $i }}</a>
                                            @endif


                                            @endif
                                            @endfor

                                            <a class="next page-numbers" href="{{ $cancels->url($cancels->lastPage()) }}"><i class="fas fa-angle-right"></i></a>
                                            @endif
                                </div>
                            </nav>
                        </div>
                    </div>
                    @endif
                    @if(count($cancels) == 0)
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="mt-5 text-center text-dark">
                                ( Chưa có dữ liệu !! )
                            </p>
                        </div>
                    </div>
                    @endif

                </div>

            </div>
            <div class="tab-pane fade {{Request::has('waitings') ? 'show active' : ''}}" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                <div class="dashboard-apply-area">
                    @if(!empty($waitings))
                    <div class="manage-candidate-container mt-5 " style="overflow-x:hidden">

                        <table class="table">

                            <tbody>
                                @foreach($waitings as $row)
                                <tr>
                                    <td class="title">
                                        <div class="thumb">
                                            <a href="{{route('recruitment',$row->post->slug)}}">
                                                <img src="{{$row->recruitmentPost->company->getAvatar()}}" onerror="this.src='assets/clients/img/job/company-logo-1.png'" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="body">
                                            <h5>
                                                <a href="{{route('recruitment',$row->post->slug)}}">{{Str::limit($row->post->title,45)}}</a>
                                            </h5>
                                            <div class="info">
                                                <span class="designation"><a href="#"><i data-feather="check-square"></i>{{
                                                    date('d/m/Y H:i:s', strtotime($row->created_at))}}</a></span>
                                            </div>
                                        </div>
                                    </td>

                                    <td class="status text-center">
                                        <p class="text-primary">Đợi doanh nghiệp </br> xác nhận</p>
                                    </td>
                                    <td class="status text-center">
                                        <button type="button" class="btn btn-danger" onclick="deleted('{{$row->id}}')"><i class="fas fa-times-circle"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-list text-center">
                            <nav class="navigation pagination">
                                <div class="nav-links">
                                    @if ($waitings->lastPage() > 1)
                                    <a class="prev page-numbers" href="{{ $waitings->url(1) }}"><i class="fas fa-angle-left"></i></a>
                                    @for ($i = 1; $i <= $waitings->lastPage(); $i++)
                                        <?php
                                        $half_total_links = floor(5 / 2);
                                        $from = $waitings->currentPage() - $half_total_links;
                                        $to = $waitings->currentPage() + $half_total_links;
                                        if ($waitings->currentPage() < $half_total_links) {
                                            $to += $half_total_links - $waitings->currentPage();
                                        }
                                        if ($waitings->lastPage() - $waitings->currentPage() < $half_total_links) {
                                            $from -= $half_total_links - ($waitings->lastPage() - $waitings->currentPage()) - 1;
                                        }
                                        ?>
                                        @if ($from < $i && $i < $to) @if ($waitings->currentPage() == $i)
                                            <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                            @else
                                            <a class="page-numbers" href="{{ $waitings->url($i) }}">{{ $i }}</a>
                                            @endif


                                            @endif
                                            @endfor

                                            <a class="next page-numbers" href="{{ $waitings->url($waitings->lastPage()) }}"><i class="fas fa-angle-right"></i></a>
                                            @endif
                                </div>
                            </nav>
                        </div>

                    </div>
                    @endif
                    @if(count($waitings) == 0)
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="mt-5 text-center text-dark">
                                ( Chưa có dữ liệu !! )
                            </p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
</div>
<!-- Modal Xem lịch phỏng vấn -->
<div class="experience dashboard-section details-section">
    <div class="modal fade modal-experience" id="modal-experience" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="title">
                        <h4><i data-feather="briefcase"></i>Lịch phỏng vấn</h4>
                    </div>
                    <div class="content">
                        <form action="#">
                            <div class="input-block-wrap">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Thông tin doanh nghiệp</label>
                                    <div class="col-sm-9">
                                        <div class="input-group row">
                                            <div class="input-group-prepend col-sm-4">
                                                <div class="input-group-text">Tên doanh nghiệp : </div>
                                            </div>
                                            <input type="text" class="form-control col-sm-8" id="name" value="" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-3 col-sm-9">
                                        <div class="input-group row">
                                            <div class="input-group-prepend col-sm-4">
                                                <div class="input-group-text">Số điện thoại : </div>
                                            </div>
                                            <input type="text" class="form-control col-sm-8" id="phone" value="" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-9 offset-sm-3">
                                        <div class="input-group row">
                                            <div class="input-group-prepend col-sm-4">
                                                <div class="input-group-text">Email : </div>
                                            </div>
                                            <input type="text" class="form-control col-sm-8" id="email" value="" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-9 offset-sm-3">
                                        <div class="input-group row">
                                            <div class="input-group-prepend col-sm-4">
                                                <div class="input-group-text">Địa chỉ : </div>
                                            </div>
                                            <input type="text" class="form-control col-sm-8" id="address" value="" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input-block-wrap">
                                <!-- ZOOM -->
                                <div id="zoom"></div>
                                <!-- GG MEET -->
                                <div id="meet"></div>
                                <!-- Trực tiếp -->
                                <div id="location"></div>
                            </div>

                            <div class="row">
                                <div class="text-left col-sm-12" style="font-size: 15px; margin-top: 8px; border: 1px dashed #ccc; padding: 5px 10px;">
                                    Lưu ý: <br>
                                    1. Sinh viên muốn huỷ cuộc phỏng vấn này , vui lòng bấm <a type="button" onclick="cancelInterview(this)" id="cancelInterviews"><strong class="text-info" style="font-weight: bold;">tại đây.</strong></a><br>
                                    2. Nếu sinh viên huỷ cuộc phỏng vấn này thì sẽ không thể ứng tuyển lại được nữa . Sinh viên nên cân nhắc điều này !
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal hiện cảnh báo chấp nhận việc làm -->
<div class="experience dashboard-section details-section">
    <div class="modal fade modal-accept" id="modal-accept" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="title">
                        <h4><i data-feather="briefcase"></i>Những lưu ý khi sinh viên chấp nhận việc làm</h4>
                    </div>
                    <div class="content">
                        <form action="#">
                            <div class="input-block-wrap">
                                <div class="row">
                                    <div id="warning">

                                    </div>

                                    <div class="text-left col-sm-12" style="font-size: 15px; margin-top: 8px; border: 1px dashed #ccc; padding: 5px 10px;">
                                        Lưu ý: <br>
                                        1. Sau khi sinh viên nhấn nút <span class="text-primary">đồng ý</span> , thì tất cả các bài ứng tuyển mà sinh viên đã ứng tuyển sẽ bị xoá !<br>
                                        2. Nếu sinh viên từ chối ứng tuyển này thì sẽ không thể ứng tuyển lại được nữa . Từ chối ứng tuyển <a type="button" class="getPendingApproved" onclick="cancelPendingApproved(this)" id=""><strong class="text-info" style="font-weight: bold;">tại đây.</strong></a><br>
                                        3. Sinh viên vui lòng suy nghĩ thật kỹ khi đưa ra quyết định . Sinh viên nên cân nhắc điều này !
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="text-left col-sm-12" style="font-size: 15px; margin-top: 8px; border: 1px dashed #ccc; padding: 5px 10px;">
                                        <div id="getCompany"></div>

                                    </div>
                                </div>

                                <div class="row mt-5">
                                    <div class="offset-sm-3 col-sm-9">
                                        <div class="buttons">
                                            <button class="primary-bg getPendingApproved" type="button" onclick="accuracy(this)" id="">Đồng ý</button>
                                            <button class="" data-dismiss="modal">Đóng</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    /* Load data Modal */
    function getCompany(companyId, recruitmentDetailId) {
        $.ajax({
            type: "POST",
            url: "{{ route('ajax.recruitment-detail') }}",
            data: {
                _token: '{{ csrf_token() }}',
                companyId: companyId,
                recruitmentDetailId: recruitmentDetailId,

            },
            dataType: "json",
            success: function(data) {
                $('#name').val(data['company']['name']);
                $('#phone').val(data['company']['phone']);
                $('#email').val(data['company']['email']);
                $('#address').val(data['company']['address']);
                if (data['recruitmentPostDetail']['interview']['online'] == false) {
                    var location = document.getElementById('location');
                    location.innerHTML = `
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Thông tin cuộc phỏng vấn</label>
                                    <div class="col-sm-9">
                                        <div class="input-group row">
                                            <div class="input-group-prepend col-sm-4">
                                                <div class="input-group-text">Địa điểm : </div>
                                            </div>
                                            <input type="text" class="form-control col-sm-8" id="location" value="${data['recruitmentPostDetail']['interview']['location']}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-9 offset-sm-3">
                                        <div class="input-group row">
                                            <div class="input-group-prepend col-sm-4">
                                                <div class="input-group-text">Thời gian bắt đầu : </div>
                                            </div>
                                            <input type="text" class="form-control col-sm-8" id="date_start" value="${data['recruitmentPostDetail']['interview_start']}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-9 offset-sm-3">
                                        <div class="input-group row">
                                            <div class="input-group-prepend col-sm-4">
                                                <div class="input-group-text">Thời gian kết thúc : </div>
                                            </div>
                                            <input type="text" class="form-control col-sm-8" id="date_end" value="${data['recruitmentPostDetail']['interview_end']}" readonly>
                                        </div>
                                    </div>
                                </div>

                    `;
                }
                if (data['recruitmentPostDetail']['interview']['online'] == true && data['recruitmentPostDetail']['interview']['app'] == 'zoom') {
                    var zoom = document.getElementById('zoom');
                    zoom.innerHTML = `
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Thông tin cuộc phỏng vấn</label>
                            <div class="col-sm-9">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Loại ứng dụng : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="app"  value="${data['recruitmentPostDetail']['interview']['app']}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Phòng : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="room"  value="${data['recruitmentPostDetail']['interview']['room']}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9 offset-sm-3">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Mật khẩu : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="password"  value="${data['recruitmentPostDetail']['interview']['password']}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9 offset-sm-3">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Thời gian bắt đầu : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="date_start" value="${data['recruitmentPostDetail']['interview_start']}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9 offset-sm-3">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Thời gian kết thúc : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="date_end" value="${data['recruitmentPostDetail']['interview_end']}" readonly>
                                </div>
                            </div>
                        </div>
                        `;

                }
                if (data['recruitmentPostDetail']['interview']['online'] == true && data['recruitmentPostDetail']['interview']['app'] == 'meet') {
                    var meet = document.getElementById('meet');
                    meet.innerHTML = `
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Thông tin cuộc phỏng vấn</label>
                            <div class="col-sm-9">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Loại ứng dụng : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="app" value="${data['recruitmentPostDetail']['interview']['app']}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9 offset-sm-3">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Đường dẫn : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="url"  value="${data['recruitmentPostDetail']['interview']['url']}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9 offset-sm-3">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Thời gian bắt đầu : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="date_start" value="${data['recruitmentPostDetail']['interview_start']}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9 offset-sm-3">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Thời gian kết thúc : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="date_end" value="${data['recruitmentPostDetail']['interview_end']}" readonly>
                                </div>
                            </div>
                        </div>
                    `;
                }
                $('#app').val(data['recruitmentPostDetail']['interview']['app']);
                $('#room').val(data['recruitmentPostDetail']['interview']['room']);
                $('#url').val(data['recruitmentPostDetail']['interview']['url']);
                $('#password').val(data['recruitmentPostDetail']['interview']['password']);
                $('#location').val(data['recruitmentPostDetail']['interview']['location']);
                $('#date_start').val(data['recruitmentPostDetail']['interview_start']);
                $('#date_end').val(data['recruitmentPostDetail']['interview_end']);
                $('#cancelInterviews').attr("recruitmentId", data['recruitmentPostDetail']['id']);
            },
            error: function(data) {
                swal({
                    title: "Hệ thống đã xảy ra lỗi !",
                    text: "Vui lòng quay lại sau !",
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-warning",
                    type: "warning",
                    confirmButtonText: 'Đồng ý !',
                }).catch(swal.noop)
            }
        });
    }

    function getPending(companyId, postId, recruitmentDetailId) {
        $.ajax({
            type: "POST",
            url: "{{ route('ajax.recruitment-pending') }}",
            data: {
                _token: '{{ csrf_token() }}',
                companyId: companyId,
                postId: postId,
                recruitmentDetailId: recruitmentDetailId,
            },
            dataType: "json",
            success: function(data) {
                $('#name').val(data['company']['name']);
                $('#phone').val(data['company']['phone']);
                $('#email').val(data['company']['email']);
                $('#address').val(data['company']['address']);
                $('#namePost').val(data['post']['title']);
                console.log(data['recruitmentPostDetail']['id']);
                $('.getPendingApproved').attr("recruitmentIds", data['recruitmentPostDetail']['id']);
                if (!data['checkTimeLine']) {
                    document.getElementById('warning').innerHTML = `<div class="text-left text-danger col-sm-12" style="font-size: 15px; margin-top: 8px; border: 1px dashed #ccc; padding: 5px 10px;">
                                        Cảnh báo: <br>
                                        Hiện tại bạn đã trễ hạn thực tập theo kế hoạch của nhà trường.<br>
                                        Nếu bạn đồng ý thực tập vào <b>${data['company']['name']}</b> báo cáo thực tập của bạn sẽ được nộp vào kỳ sau.
                                        Sinh viên vui lòng suy nghỉ kỹ trước khi xác nhận vào doanh nghiệp.
                                    </div>`;
                }
                if (data['company']['name']) {
                    var name = document.getElementById('getCompany');
                    name.innerHTML = `
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Thông tin doanh nghiệp</label>
                            <div class="col-sm-9">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Tên doanh nghiệp : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="name" value="${data['company']['name']}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9 offset-sm-3">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Địa chỉ : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="address" value="${data['company']['address']}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                                    <div class="offset-sm-3 col-sm-9">
                                        <div class="input-group row">
                                            <div class="input-group-prepend col-sm-4">
                                                <div class="input-group-text">Số điện thoại : </div>
                                            </div>
                                            <input type="text" class="form-control col-sm-8" id="phone" value="${data['company']['phone']}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-9 offset-sm-3">
                                        <div class="input-group row">
                                            <div class="input-group-prepend col-sm-4">
                                                <div class="input-group-text">Email : </div>
                                            </div>
                                            <input type="text" class="form-control col-sm-8" id="email" value="${data['company']['email']}" readonly>
                                        </div>
                                    </div>
                                </div>
                        <div class="form-group row">
                            <div class="col-sm-9 offset-sm-3">
                                <div class="input-group row">
                                    <div class="input-group-prepend col-sm-4">
                                        <div class="input-group-text">Tên bài viết : </div>
                                    </div>
                                    <input type="text" class="form-control col-sm-8" id="namePost" value="${data['post']['title']}" readonly>
                                </div>
                            </div>
                        </div>
                    `;
                }
            },
            error: function(data) {
                swal({
                    title: "Hệ thống đã xảy ra lỗi !",
                    text: "Vui lòng quay lại sau !",
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-warning",
                    type: "warning",
                    confirmButtonText: 'Đồng ý !',
                }).catch(swal.noop)
            }
        });
    }
    /* Update status From PEDDING TO APPROVED */
    function accuracy(recruitmentId) {
        let id = recruitmentId.getAttribute('recruitmentIds');
        let status = "{{RECRUITMENT_APPROVED}}";
        let data = {
            'status': status,
            'id': id
        };
        $('#modal-accept').modal('hide');
        getLoading();
        console.log(data);
        $.ajax({
            type: "PUT",
            url: "{{route('ajax.recruitment.status')}}",
            data: {
                _token: '{{ csrf_token() }}',
                data: data,
            },
            dataType: "json",

            success: function(data) {
                getLoading();
                swal({
                        title: "Chúc mừng bạn đã có nơi thực tập",
                        text: "Bạn đã được nhận vào doanh nghiệp !",
                        buttonsStyling: false,
                        timer: 10000,
                        confirmButtonClass: "btn btn-success",
                        type: "success",
                        confirmButtonText: 'Đồng ý !',

                    }).then(() => {
                        Swal.fire({
                            title: 'Bạn có muốn làm <br> báo cáo thực tập luôn không ?',
                            type: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Có',
                            cancelButtonText: 'Không',
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = "{{route('student.report')}}";
                            } else {
                                window.location.reload();
                            }
                        })
                    })
                    .catch(swal.noop);
            },
            error: function(data) {
                console.log(data);
                swal({
                    title: "Hệ thống đã xảy ra lỗi !",
                    text: "Vui lòng quay lại sau !",
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-warning",
                    type: "warning",
                    confirmButtonText: 'Đồng ý !',
                }).catch(swal.noop)
            }
        });

    }

    function statusInterview(id) {
        let status = '{{RECRUITMENT_INTERVIEW_WAITING}}';
        let data = {
            'status': status,
            'id': id
        };
        getLoading();
        $.ajax({
            type: "PUT",
            url: "{{route('ajax.recruitment.status')}}",
            data: {
                _token: '{{ csrf_token() }}',
                data: data,
            },
            dataType: "json",

            success: function(data) {
                getLoading();
                swal({
                        title: "Đã chấp nhận thành công !",
                        text: "Doanh nghiệp sẽ lên lịch phỏng vấn sớm nhất !",
                        buttonsStyling: false,
                        timer: 10000,
                        confirmButtonClass: "btn btn-success",
                        type: "success",
                        confirmButtonText: 'Đồng ý !',

                    }).then(() => window.location.reload())
                    .catch(swal.noop);
            },
            error: function(data) {

                swal({
                    title: "Hệ thống đã xảy ra lỗi !",
                    text: "Vui lòng quay lại sau !",
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-warning",
                    type: "warning",
                    confirmButtonText: 'Đồng ý !',
                }).catch(swal.noop)
            }
        });

    }

    function statusInterviewSuccess(id) {
        let status = '{{RECRUITMENT_APPROVED}}';
        let data = {
            'status': status,
            'id': id
        };
        Swal.fire({
            title: 'Bạn có muốn chấp nhận </br > để vào doanh nghiệp ?',
            text: "Bạn nên suy nghĩ thật kỷ trước khi đưa ra lựa chọn !",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Chấp nhận!',
            cancelButtonText: 'Đóng',
        }).then((result) => {
            if (result.value) {
                getLoading();
                $.ajax({
                    type: "PUT",
                    url: "{{route('ajax.recruitment.status')}}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        data: data,
                    },
                    dataType: "json",

                    success: function(data) {
                        getLoading();
                        swal({
                                title: "Đã chấp nhận thành công !",
                                text: "Chúc bạn thực tập tốt và học nhiều điều mới !",
                                buttonsStyling: false,
                                timer: 10000,
                                confirmButtonClass: "btn btn-success",
                                type: "success",
                                confirmButtonText: 'Đồng ý !',

                            }).then(() => {
                                Swal.fire({
                                    title: 'Bạn có muốn làm <br> báo cáo thực tập luôn không ?',
                                    type: 'question',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Có',
                                    cancelButtonText: 'Không',
                                }).then((result) => {
                                    if (result.value) {
                                        window.location.href = "{{route('student.report')}}";
                                    } else {
                                        window.location.reload();
                                    }
                                })
                            })
                            .catch(swal.noop);
                    },
                    error: function(data) {

                        swal({
                            title: "Hệ thống đã xảy ra lỗi !",
                            text: "Vui lòng quay lại sau !",
                            buttonsStyling: false,
                            confirmButtonClass: "btn btn-warning",
                            type: "warning",
                            confirmButtonText: 'Đồng ý !',
                        }).catch(swal.noop)
                    }
                });
            }
        })


    }
    /* DATELE DATA RECRUITMENT */
    function deleted(id) {
        Swal.fire({
        title: "Bạn có muốn xóa ?",
        text: "Bạn sẽ không thể hoàn tác điều này!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Đồng ý",
        cancelButtonText: "Hủy",
        confirmButtonClass: "btn btn-primary mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
        }).then((data) => {
            if (data.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{ route('student.applied-job.destroy') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: id,
                    },
                    dataType: "json",
                    success: function(data) {
                        getLoading();
                        Swal.fire(
                            'Huỷ thành công!',
                            'Bạn đã huỷ ứng tuyển!',
                            'success'
                        ).then(() => window.location.reload())
                    },

                })
            }
        });
    }
    /* CANCEL DATA RECRUITEMNT */
    function cancelInterview(abc) {
        var id = abc.getAttribute('recruitmentId');
        var status = '{{ RECRUITMENT_CANCEL }}';
        $('#modal-experience').modal('hide');
        Swal.fire({
            title: "Lý do để bạn huỷ phỏng vấn là gì ?",
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Đồng ý',
            cancelButtonText: 'Đóng',
            showLoaderOnConfirm: true,
            preConfirm: (reason) => {
                return reason;
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((reason) => {
            if (reason.value === "" || reason.value) {
                getLoading();
                $.ajax({
                    type: "PUT",
                    url: "{{ route('ajax.recruitment.status') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        data: {
                            id: id,
                            status: status,
                            reason: reason.value,
                        },
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#modal-experience').modal('hide');
                        getLoading();
                        if (data['error']) {
                            swal({
                                title: "Vui lòng nhập lý do !",
                                text: "Bạn phải nhập lý do để được huỷ phỏng vấn !",
                                buttonsStyling: false,
                                confirmButtonClass: "btn btn-warning",
                                type: "warning",
                                confirmButtonText: 'Đồng ý !',
                            }).catch(swal.noop)
                        } else {
                            swal({
                                    title: "Đã huỷ lịch phỏng vấn !",
                                    text: "Chúc bạn may mắn trong quá trình tìm việc !",
                                    buttonsStyling: false,
                                    confirmButtonClass: "btn btn-success",
                                    type: "success",
                                    confirmButtonText: 'Đồng ý !',
                                }).then(() => window.location.reload())
                                .catch(swal.noop)
                        }
                    },
                    error: function(data) {
                        swal({
                            title: "Hệ thống đã xảy ra lỗi !",
                            text: "Vui lòng quay lại sau !",
                            buttonsStyling: false,
                            confirmButtonClass: "btn btn-warning",
                            type: "warning",
                            confirmButtonText: 'Đồng ý !',
                        }).catch(swal.noop)

                    }
                });
            }

        });

    }
    function cancelPendingApproved(abc) {
        var id = abc.getAttribute('recruitmentIds');
        var status = '{{ RECRUITMENT_CANCEL }}';
        $('#modal-accept').modal('hide');
        Swal.fire({
            title: "Lý do để bạn từ chối ứng tuyển </br > là gì ?",
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Đồng ý',
            cancelButtonText: 'Đóng',
            showLoaderOnConfirm: true,
            preConfirm: (reason) => {
                return reason;
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((reason) => {
            if (reason.value === "" || reason.value) {
                getLoading();
                $.ajax({
                    type: "PUT",
                    url: "{{ route('ajax.recruitment.status') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        data: {
                            id: id,
                            status: status,
                            reason: reason.value,
                        },
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#modal-accept').modal('hide');
                        getLoading();
                        if (data['error']) {
                            swal({
                                title: "Vui lòng nhập lý do !",
                                text: "Bạn phải nhập lý do để từ chối ứng tuyển !",
                                buttonsStyling: false,
                                confirmButtonClass: "btn btn-warning",
                                type: "warning",
                                confirmButtonText: 'Đồng ý !',
                            }).catch(swal.noop)
                        } else {
                            swal({
                                    title: "Đã từ chối ứng tuyển !",
                                    text: "Chúc bạn may mắn trong quá trình tìm việc !",
                                    buttonsStyling: false,
                                    confirmButtonClass: "btn btn-success",
                                    type: "success",
                                    confirmButtonText: 'Đồng ý !',
                                }).then(() => window.location.reload())
                                .catch(swal.noop)
                        }
                    },
                    error: function(data) {
                        swal({
                            title: "Hệ thống đã xảy ra lỗi !",
                            text: "Vui lòng quay lại sau !",
                            buttonsStyling: false,
                            confirmButtonClass: "btn btn-warning",
                            type: "warning",
                            confirmButtonText: 'Đồng ý !',
                        }).catch(swal.noop)

                    }
                });
            }

        });

    }
</script>

@endsection
