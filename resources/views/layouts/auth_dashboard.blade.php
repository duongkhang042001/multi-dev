@include('layouts.dashboard.auth_header')

@yield('content')

@include('layouts.dashboard.auth_footer')
