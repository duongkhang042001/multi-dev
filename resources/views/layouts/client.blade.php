@include('layouts.client.header')
<div class="alice-bg padding-top-70 padding-bottom-70">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="breadcrumb-area">
                    <h1>@yield('title')</h1>
                    @yield('breadcrumb')
                </div>
            </div>
            <div class="col-md-6">
                <div class="breadcrumb-area">
                    <a href="{{ route('home') }}" class="text-primary float-right d-flex align-items-center"><i
                            data-feather="arrow-left"></i> <span> Quay về trang chủ</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->
<div class="alice-bg section-padding-bottom">
    <div class="container no-gliters">
        <div class="row no-gliters">
            <div class="col">
                <div class="dashboard-container">
                    @yield('content')
                    @include('layouts.client.sidebar')
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.client.footer')
