<!-- Call to Action -->
<div class="call-to-action-bg padding-top-90 padding-bottom-90">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="call-to-action-2">
                    <div class="call-to-action-content">
                        <h2>FPT INTERNSHIP</h2>
                        <p>Với niềm tin càng giàu trải nghiệm càng thành công, Trường học trải nghiệm FPT INTERNSHIP
                            đồng hành cùng người học trên con đường tự kiến tạo tương lai của chính mình.</p>
                    </div>
                    <div class="call-to-action-button">
                        <a href="#" class="button">HOTLINE: 1800.6600</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Call to Action End -->
<!-- Footer -->
<footer class="footer-bg">
    <div class="footer-top border-bottom section-padding-top padding-bottom-40">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="footer-logo">
                        <a href="#">
                            <img src="/assets/clients/img/logo-2.png" class="w-50" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="footer-social">
                        <ul class="social-icons">
                            <li><a href="#"><i data-feather="facebook"></i></a></li>
                            <li><a href="#"><i data-feather="twitter"></i></a></li>
                            <li><a href="#"><i data-feather="linkedin"></i></a></li>
                            <li><a href="#"><i data-feather="instagram"></i></a></li>
                            <li><a href="#"><i data-feather="youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-widget-wrapper padding-bottom-60 padding-top-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <div class="footer-widget widget-about">
                        <h4>Fpt Internship</h4>
                        <div class="widget-inner">
                            <p class="description"> Hệ thống quản lý thực tập dành cho sinh viên trường Cao đẳng FPT
                                Polytechnic và các doanh nghiệp tìm kiếm tài năng của trường</p>
                            <span class="about-contact"><i data-feather="phone-forwarded"></i>+8 246-345-0698</span>
                            <span class="about-contact"><i data-feather="mail"></i>internshipfpt2021@gmail.com</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1 col-sm-6">
                    <div class="footer-widget footer-shortcut-link">
                        <h4>Thông tin</h4>
                        <div class="widget-inner">
                            <ul>
                                <li><a href="{{ route('about') }}">Giới thiệu</a></li>
                                <li><a href="{{ route('contact') }}">Liên hệ với chúng tôi</a></li>
                                <li><a href="#">Chính sách bảo mật</a></li>
                                <li><a href="#">Điều khoản và điều kiện</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="footer-widget footer-shortcut-link">
                        <h4>Về sinh viên</h4>
                        <div class="widget-inner">
                            <ul>
                                <li><a href="#">Hướng dẫn tạo tài khoản</a></li>
                                <li><a href="#">Hoạt động báo cáo</a></li>
                                <li><a href="#">Tìm kiếm doanh nghiệp</a></li>
                                <li><a href="#">Câu hỏi thường gặp</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="footer-widget footer-shortcut-link">
                        <h4>Về Doanh Nghiệp</h4>
                        <div class="widget-inner">
                            <ul>
                                <li><a href="#">Hướng dẫn tạo tài khoản</a></li>
                                <li><a href="#">Dịch vụ</a></li>
                                <li><a href="#">Viết bài đăng tuyển</a></li>
                                <li><a href="#">Câu hỏi thường gặp</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="footer-bottom border-top">
                        <div class="row">
                            <div class="col-xl-4 col-lg-5 order-lg-2">
                                <div class="footer-app-download">
                                    <a href="#" class="apple-app">Apple Store</a>
                                    <a href="#" class="android-app">Google Play</a>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 order-lg-1">
                                <p class="copyright-text">Copyright &copy; 2021, Multi-Thinking</p>
                            </div>
                            <div class="col-xl-4 col-lg-3 order-lg-3">
                                <div class="back-to-top">
                                    <a href="#">ĐẦU TRANG<i class="fas fa-angle-up"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->
<div id="fullDiv" class="">
    <div id="fullDiv_spinner">
        <div class="double-loading">
            <div class="c1"></div>
            <div class="c2"></div>
        </div>
    </div>
</div>
<div class="about-details details-section dashboard-section">
    <div class="modal fade" id="find-jobs" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="title">
                        <h4>Hướng dẫn tìm kiếm nơi thực tập</h4>
                    </div>
                    <div class="row content">
                        <div class="col-sm-6" style="border: 1px dashed #ccc;">
                            <h4 class="my-3">Đối với sinh viên thực chưa có nơi thực tập</h4>
                            <p>
                                1. Sinh viên bổ sung đầy đủ thông tin của báo cáo thực tập. <br>
                                2. Hạn nộp trước ngày kết thúc thực tập của kỳ. <br>
                                3. Sinh viên cân nhắc hủy thực tập hoặc thay đổi nơi thực tập, nếu thay đổi trong quá
                                trình thực tập sẽ bị đánh rớt và báo cáo thực tập sẽ được bổ sung vào kỳ sau.
                            </p>
                            <div class="buttons mt-5 mb-4">
                                <a href="{{ route('recruitment.list')}}" class="btn btn-primary p-3 text-white" style="font-size: 14px">Tìm kiếm doanh nghiệp thực tập</a>
                            </div>
                        </div>
                        <div class="col-sm-6" style="border: 1px dashed #ccc; border-left: 0px">
                            <h4 class="my-3">Sinh viên đã có nơi thực tập</h4>
                            <p>
                                1. Sinh viên tự tìm nơi thực tập bên ngoài hệ thống, vui lòng đăng ký doanh nghiệp và
                                thực tập. <br>
                                2. Sinh viên bổ sung thông tin và báo cáo ngày trên hệ thống, và tải lên file đánh giá
                                của doanh nghiệp. <br>
                                3. Sinh viên lưu ý thời hạn đăng ký và nộp báo cáo thực tập! <br>
                            </p>
                            <div class="buttons mt-5 mb-4">
                            <a href="{{ route('student.company.create') }}" class="btn btn-primary p-3 text-white" style="font-size: 14px">Đăng ký doanh nghiệp thực tập</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/assets/clients/js/jquery.min.js"></script>
<script src="/assets/clients/js/popper.min.js"></script>
<script src="/assets/clients/js/bootstrap.min.js"></script>
<script src="/assets/clients/js/feather.min.js"></script>
<script src="/assets/clients/js/bootstrap-select.min.js"></script>
<script src="/assets/clients/js/jquery.nstSlider.min.js"></script>
<script src="/assets/clients/js/owl.carousel.min.js"></script>
<script src="/assets/clients/js/visible.js"></script>
<script src="/assets/clients/js/jquery.countTo.js"></script>
<script src="/assets/clients/js/chart.js"></script>
<script src="/assets/clients/js/plyr.js"></script>
<script src="/assets/clients/js/tinymce.min.js"></script>
<script src="/assets/clients/js/slick.min.js"></script>
<script src="/assets/clients/js/jquery.ajaxchimp.min.js"></script>
<script src="/assets/libs/jquery-toast/jquery.toast.min.js"></script>
<script src="/assets/clients/js/custom.js"></script>
<script src="/assets/clients/js/dashboard.js"></script>
<script src="/assets/clients/js/datePicker.js"></script>
<script src="/assets/clients/js/upload-input.js"></script>
<!-- Sweetalert-->
<script src="/assets/js/plugins/sweetalert2.js"></script>
<script src="/assets/libs/moment/moment-local.min.js"></script>
<script src="/assets/libs/moment/moment.min.js"></script>


@yield('scripts')
<livewire:scripts />
<script src="/assets/js/custom.js"></script>

@if (Session::has('fail'))
    <script>
        var msg = '{{ Session::get('fail') }}';
        callNotify('error', msg);
    </script>
@endif
@if (Session::has('success'))
    <script>
        var msg = '{{ Session::get('success') }}';
        callNotify('success', msg);
    </script>
@endif
@if (Session::has('status'))
    <script>
        var msg = '{{ Session::get('success') }}';
        callNotify('success', msg);
    </script>
@endif
@if (Session::has('notify'))
    <script>
        var msg = '{{ Session::get('notify') }}';
        callNotify('info', msg);
    </script>
@endif
@if (Session::has('warning') || Session::has('errors'))
    <script>
        var msg =
            '{{ Session::has('warning') ? Session::get('warning') : 'Vui lòng kiểm tra lại thông tin trước khi xác nhận!' }}';
        callNotify('warning', msg);
    </script>
@endif
@if (Auth::guard('company')->check() || Auth::guard('student')->check())
    <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
    <script type="text/javascript">
        var pusher = new Pusher('4f19ef6d3cd63794e41f', {
            encrypted: true,
            cluster: "ap1"
        });
    </script>
@endif
@if (Auth::guard('company')->check() || Auth::guard('student')->check())
    <script type="text/javascript">
        var channel = pusher.subscribe('all-user');
        channel.bind('all-user', function(data) {
            getNotify();
        });
    </script>
@endif
@if (Auth::guard('student')->check())
    <script type="text/javascript">
        var channel = pusher.subscribe('all-student');
        channel.bind('all-student', function(data) {
            getNotify();
        });

        var channel = pusher.subscribe('student-detail');
        var id = {{ Auth::guard('student')->user()->id }};
        channel.bind(`companyToStudent${id}`, function(data) {
            getNotify();
        });
        var channel = pusher.subscribe('student');
        channel.bind(`student${id}`, function(data) {
            getNotify();
        });
    </script>
@elseif(Auth::guard('company')->check())
    <script type="text/javascript">
        var channel = pusher.subscribe('all-company');
        channel.bind('all-company', function(data) {
            getNotify();
        });
        var channel = pusher.subscribe('company-detail');
        var id = {{ Auth::guard('company')->user()->company_id }};
        channel.bind(`studentToCompany${id}`, function(data) {
            getNotify();
        });
    </script>
@endif

<script>
    $(".notification-button").click(function() {
        $("#dotNotify").remove();
        var styleTag = document.getElementById('style-tag');
        if (!styleTag) return;
        document.getElementsByTagName('head')[0].removeChild(styleTag);
        $.ajax({
            url: '{{ route('ajax.watchedNotification') }}',
            type: 'get',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
            },
        });
    });
</script>

@if (Auth::guard('company')->check())
    <script type="text/javascript">
        var channel = pusher.subscribe('my-company');
    </script>
@endif

@if (Auth::guard('company')->check() || Auth::guard('student')->check())
    <script type="text/javascript">
        window.onload = function() {
            $(".notification-body").html(`Chưa có dữ liệu thông báo`);
            getNotify();
        }

        function getNotify(event = false) {
            $.ajax({
                url: '{{ route('ajax.notification') }}',
                type: 'get',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                },
                success: function(response) {
                    let count = 0;
                    $(".notification-body").html(``);
                    $("#mdi-record").html(``);
                    response.forEach(element => {
                        if (element['is_active'] == 0) count += 1;
                        var d = Date.parse(element['created_at'])
                        let timez = timeDifference(Date.now(), d);
                        var urlStudent = '{{ route('student.applied-job.index') }}';
                        var urlCompany = '{{ route('company.recruitment-manager.index') }}';
                        $(".notification-body").append(`<a href="${element['notify']['object']== 4  ? urlStudent : (element['notify']['object']== 5 ? urlCompany : `./notify-detail/${element['id']}`)}" class="${element['is_active']== 0 ? 'bg-light' : ''} notification-list">
                                <i class="fas fa-bolt"></i>
                                <p class="${element['is_active'] == 0 ? 'text-dark' : 'text-primary'}">${element['notify']['title']}</p>
                                <span class="time">${timez}</span>
                            </a>`);
                    });
                    if (count > 0) {
                        $("#mdi-record").append(`<i class="mdi mdi-record" id='dotNotify'></i>`);
                        var style = document.createElement('style');
                        style.id = 'style-tag';
                        style.innerHTML = `
                        .cp-nav
                            .navbar-collapse
                            .navbar-nav.account-nav
                            .header-top-notification
                            .notification-button:after {
                            position: absolute;
                            top: -14px;
                            right: -7px;
                            height: 4px;
                            width: 4px;
                            border-radius: 5px;
                            background: #ffffff;
                            content: "";
                        `;
                        document.head.appendChild(style);
                    }
                },

            });
        }
    </script>
@endif

@if (Auth::guard('company')->check() || Auth::guard('student')->check())
    <script type="text/javascript">
        function timeDifference(current, previous) {
            var msPerMinute = 60 * 1000;
            var msPerHour = msPerMinute * 60;
            var msPerDay = msPerHour * 24;
            var msPerMonth = msPerDay * 30;
            var msPerYear = msPerDay * 365;

            var elapsed = current - previous;

            if (elapsed < msPerMinute) {
                return Math.round(elapsed / 1000) + ' giây trước';
            } else if (elapsed < msPerHour) {
                return Math.round(elapsed / msPerMinute) + ' phút trước';
            } else if (elapsed < msPerDay) {
                return Math.round(elapsed / msPerHour) + ' giờ trước';
            } else if (elapsed < msPerMonth) {
                return Math.round(elapsed / msPerDay) + ' Ngày trước';
            } else if (elapsed < msPerYear) {
                return Math.round(elapsed / msPerMonth) + ' tháng trước';
            } else {
                return Math.round(elapsed / msPerYear) + ' năm trước';
            }
        }

        window.onload = function() {
            $(".notification-body").html(`Chưa có dữ liệu thông báo`);
            getNotify();
        };

        function getNotify(event = false) {
            $.ajax({
                url: '{{ route('ajax.notification') }}',
                type: 'get',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                },
                success: function(response) {
                    var count = 0;
                    $(".notification-body").html(``);
                    $("#mdi-record").html(``);
                    response.forEach(element => {
                        if (element['is_active'] == 0) count += 1;
                        var d = Date.parse(element['created_at'])
                        let timez = timeDifference(Date.now(), d);
                        $(".notification-body").append(`<a href="${element['notify']['url']}" class="${element['is_active']== 0 ? '' : ''} notification-list">
                                    <i class="fas fa-bolt"></i>
                                    <p class="${element['is_active']== 0 ? 'text-dark' : ''}">${element['notify']['title']}</p>
                                    <span class="time">${timez}</span>
                                </a>`);
                    });
                    if (count > 0) {
                        $("#mdi-record").append(`<i class="mdi mdi-record" id='dotNotify'></i>`);
                        var style = document.createElement('style');
                        style.id = 'style-tag';
                        style.innerHTML = `
                            .cp-nav
                                .navbar-collapse
                                .navbar-nav.account-nav
                                .header-top-notification
                                .notification-button:after {
                                position: absolute;
                                top: -14px;
                                right: -7px;
                                height: 4px;
                                width: 4px;
                                border-radius: 5px;
                                background: #ffffff;
                                content: "";
                            `;
                        document.head.appendChild(style);
                    }
                }
            })
        }
    </script>
@endif
@stack('script')
</body>

</html>
