<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="assets/clients/js/jquery.min.js"></script>
<script src="assets/clients/js/popper.min.js"></script>
<script src="assets/clients/js/bootstrap.min.js"></script>
<script src="assets/clients/js/feather.min.js"></script>
<script src="assets/clients/js/bootstrap-select.min.js"></script>
<script src="assets/clients/js/jquery.nstSlider.min.js"></script>
<script src="assets/clients/js/owl.carousel.min.js"></script>
<script src="assets/clients/js/visible.js"></script>
<script src="assets/clients/js/jquery.countTo.js"></script>
<script src="assets/clients/js/chart.js"></script>
<script src="assets/clients/js/plyr.js"></script>
<script src="assets/clients/js/tinymce.min.js"></script>
<script src="assets/clients/js/slick.min.js"></script>
<script src="assets/clients/js/jquery.ajaxchimp.min.js"></script>
<script src="/assets/libs/jquery-toast/jquery.toast.min.js"></script>

<script src="assets/clients/js/custom.js"></script>
@if (Session::has('fail') || Session::has('errors'))
    @if (Route::is('company.register'))
        <script type="text/javascript">
            $.toast({
                heading: "Cảnh báo",
                text: "Vui lòng kiểm tra lại thông tin",
                position: "top-right",
                loaderBg: "#da8609",
                icon: "warning",
                hideAfter: 3e3,
                stack: 1
            })
        </script>
    @else
        <script type="text/javascript">
            $.toast({
                heading: "Lỗi đăng nhập!",
                text: "Vui lòng điền đúng thông tin đăng nhập!",
                position: "top-right",
                loaderBg: "#bf441d",
                icon: "error",
                hideAfter: 3e3,
                stack: 1
            })
        </script>
    @endif
@endif
@if(Session::has('confirm'))
    <script type="text/javascript">
        $.toast({
            heading: "Thông báo!",
            text: "Vui lòng kiểm tra email của bạn để xác minh tài khoản!",
            position: "top-right",
            loaderBg: "#3b98b5",
            icon: "info",
            hideAfter: 3e3,
            stack: 1
        })
    </script>
@endif
</body>
</html>
