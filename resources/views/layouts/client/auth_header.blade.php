<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>@yield('title')</title>
    <base href="{{ asset('/') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/clients/css/bootstrap.min.css">

    <!-- External Css -->
    <link rel="stylesheet" href="assets/clients/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/clients/css/themify-icons.css" />
    <link rel="stylesheet" href="assets/clients/css/et-line.css" />
    <link rel="stylesheet" href="assets/clients/css/bootstrap-select.min.css" />
    <link rel="stylesheet" href="assets/clients/css/plyr.css" />
    <link rel="stylesheet" href="assets/clients/css/flag.css" />
    <link rel="stylesheet" href="assets/clients/css/slick.css" />
    <link rel="stylesheet" href="assets/clients/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="assets/clients/css/jquery.nstSlider.min.css" />
    <link href="/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />

    <!-- Custom Css -->
    <link rel="stylesheet" type="text/css" href="assets/clients/css/main.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600%7CRoboto:300i,400,500" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="assets/clients/img/favicon.png">
    
    <livewire:styles />

</head>

<body>
