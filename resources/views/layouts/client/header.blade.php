<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <base href="{{ asset('/') }}">

    @stack('meta')

    <title>
        @yield('page-title')
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="/assets/css/icons.min.css" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/clients/css/bootstrap.min.css">
    <link href="/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />

    <!-- External Css -->
    <link rel="stylesheet" href="/assets/clients/css/et-line.css" />
    <link rel="stylesheet" href="/assets/clients/css/themify-icons.css" />
    <link rel="stylesheet" href="/assets/clients/css/style.css" />
    <link rel="stylesheet" href="/assets/clients/css/bootstrap-select.min.css" />
    <link rel="stylesheet" href="/assets/clients/css/plyr.css" />
    <link rel="stylesheet" href="/assets/clients/css/flag.css" />
    <link rel="stylesheet" href="/assets/clients/css/slick.css" />
    <link rel="stylesheet" href="/assets/clients/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="/assets/clients/css/jquery.nstSlider.min.css" />
    <!-- Custom Css -->
    <link rel="stylesheet" type="text/css" href="/assets/clients/css/main.css">
    <link rel="stylesheet" type="text/css" href="/assets/clients/css/dashboard.css">
    @stack('css')

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600" rel="stylesheet">
    <!-- Favicon -->
    <link rel="icon" href="/assets/clients/img/favicon.png">

</head>

<body>
    @if (Route::is('home'))
        <!-- Header INDEX -->
        <header>
            <nav class="navbar navbar-expand-xl absolute-nav transparent-nav cp-nav navbar-light bg-light fluid-nav">
                <a class="navbar-brand" href="/">
                    <img src="/assets/clients/img/logo.png" class="img-fluid" alt="" width="265px">
                </a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto job-browse">
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">Tìm việc làm</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li class="search-by">
                                    <h5>Theo nhóm ngành</h5>
                                    <ul>
                                        @foreach ($careerGroups as $row)
                                            <li><a href="{{ route('list-career', ['career-groups', $row->slug]) }}">{{ $row->name }}
                                                    <span>( {{ count($row->recruitmentPosts) }} ) </span></a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="search-by">
                                    <h5>Theo địa điểm</h5>
                                    <ul>
                                        @foreach ($cities as $row)
                                            <li><a href="{{ route('list-city', $row->id) }}">{{ $row->name }}
                                                    <span>(
                                                        {{ count($row->recruitmentPosts) }} )</span></a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto main-nav">
                        <li class="menu-item active"><a title="" href="{{ route('home') }}">Trang Chủ</a></li>
                        <li class="menu-item active"><a title="Home" href="{{ route('recruitment.list') }}">Việc
                                làm</a></li>

                        <li class="menu-item dropdown">
                            <a title="" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true"
                                aria-expanded="false">Tin tức</a>
                            <ul class="dropdown-menu">
                                <li class="menu-item"><a href="{{ Route('post') }}">Bài viết</a></li>
                                <li class="menu-item"><a href="{{ Route('postFAQ') }}">Câu hỏi thường gặp</a>
                                </li>
                                <li class="menu-item"><a href="{{ Route('postCompany') }}">Bài viết doanh
                                        nghiệp</a></li>
                            </ul>
                        </li>
                        <li class="menu-item active"><a title="" href="{{ route('contact') }}">Liên hệ</a></li>
                        <li class="menu-item active"><a title="" href="{{ route('about') }}">Giới thiệu</a></li>
                        @auth('company')
                            <li class="menu-item post-job">
                                <a title="Title" href="{{ route('company.recruitment.create') }}"><i
                                        class="fas fa-plus"></i>ĐĂNG TUYỂN</a>
                            </li>
                            @elseauth('student')
                            @if (!empty(Auth::guard('student')->user()->report))
                                <li class="menu-item post-job">
                                    <a href="{{ route('student.report') }}"><i class="fas fa-plus"></i>VIẾT BÁO CÁO</a>
                                </li>
                            @else
                                <li class="menu-item post-job">
                                    <a href="" data-toggle="modal" data-target="#find-jobs"><i class="fas fa-plus"></i>TÌM NƠI THỰC TẬP</a>
                                </li>
                            @endif
                            {{-- <li class="menu-item post-job">
                                <a href="{{ route('student.report') }}" title="Title"><i class="fas fa-plus"></i>Viết
                                    báo cáo
                                </a>
                            </li> --}}
                            @elseauth('manager')
                            @elseauth('staff')
                        @else
                            <li class="menu-item post-job"><a title="Title" href="{{ route('student.login') }}"><i
                                        class="fas fa-user"></i>SINH VIÊN</a></li>
                            <li class="menu-item post-job"><a title="Title" href="{{ route('company.login') }}"><i
                                        class="fas fa-building"></i>DOANH NGHIỆP</a></li>
                        @endauth
                    </ul>
                    @if (Auth::guard('student')->check() || Auth::guard('company')->check() || Auth::guard('staff')->check() || Auth::guard('manager')->check())
                        <ul class="navbar-nav ml-auto account-nav">
                            @if (!Auth::guard('staff')->check() && !Auth::guard('manager')->check())
                                <li class="dropdown menu-item header-top-notification">
                                    <p class="notification-button pointer"></p>
                                    <div class="notification-card">
                                        <div class="notification-head">
                                            <span>Thông báo</span>
                                        </div>
                                        <div class="notification-body">
                                        </div>
                                        <div class="notification-footer">
                                            <a href="                 @if (Auth::guard('student')->check()){{ route('student.notification.getnotification') }}
                                            @elseif(Auth::guard('company')->check()){{ route('company.notification.getnotification') }}
                                                @endif">Xem tất cả</a>
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @auth('student')
                                <li class="menu-item dropdown">
                                    <a title="" href=" {{ route('student.home') }}" data-toggle="dropdown"
                                        class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-user-circle mr-2"></i>
                                        {{ !empty(Auth::guard('student')->user()->profile) ? 'Chào! ' . Auth::guard('student')->user()->profile->last_name : Auth::guard('student')->user()->name }}
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="menu-item"><a href="{{ route('student.home') }}"><i
                                                    class="fas fa-cog"></i> Vào trang quản
                                                trị</a></li>
                                        <li class="menu-item"><a href="{{ route('student.profile') }}"><i
                                                    class="fas fa-user-circle"></i> Tài khoản
                                                của tôi</a>
                                        </li>
                                        <li class="menu-item"><a href="{{ route('student.profile') }}"><i
                                                    class="fas fa-unlock-alt"></i> Đổi mật
                                                khẩu</a>
                                        </li>
                                        <li class="menu-item">
                                            <a
                                                onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                                    class="fas fa-sign-out-alt pointer"></i> Đăng
                                                xuất</a>
                                        </li>
                                        <form
                                            action="@auth('student'){{ route('student.logout', ['guard' => 'student']) }}
                                                                                                                                                    @elseauth('company'){{ route('company.logout', ['guard' => 'company']) }}@endauth "
                                                id="logout-form" method="post">
                                                @csrf
                                            </form>
                                        </ul>
                                    </li>
                                    @elseauth('company')
                                    <li class="menu-item dropdown">
                                        <a title="Title" href=" {{ route('company.home') }}" data-toggle="dropdown"
                                            class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-user-circle mr-2"></i>
                                            {{ !empty(Auth::guard('company')->user()->profile) ? 'Chào! ' . Auth::guard('company')->user()->profile->last_name : Auth::guard('company')->user()->name }}
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="menu-item"><a href="{{ route('company.home') }}"><i
                                                        class="fas fa-cog"></i> Vào trang quản
                                                    trị</a></li>
                                            <li class="menu-item"><a href="{{ route('company.profile') }}"><i
                                                        class="fas fa-user-circle"></i> Doanh nghiệp của tôi</a>
                                            </li>
                                            <li class="menu-item"><a href=""><i class="fas fa-unlock-alt"></i> Đổi mật
                                                    khẩu</a>
                                            </li>
                                            <li class="menu-item">
                                                <a
                                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                                        class="fas fa-sign-out-alt pointer"></i> Đăng
                                                    xuất</a>
                                            </li>
                                            <form
                                                action="@auth('student'){{ route('student.logout', ['guard' => 'student']) }}                                                                                                                                             @elseauth('company'){{ route('company.logout', ['guard' => 'company']) }}@endauth "
                                                    id="logout-form" method="post">
                                                    @csrf
                                                </form>
                                            </ul>
                                        </li>
                                    @endauth
                                    @auth('staff')
                                        <li class="menu-item dropdown">
                                            <a title="Title" href=" {{ route('staff.home') }}" data-toggle="dropdown"
                                                class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-user-circle mr-2"></i>
                                                {{ !empty(Auth::guard('staff')->user()->profile) ? 'Chào! ' . Auth::guard('staff')->user()->profile->last_name : Auth::guard('staff')->user()->name }}
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="menu-item"><a href="{{ route('staff.home') }}"><i
                                                            class="fas fa-cog"></i> Vào trang quản
                                                        trị</a></li>
                                                <li class="menu-item">
                                                    <a
                                                        onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                                            class="fas fa-sign-out-alt pointer"></i> Đăng
                                                        xuất</a>
                                                </li>
                                                <form action="@auth('manager'){{ route('manager.logout', ['guard' => 'manager']) }} @endauth
                                                                                                                        @auth('staff'){{ route('staff.logout', ['guard' => 'staff']) }} @endauth
                                                                                                                            "
                                                    id="logout-form" class="d-none" method="post">@csrf
                                                </form>
                                            </ul>
                                        </li>
                                        @elseauth('manager')
                                        <li class="menu-item dropdown">
                                            <a title="Title" href=" {{ route('manager.home') }}" data-toggle="dropdown"
                                                class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-user-circle mr-2"></i>
                                                {{ !empty(Auth::guard('manager')->user()->profile) ? 'Chào! ' . Auth::guard('manager')->user()->profile->last_name : Auth::guard('manager')->user()->name }}
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="menu-item"><a href="{{ route('manager.home') }}"><i
                                                            class="fas fa-cog"></i> Vào trang quản
                                                        trị</a></li>
                                                <li class="menu-item">
                                                    <a 
                                                        onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                                            class="fas fa-sign-out-alt pointer"></i> Đăng
                                                        xuất</a>
                                                </li>
                                                <form action="@auth('manager'){{ route('manager.logout', ['guard' => 'manager']) }} @endauth
                                                                                                                        @auth('staff'){{ route('staff.logout', ['guard' => 'staff']) }} @endauth
                                                                                                                            "
                                                    id="logout-form" class="d-none" method="post">@csrf
                                                </form>
                                            </ul>
                                        </li>
                                    @endauth
                                </ul>
                            @endif
                        </div>
                    </nav>
                </header>
                <!-- Header End -->
            @else
                <!-- Header Dashboard -->
                <header class="header-2">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="header-top">
                                    <div class="logo-area">
                                        <a href="/"><img src="/assets/clients/img/logo-2.png" alt=""></a>
                                    </div>
                                    <div class="header-top-toggler">
                                        <div class="header-top-toggler-button"></div>
                                    </div>
                                    <div class="top-nav">
                                        @if (Auth::guard('company')->check() || Auth::guard('student')->check())
                                            <div class="dropdown header-top-notification">
                                                <p class="notification-button pointer">Thông báo<sup class="text-danger"
                                                        id="mdi-record"></sup></p>
                                                <div class="notification-card">
                                                    <div class="notification-head">
                                                        <span>Thông báo</span>
                                                       
                                                    </div>
                                                    <div class="notification-body">

                                                    </div>
                                                    <div class="notification-footer">
                                                        <a href="                 @if (Auth::guard('student')->check()){{ route('student.notification.getnotification') }}
                                                        @elseif(Auth::guard('company')->check()){{ route('company.notification.getnotification') }}
                                                            @endif">Xem tất cả</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="dropdown header-top-account">
                                            @if (Auth::guard('student')->check() || Auth::guard('company')->check() || Auth::guard('manager')->check() || Auth::guard('staff')->check())
                                                @auth('student')
                                                    <a href="#" class="account-button">Tài khoản của tôi</a>
                                                    <div class="account-card">
                                                        <div class="header-top-account-info">
                                                            <a href="{{route('student.home')}}" class="account-thumb">
                                                                <img src="{{ !empty(Auth::guard('student')->user())
                                                                    ? Auth::guard('student')->user()->getAvatar()
                                                                    : '' }}"
                                                                    onerror="this.src='assets/clients/img/avt-default.png'"
                                                                    class="img-fluid" alt="">
                                                            </a>
                                                            <div class="account-body">
                                                                <h5><a href="{{route('student.home')}}">{{ !empty(Auth::guard('student')->user()->profile) ? Auth::guard('student')->user()->profile->full_name : Auth::guard('student')->user()->name }}
                                                                    </a>
                                                                </h5>
                                                                <span
                                                                    class="mail">{{ Auth::guard('student')->user()->email }}</span>
                                                            </div>
                                                        </div>
                                                        <ul class="account-item-list">
                                                            <li>
                                                                <a href="{{ route('student.home') }}">
                                                                    <span class="ti-user"></span>
                                                                    Tài khoản của
                                                                    tôi</a>
                                                            </li>
                                                            <li><a href="{{route('student.profile')}}"><span class="ti-settings"></span>Đổi mật khẩu</a>
                                                            </li>
                                                            <li><a href="#"
                                                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();"><span
                                                                        class="ti-power-off"></span>Đăng xuất</a></li>
                                                            <form
                                                                action="@auth('student'){{ route('student.logout', ['guard' => 'student']) }}
                                                                                                                                                                                                                                                                                                                                        @elseauth('company'){{ route('company.logout', ['guard' => 'company']) }}
                                                                                                                                                                                                                                                    @endauth "
                                                                id="logout-form" method="post">" id="logout-form" method="post">
                                                                @csrf
                                                            </form>
                                                        </ul>
                                                    </div>
                                                    @elseauth('company')
                                                    <a href="#" class="account-button">Tài khoản của tôi</a>
                                                    <div class="account-card">
                                                        <div class="header-top-account-info">
                                                            <a href="{{route('company.home')}}" class="account-thumb">
                                                                <img src="{{ !empty(Auth::guard('company')->user())
                                                                    ? Auth::guard('company')->user()->getAvatar()
                                                                    : '' }}"
                                                                    onerror="this.src='assets/clients/img/default-avatar.png'"
                                                                    class="rounded-circle" alt="">
                                                            </a>
                                                            <div class="account-body">
                                                                <h5><a href="{{route('company.home')}}">{{ !empty(Auth::guard('company')->user()->profile) ? Auth::guard('company')->user()->profile->full_name : Auth::guard('company')->user()->name }}
                                                                    </a>
                                                                </h5>
                                                                <span
                                                                    class="mail">{{ Auth::guard('company')->user()->email }}</span>
                                                            </div>
                                                        </div>
                                                        <ul class="account-item-list">
                                                            <li><a
                                                                    href="{{ route('company.user.profile', Auth::guard('company')->user()->id) }}"><span
                                                                        class="ti-user"></span>Tài khoản của
                                                                    tôi</a>
                                                            </li>
                                                            <li><a href="#"><span class="ti-settings"></span>Đổi mật khẩu</a>
                                                            </li>
                                                            <li><a href="#"
                                                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();"><span
                                                                        class="ti-power-off"></span>Đăng xuất</a></li>
                                                            <form
                                                                action="
                                                                                                                                                                                                                                            @auth('student'){{ route('student.logout', ['guard' => 'student']) }}
                                                                                                                                                                                                                                                                                                                                @elseauth('company'){{ route('company.logout', ['guard' => 'company']) }}
                                                                                                                                                                                                                                            @endauth "
                                                                id="logout-form" method="post">
                                                                @csrf
                                                            </form>
                                                        </ul>
                                                    </div>
                                                @endauth
                                                @auth('staff')
                                                    <a href="#" class="account-button">Tài khoản của tôi</a>
                                                    <div class="account-card">
                                                        <div class="header-top-account-info">
                                                            <div class="account-body">
                                                                <h5><a href="{{route('staff.home')}}">{{ !empty(Auth::guard('staff')->user()->profile) ? Auth::guard('staff')->user()->profile->full_name : Auth::guard('staff')->user()->name }}
                                                                        ( Nhân viên )
                                                                    </a>
                                                                </h5>
                                                                <span
                                                                    class="mail">{{ Auth::guard('staff')->user()->email }}</span>
                                                            </div>
                                                        </div>
                                                        <ul class="account-item-list">
                                                            <li>
                                                                <a href="{{ route('staff.home') }}">
                                                                    <span class="ti-user"></span>Vào trang quản trị</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0);"
                                                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();"><span
                                                                        class="ti-settings"></span>Đăng xuất</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    @elseauth('manager')
                                                    <a href="#" class="account-button">Tài khoản của tôi</a>
                                                    <div class="account-card">
                                                        <div class="header-top-account-info">
                                                            <div class="account-body">
                                                                <h5><a href="{{route('manager.home')}}">{{ !empty(Auth::guard('manager')->user()->profile) ? Auth::guard('manager')->user()->profile->full_name : Auth::guard('manager')->user()->name }}
                                                                        ( Trưởng phòng )
                                                                    </a>
                                                                </h5>
                                                                <span
                                                                    class="mail">{{ Auth::guard('manager')->user()->email }}</span>
                                                            </div>
                                                        </div>
                                                        <ul class="account-item-list">
                                                            <li>
                                                                <a href="{{ route('manager.home') }}">
                                                                    <span class="ti-user"></span>Vào trang quản trị</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0);"
                                                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();"><span
                                                                        class="ti-settings"></span>Đăng xuất</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                @endauth
                                                <form action="@auth('manager'){{ route('manager.logout', ['guard' => 'manager']) }} @endauth
                                                                                                            @auth('staff'){{ route('staff.logout', ['guard' => 'staff']) }} @endauth
                                                                                                                "
                                                    id="logout-form" class="d-none" method="post">@csrf
                                                </form>
                                            @else
                                                <a href="#" class="account-button">Đăng nhập</a>
                                                <div class="account-card">
                                                    <ul class="account-item-list">
                                                        <li>
                                                            <a href="{{ route('student.login') }}"><span
                                                                    class="ti-user"></span>Đăng nhập sinh
                                                                viên</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ route('company.login') }}"><span
                                                                    class="ti-briefcase"></span>Đăng nhập doanh
                                                                nghiệp</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @include('layouts.client.navbar')
                            </div>
                        </div>
                    </div>
                </header>
                <!-- Header End -->
            @endif
            <script type="text/javascript">

            </script>
