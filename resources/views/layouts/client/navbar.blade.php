<nav class="navbar navbar-expand-lg cp-nav-2">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
            <li class="menu-item "><a title="Home" href="{{ Route('home') }}">Trang chủ</a></li>
            @auth('student')
                <li class="menu-item "><a title="Home" href="{{ Route('student.home') }}">Tổng quan</a></li>
            @elseauth('company')
                <li class="menu-item "><a title="Home" href="{{ Route('company.home') }}">Trang quản trị</a></li>
            @else
            @endauth
            <li class="menu-item "><a title="Home" href="{{ Route('recruitment.list') }}">Việc làm</a></li>
            <li class="menu-item dropdown">
                <a title="" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true"
                   aria-expanded="false">Tin tức</a>
                <ul class="dropdown-menu">
                    <li class="menu-item"><a href="{{ Route('post') }}">Bài viết</a></li>
                    <li class="menu-item"><a href="{{ Route('postFAQ') }}">Câu hỏi thường gặp</a>
                    </li>
                    <li class="menu-item"><a href="{{ Route('postCompany') }}">Bài viết doanh
                            nghiệp</a></li>
                </ul>

            </li>
            <li class="menu-item"><a href="{{ Route('contact') }}">Liên hệ</a></li>
            <li class="menu-item"><a href="{{ Route('about') }}">Giới thiệu</a></li>
            @auth('student')

                @if (!empty(Auth::guard('student')->user()->report) && (Auth::guard('student')->user()->report->status == REPORT_PENDING) && is_null(Auth::guard('student')->user()->student->status))
                    <li class="menu-item post-job">
                        <a href="{{ route('student.report') }}"><i class="fas fa-plus"></i>VIẾT BÁO CÁO</a>
                    </li>
                @endif
                @if(Auth::guard('student')->user()->student->is_accept)
                    <li class="menu-item post-job">
                        <a href="" data-toggle="modal" data-target="#find-jobs"><i class="fas fa-plus"></i>TÌM NƠI THỰC
                            TẬP</a>
                    </li>
                @endif
            @elseauth('company')
                <li class="menu-item post-job">
                    <a href="{{ route('company.recruitment.create') }}"><i class="fas fa-plus"></i>TẠO BÀI
                        ĐĂNG</a>
                </li>
            @endauth
        </ul>
    </div>
</nav>
