<div class="dashboard-sidebar">
    <div class="company-info">
        <div class="thumb">
            <img src="{{ Auth::user()->getAvatar() }}" onerror="this.src='assets/clients/img/avt-default.png'"
                class="img-fluid" alt="">
        </div>
    </div>
    <div class="company-body">
        <h5>{{ !empty(Auth::user()->profile) ? Auth::user()->profile->full_name : Auth::user()->name }}
        </h5>
        <span>@_{{ !empty(Auth::user()->profile) ? Auth::user()->profile->last_name : 'username' }}</span>
    </div>

    @auth('student')
        <div class="dashboard-menu">
            <ul>
                <li class="{{ Route::is('student.home') ? 'active' : '' }}">
                    <i class="fas fa-home"></i><a href="{{ Route('student.home') }}">Trang tổng quan</a>
                </li>
                <li class="{{ Route::is('student.profile') ? 'active' : '' }}">
                    <i class="fas fa-user"></i><a href="{{ Route('student.profile') }}">Thông tin</a>
                </li>
                <li class="{{ Route::is(['student.resume', 'student.resume.create']) ? 'active' : '' }}">
                    <i class=" fas fa-address-book"></i><a href="{{ Route('student.resume') }}">Quản lý hồ sơ</a>
                </li>
                <li class="{{ Route::is('student.applied-job.index') ? 'active' : '' }}">
                    <i class="fas fa-briefcase"></i><a href="{{ Route('student.applied-job.index') }}">Công việc đã ứng
                        tuyển</a>
                </li>
                <li class="{{ Route::is(['student.report']) ? 'active' : '' }}">
                    <i class="fas fa-file"></i><a href="{{ Route('student.report') }}">Báo cáo thực tập</a>
                </li>
                <li class="{{ Route::is(['student.notification.getnotification']) ? 'active' : '' }}">
                    <i class="fas fa-bell"></i><a href="{{ Route('student.notification.getnotification') }}">Thông
                        báo</a>
                </li>
                <li class="{{ Route::is(['student.feedBack-student.index']) ? 'active' : '' }}">
                    <i class="fas fa-comment"></i><a href="{{ route('student.feedBack-student.index') }}">Phản hồi</a>
                </li>
                <li>
                    <i class="fas fa-globe"></i><a href="{{ route('student.service-exemption-student.index') }}">Dịch vụ</a>
                </li>
            </ul>
            <ul class="delete">
                <li><i class="fas fa-power-off"></i><a
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();" href="">Đăng
                        xuất</a></li>
            </ul>
        </div>
        @elseauth('company')
        <div class="dashboard-menu">
            <ul>
                <li class="{{ Route::is('company.home') ? 'active' : '' }}">
                    <i class="fas fa-home"></i><a href="{{ route('company.home') }}">Trang tổng quan</a>
                </li>
                <li class="{{ Route::is('company.user.*') ? 'active' : '' }}">
                    <i class="fas fa-user"></i><a   
                        href="{{ route('company.user.profile', Auth::guard('company')->user()->id) }}">Tài khoản</a>
                </li>
                <li class="{{ Route::is('company.information.*') ? 'active' : '' }}">
                    <i class="fas fa-building"></i><a href="{{ route('company.information.index') }}">Thông tin doanh
                        nghiệp</a>
                </li>
                </li>
                <li class="{{ Route::is('company.intern-student.*') ? 'active' : '' }}">
                    <i class="fas fa-users"></i><a href="{{ route('company.intern-student.index') }}">Quản lý thực tập
                        sinh</a>
                </li>
                <li class="{{ Route::is('company.recruitment-manager.*') ? 'active' : '' }}">
                    <i class="fas fa-shopping-bag"></i><a href="{{ route('company.recruitment-manager.index') }}">Quản lý
                        ứng
                        tuyển</a>
                </li>
                <li class="{{ Route::is('company.recruitment.*') ? 'active' : '' }}">
                    <i class="fas fa-clipboard"></i><a href="{{ route('company.recruitment') }}">Quản lý đăng tuyển</a>
                </li>
                <li class="{{ Route::is('company.notification.getnotification') ? 'active' : '' }}">
                    <i class="fas fa-bell"></i><a href="{{ Route('company.notification.getnotification') }}">Thông
                        báo</a>
                </li>
                <li class="{{ Route::is('company.feedBack.*') ? 'active' : '' }}">
                    <i class="fas fa-comment-alt"></i><a href="{{ route('company.feedBack.index') }}">Phản hồi</a>
                </li>


            </ul>
            <ul class="delete">
                <li><i class="fas fa-power-off"></i><a href="{{ route('company.logout') }}"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">Đăng xuất</a>
                </li>
                <form action="{{ route('company.logout', ['guard' => 'company']) }}" id="logout-form" method="post">@csrf
                </form>
            </ul>
        </div>
    @endauth
</div>
@push('script')
    <script type="text/javascript">

    </script>
@endpush
