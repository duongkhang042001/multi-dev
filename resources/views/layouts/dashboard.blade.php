@include('layouts.dashboard.header')
<div class="content-page">
    <div class="content">
        <div class="content" >
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        &copy;2021 Made by <span class="font-italic text-info">MULTI-THINKING</span>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->
    </div>
</div>
@include('layouts.dashboard.footer')
