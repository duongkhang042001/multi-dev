<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <base href="{{asset('/')}}">
    <title>
        FPT INTERNSHIP
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" />

</head>

<body class="authentication-bg bg-gradient">
@if ($errors->all() || Session::has('fail'))
    <div class="fixed-top text-center">
        <div class="alert alert-danger rounded-0" role="alert">
            @if(Session::has('fail')) {{Session::get('fail')}} @else   {{Route::is('admin.change.show') ? "Vui lòng kiểm tra lại thông tin!" : "Xin hãy điền đúng thông tin đăng nhập!"}} @endif
        </div>
    </div>
@elseif(Session::has('notify'))
    <div class="fixed-top text-center">
        <div class="alert alert-info rounded-0" role="alert">
            {{Session::get('notify')}}
        </div>
    </div>
@endif
</div>
<div class="account-pages mt-5 pt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
