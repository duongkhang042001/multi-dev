<!-- Right Sidebar -->
<div class="right-bar">
    <div class="rightbar-title">
        <a href="javascript:void(0);" class="right-bar-toggle float-right">
            <i class="mdi mdi-close"></i>
        </a>
        <h5 class="m-0 text-white">Danh mục</h5>
    </div>
    <div class="slimscroll-menu">
        <hr class="mt-0">
        <h5 class="pl-3">Chuyển hướng trang</h5>
        <hr class="mb-0" />
        <div class="p-3">
            <div class="custom-control custom-checkbox mb-2">
                <a href="{{ route('home') }}" target="_blank">Trang chủ</a>
            </div>
            <div class="custom-control custom-checkbox mb-2">
                <a href="{{ route('post') }}" target="_blank">Bài viết</a>
            </div>
            <div class="custom-control custom-checkbox mb-2">
                <a href="{{ route('recruitment.list') }}" target="_blank">Đăng tuyển</a>
            </div>
            <div class="custom-control custom-checkbox mb-2">
                <a href="{{ route('postFAQ') }}" target="_blank">Câu hỏi thường gặp</a>
            </div>
            <div class="custom-control custom-checkbox mb-2">
                <a href="{{ route('postCompany') }}" target="_blank">Bài viết doanh nghiệp</a>
            </div>
            <div class="custom-control custom-checkbox mb-2">
                <a href="{{ route('contact') }}" target="_blank">Liên hệ</a>
            </div>
            <div class="custom-control custom-checkbox">
                <a href="{{ route('about') }}" target="_blank">Giới thiệu</a>
            </div>
        </div>

        <!-- Messages -->
        <hr class="mt-0" />
        <h5 class="pl-3 pr-3">Bình luận mới nhất
        </h5>
        <hr class="mb-0" />
        @livewire('new-comment')
    </div> <!-- end slimscroll-menu-->
</div>
<!-- /Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
