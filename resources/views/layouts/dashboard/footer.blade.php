</div>
<div id="fullDiv" class="">
    <div id="fullDiv_spinner">
        <div class="double-loading">
            <div class="c1"></div>
            <div class="c2"></div>
        </div>
    </div>
</div>


<!-- END wrapper -->
@include('layouts.dashboard.right-sidebar')

<!-- Main js -->
<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/app.min.js"></script>

<!-- Tost-->
<script src="assets/libs/jquery-toast/jquery.toast.min.js"></script>
<!-- Ck editor -->
<script src="assets/libs/ckeditor/ckeditor.js"></script>
<!-- Sweetalert-->
<script src="assets/libs/sweetalert2/sweetalert2.min.js"></script>
<!-- BAOKUNCUSTOMNE-->
<script src="assets/js/custom.js"></script>
<livewire:scripts />
@if (Session::has('fail'))
<script>
    var msg = '{{ Session::get('fail') }}';
        callNotify('error', msg);
</script>
@endif
@if (Session::has('success'))
<script>
    var msg = '{{ Session::get('success') }}';
        callNotify('success', msg);
</script>
@endif
@if (Session::has('status'))
<script>
    var msg = '{{ Session::get('success') }}';
        callNotify('success', msg);
</script>
@endif
@if (Session::has('notify'))
<script>
    var msg = '{{ Session::get('notify') }}';
        callNotify('info', msg);
</script>
@endif
@if (Session::has('warning') || Session::has('errors'))
<script>
    var msg =
            '{{ Session::has('warning') ? Session::get('warning') : 'Vui lòng kiểm tra lại thông tin trước khi xác nhận!' }}';
        callNotify('warning', msg);
</script>
@endif
<script src="https://js.pusher.com/5.0/pusher.min.js"></script>
<script type="text/javascript">
    var pusher = new Pusher('4f19ef6d3cd63794e41f', {
        encrypted: true,
        cluster: "ap1"
    });
</script>

@stack('script')
@if (Auth::guard('staff')->check())
<script type="text/javascript">
    var allUser = pusher.subscribe('all-user');
        allUser.bind('all-user', function() {
            getNotify();
        });
        var allStaff = pusher.subscribe('all-staff');
        allStaff.bind('all-staff', function() {
            getNotify();
        });
        var id = {{Auth::guard('staff')->user()->id}};
        var staff = pusher.subscribe('staff');
        staff.bind(`staff${id}`, function() {
            getNotify();
        });

</script>
<script type="text/javascript">
    $("#btn-icon-notify").on('click', function() {
            $("#dotNotify").remove();
            var xhttp = new XMLHttpRequest();
            xhttp.open("GET", "{{ route('ajax.watchedNotification') }}");
            xhttp.send();
            
        });

        function timeDifference(current, previous) {
            var msPerMinute = 60 * 1000;
            var msPerHour = msPerMinute * 60;
            var msPerDay = msPerHour * 24;  
            var msPerMonth = msPerDay * 30;
            var msPerYear = msPerDay * 365;

            var elapsed = current - previous;

            if (elapsed < msPerMinute) {
                return Math.round(elapsed / 1000) + ' giây trước';
            } else if (elapsed < msPerHour) {
                return Math.round(elapsed / msPerMinute) + ' phút trước';
            } else if (elapsed < msPerDay) {
                return Math.round(elapsed / msPerHour) + ' giờ trước';
            } else if (elapsed < msPerMonth) {
                return Math.round(elapsed / msPerDay) + ' Ngày trước';
            } else if (elapsed < msPerYear) {
                return Math.round(elapsed / msPerMonth) + ' tháng trước';
            } else {
                return Math.round(elapsed / msPerYear) + ' năm trước';
            }
        }

        function getNotify() {

            var xhttp = new XMLHttpRequest();
            xhttp.onload = function() {
                var response = JSON.parse(this.responseText);
                    let count = 0;
                    $("#btn-notiz").html(``);
                    $(".slimscroll").html(``);
                    response.forEach(element => {
                        if (element['is_active'] == 0) count += 1;
                        var d = Date.parse(element['created_at'])
                        let timez = timeDifference(Date.now(), d);
                        $(".slimscroll").append(`<a href="staff${element['notify']['url']}" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-success"><i class="mdi mdi-comment-account-outline"></i></div>
                                            <p class="notify-details">${element['notify']['title']}<small class="text-muted">${timez}</small></p>
                                        </a>`);
                    });
                    if (count > 0) {
                        $("#btn-notiz").html(
                            `<span class="badge badge-danger rounded-circle noti-icon-badge" id='dotNotify' ></span>`
                            );
                    }

                }

         
            xhttp.open("GET", "{{ route('ajax.notification') }}");
            xhttp.send();
        }
        window.onload = function() {
            getNotify();
            $("#doc-notify").removeAttr("style");
            $("#doc-notify").css("overflow", "hidden");
            $("#doc-notify").css("width", "auto");
        };
</script>
@endif

</body>

</html>