@if (Auth::guard('manager')->check())
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="">
        @elseif(Auth::guard('staff')->check())
            <div class="sidebar" data-color="purple" data-background-color="black" data-image="">
            @endif
            <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
                <div class="logo">
                    <a href="/admin" class="simple-text logo-mini">
                        MT
                    </a>
                    <a href="/admin" class="simple-text logo-normal">
                        Multi-Thinking
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <div class="user">
                        <div class="photo">
                            <img src="{{ Auth::user()->avatar }}" onerror="this.src='../assets/img/faces/avatar.jpg'"/>
                        </div>
                        <div class="user-info">
                            <a data-toggle="collapse" href="#collapseExample" class="username">
                         <span>
                             {{ Auth::user()->name }}
                             <b class="caret"></b>
                         </span>
                            </a>
                            <div class="collapse {{ Route::is('manager.profile') ? 'show' : '' }}" id="collapseExample">
                                <ul class="nav">
                                    <li class="nav-item {{ Route::is('manager.profile') ? 'active' : '' }}">
                                        @auth('manager')
                                            <a class="nav-link" href="{{ route('manager.profile') }}">
                                                <span class="sidebar-mini"> MP </span>
                                                <span class="sidebar-normal"> Hồ sơ cá nhân</span>
                                            </a>
                                        @endauth
                                        @auth('staff')
                                            <a class="nav-link" href="{{ route('staff.profile') }}">
                                                <span class="sidebar-mini"> MP </span>
                                                <span class="sidebar-normal"> Hồ sơ cá nhân</span>
                                            </a>
                                        @endauth
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('staff.editProfile') }}">
                                            <span class="sidebar-mini"> EP </span>
                                            <span class="sidebar-normal"> Chỉnh sửa thông tin </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <span class="sidebar-mini"> S </span>
                                            <span class="sidebar-normal"> Cài đặt </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <ul class="nav">
                        <li class="nav-item  {{ Route::is('staff.home')|Route::is('manager.home') ? 'active' : '' }} ">
                            <a class="nav-link" href="/admin">
                                <i class="material-icons">dashboard</i>
                                <p> Tổng quan </p>
                            </a>
                        </li>
                        @auth('manager')
                            <li class="nav-item ">
                                <a class="nav-link" href="">
                                    <i class="material-icons">message</i>
                                    <p>Tin nhắn</p>
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pageStaff">
                                    <i class="material-icons">people_alt</i>
                                    <p>Nhân Viên
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div
                                    class="collapse {{ Route::is('manager.staff.*') ? 'show' : '' }} {{ Route::is('manager.import') ? 'show' : '' }}"
                                    id="pageStaff">
                                    <ul class="nav">
                                        <li class="nav-item {{ Route::is('manager.staff.index') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{ route('manager.staff.index') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Danh sách</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('manager.staff.create') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{ route('manager.staff.create') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Thêm nhân viên</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('manager.import') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{ route('manager.import') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Import nhân viên</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('manager.userlog') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{ route('manager.userlog') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Lịch sử hoạt động</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pageTime">
                                    <i class="material-icons">date_range</i>
                                    <p>Thời gian thực tập
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div class="collapse " id="pageTime">
                                    <ul class="nav">
                                        <li class="nav-item ">
                                            <a class="nav-link" href="{{ route('manager.timeline.index') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Danh sách</span>
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a class="nav-link" href="{{ route('manager.timeline.create') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Thêm thời gian thực tập</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pageTimes">
                                    <i class="material-icons">event_note</i>
                                    <p>Quản Lý Kỳ Học
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div class="collapse" id="pageTimes">
                                    <ul class="nav">
                                        <li class="nav-item ">
                                            <a class="nav-link" href="{{ route('manager.semester.index') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Danh Sách Kỳ</span>
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a class="nav-link" href="{{ route('manager.semester.create') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Thêm Kỳ Học</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pageSetting">
                                    <i class="material-icons">notifications</i>
                                    <p>Quản Lý Thông Báo
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div class="collapse" id="pageSetting">
                                    <ul class="nav">
                                        <li class="nav-item ">
                                            <a class="nav-link" href="{{ route('manager.notify.index') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Danh sách thông báo</span>
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a class="nav-link" href="{{ route('manager.notify.create') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Thêm thông báo</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pageSetting">
                                    <i class="material-icons">settings</i>
                                    <p>Thiết lập hệ thống
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div class="collapse" id="pageSetting">
                                    <ul class="nav">
                                        <li class="nav-item ">
                                            <a class="nav-link" href="{{ route('manager.staff.index') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Danh sách nhân viên</span>
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a class="nav-link" href="{{ route('manager.staff.index') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Thêm nhân viên</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        @endauth
                        @auth('staff')
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pagesExamples">
                                    <i class="material-icons">school</i>
                                    <p>Sinh Viên
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div class="collapse {{ Route::is('staff.student.*') ? 'show' : '' }}"
                                     id="pagesExamples">
                                    <ul class="nav">
                                        <li class="nav-item {{ Route::is('staff.student.index') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.student.index')}}">
                                                <span class="sidebar-mini"> TC </span>
                                                <span class="sidebar-normal">Tất cả</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('staff.student.create') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.student.create')}}">
                                                <span class="sidebar-mini">TM</span>
                                                <span class="sidebar-normal">Thêm Mới</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('staff.import') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{ route('staff.import') }}">
                                                <span class="sidebar-mini"> P </span>
                                                <span class="sidebar-normal">Import Sinh Viên</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pagesExamples1">
                                    <i class="material-icons">apartment</i>
                                    <p class="{{ Route::is('staff.company.*') ? 'font-weight-bold' : '' }}">Doanh Nghiệp
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div class="collapse {{ Route::is('staff.company.*') ? 'show' : '' }}"
                                     id="pagesExamples1">
                                    <ul class="nav">
                                        <li class="nav-item {{ Route::is('staff.company.pending') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.pending')}}">
                                                <span class="sidebar-mini">TCC</span>
                                                <span class="sidebar-normal">Danh Sách Chờ</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('staff.company.index') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.company.index')}}">
                                                <span class="sidebar-mini"> TC </span>
                                                <span class="sidebar-normal">Tất cả</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('staff.company.create') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.company.create')}}">
                                                <span class="sidebar-mini">TM</span>
                                                <span class="sidebar-normal">Thêm Mới</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pagesPost">
                                    <i class="material-icons">post_add</i>
                                    <p class="{{ Route::is('staff.post-type.*') || Route::is('staff.post.*') ? 'font-weight-bold' : '' }}">
                                        Bài Viết
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div
                                    class="collapse {{ Route::is('staff.post-type.*') || Route::is('staff.post.*') ? 'show' : '' }}"
                                    id="pagesPost">
                                    <ul class="nav">
                                        <li class="nav-item {{ Route::is('staff.post.index') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.post.index')}}">
                                                <span class="sidebar-mini">TC</span>
                                                <span class="sidebar-normal">Tất cả</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('staff.post.create') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.post.create')}}">
                                                <span class="sidebar-mini">TBV</span>
                                                <span class="sidebar-normal">Thêm mới</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('staff.post-type.index') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.post-type.index')}}">
                                                <span class="sidebar-mini"> DM </span>
                                                <span class="sidebar-normal">Danh Mục</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pagesRecuitment">
                                    <i class="material-icons">hive</i>
                                    <p class="{{ Route::is('staff.recruitment.*') || Route::is('staff.welfare.*') ? 'font-weight-bold' : '' }}">
                                        Đăng Tuyển
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div
                                    class="collapse {{ Route::is('staff.recruitment.*') || Route::is('staff.welfare.*') ? 'show' : '' }}"
                                    id="pagesRecuitment">
                                    <ul class="nav">
                                        <li class="nav-item {{ Route::is('staff.recruitment.index') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.recruitment.index')}}">
                                                <span class="sidebar-mini">TC</span>
                                                <span class="sidebar-normal">Tất cả</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('staff.recruitment.create') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.recruitment.create')}}">
                                                <span class="sidebar-mini">TM</span>
                                                <span class="sidebar-normal">Tạo Mới</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('staff.welfare.index') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.welfare.index')}}">
                                                <span class="sidebar-mini">P</span>
                                                <span class="sidebar-normal">Phúc Lợi</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pagescareer_group">
                                    <i class="material-icons">psychology</i>
                                    <p>Nghành Nghề
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div class="collapse {{ Route::is('staff.careerGroup.*') ? 'show' : '' }}"
                                     id="pagescareer_group">
                                    <ul class="nav">
                                        <li class="nav-item {{ Route::is('staff.careers.index') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.careers.index')}}">
                                                <span class="sidebar-mini"> N </span>
                                                <span class="sidebar-normal">Ngành</span>
                                            </a>
                                        </li>
                                        <li class="nav-item {{ Route::is('staff.career_group.index') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.careerGroup.index')}}">
                                                <span class="sidebar-mini"> NN </span>
                                                <span class="sidebar-normal">Nhóm Ngành</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pageMessages">
                                    <i class="material-icons">message</i>
                                    <p>Tin Nhắn
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div class="collapse" id="pageMessages">
                                    <ul class="nav">

                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="collapse" href="#pages3">
                                    <i class="material-icons">feedback</i>
                                    <p>Góp Ý
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <div class="collapse {{ Route::is('staff.feedback.*') ? 'show' : '' }}" id="pages3">
                                    <ul class="nav">
                                        <li class="nav-item {{ Route::is('staff.feedback.*') ? 'active' : '' }}">
                                            <a class="nav-link" href="{{route('staff.feedback.index')}}">
                                                <span class="sidebar-mini"> TC </span>
                                                <span class="sidebar-normal">Tất cả</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        @endauth
                    </ul>
                </div>
            </div>
