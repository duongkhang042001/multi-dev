<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <li class="menu-title">MENU</li>

                <li>
                    <a href="/admin"
                        class="{{ Route::is('staff.home') || Route::is('manager.home') ? 'active' : '' }}">
                        <i class="fe-airplay"></i>
                        <!-- <span class="badge badge-danger badge-pill float-right">3</span> -->
                        <span> Tổng Quan </span>
                    </a>
                </li>
                @auth('manager')
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fe-users"></i>
                            <span> Nhân Viên </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('manager.staff.index') }}">Danh Sách</a></li>
                            <li>
                                <a href="javascript: void(0);">
                                    <span>Thêm Mới</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level nav" aria-expanded="false">
                                    <li><a href="{{ route('manager.staff.create') }}">Thêm chi tiết</a></li>
                                    <li><a href="{{ route('manager.import.staff.index') }}">Import Excel</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ route('manager.userlog') }}">Nhật Ký</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fe-clock  "></i>
                            <span>Mốc Thời Gian</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="nav-second-level nav" aria-expanded="false">
                            <li>
                                <a href="javascript: void(0);" aria-expanded="false">
                                    Thời Gian Thực Tập
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-third-level nav" aria-expanded="false">
                                    <li>
                                        <a href="{{ route('manager.timeline.index') }}">Tất Cả</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('manager.timeline.create') }}">Tạo Mới</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);" aria-expanded="false">
                                    Quản Lý Kỳ Học
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-third-level nav" aria-expanded="false">
                                    <li>
                                        <a href="{{ route('manager.semester.index') }}">Tất Cả</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('manager.semester.create') }}">Tạo Mới</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fe-bell"></i>
                            <span>Thông Báo</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('manager.notify.index') }}">Tất Cả</a></li>
                            <li><a href="{{ route('manager.notify.create') }}">Tạo Mới</a></li>
                        </ul>
                    </li>

                    @elseauth('staff')
                    @php
                        $requestCount = \App\Models\Service::where('type', SERVICE_TYPE_INTERNSHIP_CANCEL)
                            ->where('status', SERVICE_STATUS_PENDING)
                            ->count();
                        $requestResetCount = \App\Models\Service::where('type', SERVICE_TYPE_TRANSCRIPT)
                            ->where('status', SERVICE_STATUS_PENDING)
                            ->count();

                        $exemptionCount = \App\Models\Service::where('type', SERVICE_TYPE_EXEMPTION)
                            ->whereIn('status', [SERVICE_STATUS_PENDING])
                            ->count();
                        $internshipAgainCount = \App\Models\Service::where('type', SERVICE_TYPE_INTERNSHIP)
                            ->whereIn('status', [SERVICE_STATUS_PENDING])
                            ->count();
                        $totalRequest = $requestCount + $requestResetCount + $exemptionCount + $internshipAgainCount;
                    @endphp
                    <li>
                        <a href="javascript: void(0);">
                            <i class="far fa-paper-plane"></i>
                            <span> Yêu Cầu </span>
                            @if ($totalRequest)
                                <span class="badge badge-danger badge-pill float-right">{{ $totalRequest }}</span>
                            @else
                                <span class="menu-arrow"></span>
                            @endif
                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="{{ route('staff.request-report.index') }}">Hủy thực tập
                                    @if ($requestCount)
                                        <span
                                            class="badge badge-danger badge-pill float-right">{{ $requestCount }}</span>
                                    @endif
                                </a>
                            </li>
                            <li><a href="{{ route('staff.transcript.index') }}">Cấp bảng điểm</a></li>
                            <li>
                                <a href="{{ route('staff.request-reset-report.index') }}">Sinh viên trễ thực tập
                                    @if ($requestResetCount)
                                        <span
                                            class="badge badge-danger badge-pill float-right">{{ $requestResetCount }}</span>
                                    @endif
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('staff.exemption.index') }}">Miễn giảm thực tập
                                    @if ($exemptionCount)
                                        <span
                                            class="badge badge-info badge-pill float-right">{{ $exemptionCount }}</span>
                                    @endif
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('staff.internship-again.index') }}">Đăng ký thực tập lại
                                    @if ($internshipAgainCount)
                                        <span
                                            class="badge badge-warning badge-pill float-right">{{ $internshipAgainCount }}</span>
                                    @endif
                                </a>
                            </li>
                        </ul>
                    </li>
                    @php $penCom = \App\Models\Report::where('status',REPORT_FINISHED)->count(); @endphp
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fe-users"></i>
                            <span> Sinh Viên </span>
                            @if ($penCom)
                                <span class="badge badge-info badge-pill float-right">{{ $penCom }}</span>
                            @else
                                <span class="menu-arrow"></span>
                            @endif
                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('staff.student.index') }}">Danh Sách</a></li>
                            <li>
                                <a href="javascript: void(0);">
                                    <span>Thêm Mới</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level nav" aria-expanded="false">
                                    <li><a href="{{ route('staff.student.create') }}">Thêm chi tiết</a></li>
                                    <li><a href="{{ route('staff.import.student.show') }}">Import Excel</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ route('staff.report.index') }}"> Báo cáo thực tập
                                    @if ($penCom)
                                        <span class="badge badge-info badge-pill float-right">{{ $penCom }}</span>
                                    @endif
                                </a>
                            </li>
                        </ul>
                    </li>
                    @php $penCom = \App\Models\Company::where('status',COMPANY_STATUS_WAITING)->count(); @endphp
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fe-box"></i>
                            <span> Doanh Nghiệp </span>
                            @if ($penCom)
                                <span class="badge badge-warning badge-pill float-right">{{ $penCom }}</span>
                            @else
                                <span class="menu-arrow"></span>
                            @endif
                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="{{ route('staff.company.pending') }}">Chờ Phê Duyệt
                                    @if ($penCom)
                                        <span class="badge badge-warning badge-pill float-right">{{ $penCom }}</span>
                                    @endif
                                </a>
                            </li>
                            <li><a href={{ route('staff.company.index') }}>Danh Sách</a></li>
                            <li><a href="{{ route('staff.company.create') }}">Thêm Mới</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fe-layout"></i>
                            <span> Bài Viết </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('staff.post.index') }}">Tất Cả</a></li>
                            <li><a href={{ route('staff.post.create') }}>Tạo Mới</a></li>
                            <li><a href="{{ route('staff.post-type.index') }}">Danh Mục</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fe-cast"></i>
                            <span> Đăng Tuyển </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('staff.recruitment.index') }}">Tất Cả</a></li>
                            <li><a href={{ route('staff.recruitment.create') }}>Tạo Mới</a></li>
                            <li><a href="{{ route('staff.welfare.index') }}">Phúc Lợi</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fe-disc"></i>
                            <span>Nghành Nghề</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('staff.careers.index') }}">Ngành</a></li>
                            <li><a href={{ route('staff.careerGroup.index') }}>Nhóm Ngành</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fe-bell"></i>
                            <span>Thông Báo</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="javascript: void(0);">
                                    <span>Thông báo hệ thống</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level nav" aria-expanded="false">
                                    <li><a href="{{ route('staff.notify.index') }}">Danh sách</a></li>
                                    <li><a href="{{ route('staff.notify.create') }}">Tạo thông báo</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ route('staff.notifition.myNotify') }}">Thông báo cá nhân</a></li>
                        </ul>
                    </li>
                    <li>
                        @php
                            $feedbackCount = \App\Models\Feedback::where('is_active', 0)->count();
                        @endphp
                        <a href="javascript: void(0);">
                            <i class="fe-refresh-cw"></i>
                            <span>Phản Hồi
                                @if ($feedbackCount)
                                    <span class="badge badge-warning badge-pill float-right">{{ $feedbackCount }}</span>
                                @else
                                    <span class="menu-arrow"></span>
                                @endif
                            </span>

                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="{{ route('staff.feedback.index') }}">Danh sách phản hồi</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="javascript: void(0);">
                            <i class="fe-mail"></i>
                            <span>Email</span>
                            {{-- <span class="badge badge-warning badge-pill float-right">35</span> --}}
                            <span class="menu-arrow"></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fe-message-circle"></i>
                            <span>Tin Nhắn</span>
                            {{-- <span class="badge badge-info badge-pill float-right">122</span> --}}
                            <span class="menu-arrow"></span>

                        </a>
                    </li>
                @endauth
                <li class="menu-title mt-3">Bổ Sung</li>
                <li>
                    <a href="#">
                        <i class="fe-settings"></i>
                        <span> Cài Đặt </span>
                    </a>
                </li>
                <li>
                    <a
                        href="@auth('manager') {{ route('manager.profile') }} @elseauth('staff')  {{ route('staff.profile') }} @endauth">
                        <i class="fe-user"></i>
                        <span> Hồ Sơ </span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                        class="text-danger">
                        <i class="fe-log-out "></i>
                        <span> Đăng Xuất </span>
                    </a>
                </li>
            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
