<!-- Topbar Start -->
<div class="navbar-custom">
    <ul class="list-unstyled topnav-menu float-right mb-0">

        @auth('staff')
        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" role="button"
                aria-haspopup="false" aria-expanded="false" id="btn-icon-notify">
                <i class="fe-bell noti-icon"></i>
                <div id="btn-notiz"></div>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-lg">
                <!-- item-->
                <div class="dropdown-item noti-title">
                    <h5 class="m-0">
                        <span class="float-right">
                        </span>Thông báo
                    </h5>
                </div>
                <div class="slimscroll noti-scroll" id="doc-notify">
                </div>
                <!-- All-->
                <a href="{{route('staff.notifition.myNotify')}}"
                    class="dropdown-item text-center text-primary notify-item notify-all">
                    Xem tất cả
                    <i class="fi-arrow-right"></i>
                </a>
            </div>
        </li>
        @endauth

        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#"
                role="button" aria-haspopup="false" aria-expanded="false">
                <img src="{{Auth::user()->getAvatar()}}" onerror="this.src='assets/images/default-avatar.png'"
                    alt="user-image" class="rounded-circle">
                <span class="ml-1">{{Auth::user()->profile->last_name}} <i class="mdi mdi-chevron-down"></i> </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <!-- item-->
                <div class="dropdown-header noti-title">
                    <h6 class="text-overflow m-0">Xin chào !</h6>
                </div>

                <!-- item-->
                <a href="@auth('manager') {{ route('manager.profile') }} @elseauth('staff')  {{ route('staff.profile') }} @endauth"
                    class="dropdown-item notify-item">
                    <i class="fe-user"></i>
                    <span>Hồ Sơ Cá Nhân</span>
                </a>

                <!-- item-->
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <i class="fe-settings"></i>
                    <span>Cài Đặt</span>
                </a>

                <!-- item-->
                <a href="javascript:void(0);"
                    onclick="event.preventDefault();document.getElementById('lockScreen-form').submit();"
                    class="dropdown-item notify-item">
                    <i class="fe-lock"></i>
                    <span>Khóa Màn Hình</span>
                </a>
                <form action="@auth('manager'){{ route('admin.lockScreen',['guard'=>'manager']) }} @endauth
                @auth('staff'){{ route('admin.lockScreen',['guard'=>'staff']) }} @endauth
                    " id="lockScreen-form" class="d-none" method="post">@csrf</form>
                <div class="dropdown-divider"></div>

                <!-- item-->
                <a href="javascript:void(0);"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                    class="dropdown-item notify-item">
                    <i class="fe-log-out"></i>
                    <span>Đăng Xuất</span>
                </a>
                <form action="@auth('manager'){{ route('manager.logout',['guard'=>'manager']) }} @endauth
                @auth('staff'){{ route('staff.logout',['guard'=>'staff']) }} @endauth
                    " id="logout-form" class="d-none" method="post">@csrf</form>
            </div>
        </li>

        <li class="dropdown notification-list">
            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Xem trang người dùng"
                class="nav-link right-bar-toggle waves-effect waves-light">
                <i class="fas fa-home noti-icon"></i>
            </a>
        </li>
    </ul>

    <!-- LOGO -->
    <div class="logo-box">
        <a href="/admin" class="logo text-center">
            <span class="logo-lg">
                <img src="assets/images/logo.png" alt="" height="50">
                <!-- <span class="logo-lg-text-light">UBold</span> -->
            </span>
            <span class="logo-sm">
                <!-- <span class="logo-sm-text-dark">U</span> -->
                <img src="assets/images/fpt.png" alt="" height="50">
            </span>
        </a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <button class="button-menu-mobile waves-effect waves-light">
                <i class="fe-menu"></i>
            </button>
        </li>

        <li class="d-none d-sm-block">
            <form class="app-search">
                <div class="app-search-box">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Tìm kiếm...">
                        <div class="input-group-append">
                            <button class="btn" type="submit">
                                <i class="fe-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </li>

    </ul>
</div>
<!-- end Topbar -->