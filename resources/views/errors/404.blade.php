<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <base href="{{ asset('/') }}">
  <title>
    404 Page Not Found
  </title>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="/assets/css/icons.min.css" />
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="/assets/clients/css/bootstrap.min.css">
  <link href="/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />

  <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" />
  @stack('css')
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600" rel="stylesheet">
  <!-- Favicon -->
  <link rel="icon" href="/assets/clients/img/favicon.png">

</head>

<body class="bg-white">
  <div class="pt-4 mt-4 mb-4">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6 col-xl-5">
          <div class="card">
            <div class="card-body p-4">
              <div class="text-center mt-3 pt-1">
                <span><img src="/assets/clients/img/error.png" class="w-50"></span>
                <h1 class="text-error">404</h1>
                <h3 class="text-uppercase text-danger mt-3 mb-0">Không tìm thấy trang</h3>
                <p class="text-muted mt-3">Có vẻ như bạn có thể đã rẽ nhầm. Đừng lo lắng ... Bạn hãy qua lại
                  trang chủ kiểm tra lại. Đây là một
                  mẹo nhỏ có thể giúp bạn đi đúng hướng.</p>
                @if(Auth::guard('manager')->check() || Auth::guard('staff')->check())
                <a class="btn btn-md btn-block btn-gradient waves-effect waves-light mt-3" href="{{url('/admin')}}">Quay về trang chủ</a>
                @else
                <a class="btn btn-md btn-block btn-gradient waves-effect waves-light mt-3" href="{{route('home')}}">Quay về trang chủ</a>
                @endif

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Vendor js -->
  <script src="assets/js/vendor.min.js"></script>

  <!-- App js -->
  <script src="assets/js/app.min.js"></script>

</body>

</html>