@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
    <div class="company-details p-0 shadow-none">
        <div class="title-and-info">
            <div class="title">
                <div class="thumb">
                    <img src="{{Auth::user()->getFile(Auth::user()->company->avatar)}}" class="img-fluid" alt="">
                </div>
                <div class="title-body">
                    <h5>{{Auth::user()->company->name}}</h5>
                    <div class="info">
                        <span class="company-type"><i data-feather="briefcase"></i>Software Firm</span>
                        <span class="office-location"><i data-feather="map-pin"></i>{{Auth::user()->company->city_name}}</span>
                    </div>
                </div>
            </div>
            <div class="download-resume">
                <a href="{{route('company.information.edit',Auth::user()->company->id)}}" class="save-btn text-center"><i data-feather="edit"></i></a>
            </div>
        </div>

        <div class="about-details details-section dashboard-section">
            <h4 class="mt-4"><i data-feather="align-left"></i>Giới thiệu</h4>
            <p>{!!Auth::user()->company->description!!}</p>
            <div class="information">
                <h4>Thông tin doanh nghiệp</h4>
                <ul>
                    <li><span>Lĩnh vực:</span>{{Auth::user()->company->careerGroup->name}}</li>
                    <li><span>Địa chỉ:</span>{{Auth::user()->company->getAddress()}}</li>
                    <li><span>Điện thoại:</span>{{Auth::user()->company->phone}}</li>
                    <li><span>Email:</span>{{Auth::user()->company->email}}</li>
                    <li><span>Mã số thuế:</span>{{Auth::user()->company->tax_number}}</li>
                    <li><span>Website:</span>
                        @foreach(json_decode(Auth::user()->company->url) as $key => $url)
                        @if($key == 'url')
                        {{$url}}
                        @endif
                        @endforeach
                    </li>
                </ul>
            </div>
        </div>

        <div class="intor-video details-section mt-4">
            <h4><i data-feather="image"></i>Hình ảnh doanh nghiệp</h4>

            <img src="{{ Auth::user()->getFile(Auth::user()->company->banner) }}" onerror="this.src='assets/clients/img/avt-default.png'" class="img-fluid mt-4" width="700">

        </div>
    </div>
</div>

@endsection
