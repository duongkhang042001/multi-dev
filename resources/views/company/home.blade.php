@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="dashboard-content-wrapper">
        <div class="dashboard-section user-statistic-block">
            <div class="user-statistic">
                <i data-feather="pie-chart"></i>
                <h3>{{ !empty($countRcm) ? $countRcm : 0 }}</h3>
                <span>Bài đăng tuyển</span>
            </div>
            <div class="user-statistic">
                <i data-feather="briefcase"></i>
                <h3>{{ !empty($countRcmDetail) ? $countRcmDetail : 0 }}</h3>
                <span>Ứng tuyển mới</span>
            </div>
            <div class="user-statistic">
                <i data-feather="heart"></i>
                <h3>{{ !empty($countReport) ? $countReport : 0 }}</h3>
                <span>Báo cáo</span>
            </div>
        </div>
        @livewire('new-recruitment-detail')
    </div>
@endsection
