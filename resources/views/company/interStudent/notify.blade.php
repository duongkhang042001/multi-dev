@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section-padding-150 error-page-wrap text-center white-bg">
                    <div class="icon">
                        <img src="assets/images/error.png" class="img-fluid" alt="">
                    </div>
                    <h1>404</h1>
                    <p>Sinh viên chưa hoàn thành báo cáo thực tập, vui lòng doanh nghiệp quay lại sao</p>
                    <a href="{{route('company.intern-student.index')}}" class="button">Quay lại</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection