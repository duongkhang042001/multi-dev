@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="privacy-tab" data-toggle="tab" href="#privacy" role="tab" aria-controls="privacy" aria-selected="true">Thực tập Sinh</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="terms-tab" data-toggle="tab" href="#terms" role="tab" aria-controls="terms" aria-selected="false">Đánh giá thực tập</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="regal-tab" data-toggle="tab" href="#regal" role="tab" aria-controls="regal" aria-selected="false">Hoàn tất thực tập</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="regal1-tab" data-toggle="tab" href="#regal1" role="tab" aria-controls="regal1" aria-selected="false">Hủy thực tập</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="privacy" role="tabpanel" aria-labelledby="privacy-tab">
            <div class="manage-candidate-container">
                @if(count($internStudent))
                <table class="table">
                    <thead>
                        <tr>
                            <th>Thông tin</th>
                            <th>Vị trí thực tập</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="action">Chức năng</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($internStudent as $row)
                        <tr class="candidates-list">
                            <td class="title">
                                <div class="thumb">
                                    <img src="{{FILE_URL .$row->user->avatar}}" onerror="this.src='assets/clients/img/avt-default.png'" class="img-fluid" alt="">
                                </div>
                                <div class="body">
                                    <h5>
                                        <a type="button" onclick="infoStudent({{$row->user_id}})" data-toggle="modal" data-target="#modal-about-me1">{{$row->user->name}}</a>
                                    </h5>
                                    <div class="info">
                                        <span class="designation"><i data-feather="check-square"></i>
                                            {{date('d/m/Y H:i:s', strtotime($row->created_at))}}
                                        </span>
                                    </div>
                                </div>
                            </td>

                            <td>{{$row->intern_position}}</td>
                            <td class="text-center">
                                @if(!empty($row->serviceCancel) && $row->serviceCancel->status == SERVICE_STATUS_PENDING)
                                    <p class="text-warning">Đang yêu cầu hủy thực tập</p>

                                @elseif ($row->status == REPORT_PENDING)
                                    <p class="text-warning">Thực tập</p>

                                @endif
                            </td>
                            <td class="action">
                                <a href="#" title="Tải báo cáo" class="download"><i class="fas fa-download"></i></a>
                                <a href="{{route('company.intern-student.show',$row->id)}}" title="Báo cáo thực tập" class="inbox"><i class="fas fa-book-open"></i></a>
                                @if(!empty($row->serviceCancel) && $row->serviceCancel->status == SERVICE_STATUS_PENDING)
                                @else
                                <a onclick="cancelReport({{$row->id}})" title="Hủy thực tập" class="remove" data-toggle="modal" data-target="#modal-about-me"><i class="fas fa-times"></i></a>
                                @endif
                               
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                @else
                 <p class="p-3">Chưa có dữ liệu!</p>
                @endif
                <div class="pagination-list text-center">
                    <nav class="navigation pagination">
                        <div class="nav-links">
                            @if ($internStudent->lastPage() > 1)
                            <a class="prev page-numbers" href="{{ $internStudent->url(1) }}"><i class="fas fa-angle-left"></i></a>
                            @for ($i = 1; $i <= $internStudent->lastPage(); $i++)
                                <?php
                                $half_total_links = floor(5 / 2);
                                $from = $internStudent->currentPage() - $half_total_links;
                                $to = $internStudent->currentPage() + $half_total_links;
                                if ($internStudent->currentPage() < $half_total_links) {
                                    $to += $half_total_links - $internStudent->currentPage();
                                }
                                if ($internStudent->lastPage() - $internStudent->currentPage() < $half_total_links) {
                                    $from -= $half_total_links - ($internStudent->lastPage() - $internStudent->currentPage()) - 1;
                                }
                                ?>
                                @if ($from < $i && $i < $to) @if ($internStudent->currentPage() == $i)
                                    <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                    @else
                                    <a class="page-numbers" href="{{ $internStudent->url($i) }}">{{ $i
                                        }}</a>
                                    @endif
                                    @endif
                                    @endfor
                                    <a class="next page-numbers" href="{{ $internStudent->url($internStudent->lastPage()) }}"><i class="fas fa-angle-right"></i></a>
                                    @endif
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="terms" role="tabpanel" aria-labelledby="terms-tab">
            <div class="manage-candidate-container">
                @if(count($reportRate))
                <table class="table">
                    <thead>
                        <tr>
                            <th>Thông tin</th>
                            <th>Vị trí thực tập</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="action">Chức năng</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($reportRate as $row)
                        <tr class="candidates-list">
                            <td class="title">
                                <div class="thumb">
                                    <img src="{{FILE_URL .$row->user->avatar}}" onerror="this.src='assets/clients/img/avt-default.png'" class="img-fluid" alt="">
                                </div>
                                <div class="body">
                                    <h5>
                                        <a type="button" onclick="infoStudent({{$row->user_id}})" data-toggle="modal" data-target="#modal-about-me1">{{$row->user->name}}</a>
                                    </h5>
                                    <div class="info">
                                        <span class="designation"><i data-feather="check-square"></i>
                                            {{date('d/m/Y H:i:s', strtotime($row->created_at))}}
                                        </span>
                                    </div>
                                </div>
                            </td>

                            <td>{{$row->intern_position}}</td>

                            <td class="text-center">
                                @if(!empty($row->reportRate->id))
                                @if(($row->reportRate->is_active == 0))
                                <form action="{{route('company.report-rate.update',$row->id)}}" method="post" onsubmit="getLoading()">
                                    @csrf
                                    @method('PUT')
                                    <div class="buttons">
                                        <button class="btn btn-info">
                                            <p>Chấp nhận đánh giá</p>
                                        </button>
                                    </div>
                                </form>
                                @else
                                <p class="text-success">Hoàn tất đánh giá</p>
                                @endif
                                @else
                                <p class="text-info">Yêu cầu đánh giá</p>
                                @endif
                            </td>
                            <td class="action">

                                <a href="#" title="Tải báo cáo" class="download"><i class="fas fa-download"></i></a>
                                <a href="{{route('company.intern-student.show',$row->id)}}" title="Báo cáo thực tập" class="inbox"><i class="fas fa-book-open"></i></a>

                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                @else
                 <p class="p-3">Chưa có dữ liệu!</p>
                @endif
                <div class="pagination-list text-center">
                    <nav class="navigation pagination">
                        <div class="nav-links">
                            @if ($reportRate->lastPage() > 1)
                            <a class="prev page-numbers" href="{{ $reportRate->url(1) }}"><i class="fas fa-angle-left"></i></a>
                            @for ($i = 1; $i <= $reportRate->lastPage(); $i++)
                                <?php
                                $half_total_links = floor(5 / 2);
                                $from = $reportRate->currentPage() - $half_total_links;
                                $to = $reportRate->currentPage() + $half_total_links;
                                if ($reportRate->currentPage() < $half_total_links) {
                                    $to += $half_total_links - $reportRate->currentPage();
                                }
                                if ($reportRate->lastPage() - $reportRate->currentPage() < $half_total_links) {
                                    $from -= $half_total_links - ($reportRate->lastPage() - $reportRate->currentPage()) - 1;
                                }
                                ?>
                                @if ($from < $i && $i < $to) @if ($reportRate->currentPage() == $i)
                                    <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                    @else
                                    <a class="page-numbers" href="{{ $reportRate->url($i) }}">{{ $i
                                        }}</a>
                                    @endif
                                    @endif
                                    @endfor
                                    <a class="next page-numbers" href="{{ $reportRate->url($reportRate->lastPage()) }}"><i class="fas fa-angle-right"></i></a>
                                    @endif
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="regal" role="tabpanel" aria-labelledby="regal-tab">
            <div class="manage-candidate-container">
                @if(count($reportRatePass))
                <table class="table">
                    <thead>
                        <tr>
                            <th>Thông tin</th>
                            <th>Vị trí thực tập</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="action">Chức năng</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($reportRatePass as $row)
                        <tr class="candidates-list">
                            <td class="title">
                                <div class="thumb">
                                    <img src="{{FILE_URL .$row->user->avatar}}" onerror="this.src='assets/clients/img/avt-default.png'" class="img-fluid" alt="">
                                </div>
                                <div class="body">
                                    <h5>
                                        <a type="button" onclick="infoStudent({{$row->user_id}})" data-toggle="modal" data-target="#modal-about-me1">{{$row->user->name}}</a>
                                    </h5>
                                    <div class="info">
                                        <span class="designation"><i data-feather="check-square"></i>
                                            {{date('d/m/Y H:i:s', strtotime($row->created_at))}}
                                        </span>
                                    </div>
                                </div>
                            </td>

                            <td>{{$row->intern_position}}</td>
                            <td class="text-center">
                                @if($row->status == REPORT_PASS)<p class="text-success">Đạt thực tập</p>@endif
                            </td>
                            <td class="action">

                                <a href="#" title="Tải báo cáo" class="download"><i class="fas fa-download"></i></a>
                                <a href="{{route('company.intern-student.show',$row->id)}}" title="Báo cáo thực tập" class="inbox"><i class="fas fa-book-open"></i></a>

                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                @else
                 <p class="p-3">Chưa có dữ liệu!</p>
                @endif
                <div class="pagination-list text-center">
                    <nav class="navigation pagination">
                        <div class="nav-links">
                            @if ($reportRatePass->lastPage() > 1)
                            <a class="prev page-numbers" href="{{ $reportRatePass->url(1) }}"><i class="fas fa-angle-left"></i></a>
                            @for ($i = 1; $i <= $reportRatePass->lastPage(); $i++)
                                <?php
                                $half_total_links = floor(5 / 2);
                                $from = $reportRatePass->currentPage() - $half_total_links;
                                $to = $reportRatePass->currentPage() + $half_total_links;
                                if ($reportRatePass->currentPage() < $half_total_links) {
                                    $to += $half_total_links - $reportRatePass->currentPage();
                                }
                                if ($reportRatePass->lastPage() - $reportRatePass->currentPage() < $half_total_links) {
                                    $from -= $half_total_links - ($reportRatePass->lastPage() - $reportRatePass->currentPage()) - 1;
                                }
                                ?>
                                @if ($from < $i && $i < $to) @if ($reportRatePass->currentPage() == $i)
                                    <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                    @else
                                    <a class="page-numbers" href="{{ $reportRatePass->url($i) }}">{{ $i
                                        }}</a>
                                    @endif
                                    @endif
                                    @endfor
                                    <a class="next page-numbers" href="{{ $reportRatePass->url($reportRatePass->lastPage()) }}"><i class="fas fa-angle-right"></i></a>
                                    @endif
                        </div>
                    </nav>
                </div>
            </div>

        </div>
        <div class="tab-pane fade" id="regal1" role="tabpanel" aria-labelledby="regal1-tab">
            <div class="manage-candidate-container">
                @if(count($reportRateFail))
                <table class="table">
                    <thead>
                        <tr>
                            <th>Thông tin</th>
                            <th>Vị trí thực tập</th>
                            <th class="text-center">Trạng thái</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reportRateFail as $row)
                        <tr class="candidates-list">
                            <td class="title">
                                <div class="thumb">
                                    <img src="{{FILE_URL .$row->user->avatar}}" onerror="this.src='assets/clients/img/avt-default.png'" class="img-fluid" alt="">
                                </div>
                                <div class="body">
                                    <h5>
                                        <a type="button" onclick="infoStudent({{$row->user_id}})" data-toggle="modal" data-target="#modal-about-me1">{{$row->user->name}}</a>
                                    </h5>
                                    <div class="info">
                                        <span class="designation"><i data-feather="check-square"></i>
                                            {{date('d/m/Y H:i:s', strtotime($row->created_at))}}
                                        </span>
                                    </div>
                                </div>
                            </td>

                            <td>{{$row->intern_position}}</td>
                            <td class="text-center">
                                @if($row->serviceCancel->status == SERVICE_STATUS_PENDING)<p class="text-danger">Chờ xử lý</p>
                                @elseif($row->serviceCancel->status == SERVICE_STATUS_APPROVED)<p class="text-warning">Đã hủy thực tập</p>
                                @else($row->serviceCancel->status == SERVICE_STATUS_CANCEL)<p class="text-danger">Từ chối hủy thực tập</p>
                                @endif
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                @else
                <p class="p-3">Chưa có dữ liệu!</p>
                @endif
                <div class="pagination-list text-center">
                    <nav class="navigation pagination">
                        <div class="nav-links">
                            @if ($reportRateFail->lastPage() > 1)
                            <a class="prev page-numbers" href="{{ $reportRateFail->url(1) }}"><i class="fas fa-angle-left"></i></a>
                            @for ($i = 1; $i <= $reportRateFail->lastPage(); $i++)
                                <?php
                                $half_total_links = floor(5 / 2);
                                $from = $reportRateFail->currentPage() - $half_total_links;
                                $to = $reportRateFail->currentPage() + $half_total_links;
                                if ($reportRateFail->currentPage() < $half_total_links) {
                                    $to += $half_total_links - $reportRateFail->currentPage();
                                }
                                if ($reportRateFail->lastPage() - $reportRateFail->currentPage() < $half_total_links) {
                                    $from -= $half_total_links - ($reportRateFail->lastPage() - $reportRateFail->currentPage()) - 1;
                                }
                                ?>
                                @if ($from < $i && $i < $to) @if ($reportRateFail->currentPage() == $i)
                                    <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                    @else
                                    <a class="page-numbers" href="{{ $reportRateFail->url($i) }}">{{ $i
                                        }}</a>
                                    @endif
                                    @endif
                                    @endfor
                                    <a class="next page-numbers" href="{{ $reportRateFail->url($reportRateFail->lastPage()) }}"><i class="fas fa-angle-right"></i></a>
                                    @endif
                        </div>
                    </nav>
                </div>
            </div>

        </div>
    </div>
    <div class="about-details details-section dashboard-section">
        <div class="modal fade" id="modal-about-me1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" id="updateStatus">
                </div>
            </div>
        </div>
    </div>
    <div class="about-details details-section dashboard-section">
        <div class="modal fade" id="modal-about-me" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" id="cancelReport">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    function infoStudent(id) {
        document.getElementById('updateStatus').innerHTML = `Đang tải dữ liệu`;
        $.ajax({
            url: `{{route('ajax.student')}}`,
            type: 'get',
            data: {
                _token: '{{csrf_token()}}',
                id: id,
            },
            success: function(response) {
                document.getElementById('updateStatus').innerHTML = response;
                console.log(response);
            },
            error: function(response) {
                document.getElementById('updateStatus').innerHTML = response['responseText'];
            }
        });
    }

    function cancelReport(id) {
        document.getElementById('cancelReport').innerHTML = `Đang tải dữ liệu`;
        $.ajax({
            url: `{{route('ajax.reportCancel')}}`,
            type: 'get',
            data: {
                _token: '{{csrf_token()}}',
                id: id,
            },
            success: function(response) {
                document.getElementById('cancelReport').innerHTML = response;
                console.log(response);
            },
            error: function(response) {
                document.getElementById('cancelReport').innerHTML = response['cancelReport'];
            }
        });
    }
</script>
@endpush