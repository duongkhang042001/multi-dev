@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
    <h3 class="text-center mt-3">BÁO CÁO THỰC TẬP TẠI DOANH NGHIỆP</h3>
    <p class="text-center" style="font-size: 20px;">Thời gian: từ 16/08/2021 đến 28/10/2021</p>
    <div class="information-and-contact mb-4">
        <div class="information">
            <h4>Thông tin thực tập</h4>
            <div class="row">
                <div class="col-sm-2">
                    <img src="{{ FILE_URL . $report->user->avatar }}" class="w-100">
                </div>
                <div class="col-sm-10">
                    <ul class="row">
                        <div class="col-sm-6">
                            <li><span>Sinh viên:</span>{{ !empty($report->user->name) ? $report->user->name : "Chưa có dữ liệu" }}
                            </li>
                            <li><span>Vị
                                    trí:</span>{{ !empty($report->intern_position) ? $report->intern_position : "Chưa có dữ liệu" }}
                            </li>
                            <li><span>Điện
                                    thoại:</span>{{ !empty($report->user->profile->phone) ? $report->user->profile->phone : "Chưa có dữ liệu" }}
                            </li>
                            <li><span>Email:</span>{{ !empty($report->user->email) ? $report->user->email : "Chưa có dữ liệu" }}
                            </li>
                        </div>
                        <div class="col-sm-6">
                            <li><span>Người hướng
                                    dẫn:</span>{{ !empty($report->tutor_name) ? $report->tutor_name : "Chưa có dữ liệu" }}</li>
                            <li><span>Vị
                                    trí:</span>{{ !empty($report->tutor_position) ? $report->tutor_position : "Chưa có dữ liệu" }}
                            </li>
                            <li><span>Điện
                                    thoại:</span>{{ !empty($report->tutor_phone) ? $report->tutor_phone : "Chưa có dữ liệu" }}
                            </li>
                            <li><span>Email:</span>{{ !empty($report->tutor_email) ? $report->tutor_email : "Chưa có dữ liệu" }}
                            </li>
                        </div>
                    </ul>
                </div>

            </div>

        </div>
    </div>
    <div class="edication-background details-section dashboard-section">
        <h4><i data-feather="align-left"></i>GIỚI THIỆU KHÁI QUÁT VỀ ĐƠN VỊ THỰC TẬP</h4>
        <div class="education-label">
            <span class="study-year">
                <h5>Tóm lược quá trình hình thành và phát triển</h5>
            </span>
            <p class="text-dark">{!! !empty($report->about) ? $report->about : "Chưa có dữ liệu" !!}</p>
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Chức năng và lĩnh vực hoạt động</h5>
            </span>
            <p class="text-dark">{!! !empty($report->function_area_activities) ? $report->function_area_activities : "Chưa có dữ liệu" !!}</p>
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Sản phẩm và dịch vụ</h5>
            </span>
            <p class="text-dark">{!! !empty($report->product_services) ? $report->product_services : "Chưa có dữ liệu" !!}</p>
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Tổ chức quản lý hành chính, nhân sự</h5>
            </span>
            <p class="text-dark">{!! !empty($report->organization) ? $report->organization : "Chưa có dữ liệu" !!}</p>
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Chiến lược và phương hướng phát triển của đơn vị trong tương lai</h5>
            </span>
            <p class="text-dark">{!! !empty($report->strategy_future) ? $report->strategy_future : "Chưa có dữ liệu" !!}</p>
        </div>
    </div>
    <div class="edication-background details-section dashboard-section">
        <h4><i data-feather="align-left"></i>BÁO CÁO NỘI DUNG CÔNG VIỆC THỰC TẬP</h4>
        <div class="education-label">
            <span class="study-year">
                <h5>Giới thiệu tóm tắt các hoạt động/công việc tại đơn vị thực tập</h5>
            </span>
            <p class="text-dark">
                {!! !empty($report->summary_activities) ? $report->summary_activities : "Chưa có dữ liệu" !!}
            </p>
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Mô tả các công việc được phân công thực hiện hoặc được tham gia tại đơn vị </h5>
            </span>
            <p class="text-dark">
                {!! !empty($report->work) ? $report->work : "Chưa có dữ liệu" !!}
            </p>
        </div>
    </div>
    <div class="about-details details-section dashboard-section">
        <h4><i data-feather="briefcase"></i>Báo cáo công tác hàng tuần</h4>
        <div class="policy-tab">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                @foreach ($allWeek as $key => $week)
                <li class="nav-item">
                    <a class="nav-link" id="privacy-tab{{$key}}" href="#privacy{{$key}}" data-toggle="tab" role="tab" aria-controls="privacy{{$key}}" aria-selected="true"> Tuần {{$key}}</a>
                </li>
                @endforeach
            </ul>
            <div class="tab-content" id="myTabContent">
                @foreach ($allWeek as $key => $week)
                <div class="tab-pane fade" id="privacy{{$key}}" role="tabpanel" aria-labelledby="privacy-tab{{$key}}" class="mb-0">
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <thead>
                                <th style="width: 20%; text-align: center;">
                                    {{date('d/m/Y', strtotime($week['start_week']))}}
                                    {{date('d/m/Y', strtotime($week['end_week']))}}
                                </th>
                                <th style="width: 40%; text-align: center;">
                                    <h5>Sáng</h5>
                                </th>
                                <th style="width: 40%; text-align: center;">
                                    <h5>Chiều</h5>
                                </th>
                            </thead>
                            <tbody>
                                @foreach ($reportDetail as $item)
                                @if (strtotime($item->date) <= strtotime($week['end_week']) && strtotime($item->date) >= strtotime($week['start_week']))
                                    <tr>
                                        <td class="text-center">
                                            {{DAYOFWEEK[\Carbon\Carbon::parse($item->date)->dayOfWeek]}} <br>
                                            ({{date('d/m/Y', strtotime($item->date))}})
                                        </td>
                                        <td>{{ $item->content_morning }}</td>
                                        <td>{{ $item->content_afternoon }} </td>
                                    </tr>
                                    @endif
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>
    <div class="professonal-skill dashboard-section details-section">
        <h4><i data-feather="feather"></i>Kết quả đạt được trong thời gian thực tập</h4>
        <p class="text-dark">{!! !empty($report->result) ? $report->result : "Chưa có dữ liệu" !!}</p>
    </div>
    <div class="special-qualification dashboard-section details-section">
        <h4><i data-feather="gift"></i>Những điều chưa thực hiện được</h4>
        <p class="text-dark">{!! !empty($report->work_unfinished) ? $report->work_unfinished : "Chưa có dữ liệu" !!}</p>
    </div>
    <div class="professonal-skill dashboard-section details-section">
        <h4><i data-feather="feather"></i>Các vấn đề cần tiếp tục nghiên cứu, phát triển</h4>
        <p class="text-dark">{!! !empty($report->problem_develop) ? $report->problem_develop : "Chưa có dữ liệu" !!}</p>
    </div>
    <div class="edication-background details-section dashboard-section">
        <h4><i data-feather="book"></i>Nhận xét chung, những thuận lợi, khó khăn trong suốt quá trình thực tập</h4>
        <div class="education-label">
            <span class="study-year">
                <h5>Nhận xét chung</h5>
            </span> <br>
            {!! !empty($report->general_comment) ? json_decode($report->general_comment)->general : "Chưa có dữ liệu" !!}
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Thuận lợi</h5>
            </span> <br>
            {!! !empty($report->general_comment) ? json_decode($report->general_comment)->advantage : "Chưa có dữ liệu" !!}
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Khó khăn</h5>
            </span> <br>
            {!! !empty($report->general_comment) ? json_decode($report->general_comment)->difficult : "Chưa có dữ liệu" !!}
        </div>
    </div>

    <div class="details-section">
        <h4><i data-feather="align-left"></i>ĐỀ XUẤT – KIẾN NGHỊ VỚI ĐƠN VỊ THỰC TẬP</h4>
        <p class="text-dark">{!! !empty($report->propose) ? $report->propose : "Chưa có dữ liệu" !!}</p>
    </div>
    <div class="edication-background details-section dashboard-section">
        <div class="row">
            <div class="col-10">
                <h4><i data-feather="align-left"></i>NHẬN XÉT CỦA CƠ QUAN THỰC TẬP</h4>
            </div>
            <div class="col-2">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-about-me">
                    <i data-feather="edit-2"></i>
                </button>
            </div>
        </div>
        <div class="modal fade" id="modal-about-me" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="content">
                            @if (!empty($report->reportRate->id))
                            <form action="{{ route('company.intern-student.update', $report->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <h4><i data-feather="align-left"></i>Nhận xét từ doanh nghiệp</h4>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Ưu điểm sinh viên thực tập</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="advantages" value="{{ $report->reportRate->advantages }}">
                                        @error('advantages')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Hạn chế sinh viên thực tập</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="defect" value="{{ $report->reportRate->defect }}">
                                        @error('defect')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Đề xuất vào góp ý sinh viên</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="content" value="{{ $report->reportRate->content }}">
                                        @error('content')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Điểm thái độ</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="attitude_point" value="{{ $report->reportRate->attitude_point }}">
                                        @error('attitude_point')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Điểm kết quả công việc</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="work_point" value="{{ $report->reportRate->work_point }}">
                                        @error('work_point')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Kết quả cuối cùng</label>
                                    <div class="col-sm-9">
                                        <input class="custom-radio" type="radio" id="radio_1" name="status" value="0" @if ($report->reportRate->status == 0) checked @endif>
                                        <label for="radio_1">
                                            <span class="dot"></span> <span class="package-type">Đạt</span>
                                        </label>
                                        <input class="custom-radio" type="radio" id="radio_2" name="status" value="1" @if ($report->reportRate->status == 1) checked @endif>
                                        <label for="radio_2">
                                            <span class="dot"></span> <span class="package-type">Không đạt</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="buttons text-center">
                                    <button class="primary-bg">Cập nhật</button>
                                    <button class="" data-dismiss="modal">Hủy</button>
                                </div>


                            </form>
                            @else
                            <form action="{{ route('company.intern-student.store') }}" method="POST">
                                @csrf
                                @method('POST')
                                <input type="hidden" name="id" value="{{ $report->id }}">
                                <h4><i data-feather="align-left"></i>Nhận xét từ doanh nghiệp</h4>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Ưu điểm sinh viên thực tập</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="advantages" value="{{old('advantages')}}">
                                        @error('advantages')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Hạn chế sinh viên thực tập</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="defect" value="{{old('defect')}}">
                                        @error('defect')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Đề xuất vào góp ý sinh viên</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="content" value="{{old('content')}}">
                                        @error('content')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Điểm thái độ</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="attitude_point" value="{{old('attitude_point')}}">
                                        @error('attitude_point')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Điểm kết quả công việc</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="work_point" value="{{old('work_point')}}">
                                        @error('work_point')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Kết quả cuối cùng</label>
                                    <div class="col-sm-9">
                                        <input class="custom-radio" type="radio" id="radio_1" name="status" value="0" @if (old('status')==0) checked @endif>
                                        <label for="radio_1">
                                            <span class="dot"></span> <span class="package-type">Đạt</span>
                                        </label>
                                        <input class="custom-radio" type="radio" id="radio_2" name="status" value="1" @if (old('status')==1) checked @endif>
                                        <label for="radio_2">
                                            <span class="dot"></span> <span class="package-type">Không đạt</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="buttons text-center">
                                    <button class="primary-bg">Cập nhật</button>
                                    <button class="" data-dismiss="modal">Hủy</button>
                                </div>


                            </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Ưu điểm :</h5>
            </span>
            <span>{{ !empty($report->reportRate->advantages) ? $report->reportRate->advantages : "Chưa có dữ liệu" }}</span>
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Hạn chế :</h5>
            </span>
            <span>{{ !empty($report->reportRate->defect) ? $report->reportRate->defect : "Chưa có dữ liệu" }}</span>
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Đề xuất, góp ý :</h5>
            </span>
            <span>{{ !empty($report->reportRate->content) ? $report->reportRate->content : "Chưa có dữ liệu" }}</span>
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Điểm đánh giá(Chấm điểm theo thang điểm từ 1 đến 10, 10 là điểm cao
                    nhất)
                </h5>
            </span>
            <br>
            <span class="study-year">
                <h5>Thái độ, ý thức,quy định của doanh nghiệp: </h5>
            </span>
            <span>{{ !empty($report->reportRate->attitude_point) ? $report->reportRate->attitude_point : "Chưa có dữ liệu" }} điểm</span>
            <br>
            <span class="study-year">
                <h5>Kết quả công việc: </h5>
            </span>
            <span> {{ !empty($report->reportRate->work_point) ? $report->reportRate->work_point : "Chưa có dữ liệu" }} điểm</span>
        </div>
        <div class="education-label">
            <span class="study-year">
                <h5>Đánh giá cuối cùng:</h5>
            </span>
            <span>{{isset($report->reportRate->status) ? ($report->reportRate->status == 0 ? 'Đạt' : 'Không đạt') : "Chưa có dữ liệu" }}</span>
        </div>
    </div>
</div>

@endsection

@push('script')
@if ($errors->any())
<script>
    $('#modal-about-me').modal('show')
</script>
@endif
@endpush
