@extends('layouts.client_guest')
@section('page-title', 'Doanh nghiệp')
@section('content')
<div class="alice-bg padding-top-70 padding-bottom-70">
    <div class="container no-gliters">
        <div class="row no-gliters">
            <div class="col">
                <div class="post-container">
                    <div class="post-content-wrapper">
                        <form action="{{route('company.initial.profile.update',$company->id)}}" class="job-post-form" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="basic-info-input">
                                <h4><i data-feather="plus-circle"></i>THÔNG TIN DOANH NGHIỆP</h4>
                                <div id="information" class="form-group row">
                                    <label class="col-md-3 col-form-label">Tên đầy đủ <span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="name" class="form-control" placeholder="Công ty cổ phần FPT Telecom" value="{{!empty(old('name')) ? old('name') : $company->name}}">
                                        @error('name')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div id="information" class="form-group row">
                                    <label class="col-md-3 col-form-label">Tên viết tắt <span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="short_name" class="form-control" placeholder="Công ty cổ phần FPT Telecom" value="{{!empty(old('short_name')) ? old('short_name') : $company->short_name}}">
                                        @error('short_name')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div id="tax-number" class="form-group row">
                                    <label class="col-md-3 col-form-label">Mã số thuế <span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="tax_number" class="form-control" placeholder="0123456789" value="{{!empty(old('tax_number')) ? old('tax_number') : $company->tax_number}}">
                                        @error('tax_number')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="email" class="form-group row">
                                    <label class="col-md-3 col-form-label">Email doanh nghiệp <span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <input type="email" name="email" class="form-control" placeholder="tienphongcds@tienphong.com" value="{{!empty(old('email')) ? old('email') : $company->email}}">
                                        @error('email')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="phone" class="form-group row">
                                    <label class="col-md-3 col-form-label">Số điện thoại<span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="phone" class="form-control" placeholder="0229634567" value="{{!empty(old('phone')) ? old('phone') : $company->phone}}">
                                        @error('phone')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="" class="form-group row">
                                    <label class="col-md-3 col-form-label">Ngày thành lập <span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <input type="date" name="founded_at" class="form-control" placeholder="" value="{{ date('Y-m-d', strtotime($company->founded))}}">
                                        @error('founded_at')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="" class="form-group row">
                                    <label class="col-md-3 col-form-label">Lĩnh vực <span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <select class="form-control" name="careerGroup">
                                                <option>Lĩnh vực hoạt động</option>
                                                @foreach ($careerGroups as $item)
                                                <option value="{{$item->id}}" @if($item->id == $company->career_group_id) selected @endif>{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                            <i class="fa fa-caret-down"></i>
                                            @error('career_group')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div id="" class="form-group row">
                                    <label class="col-md-3 col-form-label">Ngành tuyển dụng<span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <select class="selectpicker form-control" data-style="select-with-transition" multiple title="Chọn ngành công ty" name="careers[]">
                                                @foreach($career as $item)
                                                <option value="{{$item->id}}" style="font-size: 12px;" @if(!empty($company->careers))
                                                    @foreach (json_decode($company->careers) as $row)
                                                    @if ($row == $item->id)
                                                    selected
                                                    @endif
                                                    @endforeach
                                                    @endif > {{$item->name}}
                                                </option>
                                                @endforeach

                                            </select>

                                            @error('careers')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div id="about" class="row">
                                    <label class="col-md-3 col-form-label">Giới thiệu</label>
                                    <div class="col-md-9">
                                        <textarea class="tinymce-editor-1" name="description" style="height: auto;" placeholder="Description text here">{{!empty(old('description')) ? old('description') :  $company->description}}</textarea>
                                        @error('description')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="website" class="row">
                                    <label class="col-md-3 col-form-label">Website <span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="url" @foreach(json_decode($company->url) as $key => $url)
                                            @if($key == 'url')
                                            value="{{$url}}"
                                            @endif
                                            @endforeach>
                                            @error('url')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Cộng đồng</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text dropdown-label">
                                                        <select class="form-control" disabled>
                                                            <option value="facebook">Facebook</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" name="facebook" @foreach(json_decode($company->url) as $key => $url)
                                                @if($key == 'facebook')
                                                value="{{$url}}"
                                                @endif
                                                @endforeach>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text dropdown-label">
                                                        <select class="form-control" disabled>
                                                            <option value="twitter">Twitter</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" name="twitter" @foreach(json_decode($company->url) as $key => $url)
                                                @if($key == 'twitter')
                                                value="{{$url}}"
                                                @endif
                                                @endforeach>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text dropdown-label">
                                                        <select class="form-control" disabled>
                                                            <option>Linkedin</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" name="linkedin" @foreach(json_decode($company->url) as $key => $url)
                                                @if($key == 'linkedin')
                                                value="{{$url}}"
                                                @endif
                                                @endforeach>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text dropdown-label">
                                                        <select class="form-control" disabled>
                                                            <option>Instagram</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" name="instagram" @foreach(json_decode($company->url) as $key => $url)
                                                @if($key == 'instagram')
                                                value="{{$url}}"
                                                @endif
                                                @endforeach>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="location" class="row">
                                    <label class="col-md-3 col-form-label">Địa chỉ <span class="text-danger">*</span> </label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="address" class="form-control" placeholder="123 Lạc Long Quân" value="{{!empty(old('address')) ? old('address') : $company->address}}">
                                            @error('address')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                @livewire('change-address-custom', ['cityId' => $company->city_id,'districtId' => $company->district_id,'wardId'=>$company->ward_id])
                                <div id="image" class="row padding-bottom-70">
                                    <label class="col-md-3 col-form-label">Hình ảnh</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Logo</div>
                                                </div>
                                                <div class="upload-portfolio-image">
                                                    <div class="update-photo">
                                                        <img class="image" src="{{FILE_URL .$company->avatar}}" alt="">
                                                    </div>

                                                    <div class="file-upload">
                                                        <input type="file" class="file-input" name="avatar" value="{{$company->avatar}}">
                                                        <i data-feather="plus"></i>
                                                    </div>

                                                </div>

                                            </div>
                                            @error('avatar')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Banner</div>
                                                </div>
                                                <div class="upload-portfolio-image">
                                                    <div class="update-photo " style="width: 300px">
                                                        <img class="image w-100" src="{{FILE_URL .$company->banner}}" alt="">
                                                    </div>
                                                    <div class="file-upload" style="width: 300px">
                                                        <input type="file" class="file-input" name="banner" value="{{$company->banner}}">
                                                        <i data-feather="plus"></i>

                                                    </div>
                                                </div>
                                            </div>
                                            @error('banner')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9 offset-md-3">
                                        <div class="form-group mt-0 terms">
                                            <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition">
                                            <label for="radio-4">
                                                <span class="dot"></span> Bạn chấp nhận <a href="#">Các điều khoản</a> và <a href="#">Chính sách bảo mật</a> của chúng tôi.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row padding-bottom-70">
                                    <label class="col-md-3 col-form-label"></label>
                                    <div class="col-md-9">
                                        <button class="button">Gửi Thông Tin</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="post-sidebar">
                        <h5><i data-feather="arrow-down-circle"></i>Tóm tắt</h5>
                        <ul class="sidebar-menu">
                            <li><a href="#information">Thông Tin</a></li>
                            <li><a href="#about">Giới Thiệu</a></li>
                            <li><a href="#website">Website</a></li>
                            <li><a href="#location">Địa Điểm</a></li>
                            <li><a href="#image">Hình Ảnh</a></li>
                        </ul>
                        <div class="signin-option">
                            <p>Doanh nghiệp của bạn đang hoạt động trên hệ thống!</p>
                            <div class="buttons">
                                <a href="#" class="signin">Liên hệ ngay</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection