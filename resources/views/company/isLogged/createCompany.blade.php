@extends('layouts.client_guest')
@section('page-title', 'Doanh nghiệp')
@section('content')
<div class="alice-bg padding-top-70 padding-bottom-70">
    <div class="container no-gliters">
        <div class="row no-gliters">
            <div class="col">
                <div class="post-container">
                    <div class="post-content-wrapper">
                        <form action="{{route('company.initial.company.store')}}" class="job-post-form" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('POST')
                            <div class="basic-info-input">
                                <h4><i data-feather="plus-circle"></i>THÔNG TIN DOANH NGHIỆP</h4>
                                <div id="information" class="form-group row">
                                    <label class="col-md-3 col-form-label">Tên đầy đủ <span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="name" class="form-control" placeholder="Công ty cổ phần FPT Telecom" value="{{ old('name') }}">
                                        @error('name')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div id="information" class="form-group row">
                                    <label class="col-md-3 col-form-label">Tên viết tắt<span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" name="short_name" class="form-control" placeholder="Công ty cổ phần FPT Telecom" value="{{ old('short_name') }}">
                                        @error('short_name')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div id="tax-number" class="form-group row">
                                    <label class="col-md-3 col-form-label">Mã số thuế <span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" name="tax_number" class="form-control" placeholder="0123456789" value="{{ old('tax_number') }}">
                                        @error('tax_number')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="email" class="form-group row">
                                    <label class="col-md-3 col-form-label">Email doanh nghiệp <span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <input type="email" name="email" class="form-control" placeholder="tienphongcds@tienphong.com" value="{{ old('email') }}">
                                        @error('email')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="phone" class="form-group row">
                                    <label class="col-md-3 col-form-label">Số điện thoại<span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" name="phone" class="form-control" placeholder="0229634567" value="{{ old('phone') }}">
                                        @error('phone')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="" class="form-group row">
                                    <label class="col-md-3 col-form-label">Ngày thành lập <span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <input type="date" name="founded_at" class="form-control" placeholder="" value="{{ old('founded_at') }}">
                                        @error('founded_at')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="" class="form-group row">
                                    <label class="col-md-3 col-form-label">Lĩnh vực <span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" name="careerGroup">
                                                <option value="">Lĩnh vực hoạt động</option>
                                                @foreach ($careerGroups as $item)
                                                <option value="{{$item->id}}" @if(old('careerGroup') == $item->id) selected @endif>{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                            <i class="fa fa-caret-down"></i>
                                            @error('careerGroup')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div id="" class="form-group row">
                                    <label class="col-md-3 col-form-label">Ngành có thể tuyển dụng<span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="selectpicker form-control" data-style="select-with-transition" multiple title="Chọn ngành công ty" name="careers[]">
                                                @foreach ($career as $item)
                                                <option value="{{$item->id}}" style="font-size: 12px;" @if(!empty(old('careers'))) @foreach (old('careers') as $row) @if ($row==$item->id)
                                                    selected
                                                    @endif
                                                    @endforeach
                                                    @endif > {{$item->name}}
                                                </option>


                                                @endforeach

                                            </select>

                                            @error('careers')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div id="about" class="row">
                                    <label class="col-md-3 col-form-label">Giới thiệu</label>
                                    <div class="col-md-9">
                                        <textarea class="tinymce-editor-1" name="description" placeholder="Description text here">{{ old('description') }}</textarea>
                                        @error('description')
                                        <span class="text-danger small font-italic">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="website" class="row">
                                    <label class="col-md-3 col-form-label">Website <span class="text-danger">*</span></label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="url" placeholder="doanhnghiep.com.vn" value="{{ old('url') }}">
                                            @error('url')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Cộng đồng</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text dropdown-label">
                                                        <select class="form-control" disabled>
                                                            <option value="facebook">Facebook</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" name="facebook" placeholder="Đường dẫn ..." value="{{ old('facebook') }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text dropdown-label">
                                                        <select class="form-control" disabled>
                                                            <option value="twitter">Twitter</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" name="twitter" placeholder="Đường dẫn ..." value="{{ old('twitter') }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text dropdown-label">
                                                        <select class="form-control" disabled>
                                                            <option>Linkedin</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" name="linkedin" placeholder="Đường dẫn ..." value="{{ old('linkedin') }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text dropdown-label">
                                                        <select class="form-control" disabled>
                                                            <option>Instagram</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" name="instagram" placeholder="Đường dẫn ..." value="{{ old('instagram') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="location" class="row">
                                    <label class="col-md-3 col-form-label">Địa chỉ <span class="text-danger">*</span> </label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="address" class="form-control" placeholder="123 Lạc Long Quân" value="{{ old('address') }}">
                                            @error('address')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                @livewire('change-address-custom', ['cityId' => old('city'),'districtId' => old('district'),'wardId'
                                =>old('ward')])
                                <div id="image" class="row padding-bottom-70">
                                    <label class="col-md-3 col-form-label">Hình ảnh</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Logo</div>
                                                </div>
                                                <div class="upload-portfolio-image">
                                                    <div class="update-photo">
                                                        <img class="image" src="assets/clients/img/portfolio/thumb-1.jpg" alt="">
                                                    </div>

                                                    <div class="file-upload">
                                                        <input type="file" class="file-input" name="avatar">
                                                        <i data-feather="plus"></i>
                                                    </div>

                                                </div>

                                            </div>
                                            @error('avatar')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Banner</div>
                                                </div>
                                                <div class="upload-portfolio-image">
                                                    <div class="update-photo " style="width: 300px">
                                                        <img class="image w-100" src="assets/clients/img/portfolio/thumb-1-banner.jpg" alt="">
                                                    </div>
                                                    <div class="file-upload" style="width: 300px">
                                                        <input type="file" class="file-input" name="banner" value="{{ old('banner') }}">
                                                        <i data-feather="plus"></i>

                                                    </div>
                                                </div>
                                            </div>
                                            @error('banner')
                                            <span class="text-danger small font-italic">{{ $message }}</span>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9 offset-md-3">
                                        <div class="form-group mt-0 terms">
                                            <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition" required>
                                            <label for="radio-4">
                                                <span class="dot"></span> Bạn chấp nhận <a href="#">Các điều khoản</a> và <a href="#">Chính sách bảo mật</a> của chúng tôi.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row padding-bottom-70">
                                    <label class="col-md-3 col-form-label"></label>
                                    <div class="col-md-9">
                                        <button class="button">Gửi Thông Tin</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="post-sidebar">
                        <h5><i data-feather="arrow-down-circle"></i>Tóm tắt</h5>
                        <ul class="sidebar-menu">
                            <li><a href="#information">Thông Tin</a></li>
                            <li><a href="#about">Giới Thiệu</a></li>
                            <li><a href="#website">Website</a></li>
                            <li><a href="#location">Địa Điểm</a></li>
                            <li><a href="#image">Hình Ảnh</a></li>
                        </ul>
                        <div class="signin-option">
                            <p>Doanh nghiệp của bạn đang hoạt động trên hệ thống!</p>
                            <div class="buttons">
                                <a href="#" class="signin">Liên hệ ngay</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection