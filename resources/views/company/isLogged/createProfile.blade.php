@extends('layouts.client_guest')
@section('page-title', 'Doanh nghiệp')

@section('content')
<div class="alice-bg padding-top-70 padding-bottom-70 ">
    <div class="container">
        <div class="dashboard-content-wrapper">
            <form action="{{route('company.initial.profile.store')}}" class="dashboard-form" method="post" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="dashboard-section upload-profile-photo">
                    <div class="update-photo">
                        <img class="image" src="assets/clients/img/avt-default.png" alt="">
                    </div>
                    <div class="file-upload">
                        <input type="file" name="avatar" class="file-input">Thay đổi ảnh đại diện

                    </div>
                </div>
                @error('avatar')
                <p class="text-danger small font-italic mb-3">{{ $message }}</p>
                @enderror
                <div class="dashboard-section basic-info-input">
                    <h4><i data-feather="user-check"></i>Thông tin cá nhân</h4>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Họ và Tên <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="full_name" class="form-control" placeholder="Nguyễn Văn A" value="{{ old('full_name') }}">
                            @error('full_name')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Email cá nhân </label>
                        <div class="col-sm-9">
                            <input type="text" name="email_personal" class="form-control" placeholder="email@gmail.com" value="{{ old('email_personal') }}">
                            @error('email_personal')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Số điện thoại <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="phone" class="form-control" placeholder="0123456789" value="{{ old('phone') }}">
                            @error('phone')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group row">

                        <label class="col-sm-3 col-form-label">Ngày sinh <span class="text-danger">*</span></label>

                        <div class="col-sm-6 col-md-3">
                            <input type="date" name="birthday" class="form-control" placeholder="2020-05-12" value="{{ old('birthday') }}">
                            @error('birthday')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Giới tính <span class="text-danger">*</span></label>
                        <div class="col-3 col-sm-1">
                            <input class="custom-radio" type="radio" id="radio_1" name="gender" value="0" @if(old('gender')==0) checked @endif>
                            <label for="radio_1">
                                <span class="dot"></span> <span class="package-type">Nam</span>
                            </label>
                        </div>
                        <div class="col-3 col-sm-1">
                            <input class="custom-radio" type="radio" id="radio_2" name="gender" value="1" @if(old('gender')==1) checked @endif>
                            <label for="radio_2">
                                <span class="dot"></span> <span class="package-type">Nữ</span>
                            </label>
                        </div>
                        @error('gender')
                        <span class="text-danger small font-italic">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">CCCD/CMND <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="indo" class="form-control" placeholder="113113113" value="{{ old('indo') }}">
                            @error('indo')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Địa chỉ <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="address" placeholder="123 Lạc Long Quân" value="{{ old('address') }}">
                            @error('address')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                    @livewire('change-address-custom', ['cityId' => old('city'),'districtId' => old('district'),'wardId'
                    =>old('ward')])
                    <div class="form-group row padding-top-70">
                        <label class="col-sm-3 col-form-label">Giới thiệu</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description" placeholder="Giới thiệu về bản thân">{{ old('description') }}</textarea>
                            @error('description')
                            <span class="text-danger small font-italic">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                </div>
                <div class="form-group row padding-top-70">
                    <label class="col-md-3 col-form-label"></label>
                    <div class="col-md-9 t">
                        <button class="button ">Cập Nhật Thông Tin</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection