@extends('layouts.client')
@section('page-title', 'Hồ sơ cá nhân')
@section('title', 'Hồ sơ cá nhân')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Tổng quan</a></li>
        <li class="breadcrumb-item active" aria-current="page">Tài khoản cá nhân</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
    <form action="{{route('company.user.profile.update',$user->id)}}" class="dashboard-form" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="dashboard-section upload-profile-photo">
            <div class="update-photo">
                <img src="{{FILE_URL .$user->avatar}}" onerror="this.src='assets/clients/img/avt-default.png'" class="image" alt="">
            </div>
            <div class="file-upload">
                <input type="file" class="file-input" name="avatar">Thay đổi ảnh đại diện
            </div>
           
        </div>
        @error('avatar')
            <span class="text-danger small font-italic">{{ $message }}</span>
            @enderror
        <div class="dashboard-section basic-info-input">
            <h4><i data-feather="user-check"></i>THÔNG TIN CƠ BẢN</h4>

            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Họ và Tên</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control px-3" name="name" value="{{!empty(old('name')) ? old('name') : $user->name}}">
                    @error('name')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Email</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control px-3" name="email" value="{{$user->email}}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Email Cá Nhân</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control px-3" name="email_personal" value="{{!empty(old('email_personal')) ? old('email_personal') : $user->profile->email_personal}}">
                    @error('email_personal')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control px-3" name="phone" value="{{!empty(old('phone')) ? old('phone') : $user->profile->phone}}">
                    @error('phone')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">CMND/CCCD</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control px-3" name="indo" value="{{!empty(old('indo')) ? old('indo') : $user->profile->indo}}">
                    @error('indo')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Ngày sinh</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control px-3" name="birthday" value="{{date('d-m-Y',strtotime($user->profile->birthday))}}">
                    @error('birthday')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Địa chỉ</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control px-3" name="address" value="{{!empty(old('address')) ? old('address') : $user->profile->address}}">
                    @error('address')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            @livewire('change-address-custom', ['cityId' =>$user->profile->city_id,'districtId' =>$user->profile->district_id,'wardId'=>$user->profile->ward_id])
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Giới thiệu</label>
                <div class="col-sm-9">
                    <textarea class="form-control" placeholder="Giới thiệu về bản thân" name="description">{{!empty(old('description')) ? old('description') : $user->profile->description}}</textarea>
                    @error('description')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="dashboard-section basic-info-input">
            <h4><i data-feather="lock"></i>THAY ĐỔI MẬT KHẨU</h4>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Mật Khẩu Cũ</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="password" placeholder="Mật khẩu cũ">
                    @error('password')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>

            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Mật Khẩu Mới</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="password_new" placeholder="Mật khẩu mới">
                    @error('password_new')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>

            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nhập Lại Mật Khẩu</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="password_confirmation" placeholder="Nhập lại mật khẩu">
                    @error('password_confirmation')
                    <span class="text-danger small font-italic">{{ $message }}</span>
                    @enderror
                </div>

            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label"></label>
                <div class="col-sm-9">
                    <button class="button">Lưu Thay Đổi</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection