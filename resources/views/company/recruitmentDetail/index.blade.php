@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link {{Request::has('handle')||empty(Request::all()) ? 'active' : ''}}
               " id="nav-profile-tab" data-toggle="tab" href="#handle" role="tab" aria-controls="nav-profile"
                aria-selected="false">Chờ xử lí
            </a>
            <a class="nav-item nav-link {{Request::has('done') ? 'active' : ''}} " id="doneHandle" data-toggle="tab"
                href="#done-handle" role="tab" aria-controls="nav-profile" aria-selected="false">Đã xử lí</a>
            <a class="nav-item nav-link {{Request::has('interview') ? 'active' : ''}} " id="nav-home-tab"
                data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Phỏng
                vấn</a>
            <a class="nav-item nav-link {{Request::has('cancel') ? 'active' : ''}} " id="nav-home-tab" data-toggle="tab"
                href="#cancel" role="tab" aria-controls="nav-home" aria-selected="true">Từ chối ứng tuyển</a>
            <a class="nav-item nav-link {{Request::has('apporve') ? 'active' : ''}} " id="nav-contact-tab"
                data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Ứng
                tuyển thành công</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade {{Request::has('handle')||empty(Request::all()) ? 'show active' : ''}}" id="handle"
            role="tabpanel" aria-labelledby="nav-profile-tab">
            <div class="manage-candidate-container">
                <div class="manage-job-container">
                    @if (!empty($recruitmentDetailHandle) && count($recruitmentDetailHandle) == 0)
                    <p class="p-3">Chưa có dữ liệu!</p>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Thông tin</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="text-center">Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($recruitmentDetailHandle as $row)
                            <tr>
                                <td class="title">
                                    <div class="thumb">
                                        <img src="{{$row->user->getAvatar()}}"
                                            onerror="this.src='assets/clients/img/avt-default.png'" class="img-fluid"
                                            alt="">
                                    </div>
                                    <div class="body">
                                        <h5>
                                            <a type="button" class="" onclick="infoStudent({{$row->id}})"
                                                data-toggle="modal" data-target="#modal-about-me">
                                                {{$row->user->name}}
                                            </a>
                                        </h5>
                                        <div class="info">
                                            <span class="designation"><a href="#"><i data-feather="check-square"></i>{{
                                                    date('d/m/Y H:i:s', strtotime($row->updated_at))}}</a></span>
                                        </div>
                                        <a
                                            href="{{route('company.recruitment.detail',$row->recruitmentPost->id)}}">{{Str::limit($row->post->title,35)}}</a>
                                    </div>
                                </td>

                                <td class="status text-center">
                                    @if($row->status == RECRUITMENT_WAITING)<p class="text-primary">Chờ duyệt</p>
                                    @elseif($row->status == RECRUITMENT_INTERVIEW_WAITING)<p style="color: coral">Lên
                                        lịch
                                        phỏng vấn
                                    </p>
                                    @elseif($row->status == RECRUITMENT_INTERVIEW)
                                    <p style="color: coral">Đánh giá phỏng vấn
                                    </p>
                                    @else
                                    <p class="text-danger">Chưa xác định</p>
                                    @endif
                                </td>
                                <td class="action text-center">
                                    <a type="button" class="preview" title="Chi tiết"
                                        onclick="infoStudent({{$row->id}})" data-toggle="modal"
                                        data-target="#modal-about-me">
                                        <i data-feather="eye"></i>
                                    </a>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                    <div class="pagination-list text-center">
                        <nav class="navigation pagination">
                            <div class="nav-links">
                                @if ($recruitmentDetailHandle->lastPage() > 1)
                                <a class="prev page-numbers" href="{{ $recruitmentDetailHandle->url(1) }}"><i
                                        class="fas fa-angle-left"></i></a>
                                @for ($i = 1; $i <= $recruitmentDetailHandle->lastPage(); $i++)
                                    @php
                                    $half_total_links = floor(5 / 2);
                                    $from = $recruitmentDetailHandle->currentPage() - $half_total_links;
                                    $to = $recruitmentDetailHandle->currentPage() + $half_total_links;
                                    if ($recruitmentDetailHandle->currentPage() < $half_total_links) { $to
                                        +=$half_total_links - $recruitmentDetailHandle->currentPage();
                                        }
                                        if ($recruitmentDetailHandle->lastPage() -
                                        $recruitmentDetailHandle->currentPage() < $half_total_links) { $from
                                            -=$half_total_links - ($recruitmentDetailHandle->lastPage() -
                                            $recruitmentDetailHandle->currentPage()) - 1;
                                            }
                                            @endphp
                                            @if ($from < $i && $i < $to) @if ($recruitmentDetailHandle->currentPage() ==
                                                $i)
                                                <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                                @else
                                                <a class="page-numbers"
                                                    href="{{ $recruitmentDetailHandle->url($i) }}">{{ $i
                                                    }}</a>
                                                @endif


                                                @endif
                                                @endfor

                                                <a class="next page-numbers"
                                                    href="{{ $recruitmentDetailHandle->url($recruitmentDetailHandle->lastPage()) }}"><i
                                                        class="fas fa-angle-right"></i></a>
                                                @endif
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade {{Request::has('done') ? 'show active' : ''}}" id="done-handle" role="tabpanel"
            aria-labelledby="doneHandle">
            <div class="manage-candidate-container">
                <div class="manage-job-container">
                    @if (!empty($recruitmentDetailDoneHandle) && count($recruitmentDetailDoneHandle) == 0)
                    <p class="p-3">Chưa có dữ liệu!</p>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Thông tin</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="action text-center">Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($recruitmentDetailDoneHandle as $row)
                            <tr class="candidates-list">
                                <td class="title">
                                    <div class="thumb">
                                        <img src="{{$row->user->getAvatar()}}"
                                            onerror="this.src='assets/clients/img/avt-default.png'" class="img-fluid"
                                            alt="">
                                    </div>
                                    <div class="body">
                                        <h5>
                                            <a type="button" class="" onclick="infoStudent({{$row->id}})"
                                                data-toggle="modal" data-target="#modal-about-me">
                                                {{$row->user->name}}
                                            </a>
                                        </h5>
                                        <div class="info">
                                            <span class="designation"><a href="#"><i data-feather="check-square"></i>{{
                                                    date('d/m/Y H:i:s', strtotime($row->updated_at))}}</a></span>
                                        </div>
                                        <a
                                            href="{{route('company.recruitment.detail',$row->recruitmentPost->id)}}">{{Str::limit($row->post->title,35)}}</a>
                                    </div>
                                </td>

                                <td class="status text-center">
                                    @if($row->status == RECRUITMENT_PENDING)<p class="text-primary">Đợi xác thực</p>
                                    @elseif($row->status == RECRUITMENT_PENDING_APPROVE)<p class="text-warning">Đợi
                                        chấp thuận </p>
                                    @elseif($row->status == RECRUITMENT_INTERVIEW_SUCCESS)<p class="text-warning">Đợi
                                        chấp
                                        thuận</p>
                                    @else
                                    <p class="text-danger">Chưa xác định
                                    </p>
                                    @endif
                                </td>
                                <td class="action text-center">
                                    <a type="button" class="preview" title="Chi tiết"
                                        onclick="infoStudent({{$row->id}})" data-toggle="modal"
                                        data-target="#modal-about-me">
                                        <i data-feather="eye"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                    <div class="pagination-list text-center">
                        <nav class="navigation pagination">
                            <div class="nav-links">
                                @if ($recruitmentDetailDoneHandle->lastPage() > 1)
                                <a class="prev page-numbers" href="{{ $recruitmentDetailDoneHandle->url(1) }}"><i
                                        class="fas fa-angle-left"></i></a>
                                @for ($i = 1; $i <= $recruitmentDetailDoneHandle->lastPage(); $i++)
                                    @php
                                    $half_total_links = floor(5 / 2);
                                    $from = $recruitmentDetailDoneHandle->currentPage() - $half_total_links;
                                    $to = $recruitmentDetailDoneHandle->currentPage() + $half_total_links;
                                    if ($recruitmentDetailDoneHandle->currentPage() < $half_total_links) { $to
                                        +=$half_total_links - $recruitmentDetailDoneHandle->currentPage();
                                        }
                                        if ($recruitmentDetailDoneHandle->lastPage() -
                                        $recruitmentDetailDoneHandle->currentPage() < $half_total_links) { $from
                                            -=$half_total_links - ($recruitmentDetailDoneHandle->lastPage() -
                                            $recruitmentDetailDoneHandle->currentPage()) - 1;
                                            }
                                            @endphp
                                            @if ($from < $i && $i < $to) @if ($recruitmentDetailDoneHandle->
                                                currentPage() == $i)
                                                <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                                @else
                                                <a class="page-numbers"
                                                    href="{{ $recruitmentDetailDoneHandle->url($i) }}">{{ $i
                                                    }}</a>
                                                @endif


                                                @endif
                                                @endfor

                                                <a class="next page-numbers"
                                                    href="{{ $recruitmentDetailDoneHandle->url($recruitmentDetailDoneHandle->lastPage()) }}"><i
                                                        class="fas fa-angle-right"></i></a>
                                                @endif
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade {{Request::has('interview') ? 'show active' : ''}}" id="nav-home" role="tabpanel"
            aria-labelledby="nav-home-tab">
            <div class="manage-candidate-container">
                <div class="manage-job-container">
                    @if (!empty($recruitmentDetailInterview) && count($recruitmentDetailInterview) == 0)
                    <p class="p-3">Chưa có dữ liệu!</p>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Thông tin</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="action text-center">Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($recruitmentDetailInterview as $row)
                            <tr class="candidates-list">
                                <td class="title">
                                    <div class="thumb">
                                        <img src="{{$row->user->getAvatar()}}"
                                            onerror="this.src='assets/clients/img/avt-default.png'" class="img-fluid"
                                            alt="">
                                    </div>
                                    <div class="body">
                                        <h5> <a type="button" class="" onclick="infoStudent({{$row->id}})"
                                                data-toggle="modal"
                                                data-target="#modal-about-me">{{$row->user->name}}</a>
                                        </h5>
                                        <div class="info">
                                            <span class="designation"><a href="#"><i
                                                        data-feather="check-square"></i>{{$row->updated_at}}</a></span>
                                        </div>
                                        <a
                                            href="{{route('company.recruitment.detail',$row->recruitmentPost->id)}}">{{Str::limit($row->post->title,35)}}</a>
                                    </div>
                                </td>

                                <td class="status text-center">
                                    @if($row->status == RECRUITMENT_INTERVIEW)<p class="text-warning">Chờ phỏng vấn</p>
                                    @else
                                    <p class="text-success">Thành công
                                    </p>
                                    @endif
                                </td>
                                <td class="action text-center">
                                    <a type="button" class="preview" title="Chi tiết"
                                        onclick="infoStudent({{$row->id}})" data-toggle="modal"
                                        data-target="#modal-about-me">
                                        <i data-feather="eye"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                    <div class="pagination-list text-center">
                        <nav class="navigation pagination">
                            <div class="nav-links">
                                @if ($recruitmentDetailInterview->lastPage() > 1)
                                <a class="prev page-numbers" href="{{ $recruitmentDetailInterview->url(1) }}"><i
                                        class="fas fa-angle-left"></i></a>
                                @for ($i = 1; $i <= $recruitmentDetailInterview->lastPage(); $i++)
                                    @php
                                    $half_total_links = floor(5 / 2);
                                    $from = $recruitmentDetailInterview->currentPage() - $half_total_links;
                                    $to = $recruitmentDetailInterview->currentPage() + $half_total_links;
                                    if ($recruitmentDetailInterview->currentPage() < $half_total_links) { $to
                                        +=$half_total_links - $recruitmentDetailInterview->currentPage();
                                        }
                                        if ($recruitmentDetailInterview->lastPage() -
                                        $recruitmentDetailInterview->currentPage() < $half_total_links) { $from
                                            -=$half_total_links - ($recruitmentDetailInterview->lastPage() -
                                            $recruitmentDetailInterview->currentPage()) - 1;
                                            }
                                            @endphp
                                            @if ($from < $i && $i < $to) @if ($recruitmentDetailInterview->currentPage()
                                                == $i)
                                                <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                                @else
                                                <a class="page-numbers"
                                                    href="{{ $recruitmentDetailInterview->url($i) }}">{{ $i
                                                    }}</a>
                                                @endif


                                                @endif
                                                @endfor

                                                <a class="next page-numbers"
                                                    href="{{ $recruitmentDetailInterview->url($recruitmentDetailInterview->lastPage()) }}"><i
                                                        class="fas fa-angle-right"></i></a>
                                                @endif
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade {{Request::has('cancel') ? 'show active' : ''}}" id="cancel" role="tabpanel"
            aria-labelledby="nav-home-tab">
            <div class="manage-candidate-container">
                <div class="manage-job-container">
                    @if (!empty($recruitmentDetailCancel) && count($recruitmentDetailCancel) == 0)
                    <p class="p-3">Chưa có dữ liệu!</p>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Thông tin</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="action text-center">Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($recruitmentDetailCancel as $row)
                            <tr class="candidates-list">
                                <td class="title">
                                    <div class="thumb">
                                        <img src="{{$row->user->getAvatar()}}"
                                            onerror="this.src='assets/clients/img/avt-default.png'" class="img-fluid"
                                            alt="">
                                    </div>
                                    <div class="body">
                                        <h5> @if($row->status == RECRUITMENT_CANCEL) <a type="button" class=""
                                                onclick="infoStudent({{$row->id}})" data-toggle="modal"
                                                data-target="#modal-about-me">{{$row->user->name}} </a>
                                            @else
                                            {{$row->user->name}}
                                            @endif
                                        </h5>
                                        <div class="info">
                                            <span class="designation"><a href="#"><i
                                                        data-feather="check-square"></i>{{$row->updated_at}}</a></span>
                                        </div>
                                        <a
                                            href="{{route('company.recruitment.detail',$row->recruitmentPost->id)}}">{{Str::limit($row->post->title,35)}}</a>
                                    </div>
                                </td>
                                <td class="status text-center">
                                    @if($row->status == RECRUITMENT_CANCEL) <p class="text-danger">Sinh viên hủy phỏng
                                        vấn</p>
                                    @else
                                    Từ chối sinh viên
                                    @endif
                                </td>
                                <td class="action text-center">
                                    @if($row->status == RECRUITMENT_CANCEL) <a type="button" class="preview"
                                        title="Chi tiết" onclick="infoStudent({{$row->id}})" data-toggle="modal"
                                        data-target="#modal-about-me">
                                        <i data-feather="eye"></i>
                                    </a>
                                    @else
                                    <i class="status">không có</i>
                                    @endif
                                </td>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                    <div class="pagination-list text-center">
                        <nav class="navigation pagination">
                            <div class="nav-links">
                                @if ($recruitmentDetailCancel->lastPage() > 1)
                                <a class="prev page-numbers" href="{{ $recruitmentDetailCancel->url(1) }}"><i
                                        class="fas fa-angle-left"></i></a>
                                @for ($i = 1; $i <= $recruitmentDetailCancel->lastPage(); $i++)
                                    @php
                                    $half_total_links = floor(5 / 2);
                                    $from = $recruitmentDetailCancel->currentPage() - $half_total_links;
                                    $to = $recruitmentDetailCancel->currentPage() + $half_total_links;
                                    if ($recruitmentDetailCancel->currentPage() < $half_total_links) { $to
                                        +=$half_total_links - $recruitmentDetailCancel->currentPage();
                                        }
                                        if ($recruitmentDetailCancel->lastPage() -
                                        $recruitmentDetailCancel->currentPage() < $half_total_links) { $from
                                            -=$half_total_links - ($recruitmentDetailCancel->lastPage() -
                                            $recruitmentDetailCancel->currentPage()) - 1;
                                            }
                                            @endphp
                                            @if ($from < $i && $i < $to) @if ($recruitmentDetailCancel->currentPage() ==
                                                $i)
                                                <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                                @else
                                                <a class="page-numbers"
                                                    href="{{ $recruitmentDetailCancel->url($i) }}">{{ $i
                                                    }}</a>
                                                @endif


                                                @endif
                                                @endfor

                                                <a class="next page-numbers"
                                                    href="{{ $recruitmentDetailCancel->url($recruitmentDetailCancel->lastPage()) }}"><i
                                                        class="fas fa-angle-right"></i></a>
                                                @endif
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade {{Request::has('apporve') ? 'show active' : ''}}" id="nav-contact" role="tabpanel"
            aria-labelledby="nav-contact-tab">
            <div class="manage-candidate-container">
                <div class="manage-job-container">
                    @if (!empty($recruitmentDetailApprove) && count($recruitmentDetailApprove) == 0)
                    <p class="p-3">Chưa có dữ liệu!</p>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Thông tin</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="action text-center">Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($recruitmentDetailApprove as $row)
                            <tr class="candidates-list">
                                <td class="title">
                                    <div class="thumb">
                                        <img src="{{$row->user->getAvatar()}}"
                                            onerror="this.src='assets/clients/img/avt-default.png'" class="img-fluid"
                                            alt="">
                                    </div>
                                    <div class="body">
                                        <h5>{{$row->user->name}}
                                        </h5>
                                        <div class="info">
                                            <span class="designation"><a href="#"><i
                                                        data-feather="check-square"></i>{{$row->updated_at}}</a></span>
                                        </div>
                                        <a
                                            href="{{route('company.recruitment.detail',$row->recruitmentPost->id)}}">{{Str::limit($row->post->title,35)}}</a>
                                    </div>
                                </td>
                                <td class="status text-center">
                                    @if($row->status == RECRUITMENT_APPROVED)<p class="text-success">Hoàn tất</p>
                                    @endif</td>
                                <td class="action text-center">
                                    <a class="preview" title="Chi tiết"
                                        href="{{ route('company.intern-student.index') }}">
                                        <i data-feather="eye"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                    <div class="pagination-list text-center">
                        <nav class="navigation pagination">
                            <div class="nav-links">
                                @if ($recruitmentDetailApprove->lastPage() > 1)
                                <a class="prev page-numbers" href="{{ $recruitmentDetailApprove->url(1) }}"><i
                                        class="fas fa-angle-left"></i></a>
                                @for ($i = 1; $i <= $recruitmentDetailApprove->lastPage(); $i++)
                                    @php
                                    $half_total_links = floor(5 / 2);
                                    $from = $recruitmentDetailApprove->currentPage() - $half_total_links;
                                    $to = $recruitmentDetailApprove->currentPage() + $half_total_links;
                                    if ($recruitmentDetailApprove->currentPage() < $half_total_links) { $to
                                        +=$half_total_links - $recruitmentDetailApprove->currentPage();
                                        }
                                        if ($recruitmentDetailApprove->lastPage() -
                                        $recruitmentDetailApprove->currentPage() < $half_total_links) { $from
                                            -=$half_total_links - ($recruitmentDetailApprove->lastPage() -
                                            $recruitmentDetailApprove->currentPage()) - 1;
                                            }
                                            @endphp
                                            @if ($from < $i && $i < $to) @if ($recruitmentDetailApprove->currentPage()
                                                == $i)
                                                <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                                @else
                                                <a class="page-numbers"
                                                    href="{{ $recruitmentDetailApprove->url($i) }}">{{ $i
                                                    }}</a>
                                                @endif
                                                @endif
                                                @endfor
                                                <a class="next page-numbers"
                                                    href="{{ $recruitmentDetailApprove->url($recruitmentDetailApprove->lastPage()) }}"><i
                                                        class="fas fa-angle-right"></i></a>
                                                @endif
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-details details-section dashboard-section">
        <div class="modal fade" id="modal-about-me" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header px-5 bg-light">
                        <h5 class="modal-title "><i class="mdi mdi-file-document-box-multiple"></i>Thông Tin Ứng Tuyển
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body pt-3">
                        <div class="content">
                            <form id="updateStatus">

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script src="/assets/libs/moment/moment.min.js"></script>
<script>
    function infoStudent(id) {
            document.getElementById('updateStatus').innerHTML = `Đang tải dữ liệu`;
            $.ajax({
                url: '{{route('ajax.recruitment')}}',
                type: 'post',
                dataType: 'json',
                data: {
                    _token: '{{csrf_token()}}',
                    id: id,
                },
                error: function(response) {
                    document.getElementById('updateStatus').innerHTML = response['responseText'];
                }
            });
        }

        $( "#updateStatus" ).submit(function( event ) {
            event.preventDefault();
            let data = $('form#updateStatus').serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});
            $('#modal-about-me').modal('hide');
            getLoading();
            $.ajax({
                url: '{{route('ajax.recruitment.status')}}',
                type: 'put',
                dataType: 'json',
                data: {
                    _token: '{{csrf_token()}}',
                    data: data
                },
                success: function(response) {
                   if(response['error']) {
                    error_interview_start = document.getElementById('error_interview_start');
                    interview_end = document.getElementById('error_interview_end');
                    type_interview = document.getElementById('error_type_interview');
                    url = document.getElementById('error_url');
                    id_url = document.getElementById('error_id_url');
                    pass_url = document.getElementById('error_pass_url');
                    address = document.getElementById('error_address');
                    error_interview_start.innerHTML=``;
                    interview_end.innerHTML=``;
                    type_interview.innerHTML=``;
                    url.innerHTML=``;
                    id_url.innerHTML='';
                    pass_url.innerHTML='';
                    address.innerHTML='';
                       if(response['error']['interview_start']){
                        error_interview_start.innerHTML = `<span class="text-danger small font-italic">${response['error']['interview_start']}</span>`
                       }
                       if(response['error']['interview_end']){
                        interview_end.innerHTML = `<span class="text-danger small font-italic">${response['error']['interview_end']}</span>`
                       }
                       if(response['error']['type_interview']){
                        type_interview.innerHTML = `<span class="text-danger small font-italic">${response['error']['type_interview']}</span>`
                       }
                       if(response['error']['url']){
                        url.innerHTML = `<span class="text-danger small font-italic">${response['error']['url']}</span>`
                       }
                       if(response['error']['id_url']){
                        id_url.innerHTML = `<span class="text-danger small font-italic">${response['error']['id_url']}</span>`
                       }
                       if(response['error']['pass_url']){
                        pass_url.innerHTML = `<span class="text-danger small font-italic">${response['error']['pass_url']}</span>`
                       }
                       if(response['error']['address']){
                        address.innerHTML = `<span class="text-danger small font-italic">${response['error']['address']}</span>`
                       }
                   }else{
                    removeLoading();
                    swal({
                        title: "Cập nhập thành công!",
                        text: "Bạn vừa thay đổi trạng thái thành công !",
                        buttonsStyling: false,
                        timer: 3000,
                        confirmButtonClass: "btn btn-success",
                        type: "success",
                        confirmButtonText: 'Đồng ý !',
                        }).then(function() {
                            window.location.reload();
                        }).catch(swal.noop)
                   }
                },
                error: function(response) {
                    swal({
                        title: "Lỗi hệ thống !",
                        text: "Vui lòng thử lại sau !",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-warning",
                        type: "warning",
                        confirmButtonText: 'Đồng ý !',
                    }).catch(swal.noop)
                }
            });
        });

        
        function address(a) {
            moreInformation = document.getElementById('moreInformation');
            moreInformation.innerHTML = '';
            if(a.value == 'meet'){
            moreInformation.innerHTML=`
                    <div class="col-md-12">
                            <div class="form-group">
                              <input type="text" class="form-control" name="url" placeholder="Chọn đường dẫn">
                              <div id="error_url"></div>
                            </div>
                          </div>
                        `;
                    }else if(a.value == 'zoom'){
                            moreInformation.innerHTML=`
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <input type="text" class="form-control"  name="id_url" placeholder="ID cuộc họp">
                                            <div id="error_id_url"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <input type="text" class="form-control" name="pass_url" placeholder="Mật khẩu">
                                            <div id="error_pass_url"></div>
                                            </div>
                                        </div>
                                        `;
                    }else if(a.value == 'offline'){
                            moreInformation.innerHTML=`
                            <div class="col-md-12">
                                            <div class="form-group">
                                            <input type="text" class="form-control" name="address" placeholder="Chọn địa điểm">
                                            <div id="error_address"></div>
                                            </div>
                                        </div>
                                        `;
                                    }
   }
   function showReason(i){
    reason = document.getElementById('reason_fail');
            if(i==1){
                    reason.innerHTML=`<input class="form-control" type="text" id="" name="reason" value=""
                         placeholder="lý do từ chối (nếu có)">`;
            }else{
                    reason.innerHTML=``;
            }
        }
    function showinterviewagian(){
        let showinterviewagian = document.getElementById('showinterviewagian');
        showinterviewagian.innerHTML = `<div class="row col-md-9">
        <label class="col-md-4 col-form-label">Thời gian bắt đầu</label>
        <div class="col-md-8">
            <div class="form-group">
                <input type="datetime-local" name="interview_start" class="form-control">
                <div id="error_interview_start"></div>
            </div>
        </div>
        <label class="col-md-4 col-form-label">Thời gian kêt thúc</label>
        <div class="col-md-8">
            <div class="form-group">
                <input type="datetime-local" name="interview_end" class="form-control">
                <div id="error_interview_end"></div>
            </div>
        </div>
        <label class="col-md-4 col-form-label">Chọn hình thức</label>
        <div class="form-group col-md-8">
            <select class="form-control" name="type_interview" id="selectBox" onchange="address(this);">
                <option value="">Chọn hình thức</option>
                <option value="meet">Google Meet</option>
                <option value="zoom">Zoom</option>
                <option value="offline">Trực tiếp</option>
            </select>
            <div id="error_type_interview"></div>
        </div>
        <div id="moreInformation" class="col-md-12 row">
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <input type="text" class="form-control" name="infor" placeholder="Thông tin thêm">
            </div>
        </div>

    </div>`;
    submitinterviewagain = document.getElementById('submitinterviewagain');
    submitinterviewagain.hidden=false;
}
</script>

@endpush