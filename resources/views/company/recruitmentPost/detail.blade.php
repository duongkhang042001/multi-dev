@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
    </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
    <div class="job-title-and-info">
        <div class="title">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-2">
                        <img src="{{Auth::user()->getFile(Auth::user()->company->avatar)}}" class="w-100">
                    </div>
                    <div class="col-sm-10">
                        <div class="row ml-0">
                            <h5>{{$recruitmentPost->post->title}}</h5>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="row mt-3">
                                    <div class="col-sm-4">
                                        <p>Hiện thị</p>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="switchToggle">
                                            <input type="checkbox" id="switch{{$recruitmentPost->post->id}}" onclick="isActive('{{$recruitmentPost->post->id}}','RECRUITMENT_SHOW_COMPANY')" @if($recruitmentPost->post->is_show == 1) checked @endif>
                                            <label for="switch{{$recruitmentPost->post->id}}"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="row mt-3">
                                    <div class="col-sm-6">
                                        <p>Ứng tuyển</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="switchToggle">
                                            <input type="checkbox" id="switch{{$recruitmentPost->id}}" onclick="isActive('{{$recruitmentPost->id}}','RECRUITMENT_ACTIVE_COMPANY')" @if($recruitmentPost->is_active == 1) checked @endif>
                                            <label for="switch{{$recruitmentPost->id}}"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6">
            <div class="call-to-action-box candidate-box">
                <div class="icon">
                    <img src="assets/images/2.png" alt="" width="120">
                </div>
                <span>Số lượng ứng tuyển</span>
                <h3>{{$countUser}}</h3>
                <a href="#" data-toggle="modal" data-target="#modal-about-me">Chi tiết ứng tuyển<i class="fas fa-arrow-right"></i></a>
            </div>
            <div class="about-details details-section dashboard-section">
                <div class="modal fade" id="modal-about-me" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <h4 class="mt-4 ml-3"><i data-feather="align-left"></i> Danh sách ứng tuyển</h4>
                            <div class="modal-body">
                                <div class="content">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Họ và tên</th>
                                                <th>Thời gian ứng tuyển</th>
                                                <th class="action">Chức năng</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($listUser as $item)
                                            <tr class="job-items">
                                                <td>
                                                    <a href="#">{{$item->user->name}}</a>
                                                </td>

                                                <td class="deadline">{{date('d/m/Y H:i:s',strtotime($item->created_at))}}</td>
                                                <td class="action">
                                                    <a href="#" class="preview text-info ml-4" title="Chi tiết"><i data-feather="eye"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="call-to-action-box candidate-box">
                <div class="icon">
                    <img src="/assets/clients/img/register-box/1.png" alt="" width="120">
                </div>
                <span>Đã ứng tuyển</span>
                <h3>{{$statusUser}}/{{$recruitmentPost->quantity}}</h3>
                <a href="#" data-toggle="modal" data-target="#exampleModalLong2">Thông tin sinh viên<i class="fas fa-arrow-right"></i></a>
            </div>
            <div class="about-details details-section dashboard-section">
                <div class="modal fade" id="exampleModalLong2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <h4 class="mt-4 ml-3"><i data-feather="align-left"></i> Danh sách đã ứng tuyển thành công</h4>
                            <div class="modal-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Họ và tên</th>
                                            <th>Thời gian</th>
                                            <th class="action">Chức năng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($listUserActive as $item)
                                        <tr class="job-items">
                                            <td>
                                                <a href="#">{{$item->user->name}}</a>
                                            </td>

                                            <td class="deadline">{{date('d/m/Y H:i:s',strtotime($item->updated_at))}}</td>
                                            <td class="action">
                                                <a href="#" class="preview text-info ml-4" title="Chi tiết"><i data-feather="eye"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-details details-section dashboard-section">
        <h4 class="font-weight-bold mt-4"><i data-feather="align-left"></i>Mô tả công việc</h4>
        <p>{!!$recruitmentPost->post->content!!}</p>
        <div class="information-and-contact">
            <div class="row information">
                <div class="col-10">
                    <h4>Chi tiết công việc</h4>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-about-me">
                        <i data-feather="edit-2"></i>
                    </button>
                </div>
                <ul>
                    <li><span>Công việc:</span>{{$recruitmentPost->career->name}}</li>
                    <li><span>Nơi làm việc:</span>{{$recruitmentPost->getAddress()}}</li>
                    <li><span>Hình thức:</span>{{($recruitmentPost->working_form) ? "Sinh viên tự sắp xếp":"Toàn thời gian"}}</li>
                    <li><span>Số lượng tuyển:</span>{{$recruitmentPost->quantity}} người </li>
                    <li><span>Mức lương:</span> {{number_format($recruitmentPost->salary_min)}} - {{number_format($recruitmentPost->salary_max)}}</li>
                    <li><span>Giới tính:</span>
                        @if($recruitmentPost->gender == 0 )
                        Nam
                        @elseif($recruitmentPost->gender == 1)
                        Nữ
                        @else
                        Không xác định 
                        @endif
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal fade" id="modal-about-me" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="content">
                            <form action="{{route('company.recruitment.updateDetail',$recruitmentPost->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <h4><i data-feather="align-left"></i>Chiết tiết công việc</h4>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Hình thức làm việc</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="working_form">
                                            <option value="0" @if ($recruitmentPost->working_form == 0) selected @endif>Toàn thời gian</option>
                                            <option value="1" @if ($recruitmentPost->working_form == 1) selected @endif>Thời gian sinh viên sắp xếp</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nơi làm việc</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="address" value="{{$recruitmentPost->address}}">
                                    </div>
                                </div>
                                @livewire('change-address-custom', ['cityId' => $recruitmentPost->city_id,'districtId' => $recruitmentPost->district_id,'wardId'=>$recruitmentPost->ward_id])
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Mức lương thấp nhất</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="salary_min" value="{{$recruitmentPost->salary_min}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Mức lương thấp nhất</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="salary_max" value="{{$recruitmentPost->salary_max}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="offset-sm-3 col-sm-9">
                                        <div class="buttons">
                                            <button class="primary-bg">Cập nhật</button>
                                            <button class="" data-dismiss="modal">Hủy</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="edication-background details-section dashboard-section">
        <h4 class="font-weight-bold"><i data-feather="book"></i>Yêu cầu công việc</h4>
        <p>{!!$recruitmentPost->requirement!!}</p>
    </div>
    <div class="experience dashboard-section details-section">
        <h4 class="font-weight-bold"><i data-feather="briefcase"></i>Quyền lợi</h4>
        <div class="filter-categories pt-0">
            <ul>
                @foreach ($welfares as $item)
                @if(!empty($recruitmentPost->welfares))
                @foreach (json_decode($recruitmentPost->welfares) as $row)
                @if ($row == $item->id)
                <li><a href="" class="experience-section">{{ $item->name }}</a></li>
                @endif
                @endforeach
                @endif
                @endforeach
            </ul>
        </div>


    </div>

</div>

<!--  Plugin for Sweet Alert -->
<script src="/assets/js/plugins/sweetalert2.js"></script>
<script type="text/javascript">
    function isActive(id, model) {
        $.ajax({
            url: `{{route('ajax.status')}}`,
            type: 'PUT',
            dataType: 'json',
            data: {
                _token: '{{csrf_token()}}',
                id: id,
                model: model,
            },
            success: function(response) {
                if (response) {
                    let text = (model == 'RECRUITMENT_SHOW_COMPANY') ? 'Đã bật hiện thị!' : 'Đã bật ứng tuyển!'
                    swal({
                        title: text,
                        text: "Bạn đã vừa thay đổi trạng thái bài ứng tuyển!",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-success",
                        type: "success"
                    }).catch(swal.noop)
                } else {
                    let text = (model == 'RECRUITMENT_SHOW_COMPANY') ? 'Đã tắt hiện thị!' : 'Đã tắt ứng tuyển!'
                    swal({
                        title: text,
                        text: "Bạn đã vừa thay đổi trạng thái bài ứng tuyển!",
                        icon: 'error',
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-warning",
                        type: 'warning',
                    }).catch(swal.noop)
                }
                if (response['isFull']) {
                    swal({
                        title: "Vui lòng chỉnh số lượng ứng tuyển",
                        text: "Bạn muốn thay đổi trạng thái,chỉnh số lượng ứng tuyển!",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-warning",
                        type: "warning"
                    }).catch(swal.noop).then(() => window.location.reload())
                }
            },
            // showNotification ('top','center','success', 'Thay đổi trạng thái thành công!',1000);
            error: function(response) {
                swal({
                    title: "Lỗi hệ thống!",
                    text: "Đã xảy ra sự cố vui lòng thử lại sau!",
                    icon: 'error',
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-danger",
                    type: 'error',
                }).catch(swal.noop)
            }
        });
    }
</script>
@endsection

@section('script')

@endsection