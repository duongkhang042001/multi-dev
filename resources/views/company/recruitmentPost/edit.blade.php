@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
    <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
  <form action="{{route('company.recruitment.update',$recruitmentPost->id)}}" class="job-post-form" method="POST">
    @csrf
    @method('PUT')
    <div class="basic-info-input">
      <h4><i data-feather="plus-circle"></i>Bài đăng tuyển</h4>
      <div id="job-title" class="form-group row">
        <label class="col-md-3 col-form-label">Tiêu đề đăng tuyển</label>
        <div class="col-md-9">
          <input type="text" class="form-control" name="title" placeholder="Tuyển thực tập sinh k17" value="{{!empty(old('title')) ? old('title') : $recruitmentPost->post->title}}">
          @error('title')
          <span class="text-danger small font-italic">{{ $message }}</span>
          @enderror
        </div>
      </div>
      <div id="job-summery" class="row">
        <label class="col-md-3 col-form-label">Tóm tắt</label>
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <select class="form-control" name="careers">
                  <option>Chọn ngành</option>
                  @foreach($careers as $item)
                  <option value="{{$item->id}}" @if($item->id == $recruitmentPost->career_id) selected @endif >{{$item->name}}</option>
                  @endforeach
                </select>
                <i class="fa fa-caret-down"></i>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control" name="quantity" placeholder="Số lượng tuyển dụng" value="{{!empty(old('quantity')) ? old('quantity') : $recruitmentPost->quantity}}">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Hình thức làm việc</label>
                <select class="form-control" name="working_form">
                  <option value="0" @if ($recruitmentPost->working_form == 0) selected @endif>Toàn thời gian</option>
                  <option value="1" @if ($recruitmentPost->working_form == 1) selected @endif>Thời gian sinh viên sắp xếp</option>
                </select>
                <i class="fa fa-caret-down"></i>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Giới tính</label>
                <select class="form-control" name="gender">
                  <option value="0" @if ($recruitmentPost->gender == 0) selected @endif>Nam</option>
                  <option value="1" @if ($recruitmentPost->gender == 1) selected @endif>Nữ</option>
                  <option value="2" @if ($recruitmentPost->gender == 2) selected @endif>Không yêu cầu giới tính</option>
                </select>
                <i class="fa fa-caret-down"></i>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Mức lương thấp nhất</label>
                <input type="text" class="form-control" name="salary_min" placeholder="800000" value="{{!empty(old('salary_min')) ? old('salary_min') : $recruitmentPost->salary_min}}">
                @error('salary_min')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Mức lương cao nhất</label>
                <input type="text" class="form-control" name="salary_max" placeholder="150000" value="{{!empty(old('salary_max')) ? old('salary_max') : $recruitmentPost->salary_max}}">
                @error('salary_max')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group datepicker">
                <label>Ngày bắt đầu</label>
                <input type="date" class="form-control" name="date_start" value="{{!empty(old('data_start')) ? old('data_satart') : date('Y-m-d', strtotime($recruitmentPost->date_start)) }}">
                @error('data_start')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group datepicker">
                <label>Ngày kết thúc</label>
                <input type="date" class="form-control" name="date_end" value="{{!empty(old('data_end')) ? old('data_end') : date('Y-m-d', strtotime($recruitmentPost->date_end)) }}">
                @error('data_end')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="job-description" class="row">
        <label class="col-md-3 col-form-label">Phúc lợi</label>
        <div class="col-md-9">
          <select class="selectpicker form-control" data-style="select-with-transition" multiple title="" name="welfares[]">
            @foreach($welfares as $item)
            <option value="{{$item->id}}" style="font-size: 12px;" @if(!empty($recruitmentPost->welfares))
              @foreach (json_decode($recruitmentPost->welfares) as $row)
              @if ($row == $item->id)
              selected
              @endif
              @endforeach
              @endif > {{$item->name}}
            </option>
            @endforeach
          </select>
        </div>
      </div>
      <div id="job-description" class="row">
        <label class="col-md-3 col-form-label">Mô tả bài đăng tuyển</label>
        <div class="col-md-9">
          <textarea id="mytextarea" class="tinymce-editor tinymce-editor-1" name="short_description">{{!empty(old('short_description')) ? old('short_description') : $recruitmentPost->post->short_description }}</textarea>
          @error('short_description')
          <span class="text-danger small font-italic">{{ $message }}</span>
          @enderror
        </div>
      </div>
      <div id="job-description" class="row">
        <label class="col-md-3 col-form-label">Nội dung bài đăng tuyển</label>
        <div class="col-md-9">
          <textarea id="mytextarea" class="tinymce-editor tinymce-editor-1" name="content">{{!empty(old('content')) ? old('content') : $recruitmentPost->post->content}}</textarea>
          @error('content')
          <span class="text-danger small font-italic">{{ $message }}</span>
          @enderror
        </div>
      </div>
      <div id="job-description" class="row">
        <label class="col-md-3 col-form-label">Yêu cầu bài đăng tuyển</label>
        <div class="col-md-9">
          <textarea id="mytextarea" class="tinymce-editor tinymce-editor-1" name="requirement">{{!empty(old('requirement')) ? old('requirement') : $recruitmentPost->requirement}}</textarea>
          @error('requirement')
          <span class="text-danger small font-italic">{{ $message }}</span>
          @enderror
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 col-form-label"></label>
        <div class="col-md-9">
          <button class="button">Chỉnh bài đăng tuyển</button>
        </div>
      </div>
    </div>
  </form>
</div>


@endsection