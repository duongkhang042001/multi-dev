@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
    <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="dashboard-content-wrapper">
  <form action="{{route('company.recruitment.store')}}" class="job-post-form" method="post">
    @csrf
    @method('POST')
    <div class="basic-info-input">
      <h4><i data-feather="plus-circle"></i>Bài đăng tuyển</h4>
      <div id="job-title" class="form-group row">
        <label class="col-md-3 col-form-label">Tiêu đề đăng tuyển <span class="text-danger">*</span></label>
        <div class="col-md-9">
          <input type="text" class="form-control" name="title" placeholder="Tiêu đề" value="{{ old('title') }}">
          @error('title')
          <span class="text-danger small font-italic">{{ $message }}</span>
          @enderror
        </div>

      </div>

      <div id="job-summery" class="row">
        <label class="col-md-3 col-form-label">Tóm tắt <span class="text-danger">*</span></label>
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <select class="form-control" name="careers">
                  <option value="">Chọn ngành</option>
                  @foreach($careers as $item)
                  <option value="{{$item->id}}" @if(old('careers')==$item->id) selected @endif>{{$item->name}}</option>
                  @endforeach
                </select>
                <i class="fa fa-caret-down"></i>
                @error('careers')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control" name="quantity" placeholder="Số lượng tuyển dụng" value="{{ old('quantity') }}">
                @error('quantity')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Hình thức làm việc</label>
                <select class="form-control" name="working_form" value="{{ old('working_form') }}">
                  <option value="0">Toàn thời gian</option>
                  <option value="1">Thời gian sinh viên sắp xếp</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Giới tính</label>
                <select class="form-control" name="gender">
                  <option value="0">Nam</option>
                  <option value="1">Nữ</option>
                  <option value="-1">Không yêu cầu giới tính</option>
                </select>
                <i class="fa fa-caret-down"></i>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <input class="custom-radio" type="checkbox" id="radio-5" name="check" value="0" onclick="myFunction1()" checked>
                <label for="radio-5">
                  <span class="dot"></span><label class="col-form-label">Mức lương thỏa thuận</label>
                </label>
              </div>
              <div id="textid" class="d-none">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Mức lương thấp nhất</label>
                      <input type="text" class="form-control" name="salary_min" placeholder="00000" value="{{ old('salary_min') }}">
                      @error('salary_min')
                      <span class="text-danger small font-italic">{{ $message }}</span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Mức lương cao nhất</label>
                      <input type="text" class="form-control" name="salary_max" placeholder="0000" value="{{ old('salary_max') }}">
                      @error('salary_max')
                      <span class="text-danger small font-italic">{{ $message }}</span>
                      @enderror
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>Ngày bắt đầu</label>
                <input type="date" class="form-control" name="date_start" value="{{ old('date_start') }}">
                @error('date_start')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Ngày kết thúc</label>
                <input type="date" class="form-control" name="date_end" value="{{ old('date_end') }}">
                @error('date_end')
                <span class="text-danger small font-italic">{{ $message }}</span>
                @enderror
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <label class="col-md-3 col-form-label">Phúc lợi</label>
        <div class="col-md-9">
          <select class="selectpicker form-control" data-style="select-with-transition" multiple title="Chọn phúc lợi" name="welfares[]">
            @foreach($welfares as $item)
            <option value="{{$item->id}}" style="font-size: 14px;" @if(!empty(old('welfares'))) @foreach (old('welfares') as $row) @if ($row==$item->id)
              selected
              @endif
              @endforeach
              @endif>{{$item->name}}</option>
            @endforeach
          </select>
          @error('welfares')
          <span class="text-danger small font-italic">{{ $message }}</span>
          @enderror
        </div>
      </div>
      <div id="job-description" class="row mt-2">
        <label class="col-md-3 col-form-label">Mô tả bài đăng tuyển <span class="text-danger">*</span></label>
        <div class="col-md-9">
          <textarea id="mytextarea" class="tinymce-editor tinymce-editor-1" name="short_description">{{old('short_description')}}</textarea>
          @error('short_description')
          <span class="text-danger small font-italic">{{ $message }}</span>
          @enderror
        </div>
      </div>
      <div id="job-description" class="row">
        <label class="col-md-3 col-form-label">Nội dung bài đăng tuyển <span class="text-danger">*</span></label>
        <div class="col-md-9">
          <textarea id="mytextarea" class="tinymce-editor tinymce-editor-1" name="content">{{old('content')}}</textarea>
          @error('content')
          <span class="text-danger small font-italic">{{ $message }}</span>
          @enderror
        </div>
      </div>
      <div id="job-description" class="row">
        <label class="col-md-3 col-form-label">Yêu cầu bài đăng tuyển <span class="text-danger">*</span></label>
        <div class="col-md-9">
          <textarea id="mytextarea" class="tinymce-editor tinymce-editor-1" name="requirement">{{old('requirement')}}</textarea>
          @error('requirement')
          <span class="text-danger small font-italic">{{ $message }}</span>
          @enderror
        </div>
      </div>
      <div class="more-option">
        <div class="mt-0 terms">
          <input class="custom-radio" type="checkbox" id="radio-4" name="check" value="1" onclick="myFunction()" checked>
          <label for="radio-4">
            <span class="dot"></span><label class="col-form-label">Nơi làm việc mặc định của công ty</label>
          </label>
        </div>
      </div>
      <div id="text" class="d-none mt-2">
        <div id="job-title" class="form-group row">
          <label class="col-md-3 col-form-label">Địa chỉ cụ thể <span class="text-danger">*</span></label>
          <div class="col-md-9">
            <input type="text" class="form-control" name="address" placeholder="123 bình chánh">
            @error('address')
            <span class="text-danger small font-italic">{{ $message }}</span>
            @enderror
          </div>
        </div>
        @livewire('change-address-custom', ['cityId' => old('city'),'districtId' => old('district'),'wardId'=>old('ward')])

      </div>
      <div class="form-group row mt-3">
        <label class="col-md-3 col-form-label"></label>
        <div class="col-md-9">
          <button class="button">Tạo bài đăng tuyển</button>
        </div>
      </div>
    </div>
  </form>
</div>

<script>
  function myFunction() {
    let form = ``;
    var checkBox = document.getElementById("radio-4");
    var text = document.getElementById("text");
    if (checkBox.checked == false) {
      text.classList.remove('d-none');
      text.classList.add('d-block');
    } else {
      text.classList.remove('d-block');
      text.classList.add('d-none');
    }
  }

  function myFunction1() {
    let form = ``;
    var checkBox = document.getElementById("radio-5");
    var text = document.getElementById("textid");
    if (checkBox.checked == false) {
      text.classList.remove('d-none');
      text.classList.add('d-block');
    } else {
      text.classList.remove('d-block');
      text.classList.add('d-none');
    }
  }
</script>
@endsection