@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="dashboard-content-wrapper">
        <div class="manage-job-container">
            <table class="table">
                @if (count($recruitmentPost))
                    <thead>
                        <tr>
                            <th>Tiêu đề</th>
                            <th>Lượt xem</th>
                            <th class="text-center">Hiện thị</th>
                            <th class="text-center">Ứng tuyển</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="action">Chức năng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($recruitmentPost as $item)
                            <tr class="job-items">
                                <td class="title">
                                    <h5><a
                                            href="{{ route('company.recruitment.detail', $item->id) }}">{{ Str::limit($item->post->title, 30) }}</a>
                                    </h5>
                                    <div class="info">
                                        <span class="office-location"><a href="#"><i
                                                    data-feather="calendar"></i>{{ date('d/m/Y', strtotime($item->date_start)) }}
                                                - {{ date('d/m/Y', strtotime($item->date_end)) }}</a></span>
                                        <!-- <span class="job-type full-time"><a href="#"><i class="fas fa-user"></i> {{ $item->quantity }}</a></span> -->
                                    </div>
                                </td>
                                <td>{{ number_format($item->post->view) }}</td>
                                <td class="text-center">
                                    <div class="switchToggle" style="margin-left:20px;">
                                        <input type="checkbox" id="switch{{ $item->post->id }}"
                                            onclick="isActive('{{ $item->post->id }}','RECRUITMENT_SHOW_COMPANY')"
                                            @if ($item->post->is_show == 1) checked @endif>
                                        <label for="switch{{ $item->post->id }}"></label>
                                    </div>
                                </td>

                                <td>
                                    <div class="switchToggle" style="margin-left:30px;">
                                        <input type="checkbox" id="switch{{ $item->id }}"
                                            onclick="isActive('{{ $item->id }}','RECRUITMENT_ACTIVE_COMPANY')"
                                            @if ($item->is_active == 1) checked @endif>
                                        <label for="switch{{ $item->id }}"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    @if ($item->is_active == 1)
                                        <p class="text-success">Hoạt động</p>
                                    @elseif($item->is_active == 0)<p class="text-danger">Dừng hoạt động</p>
                                    @endif
                                </td>
                                <td class="action">
                                    <a href="{{ route('company.recruitment.detail', $item->id) }}" class="preview"
                                        title="Chi tiết"><i data-feather="eye"></i></a>
                                    <a href="{{ route('company.recruitment.edit', $item->id) }}" class="edit"
                                        title="Chỉnh sửa"><i data-feather="edit"></i></a>
                                    <a href="javascript: document.getElementById('deletedRow{{ $item->id }}').submit();"
                                        onclick="return confirm('Bạn Có muốn xóa???');" class="remove"
                                        title="xóa"><i data-feather="trash-2"></i></a>
                                    <form action="{{ route('company.recruitment.delete', $item->id) }}" method="POST"
                                        id="deletedRow{{ $item->id }}" class="d-none">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <h5>Bạn chưa có bài đăng tuyển nào!!</h5>
                @endif
                </tbody>
            </table>
            <div class="pagination-list text-center">
                <nav class="navigation pagination">
                    <div class="nav-links">

                        @if ($recruitmentPost->lastPage() > 1)
                            <a class="prev page-numbers" href="{{ $recruitmentPost->url(1) }}"><i
                                    class="fas fa-angle-left"></i></a>
                            @for ($i = 1; $i <= $recruitmentPost->lastPage(); $i++)
                                <?php
                                $half_total_links = floor(5 / 2);
                                $from = $recruitmentPost->currentPage() - $half_total_links;
                                $to = $recruitmentPost->currentPage() + $half_total_links;
                                if ($recruitmentPost->currentPage() < $half_total_links) {
                                    $to += $half_total_links - $recruitmentPost->currentPage();
                                }
                                if ($recruitmentPost->lastPage() - $recruitmentPost->currentPage() < $half_total_links) {
                                    $from -= $half_total_links - ($recruitmentPost->lastPage() - $recruitmentPost->currentPage()) - 1;
                                }
                                ?>
                                @if ($from < $i && $i < $to)
                                    @if ($recruitmentPost->currentPage() == $i)
                                        <span aria-current="page" class="page-numbers current">{{ $i }}</span>
                                    @else
                                        <a class="page-numbers"
                                            href="{{ $recruitmentPost->url($i) }}">{{ $i }}</a>
                                    @endif


                                @endif
                            @endfor

                            <a class="next page-numbers"
                                href="{{ $recruitmentPost->url($recruitmentPost->lastPage()) }}"><i
                                    class="fas fa-angle-right"></i></a>

                        @endif
                    </div>
                </nav>
            </div>
        </div>
    </div>


    <script src="/assets/js/plugins/sweetalert2.js"></script>
    <script type="text/javascript">
        function isActive(id, model) {
            $.ajax({
                url: `{{ route('ajax.status') }}`,
                type: 'PUT',
                dataType: 'json',
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id,
                    model: model,
                },
                success: function(response) {
                    if (response) {
                        let text = (model == 'RECRUITMENT_SHOW_COMPANY') ? 'Đã bật hiện thị!' :
                            'Đã bật ứng tuyển!'
                        swal({
                            title: text,
                            text: "Bạn đã vừa thay đổi trạng thái bài ứng tuyển!",
                            buttonsStyling: false,
                            confirmButtonClass: "btn btn-success",
                            type: "success"
                        }).catch(swal.noop).then(() => window.location.reload())
                    } else {
                        let text = (model == 'RECRUITMENT_SHOW_COMPANY') ? 'Đã tắt hiện thị!' :
                            'Đã tắt ứng tuyển!'
                        swal({
                            title: text,
                            text: "Bạn đã vừa thay đổi trạng thái bài ứng tuyển!",
                            icon: 'error',
                            buttonsStyling: false,
                            confirmButtonClass: "btn btn-warning",
                            type: 'warning',
                        }).catch(swal.noop).then(() => window.location.reload())
                    }
                },
                error: function(response) {
                    swal({
                        title: "Lỗi hệ thống!",
                        text: "Đã xảy ra sự cố vui lòng thử lại sau!",
                        icon: 'error',
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-danger",
                        type: 'error',
                    }).catch(swal.noop)
                }
            });
        }
    </script>
@endsection
