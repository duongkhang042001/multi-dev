@extends('layouts.client')
@section('page-title', 'Trang chủ')
@section('title', 'Tổng Quan')
@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">Trang quản trị</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="dashboard-content-wrapper">
        <div class="manage-job-container">
            <table class="table">
                @if (count($feedBack))
                    <thead>
                        <tr>
                            <th>Tiêu đề</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Ngày phản hồi</th>
                            <th class="action">Chức năng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($feedBack as $row)
                            <tr class="job-items">
                                <td class="title">
                                    <h5><a
                                            href="{{ route('company.feedBack.show', $row->id) }}">{{ Str::limit($row->title, 30) }}</a>
                                    </h5>

                                </td>
                                <td class="text-center">
                                    @if (!empty($row->content_return))
                                        <p class="text-success">Đã nhận phản hồi</p>
                                    @elseif(empty($row->content_return))<p class="text-danger">Chưa phản hồi</p>
                                    @endif
                                </td>
                                <td class="text-center">{{ date('d/m/Y H:i:s', strtotime($row->created_at)) }}</td>
                                <td class="action">
                                    <a href="{{ route('company.feedBack.show', $row->id) }}" class="preview"
                                        title="Chi tiết"><i data-feather="eye"></i></a>
                                    <a href="javascript: document.getElementById('deletedRow{{ $row->id }}').submit();"
                                        onclick="return confirm('Bạn Có muốn xóa???');" class="remove"
                                        title="xóa"><i data-feather="trash-2"></i></a>
                                    <form action="{{ route('company.feedBack.destroy', $row->id) }}" method="POST"
                                        id="deletedRow{{ $row->id }}" class="d-none">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <p class="p-3">Bạn chưa có phản hồi nào!!</p>
                @endif
                </tbody>
            </table>
        </div>
    </div>


    <script src="/assets/js/plugins/sweetalert2.js"></script>
@endsection
