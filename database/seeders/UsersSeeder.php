<?php


namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class UsersSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('roles')->insert([
            ['id' => 1, 'code' => 'MANAGER', 'name' => 'Trưởng Phòng'],
            ['id' => 2, 'code' => 'STAFF', 'name' => 'Nhân Viên'],
            ['id' => 3, 'code' => 'STUDENT', 'name' => 'Sinh Viên'],
            ['id' => 4, 'code' => 'COMPANY', 'name' => 'Nhân viên doanh nghiệp'],
        ]);
        DB::table('users')->insert([
            ['code'=>'admin','name' => 'admin', "phone" => "0123456789",'email' => 'admin@gmail.com', 'password' => Hash::make('password'), 'role_id' => MANAGER_ROLE_ID],
            ['code'=>'staff1','name' => 'staff1',"phone" => "0123456779", 'email' => 'staff1@gmail.com', 'password' => Hash::make('password'), 'role_id' => STAFF_ROLE_ID],
            ['code'=>'staff2','name' => 'staff2', "phone" => "0123456769",'email' => 'staff2@gmail.com', 'password' => Hash::make('password'), 'role_id' => STAFF_ROLE_ID],
            ['code'=>'student1','name' => 'student1', "phone" => "0123456759",'email' => 'student1@gmail.com', 'password' => Hash::make('password'), 'role_id' => STUDENT_ROLE_ID],
            ['code'=>'student2','name' => 'student2',"phone" => "0123456749", 'email' => 'student2@gmail.com', 'password' => Hash::make('password'), 'role_id' => STUDENT_ROLE_ID],
            ['code'=>'student3','name' => 'student3',"phone" => "0123456739", 'email' => 'student3@gmail.com', 'password' => Hash::make('password'), 'role_id' => STUDENT_ROLE_ID],
            ['code'=>'company_staff','name' => 'company staff', "phone" => "0123456729",'email' => 'company_staff@gmail.com', 'password' => Hash::make('password'), 'role_id' => COMPANY_ROLE_ID],
        ]);
    }
}
