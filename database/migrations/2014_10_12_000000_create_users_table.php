<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->string('provider');
            $table->string('provider_id');
            $table->string('description')->nullable();
            $table->boolean('is_active')->default(1);
            $table->boolean('deleted')->nullable()->default(0);
            $table->datetime('deleted_at')->nullable();
            $table->bigInteger('deleted_by')->nullable();
            $table->datetime('created_at')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->unsignedBigInteger('role_id');
            $table->boolean('is_active')->default(1);
            $table->boolean('deleted')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->bigInteger('deleted_by')->nullable();
            $table->datetime('created_at')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->bigInteger('updated_by')->nullable();
        });

        Schema::table('users', function($table) {
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
