<?php

//--------ROLE-------->
define('MANAGER_ROLE_ID', 1);
define('MANAGER_ROLE_CODE', "MANAGER");
define('STAFF_ROLE_ID', 2);
define('STAFF_ROLE_CODE', "STAFF");
define('STUDENT_ROLE_ID', 3);
define('STUDENT_ROLE_CODE', "STUDENT");
define('COMPANY_ROLE_ID', 4);
define('COMPANY_ROLE_CODE', "COMPANY");

//-------PASSWORD DEFAULT-------->
define('FPT_INTERN_PASSWORD', 12345678);


//-------GENDER------------->
define('MAN', 0);
define('WOMEN', 1);
define('UNKNOWN', -1);


//-------Student Study Status-------->
define('HOCDI', 1);
define('BAOLUU', 2);

//-------Student Study Status-------->
define('STUDENT_STATUS_PASS', 'PASS');
define('STUDENT_STATUS_FAIL', 'FAIL');


//-------Service Status-------->
define('SERVICE_STATUS_WAITING', 'WAITING');
define('SERVICE_STATUS_PENDING', 'PENDING');
define('SERVICE_STATUS_APPROVED', 'APPROVED');
define('SERVICE_STATUS_DENINED', 'DENINED');
define('SERVICE_STATUS_CANCEL', 'CANCEL');

//-------Service TYPE-------->
define('SERVICE_TYPE_TRANSCRIPT', 'TRANSCRIPT');
define('SERVICE_TYPE_INTERNSHIP', 'INTERNSHIP');
define('SERVICE_TYPE_EXEMPTION', 'EXEMPTION');
define('SERVICE_TYPE_INTERNSHIP_CANCEL', 'INTERNSHIP_CANCEL');

//-------Company Register-------->
define('COMPANY_STATUS_WAITING', 'WAITING');
define('COMPANY_STATUS_APPROVED', 'APPROVED');
define('COMPANY_STATUS_DENIED', 'DENIED');


//------Transcript Status------------->
define('TRANSCRIPT_HAVE_NEVER', 0); //chưa gửi yêu cầu
define('TRANSCRIPT_REQUEST', 1); /* Gửi yêu ầu cấp */
define('TRANSCRIPT_RESPONE', 2); /* Đã trả yêu cầu */

//------Notify Type------------->
define('NOTIFY_ALL', 0);
define('NOTIFY_STAFF', 1);
define('NOTIFY_STUDENT', 2);
define('NOTIFY_COMPANY', 3);
define('NOTIFY_COMPANY_TO_STUDENT', 4);
define('NOTIFY_STUDENT_TO_COMPANY', 5);

//------Post Type------------->
define('POST_TYPE_NORMAL_SLUG', 'bai-viet');
define('POST_TYPE_FAQ_SLUG', 'cau-hoi-thuong-gap');
define('POST_TYPE_COMPANY_SLUG', 'bai-viet-doanh-nghiep');
define('POST_TYPE_RECRUITMENT_SLUG', 'bai-dang-tuyen');

//------Career Group------------->
define('INFORMATION-TECHNOLOGY', 'cong-nghe-thong-tin');
define('ENGINEERING', 'co-khi-dien-tu-dong-hoa');
define('TOURISM_AND_HOSPITALITY_MANAGEMENT', 'du-lich-nha-hang-khach-san');
define('ECONOMY', 'kinh-te');

//------UPLOAD FILE------------->
define('FILE_UPLOAD_URL', 'https://fpt-internship.live:8443/upload');
define('FILE_URL', 'https://fpt-internship.live:8443/file/');

//------METHOD SECRET------------->
define('VERIFY_ACCOUNT', 'bGPeCvdlpTIJW9AI');
define('FORGOT_PASSWORD', '0DRuDds6U3aISAGe');

//-----FEEDBACK--------------->
define('FEEDBACK_STUDENT', 1);
define('FEEDBACK_COMPANY', 2);
define('FEEDBACK_ALL', '0');

//-----RECRUITMENT POST DETAILS--------------->
define('RECRUITMENT_WAITING', 'WAITING');
define('RECRUITMENT_PENDING_APPROVE', 'PENDING_APPROVED');
define('RECRUITMENT_PENDING', 'PENDING');
define('RECRUITMENT_INTERVIEW', 'INTERVIEW');
define('RECRUITMENT_INTERVIEW_WAITING', 'INTERVIEW_WAITING');
define('RECRUITMENT_INTERVIEW_SUCCESS', 'INTERVIEW_SUCCESS');
define('RECRUITMENT_CANCEL', 'CANCEL');
define('RECRUITMENT_APPROVED', 'APPROVED');
define('RECRUITMENT_DENIED', 'DENIED');

//-----STATUS EXEMPTION USER-STUDENT-------------//
define('EXEMPTION_CREATED', 'CREATED');
define('EXEMPTION_WAITING', 'WAITING');
define('EXEMPTION_APPROVED', 'APPROVED');
define('EXEMPTION_CANCEL', 'CANCEL');
//-----STATUS USER-STUDENT-------------//
define('USERSTUDENT_PASS', 'PASS');
define('USERSTUDENT_FAIL', 'FAIL');
define('USERSTUDENT_EXEMPTION', 'EXEMPTION');
//-----STATUS INTERNSHIP USER-STUDENT-------------//
define('INTERNSHIP_PASS', 'PASS');
define('INTERNSHIP_FAIL', 'FAIL');
define('INTERNSHIP_WAITING', 'WAITING');
//-----TYPE CV FILE--------
define('CV_SYSTEM', 'system');
define('CV_PERSONAL', 'personal');

//-----REPORTS-------------//
define('REPORT_PENDING', 'PENDING');
define('REPORT_CANCEL', 'CANCEL');
define('REPORT_PASS', 'PASS');
define('REPORT_FAIL', 'FAIL');
define('REPORT_DONE', 'DONE');
define('REPORT_FINISHED', 'FINISHED');

//------DATE------------//
define('DAYOFWEEK', [
    0 => 'Chủ nhật',
    1 => 'Thứ hai',
    2 => 'Thứ ba',
    3 => 'Thứ tư',
    4 => 'Thứ năm',
    5 => 'Thứ sáu',
    6 => 'Thứ bảy',
]);

define('PLAN_RESET_REPORT', [
    '1' => 'Giữ lại báo cáo và nộp vào kỳ thưc tập sau',
    '2' => 'Tìm doanh nghiệp thực tập mới vào kỳ sau',
]);
