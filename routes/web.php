<?php

use App\Http\Controllers\Auth\AuthenticationController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Import\StaffImportController;
use App\Http\Controllers\Import\StudentImportController;
use App\Http\Controllers\Manager\StaffController;
use App\Http\Controllers\Manager\HomeController as HomeManagerController;
use App\Http\Controllers\Manager\NotifyController;
use App\Http\Controllers\Staff\NotifyController as  NotifyStaffController;
use App\Http\Controllers\Manager\SemesterController;
use App\Http\Controllers\Manager\TimelineController;
use App\Http\Controllers\Manager\UserLogController;
use App\Http\Controllers\Staff\StudentController;
use App\Http\Controllers\Staff\CompanyController as StaffCompanyController;
use App\Http\Controllers\Staff\CareersController;
use App\Http\Controllers\Staff\CareerGroupController;
use App\Http\Controllers\Staff\FeedBackController;
use App\Http\Controllers\Staff\HomeController as HomeStaffController;
use App\Http\Controllers\Staff\PostController;
use App\Http\Controllers\Staff\PostTypeController;
use App\Http\Controllers\Staff\RecruitmentController;
use App\Http\Controllers\Staff\WelfareController;
use App\Http\Controllers\Staff\ReportController;
use App\Http\Controllers\Student\HomeController as HomeStudentController;
use App\Http\Controllers\Student\ResumeController;
use App\Http\Controllers\Student\ReportController as StudentReportController;
use App\Http\Controllers\Student\ReportDetailController;
use App\Http\Controllers\Student\StudentController as StudentStudentController;
use App\Http\Controllers\Student\CompanyController as StudentCompanyController;
use App\Http\Controllers\Company\HomeController as HomeCompanyController;
use App\Http\Controllers\Company\UserController as UserCompanyController;
use App\Http\Controllers\Company\CompanyInformationController;
use App\Http\Controllers\Company\FeedBackController as CompanyFeedBackController;
use App\Http\Controllers\Company\RecruitmentDetailController;
use App\Http\Controllers\Company\RecruitmentPostController;
use App\Http\Controllers\Company\ReportRateController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Staff\ExemptionController;
use App\Http\Controllers\Staff\InternshipAgainController;
use App\Http\Controllers\Staff\RequestController;
use App\Http\Controllers\Staff\Transcript;
use App\Http\Controllers\Student\FeedbackController as StudentFeedbackController;
use App\Http\Controllers\Student\InternShipCancel;
use App\Http\Controllers\Student\ServiceExemptionController;
use App\Http\Controllers\Student\ServiceInternshipAgainController;
use App\Http\Controllers\Student\Transcript as StudentTranscript;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/error', function () {
    abort(404);
});
/*------------Home--------------*/
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/notify', [HomeController::class, 'notify'])->name('notify');
Route::view('/contact', 'clients.contact')->name('contact');
Route::view('/about', 'clients.about')->name('about');
Route::get('/recruitment/{slug}', [HomeController::class, 'recruitment'])->name('recruitment');
Route::get('/recruitment', [HomeController::class, 'recruitmentList'])->name('recruitment.list');
Route::any('/search-recruitment', [HomeController::class, 'searchRecruitment'])->name('search-recruitment.list');
Route::any('/list-career/{type}/{slug}', [HomeController::class, 'listCareerRecruitment'])->name('list-career');
Route::any('/list-city/{id}', [HomeController::class, 'listCityRecruitment'])->name('list-city');
Route::get('/company-infomation/{slug}', [HomeController::class, 'company'])->name('company.detail');
Route::post('/feed-back', [HomeController::class, 'feedBackStore'])->name('feed-back.store');
Route::get('/notify-detail/{id}', [HomeController::class, 'notifyDetail'])->name('notifyDetail')->middleware('auth:company,student')->where('id', '[0-9]+');
Route::get('/posts', [HomeController::class, 'post'])->name('post');
Route::get('/post/{slug}', [HomeController::class, 'postDetail'])->name('postDetail');
Route::get('/postFAQ', [HomeController::class, 'postFAQ'])->name('postFAQ');
Route::get('/postCompany', [HomeController::class, 'postCompany'])->name('postCompany');

/*------------Ajax--------------*/
Route::prefix('ajax')->name('ajax.')->group(function () {
    Route::put('/status', [AjaxController::class, 'status'])->name('status')->middleware('auth:manager,staff,company,student');
    Route::post('/recruitment', [AjaxController::class, 'recruitment'])->name('recruitment')->middleware('auth:company');
    Route::put('/recruitment', [AjaxController::class, 'statusRecruitment'])->name('recruitment.status')->middleware('auth:company,student');
    Route::post('/recruitment-detail', [AjaxController::class, 'getRecruitmentDetail'])->name('recruitment-detail')->middleware('auth:student');
    Route::post('/recruitment-pending', [AjaxController::class, 'getRecruitmentDetailPending'])->name('recruitment-pending')->middleware('auth:student');
    Route::get('/company', [AjaxController::class, 'getCompany'])->name('company')->middleware('auth:staff,student');
    Route::get('/student-reset-report', [AjaxController::class, 'getStudentPending'])->name('student.reset-report')->middleware('auth:staff,student');
    Route::post('/upload', [AjaxController::class, 'uploadFile'])->name('upload')->middleware('auth:manager,staff,company,student');
    Route::get('/student', [AjaxController::class, 'getStudent'])->name('student')->middleware('auth:company');
    Route::get('/service', [AjaxController::class, 'getReport'])->name('report')->middleware('auth:staff');
    Route::get('/cancel-report', [AjaxController::class, 'cancelReport'])->name('reportCancel')->middleware('auth:company');
    Route::get('/notifications', [AjaxController::class, 'notification'])->name('notification')->middleware('auth:staff,company,student');
    Route::post('/getnotification', [AjaxController::class, 'getOneNotification'])->name('getOneNotification')->middleware('auth:staff');
    Route::get('/watchedNotification', [AjaxController::class, 'watchedNotification'])->name('watchedNotification')->middleware('auth:staff,company,student');
    Route::post('/notify', [AjaxController::class, 'getNotify'])->name('notify')->middleware('auth:manager,staff');
    Route::post('/notification', [AjaxController::class, 'staffGetNotify'])->name('staffGetNotify')->middleware('auth:staff');
    Route::get('/checkTimeline/{type}', [Controller::class, 'checkTimeLine'])->name('checkTimeline')->middleware('auth:student');
    Route::get('/transcript',[AjaxController::class,'uploadTranscript'])->name('transcript')->middleware('auth:staff');
    Route::get('/request-cancle-student',[AjaxController::class,'requestCancleStudent'])->name('requestCancleStudent')->middleware('auth:student');
});

/*------------Social Login--------------*/
Route::get('/redirect/{provider}', [AuthenticationController::class, 'redirect'])->name('social.redirect');
Route::get('/callback/{provider}', [AuthenticationController::class, 'callback'])->name('social.callback');
Route::get('/email/verify/{guard}/{code}/{method}/{token}', [AuthenticationController::class, 'verified']);
/*------------Dashboard Route--------------*/
Route::prefix('admin')->name('admin.')->group(function () {
    Route::middleware(['guest:manager,staff', 'PreventBackHistory'])->group(function () {
        Route::view('/', 'auth.login');
        Route::view('/login', 'auth.login')->name('login');
        Route::view('/forgot', 'auth.forgotPassword')->name('forgot.show');
        Route::post('/forgot', [AuthenticationController::class, 'forgotPassword'])->name('forgot.send');
        Route::view('/confirm', 'auth.confirmMail')->name('confirm');
        Route::view('/change', 'auth.changePassword')->name('change.show');
        Route::post('/change', [AuthenticationController::class, 'changePassword'])->name('change.update');
        Route::post('/check', [AuthenticationController::class, 'check'])->name('check');
        Route::view('/lock-screen', 'auth.lockScreen')->name('lockScreen.index');
    });
    Route::middleware(['auth:manager,staff', 'PreventBackHistory'])->group(function () {
        Route::post('/lock-screen', [AuthenticationController::class, 'lockScreen'])->name('lockScreen');
    });
});
/*------------Manager Route--------------*/
Route::prefix('manager')->middleware(['auth:manager', 'PreventBackHistory'])->name('manager.')->group(function () {
    Route::get('/home', [HomeManagerController::class, 'index'])->name('home');
    Route::post('/logout', [AuthenticationController::class, 'logout'])->name('logout');
    Route::get('/profile', [AdminController::class, 'profile'])->name('profile');
    Route::get('/editProfile', [AdminController::class, 'editProfile'])->name('editProfile');
    Route::post('/updateProfile', [AdminController::class, 'updateProfile'])->name('updateProfile');
    //Todo MANAGER----->
    Route::resource('/staff', StaffController::class);
    Route::get('/import/staff', [StaffImportController::class,'index'])->name('import.staff.index');
    Route::post('/import/staff', [StaffImportController::class, 'store'])->name('import.staff.store');
    Route::resource('/timeline', TimelineController::class);
    Route::resource('/semester', SemesterController::class);
    Route::resource('/notify', NotifyController::class);
    Route::post('/notify/send/{id}', [NotifyController::class, 'sendNotify'])->name('sendNotify');
    Route::get('/userlog', [UserLogController::class, 'index'])->name('userlog');
    Route::get('/userlog/{id}', [UserLogController::class, 'getLogByUser'])->name('userlog.detail')->where('id', '[0-9]+');
});
/*------------Staff Route--------------*/
Route::prefix('staff')->middleware(['auth:staff', 'PreventBackHistory'])->name('staff.')->group(function () {

    Route::post('/logout', [AuthenticationController::class, 'logout'])->name('logout');
    Route::get('/profile', [AdminController::class, 'profile'])->name('profile');
    Route::get('/editProfile', [AdminController::class, 'editProfile'])->name('editProfile');
    Route::post('/updateProfile', [AdminController::class, 'updateProfile'])->name('updateProfile');
    //Todo STAFF----->
    Route::middleware(['isLogged'])->group(function () {
        //HOME
        Route::get('/home', [HomeStaffController::class, 'index'])->name('home');
        //Import File
        Route::post('/import-student', [StudentImportController::class, 'import'])->name('import.student');
        Route::get('/import-student', [StudentImportController::class,'index'])->name('import.student.show');
        //Student-manager
        Route::resource('/student', StudentController::class);
        Route::get('/student-report-detail/{id}', [StudentController::class, 'showReportDetail'])->name('student.showReportDetail')->where('id', '[0-9]+');
        Route::get('/student/{id}/sendMail', [StudentController::class, 'sendAccount'])->name('student.send')->where('id', '[0-9]+');
        Route::get('/student-service/{id}', [StudentController::class, 'studentSerivce'])->name('student.service')->where('id', '[0-9]+');
        //Company-manager
        Route::resource('/company', StaffCompanyController::class);
        Route::get('/pending-company', [StaffCompanyController::class, 'pendingCompany'])->name('company.pending');
        Route::post('/pending-company/{id}', [StaffCompanyController::class, 'approveCompany'])->name('company.approve')->where('id', '[0-9]+');

        Route::get('/staff-companys/{id}', [StaffCompanyController::class, 'showStaffCompany'])->name('staff-companys.show')->where('id', '[0-9]+');
        Route::get('/staff-company', [StaffCompanyController::class, 'createStaffCompany'])->name('staff-company.create');
        Route::post('/staff-company', [StaffCompanyController::class, 'storeStaffCompany'])->name('staff-company.store');
        Route::get('/staff-company/{id}', [StaffCompanyController::class, 'editStaffCompany'])->name('staff-company.edit')->where('id', '[0-9]+');
        Route::put('/staff-company/{id}', [StaffCompanyController::class, 'updateStaffCompany'])->name('staff-company.update')->where('id', '[0-9]+');
        Route::delete('/staff-company/{id}', [StaffCompanyController::class, 'destroyStaffCompany'])->name('staff-company.destroy')->where('id', '[0-9]+');

        Route::get('/post-companys/{id}', [StaffCompanyController::class, 'showPostCompany'])->name('post-companys.show')->where('id', '[0-9]+');
        Route::get('/post-company/{id}', [StaffCompanyController::class, 'editPostCompany'])->name('post-company.edit')->where('id', '[0-9]+');
        Route::put('/post-company/{id}', [StaffCompanyController::class, 'updatePostCompany'])->name('post-company.update')->where('id', '[0-9]+');
        Route::delete('/post-company/{id}', [StaffCompanyController::class, 'destroyPostCompany'])->name('post-company.destroy')->where('id', '[0-9]+');

        Route::get('/student-companys/{id}', [StaffCompanyController::class, 'showStudentCompany'])->name('student-companys.show')->where('id', '[0-9]+');
        Route::get('/student-company/{id}', [StaffCompanyController::class, 'editStudentCompany'])->name('student-company.edit')->where('id', '[0-9]+');
        Route::put('/student-company/{id}', [StaffCompanyController::class, 'updateStudentCompany'])->name('student-company.update')->where('id', '[0-9]+');
        Route::delete('/student-company/{id}', [StaffCompanyController::class, 'destroyStudentCompany'])->name('student-company.destroy')->where('id', '[0-9]+');
        //Other
        Route::resource('post-type', PostTypeController::class);
        Route::resource('post', PostController::class);
        Route::resource('welfare', WelfareController::class);
        Route::resource('careerGroup', CareerGroupController::class);
        Route::resource('careers', CareersController::class);
        Route::resource('report', ReportController::class);
        Route::resource('recruitment', RecruitmentController::class);
        //Feedback-Manager
        Route::resource('feedback', FeedBackController::class);
        Route::put('/feedback-reply/{id}', [FeedBackController::class, 'returnFeedback'])->name('feedback.reply')->where('id', '[0-9]+');
        Route::put('/feedback/reply/{id}', [FeedBackController::class, 'updateReply'])->name('feedback.updateReply')->where('id', '[0-9]+');
        Route::post('/feedback/sendMailStudent/{email}', [FeedBackController::class, 'sendMailStudent'])->name('feedback.sendMailStudent');
        //ReportRate
        // Route::get('/repost', [StaffReportRateController::class, 'showReport'])->name('report.index');
        // Route::get('/repost/{id}', [StaffReportRateController::class, 'detailReport'])->name('report.show');
        // Route::put('/repost-update/{id}', [StaffReportRateController::class, 'updateReportRate'])->name('report.update');

        //Request-Report
        Route::get('/request-report', [RequestController::class, 'showRequestReport'])->name('request-report.index');
        Route::get('/request-reset-report', [RequestController::class, 'listRequestResetReport'])->name('request-reset-report.index');
        Route::get('/handle-request-reset-report/{id}', [RequestController::class, 'handleRequestResetReport'])->name('request-reset-report.handle')->where('id', '[0-9]+');
        Route::put('/request-company/{id}', [RequestController::class, 'updateRequestCompany'])->name('request-company.update')->where('id', '[0-9]+');
        Route::put('/request-student/{id}', [RequestController::class, 'updateRequestStudent'])->name('request-student.update');
        //Request-Exemption
        Route::get('/exemption', [ExemptionController::class, 'index'])->name('exemption.index');
        Route::put('/exemption/{id}', [ExemptionController::class, 'updateSuccess'])->name('exemption.success');
        Route::put('/exemption', [ExemptionController::class, 'exemptionFail'])->name('exemption.fail');
        //Service Internship Again
        Route::get('/internship-again', [InternshipAgainController::class, 'index'])->name('internship-again.index');
        Route::put('/internship-pass/{id}', [InternshipAgainController::class, 'internshipPass'])->name('internship-again.pass');
        Route::put('/internship-again', [InternshipAgainController::class, 'internshipFail'])->name('internship-again.fail');

        // notifi
        Route::resource('/notify', NotifyStaffController::class);
        Route::get('/notifition', [NotifyStaffController::class, 'myNotify'])->name('notifition.myNotify');
        Route::get('/notify-detail/{id}', [NotifyStaffController::class, 'myNotifyDetail']);
        //transcript
        Route::get('/transcript',[Transcript::class,'index'])->name('transcript.index');
        Route::post('/transcript/{id}',[Transcript::class,'store'])->name('transcript.store');
    });
});

/*------------Client Route--------------*/
Route::prefix('student')->name('student.')->group(function () {
    Route::middleware(['guest:student', 'PreventBackHistory'])->group(function () {
        Route::view('/', 'auth.student.login');
        Route::view('/login', 'auth.student.login')->name('login');
        Route::post('/check', [AuthenticationController::class, 'check'])->name('check');
        Route::get('/check', [AuthenticationController::class, 'check'])->name('check');
    });

    Route::middleware(['auth:student', 'PreventBackHistory'])->group(function () {
        Route::middleware('isLogged')->group(function () {
            Route::get('/notification', [HomeController::class, 'getNotification'])->name('notification.getnotification');
            Route::get('/home', [HomeStudentController::class, 'index'])->name('home');
            Route::view('/profile', 'student.users.profile')->name('profile');
            //Resume
            Route::get('/resume', [ResumeController::class, 'index'])->name('resume');
            Route::get('/resume/create', [ResumeController::class, 'create'])->name('resume.create');
            Route::get('/resume/update', [ResumeController::class, 'edit'])->name('resume.update');
            Route::post('/resume/store', [ResumeController::class, 'store'])->name('resume.store');
            Route::post('/resume/store-upload-cv', [ResumeController::class, 'storeUploadCV'])->name('resume.store-upload');
            Route::delete('/resume/{code}', [ResumeController::class, 'deleteCV'])->name('resume.delete-upload');
            Route::get('/resume/{code}', [ResumeController::class, 'detail'])->name('resume.detail');
            Route::put('/resume', [ResumeController::class, 'changeActive'])->name('resume.change-active');
            //Apply
            Route::post('/recruitment-apply', [HomeController::class, 'storeRecruitment'])->name('recruitment.store');
            Route::get('/applied-job', [StudentStudentController::class, 'studentApplicationManager'])->name('applied-job.index');
            Route::get('/accuracy', [StudentStudentController::class, 'accuracyAccept'])->name('accuracy.accept');
            Route::post('/applied-job-deleted', [StudentStudentController::class, 'deleteRecruitmentPost'])->name('applied-job.destroy');
            //Company
            Route::resource('/company', StudentCompanyController::class);
            Route::post('/register-report', [HomeStudentController::class, 'registerReport'])->name('register.report');
            //Report
            Route::get('/report', [StudentReportController::class, 'index'])->name('report');
            Route::get('/report-done', [StudentReportController::class, 'reportDone'])->name('report-done');
            Route::get('/report-finish', [StudentReportController::class, 'reportFinish'])->name('report-finish');
            Route::post('/report', [StudentReportController::class, 'store'])->name('report.store');
            Route::post('/report-edit', [ReportDetailController::class, 'edit'])->name('report-detail.edit');
            Route::post('/report-detail', [ReportDetailController::class, 'store'])->name('report-detail.store');
            Route::delete('/report-destroy', [ReportDetailController::class, 'destroy'])->name('report-detail.destroy');
            Route::post('/report-reset', [StudentReportController::class, 'requestResetReport'])->name('report.reset');
            Route::post('/report-rate', [StudentReportController::class, 'uploadRateFile'])->name('report.upload-rate');
            Route::resource('/feedBack-student', StudentFeedbackController::class);
            Route::get('/transcript',[StudentTranscript::class,'index'])->name('transcript.index');
            Route::post('/transcript',[StudentTranscript::class,'store'])->name('transcript.store');
            Route::resource('/service-exemption-student', ServiceExemptionController::class);
            Route::delete('/service-exemption-student/{id}', [ServiceExemptionController::class, 'delete'])->name('service-exemption-student.delete');
            Route::resource('/service-internship-again-student', ServiceInternshipAgainController::class);

            Route::post('/service-internship-again-student', [ServiceInternshipAgainController::class, 'store'])->name('service-internship-again-student.store');
            Route::get('/internship-cancel',[InternShipCancel::class,'index'])->name('service.intershipCancle.index');
            Route::post('/internship-cancel',[InternShipCancel::class,'store'])->name('service.intershipCancle.store');
        });
        Route::middleware('endIsLogged')->group(function () {
            Route::get('/exemptionPass',  [StudentStudentController::class, 'checkExemptionPass'])->name('exemptionPass');
            Route::get('/initial/profile',  [StudentStudentController::class, 'index'])->name('initial.profile');
            Route::post('/initial/profile', [StudentStudentController::class, 'updateProfile'])->name('initial.update');
        });
        Route::post('logout', [AuthenticationController::class, 'logout'])->name('logout');
    });

});

Route::prefix('company')->name('company.')->group(function () {
    Route::middleware(['guest:company', 'PreventBackHistory'])->group(function () {
        Route::view('/', 'auth.company.login');
        Route::view('/login', 'auth.company.login')->name('login');
        Route::view('/register', 'auth.company.register')->name('register');
        Route::post('/register', [AuthenticationController::class, 'register'])->name('register.create');
        Route::post('/check', [AuthenticationController::class, 'check'])->name('check');
    });

    Route::middleware(['auth:company', 'PreventBackHistory'])->group(function () {
        Route::middleware('isLogged')->group(function () {
            Route::get('/home', [HomeCompanyController::class, 'index'])->name('home');
            Route::view('/profile', 'company.profile')->name('profile');
            //Company User
            Route::resource('/information', CompanyInformationController::class);
            Route::get('user-profile/{id}', [UserCompanyController::class, 'detailProfile'])->name('user.profile');
            Route::put('/edit-profile/{id}', [UserCompanyController::class, 'updateProfile'])->name('user.profile.update');
            //bài đăng tuyển
            Route::get('/recruitment', [RecruitmentPostController::class, 'recruitmentPost'])->name('recruitment');
            Route::get('/detail-recruitment/{id}', [RecruitmentPostController::class, 'detailRecruitmentPost'])->name('recruitment.detail')->where('id', '[0-9]+');
            Route::get('/create-recruitment', [RecruitmentPostController::class, 'createdRecruitmentPost'])->name('recruitment.create');
            Route::post('/recruitment', [RecruitmentPostController::class, 'storeRecruitmentPost'])->name('recruitment.store');
            Route::get('/edit-recruitment/{id}', [RecruitmentPostController::class, 'editdRecruitmentPost'])->name('recruitment.edit')->where('id', '[0-9]+');
            Route::put('/updated-recruitment/{id}', [RecruitmentPostController::class, 'updateRecruitmentPost'])->name('recruitment.update')->where('id', '[0-9]+');
            Route::put('/detail-recruitment/{id}', [RecruitmentPostController::class, 'updatedRecruimentDetail'])->name('recruitment.updateDetail')->where('id', '[0-9]+');
            Route::delete('/delete-recruitment/{id}', [RecruitmentPostController::class, 'deleteRecruitmentPost'])->name('recruitment.delete')->where('id', '[0-9]+');
            //Recruitment Manager
            Route::resource('/recruitment-manager', RecruitmentDetailController::class);
            //Intern Student
            Route::resource('/intern-student', ReportRateController::class);
            Route::put('/report-rate/{id}', [ReportRateController::class, 'updateStatus'])->name('report-rate.update')->where('id', '[0-9]+');
            Route::put('/report-rate-cacle/{id}', [ReportRateController::class, 'updateCancel'])->name('report-rate.update.cancel')->where('id', '[0-9]+');
            // Notify
            Route::get('/notification', [HomeController::class, 'getNotification'])->name('notification.getnotification');
            //feedback
            Route::resource('/feedBack', CompanyFeedBackController::class);
        });
        Route::middleware('endIsLogged')->group(function () {
            Route::view('/initial/profile', 'company.isLogged.createProfile')->name('initial.profile');
            Route::get('/initial/company', [HomeCompanyController::class, 'create'])->name('initial.company');
            Route::post('/initial/company', [HomeCompanyController::class, 'storeCompany'])->name('initial.company.store');
            Route::post('/initial/profile', [UserCompanyController::class, 'storeProfile'])->name('initial.profile.store');
            Route::get('/initial/company/{id}/edit', [HomeCompanyController::class, 'editCompany'])->name('initial.profile.edit')->where('id', '[0-9]+');
            Route::put('/initial/company/{id}', [HomeCompanyController::class, 'updateCompany'])->name('initial.profile.update')->where('id', '[0-9]+');
        });

        Route::post('logout', [AuthenticationController::class, 'logout'])->name('logout');
    });
});
