<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsLogged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::guard('company')->check()) {
            if (!Auth::user()->is_logged) {
                return  redirect()->route('company.initial.profile')->with('notify', 'Vui lòng cập nhật thông tin cá nhân!');
            }
            if (!Auth::user()->company_id) {
                
                return  redirect()->route('company.initial.company')->with('notify', 'Vui lòng cập nhật thông tin doanh nghiệp!');
            }
            if (!Auth::user()->company->code) {
                if(Auth::user()->company->status == COMPANY_STATUS_DENIED){
                    return  redirect()->route('notify')->with(['notify-content'=>'Doanh nghiệp của bạn đã bị từ chối, vui lòng kiểm tra lại đăng ký <a href="'.route('company.initial.profile.edit',Auth::user()->company->id).'" class="text-info"> tại đây </a> !']);
                }
                return  redirect()->route('notify')->with(['notify-content'=>'Doanh nghiệp của bạn đang kiểm tra phê duyệt vui lòng quay lại sau!']);
            }
            if (!Auth::user()->company->is_active) {
                return  redirect()->route('notify')->with(['warning'=>'Hạn chế truy cập!','notify-content'=> 'Doanh nghiệp của bạn đang tạm ngưng hoạt động!']);
            }
        }
        if (Auth::guard('staff')->check()) {
            if (!Auth::user()->is_logged) {
                return  redirect()->route('staff.editProfile')->with('notify', 'Vui lòng cập nhật thông tin cá nhân!');
            }
        }
        if (Auth::guard('student')->check()) {
            if (!Auth::user()->is_logged) {
                return  redirect()->route('student.initial.profile')->with('notify', 'Vui lòng cập nhật thông tin cá nhân!');
            }
            if (Auth::user()->student->status == USERSTUDENT_PASS) {
                return  redirect()->route('student.exemptionPass')->with('notify', 'Bạn đã hoàn thành thực tập !');
            }
            
        }
        return $next($request);
    }
}
