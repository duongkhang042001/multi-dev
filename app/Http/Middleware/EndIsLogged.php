<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class EndIsLogged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $checkClient = false;
        if (Auth::guard('company')->check()) {
            if (Route::is('company.initial.profile ') && Auth::user()->is_logged) {
                $checkClient = true;
            }
            if (Route::is('company.initial.company') && Auth::user()->company_id) {
                $checkClient = true;
            }
            if (Route::is('company.initial.profile.edit') && Auth::user()->company->status != COMPANY_STATUS_DENIED) {
                $checkClient = true;
            }
        }
        if (Auth::guard('student')->check()) {
            if (Route::is('student.initial.profile') && Auth::user()->is_logged) {
                $checkClient = true;
            }
            if (Route::is('student.exemptionPass') && !Auth::user()->student->status == USERSTUDENT_PASS) {
                $checkClient = true;
            }
        }
        if($checkClient == true) return  redirect()->route('home');
        return $next($request);
    }
}
