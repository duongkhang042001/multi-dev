<?php

namespace App\Http\Livewire;

use App\Models\Comment;
use Livewire\Component;

class NewComment extends Component
{
    public $limit = 10;
    public function render()
    {
        return view('livewire.new-comment',[
            'comments' =>  Comment::latest()->limit($this->limit)->orderBy('id', 'DESC')->get(),
        ]);
    }
}
