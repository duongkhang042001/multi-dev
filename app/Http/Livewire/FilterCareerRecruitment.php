<?php

namespace App\Http\Livewire;

use App\Models\Careers;
use App\Models\City;
use App\Models\Post;
use App\Models\RecruitmentPost;
use Livewire\Component;
use Livewire\WithPagination;
class FilterCareerRecruitment extends Component
{
    use WithPagination;
    public $byCareer = null;
    public $gender = -1;
    public $city;
    public $jobType;
    public $perPage = 15;
    public $search;

    
    public function render()
    { 
        return view('livewire.filter-career-recruitment',[
            'careers'     => Careers::orderBy('name','asc')->get(),
            'cities'      => City::where('is_education',1)->where('is_active',1)->get(),
            'recruitment' => RecruitmentPost::when($this->byCareer, function($query){
                        $query->where('career_id',$this->byCareer);
                        })
                ->search(trim($this->search))
                ->city(trim($this->city))
                ->jobType(trim($this->jobType))
                ->gender(trim($this->gender))
                ->paginate($this->perPage)   
        ]);
    }
}
