<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\CareerGroup;
use App\Models\Careers;

class ChangeGroupCareer extends Component
{
    public $careers = [];
    public $careerId;
    public $careerGroupId;
    public $is_education;

    public function render()
    {
        $this->is_education ?
            $reGroup = CareerGroup::where('is_education',1)->get()
            : $reGroup = CareerGroup::all();
        return view(
            'livewire.change-group-career',
            ['groupCareers' => $reGroup]
        );
    }
    public function updatedCareerGroupId($id)
    {
        $this->careers = Careers::where('career_group_id', $this->careerGroupId)->get();
    }
    public function changeGroupCareer(){
        $this->careers = Careers::where('career_group_id', $this->careerGroupId)->get();
    }
    public function mount(){
        $this->changeGroupCareer();
    }
}
