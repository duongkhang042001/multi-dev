<?php

namespace App\Http\Livewire;

use App\Models\City;
use App\Models\District;
use App\Models\Ward;
use Livewire\Component;

class EditAddressAdmin extends Component
{
    public $cityCode;
    public $districtCode;
    public $wardsCode;
    public $districts = [];
    public $wards = [];

    public function render()
    {
        return view(
            'livewire.edit-address-admin',
            ['cities' => City::all()]
        );
    }
    public function updatedCityCode($id)
    {
        $this->districts = District::where('city_id', $id)->get();
        $this->wards = [];
    }
    public function updatedDistrictCode($id)
    {
        $this->wards = Ward::where('district_id', $id)->get();
    }
    public function changeDistrict()
    {
        $this->districts = District::where('city_id', $this->cityCode)->get();
    }
    public function changeWard()
    {
        $this->wards = Ward::where('district_id', $this->districtCode)->get();
    }
    public function mount()
    {
        $this->changeDistrict();
        $this->changeWard();
    }
}
