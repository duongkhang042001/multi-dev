<?php

namespace App\Http\Livewire;

use App\Models\Company;
use App\Models\RecruitmentPost;
use Livewire\Component;
use Livewire\WithPagination;
class ListRecruitmentCompany extends Component
{
    use WithPagination;
    public $idCompany;
    public $perPage = 6;
    public function render()
    {
        $company = Company::where('id', $this->idCompany)->first();
        return view('livewire.list-recruitment-company',[
            'listRecruitmentCompany' => RecruitmentPost::where('company_id',$company->id)->paginate($this->perPage),
        ]);
    }
}
