<?php

namespace App\Http\Livewire;

use App\Models\RecruitmentPost;
use Livewire\Component;
use Livewire\WithPagination;
class ListRelateWord extends Component
{
    use WithPagination;
    public $idRecruitment;
    public $perPage = 5;
    public function render()
    {
        
        $recruitment = RecruitmentPost::where('id', $this->idRecruitment)->first();
        return view('livewire.list-relate-word',[
            'relatedWork' => RecruitmentPost::with('company', 'career', 'post')
                ->where('career_id', '=', $recruitment->career->id)
                ->whereNotIn('recruitment_post.post_id', [$recruitment->post_id])->paginate($this->perPage),
        ]);
    }
}
