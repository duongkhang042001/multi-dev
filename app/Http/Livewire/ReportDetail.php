<?php

namespace App\Http\Livewire;

use App\Models\Report;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Livewire\Component;

class ReportDetail extends Component
{
    public $idUser;
    public $report;
    public $reportCount;
    public $reportAll;
    public $reportDetail;
    public $arrDate;
    public $allWeek;
    public $getReportClick;
    public $reportDetailClick;
    public $arrDateClick;
    public $allWeekClick;
    public function render()
    {
        return view('livewire.report-detail');
    }
    public function mount()
    {
        $this->report = Report::with(['reportDetail' => function ($query) {
            $query->orderBy('date', 'ASC');
        }])->where('user_id', $this->idUser)->first();
        if($this->report)
        {
            $this->reportCount = Report::with(['reportDetail' => function ($query) {
                $query->orderBy('date', 'ASC');
            }])->where('user_id', $this->idUser)->count();
            $this->reportAll = Report::where('user_id',$this->idUser)->latest()->get();
            $this->reportDetail = $this->report->reportDetail;
            $this->arrDate = Arr::pluck($this->reportDetail, 'date');
            $this->allWeek = $this->getAllWeek($this->report->start, $this->report->end);
        }
    }
    public function getAllWeek()
    {
        if (!empty($start_date_input)) {
            $start_date     = Carbon::parse($start_date_input);
            $end_date       = !empty($end_date) ? Carbon::parse($end_date) : Carbon::parse($start_date)->addWeek(10); //25 01
            $weekOfYear     = $start_date->weekOfYear;
            $endWeekOfYear  = $end_date->weekOfYear;
            $countWeek      = $endWeekOfYear - $weekOfYear;
            if ($countWeek <= 0) {
                $tempWeek       = $start_date->endOfYear()->weekOfYear;
                $countWeek      = $tempWeek - $weekOfYear + $endWeekOfYear;
            }
            for ($i = 1; $i <= $countWeek; $i++) {
                if ($i == 1) {
                    $now            = Carbon::parse($start_date_input);
                } else {
                    $now            = $start_date->addWeek(1);
                }
                $weekStartDate  = $now->startOfWeek()->format('Y-m-d H:i:s');
                $weekEndDate    = $now->endOfWeek()->format('Y-m-d H:i:s');
                $start_date     = $now;
                $time_week[$i]      = ["start_week" => $weekStartDate, "end_week" => $weekEndDate];
            }
        } else {
            return $tempWeek = [];
        }
        return $time_week;
    }
    public function getReportDetail($id)
    {
        $this->getReportClick = Report::with(['reportDetail' => function ($query) {
            $query->orderBy('date', 'ASC');
        }])->where('id', $id)->first();
        $this->reportDetailClick = $this->getReportClick->reportDetail;
        $this->arrDateClick = Arr::pluck($this->reportDetailClick, 'date');
        $this->allWeekClick = $this->getAllWeek($this->getReportClick->start, $this->getReportClick->end);
    }
}
