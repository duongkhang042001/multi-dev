<?php

namespace App\Http\Livewire;

use App\Models\RecruitmentPostDetail;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class NewRecruitmentDetail extends Component
{
    public function render()
    {
        return view('livewire.new-recruitment-detail', [
            'listRcmDetail' => RecruitmentPostDetail::whereHas('recruitmentPost.company', function ($query) {
                $query->where('company_id', Auth::guard('company')->user()->company->id);
            })->latest()->limit(10)->get(),
        ]);
    }
}
