<?php

namespace App\Http\Livewire;

use App\Models\Comment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Comments extends Component
{
    public $content;
    public $user;
    public $post_id;
    public $idPost;
    public $countAllComment;
    public $limit = 3;
    public $updateMode = false;
    protected $rules = [
        'content' => 'required|min:2|max:225'
    ];
    protected $messages = [
        'content.required'  => 'Vui lòng nhập nội dung bình luận.',
        'content.min'       => 'Vui lòng không nhập nhỏ hơn 2 ký tự.',
        'content.max'       => 'Vui lòng không nhập lớn hơn 500 ký tự.',
    ];
    public function mount()
    {
        $this->getIdUser();
        $this->countAllComment = Comment::where('post_id', $this->idPost)->count();
    }
    public function render()
    {
        return view('livewire.comments', [
            'total' => Comment::where('post_id', $this->idPost)->count(),
            'comments' => Comment::where('post_id', $this->idPost)->latest()->take($this->limit)->get(),
        ]);
    }

    public function getIdUser()
    {
        if (Auth::guard('manager')->check()) $this->userIdCurrent = Auth::guard('manager')->id();
        if (Auth::guard('staff')->check())   $this->userIdCurrent = Auth::guard('staff')->id();
        if (Auth::guard('student')->check()) $this->userIdCurrent = Auth::guard('student')->id();
        if (Auth::guard('company')->check()) $this->userIdCurrent = Auth::guard('company')->id();
    }

    public function showLimtComment()
    {
        $this->limit += 3;
        $this->render();
    }

    public function showsLimtComment()
    {
        $this->limit = 3;
    }

    private function resetInput()
    {
        $this->content = null;
        $this->countAllComment = Comment::where('post_id', $this->idPost)->count();
    }
    public function store()
    {
        $this->validate();
        Comment::create([
            'content' => $this->content,
            'post_id' => $this->idPost,

        ]);
        session()->flash('message', 'Thêm bình luận thành công');
        $this->resetInput();
    }
    public function edit($id)
    {
        $record = Comment::findOrFail($id);
        $this->post_id = $id;
        $this->content = $record->content;
        $this->updateMode = true;
    }
    public function update()
    {
        $this->validate();
        if ($this->post_id) {
            $record = Comment::find($this->post_id);
            $record->update([
                'content' => $this->content,
            ]);
            session()->flash('message', 'Chỉnh sửa bình luận thành công');
            $this->resetInput();
            $this->updateMode = false;
        }
    }
    public function deleteComment($id)
    {
        $this->countAllComment = Comment::where('post_id', $this->idPost)->count();
        $comment = Comment::find($id);
        $comment->delete();
    }
}
