<?php

namespace App\Http\Livewire;

use App\Models\City;
use App\Models\District;
use App\Models\Ward;
use Livewire\Component;

class ChangeAddressCustom extends Component
{
    public $cityId;
    public $districtId;
    public $wardId;
    public $districts = [];
    public $wards = [];

    public function render()
    {
        return view(
            'livewire.change-address-custom',
            ['cities' => City::all()]
        );
    }
    public function updatedcityId($id)
    {
        $this->districts = District::where('city_id', $id)->get();
        $this->wards = [];
    }
    public function updateddistrictId($id)
    {
        $this->wards = Ward::where('district_id', $id)->get();
    }
    public function changeDistrict()
    {
        $this->districts = District::where('city_id', $this->cityId)->get();
    }
    public function changeWard()
    {
        $this->wards = Ward::where('district_id', $this->districtId)->get();
    }
    public function mount()
    {
        $this->changeDistrict();
        $this->changeWard();
    }
}
