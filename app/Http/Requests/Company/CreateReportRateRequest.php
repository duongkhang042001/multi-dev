<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateReportRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'advantages'                        => 'required',
            'defect'                            => 'required',
            'content'                           => 'required',
            'attitude_point'                    => 'required|in:' . implode(',', ['1', '2', '3', '4','5','6','7','8','9','10']),
            'work_point'                        => 'required|in:' . implode(',', ['1', '2', '3', '4','5','6','7','8','9','10']),
        ];
    }

    public function messages()
    {
        return [
            'advantages.required'               => 'Vui lòng đánh giá ưu điểm của sinh viên',
            'defect.required'                   => 'Vui lòng đánh giá hạn chế của sinh viên',
            'content.required'                  => 'Vui lòng nhập góp ý cho sinh viên',
            'attitude_point.required'           => 'Vui lòng nhập điểm thái độ cho sinh viên',
            'attitude_point.in'                 => 'Điểm thuộc dạng số từ 1 tới 10 điểm ',
            // 'attitude_point.digits'             => 'Điểm thuộc dạng số từ 1 tới 10 điểm ',
            'work_point.required'               => 'Vui lòng nhập điểm công việc cho sinh viên',
            'work_point.in'                     => 'Điểm thuộc dạng số từ 1 tới 10 điểm ',                           
        ];
    }
}
