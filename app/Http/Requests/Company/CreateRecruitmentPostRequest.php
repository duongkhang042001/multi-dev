<?php

namespace App\Http\Requests\Company;

use App\Models\RecruitmentPost;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class CreateRecruitmentPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // dd($this->request->all());
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                      => 'required|max:155',
            'short_description'          => 'required|max:1255',
            'content'                    => 'required|max:2255',
            'careers'                    => 'required',
            'quantity'                   => 'required|numeric|min:1',
            'date_start'                 => 'required|date|after_or_equal:today',
            'date_end'                   => 'required|date|after:date_start',
            'salary_min'                 => 'nullable|numeric|min:0',
            'salary_max'                 => 'nullable|numeric|gte:salary_min',
            'requirement'                => 'required|max:2255',
            'welfares'                   => 'required',
            // 'address'                    => 'required_with:check',
            // 'ward'                       => 'required_with:check|exists:wards,id',
            // 'district'                   => 'required_with:check|exists:districts,id',
            // 'city'                       => 'required_with:check|exists:cities,id',
        ];
    }
    public function messages()
    {
        return [
            'title.required'             => "Vui lòng nhập tiêu đề",
            'title.max'                  => "Tiêu đề không vượt quá 155 ký tự",
            'slug.unique'                => "Đường dẫn đã tồn tại",
            'slug.required'              => "Vui lòng nhập đường dẫn",
            'slug.max'                   => "Đường dẫn không vượt quá 155 ký tự",
            'short_description.required' => "Vui lòng nhập mô tả",
            'short_description.max'      => "Mô tả không quá 1000 ký tự",
            'content.required'           => "Vui lòng nhập nội dung công việc",
            'content.max'                => "Nội dung công việc không quá 2000 ký tự",
            'careers.required'           => "Vui lòng chọn nghành",
            'post_type.required'         => "Vui lòng chọn loại bài viết",
            'company_id.required'        => "Vui lòng chọn doanh nghiệp",
            'quantity.required'          => "Vui lòng nhập số lượng",
            'quantity.numeric'           => "Vui lòng chọn số",
            'quantity.min'               => "Vui lòng nhập số lớn hơn bằng 1",
            'date_start.required'        => "Vui lòng nhập ngày bắt đầu",
            'date_start.date'            => "Vui lòng chọn đúng định dạng ngày",
            'date_start.after_or_equal'  => "Vui lòng chọn lớn hơn hoặc bằng ngày hiện tại",
            'date_end.required'          => "Vui lòng nhập ngày kết thúc",
            'date_end.date'              => "Vui lòng chọn định dạng ngày",
            'date_end.after'             => "Vui lòng chọn ngày kết thúc lớn hơn ngày bắt đầu",
            'salary_min.required'        => "Vui lòng nhập lương",
            'salary_min.numeric'         => "Vui lòng chọn số",
            'salary_min.min'             => "Vui lòng nhập số lớn hơn bằng 0",
            'salary_max.required'        => "Vui lòng nhập lương",
            'salary_max.numeric'         => "Vui lòng chọn loại bài viết",
            'salary_max.gte'             => "Lương cao nhất phải lớn hơn lương thấp",
            'requirement.required'       => "Vui lòng nhập yêu cầu công việc",
            'requirement.max'            => "Yêu cầu công việc không 2000 ký tự",
            'welfares.required'          => "Vui lòng chọn phúc lợi",
            // 'welfares.required'          => "Vui lòng chọn phúc lợi",
            // 'address.required_with'      => "Vui lòng nhập địa chỉ cụ thể",
            // 'ward.required_with'         => "Vui lòng chọn phường, xã, thị trấn.",
            // 'ward.exists'                => "Phường, xã, thị trấn không có trong hệ thống.",
            // 'district.required_with'     => "Vui lòng chọn quận, huyện",
            // 'district.exists'            => "Quận, huyện không có trong hệ thống.",
            // 'city.required_with'         => "Vui lòng chọn tỉnh, thành phố.",
            // 'city.exists'                => "Tỉnh, thành phố không có trong hệ thống.",
        ];
    }

    //  protected function failedValidation(Validator $validator)
    // {
    //     dd($validator->errors());
    // }
}
