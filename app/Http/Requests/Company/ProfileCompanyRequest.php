<?php

namespace App\Http\Requests\Company;

use App\Rules\UniqueProfileRule;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ProfileCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name'       => 'required',
            'email_personal'  => ['nullable','email',new UniqueProfileRule(null , 'Nhân Viên')],
            'phone'  => [
                'required','regex:/(0)[0-9]{9}/','max:10',
                Rule::unique('profiles')->where(function ($query) {
                    return $query->where('deleted_at', NULL);
                })

            ],
            'indo'  => [
                'required','digits_between:9,13',
                Rule::unique('profiles')->where(function ($query) {
                    return $query->where('deleted_at', NULL);
                })

            ],
            'avatar'        => 'mimes:png,jpeg,jpg',
            'birthday'      => 'required',
            'avatar'        => 'nullable|mimes:png,jpeg,jpg',
            'district'      => 'required',
            'ward'          => 'required|exists:wards,id',
            'district'      => 'required|exists:districts,id',
            'city'          => 'required|exists:cities,id',
            'description'   => 'max:355',
        ];
    }

    public function messages()
    {
        return [
            'full_name.required'       => 'Vui lòng nhập họ và tên',
            'email_personal.required'  => 'Vui lòng nhập Email',
            'birthday.required'        => 'Vui lòng nhập ngày tháng năm sinh',
            'address.required'         => 'Vui lòng nhập địa chỉ',
            'phone.required'           => 'Vui lòng nhập số điện thoại',
            'phone.regex'              => 'Số điện thoại không đúng',
            'phone.max'                => 'Số điện thoại không quá 10 số',
            'phone.unique'             => 'Số điện thoại đã tồn tại',
            // 'phone.numerric'           => 'Số điện thoại thuộc dạng số',
            'indo.required'            => 'Vui lòng nhập CMND/CCCD',
            'indo.unique'              => 'CMND/CCCD đã tồn tại',
            'indo.digits_between'      => 'CMND/CCCD không nhỏ hơn 9 số và không lớn hơn 13 số',
            // 'indo.numerric'            => 'CMND/CCCD thuộc dạng số',
            'description.max'          => 'Giới thiệu không quá 355 kí tự',
            'ward.required'            => "Vui lòng chọn phường, xã, thị trấn.",
            'ward.exists'              => "Phường, xã, thị trấn không có trong hệ thống.",
            'district.required'        => "Vui lòng chọn quận, huyện",
            'district.exists'          => "Quận, huyện không có trong hệ thống.",
            'city.required'            => "Vui lòng chọn tỉnh, thành phố.",
            'city.exists'              => "Tỉnh, thành phố không có trong hệ thống.",
            'avatar.mimes'             => "Hình Ảnh Phải Có Dạng : jpeg,png,jpg",


        ];
    }
}
