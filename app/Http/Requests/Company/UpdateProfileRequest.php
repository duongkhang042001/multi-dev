<?php

namespace App\Http\Requests\Company;

use App\Rules\UniqueProfileRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        return [
            'name'                    => 'required',
            'password' => ['nullable',function ($attribute, $value, $fail) {
                if (!Hash::check($value, Auth::user()->password)) {
                    $fail('Mật khẩu cũ không đúng');
                }
            }],
            'password_new'            => 'required_with:password',
            'password_confirmation'   => 'required_with:password_new|same:password_new',
            'email_personal'          =>  ['nullable', new UniqueProfileRule($id, 'Nhân viên')],
            'phone'                   =>  ['required', 'digits_between:9,12', new UniqueProfileRule($id, 'Nhân Viên')],
            'indo'                    =>  ['nullable', 'digits_between:9,13', new UniqueProfileRule($id, 'Nhân Viên')],
            'address'                 => 'required',
            'avatar'                  => 'mimes:png,jpeg,jpg',
            'description'             => 'nullable|max:255',
            'ward'                    => 'required|exists:wards,id',
            'district'                => 'required|exists:districts,id',
            'city'                    => 'required|exists:cities,id',

        ];
    }

    public function messages()
    {
        return [
            'email_personal.required'                       => 'Vui lòng nhập Email',
            'password_new.different'                        => 'Mật khẩu mới không trùng mật khẩu cũ ',
            'password_new.min'                              => "Mật khẩu không quá 8 kí tự",
            'password_new.required_with'                    => "Vui lòng nhập mật khẩu mới",
            'password_confirmation.same'                    => 'Mật khẩu phải trùng mật khẩu mới',
            'password_confirmation.required_with'           => 'Vui lòng nhập lại mật khẩu mới',
            'phone.required'                                => "Vui lòng nhập số điện thoại",
            'phone.digits_between'                          => "Số điện thoại không được nhỏ hơn 9 Và không lớn hơn 12 số",
            'indo.digits_between'                           => "Số CMND không được nhỏ hơn 9 Và không lớn hơn 13 số",
            'description.max'                               => "Giới thiệu không quá 255 ký tự",
            'address.required'                              => "Vui lòng nhập địa chỉ",
            'ward.required'                                 => "Vui lòng chọn phường, xã, thị trấn.",
            'ward.exists'                                   => "Phường, xã, thị trấn không có trong hệ thống.",
            'district.required'                             => "Vui lòng chọn quận, huyện",
            'district.exists'                               => "Quận, huyện không có trong hệ thống.",
            'city.required'                                 => "Vui lòng chọn tỉnh, thành phố.",
            'city.exists'                                   => "Tỉnh, thành phố không có trong hệ thống.",
            // 'avatar.image'                                  => "Hình Ảnh Phải Có Dạng : jpeg, png, bmp, gif, svg",
            'avatar.mimes'                                  => "Hình Ảnh Phải Có Dạng : jpeg,png,jpg",
        ];
    }
}
