<?php

namespace App\Http\Requests\Company;

use App\Rules\UniqueCompanyRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        dd($id);
        return [
            'name'                       => ['required',new UniqueCompanyRule($id)],
            'email'                      => ['required','email',new UniqueCompanyRule($id)],
            'phone'                      => ['required','digits_between:9,12',new UniqueCompanyRule($id)],
            'short_name'                 => 'required',
            'url'                        => 'nullable|active_url',
            'banner'                     => 'nullable','mimes:png,jpeg,jpg',
            'avatar'                     => 'required','mimes:png,jpeg,jpg',
            'tax_number'                 => ['required',new UniqueCompanyRule($id)],
            'ward'                       => 'nullable|exists:wards,id',
            'district'                   => 'nullable|exists:districts,id',
            'city'                       => 'nullable|exists:cities,id',
        ];
    }

    public function messages()
    {
       return[
            'name.required'              => "Phải Nhập Tên doanh nghiệp",
            'email.required'             => 'Phải Nhập Email',
            'short_name.required'        => 'Vui lòng nhập tên viết tắt doanh nghiệp',
            'phone.required'             => "Phải Nhập Số Điện Thoại" ,
            'phone.digits_between'       => "Số Điện Thoại Không Được Nhỏ Hơn 9 Và Không Lớn Hơn 12 Số",
            'tax_number.required'        => "Phải Nhập Số Thuế",
            'url.active_url'             => "Phải Nhập Đúng Địa Chỉ Trang Website Và Địa Chỉ Này Phải Tồn Tại",
            'banner.mimes'               => "Hình Ảnh Phải Có Dạng : jpeg,png,jpg",
            'avatar.mimes'               => "Hình Ảnh Phải Có Dạng : jpeg,png,jpg",
       ];
    }

    //  protected function failedValidation(Validator $validator)
    // {
    //     dd($validator->errors());
    // }

}
