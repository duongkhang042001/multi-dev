<?php

namespace App\Http\Requests\Manager;

use App\Models\User;
use App\Models\Profile;
use App\Rules\abc;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StaffUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        $profile = Profile::where('user_id', $id)->first();
        return[
            'name'                    => 'required',
            'email'  =>  [
                Rule::unique('users')->where(function ($query) use ($id) {
                    return $query->where('id', '!=', $id)->where('deleted_at', NULL);
                }),
                'required', 'max:255',
            ],
            'code'  =>  [
                Rule::unique('users')->where(function ($query) use ($id) {
                    return $query->where('id', '!=', $id)->where('deleted_at', NULL);
                }),
                'required', 'max:20',
            ],
            'phone'  =>  [
                Rule::unique('profiles')->where(function ($query) use ($profile) {
                    return $query->where('id', '!=', $profile->id)->where('deleted_at', NULL);
                }),
                'required', 'max:10',
            ],
            'indo'                    => 'nullable|digits_between:9,12',
            'ward'                    => 'nullable|exists:wards,id',
            'district'                => 'nullable|exists:districts,id',
            'city'                    => 'nullable|exists:cities,id',
        ];
    }

    public function messages()
    {
       return[
           'name.required'           => "Nhập họ tên nhân viên" ,
           'email.email'             => 'Vui lòng nhập email',
           'email.end_with'          => 'Email phải có đuôi là @gmail.com hoặc @fpt.edu.vn',
           'code.required'           => "Vui lòng nhập code" ,
           'code.unique'             => "Mã số nhân viên đã tồn tại",
           'code.max'                => "Mã số nhân viên không được quá 20 kí tự" ,
           'phone.required'          => "Vui lòng nhập số điện thoại " ,
           'phone.max'               => "Số điện thoại không quá 10 số",
           'indo.digits_between'     => "Số CCCD tối thiểu 9 số và quá 12 số",
       ];
    }
}
