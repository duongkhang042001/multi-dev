<?php

namespace App\Http\Requests\Manager;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class NotifyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        return [
            'title'  =>  ['required', 'max:255',
            ],
            "content" => "required",
            "object"    => "required",
        ];
    }

    public function messages()
    {
        return[
            "title.required"   => "Vui lòng nhập tiêu đề",
            "title.unique"     => "Tiêu đề đã tồn tại",
            "title.max"        => "Tiêu đề không quá 255 kí tự",
            "content.required" => "Vui lòng nhập nội dung",
            "type.required"    => "Vui lòng chọn đối tượng thông báo",

        ];
    }
}
