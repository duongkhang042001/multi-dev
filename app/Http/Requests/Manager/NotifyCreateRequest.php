<?php

namespace App\Http\Requests\Manager;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class NotifyCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  =>  [
                'required', 'max:255'
            ],
            "content" => "required",
        ];
    }

    public function messages()
    {
        return[
            "title.required"   => "Vui lòng nhập tiêu đề",
            "title.max"        => "Tiêu đề không quá 255 kí tự",
            "content.required" => "Vui lòng nhập nội dung",
        ];
    }
}
