<?php

namespace App\Http\Requests\Manager;

use Illuminate\Foundation\Http\FormRequest;

class TimelineEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_find'        => 'required|date',
            'end_find'          => 'required|date|after:start_find',
            'start_support'     => 'required|date|after:start_find',
            'end_support'       => 'required|date|after:start_support',
            'first_notify'      => 'required|date|after:end_find',
            'second_notify'     => 'required|date|after:first_notify',
            'start'             => 'required|date|after:end_find',
            'end'               => 'required|date|after:start',
        ];
    }
    public function messages() {
        return  [
            'start_find.required'        => 'Chưa nhập ngày bắt đầu tìm doanh nghiệp',
            'start_find.date'            => 'Vui lòng nhập đúng định dạng ngày bắt đầu tìm doanh nghiệp',
            'end_find.required'          => 'Chưa nhập ngày kết thúc tìm doanh nghiệp',
            'end_find.date'              => 'Vui lòng nhập đúng định dạng ngày kết thúc tìm doanh nghiệp',
            'end_find.after'             => 'vui lòng nhập ngày bắt đầu tìm Dn bé hơn ngày kết thúc tìm DN',
            'start_support.required'     => 'Chưa nhập ngày bắt đầu hỗ trợ',
            'start_support.date'         => 'Vui lòng nhập đúng định dạng ngày bắt đầu hỗ trợ',
            'start_support.after'        => 'vui lòng nhập ngày bắt đầu tìm doanh nghiệp bé hơn ngày bắt đầu hỗ trợ',
            'end_support.required'       => 'Chưa nhập ngày kết thúc',
            'end_support.date'           => 'Vui lòng nhập đúng định dạng ngày kết thúc hỗ trợ',
            'end_support.after'          => 'vui lòng nhập ngày bắt đầu hỗ trợ bé hơn ngày kết thúc hỗ trợ',
            'first_notify.required'     => 'Chưa nhập ngày thông báo lần 1',
            'first_notify.date'         => 'Vui lòng nhập đúng định dạng ngày thông báo lần 1',
            'first_notify.after'        => 'vui lòng nhập ngày kết thúc tìm kiếm bé hơn ngày thông báo lần 1',
            'second_notify.required'       => 'Chưa nhập ngày thông báo lần 2',
            'second_notify.date'           => 'Vui lòng nhập đúng định dạng ngày thông báo lần 2',
            'second_notify.after'          => 'vui lòng nhập ngày thông báo lần đầu bé hơn ngày thông báo lần 2',
            'start.required'             => 'Chưa nhập ngày bắt đầu',
            'start.date'                 => 'Vui lòng nhập đúng định dạng ngày bắt đầu',
            'end.required'               => 'Chưa nhập ngày kết thúc',
            'end.date'                   => 'Vui lòng nhập đúng định dạng ngày kết thúc',
            'end.after'                  => 'vui lòng nhập ngày bắt đầu thực tập bé hơn ngày kết thúc thực tập',
        ];
      }
}
