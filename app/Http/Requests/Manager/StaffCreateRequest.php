<?php

namespace App\Http\Requests\Manager;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StaffCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return[
            'name'                    => 'required|min:5|max:255',
            'email'  =>  [
                Rule::unique('users')->where(function ($query){
                    return $query->where('deleted_at', NULL);
                }),
                'email', 'max:255','ends_with:@gmail.com,@fpt.edu.vn'
            ],

            'email_personal'  =>  [
                Rule::unique('profiles','email_personal')->where(function ($query){
                    return $query->where('deleted_at', NULL);
                }),
                'nullable', 'max:255','ends_with:@gmail.com,@fpt.edu.vn'
            ],

            'code'  =>  [
                Rule::unique('users')->where(function ($query){
                    return $query->where('deleted_at', NULL);
                }),
                'required', 'max:255'
            ],

            // 'email'                   => 'email|unique:users,email|ends_with:@gmail.com,@fpt.edu.vn|max:255',
            // 'email_personal'          => 'nullable|email|unique:profiles,email_personal|ends_with:@gmail.com,@fpt.edu.vn|max:255',
            'code'                    => 'required|unique:users|max:20',
            'ward'                    => 'nullable|exists:wards,id',
            'district'                => 'nullable|exists:districts,id',
            'city'                    => 'nullable|exists:cities,id',
            'phone'                   => 'required|max:10',
            'indo'                    => 'nullable|digits_between:9,13',
            'gender'                  => 'required|numeric|in:-1,0,1',
        ];
    }

    public function messages()
    {
       return[
            'name.required'            => "Nhập họ tên nhân viên" ,
            'email.email'              => 'Vui lòng nhập email',
            'email.unique'             => 'Email đã tồn tại',
            'email.ends_with'          => 'Email phải có đuôi là @gmail.com hoặc @fpt.edu.vn',
            'email_personal.ends_with' => 'Email phải có đuôi là @gmail.com hoặc @fpt.edu.vn',
            'email_personal.unique'    => 'Email đã tồn tại',
            'code.required'            => "Vui lòng nhập code" ,
            'code.unique'              => "Mã số nhân viên đã tồn tại",
            'code.max'                 => "Mã số nhân viên không được quá 20 kí tự" ,
            'phone.required'           => "Vui lòng nhập số điện thoại " ,
            'phone.max'                => "Số điện thoại không quá 10 số",
            'indo.digits_between'      => "số CCCD không nhỏ hơn 9 số và không lớn hơn 12 số",
       ];
    }
    // protected function failedValidation(Validator $validator)
    // {
    //     dd($validator->errors());
    // }
}
