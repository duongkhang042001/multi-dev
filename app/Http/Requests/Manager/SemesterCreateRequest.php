<?php

namespace App\Http\Requests\Manager;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SemesterCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  [
                Rule::unique('semesters')->where(function ($query){
                    return $query->where('deleted_at', NULL);
                }),
                'required', 'min:3',
            ],

            'from'        => 'required|date|after_or_equal:today',
            'to'          => 'required|date|after:from',
        ];
    }
    public function messages()
    {
        return  [
            'name.required'        => 'Vui lòng nhập tên kỳ học',
            'name.unique'        => 'Vui lòng chọn tên khác',
            'name.min'        => 'Tên kỳ phải hơn hơn 3 ký tự',
            'from.required'        => 'Vui lòng chọn thời gian bắt đầu',
            'from.date'              => 'Vui lòng nhập lại dữ liệu',
            'from.after_or_equal'    => 'Vui lòng nhập dữ liệu lớn hơn ngày hiện tại',
            'to.required'            => 'Vui lòng chọn thời gian kết thúc',
            'to.date'                => 'Vui lòng nhập lại dữ liệu',
            'to.after'               => 'Ngày kết thúc phải lớn hơn ngày bắt đầu',
        ];
    }
}
