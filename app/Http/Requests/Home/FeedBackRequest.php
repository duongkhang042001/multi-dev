<?php

namespace App\Http\Requests\Home;

use Illuminate\Foundation\Http\FormRequest;

class FeedBackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required',
            'content'   => 'required',
            'phone'     => 'required|regex:/(0)[0-9]{9}/|max:10',
            'email'     => 'required','email',
            'title'     => 'required|max:255',
        ];
    }

    public function messages()
    {
        return[
            'full_name.required'    => "Vui lòng nhập họ và tên",
            'content.required'      => "Vui lòng nhập nội dung",
            'phone.required'        => "Vui lòng nhập số điện thoại",
            'phone.regex'           => "Số điện thoại không đúng",
            'phone.max'             => "Số điện thoại không quá 10 số",
            'email.required'        => "Vui lòng nhập email",
            'email.email'           => "Email không đúng định dạng",
            'title.required'        => "Vui lòng nhập  tiêu đề",
            'title.max'             => "Tiêu đề không quá 255 ký tự",   
        ];
    }
}
