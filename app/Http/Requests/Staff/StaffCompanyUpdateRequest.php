<?php

namespace App\Http\Requests\Staff;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\UniqueProfileRule;
use App\Rules\UniqueUserRule;
use Illuminate\Support\Str;

class StaffCompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        return [
            'name'                    => 'required',
            'code'                    =>  ['required', 'max:30', new UniqueUserRule($id , 'Nhân Viên')],
            'email'                   =>  ['required', new UniqueUserRule($id , 'Nhân Viên')],
            'email_personal'          =>  ['nullable', new UniqueProfileRule($id , 'Nhân Viên')],
            'phone'                   =>  ['required', 'digits_between:9,12', new UniqueProfileRule($id , 'Nhân Viên')],
            'indo'                    =>  ['nullable', 'digits_between:9,13', new UniqueProfileRule($id , 'Nhân Viên')],
            'address'                 => 'required',
            'ward'                    => 'nullable|exists:wards,id',
            'district'                => 'nullable|exists:districts,id',
            'city'                    => 'nullable|exists:cities,id'
        ];
    }
    public function messages()
    {
        return [
            'name.required'              => "Vui lòng nhập họ và tên",
            'code.required'              => "Vui lòng nhập mã số sinh viên",
            'code.max'                   => "Mã số sinh viên không quá 30 ký tự",
            'email.required'             => 'Vui lòng nhập Email',
            'phone.required'             => "Vui lòng nhập số điện thoại",
            'phone.digits_between'       => "Số điện thoại không được nhỏ hơn 9 Và không lớn hơn 12 số",
            'indo.digits_between'        => "Số CMND không được nhỏ hơn 9 Và không lớn hơn 13 số",
            'address.required'           => "Vui lòng nhập địa chỉ",
        ];
    }
    protected function prepareForValidation()
    {
        $this->merge([
            'email' => is_string($this->email)
                ? Str::lower($this->email) : $this->email,

            'email_personal' => (!empty($this->email_personal) &&  is_string($this->email_personal))
                ? Str::lower($this->email_personal) : $this->email_personal,
        ]);
    }
}
