<?php

namespace App\Http\Requests\Staff;

use App\Rules\UniqueProfileRule;
use App\Rules\UniqueUserRule;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StaffCompanyCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                       => 'required',
            'code'                       => ['required', 'max:30', new UniqueUserRule(null , 'Nhân Viên')],
            'email'                      => ['required', 'email', 'ends_with:@gmail.com,@fpt.edu.vn', new UniqueUserRule(null , 'Nhân Viên')],
            'email_personal'             => ['nullable', 'email', 'ends_with:@gmail.com,@fpt.edu.vn',new UniqueProfileRule(null , 'Nhân Viên')],
            'phone'                      => ['required', 'digits_between:9,12',new UniqueProfileRule(null , 'Nhân Viên')],
            'indo'                       => ['nullable', 'digits_between:9,13',new UniqueProfileRule(null , 'Nhân Viên')],
            'address'                    => 'required',
            'ward'                       => 'nullable|exists:wards,id',
            'district'                   => 'nullable|exists:districts,id',
            'city'                       => 'nullable|exists:cities,id',
            'gender'                     => 'required|numeric|in:-1,0,1',
        ];
    }
    public function messages()
    {
        return [
            'name.required'              => "Vui lòng nhập họ và tên",
            'code.required'              => "Vui lòng nhập mã số sinh viên",
            'code.max'                   => "Mã số sinh viên không quá 30 ký tự",
            'email_personal.email'       => 'Vui lòng nhập đúng định dạng Email',
            'email_personal.ends_with'   => 'Email phải có đuôi là @gmail.com',
            'email.required'             => 'Vui lòng nhập Email',
            'email.email'                => 'Vui lòng nhập đúng định dạng Email',
            'email.ends_with'            => 'Email phải có đuôi là @fpt.edu.vn',
            'phone.required'             => "Vui lòng nhập số điện thoại",
            'phone.digits_between'       => "Số điện thoại không được nhỏ hơn 9 Và không lớn hơn 12 số",
            'indo.digits_between'        => "Số CMND không được nhỏ hơn 9 Và không lớn hơn 13 số",
            'address.required'           => "Vui lòng nhập địa chỉ",
           
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'email' => is_string($this->email)
                ? Str::lower($this->email) : $this->email,

            'email_personal' => (!empty($this->email_personal) &&  is_string($this->email_personal))
                ? Str::lower($this->email_personal) : $this->email_personal,
        ]);
    }
}
