<?php

namespace App\Http\Requests\Staff;
use Illuminate\Validation\Rule;
use App\Rules\UniqueCompanyRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
class CompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        return [
            'name'                       => ['required',new UniqueCompanyRule($id)],
            'code'                       => ['required','max:100',new UniqueCompanyRule($id)],
            'email'                      => ['required','email',new UniqueCompanyRule($id)],
            'phone'                      => ['required','digits_between:9,12',new UniqueCompanyRule($id)],
            'banner'                     => 'nullable|image',
            'avatar'                     => 'nullable|image',
            'tax_number'                 => ['required',new UniqueCompanyRule($id)],
            'ward'                       => 'nullable|exists:wards,id',
            'district'                   => 'nullable|exists:districts,id',
            'city'                       => 'nullable|exists:cities,id',
        ];
    }

    public function messages()
    {
       return[
            'name.required'              => "Vui lòng nhập tên doanh nghiệp",
            'code.required'              => "Vui lòng nhập mã doanh nghiệp",
            'code.max'                   => "Mã doanh nghiệp không được lớn hơn 100 ký tự",
            'email.required'             => 'Vui lòng nhập email',
            'phone.required'             => "Vui lòng nhập số điện thoại" ,
            'phone.digits_between'       => "Số điện thoại không được nhỏ hơn 9 và không lớn hơn 12 số",
            'tax_number.required'        => "Vui lòng nhập số thuế",
            'url.active_url'             => "Vui lòng nhập đúng địa chỉ trang website và địa chỉ này phải tồn tại",
            'banner.image'               => "Hình ảnh phải có dạng : jpeg, png, bmp, gif, svg",
            'avatar.image'               => "Hình ảnh phải có dạng : jpeg, png, bmp, gif, svg",
       ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'email' => is_string($this->email)
             ? Str::lower($this->email) : $this->email,
        ]);
    }
    // protected function failedValidation(Validator $validator)
    // {
    //     dd($validator->errors());
    // }
}
