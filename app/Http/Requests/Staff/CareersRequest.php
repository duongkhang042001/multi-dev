<?php

namespace App\Http\Requests\Staff;

use App\Models\Careers;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class CareersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','min:3','max:50'],
            'career_group_id'            => 'required|exists:career_groups,id,deleted_at,NULL',
        ];
    }
    public function messages()
    {
        return [
            'name.required'              => "Vui Lòng Nhập Tên Ngành",
            'name.min'                => 'Tên Phải Lớn Hơn 3 Kí Tự',
            'name.max'                => 'Tên Không Được Quá 30 Kí Tự',
            'name.unique'                => 'Ngành Đã Tồn Tại',
            'career_group_id.required'              => "Vui Lòng chọn Nhóm Ngành",
            'career_group_id.exists'                => 'Mã Nhóm Ngành Chưa Tồn Tại Trong Hệ Thống',
        ];
    }
    // protected function prepareForValidation()
    // {
    //     $this->merge([
    //         'name' => !empty($this->name) ? Str::slug($this->name) : $this->name,

    //     ]);
    // }
    private function checkSlugIsInvalid($name)
    {
        $slug= Careers::where('slug',Str::slug($name))->exists();
        return $slug;
    }
    public function withValidator(Validator $validator)
    {
        $validator->after(function ($validator) {
            if ($this->checkSlugIsInvalid($this->name)) {
                $validator->errors()->add('name', 'Username is invalid!');
            }
        });
    }
}
