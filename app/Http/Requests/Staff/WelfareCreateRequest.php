<?php

namespace App\Http\Requests\Staff;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class WelfareCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [Rule::unique('welfares')->where(function ($query) {
                return $query->where('deleted_at', NULL);
            }),'required','max:155'],
            'code' =>  'required|max:155'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên phúc lợi',
            'name.unique'   => 'Tên phúc lợi đã tồn tại',
            'name.max'      => 'Tên phúc lợi không quá 155 ký tự',
            'code.required' => 'Vui lòng nhập mã phúc lợi',
            'code.max'      => 'Mã phúc lợi không quá 155 ký tự',


        ];
    }
}
