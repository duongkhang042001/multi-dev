<?php

namespace App\Http\Requests\Staff;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;


class PostTypeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'name'         => 'required|max:255',
             'slug'         => [
                 Rule::unique('post_types')->where(function ($query) {
                return $query->where('deleted_at', NULL);
            }),'required','max:155'
        ],
            'description' => 'required|max:255',
        ];
    }

    public function messages()
    {
        return [
            'slug.required'        => "Vui lòng nhập đường dẫn",
            'slug.unique'          => "Đường dẫn đã tồn tại",
            'slug.max'             => "Đường dẫn không quá 255 kí tự",
            'name.required'        => "Vui lòng nhập tiêu đề",
            'name.max'             => "Tiêu đề không quá 255 kí tự",
            'description.required' => "Vui lòng nhập mô tả",
            'description.max'      => "Vui lòng không quá 255 kí tự"   
        ];
    }
}
