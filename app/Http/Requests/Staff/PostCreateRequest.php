<?php

namespace App\Http\Requests\Staff;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PostCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => Rule::unique('posts')->where(function ($query) {
                return $query->where('deleted_at', NULL);
            }),
            'title'                     => 'required|max:155',
            'description'               => 'required|max:855',
            'content'                   => 'required',
            'thumbnail'                 => 'required|image',
            'post-type'                 => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'             => "Vui lòng nhập tiêu đề",
            'slug.unique'                => "Đường dẫn đã tồn tại",
            'slug.required'              => "Vui lòng nhập đường dẫn",
            'slug.max'                   => "Đường dẫn không quá 255 ký tự",
            'title.max'                  => "Tiêu đề không vượt quá 155 ký tự",
            'description.required'       => "Vui lòng nhập mô tả",
            'description.max'            => "Mô tả không quá 855 ký tự",
            'content.required'           => "Vui lòng nhập chi tiết mô tả",
            'content.max'                => "Nội dung bài viết không quá 30000 ký tự",
            'thumbnail.required'         => "Vui lòng chọn hình ảnh",
            'thumbnail.image'            => "Hình Ảnh Phải Có Dạng : jpeg, png, bmp, gif, svg",
            'post-type.required'         => "Vui lòng chọn loại bài viết",
        ];
    }
}
