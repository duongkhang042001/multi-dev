<?php

namespace App\Http\Requests\Staff;

use App\Rules\UniqueCompanyRule;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
class CompanyCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                       => ['required',new UniqueCompanyRule()],
            'code'                       => ['required','max:100',new UniqueCompanyRule()],
            'email'                      => ['required','email',new UniqueCompanyRule()],
            'phone'                      => ['required','digits_between:9,12',new UniqueCompanyRule()],
            'address'                    => 'required',
            'banner'                     => 'nullable|mimes:png,jpeg,jpg',
            'avatar'                     => 'nullable|mimes:png,jpeg,jpg',
            'tax_number'                 => ['required',new UniqueCompanyRule()],
            'ward'                       => 'nullable|exists:wards,id',
            'district'                   => 'nullable|exists:districts,id',
            'city'                       => 'nullable|exists:cities,id',
        ];
    }

    public function messages()
    {
       return[
            'name.required'              => "Vui lòng nhập tên doanh nghiệp",
            'code.required'              => "Vui lòng nhập mã doanh nghiệp",
            'code.max'                   => "Mã doanh nghiệp không được lớn hơn 100 ký tự",
            'email.required'             => 'Vui lòng nhập email',
            'email.email'                => 'Vui lòng nhập đúng dạng email',
            'phone.required'             => "Vui lòng nhập số điện thoại" ,
            'phone.digits_between'       => "Số điện thoại của doanh nghiệp không đúng",
            'address.required'           => "Vui lòng nhập địa chỉ",
            'tax_number.required'        => "Vui lòng nhập mã số thuế",
            'tax_number.digits_between'  => "Mã số thuế không được nhỏ hơn 10 và không lớn hơn 13 số",
            'url.active_url'             => "Vui lòng nhập đúng địa chỉ trang website",
            'banner.mimes'               => "Hình ảnh chưa đúng định dạng : jpeg,png,jpg",
            'avatar.mimes'               => "Hình Ảnh chưa đúng định dạng : jpeg,png,jpg",
       ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'email' => is_string($this->email)
             ? Str::lower($this->email) : $this->email,
        ]);
    }
}
