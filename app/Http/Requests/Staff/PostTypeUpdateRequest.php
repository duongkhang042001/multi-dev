<?php

namespace App\Http\Requests\Staff;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PostTypeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        return [
            'name'  => 'required',
            'slug'  =>  [
                Rule::unique('post_types')->where(function ($query) use ($id) {
                    return $query->where('id', '!=', $id)->where('deleted_at', NULL);
                }),
                'required', 'max:255',
            ],
            'description' => 'required|max:150',
        ];
    }


    public function messages()
    {
        return [
            'name.required'        => "Vui lòng nhập tiêu đề",
            'slug.unique'          => "Đường dẫn đã tồn tại",
            'slug.required'        => "Vui lòng nhập đường dẫn",
            'slug.max'             => "Vui lòng không nhập quá 255 kí tự",
            'description.required' => "Vui lòng nhập mô tả",
            'description.max'      => "Vui lòng không quá 155 kí tự"
        ];
    }
}
