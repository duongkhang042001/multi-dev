<?php

namespace App\Http\Requests\Staff;

use Illuminate\Foundation\Http\FormRequest;

class ImportStudentFileRequest extends FormRequest
{
   /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
   public function authorize()
   {
       return true;
   }

   /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
   public function rules()
   {
       return [
           'file' => 'required|file|max:10000|mimes:xlsx,xls',
       ];
   }
   public function messages()
   {
      return[
          'file.required' => "File danh sách sinh viên không được để trống.",
          'file.file'     => "Vui lòng tải lại file",
          'file.max'      => "Tối đa 10000 dòng",
          'file.mimes'    => "File phải có đuôi là: xlsx, xls",
      ];
   }
}
