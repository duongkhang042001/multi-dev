<?php

namespace App\Http\Requests\Staff;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PostUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id=$this->segment(3);
        return [
            'slug'  =>  [
                Rule::unique('posts')->where(function ($query) use ($id) {
                    return $query->where('id', '!=', $id)->where('deleted_at', NULL);
                }),
                'required', 'max:255',
            ],
            'title'                     => 'required|max:155',
            'content'                   => 'required',
            'thumbnail'                 => 'nullable|image',
            'post-type'                 => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'             => "Vui lòng nhập tiêu đề",
            'title.max'                  => "Tiêu đề không quá 155 ký tự",
            'slug.required'              => "Vui lòng nhập đường dẫn",
            'slug.max'                   => "Đường dẫn không quá 255 ký tự",
            'slug.unique'                => "Đường dẫn đã tồn tại",
            'title.max'                  => "Tiêu đề không vượt quá 255 ký tự",
            'short-description.required' => "Vui lòng nhập mô tả",
            'content.required'           => "Vui lòng nhập chi tiết mô tả",
            'thumbnail.required'         => "Vui lòng chọn hình ảnh",
            'thumbnail.image'            => "Hình Ảnh Phải Có Dạng : jpeg, png, bmp, gif, svg", 
            'post-type.required'         => "Vui lòng chọn loại bài viết",
        ];
    }

    protected function failedValidation(Validator $validator)
    {
      dd($validator->errors()->all());
    }
}
