<?php

namespace App\Http\Requests\Staff;

use App\Rules\UniqueProfileRule;
use App\Rules\UniqueUserRule;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StudentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name'           => 'required',
            'code'           => ['required', 'min:5','max:10', new UniqueUserRule(null, 'Sinh Viên')],
            'email'          => ['required', 'email', 'ends_with:@gmail.com,@fpt.edu.vn', new UniqueUserRule(null, 'Sinh Viên')],
            'email_personal' => ['nullable', 'email', 'ends_with:@gmail.com,@fpt.edu.vn', new UniqueProfileRule(null, 'Sinh Viên')],
            'phone'          => ['required', 'digits_between:9,12', new UniqueProfileRule(null, 'Sinh Viên')],
            'indo'           => ['nullable', 'digits_between:9,13', new UniqueProfileRule(null, 'Sinh Viên')],
            'semester'       => ['required', 'not_in:0'],
            'career'         => 'required',
            'group_career'   => 'required',
            'address'        => 'required',
            'ward'           => 'nullable|exists:wards,id',
            'district'       => 'nullable|exists:districts,id',
            'city'           => 'nullable|exists:cities,id',
            'gender'         => 'required|numeric|in:-1,0,1',
        ];
    }

    public function messages()
    {
        return [
            'name.required'            => "Vui lòng nhập họ và tên",
            'code.required'            => "Vui lòng nhập mã số sinh viên",
            'code.min'                 => "Mã số sinh viên không ít hơn 8 ký tự",
            'code.max'                 => "Mã số sinh viên không quá 8 ký tự",
            'email_personal.email'     => 'Vui lòng nhập đúng định dạng Email',
            'email_personal.ends_with' => 'Email phải có đuôi là @gmail.com',
            'email.required'           => 'Vui lòng nhập Email',
            'email.email'              => 'Vui lòng nhập đúng định dạng Email',
            'email.ends_with'          => 'Email phải có đuôi là @fpt.edu.vn',
            'semester.required'        => "Vui lòng nhập học kỳ",
            'semester.not_in'          => "Vui lòng nhập học kỳ",
            'career.required'          => "Vui lòng chọn ngành nghề",
            'group_career.required'    => "Vui lòng chọn nhóm ngành",
            'phone.required'           => "Vui lòng nhập số điện thoại",
            'phone.digits_between'     => "Số điện thoại không được nhỏ hơn 9 Và không lớn hơn 12 số",
            'indo.digits_between'      => "Số CMND không được nhỏ hơn 9 Và không lớn hơn 13 số",
            'address.required'         => "Vui lòng nhập địa chỉ",
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'email' => is_string($this->email)
                ? trim(Str::lower($this->email)) : $this->email,

            'email_personal' => (!empty($this->email_personal) && is_string($this->email_personal))
                ? trim(Str::lower($this->email_personal)) : $this->email_personal,

            'code' => is_string($this->code)
                ? trim(Str::upper($this->code)) : $this->code,
        ]);
    }
}
