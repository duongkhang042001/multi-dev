<?php

namespace App\Http\Requests\Staff;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RecruimentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => [Rule::unique('posts')->where(function ($query) {
                return $query->where('deleted_at', NULL);
            }), 'required', 'max:255',],
            'title'                      => 'required|max:155',
            'description'                => 'required|max:155',
            'content'                    => 'required',
            'career'                     => 'required',
            'post_type'                  => 'required',
            'company_id'                 => 'required',
            'quantity'                   => 'required|numeric|min:1',
            'date_start'                 => 'required|date|after_or_equal:today',
            'date_end'                   => 'required|date|after:date_start',
            'salary_min'                 => 'required|numeric|min:0',
            'salary_max'                 => 'required|numeric|gte:salary_min',
        ];
    }
    public function messages()
    {
        return [
            'title.required'             => "Vui lòng nhập tiêu đề",
            'title.max'                  => "Tiêu đề không vượt quá 155 ký tự",
            'slug.unique'                => "Đường dẫn đã tồn tại",
            'slug.required'              => "Vui lòng nhập đường dẫn",
            'slug.max'                   => "Đường dẫn không vượt quá 155 ký tự",
            'description.required'       => "Vui lòng nhập mô tả",
            'description.max'            => "Mô tả không quá 155 ký tự",
            'content.required'           => "Vui lòng nhập chi tiết mô tả",
            'career.required'            => "Vui lòng chọn ngành",
            'post_type.required'         => "Vui lòng chọn loại bài viết",
            'company_id.required'        => "Vui lòng chọn doanh nghiệp",
            'quantity.required'          => "Vui lòng nhập số lượng",
            'quantity.numeric'           => "Vui lòng chọn số",
            'quantity.min'               => "Vui lòng nhập số lớn hơn bằng 1",
            'date_start.required'        => "Vui lòng nhập ngày bắt đầu",
            'date_start.date'            => "Vui lòng chọn đúng định dạng ngày",
            'date_start.after_or_equal'  => "Vui lòng chọn lớn hơn hoặc bằng ngày hiện tại",
            'date_end.required'          => "Vui lòng nhập ngày kết thúc",
            'date_end.date'              => "Vui lòng chọn định dạng ngày",
            'date_end.after'             => "Vui lòng chọn ngày kết thúc lớn hơn ngày bắt đầu",
            'salary_min.required'        => "Vui lòng nhập lương",
            'salary_min.numeric'         => "Vui lòng chọn số",
            'salary_min.min'             => "Vui lòng nhập số lớn hơn bằng 0",
            'salary_max.required'        => "Vui lòng nhập lương",
            'salary_max.numeric'         => "Vui lòng chọn loại bài viết",
            'salary_max.gte'             => "Lương cao nhất phải lớn hơn lương thấp",
        ];
    }
}
