<?php

namespace App\Http\Requests\Staff;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Models\Careers;
use Illuminate\Contracts\Validation\Validator;

class CareerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    private $id;
    public function rules()
    {

        return [
            'name' => Rule::unique('careers', 'slug')->where(function ($query) {
                return  $query->where('deleted_at', NULL);
            })->ignore($this->id),
            'career_group_id'            => 'required|exists:career_groups,id,deleted_at,NULL',
        ];
    }
    public function messages()
    {
        return [
            'name.required'              => "Vui Lòng Nhập Tên Ngành",
            'name.min'                   => 'Tên Phải Lớn Hơn 3 kí tự',
            'name.unique'                => 'Ngành Đã Tồn Tại',
            'career_group_id.required'   => "Vui Lòng chọn Nhóm Ngành",
            'career_group_id.exists'     => 'Mã Nhóm Ngành Chưa Tồn Tại Trong Hệ Thống',
        ];
    }
    private function checkSlugIsInvalid($name,$id)
    {
        $slug= Careers::where('slug',Str::slug($name))->where('id','!=',$id)->exists();
        return $slug;
    }
    public function withValidator(Validator $validator)
    {
        $this->id = $this->segment(3);
        $validator->after(function ($validator) {
            if ($this->checkSlugIsInvalid($this->name,$this->id)) {
                $validator->errors()->add('name', 'Vui Lòng Chon Tên Khác');
            }
        });
    }
}
