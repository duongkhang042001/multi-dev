<?php

namespace App\Http\Requests\Staff;

use App\Models\User;
use App\Rules\abc;
use App\Rules\UniqueProfileRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use phpDocumentor\Reflection\Types\Nullable;

class ProfileStaffUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = \Auth::id();

        return [
            'full_name'      => 'required',
            'gender'         => 'required|numeric',
            'birthday'       => 'required|date',
            'description'    => 'nullable|max:255',
            'email_personal' => ['nullable', new UniqueProfileRule($id)],
            'phone'          => ['required', new UniqueProfileRule($id)],
            'indo'           => ['required', new UniqueProfileRule($id)],
            'address'        => 'required|max:255',
            'ward'           => 'required|exists:wards,id',
            'district'       => 'required|exists:districts,id',
            'city'           => 'required|exists:cities,id',
        ];
    }

    public function messages()
    {
        return [
            'full_name.required'      => "vui lòng nhập họ tên của bạn",
            'gender.required'         => 'Vui lòng chọn giới tính',
            'email_personal.required' => 'Vui lòng nhập email của bạn',
            'email_personal.max'      => 'Không nhập quá 255 ký tự',
            'indo.required'           => "Vui lòng nhập số chứng minh thư của bạn",
            'phone.required'          => "Vui lòng nhập số điện thoại ",
            'address.required'        => "Vui lòng nhập địa chỉ của bạn",
            'address.max'             => "Địa chỉ quá dài",
            'city.required'           => "Vui lòng chọn Tỉnh / Thành Phố",
            'district.required'       => "Vui lòng chọn Quận / Huyện",
            'ward.required'           => "Vui lòng chọn Phường / Xã",
            'description.max'           => "Tiểu sử quá dài",
        ];
    }
    // protected function failedValidation(Validator $validator)
    // {
    //     dd($validator->errors());
    // }
}
