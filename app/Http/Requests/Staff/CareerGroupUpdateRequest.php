<?php

namespace App\Http\Requests\Staff;

use App\Models\CareerGroup;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Contracts\Validation\Validator;

class CareerGroupUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                       => 'required|min:3',
        ];

    }

    public function messages() {
        return [
            'name.required'              => "Vui Lòng Tên Nhóm Ngành",
            'name.min'                => 'Tên Phải Lớn Hơn 3 kí tự',
        ];
      }
    private function checkSlugIsInvalid($name,$id)
    {
        $slug= CareerGroup::where('slug',Str::slug($name))->where('id','!=',$id)->exists();
        return $slug;
    }
    public function withValidator(Validator $validator)
    {
        $this->id = $this->segment(3);
        $validator->after(function ($validator) {
            if ($this->checkSlugIsInvalid($this->name,$this->id)) {
                $validator->errors()->add('name', 'Vui Lòng Chon Tên Khác');
            }
        });
    }
}
