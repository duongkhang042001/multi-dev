<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;

class StudentUploadExemptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'description' => 'max:300',
            'file' => 'required|file|mimes:pdf',
        ];
    }
    public function messages()
    {
        return [
            'name.required'           => "Tên hồ sơ của bạn không được để trống.",
            'name.min'                => "Tên hồ sơ của bạn phải trên 5 kí tự.",
            'file.required'           => "File hồ sơ không được để trống.",
            'file.file'               => "Chọn đúng định dạng là tệp tin vào hệ thống.",
            'file.mimes'              => "File phải có đuôi là: pdf.",
            'description.max'         => "Vui lòng nhập dưới 300 ký tự.",
        ];
    }
}
