<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;

class StudentUploadResumeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'file' => 'required|file|mimes:pdf',
        ];
    }

    public function messages()
    {
        return [
            'name.required'           => "Tên hồ sơ của bạn không được để trống.",
            'name.min'                => "Tên hồ sơ của bạn phải trên 5 kí tự.",
            'file.required'           => "File hồ sơ không được để trống.",
            'file.file'               => "Chọn đúng định dạng là tệp tin vào hệ thống.",
            'file.mimes'              => "File phải có đuôi là: pdf.",
        ];
    }
}
