<?php

namespace App\Http\Requests\Student;

use App\Rules\UniqueProfileRule;
use App\Rules\UniqueUserRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class StudentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Auth::user()->id;
        return [
            'full_name'               => 'required',
            'code'                    =>  ['required', 'max:30', new UniqueUserRule($id, 'sinh viên')],
            'email'                   =>  ['required', new UniqueUserRule($id, 'sinh viên')],
            'email_personal'          =>  ['email','nullable', new UniqueProfileRule($id, 'sinh viên')],
            'phone'                   =>  ['required', 'digits_between:9,12', new UniqueProfileRule($id, 'sinh viên')],
            'indo'                    =>  ['nullable', 'digits_between:9,13', new UniqueProfileRule($id, 'sinh viên')],
            'address'                 => 'required',
            'ward'                    => 'required|exists:wards,id',
            'district'                => 'required|exists:districts,id',
            'city'                    => 'required|exists:cities,id',
            'birthday'                => 'required',
            'description'             => 'nullable',
            'avatar'                  => 'nullable|file|mimes:png,jpg,webp,gif',
            'gender'                  => 'required|in:0,1',
            'password'                  => ['nullable', function ($attribute, $value, $fail) {
                if (!Hash::check($value, Auth::user()->password)) {
                    $fail('Mật khẩu cũ không chính xác.');
                }
            }],
            'password_new'            => 'required_with:password',
            'password_confirmation'   => 'required_with:password_new|same:password_new',
        ];
    }

    public function messages()
    {
        return [
            'full_name.required'                    => 'Họ và tên không được để trống.',
            'code.required'                         => 'Mã số sinh viên không được để trống.',
            'code.max'                              => 'Mã số sinh viên không được quá 30 ký tự.',
            'email.required'                        => 'Email sinh viên không được để trống.',
            'email_personal.required'               => 'Email cá nhân sinh viên không được để trống.',
            'email.email'                           => 'Email cá nhân sinh viên không đúng định dạng.',
            'phone.required'                        => "Số điện thoại không được để trống.",
            'phone.digits_between'                  => "Số điện thoại không được nhỏ hơn 9 và không lớn hơn 12 số.",
            'indo.digits_between'                   => "Số CMND không được nhỏ hơn 9 và không lớn hơn 13 số.",
            'address.required'                      => "Địa chỉ không được để trống.",
            'ward.required'                         => "Vui lòng chọn phường, xã, thị trấn.",
            'ward.exists'                           => "Phường, xã, thị trấn không có trong hệ thống.",
            'district.required'                     => "Vui lòng chọn quận, huyện",
            'district.exists'                       => "Quận, huyện không có trong hệ thống.",
            'city.required'                         => "Vui lòng chọn tỉnh, thành phố.",
            'city.exists'                           => "Tỉnh, thành phố không có trong hệ thống.",
            'description.required'                  => "Vui lòng giới thiệu bản thân.",
            'gender.required'                       => "Vui lòng chọn giới tính.",
            'gender.in'                             => "Giới tính không nằm trong hệ thống.",
            'avatar.file'                           => "Hình ảnh không hợp lệ.",
            'avatar.mimes'                          => "Vui lòng chọn hình ảnh để làm ảnh đại diện.",
            'password_new.required_with'            => "Vui lòng nhập mật khẩu mới.",
            'password_confirmation.required_with'   => "Vui lòng nhập mật khẩu mới.",
        ];
    }

    // protected function failedValidation(Validator $validator)
    // {
    //     dd($validator->errors());
    // }

    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => preg_replace('/[^0-9.]/', '', $this->phone),
            'email' => is_string($this->email)
                ? Str::lower($this->email) : $this->email,
            'email_personal' => (!empty($this->email_personal) &&  is_string($this->email_personal))
                ? Str::lower($this->email_personal) : $this->email_personal,
        ]);
    }
}
