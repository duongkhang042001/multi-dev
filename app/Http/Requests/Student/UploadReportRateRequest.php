<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;

class UploadReportRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|file|mimes:pdf',
        ];
    }
    public function messages()
    {
        return [
            'file.required'           => "Nhận xét thực tập không được để trống.",
            'file.file'               => "Chọn đúng định dạng là tệp tin vào hệ thống.",
            'file.mimes'              => "Nhận xét thực tập phải định dạng là PDF.",
        ];
    }
}
