<?php

namespace App\Http\Requests\Student;

use App\Rules\UniqueCompanyRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // dd($this->request->all());
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name'                       => ['required',new UniqueCompanyRule()],
            'email'                      => ['required','email',new UniqueCompanyRule()],
            'url'                        => 'nullable|active_url',
            'short_name'                 => 'required',
            'phone'                      => ['required','regex:/(0)[0-9]{9}/',new UniqueCompanyRule()],
            'founded_at'                 => 'required',
            'careerGroup'                => 'required',
            'tax_number'                 => ['required',new UniqueCompanyRule()],
            'address'                    => 'required',
            'ward'                       => 'required|exists:wards,id',
            'district'                   => 'required|exists:districts,id',
            'city'                       => 'required|exists:cities,id',
        ];
    }

    public function messages()
    {
       return[
            'name.required'              => "Vui lòng nhập tên doanh nghiệp",
            'email.required'             => 'Vui lòng nhập email',
            'short_name.required'        => 'Vui lòng nhập tên viết tắt doanh nghiệp',
            'email.email'                => 'Vui lòng nhập đúng dạng email',
            'address.required'           => "Vui lòng nhập địa chỉ",
            'avatar.required'            => "Vui lòng nhập nhập logo",
            'founded_at.required'        => "Vui lòng nhập ngày thành lập",
            'career_group.required'      => "Vui lòng nhập lĩnh vực",
            'tax_number.required'        => "Vui lòng nhập số thuế",
            'phone.required'             => "Vui lòng nhập Số điện thoại" ,
            'phone.regex'                => 'Số điện thoại không đúng',
            'phone.digits_between'       => "Số Điện Thoại Không Được Nhỏ Hơn 9 Và Không Lớn Hơn 12 Số",
            'tax_number.digits_between'  => "Vui lòng nhập số Thuế Không Được Nhỏ Hơn 10 Và Không Lớn Hơn 13 Số",
            'url.active_url'             => "Vui lòng nhập Đúng Địa Chỉ Trang Website Và Địa Chỉ Này Phải Tồn Tại",
            'ward.required'              => "Vui lòng chọn phường, xã, thị trấn.",
            'ward.exists'                => "Phường, xã, thị trấn không có trong hệ thống.",
            'district.required'          => "Vui lòng chọn quận, huyện",
            'district.exists'            => "Quận, huyện không có trong hệ thống.",
            'city.required'              => "Vui lòng chọn tỉnh, thành phố.",
            'city.exists'                => "Tỉnh, thành phố không có trong hệ thống.",
            'careerGroup.required'       => "Vui lòng nhập lĩnh vực hoạt động",
       ];
    }

//    protected function failedValidation(Validator $validator)
//     {
//         dd($validator->errors());
//     }
}
