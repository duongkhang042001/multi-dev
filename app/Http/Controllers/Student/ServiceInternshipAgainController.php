<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Report;
use App\Models\Service;
use App\Models\User;
use App\Models\UserStudent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ServiceInternshipAgainController extends Controller
{
    public function index()
    {
        try {
            $userStudent = User::find(Auth::user()->id);
            $checkService = $userStudent->getService(SERVICE_TYPE_INTERNSHIP,true);
            $userHistory = Service::where('user_id', Auth::user()->id)->where('type', SERVICE_TYPE_INTERNSHIP)->latest()->get();                                                                                                                                                  
            return view('student.service.internshipAgain.index', compact('userStudent', 'userHistory','checkService'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            if (Auth::user()->student->status != STUDENT_STATUS_FAIL) return redirect()->back()->with(['warning' => 'Bạn không đủ điều kiện để đăng ký thực tập lại']);
                $idUser = Auth::user()->id;
                $service = Service::where('user_id', $idUser)->where('type', SERVICE_TYPE_INTERNSHIP)->latest()->first();
                if($service)
                {
                    if ($service->status == SERVICE_STATUS_PENDING)
                    {
                        return  redirect()->back()->with(['warning' => 'Đang đợi nhà trường xác nhận đăng ký thực tập lại']);
                    }
                    else{
                        $internship             = new Service;
                        $internship->user_id    = Auth::user()->id;
                        $internship->type       = SERVICE_TYPE_INTERNSHIP;
                        $internship->status     = SERVICE_STATUS_PENDING;
                        $internship->save();
                    }
                }
                else{
                    $internship             = new Service;
                    $internship->user_id    = Auth::user()->id;
                    $internship->type       = SERVICE_TYPE_INTERNSHIP;
                    $internship->status     = SERVICE_STATUS_PENDING;
                    $internship->save();
                }
            DB::commit();
            return  redirect()->back()->with(['success' => 'Đăng ký thực tập lại thành công !']);
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
    }
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $service = Service::find($id);
            if(!Auth::id() ==  $service->user_id)  abort(404);
            $service->status = SERVICE_STATUS_CANCEL;
            $service->save();
            DB::commit();
            return redirect()->back()->with('success', "Đã huỷ đăng ký thực tập lại!");
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
    }
}
