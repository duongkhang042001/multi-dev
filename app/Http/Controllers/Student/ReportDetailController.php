<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\ModelControllers\ReportDetailModel;
use App\Models\Report;
use App\Models\ReportDetail;
use App\Rules\CheckUniqueDate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ReportDetailController extends Controller
{
    public function store(Request $request)
    {
        $input = $request->all();
        $data = $input['data'];
        try {
            DB::beginTransaction();
            $userId = Auth::user()->id;
            $report = Report::where('user_id', $userId)->where('is_active', 1)->first();

            $validator = Validator::make($data, [
                'content_morning'       => 'max:1000',
                'content_afternoon'     => 'max:1000',
                'date'                  => [
                    'required',
                    'after_or_equal:' . date('Y-m-d', strtotime($report->start)),
                    new CheckUniqueDate(date('Y-m-d 00:00:00', strtotime($data['date'])),$data['id']),
                ],
                'id'                    => 'nullable|exists:report_details,id,deleted_at,NULL'
            ], [
                'content_morning.required'      => 'Nội dung làm việc buổi sáng không được để trống!',
                'content_morning.max'           => 'Nội dung làm việc buổi sáng không được quá 1000 ký tự!',
                'content_afternoon.required'    => 'Nội dung làm việc buổi chiều không được để trống!',
                'content_afternoon.max'         => 'Nội dung làm việc buổi chiều không được quá 1000 ký tự!',
                'date.required'                 => 'Ngày làm việc không được để trống!',
                'date.after_or_equal'           => 'Ngày làm việc phải lớn hơn hoặc bằng ngày bắt đầu thực tập!',
            ]);
            if ($validator->fails()) {
                return  response()->json(['error' => $validator->errors()]);
            }
            if ($report->status == REPORT_DONE || $report->status == REPORT_FINISHED) {
                return  response()->json(['checkReport' => true]);
            } else {
                $reportDetailModel  = new ReportDetailModel();
                $reportDetail       = $reportDetailModel->upsertReportDetail($data);
            }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return $reportDetail;
    }

    public function edit(Request $request)
    {
        $input = $request->only('id');
        try {
            DB::beginTransaction();
            $reportDetail = ReportDetail::findOrFail($input['id']);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return $reportDetail;
    }

    public function destroy(Request $request)
    {
        $input = $request->only('id');
        try {
            DB::beginTransaction();
            $reportDetail = ReportDetail::findOrFail($input['id']);
            $check = $reportDetail->delete();
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return $check;
    }
}
