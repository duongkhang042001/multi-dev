<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Requests\Student\StudentUpdateRequest;
use App\Models\Comment;
use App\Models\Company;
use App\Models\ModelControllers\NotifyModel;
use App\Models\ModelControllers\ReportModel;
use App\Models\ModelControllers\StudentModel;
use App\Models\RecruitmentPost;
use App\Models\RecruitmentPostDetail;
use App\Models\User;
use App\Models\UserStudent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function index()
    {
        try {
            $semester = $this->getCurrentSemester();
            $checkTimeline = $this->checkTimeLine('find');
            return view('student.users.updateProfile', compact('semester', 'checkTimeline'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function updateProfile(StudentUpdateRequest $request)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            if (!empty($request->avatar)) {
                $getAvatar              = $request->file('avatar');
                $imageUpload            = $this->upload($getAvatar);
                $input['avatar']        = $imageUpload->code;
            }
            $student = new StudentModel();
            $check = $student->updateStudent($input);
            $id = Auth::user()->id;
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        if (!empty($input['flag']) && $input['flag']) {
            $data = [
                'title'         =>  'Cập nhật thông tin tài khoản thành công !',
                'object'        => NOTIFY_STUDENT,
                'object_detail' => $id,
                'userId'        => $id,
                'url'           => '/student/profile'
            ];
            $notifyModel = new NotifyModel();
            $notifyModel->upsertNotify($data);
            return redirect()->route('student.profile')->with('success', 'Cập nhật thông tin tài khoản thành công.');
        } else {
            $data = [
                'title'         =>  'Chào mừng bạn đã đăng nhập vào hệ thống !',
                'object'        => NOTIFY_STUDENT,
                'object_detail' => $id,
                'userId'        => $id,
                'url'           => '/student/home'
            ];
            $notifyModel = new NotifyModel();
            $notifyModel->upsertNotify($data);
            return redirect()->route('student.home')->with('success', 'Cập nhật thông tin thành công.');
        }
    }

    public function studentApplicationManager()
    {
        try {
            $cancels    = RecruitmentPostDetail::whereIn('status', [RECRUITMENT_DENIED, RECRUITMENT_CANCEL, RECRUITMENT_APPROVED])->where('user_id', Auth::user()->id)->paginate($perPage = 6, $columns = ['*'], $pageName = 'cancels');
            $waitings   = RecruitmentPostDetail::where('status', RECRUITMENT_WAITING)->where('user_id', Auth::user()->id)->where('is_active', 1)->whereNull('deleted_at')->paginate($perPage = 6, $columns = ['*'], $pageName = 'waitings');
            $interviews = RecruitmentPostDetail::where('status', RECRUITMENT_INTERVIEW)->where('is_active', 1)->orderBy('interview_start', 'desc')->where('user_id', Auth::user()->id)->whereNull('deleted_at')->paginate($perPage = 6, $columns = ['*'], $pageName = 'interviews');
            $pendings   = RecruitmentPostDetail::whereIn('status', [RECRUITMENT_PENDING, RECRUITMENT_PENDING_APPROVE, RECRUITMENT_INTERVIEW_SUCCESS])->where('is_active', 1)->where('user_id', Auth::user()->id)->paginate($perPage = 6, $columns = ['*'], $pageName = 'pendings');
            $cancel     = RecruitmentPostDetail::whereIn('status', [RECRUITMENT_DENIED, RECRUITMENT_CANCEL, RECRUITMENT_APPROVED])->where('user_id', Auth::user()->id)->get();
            $waiting    = RecruitmentPostDetail::where('status', RECRUITMENT_WAITING)->where('user_id', Auth::user()->id)->where('is_active', 1)->whereNull('deleted_at')->get();
            $interview  = RecruitmentPostDetail::where('status', RECRUITMENT_INTERVIEW)->where('is_active', 1)->orderBy('interview_start', 'desc')->where('user_id', Auth::user()->id)->whereNull('deleted_at')->get();
            $pending    = RecruitmentPostDetail::whereIn('status', [RECRUITMENT_PENDING, RECRUITMENT_PENDING_APPROVE, RECRUITMENT_INTERVIEW_SUCCESS])->where('is_active', 1)->where('user_id', Auth::user()->id)->get();
            return view('student.jobs.listJobApply', compact('pendings', 'waitings', 'cancels', 'interviews', 'pending', 'waiting', 'cancel', 'interview'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }
    public function deleteRecruitmentPost(Request $request)
    {
        $input = $request->only('id');
        $id = $input['id'];
        try {
            $recruitmentPostDetail = RecruitmentPostDetail::find($id);
            if ($recruitmentPostDetail) {

                $recruitmentPostDetail->delete();
                return true;
            }
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function checkExemptionPass()
    {
        try {
            $user = Auth::user();
            $userStudent = UserStudent::where('user_id',$user->id)->first();
            $staff = User::find($userStudent->updated_by);
            return view('student.exemptionPass', compact('user','staff'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
