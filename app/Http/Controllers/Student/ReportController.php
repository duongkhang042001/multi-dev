<?php

namespace App\Http\Controllers\Student;

use App\Events\NotificationEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Student\ReportCreateRequest;
use App\Http\Requests\Student\UploadReportRateRequest;
use App\Models\ModelControllers\NotifyModel;
use App\Models\ModelControllers\ReportModel;
use App\Models\Report;
use App\Models\Semester;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ReportController extends Controller
{
    public function index()
    {
        try {
            $now    = date('Y-m-d h:i:s', time());
            $currentSemester = Semester::where('from', '<=', $now)->where('to', '>=', $now)->first();
            $timeLine = $currentSemester->timeline;
            $userId = Auth::user()->id;
            $report = Report::with(['reportDetail' => function ($query) {
                $query->orderBy('date', 'ASC');
            }])->where(['user_id' => $userId, 'is_active' => 1])->first();
            $reportDetail = null;
            if (!empty($report)) {
                $reportDetail = $report->reportDetail;
            }
            if (!empty($report->start) && !empty($report->end)) {
                $allWeek = $this->getAllWeek($report->start, $report->end);
            } else {
                $allWeek = $this->getAllWeek($report->start ?? null);
            }
            return view('student.reports.index', compact('report', 'reportDetail', 'allWeek', 'currentSemester', 'timeLine'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $data = $input['data'];
        $idUser = Auth::user()->id;
        $report = Report::where('user_id', $idUser)->where('is_active', 1)->first();
        try {
            DB::beginTransaction();
            $validator = Validator::make($data, [
                'tutor_name'                    => 'required|max:55',
                'tutor_email'                   => 'required|email',
                'tutor_phone'                   => 'required|regex:/(0)[0-9]{9}/|digits_between:9,12',
                'tutor_position'                => 'required|max:255',
                'intern_position'               => 'required|max:255',
                'start_date'                    => 'required|after_or_equal:' . date('Y-m-d', strtotime($report->registed_at)),
                'about'                         => 'nullable|max:2000',
                'function_area_activities'      => 'nullable|max:2000',
                'product_services'              => 'nullable|max:2000',
                'organization'                  => 'nullable|max:2000',
                'strategy_future'               => 'nullable|max:2000',
                'summary_activities'            => 'nullable|max:2000',
                'work'                          => 'nullable|max:2000',
                'result'                        => 'nullable|max:2000',
                'work_unfinished'               => 'nullable|max:2000',
                'problem_develop'               => 'nullable|max:2000',
                'propose'                       => 'nullable|max:2000',
                'advantage_comment'             => 'nullable|max:2000',
                'difficult_comment'             => 'nullable|max:2000',
                'general_comment'               => 'nullable|max:2000',
            ], [
                'start_date.required'           => 'Ngày bắt đầu thực tập không được để trống.',
                'start_date.after_or_equal'     => 'Ngày bắt đầu phải lớn hơn ngày bạn đã xác nhận vào doanh nghiệp.',
                'tutor_name.required'           => 'Tên người hướng dẫn không được để trống.',
                'tutor_name.max'                => 'Tên người hướng dẫn không được quá 255 kí tự.',
                'tutor_email.required'          => 'Email người hướng dẫn không được để trống.',
                'tutor_email.email'             => 'Email không đúng định dạng.',
                'tutor_phone.required'          => 'Số điện thoại người hướng dẫn không được để trống.',
                'tutor_phone.regex'             => 'Số điện thoại không đúng định dạng.',
                'tutor_phone.digits_between'    => 'Số điện thoại lớn hơn 9 và bé hơn 12 số.',
                'tutor_position.required'       => 'Vị trí người hướng dẫn không được để trống.',
                'tutor_position.max'            => 'Vị trí người hướng dẫn không được quá 255 kí tự.',
                'tutor_position.required'       => 'Vị trí thực tập không được để trống.',
                'tutor_position.max'            => 'Vị trí thực tập không được quá 255 kí tự.',
                'about.max'                     => 'Tóm lược quá trình hình thành và phát triển không được quá 2000 kí tự.',
                'function_area_activities.max'  => 'Chức năng và lĩnh vực hoạt động không được quá 2000 kí tự.',
                'product_services.max'          => 'Sản phẩm và dịch vụ không được quá 2000 kí tự.',
                'intern_position.required'      => 'Vị trí thực tập không được để trống.',
                'organization.max'              => 'Tổ chức quản lý hành chính, nhân sự không được quá 2000 kí tự.',
                'strategy_future.max'           => 'Chiến lược và phương hướng phát triển của đơn vị trong tương lai không được quá 2000 kí tự.',
                'summary_activities.max'        => 'Giới thiệu tóm tắt các hoạt động/công việc tại đơn vị thực tập không được quá 2000 kí tự.',
                'work.max'                      => 'Mô tả các công việc được phân công thực hiện hoặc được tham gia tại đơn vị không được quá 2000 kí tự.',
                'result.max'                    => 'Kết quả đạt được trong thời gian thực tập không được quá 2000 kí tự.',
                'work_unfinished.max'           => 'Những điều chưa thực hiện được không được quá 2000 kí tự.',
                'problem_develop.max'           => 'Các vấn đề cần tiếp tục nghiên cứu, phát triển không được quá 2000 kí tự.',
                'propose.max'                   => 'Đề xuất kiến nghị không được quá 2000 kí tự.',
                'advantage_comment.max'         => 'Thuận lợi trong quá trình thực tập không được quá 2000 kí tự.',
                'difficult_comment.max'         => 'Khó khăn trong quá trình thực tập không được quá 2000 kí tự.',
                'general_comment.max'           => 'Nhận xét chung trong quá trình thực tập không được quá 2000 kí tự.',

            ]);
            if ($validator->fails()) {
                return  response()->json(['error' => $validator->errors()]);
            }
            if (isset($data['end_date'])) {
                switch ($data['end_date']) {
                    case 0:
                        $endDate = Carbon::parse($data['start_date'])->addWeek(10);
                        break;
                    case 1:
                        $endDate = Carbon::parse($data['start_date'])->addWeek(12);
                        break;
                    case 2:
                        $endDate = Carbon::parse($data['start_date'])->addWeek(16);
                        break;
                    default:
                        $endDate = Carbon::parse($data['start_date'])->addWeek(10);
                        break;
                }
                $data['end_date'] = $endDate;
            }
            $report = new ReportModel();
            $check = $report->updateReport($data);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return $check;
    }
    public function reportDone()
    {
        try {
            DB::beginTransaction();
            $idUser = Auth::user()->id;
            $report = Report::where('user_id', $idUser)->where('is_active', 1)->first();
            $validator = Validator::make($report->toArray(), [
                'tutor_name'                    => 'required',
                'tutor_email'                   => 'required',
                'tutor_phone'                   => 'required',
                'tutor_position'                => 'required',
                'intern_position'               => 'required',
                'start'                         => 'required',
                'end'                           => 'required',
                'about'                         => 'required',
                'function_area_activities'      => 'required',
                'product_services'              => 'required',
                'organization'                  => 'required',
                'strategy_future'               => 'required',
                'summary_activities'            => 'required',
                'work'                          => 'required',
                'result'                        => 'required',
                'work_unfinished'               => 'required',
                'problem_develop'               => 'required',
                'propose'                       => 'required',
                'general_comment'               => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->with('fail', 'Bạn chưa hoàn thành báo cáo thực tập vui lòng cập nhật lại sau!');
            }
            $report->status = REPORT_DONE;
            $report->save();
            $input = [
                'title' =>  'Bạn đã nộp báo cáo cho doanh nghiệp !',
                'object' => NOTIFY_STUDENT,
                'object_detail' => $idUser,
                'userId' => $idUser
            ];
            $notifyModel = new NotifyModel();
            $notifyModel->upsertNotify($input);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return redirect()->back()->with('notify', 'Bạn vừa hoàn thành báo cáo thực tập. Báo cáo đã được gửi đến doanh nghiệp để xử lý.');
    }
    public function reportFinish()
    {
        try {
            DB::beginTransaction();
            $now    = date('Y-m-d h:i:s', time());
            $idUser = Auth::user()->id;
            $report = Report::where('user_id', $idUser)->where('is_active', 1)->first();
            $checkTimeline = $this->checkTimeLine('report');
            if (!empty($report->reportRate) && $report->reportRate->is_active == 1) {
                if ($checkTimeline) {
                    $report->status = REPORT_FINISHED;
                    $report->save();
                    DB::commit();
                    return redirect()->back()->with('notify', 'Bạn vừa nộp báo cáo thực tập thành công. Vui lòng đợi phản hồi từ nhà trường.');
                } else {
                    DB::commit();
                    return redirect()->back()->with('fail', 'Bạn đã trễ hạn nộp báo cáo thực tập. Vui lòng gửi yêu cầu xử lý đến nhà trường để được giải quyết!');
                }
            } else {
                DB::commit();
                return redirect()->back()->with('fail', 'Bạn không thể nộp báo cáo thực tập do doanh nghiệp chưa nhận xét báo cáo của bạn!');
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function requestResetReport(Request $request)
    {
        $data            = $request['data'];
        $idUser          = Auth::user()->id;
        $now             = date('Y-m-d h:i:s', time());
        $currentSemester = Semester::where('from', '<=', $now)->where('to', '>=', $now)->first();
        $report          = Report::where('user_id', $idUser)->where('is_active', 1)->first();
        $requestReset    = [
            'request'          => $data['request'],
            'status'           => 0,
            'created_at'       => now(),
            'current_semester' => $currentSemester->id,
            'updated_at'       => null,
        ];
        $report->request_reset = json_encode($requestReset);
        $check = $report->save();
        return $check;
    }
    public function uploadRateFile(UploadReportRateRequest $request)
    {
        try {
            DB::beginTransaction();
            $file = $request->file('file');
            if (!empty($file)) {
                $fileUpload = $this->upload($file, 'pdf');
            }
            if (!empty($fileUpload)) {
                $idUser = Auth::user()->id;
                $report = Report::where('user_id', $idUser)->where('is_active', 1)->first();
                $report->status     = REPORT_FINISHED;
                $report->rate_file  = $fileUpload->code;
                $report->save();
            }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return redirect()->back()->with('notify', 'Bạn vừa nộp nhận xét của doanh nghiệp thành công!');
    }
}
