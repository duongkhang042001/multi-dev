<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Requests\Student\StudentUploadResumeRequest;
use App\Models\Careers;
use App\Models\CV;
use App\Models\ModelControllers\ResumeModel;
use App\Models\RecruitmentPostDetail;
use App\Models\Report;
use App\Models\UserStudent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ResumeController extends Controller
{

    public function index()
    {

        try {
            DB::beginTransaction();
            $listCV    = Auth::user()->student->cv_file;
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
             abort(404);
        }
        return view('student.resumes.indexResume', compact('listCV'));
    }

    public function create()
    {
        try {
            DB::beginTransaction();
            $careers = Careers::all();
            DB::commit();
            return view('student.resumes.createResume', compact('careers'));
        } catch (\Exception $ex) {
            DB::rollBack();
             abort(404);
        }
        return redirect()->route('student.home')->with('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            if (!empty($input['check'])) {
                $resume = new ResumeModel();
                $resume->createResume($input);
            } else {
                return redirect()->back()->with('fail', 'Bạn cần chấp nhận điều khoản và dịch vụ chính sách của chúng tôi!');
            }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
             abort(404);
        }
        return redirect()->route('student.resume')->with('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    public function edit()
    {
        $idUser = Auth::user()->id;
        $resume = CV::where('created_by', $idUser)->first();
        return view('student.resumes.editResume', compact('resume'));
    }

    public function detail($code)
    {
        $filePathPublic = $this->getFilePdf($code);
        return view('student.resumes.detailResume', compact('filePathPublic'));
    }

    public function storeUploadCV(StudentUploadResumeRequest $request)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            if (!empty($input['check'])) {
                $file = $request->file('file');
                if (!empty($file)) {
                    $fileUpload = $this->upload($file, 'pdf');
                }
                if (!empty($fileUpload)) {
                    $input['code'] = $fileUpload->code;
                    $resume = new ResumeModel();
                    $resume->upLoadResume($input);
                }
            } else {
                return redirect()->back()->with('fail', "Bạn cần chấp nhận điều khoản và chính sách của chúng tôi!");
            }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
             abort(404);
        }
        return redirect()->route('student.resume')->with('success', 'Tải hồ sơ cá nhân thành công!!');
    }

    public function deleteCV($code)
    {
        try {
            DB::beginTransaction();
            $idUser = Auth::user()->id;
            $listNew = [];
            $userStudent = UserStudent::where('user_id', $idUser)->first();
            $listCV = !empty($userStudent->cv_file) ? json_decode($userStudent->cv_file) : [];
            foreach ($listCV as $key => $cv) {
                $item = json_decode($cv);
                if ($item->code == $code) {
                    array_splice($listCV, $key, 1);
                    $listNew = $listCV;
                }
            }
            $userStudent->cv_file = json_encode($listNew);
            $userStudent->save();
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
             abort(404);
        }
        return redirect()->back()->with('success', "Xoá hồ sơ cá nhân thành công!");;
    }
    function changeActive(Request $request)
    {
        $code = $request->code;
        try {
            DB::beginTransaction();
            $idUser = Auth::user()->id;
            $now    = date('Y-m-d h:i:s', time());
            $listNew = [];
            $userStudent = UserStudent::where('user_id', $idUser)->first();
            $listCV = !empty($userStudent->cv_file) ? json_decode($userStudent->cv_file) : [];
            foreach ($listCV as $key => $cv) {
                $item = json_decode($cv);
                if ($item->code == $code) {
                    $report = Report::where('user_id', $idUser)->get();
                    if (count($report) == 0) {
                        $itemCV = [
                            'code'       => $item->code,
                            'name'       => $item->name,
                            'is_active'  => $item->is_active == 0 ? 1 : 0,
                            'type'       => $item->type,
                            'created_at' => $now,
                        ];
                        array_push($listNew, json_encode($itemCV));
                    } else {
                        $itemCV = [
                            'code'       => $item->code,
                            'name'       => $item->name,
                            'is_active'  => 1,
                            'type'       => $item->type,
                            'created_at' => $now,
                        ];
                        array_push($listNew, json_encode($itemCV));
                    }
                } else {
                    $itemCV = [
                        'code'       => $item->code,
                        'name'       => $item->name,
                        'is_active'  => 0,
                        'type'       => $item->type,
                        'created_at' => $now,
                    ];
                    array_push($listNew, json_encode($itemCV));
                }
            }
            $userStudent->cv_file = json_encode($listNew);
            $check = $userStudent->save();
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
             abort(404);
        }
        return $check;
    }
}
