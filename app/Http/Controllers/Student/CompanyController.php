<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Requests\Student\CreateCompanyRequest;
use App\Models\CareerGroup;
use App\Models\ModelControllers\ProfileCompanyModel;
use App\Models\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{

    public function index()
    {

    }

    public function create()
    {
        try {
            if (!Auth::user()->student->is_accept) return redirect()->back()->with('fail', 'Bạn đã tham gia thực tập!');
            $careerGroups = CareerGroup::all();
            return view('student.company.create', compact('careerGroups'));
        } catch (\Exception $ex) {
           abort(404);
        }
    }

    public function store(CreateCompanyRequest $request)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            $companyModel = new ProfileCompanyModel();
            $company      = $companyModel->upsertCompany($input);
            if (Auth::user()->student->is_accept && $company) {
                $report = Report::create([
                    'user_id'         => Auth::id(),
                    'intern_position' => Auth::user()->student->career->name,
                    'registed_at'     => date('Y-m-d H:i:s', time()),
                    'is_outside'      => 1,
                    'is_accept'       => $this->checkTimeLine('find'),
                    'semester_id'     => $this->checkTimeLine('find') ? $this->getCurrentSemester()->id : $this->getCurrentSemester(true)->id,
                    'company_id'      => $company->id
                ]);
                if ($report) Report::where('user_id', Auth::id())->where('id', '!=', $report->id)->update(['is_active' => 0]);
                Auth::user()->student->update(['is_accept' => 0]);
            }
            DB::commit();
            return redirect()->route('student.report')->with(['success' => 'Đăng ký thành công', 'notify' => 'Thông tin doanh nghiệp của bạn đang chờ kiểm duyệt']);
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
