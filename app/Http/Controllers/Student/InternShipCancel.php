<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Report;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InternShipCancel extends Controller
{
    function index()
    {
        $user =Auth::user()->id;
        $requestCancle = Service::where('user_id', $user)->where('type',SERVICE_TYPE_INTERNSHIP_CANCEL)->latest()->get();
        $oldrequestCancle = Service::where('user_id',Auth::id())->where('type',SERVICE_TYPE_INTERNSHIP_CANCEL)->latest()->first();
        return view('student.service.internshipCancel.index', compact('requestCancle','oldrequestCancle'));
    }

    function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $oldrequestCancle = Service::where('user_id', Auth::id())->where('type', SERVICE_TYPE_INTERNSHIP_CANCEL)->latest()->first();
            if (!empty($oldrequestCancle) && $oldrequestCancle->status == SERVICE_STATUS_PENDING)  return redirect()->back()->with('warning', 'Yêu cầu bạn đang được xử  lý');
            //new
            $input = $request->all();
            $user = Auth::user()->id;
            $report = Report::where('user_id',$user)->first();
            $transcript = new Service();
            $transcript->user_id = $user;
            $transcript->report_id = $report->id;
            $transcript->description = $input['description'] ?? null;
            $transcript->type = SERVICE_TYPE_INTERNSHIP_CANCEL;
            $transcript->status = SERVICE_STATUS_PENDING;
            $transcript->save();
            DB::commit();
            return redirect()->back()->with('success', 'Gửi yêu cầu thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
