<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Requests\Student\StudentUploadExemptionRequest;
use App\Http\Requests\Student\StudentUploadResumeRequest;
use App\Models\ModelControllers\ServiceModel;
use App\Models\Report;
use App\Models\Service;
use App\Models\User;
use App\Models\UserStudent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ServiceExemptionController extends Controller
{
    public function index()
    {
        try{
            $idUser = Auth::user()->id;
            $report    = Report::where('user_id',$idUser)->first();
            return view('student.service.index',compact('report'));
        }catch(\Exception $ex)
        {
            abort(404);
        }
     
    }
    
    public function create()
    {
        try{
            DB::beginTransaction();    
            $service          = Service::where('user_id',Auth::user()->id)->where('type',SERVICE_TYPE_EXEMPTION)->latest()->first();
            $userStudent      = UserStudent::where('user_id',Auth::user()->id)->first();
            $userHistory      = Service::where('user_id', Auth::user()->id)->where('type',SERVICE_TYPE_EXEMPTION)->orderBy('created_at', 'desc')->get();
            DB::commit();
            return view('student.service.exemption.index',compact('userHistory','service','userStudent'));  
        }catch(\Exception $ex){
            DB::rollBack();
            abort(404);
        }
    }

    public function store(StudentUploadExemptionRequest $request)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            $idUser = Auth::user()->id;
            $service = Service::where('user_id', $idUser)->where('type', SERVICE_TYPE_EXEMPTION)->latest()->first();
            if($service)
            {
                if ($service->status == SERVICE_STATUS_PENDING || $service->status == SERVICE_STATUS_WAITING)
                switch ($service->status)
                {
                    case 'WAITING': return  redirect()->back()->with(['warning' => 'Bạn chưa nộp báo cáo miễn thực tập lại']);
                    case 'PENDING': return  redirect()->back()->with(['warning' => 'Vui lòng chờ đợi nhà trường xét duyệt hồ sơ miễn giảm thực tập của bạn !']);
                }
                else{
                    if (!empty($input['check'])) {
                        $file = $request->file('file');
                        if (!empty($file)) {
                            $fileUpload = $this->upload($file, 'pdf');
                        }
                        if (!empty($fileUpload)) {
                            $input['code'] = $fileUpload->code;
                            $service = new ServiceModel();
                            $service->upLoadService($input);
                        }
                    } else {
                        
                        return redirect()->back()->with('fail', "Bạn cần chấp nhận điều khoản và chính sách của chúng tôi!");
                    }
                }
            }
            else{
               
                if (!empty($input['check'])) {
                    $file = $request->file('file');
                    if (!empty($file)) {
                        $fileUpload = $this->upload($file, 'pdf');
                    }
                    if (!empty($fileUpload)) {
                        $input['code'] = $fileUpload->code;
                        $service = new ServiceModel();
                        $service->upLoadService($input);
                    }
                } else {
                    
                    return redirect()->back()->with('fail', "Bạn cần chấp nhận điều khoản và chính sách của chúng tôi!");
                }
            }
            DB::commit();
            return redirect()->route('student.service-exemption-student.create')->with('success', 'Tải hồ sơ miễn giảm thực tập thành công !');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function show($code)
    {
        $filePathPublic = $this->getFilePdf($code);
        return view('student.service.exemption.detailService', compact('filePathPublic'));
    }

    public function edit($id)
    {
        try {
            DB::beginTransaction();
            $service = Service::find($id);
            if(!Auth::id() ==  $service->user_id)  abort(404);
            $service->status = SERVICE_STATUS_PENDING;
            $service->save();
            DB::commit();
            return redirect()->back()->with('success', "Bạn đã nộp hồ sơ miễn giảm thực tập thành công!");
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }


    public function update(Request $request, $id)
    {   
        $input = $request->all();
        $input['id']=$id;
        try{
            $file = $request->file('file');
            if (!empty($file)) {
                $fileUpload = $this->upload($file, 'pdf');
            }
            if(!empty($fileUpload))
            {
                $input['code'] = $fileUpload->code;
                $service = new ServiceModel();
                $service->updateService($input,$id);
            }
            if(empty($file)){
                
                $service = new ServiceModel();
                $service->updateService($input,$id);
            }
        }catch (\Exception $ex) {
            abort(404);
        }
        return redirect()->back()->with('success', "Bạn đã chỉnh sửa hồ sơ miễn giảm thực tập thành công!");
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $service = Service::where('id', $id)->first();
            if(!Auth::id() ==  $service->user_id)  abort(404);
            $service->delete();
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return redirect()->back()->with('success', "Xoá hồ sơ miễn giảm thực tập thành công!");
    }
    public function delete($id)
    {
        try {
           
            DB::beginTransaction();
            $service = Service::where('id', $id)->first();
            if(!Auth::id() ==  $service->user_id)  abort(404);
            $service->file = null;
            $service->status = null;
            $service->save();
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return redirect()->back()->with('success', "Xoá hồ sơ miễn giảm thực tập thành công!");
    }
}
