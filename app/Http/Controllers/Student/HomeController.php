<?php


namespace App\Http\Controllers\Student;


use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\RecruitmentPost;
use App\Models\RecruitmentPostDetail;
use App\Models\Report;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    public function index()
    {
        try {
            $semester = $this->getCurrentSemester();
            $checkTimeline = $this->checkTimeLine('find');
            $idUser = Auth::user()->id;
            $report = Report::where('user_id', $idUser)->where('is_active', 1)->first();
            $checkRcm = RecruitmentPostDetail::where('user_id',$idUser)->get();
            return view('student.home', compact('semester', 'checkTimeline', 'report','checkRcm'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function registerReport(Request $request){
        $input = $request->all();
        try {
            DB::beginTransaction();
            $company = !empty($input['company_id']) ? Company::findOrFail($input['company_id']) : null;
            if (Auth::user()->student->is_accept && $company) {
                $report = Report::create([
                    'user_id' => Auth::id(),
                    'registed_at' => date('Y-m-d H:i:s', time()),
                    'is_outside' => 1,
                    'is_accept' => $this->checkTimeLine('find'),
                    'semester_id' => $this->checkTimeLine('find') ? $this->getCurrentSemester()->id : $this->getCurrentSemester(true)->id,
                    'company_id' => $input['company_id']
                ]);
                if ($report) Report::where('user_id', Auth::id())->where('id', '!=', $report->id)->update(['is_active' => 0]);
                Auth::user()->student->update(['is_accept' => 0]);
            }
            DB::commit();
            return redirect()->route('student.report')->with('success', 'Đăng ký thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            abort(404);
        }
    }
    public function serviceStudent()
    {
        try {
            return view('student.service.index');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
