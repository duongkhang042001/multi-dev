<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Transcript extends Controller
{
    function index()
    {
        try {
            $transcript = Service::where('user_id', Auth::user()->id)->where('type', SERVICE_TYPE_TRANSCRIPT)->get();
            $oldTranscript = Service::where('user_id',Auth::id())->where('type',SERVICE_TYPE_TRANSCRIPT)->latest()->first();

            return view('student.transcript.index', compact(['transcript','oldTranscript']));
        } catch (\Exception $ex) {
            abort(404);
        }
    }
    function store(Request $request)
    {
        try {
            $oldTranscript = Service::where('user_id',Auth::id())->where('type',SERVICE_TYPE_TRANSCRIPT)->latest()->first();
            if(!empty($oldTranscript) && $oldTranscript->status == SERVICE_STATUS_PENDING)  return redirect()->back()->with('warning', 'Yêu cầu bạn đang được xử  lý');

            //new
            $input = $request->all();
            $user = Auth::user()->id;
            $transcript = new Service();
            $transcript->user_id = $user;
            $transcript->description = $input['description'] ?? null;
            $transcript->type = SERVICE_TYPE_TRANSCRIPT;
            $transcript->status = SERVICE_STATUS_PENDING;
            $transcript->save();
            return redirect()->back()->with('success', 'Gửi yêu cầu thành công');
           
        } catch (\Exception $ex) {
            abort(404);
        }
    }
}
