<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected function redirectTo(){
        if(Auth::user()->role->code == MANAGER_ROLE_CODE) return route('manager.home');
        elseif(Auth::user()->role->code == STAFF_ROLE_CODE) return route('staff.home');
        elseif(Auth::user()->role->code == STUDENT_ROLE_CODE) return route('student.home');
        elseif(Auth::user()->role->code == COMPANY_ROLE_CODE) return route('company.home');
        else return route('home');
    }
}
