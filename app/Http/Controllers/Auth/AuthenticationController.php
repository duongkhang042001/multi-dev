<?php


namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Mail\VerifyEmail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Laravel\Socialite\Facades\Socialite;
use Session;

class AuthenticationController extends Controller
{
    public function check(Request $request, $guard = null)
    {
        //--------Validate Inputs-------->
        $request->validate([
            'email'    => 'required|email|exists:users,email,deleted_at,NULL',
            'password' => 'required|min:8'
        ], [
            'email.exists'            => 'Đăng nhập thất bại',
            'email.email'             => 'Sai định đạng email!',
            'email.required'          => 'Vui lòng nhập tài khoản email',
            'password.required'       => 'Vui lòng nhập password',
            'password.digits_between' => 'Đăng nhập thất bại',
        ]);
        try {
            $guard = $request['guard'] ?? null;
            if (!$guard) return redirect()->route('home')->with('fail', 'Lỗi bảo mật!, Vui lòng thử lại sau!');
            $input = $request->only('email', 'password');
            $user  = User::where('email', $input['email'])
                ->whereRelation('role', function ($q) use ($guard) {
                    if ($guard == 'admin') $q->where('code', STAFF_ROLE_CODE)->orWhere('code', MANAGER_ROLE_CODE);
                    else $q->where('code', strtoupper($guard));
                })
                ->where('is_active', 1)
                ->whereNull('deleted_at')->first();
            if (!empty($user) && Hash::check(trim($input['password']), $user->password)) {
                if (!$user->hasVerifiedEmail()) {
                    $this->sendVerifyEmail($user);
                    if (in_array($user->role->code, [MANAGER_ROLE_CODE, STAFF_ROLE_CODE])) return redirect()->route('admin.confirm')->with('confirm', $user->email);
                    return redirect()->back()->with('confirm', 'Kiểm tra email xác thực!');
                }
                $user->logged_at = date('Y-m-d H:i:s', time());
                $user->save();
                switch ($user->role->code) {
                    case MANAGER_ROLE_CODE:
                        $this->logoutWithDiffLogin();
                        Auth::guard('manager')->login($user);
                        return redirect()->route('manager.home')->with('notify', 'Đăng nhập thành công');
                    case  STAFF_ROLE_CODE:
                        $this->logoutWithDiffLogin();
                        Auth::guard('staff')->login($user);
                        return redirect()->route('staff.home')->with('notify', 'Đăng nhập thành công');
                    case STUDENT_ROLE_CODE:
                        $this->logoutWithDiffLogin();
                        Auth::guard('student')->login($user);
                        return redirect()->route('student.home')->with('notify', 'Đăng nhập thành công');
                    case COMPANY_ROLE_CODE:
                        $this->logoutWithDiffLogin();
                        Auth::guard('company')->login($user);
                        return redirect()->route('company.home')->with('notify', 'Đăng nhập thành công');
                    default:
                        break;
                }
            }
            return redirect()->back()->with('fail', 'Đăng nhập thất bại!');
        } catch (\Exception $ex) {
            abort(404);
//            dd($ex->getMessage());
        }
    }
    private function sendVerifyEmail($user, $method = VERIFY_ACCOUNT)
    {
        $token                = Str::random(64);
        $user->remember_token = $token;
        $user->save();
        $guard = $user->role->code;
        if (in_array($guard, [MANAGER_ROLE_CODE, STAFF_ROLE_CODE])) $guard = 'admin';
        $data = [
            'method' => $method,
            'name'   => $user->name,
            'email'  => $user->email,
            'code'   => $user->code,
            'token'  => $token,
            'guard'  => strtolower($guard)
        ];
        Mail::to($user->email)->send(new VerifyEmail($data));
        return true;
    }

    public function forgotPassword(Request $request)
    {
        $request->validate([
            'email' =>
            [
                'required', 'email',
                Rule::exists('users', 'email')->where(function ($query) {
                    return $query->where('is_active', 1)
                        ->whereNull('deleted_at')
                        ->whereNotNull('email_verified_at');
                }),
            ]
        ]);
        $input = $request->all();
        try {
            $user = User::where('email', $input['email'])
                ->where('deleted_at', null)
                ->where('is_active', 1)
                ->first();
            if (!empty($user)) {
                $sendEmail = $this->sendVerifyEmail($user, FORGOT_PASSWORD);
                if ($sendEmail) {
                    $guard = $user->role->code;
                    if (in_array($guard, [MANAGER_ROLE_CODE, STAFF_ROLE_CODE])) $guard = 'admin';
                    return redirect()->route($guard . '.confirm')->with('forgot', $user->email);
                }
            }
            return redirect()->back()->with('fail', 'Xác thực thất bại!');
        } catch (\Exception $exception) {
            abort(404);
//            dd($exception->getMessage());
        }
    }

    public function changePassword(Request $request)
    {
        $request->validate(
            [
                'password'              => 'required|min:8',
                'password_confirmation' => 'required_with:password|same:password',
            ],
            [
                'password.required'                   => 'Vui lòng nhập mật khẩu mới',
                'password.min'                        => 'Ít nhất 8 ký tự',
                'password.confirmed'                  => 'Nhập lại mật khẩu ko chính xác',
                'password_confirmation.same'          => 'Nhập lại mật khẩu ko chính xác',
                'password_confirmation.required_with' => 'Vui lòng nhập lại mật khẩu mới',
            ]
        );
        try {
            $id   = !empty(Session::get('user_id')) ? last(Session::get('user_id')) : null;
            $user = User::find($id);
            if ($user) {
                $user->update(['password' => Hash::make($request->password)]);
                Session::flush();
                $this->loginWithRole($user);
            }
            return redirect()->back()->with('fail', 'Đã xảy ra xự cố');
        } catch (\Exception $exception) {
            abort(404);
//            dd($exception->getMessage());
        }
    }

    public function register(Request $request)
    {
        $request->validate(
            [
                'email'    => ['required', 'email', Rule::unique('users')->where(function ($query) {
                    return $query->whereNull('deleted_at');
                })],
                'password' => 'required|min:8',
            ],
            [
                'email.required'    => 'Vui lòng nhập email!',
                'email.email'       => 'Vui lòng nhập đúng định dạng email!',
                'email.unique'      => 'Email đã tồn tại!',
                'password.required' => 'Vui lòng nhập mật khẩu!',
            ]
        );
        try {
            $user = new User();
            $user = $user->create([
                'email'    => $request->email,
                'password' => Hash::make($request->password),
                'code'     => 'COMPANY_' . time(),
                'role_id'  => COMPANY_ROLE_ID
            ]);
            if ($user) $sendEmail = $this->sendVerifyEmail($user);
            if ($user && $sendEmail) return redirect()->back()->with('confirm', 'Vui lòng kiểm tra email xác thực!');
            return redirect()->back()->with('fail', 'Đã xảy ra lỗi vui lòng thử lại sau!');
        } catch (\Exception $exception) {
            abort(404);
//            dd($exception->getMessage());
        }
    }

    public function verified($guard, $code, $method, $token)
    {
        try {
            $user = User::where('code', $code)
                ->where('remember_token', $token)
                ->where('deleted_at', null)
                ->first();
            if (!empty($user) && !empty($guard)) {
                if ($method == FORGOT_PASSWORD) {
                    Session::push('user_id', $user->id);
                    return redirect()->route($guard . '.change.show');
                } else {
                    $user->markEmailAsVerified();
                    $this->loginWithRole($user);
                }
            }
            if (!empty($guard)) return redirect()->route($guard . '.login')->with('fail', 'Đăng nhập thất bại!');
            return redirect()->route('home');
        } catch (\Exception $exception) {
            abort(404);
//            dd($exception->getMessage());
        }
    }

    private function loginWithRole($user)
    {
        $roleCode = $user->role->code ?? null;
        if (!$roleCode) return redirect()->route('home');
        $user->logged_at      = date('Y-m-d H:i:s', time());
        $user->remember_token = base64_encode(Hash::make('finishForgot'));
        $user->save();
        switch ($roleCode) {
            case MANAGER_ROLE_CODE:
                $this->logoutWithDiffLogin();
                Auth::guard('manager')->login($user);
                return redirect()->route('manager.home')->with('notify', 'Đăng nhập thành công');
            case STAFF_ROLE_CODE:
                $this->logoutWithDiffLogin();
                Auth::guard('staff')->login($user);
                return redirect()->route('staff.home')->with('notify', 'Đăng nhập thành công');
            case STUDENT_ROLE_CODE:
                $this->logoutWithDiffLogin();
                Auth::guard('student')->login($user);
                return redirect()->route('student.home')->with('notify', 'Đăng nhập thành công');
            case COMPANY_ROLE_CODE:
                $this->logoutWithDiffLogin();
                Auth::guard('company')->login($user);
                return redirect()->route('company.home')->with('notify', 'Đăng nhập thành công');
            default:
                return redirect()->to('/home');
        }
    }

    public function logout(Request $request, $guard = null)
    {
        try {
            $guard = $request['guard'] ?? $guard;
            if ($guard == 'manager') Auth::guard('manager')->logout();
            elseif ($guard == 'staff') Auth::guard('staff')->logout();
            elseif ($guard == 'student') Auth::guard('student')->logout();
            elseif ($guard == 'company') Auth::guard('company')->logout();
            else Auth::guard('web')->logout();

            if ($guard == 'student') return redirect('/student/login');
            elseif ($guard == 'company') return redirect('/company/login');
            elseif ($guard == 'manager' || $guard == 'staff') return redirect('/admin');
            else return redirect('/');
        } catch (\Exception $ex) {
            abort(404);
//            dd($ex->getMessage());
        }
    }

    public function lockScreen(Request $request, $guard = null)
    {
        try {
            $guard = $request['guard'] ?? $guard;
            if (empty($guard)) return redirect('/');
            Session(['user' => [
                'email'  => Auth::user()->email,
                'name'   => Auth::user()->profile->last_name ?? Auth::user()->name,
                'avatar' => Auth::user()->getAvatar(),
                'guard'  => $guard
            ]]);
            if ($guard == 'manager') Auth::guard('manager')->logout();
            elseif ($guard == 'staff') Auth::guard('staff')->logout();
            elseif ($guard == 'student') Auth::guard('student')->logout();
            elseif ($guard == 'company') Auth::guard('company')->logout();
            else Auth::guard('web')->logout();

            if ($guard == 'manager' || $guard == 'staff') return redirect()->route('admin.lockScreen.index')->with('notify', 'Vui lòng nhập mật khẩu để đăng nhập!');
            else return redirect('/' . $guard . '/login');
        } catch (\Exception $ex) {
            abort(404);
//            dd($ex->getMessage());
        }
    }

    public function redirect($provider)
    {
        Session::push('url', Session::previousUrl());
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        try {
            $userGG      = Socialite::driver($provider)->stateless()->user();
            $previousUrl = !empty(Session::get('url')) ? last(Session::get('url')) : null;
            if (!$previousUrl || $previousUrl == route('social.callback', $provider)) {
                return redirect()->route('home')->with('fail', 'Đã xảy ra xự cố, vui lòng thử lại sau!');
            }
            $appUrl = \Config::get('app.url');
            $guard  = "";
            if (in_array($previousUrl, [$appUrl . '/admin', $appUrl . '/admin/login'])) $guard = 'admin';
            elseif (in_array($previousUrl, [$appUrl . '/student', $appUrl . '/student/login'])) $guard = STUDENT_ROLE_CODE;
            elseif (in_array($previousUrl, [$appUrl . '/company', $appUrl . '/company/login'])) $guard = COMPANY_ROLE_CODE;
            else  redirect()->to($previousUrl)->with('fail', 'Đăng nhập thất bại!');
            $user = User::with('role')
                ->where('email', $userGG->email)
                /* ->where(function ($query) use ($provider, $userGG){
                     $query->where(function($query) use ($userGG){
                         $query->whereNull('email_verified_at')->where('email', $userGG->email);
                              })->orWhere(function($query) use ($provider, $userGG){
                         $query->whereNotNull('email_verified_at')->where('provider', $provider)->where('provider_id', $userGG->id);
                              });
                 })*/
                ->where(function ($query) use ($guard) {
                    if ($guard == 'admin') {
                        $query->whereRelation('role', 'code', MANAGER_ROLE_CODE)
                            ->orWhereRelation('role', 'code', STAFF_ROLE_CODE);
                    } else $query->whereRelation('role', 'code', $guard);
                })
                ->whereNull('deleted_at')
                ->where('is_active', 1)
                ->first();
            if (!$user) return redirect($previousUrl)->with('fail', 'Đăng nhập thất bại!');
            if ($user->email_verified_at == null) $user->markEmailAsVerified();
            $user->provider    = $provider;
            $user->provider_id = $userGG->id;
            $user->logged_at   = date('Y-m-d H:i:s', time());
            $user->save();
            Session::forget('url');
            switch ($user->role->code) {
                case MANAGER_ROLE_CODE:
                    $this->logoutWithDiffLogin();
                    Auth::guard('manager')->login($user);
                    return redirect()->route('manager.home')->with('notify', 'Đăng nhập thành công');
                case  STAFF_ROLE_CODE:
                    $this->logoutWithDiffLogin();
                    Auth::guard('staff')->login($user);
                    return redirect()->route('staff.home')->with('notify', 'Đăng nhập thành công');
                case STUDENT_ROLE_CODE:
                    $this->logoutWithDiffLogin();
                    Auth::guard('student')->login($user);
                    return redirect()->route('student.home')->with('notify', 'Đăng nhập thành công');
                case COMPANY_ROLE_CODE:
                    $this->logoutWithDiffLogin();
                    Auth::guard('company')->login($user);
                    return redirect()->route('company.home')->with('notify', 'Đăng nhập thành công');
                default:
                    return redirect()->to('/home');
            }
        } catch (\Exception $ex) {
            abort(404);
//            dd($ex->getMessage());
        }
    }
    private function logoutWithDiffLogin(){
        foreach(array_keys(config('auth.guards')) as $guard){
            if(auth()->guard($guard)->check()) auth()->guard($guard)->logout();
        }
    }
}
