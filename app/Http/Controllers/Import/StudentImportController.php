<?php

namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use App\Http\Requests\Staff\ImportStudentFileRequest;
use App\Imports\StudentImport;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\HeadingRowImport;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

class StudentImportController extends Controller
{
    protected $_headerFormat;
    protected $_header;

    public function __construct()
    {
        $this->_header       = [
            0 => "STT",
            1 => "MSSV",
            2 => "Họ và Tên",
            3 => "Giới tính",
            4 => "Ngày tháng năm sinh",
            5 => "Email",
            6 => "Số điện thoại",
            7 => "Kỳ hiện tại",
            8 => "Ngành học",
            9 => "Trạng thái học",
        ];
        $this->_headerFormat = [
            0 => "stt",
            1 => "mssv",
            2 => "ho_va_ten",
            3 => "gioi_tinh",
            4 => "ngay_thang_nam_sinh",
            5 => "email",
            6 => "so_dien_thoai",
            7 => "ky_hien_tai",
            8 => "nganh_hoc",
            9 => "trang_thai_hoc",
        ];
    }

    public function index()
    {
        return view('staff.student.importStudent');
    }

    public function import(ImportStudentFileRequest $request)
    {

        try {
            DB::beginTransaction();
            $file     = $request->file('file')->store('import');
            $headings = (new HeadingRowImport)->toArray($file);
            if (array_diff($this->_headerFormat, $headings[0][0])) {
                return back()->withErrors(['file' => 'File import không đúng!'])->with(['headerError' => $this->_header]);
            }
            $import = new StudentImport();
            $import->import($file);

            DB::commit();

            return redirect()->back()->with('success', 'Thêm danh sách sinh viên thành công!');

        } catch (\Throwable $th) {
            DB::rollBack();
            if (!empty($th->validator)) {
                return redirect()->route('staff.import.student.show')->with('failures', $th->validator->errors()->messages()['errors'])->withErrors(['file' => 'Lỗi dữ liệu vui lòng kiểm tra lại']);
            }
            abort(404);
        }
    }
}
