<?php

namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use App\Http\Requests\Import\StaffImportRequest;
use App\Imports\StaffImport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\HeadingRowImport;


class StaffImportController extends Controller
{
    protected $_headerFormat;
    protected $_header;

    public function __construct()
    {
        $this->_header       = [
            0 => "STT",
            1 => "Mã nhân viên",
            2 => "Họ và Tên",
            3 => "Ngày tháng năm sinh",
            4 => "Email",
            5 => "Số điện thoại",
            6 => "CMND/CCCD",
        ];
        $this->_headerFormat = [
            0 => "stt",
            1 => "ma_nhan_vien",
            2 => "ho_va_ten",
            3 => "ngay_thang_nam_sinh",
            4 => "email",
            5 => "so_dien_thoai",
            6 => "cmndcccd",
        ];
    }

    public function index()
    {
        return view('manager.staff.import');
    }

    public function store(StaffImportRequest $request)
    {

        try {
            DB::beginTransaction();
            $file     = $request->file('file')->store('import');
            $headings = (new HeadingRowImport)->toArray($file);
            if (array_diff($this->_headerFormat, $headings[0][0])) {
                return back()->withErrors(['file' => 'File import không đúng!'])->with(['headerError' => $this->_header]);
            }
            $import = new StaffImport();
            $import->import($file);
            DB::commit();
            return back()->with('success', 'Thêm danh sách nhân viên thành công!');
        } catch (\Throwable  $th) {
            DB::rollBack();
            if (!empty($th->validator)) {
                return redirect()->route('manager.import.staff.index')->with('failures', $th->validator->errors()->messages()['errors'])->withErrors(['file' => 'Lỗi dữ liệu vui lòng kiểm tra lại']);
            }
            abort(404);
        }
    }
}
