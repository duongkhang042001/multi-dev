<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\ProfileCompanyRequest;
use App\Http\Requests\Company\UpdateProfileRequest;
use App\Models\ModelControllers\ProfileCompanyModel;
use App\Models\Profile;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    public function storeProfile(ProfileCompanyRequest $request)
    {
        $input = $request->all();

        try {
            DB::beginTransaction();
            $profileCompany =  new ProfileCompanyModel();
            if (!empty($request->avatar)) {
                $getAvatar = $request->file('avatar');
                $imageAvatar = $this->upload($getAvatar);
                $input['avatar'] = $imageAvatar->code;
            }
            $profileCompany->upsertProfile($input);
            DB::commit();
            return redirect()->route('company.initial.company');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function detailProfile($id)
    {
        try {
            if (Auth::guard('company')->user()->id == $id) {
                $user = User::find($id);
                return view('company.users.profile', compact('user'));
            } else {
                throw new Exception('Error');
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function updateProfile(UpdateProfileRequest $request, $id)
    {

        $input = $request->all();
        $input['id'] = $id;
        try {
            DB::beginTransaction();
            $profileCompany =  new ProfileCompanyModel();
            if (!empty($request->avatar)) {
                $getAvatar = $request->file('avatar');
                $imageAvatar = $this->upload($getAvatar);
                $input['avatar'] = $imageAvatar->code;
            }
            $profileCompany->upsertProfile($input, true);
            DB::commit();
            return redirect()->route('company.home');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
