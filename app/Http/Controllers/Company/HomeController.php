<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CreateCompanyRequest;
use App\Http\Requests\Company\UpdateCompanyProfile;
use App\Http\Requests\Company\UpdateCompanyRequest;
use App\Models\CareerGroup;
use App\Models\Careers;
use App\Models\Company;
use App\Models\ModelControllers\ProfileCompanyModel;
use App\Models\RecruitmentPost;
use App\Models\RecruitmentPostDetail;
use App\Models\Report;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class HomeController extends Controller
{

    public function index()
    {
        try {
            $countRcm = RecruitmentPost::where('company_id', Auth::guard('company')->user()->company->id)->count();
            $countRcmDetail = RecruitmentPostDetail::whereHas('recruitmentPost.company',function($query){
                $query->where('company_id',Auth::guard('company')->user()->company->id);
            })->count();
            $countReport = Report::where('company_id', Auth::guard('company')->user()->company->id)->count();
            return view('company.home', compact('countRcm','countRcmDetail','countReport'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
    public function create()
    {
        try {
            $careerGroups = CareerGroup::all();
            $career = Careers::all();
            return view('company.isLogged.createCompany', compact('careerGroups', 'career'))
                ->with('notify', 'Vui lòng cập nhật thông tin doanh nghiệp!');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function detail()
    {
        // try {
        //     return view('company.profile');
        // } catch (\Exception $ex) {
        //      abort(404);
        // }
    }



    public function storeCompany(CreateCompanyRequest $request)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            $company        = new ProfileCompanyModel();
            if (!empty($request->banner)) {
                $getBanner = $request->file('banner');
                $imageBanner = $this->upload($getBanner);
                $input['banner'] = $imageBanner->code;
            }
            if (!empty($request->avatar)) {
                $getAvatar = $request->file('avatar');
                $imageAvatar = $this->upload($getAvatar);
                $input['avatar'] = $imageAvatar->code;
            }
            $company->upsertCompany($input);
            DB::commit();
            return redirect()->route('company.home')->with('success', 'Cập nhật thông tin thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function editCompany($id)
    {
        try {
            $company = Company::findOrFail($id);
            $careerGroups = CareerGroup::all();
            $career = Careers::all();
            return view('company.editCompanyProfile', compact('company', 'careerGroups', 'career'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function updateCompany(UpdateCompanyProfile $request, $id)
    {
        $input = $request->all();
        $input['id'] = $id;
        try {
            DB::beginTransaction();
            $company        = new ProfileCompanyModel();
            if (!empty($request->banner)) {
                $getBanner = $request->file('banner');
                $imageBanner = $this->upload($getBanner);
                $input['banner'] = $imageBanner->code;
            }
            if (!empty($request->avatar)) {
                $getAvatar = $request->file('avatar');
                $imageAvatar = $this->upload($getAvatar);
                $input['avatar'] = $imageAvatar->code;
            }
            $company->upsertCompanyProfile($input, true);
            DB::commit();
            return redirect()->route('company.home');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
