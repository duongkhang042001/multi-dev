<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CreateReportRateRequest;
use App\Models\ModelControllers\ReportRateModel;
use App\Models\Report;
use App\Models\ReportDetail;
use App\Models\ReportRate;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $companyId      = Auth::user()->company_id;
            $internStudent  = Report::where('company_id', $companyId)->where('status',REPORT_PENDING)->paginate($perPage = 5, $columns = ['*'], $pageName = 'cancel');
            $reportRate     = Report::where('company_id', $companyId)->where('is_active',1)->whereIn('status', [REPORT_DONE, REPORT_FINISHED])->paginate($perPage = 5, $columns = ['*'], $pageName = 'cancel');
            $reportRatePass = Report::where('company_id', $companyId)->where('is_active',1)->where('status', REPORT_PASS)->paginate($perPage = 5, $columns = ['*'], $pageName = 'cancel');
            $reportRateFail = Report::where('company_id', $companyId)->whereRelation('services','type',SERVICE_TYPE_INTERNSHIP_CANCEL)->paginate($perPage = 5, $columns = ['*'], $pageName = 'cancel');

            return view('company.interStudent.index', compact('internStudent', 'reportRate', 'reportRatePass', 'reportRateFail'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateReportRateRequest $request)
    {
        $input = $request->all();
        try {
            $reportRateModel = new ReportRateModel();
            $reportRateModel->upsertReportRateCompany($input);
            return redirect()->route('company.intern-student.index')->with('success', 'Nhận xét thành công');
        } catch (\Exception $ex) {
             abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        try {
            $report = Report::with(['reportDetail' => function ($query) {
                $query->orderBy('date', 'ASC');
            }])->where('id', $id)->first();
            $reportDetail = $report->reportDetail;
            if (!empty($report->start) && !empty($report->end)) {
                $allWeek = $this->getAllWeek($report->start, $report->end);
            } else {
                $allWeek = $this->getAllWeek($report->start);
            }
            if ($report->status == REPORT_PENDING) {
                return redirect()->back()->with('warning', 'Sinh viên chưa hoàn thành báo cáo');
            } else {
                return view('company.interStudent.report', compact('report', 'reportDetail', 'allWeek'));
            }
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateReportRateRequest $request, $id)
    {
        $input = $request->all();
        $input['id'] = $id;
        try {
            $reportRateModel = new ReportRateModel();
            $reportRateModel->upsertReportRateCompany($input, true);
            return redirect()->back()->with('success', 'Chỉnh nhận xét thành công');
        } catch (\Exception $ex) {
             abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateStatus($id)
    {
        $reportRate = ReportRate::where('report_id', $id)->first();
        try {
            if (
                $reportRate->content == null || $reportRate->advantages == null ||
                $reportRate->defect == null  || $reportRate->attitude_point == null ||
                $reportRate->work_point == null
            ) {
                return redirect()->back()->with('warning', 'Thông tin đánh giá chưa đầy đủ');
            } else {
                $reportRate->is_active = 1;
                $input = [
                    'title' =>  'Bạn đã được doanh nghiệp đánh giá báo cáo thực tập',
                    'object' => NOTIFY_COMPANY_TO_STUDENT,
                    'userId' => $reportRate->report->user_id,
                    'url' => '/student/report',
                ];
                $this->createNotification($input);
                $reportRate->save();
                return redirect()->back()->with('success', 'Đánh giá thành công');
            }
        } catch (\Exception $ex) {
             abort(404);
        }
    }

    public function updateCancel(Request $request, $id)
    {
        $input       = $request->all();
        $input['id'] = $id;
        $userId      = Auth::user()->id;
        $report      = Report::find($id);
        try {
            $requestCancel = new Service();
            $requestCancel->description = $input['status_reason'];
            $requestCancel->type = SERVICE_TYPE_INTERNSHIP_CANCEL;
            $requestCancel->status = SERVICE_STATUS_PENDING;
            $requestCancel->user_id = $userId; 
            $requestCancel->report_id = $report->id;
            $requestCancel->save();
            return redirect()->back()->with('success', 'Gửi yêu cầu hủy thành công');
        } catch (\Exception $ex) {
             abort(404);
        }
    }
}
