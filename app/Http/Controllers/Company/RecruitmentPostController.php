<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CreateRecruitmentPostRequest;
use App\Http\Requests\Company\UpdateRecruitmentPostRequest;
use App\Models\Careers;
use App\Models\Company;
use App\Models\ModelControllers\NotifyModel;
use App\Models\ModelControllers\ProfileCompanyModel;
use App\Models\Post;
use App\Models\RecruitmentPost;
use App\Models\RecruitmentPostDetail;
use App\Models\Welfare;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RecruitmentPostController extends Controller
{
    public function recruitmentPost()
    {
        try {
            $userId = Auth::user()->company->id;
            $recruitmentPost = RecruitmentPost::where('company_id', $userId)->paginate(6);
            return view('company.recruitmentPost.index', compact('recruitmentPost'));
        } catch (\Exception $ex) {
             abort(404);
        }
    }

    public function createdRecruitmentPost()
    {
        try {
            $careers = Careers::all();
            $welfares = Welfare::all();
            return view('company.recruitmentPost.create', compact('careers', 'welfares'));
        } catch (\Exception $ex) {
             abort(404);
        }
    }

    public function detailRecruitmentPost($id)
    {
        try {
            $recruitmentPost = RecruitmentPost::find($id);
            $careers = Careers::all();
            $welfares = Welfare::all();
            $countUser = RecruitmentPostDetail::where('recruitment_post_id', $id)->where('status',RECRUITMENT_PENDING)->count();
            $listUser = RecruitmentPostDetail::where('recruitment_post_id', $id)->where('status',RECRUITMENT_PENDING)->get();
            $statusUser = RecruitmentPostDetail::where('recruitment_post_id', $id)->where('status', RECRUITMENT_APPROVED)->count();
            $listUserActive = RecruitmentPostDetail::where('recruitment_post_id', $id)->where('status', RECRUITMENT_APPROVED)->get();
            if ($statusUser >= $recruitmentPost->quantity) {
                $recruitmentPost->is_active = 0;
                $recruitmentPost->save();
            }
            return view('company.recruitmentPost.detail', compact('recruitmentPost', 'careers', 'welfares', 'countUser', 'listUser', 'statusUser', 'listUserActive'));
        } catch (\Exception $ex) {
             abort(404);
        }
    }

    public function storeRecruitmentPost(CreateRecruitmentPostRequest $request)
    {
        $input = $request->all();

        try {
            DB::beginTransaction();
            if (!empty($input['check']) && $input['check'] == 1) {
                $company = Company::find(Auth::user()->company_id);
                $input['address'] = $company->address;
                $input['ward']    = $company->ward_id;
                $input['district'] = $company->district_id;
                $input['city']    = $company->city_id;
            }
            $recruitmentPost = new ProfileCompanyModel();
            $recruitmentPost->upsertRecruitmentPost($input);
            $dataNoti = [
                'title' =>  'Thêm bài đăng tuyển thành công.',
                'object' => NOTIFY_COMPANY,
                'object_detail' => Auth::user()->company->id,
            ];
            $this->createNotification($dataNoti);
            DB::commit();
            return redirect()->route('company.recruitment')->with('success', 'Thêm bài đăng tuyển thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
//            dd($ex->getMessage());
        }
    }

    public function editdRecruitmentPost($id)
    {
        try {
            $recruitmentPost = RecruitmentPost::find($id);
            $careers = Careers::all();
            $welfares = Welfare::all();
            return view('company.recruitmentPost.edit', compact('recruitmentPost', 'careers', 'welfares'));
        } catch (\Exception $ex) {
             abort(404);
        }
    }

    public function updateRecruitmentPost(UpdateRecruitmentPostRequest $request, $id)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            $input['id'] = $id;
            $recruitmentPost = new ProfileCompanyModel();
            $recruitmentPost->upsertRecruitmentPost($input, true);
            DB::commit();
            return redirect()->route('company.recruitment')->with('success', 'Chỉnh bài đăng tuyển thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
             abort(404);
        }
    }

    public function deleteRecruitmentPost($id)
    {
        try {
            $recruitmentPost = RecruitmentPost::find($id);
            $post = Post::where('id', $recruitmentPost->post_id)->first();
            $recruitmentPost->delete();
            $post->delete();
            return redirect()->route('company.recruitment')->with('warning', 'Xóa bài đăng tuyển thành công');
        } catch (\Exception $ex) {
             abort(404);
        }
    }

    public function updatedRecruimentDetail(Request $request, $id)
    {
        $input = $request->all();
        $input['id'] = $id;
        try {
            DB::beginTransaction();
            $company = new ProfileCompanyModel();
            $company->upsertRecruitmentPostDetail($input, true);
            DB::commit();
            return redirect()->back()->with('success', 'Cập nhật thông tin thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
             abort(404);
        }
    }
}
