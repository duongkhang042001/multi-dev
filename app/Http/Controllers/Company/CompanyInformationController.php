<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CreateCompanyRequest;
use App\Models\CareerGroup;
use App\Models\Careers;
use App\Models\Company;
use App\Models\ModelControllers\ProfileCompanyModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            // $company = Company::find($id);
            return view('company.profile');
        } catch (\Exception $ex) {
             abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $careerGroups = CareerGroup::all();
            $career = Careers::all();
            return view('company.isLogged.createCompany', compact('careerGroups', 'career'))
                ->with('notify', 'Vui lòng cập nhật thông tin doanh nghiệp!');
        } catch (\Exception $ex) {
             abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $input = $request->all();
    //     try {
    //         DB::beginTransaction();
    //         $company        = new ProfileCompanyModel();
    //         if (!empty($request->banner)) {
    //             $getBanner = $request->file('banner');
    //             $imageBanner = $this->upload($getBanner);
    //             $input['banner'] = $imageBanner->code;
    //         }
    //         if (!empty($request->avatar)) {
    //             $getAvatar = $request->file('avatar');
    //             $imageAvatar = $this->upload($getAvatar);
    //             $input['avatar'] = $imageAvatar->code;
    //         }
    //         $company->upsertCompany($input);
    //         DB::commit();
    //         return redirect()->route('company.home')->with('success', 'Cập nhật thông tin thành công');
    //     } catch (\Exception $ex) {
    //         DB::rollBack();
    //          abort(404);
    //     }
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $company = Company::find($id);
            $careerGroups = CareerGroup::all();
            $career = Careers::all();
            return view('company.editCompany',compact('company','careerGroups', 'career'));
        } catch (\Exception $ex) {
             abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input= $request->all();
        $input['id']=$id;
        try {
            DB::beginTransaction();
            $company        = new ProfileCompanyModel();
            if (!empty($request->banner)) {
                $getBanner = $request->file('banner');
                $imageBanner = $this->upload($getBanner);
                $input['banner'] = $imageBanner->code;
            }
            if (!empty($request->avatar)) {
                $getAvatar = $request->file('avatar');
                $imageAvatar = $this->upload($getAvatar);
                $input['avatar'] = $imageAvatar->code;
            }
            $company->upsertCompany($input,true);
            DB::commit();
            return redirect()->route('company.information.index')->with('success', 'Cập nhật thông tin thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
             abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
