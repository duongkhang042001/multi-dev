<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Models\RecruitmentPostDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RecruitmentDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        try {
            $companyId = Auth::user()->company_id;
            $recruitmentDetailHandle = RecruitmentPostDetail::whereHas('recruitmentPost.company', function ($query) use ($companyId) {
                $query->where('company_id', $companyId)->where('on_system', 1);
            })->where('is_active', 1)
                ->where(function ($query) use ($companyId) {
                    $query->whereIn('status', [RECRUITMENT_WAITING, RECRUITMENT_INTERVIEW_WAITING])
                        ->orWhere(function ($query) use ($companyId) {
                            $query->where('status', RECRUITMENT_INTERVIEW)
                                ->where('interview_start', '<=', \Carbon\Carbon::now());
                        });
                })
                ->orderBy('interview_start', 'desc')
                ->paginate($perPage = 5, $columns = ['*'], $pageName = 'handle');

            $recruitmentDetailDoneHandle = RecruitmentPostDetail::whereIn('status', [RECRUITMENT_PENDING, RECRUITMENT_PENDING_APPROVE, RECRUITMENT_INTERVIEW_SUCCESS])
                ->whereHas('recruitmentPost.company', function ($query) use ($companyId) {
                    $query->where('company_id', $companyId);
                })->where('is_active', 1)->paginate($perPage = 5, $columns = ['*'], $pageName = 'done');

            $recruitmentDetailInterview = RecruitmentPostDetail::where('status', RECRUITMENT_INTERVIEW)
                ->where('interview_start', '>=', \Carbon\Carbon::now())
                ->whereHas('recruitmentPost.company', function ($query) use ($companyId) {
                    $query->where('company_id', $companyId);
                })->where('is_active', 1)->where('is_active', 1)->paginate($perPage = 5, $columns = ['*'], $pageName = 'interview');

            $recruitmentDetailCancel = RecruitmentPostDetail::whereIn('status', [RECRUITMENT_CANCEL, RECRUITMENT_DENIED])
                ->whereHas('recruitmentPost.company', function ($query) use ($companyId) {
                    $query->where('company_id', $companyId);
                })->where('is_active', 1)->paginate($perPage = 5, $columns = ['*'], $pageName = 'cancel');

            $recruitmentDetailApprove = RecruitmentPostDetail::where('status', RECRUITMENT_APPROVED)
                ->whereHas('recruitmentPost.company', function ($query) use ($companyId) {
                    $query->where('company_id', $companyId);
                })->where('is_active', 1)->paginate($perPage = 5, $columns = ['*'], $pageName = 'apporve');
            return view('company.recruitmentDetail.index', compact('recruitmentDetailHandle', 'recruitmentDetailDoneHandle', 'recruitmentDetailInterview', 'recruitmentDetailApprove', 'recruitmentDetailCancel'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function updateStatus(Request $request, $id, $status)
    {
        try {
            $recruitmentDetails = RecruitmentPostDetail::find($id);
            switch ($status) {
                case RECRUITMENT_PENDING_APPROVE:
                    $recruitmentDetails->status = RECRUITMENT_PENDING_APPROVE;
                    $recruitmentDetails->save();
                    return redirect()->route('company.recruitment-manager.index')->with('success', 'Cập nhật thông tin thành công.');
                    break;
                case RECRUITMENT_DENIED:
                    $recruitmentDetails->status = RECRUITMENT_DENIED;
                    $recruitmentDetails->save();
                    return redirect()->route('company.recruitment-manager.index')->with('success', 'Cập nhật thông tin thành công.');
                    break;
                case RECRUITMENT_APPROVED:
                    $recruitmentDetails->status = RECRUITMENT_APPROVED;
                    $recruitmentDetails->save();
                    return redirect()->route('company.recruitment-manager.index')->with('success', 'Cập nhật thông tin thành công.');
                    break;
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
