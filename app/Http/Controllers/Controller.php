<?php

namespace App\Http\Controllers;

use App\Events\NotificationEvent;
use App\Models\City;
use App\Models\District;
use App\Models\ModelControllers\FileModel;
use App\Models\ModelControllers\NotifyModel;
use App\Models\Semester;
use App\Models\Ward;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function upload($file, $type = null)
    {
        $fileData   = base64_encode(file_get_contents($file));
        $srcImage   = 'data: ' . $file->getMimeType() . ';base64,' . $fileData;
        $file_cut  = explode(";base64,", $srcImage);
        $fileData   = base64_decode($file_cut[1]);
        $uniqId     = strtoupper(uniqid());
        switch ($type) {
            case 'pdf':
                $fileName   = $uniqId . ".pdf";
                break;
            default:
                $fileName   = $uniqId . ".png";
                break;
        }
        $filePath   = storage_path() . "/$fileName";
        file_put_contents($filePath, $fileData);
        $client = new Client([
            'headers'      => [
                'Content-Type'        => 'image/png, image/jpeg, image/webp, application/pdf',
                'Content-Disposition' => 'attachment; form-data; filename=' . $fileName,
            ],
            'verify' => false
        ]);

        $response = $client->post(FILE_UPLOAD_URL, [
            'multipart' => [
                [
                    'name'         => $fileName,
                    'contents'     => fopen($filePath, 'r')
                ]
            ],
        ]);
        $result = json_decode($response->getBody()->getContents(), true);
        $file   =  $this->createFile($result);
        return $file;
    }

    public static function getFilePdf($code = null)
    {
        if (!empty($code)) {
            $fileData   = file_get_contents(FILE_URL . $code);
            $uniqId     = strtoupper(uniqid());
            $fileName   = $uniqId . ".pdf";
            Storage::disk('public')->put($fileName, $fileData);
            $path = asset('storage/' . $fileName);
            return $path;
        }
    }

    private function createFile($input)
    {
        try {
            $file = new FileModel();
            $response = $file->createFile($input);
        } catch (\Exception $ex) {
            abort(404);
//            dd($ex->getMessage());
        }
        return $response;
    }

    public function getLatLong($address)
    {
        $string  = trim($address);
        $string = Str::upper($string);
        $string = Str::ascii($string);
        $string = Str::replace(',', '', $string);
        $string = preg_replace('/\s+/', ' ', $string);
        $string = Str::replace(' ', '%20', $string);
        $geocode = file_get_contents(env('URL_API_GOOGLE_MAP') . $string);
        $output = json_decode($geocode);
        if (!empty($output->data[0]->latitude)) {
            return $output->data[0];
        } else {
            $array = [
                'latitude' => '10.7900822',
                'longitude' => '106.680701'
            ];
            $object = (object) $array;
            return $object;
        }
    }

    function getAllWeek($start_date_input = null, $end_date = null)
    {
        if (!empty($start_date_input)) {
            $start_date     = Carbon::parse($start_date_input);
            $end_date       = !empty($end_date) ? Carbon::parse($end_date) : Carbon::parse($start_date)->addWeek(10); //25 01
            $weekOfYear     = $start_date->weekOfYear;
            $endWeekOfYear  = $end_date->weekOfYear;
            $countWeek      = $endWeekOfYear - $weekOfYear;
            if ($countWeek <= 0) {
                $tempWeek       = $start_date->endOfYear()->weekOfYear;
                $countWeek      = $tempWeek - $weekOfYear + $endWeekOfYear;
            }
            for ($i = 1; $i <= $countWeek; $i++) {
                if ($i == 1) {
                    $now            = Carbon::parse($start_date_input);
                } else {
                    $now            = $start_date->addWeek(1);
                }
                $weekStartDate  = $now->startOfWeek()->format('Y-m-d H:i:s');
                $weekEndDate    = $now->endOfWeek()->format('Y-m-d H:i:s');
                $start_date     = $now;
                $time_week[$i]      = ["start_week" => $weekStartDate, "end_week" => $weekEndDate];
            }
        } else {
            return $tempWeek = [];
        }
        return $time_week;
    }

    public static function checkTimeLine($type)
    {
        $now    = date('Y-m-d h:i:s', time());
        $time = strtotime('2021-07-01');
        $currentSemester = Semester::where('from', '<=', $now)->where('to', '>=', $now)->first();
        $timeLine = $currentSemester->timeline;
        // if ($time <= strtotime($timeLine->end_find) && $time >= strtotime($timeLine->start_find)) {
        //     return true;
        // } else {
        //     return false;
        // }
        /* NOT DELETED PLS */
        switch ($type) {
            case 'find':
                if (time() <= strtotime($timeLine->end_find) && time() >= strtotime($timeLine->start_find)) {
                    return true;
                } else {
                    return false;
                }
            case 'report':
                if (time() <= strtotime($timeLine->end) && time() >= strtotime($timeLine->start)) {
                    return true;
                } else {
                    return false;
                }
            default:
                break;
        }
    }
    public function getCurrentSemester($next = false)
    {

        $now    =  date('Y-m-d h:i:s', time());
        $currentSemester = Semester::where(function ($query) use ($now) {
            $query->where('from', '<=', $now);
            $query->where('to', '>=', $now);
        })->first();
        return !$next ? $currentSemester
            : Semester::where(function ($query) use ($currentSemester) {
                $query->where('from', '<=', Carbon::parse($currentSemester->to)->addMonth(1));
                $query->where('to', '>=', Carbon::parse($currentSemester->to)->addMonth(1));
            })->first();
    }
    public function createNotification($input)
    {
        $input['is_active'] = 1;
        $notifyModel = new NotifyModel();
        $notifyModel->upsertNotify($input);
        event(new NotificationEvent($input));
    }

    public function vn_to_str ($str){
        $unicode = array(

            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

            'd'=>'đ',

            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

            'i'=>'í|ì|ỉ|ĩ|ị',

            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

            'y'=>'ý|ỳ|ỷ|ỹ|ỵ',

            'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'D'=>'Đ',

            'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'I'=>'Í|Ì|Ỉ|Ĩ|Ị',

            'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach($unicode as $nonUnicode=>$uni){

            $str = preg_replace("/($uni)/i", $nonUnicode, $str);

        }
        $str = str_replace(' ','',$str);

        return $str;

    }
}
