<?php

namespace App\Http\Controllers;

use App\Events\NotificationEvent;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Staff\ProfileStaffUpdate;
use App\Models\ModelControllers\NotifyModel;
use App\Models\ModelControllers\UserModel;

class AdminController extends Controller
{
    function profile()
    {
        try {
            $user = User::findOrFail(Auth::user()->id);
            return view('dashboard.indexProfileAdmin', compact('user'));
        } catch (\Exception $ex) {
            abort(404);
        }
        redirect()->route('staff.index')->with('warning', "Hệ thống đang gặp sự cố. Vui lòng thử lại sau!");
    }

    function editProfile()
    {
        try {
            $user = User::findOrFail(Auth::user()->id);
        } catch (\Exception $ex) {
            abort(404);
        }
        return view('dashboard.updateProfileAdmin', compact('user'));
    }
    function updateProfile(ProfileStaffUpdate $request)
    {
        $input = $request->all();
        try {
            $input['id'] = Auth::id();
            if (!empty($request->avatar)) {
                $getAvatar              = $request->file('avatar');
                $imageUpload            = $this->upload($getAvatar);
                $input['avatar']        = $imageUpload->code;
            }

            $userModel = new UserModel();
            $userModel->updateProfileStaff($input);

            $input = [
                'title' =>  'Cập nhập thông tin cá nhân thành công.',
                'object' => NOTIFY_STAFF,
                'is_active' => 1,
                'object_detail' => Auth::id(),
                'url' => '/profile'
            ];
            $this->createNotification($input);
        } catch (\Exception $ex) {
//            dd($ex);
            abort(404);
        }
        if (Auth::guard('staff')->check()) {
            return redirect()->route('staff.profile')->with('success', 'Cập nhập thông tin cá nhân thành công.');
        } else {
            return redirect()->route('manager.profile')->with('success', 'Cập nhập thông tin cá nhân thành công.');
        }
    }
}
