<?php

namespace App\Http\Controllers;

use App\Jobs\SendAcceptCompany;
use App\Jobs\SendCancelCompany;
use App\Jobs\SendDenidRecruitmentStudent;
use App\Jobs\SendDeniedStudent;
use App\Jobs\SendInterviewStudent;
use App\Jobs\SendInterviewSuccess;
use App\Jobs\SendPendingApproved;
use App\Jobs\SendPendingStudent;
use App\Models\Company;
use App\Models\ModelControllers\NotifyModel;
use App\Models\ModelControllers\ReportModel;
use App\Models\Notify;
use App\Models\NotifyDetail;
use App\Models\Post;
use App\Models\RecruitmentPost;
use App\Models\RecruitmentPostDetail;
use App\Models\Report;
use App\Models\Semester;
use App\Models\Service;
use App\Models\User;
use App\Models\UserStudent;
use App\Models\Welfare;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AjaxController extends Controller
{
    public function status(Request $request)
    {
        $input = $request->only(['id', 'model', 'attribute']);
        try {
            $id    = Arr::get($input, 'id', null);
            $model = Arr::get($input, 'model', null);
            $model = strtoupper($model);
            switch ($model) {
                case "WELFARE":
                    $record            = Welfare::findOrFail($id);
                    $record->is_active = $record->is_active ? 0 : 1;
                    $record->save();
                    $check = $record->is_active;
                    break;
                case "RECRUITMENT_ACTIVE_COMPANY":
                    $record            = RecruitmentPost::findOrFail($id);
                    $statusUser        = RecruitmentPostDetail::where('recruitment_post_id', $id)->where('status', RECRUITMENT_APPROVED)->count();
                    if ($statusUser >= $record->quantity) {
                        return response()->json([
                            'isFull' => true,
                        ]);
                    } else {
                        $record->is_active = $record->is_active ? 0 : 1;
                        $record->save();
                        $check = $record->is_active;
                    }
                    break;
                case "RECRUITMENT_SHOW_COMPANY":
                    $record            = Post::findOrFail($id);
                    $record->is_show   = $record->is_show ? 0 : 1;
                    $record->save();
                    $check = $record->is_show;
                    break;
                case "POST_ACTIVE":
                    $record              = Post::findOrFail($id);
                    $record->is_active   = $record->is_active ? 0 : 1;
                    $record->save();
                    $check = $record->is_active;
                    break;
                case "POST_COMMENT":
                    $record            = Post::findOrFail($id);
                    $record->is_comment   = $record->is_comment ? 0 : 1;
                    $record->save();
                    $check =  $record->is_comment;
                    break;
                default:
                    return false;
            }
            return $check;
        } catch (\Exception $exception) {
            dd($exception);
        }
    }
    public function recruitment(Request $request)
    {
        $input = $request->only(['id']);
        try {
            $id    = Arr::get($input, 'id', null);
            $ajaxStudent = RecruitmentPostDetail::where('id', $id)->with('user.student', 'user.student.career', 'user.profile')->first();
            $viewCV = null;
            $downCV = null;
            if (!empty($ajaxStudent->user->student->getCV()->code)) {
                $viewCV = $this->getFilePdf($ajaxStudent->user->student->getCV()->code);
                $downCV = FILE_URL . $ajaxStudent->user->student->getCV()->code;
            }
            return view('ajax.modelStatusCompany', compact('ajaxStudent', 'viewCV', 'downCV'));
        } catch (\Exception $exception) {
            dd($exception);
        }
    }
    public function statusRecruitment(Request $request)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            $data =  Arr::get($input, 'data', null);
            if ($data) {
                $id = Arr::get($data, 'id', null);
                $status = Arr::get($data, 'status', null);
                $reason = Arr::get($data, 'reason', null);
                $interview = Arr::get($data, 'interview', null);
                $rcmDetail = RecruitmentPostDetail::findOrFail($id);
                switch ($status) {
                    case RECRUITMENT_PENDING_APPROVE:
                        $rcmDetail->status = RECRUITMENT_PENDING_APPROVE;
                        $check =  $rcmDetail->save();
                        if ($check) {
                            $company = $rcmDetail->recruitmentPost->company;
                            $idUser = $rcmDetail->user_id;
                            $user = User::findOrFail($idUser);
                            $users = [];
                            array_push($users, $user);
                            SendPendingApproved::dispatch($users, $company);
                        }
                        $dataNoti = [
                            'title' =>  'Bạn đã được doanh nghiệp <b>' . $rcmDetail->recruitmentPost->company->short_name . '</b> chấp nhận.Xác thực ngay để vào doanh nghiệp',
                            'object' => NOTIFY_COMPANY_TO_STUDENT,
                            'userId' => $rcmDetail->user_id,
                            'url' => '/student/applied-job',
                        ];
                        $this->createNotification($dataNoti);
                        break;
                    case RECRUITMENT_PENDING:
                        $rcmDetail->status = 'PENDING';
                        $check = $rcmDetail->save();
                        if ($check) {
                            $company = $rcmDetail->recruitmentPost->company;
                            $idUser = $rcmDetail->user_id;
                            $user = User::findOrFail($idUser);
                            $users = [];
                            array_push($users, $user);
                            SendPendingStudent::dispatch($users, $company);
                        }
                        $dataNoti = [
                            'title' =>  'Doanh nghiệp <b>' . $rcmDetail->recruitmentPost->company->short_name . '</b> chấp nhận. Xác thực để được phỏng vấn.',
                            'object' => NOTIFY_COMPANY_TO_STUDENT,
                            'userId' => $rcmDetail->user_id,
                            'url' => '/student/applied-job',
                        ];
                        $this->createNotification($dataNoti);
                        break;
                    case RECRUITMENT_APPROVED:
                        $rcmDetail->status = RECRUITMENT_APPROVED;
                        $check = $rcmDetail->save();
                        $idUser = Auth::user()->id;
                        $userStudent = UserStudent::where('user_id', $idUser)->first();
                        $userStudent->is_accept = 0;
                        $userStudent->save();
                        if ($check) {
                            $company = $rcmDetail->recruitmentPost->company;
                            $idUserManager = $rcmDetail->recruitmentPost->company->manager_id;
                            $user = User::findOrFail($idUser);
                            $userStudent = User::findOrFail($idUser);
                            $userManager = User::findOrFail($idUserManager);
                            $users = [];array_push($users, $user);
                            array_push($users, $userManager);
                            SendAcceptCompany::dispatch($users, $company, $userStudent);
                        }
                        RecruitmentPostDetail::where('user_id', $rcmDetail->user_id)->where('id', '!=', $id)->update(['is_active' => 0]);
                        $career = $rcmDetail->recruitmentPost->career->name;
                        $company = $rcmDetail->recruitmentPost->company_id;
                        $checkTimeLine = $this->checkTimeLine('find');
                        Report::where('user_id', Auth::guard('student')->user()->id)->update(['is_active' => 0]);
                        $reportStore = new ReportModel();
                        $reportStore->storeReport($career, $company, $checkTimeLine);
                        $dataNoti = [
                            'title' =>  'Bạn đã xác nhận thành công vào doanh nghiệp <b>' . $rcmDetail->recruitmentPost->company->short_name . '</b> để thực tập',
                            'object' => NOTIFY_COMPANY_TO_STUDENT,
                            'userId' => $rcmDetail->user_id,
                            'object_detail' => $rcmDetail->user_id,
                            'url' => '/student/report',
                        ];
                        $this->createNotification($dataNoti);
                        $dataNoti = [
                            'title' =>  'Thêm thành công sinh viên <b>' . $rcmDetail->user->name . '</b> vào doanh nghiệp',
                            'object' => NOTIFY_STUDENT_TO_COMPANY,
                            'object_detail' => $rcmDetail->recruitmentPost->company->id,
                            'url' => '/company/intern-student',
                        ];
                        $this->createNotification($dataNoti);
                        break;
                    case RECRUITMENT_DENIED:
                        $rcmDetail->status = 'DENIED';
                        $rcmDetail->reason = $reason;
                        $check = $rcmDetail->save();
                        if ($check) {
                            $idUser  = $rcmDetail->user_id;
                            $user    = User::findOrFail($idUser);
                            $company = $rcmDetail->recruitmentPost->company;
                            $reason  = $rcmDetail->reason;
                            $users   = [];
                            array_push($users, $user);
                            SendDenidRecruitmentStudent::dispatch($users, $company, $reason);
                        }
                        $dataNoti = [
                            'title' =>  'Doanh nghiệp <b>' . $rcmDetail->recruitmentPost->company->short_name . '</b> từ chối xác nhận',
                            'object' => NOTIFY_COMPANY_TO_STUDENT,
                            'userId' => $rcmDetail->user_id,
                            'url' => '/student/applied-job',
                        ];
                        $this->createNotification($dataNoti);
                        break;
                    case RECRUITMENT_INTERVIEW_WAITING:
                        $rcmDetail->status = RECRUITMENT_INTERVIEW_WAITING;
                        $check = $rcmDetail->save();
                        $dataNoti = [
                            'title' =>  'Sinh viên <b>' . $rcmDetail->user->name . '</b> xác nhận. Vui lòng lên lịch phỏng vấn',
                            'object' => NOTIFY_STUDENT_TO_COMPANY,
                            'object_detail' => $rcmDetail->recruitmentPost->company->id,
                            'url' => 'company/recruitment-manager',
                        ];
                        $this->createNotification($dataNoti);
                        break;
                    case RECRUITMENT_CANCEL:
                        $rcmDetail->status = 'CANCEL';
                        $rcmDetail->reason = $reason;
                        $validator = Validator::make($data, [
                            'reason' => 'required',
                        ], [
                            "reason.required"   => "Vui lòng nhập lý do huỷ phỏng vấn !",
                        ]);
                        if ($validator->fails()) {
                            return  response()->json(['error' => $validator->errors()]);
                        }
                        $check = $rcmDetail->save();

                        $dataNoti = [
                            'title' =>  'Sinh viên <b>' . $rcmDetail->user->name . '</b> đã hủy phỏng vấn',
                            'object' => NOTIFY_STUDENT_TO_COMPANY,
                            'object_detail' => $rcmDetail->recruitmentPost->company->id,
                            'url' => 'company/recruitment-manager',
                        ];
                        $this->createNotification($dataNoti);
                        if ($check) {
                            $company = $rcmDetail->recruitmentPost->company;
                            $idUserManager = $rcmDetail->recruitmentPost->company->manager_id;
                            $rcmDetail = RecruitmentPostDetail::findOrFail($id);
                            $reason = $rcmDetail->reason;
                            $idUser = Auth::user()->id;
                            $userStudent = User::findOrFail($idUser);
                            $userManager = User::findOrFail($idUserManager);
                            $users = [];
                            array_push($users, $userManager);
                            SendCancelCompany::dispatch($users, $company, $reason, $userStudent);
                        }
                        break;
                    case RECRUITMENT_INTERVIEW:
                        $type_interview = Arr::get($data, 'type_interview', null);
                        $now = date('Y-m-d H:i:s', time());
                        $validator = Validator::make($data, [
                            'interview_start' => 'required|after_or_equal:' . $now,
                            'interview_end' => 'required|after:interview_start',
                            'type_interview' => 'required',
                            'url' => Rule::requiredIf(function () use ($type_interview) {
                                return $type_interview == 'meet';
                            }),
                            'id_url' => Rule::requiredIf(function () use ($type_interview) {
                                return $type_interview == 'zoom';
                            }),
                            'pass_url' => Rule::requiredIf(function () use ($type_interview) {
                                return $type_interview == 'zoom';
                            }),
                            'address' => Rule::requiredIf(function () use ($type_interview) {
                                return $type_interview == 'offline';
                            }),

                        ], [
                            "interview_start.required"          => "Vui lòng chọn thời gian bắt đầu",
                            "interview_start.after_or_equal"    => "Phải lớn hơn thời gian hiện tại",
                            "interview_end.required"            => "Vui lòng chọn thời gian kết thúc",
                            "interview_end.after"              => "Không được bé hơn thời gian hiện tại",
                            "type_interview.required"           => "Vui lòng chọn hình thức phỏng vấn",
                            "url.required"                      => "Vui lòng nhập đường đẫn",
                            "id_url.required"                   => "Vui lòng nhập ID",
                            "pass_url.required"                 => "Vui lòng nhập mật khẩu",
                            "address.required"                  => "Vui lòng nhập địa chỉ phỏng vấn",
                        ]);
                        if ($validator->fails()) {
                            return  response()->json(['error' => $validator->errors()]);
                        }
                        $type_interview = Arr::get($data, 'type_interview', null);

                        $interview['online'] = ($type_interview == 'offline') ? false : true;
                        $interview['app'] = ($type_interview == 'offline') ? null : $type_interview;
                        $interview['url'] = Arr::get($data, 'url', null);
                        $interview['room'] = Arr::get($data, 'id_url', null);
                        $interview['password'] = Arr::get($data, 'pass_url', null);
                        $interview['location'] = Arr::get($data, 'address', null);

                        $interview = json_encode($interview);
                        $rcmDetail->status = RECRUITMENT_INTERVIEW;
                        $rcmDetail->interview_start = $data['interview_start'];
                        $rcmDetail->interview_end = $data['interview_end'];
                        $rcmDetail->interview = $interview;
                        $rcmDetail->interview_information = Arr::get($data, 'infor', null);
                        $check = $rcmDetail->save();

                        if ($check) {
                            $idUser = $rcmDetail->user_id;
                            $user = User::findOrFail($idUser);
                            $company = $rcmDetail->recruitmentPost->company;
                            $rcmDetail = RecruitmentPostDetail::findOrFail($id);
                            $interview_start = $rcmDetail->interview_start;
                            $interview_end = $rcmDetail->interview_end;
                            $interview_information = $rcmDetail->interview_information;
                            $interviewDate = [
                                'interview_start'         => $interview_start,
                                'interview_end'           => $interview_end,
                                'interview_information'   => $interview_information,
                            ];
                            $interview = json_decode($rcmDetail->interview);
                            $users = [];
                            array_push($users, $user);
                            SendInterviewStudent::dispatch($users, $company, $interview, $interviewDate);
                            $dataNoti = [
                                'title' =>  'Doanh nghiệp <b>' . $rcmDetail->recruitmentPost->company->short_name . '</b> đã lên lịch phỏng vấn.',
                                'object' => NOTIFY_COMPANY_TO_STUDENT,
                                'userId' => $rcmDetail->user_id,
                                'url' => '/student/applied-job',
                            ];
                            $this->createNotification($dataNoti);
                        }
                        break;
                    case RECRUITMENT_INTERVIEW_SUCCESS:
                        $rate = Arr::get($data, 'rate', null);
                        $rcmDetail->status = $rate;
                        $rcmDetail->reason = Arr::get($data, 'reason', null);
                        $check = $rcmDetail->save();
                        if ($check && $rate == RECRUITMENT_INTERVIEW_SUCCESS) {
                            $idUser = Auth::user()->id;
                            $user = User::findOrFail($idUser);
                            $company = Company::where('id', $user->company->id)->first();
                            $users = [];
                            array_push($users, $user);
                            $wf = Welfare::all();
                            SendInterviewSuccess::dispatch($users, $company, $wf);

                            $dataNoti = [
                                'title' =>  'Bạn đã đạt phỏng vấn trong doanh nghiệp <b>' . $rcmDetail->recruitmentPost->company->short_name . '</b>. Hãy kiểm tra và xác nhận.',
                                'object' => NOTIFY_COMPANY_TO_STUDENT,
                                'userId' => $rcmDetail->user_id,
                                'url' => '/student/applied-job',
                            ];

                            $this->createNotification($dataNoti);
                            SendInterviewSuccess::dispatch($users, $company, $wf);
                        } elseif ($check && $rate == RECRUITMENT_DENIED) {
                            $idUser = $rcmDetail->user_id;
                            $user = User::findOrFail($idUser);
                            $company = $rcmDetail->recruitmentPost->company;
                            $users = [];
                            array_push($users, $user);
                            SendDeniedStudent::dispatch($users, $company);

                            $dataNoti = [
                                'title' =>  'Doanh nghiệp <b>' . $rcmDetail->recruitmentPost->company->short_name . '</b> đánh giá không đạt phỏng vấn.',
                                'object' => NOTIFY_COMPANY_TO_STUDENT,
                                'userId' => $rcmDetail->user_id,
                                'url' => '/student/applied-job',
                            ];
                            $this->createNotification($dataNoti);
                        }
                        break;
                    default:
                        return false;
                }
            }
            DB::commit();
            return $check;
        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception;
        }
    }

    public function getRecruitmentDetail(Request $request)
    {
        $input = $request->all();
        $company = Company::findOrfail($input['companyId']);
        $recruitmentPostDetail = RecruitmentPostDetail::findOrfail($input['recruitmentDetailId']);
        $recruitmentPostDetail->interview = json_decode($recruitmentPostDetail->interview);
        // $recruitmentPostDetail = RecruitmentPostDetail::where('recruitment',$company->recruitmentPost->id)->first();
        $data = [
            'company' => $company,
            'recruitmentPostDetail' => $recruitmentPostDetail,
        ];
        return $data;
    }
    public function getRecruitmentDetailPending(Request $request)
    {
        $input = $request->all();
        $company = Company::findOrfail($input['companyId']);
        $post = Post::findOrfail($input['postId']);
        $recruitmentPostDetail = RecruitmentPostDetail::findOrfail($input['recruitmentDetailId']);
        $checkTimeLine = $this->checkTimeLine('find');
        $data = [
            'company'                   => $company,
            'post'                      => $post,
            'recruitmentPostDetail'     => $recruitmentPostDetail,
            'checkTimeLine'             => $checkTimeLine,
        ];
        return $data;
    }
    public function uploadFile(Request $request)
    {
        // coming soon
    }
    public function getCompany(Request $request)
    {
        try {
            $input = $request->only(['id', 'tax_number']);
            $getCompanyClient = $input['tax_number'] ?? false;
            $company = !empty($input['tax_number'])
                ? Company::where('tax_number', $input['tax_number'])->first()
                : Company::findOrFail($input['id']);
            if ($company) return view('ajax.modelCompany', compact(['company', 'getCompanyClient']));
            return false;
        } catch (\Exception $exception) {
            return $exception;
        }
    }

    public function getStudent(Request $request)
    {
        $input = $request->only(['id']);
        $student = User::findOrFail($input['id']);
        return view('ajax.modelStudent', compact('student'));
    }

    public function getStudentPending(Request $request)
    {

        $input = $request->only(['id']);
        $student = User::findOrFail($input['id']);
        return view('ajax.modelStudentResetReport', compact('student'));
    }

    public function getReport(Request $request)
    {
        $input = $request->only(['id']);
        $service = Service::findOrFail($input['id']);
        return view('ajax.modelReport', compact('service'));
    }
    public function notification()
    {
        $userID = Auth::user()->id;
        $notifyDetail = NotifyDetail::with('notify')->where('user_id', $userID)->whereHas('notify', function ($query) {
            $query->where('is_active', 1);
        })->orderBy('id', 'DESC')->limit(10)->get();
        return $notifyDetail;
    }
    public function getOneNotification(Request $request)
    {
        $input = $request->all();
        $notifyDetail = NotifyDetail::with('notify')->where('id', $input['id'])->whereHas('notify', function ($query) {
            $query->where('is_active', 1);
        })->first();
        return $notifyDetail;
    }
    public function watchedNotification()
    {
        $userID = Auth::user()->id;
        $notifyDetail = NotifyDetail::where('user_id', $userID)->whereHas('notify', function ($query) {
            $query->where('is_active', 1);
        })->get();
        foreach ($notifyDetail as $row) {
            $row->is_active = 1;
            $row->save();
        }
        return 1;
    }

    public function cancelReport(Request $request)
    {
        $input = $request->only(['id']);
        $report = Report::findOrFail($input['id']);
        return view('ajax.modelReportCancel ', compact('report'));
    }

    public function getNotify(Request $request)
    {
        $input = $request->only(['id']);
        $notify = Notify::findOrFail($input['id']);
        return view('ajax.modelNotify ', compact('notify'));
    }
    public function staffGetNotify(Request $request)
    {
        $input = $request->only(['id']);
        $notify = NotifyDetail::with('notify')->where('id', $input['id'])->first();
        return $notify;
    }
    public function uploadTranscript(Request $request){
        $input = $request->only(['id']);
        $transcript = Service::findOrFail($input['id']);
        return view('ajax.modelTranscript ', compact('transcript'));
    }
}
