<?php

namespace App\Http\Controllers;

use App\Exceptions\Handler;
use App\Http\Requests\Home\FeedBackRequest;
use App\Models\CareerGroup;
use App\Models\RecruitmentPost;
use App\Models\RecruitmentPostDetail;
use App\Models\Welfare;
use Carbon\Carbon;
use App\Models\Careers;
use App\Models\City;
use App\Models\Company;
use App\Models\Feedback;
use App\Models\ModelControllers\RecruitmentModel;
use App\Models\NotifyDetail;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // private $userService;

    // public function __construct(UserService $userService)
    // {
    //     $this->userService = $userService;
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        try {
            $careerGroups = CareerGroup::where('is_education', 1)->orderBy('name')->get();
            $careersEdu = Careers::where('is_education', 1)->orderBy('name')->get();
            $cities     = City::where('is_education', 1)->orderBy('name')->get();
            $recruitmentRecently = RecruitmentPost::with('post', 'company')->whereRelation('post', 'post_type_id', 4)->whereRelation('post', 'is_show', 1)->orderBy('created_at', 'DESC')->limit('20')->get();
            $recruitmentHighlight = Post::with('recruitmentPost')->where('is_active', 1)->where('is_show', 1)->whereRelation('postType', 'slug', POST_TYPE_RECRUITMENT_SLUG)->orderBy('view', 'DESC')->limit('20')->get();
            $topCompany = Company::with('recruitmentPosts')->where('is_active', 1)->where('on_system', 1)->whereNull('deleted_at')->get();
            $recruitmentTotal = RecruitmentPost::whereRelation('post', 'is_active', 1)->count();
            return view('clients.home', compact('careerGroups', 'careersEdu', 'recruitmentRecently', 'recruitmentHighlight', 'topCompany', 'recruitmentTotal', 'cities'));
        } catch (\Exception $exception) {
            abort(404);
        }
    }
    public function notify()
    {
        return view('clients.notify');
    }
    public function recruitment($slug)
    {
        try {
            DB::beginTransaction();
            $recruitment = RecruitmentPost::with('company', 'career')->whereRelation('post', 'slug', $slug)->first();
            $postId = $recruitment->post->id;
            $recruitmentView = new RecruitmentModel();
            $recruitmentView->storeViewPost($postId);
            $recruitmentPostId = false;
            $filePathPublic = null;
            if (!empty(Auth::guard('student')->user())) {
                $recruitmentPostId = RecruitmentPostDetail::where('recruitment_post_id', $recruitment->id)->where('user_id', Auth::guard('student')->user()->id)->where('is_active', 1)->exists();
                if (!empty(Auth::guard('student')->user()->student->getCV())) {
                    $filePathPublic = $this->getFilePdf(Auth::guard('student')->user()->student->getCV()->code) ?? null;
                }
            }
            $welfares = Welfare::all();
            $relatedWork = RecruitmentPost::with('company', 'career', 'post')
                ->where('career_id', '=', $recruitment->career->id)
                ->whereNotIn('recruitment_post.post_id', [$recruitment->post_id])->get();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            abort(404);
        }
        return view('clients.recruitment', compact('recruitment', 'welfares', 'relatedWork', 'recruitmentPostId', 'filePathPublic'));
    }
    public function storeRecruitment(Request $request)
    {
        $input = $request->only('id', 'wish');
        $id = $input['id'];
        $recruitmentPost = RecruitmentPost::where('id', $id)->first();
        $wish = $input['wish'];
        try {
            if (!empty(Auth::guard('student')->user()->student->getCV())) {
                $recruitment = new RecruitmentModel();
                $recruitment->storeRecruitment($id, $wish);
                $dataNoti = [
                    'title' =>  'Sinh viên <b>' . Auth::user()->name . '</b> xác nhận. Vui lòng lên lịch phỏng vấn',
                    'object' => NOTIFY_STUDENT_TO_COMPANY,
                    'object_detail' => $recruitmentPost->company->id,
                    'url' => '/company/recruitment',
                ];
                $this->createNotification($dataNoti);
                return true;
            } else {
                return false;
            }
        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }
    }
    public function company($slug)
    {
        try {
            DB::beginTransaction();
            $company = Company::where('slug', $slug)->first();
            $countStaff = User::where('company_id', $company->id)->get();
            $listRecruitment = $company->recruitmentPosts()->paginate(2);
            DB::commit();
            return view('clients.company', compact('company', 'countStaff', 'listRecruitment'));
        } catch (\Exception $exception) {
            DB::rollback();
            abort(404);
        }
    }

    public function feedBackStore(FeedBackRequest $request)
    {
        $input = $request->all();
        try {
            $feedBack = new Feedback();
            $feedBack->content = $input['content'] ?? null;
            $feedBack->full_name = $input['full_name'] ?? null;
            $feedBack->email = $input['email'] ?? null;
            $feedBack->phone = $input['phone'] ?? null;
            $feedBack->title = strtoupper($input['title']) ?? null;
            if (!empty(Auth::guard('student')->user()->id)) {
                $feedBack->type  =  1;
            } elseif (!empty(Auth::guard('company')->user()->id)) {
                $feedBack->type  =  2;
                $feedBack->company_id = Auth::guard('company')->user()->company_id;
            } else {
                $feedBack->type  =  0;
            }
            $feedBack->save();
            return redirect()->route('notify')->with('notify-content', 'Cảm ơn bạn đã liên hệ với nhà trường, nhà trường sẽ liên hệ lại bạn sớm nhất!', 'success', 'Gửi liên hệ thành công');
        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }
    }
    public function recruitmentList(Request $request)
    {
        $input = $request->only('careerGroups');
        try {
            DB::beginTransaction();
            $recruitment = RecruitmentPost::whereRelation('post', 'is_show', 1)->paginate($perPage = 6, $columns = ['*'], $pageName = 'recruitment');
            $careerGroups = CareerGroup::all();
            DB::commit();
            return view('clients.recruitmentList', compact('recruitment', 'careerGroups'));
        } catch (\Exception $exception) {
            DB::rollback();
            abort(404);
        }
    }
    public function getNotification()
    {
        try {
            $userID = Auth::user()->id;
            $readNotifyDetails = NotifyDetail::where('user_id', $userID)->get();
            foreach ($readNotifyDetails as $row) {
                $row->is_active = 1;
                $row->save();
            }
            $notifyDetails = NotifyDetail::with('notify')->where('user_id', $userID)->orderBy('id', 'DESC')->paginate(7);
            return view('clients.listNotify', compact('notifyDetails'));
        } catch (\Exception $exception) {
            DB::rollback();
            abort(404);
        }
    }
    public function notifyDetail($id)
    {

        try {
            $userID = Auth::user()->id;
            $notifyDetail = NotifyDetail::with('notify')->where('user_id', $userID)
                ->whereHas('notify', function ($query) use ($id) {
                    $query->where('id', $id);
                })
                ->first();
            if ($notifyDetail) {
                $notifyDetail->is_active = 1;
                $notifyDetail->save();
                return view('clients.notifyDetail', compact('notifyDetail'));
            }
        } catch (\Exception $exception) {
            DB::rollback();
            abort(404);
        }
        abort(404);
    }

    public function post()
    {
        try {
            $post = Post::where('is_active', 1)->where('post_type_id', 1)->paginate($perPage = 6, $columns = ['*'], $pageName = 'page');
            return view('clients.post.post', compact('post'));
        } catch (\Exception $exception) {
            DB::rollback();
            abort(404);
        }
    }

    public function postDetail(Request $request, $slug)
    {
        try {
            $postDetail = Post::where('slug', $slug)->first();
            $postDetail->view = $postDetail->view + 1;
            $postDetail->save();
            $relatedPost = Post::where('post_type_id', 1)->whereNotIn('slug', [$postDetail->slug])->get();
            $relatedPostFAQ = Post::where('post_type_id', 2)->whereNotIn('slug', [$postDetail->slug])->get();
            return view('clients.post.postDetail', compact('postDetail', 'relatedPost', 'relatedPostFAQ'));
        } catch (\Exception $exception) {
            DB::rollback();
            abort(404);
        }
    }

    public function postFAQ()
    {
        try {
            $postFAQ = Post::where('is_active', 1)->where('post_type_id', 2)->paginate($perPage = 6, $columns = ['*'], $pageName = 'page');
            return view('clients.post.postFAQ', compact('postFAQ'));
        } catch (\Exception $exception) {
            DB::rollback();
            abort(404);
        }
    }

    public function postCompany()
    {
        try {
            $postCompany = Post::where('is_active', 1)->where('post_type_id', 3)->paginate($perPage = 6, $columns = ['*'], $pageName = 'page');
            return view('clients.post.postCompany', compact('postCompany'));
        } catch (\Exception $exception) {
            DB::rollback();
            abort(404);
        }
    }
    public function searchRecruitment(Request $request)
    {
        try {
            $input  =  $request->all();
            $city   =  Arr::get($input, 'address', null);
            $career =  Arr::get($input, 'career', null);
            $key    =  Arr::get($input, 'keyword', null);
            $searchRecruitment = RecruitmentPost::when($city, function ($query) use ($city) {
                $query->where('city_id', $city);
            })
                ->when($career, function ($query) use ($career) {
                    $query->where('career_id', $career);
                })
                ->when($key, function ($query) use ($key) {
                    $query->whereRelation('post', 'title', 'like', '%' . $key . '%')->where('is_active', 1);
                })
                ->paginate(10);
            return view('clients.listSearchRecruitment', compact('searchRecruitment'));
        } catch (\Exception $exception) {
            abort(404);
        }
    }
    public function listCareerRecruitment($type = null, $slug)
    {
        try {
            $career = $type == 'career' ? Careers::where('slug', $slug)->get() : Careers::whereRelation('careerGroup', 'slug', $slug)->get();
            $idCareer = $career->pluck('id');
            $searchRecruitment = RecruitmentPost::whereIn('career_id', $idCareer)->whereRelation('post', 'deleted', 0)->paginate(10);
            return view('clients.listSearchRecruitment', compact('searchRecruitment'));
        } catch (\Exception $exception) {
            abort(404);
        }
    }
    public function listCityRecruitment($id)
    {
        try {
            $searchRecruitment = RecruitmentPost::where('city_id', $id)->paginate(10);
            return view('clients.listSearchRecruitment', compact('searchRecruitment'));
        } catch (\Exception $exception) {
            abort(404);
        }
    }
}
