<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Http\Requests\Manager\StaffCreateRequest;
use App\Http\Requests\Manager\StaffUpdateRequest;
use App\Models\City;
use App\Models\District;
use App\Models\ModelControllers\UserModel;
use App\Models\Profile;
use App\Models\User;
use App\Models\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StaffController extends Controller
{
    public function index()
    {
        try {
            $user = User::where('role_id', STAFF_ROLE_ID)->get();
            return view('manager.staff.index', compact('user'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function create()
    {
        try {
            return view('manager.staff.createUser');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function store(StaffCreateRequest $request)
    {
        try {
            $input = $request->all();
            $userModel = new UserModel();
            $userModel->upsertStaff($input);
            return redirect()->route('manager.staff.index')->with('success','Thêm nhân viên thành công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function show($id)
    {
        try {
            $staff = User::find($id);
            $profile = $staff->profile;
            if ($staff && $profile) return view('manager.staff.detailUser', compact('staff', 'profile'));
            return redirect()->back()->withErrors('Tài khoản đang có sự cố!');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function edit($id)
    {

        try {
            $city = City::all();
            $district = District::all();
            $ward = Ward::all();
            $user = User::find($id);
            if ($user->is_logged) {
                return redirect()->back()->withErrors(['error' => 'Tài khoản đã được kích hoạt hệ thống']);
            }
            return view('manager.staff.editUser', compact('user','city', 'district', 'ward'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function update(StaffUpdateRequest $request, $id)
    {
        try {
            $input = $request->all();
            $input['id'] = $id;
            DB::beginTransaction();
            $userModel = new UserModel();
            $userModel->upsertStaff($input, true);
            DB::commit();
            return redirect()->route('manager.staff.index')->with('success','Chỉnh nhân viên thành công');
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $staff = User::find($id);
            $profile=Profile::where('user_id',$staff->id)->first();
            $profile->delete();
            $staff->delete();
            DB::commit();
            return redirect()->route('manager.staff.index')->with('warning','Xóa nhân viên thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
