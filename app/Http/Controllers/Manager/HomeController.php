<?php


namespace App\Http\Controllers\Manager;

use App\Models\Company;
use App\Models\Notify;
use App\Models\Semester;
use App\Models\Timeline;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Session;

class HomeController
{
    public function index()
    {
       try{
            $staff = User::where('role_id',STAFF_ROLE_ID)->count();
            $company = User::whereHas('company', function ($query) {
                $query->where('is_active', 1)->where('on_system', 1);
            })->count();
            $student= User::where('role_id',STUDENT_ROLE_ID)->count();
            $timelines = Timeline::with('semester')->get();
            $notify =Notify::all()->count();
           $companyTopRecruitment = Company::withCount('recruitmentPostDetails')->with('recruitmentPostDetails')
               ->orderBy('recruitment_post_details_count', 'DESC')->limit(15)->get();
//            $companyTopRecruitment = Company::withCount('reports')->with('reports')->whereHas('reports',function($query){
//                $query->whereNotNull('company_id')->where('is_active',1);
//            })->orderBy('reports_count','desc')->get();
            // dd($companyTopRecruitment);

            $countPendingStudents = User::with('reports')
            ->whereHas('reports', function ($query) {
                $query->where('status', 'PENDING')->where('is_active', 1);
            })
            ->get();
        $countPassStudents = User::withCount('reports')->with('reports')
            ->whereHas('reports', function ($query) {
                $query->where('status', 'PASS')->where('is_active', 1);
            })
            ->get();
        $countFailStudents = User::withCount('reports')->with('reports')
            ->whereHas('reports', function ($query) {
                $query->where('status', 'FAIL')->where('is_active', 1);
            })
            ->get();
            $countDoneStudents = User::withCount('reports')->with('reports')
            ->whereHas('reports', function ($query) {
                $query->where('status', 'DONE')->where('is_active', 1);
            })
            ->get();
            $countCancelStudents = User::withCount('reports')->with('reports')
            ->whereHas('reports', function ($query) {
                $query->where('status', 'CANCEL')->where('is_active', 1);
            })
            ->get();
            return view('manager.home',compact('staff','company','student','notify','timelines',
            'countFailStudents','countPassStudents','countPendingStudents','countDoneStudents',
            'countCancelStudents','companyTopRecruitment'));
       }catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
