<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Timeline;
use App\Models\Semester;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Manager\SemesterCreateRequest;
use App\Http\Requests\Manager\SemesterUpdateRepuest;
use App\Models\ModelControllers\SemesterModel;
use App\Supports\Log;

class SemesterController extends Controller
{
    public function index()
    {
        try {
            $semesters = Semester::all();
            return view('manager.semester.index', compact('semesters'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function create()
    {
        try {
            return view('manager.semester.createSemester');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function store(SemesterCreateRequest $request)
    {
        try {
            $input = $request->all();
            $semester = new SemesterModel;
            $newData = $semester->upsertSemester($input);
            Log::save('Nhóm Ngành', $request->method(), null, $newData);
            return redirect()->route('manager.semester.index')->with('success', 'Thêm Thành Công');
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
    }
    public function edit($id)
    {
        try {
            $semester = Semester::find($id);
            if (strtotime($semester->to) >= strtotime(date("Y-m-d H:i:s"))) {
                return view('manager.semester.editSemester', compact('semester'));
            } else {
                return redirect()->back()->with('fail', 'Các Kỳ đã qua không thể sửa');
            }
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function update(SemesterUpdateRepuest $request, $id)
    {
        try {
            $input = $request->all();
            $input['id'] = $id;
            $semester = new SemesterModel;
            $semester->upsertSemester($input, true);
            return redirect()->route('manager.semester.index')->with('success', 'Đã Sửa Thành Công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function destroy($id)
    {
        try {
            $timeLine = Timeline::where('semester_id', '=', $id)->where('deleted_at', NULL)->first();
            if ($timeLine) {
                $timeLine->delete();
            }
            $semester = Semester::find($id);
            $semester->delete();
            return redirect()->route('manager.semester.index')->with('success', 'Xóa thành công');
        } catch (\Exception $ex) {
            abort(404);
        };
    }
}
