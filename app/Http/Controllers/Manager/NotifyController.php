<?php

namespace App\Http\Controllers\Manager;

use App\Events\NotificationEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Manager\NotifyCreateRequest;
use App\Http\Requests\Manager\NotifyUpdateRequest;
use App\Models\Company;
use App\Models\ModelControllers\NotifyModel;
use App\Models\Notify;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotifyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $notifyAll    = Notify::where('object', NOTIFY_ALL)->get();
            $notifyStaff  = Notify::where('object', NOTIFY_STAFF)->get();
            $notifyCompany = Notify::where('object', NOTIFY_COMPANY)->get();
            $notifyStudent = Notify::where('object', NOTIFY_STUDENT)->get();
            return view('manager.notify.index', compact('notifyAll', 'notifyStaff', 'notifyCompany', 'notifyStudent'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $staff = User::where('role_id', STAFF_ROLE_ID)->get();
            $company = Company::where('on_system', 1)->get();
            return view('manager.notify.create', compact('staff', 'company'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NotifyCreateRequest $request)
    {
        try {
            $input = $request->all();
            $input['url']= '/notify-detail';
            $notifyModel = new NotifyModel();
            $notifyModel->upsertNotify($input);
            if (!empty($input['is_active'])) {
                event(new NotificationEvent($input));
            }
            return redirect()->back()->with('success', 'Tạo thông báo thành công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $notifyDetail = Notify::find($id);
            return view('manager.notify.detail', compact('notifyDetail'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function sendNotify(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $request->request->add(['id' => $id]);
            $input = $request->all();
            $notifyModel = new NotifyModel();
            $notifyModel->sendNotifications($input);
            DB::commit();
            return redirect()->back()->with('success', 'Gửi thông báo thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try {
            $notify = Notify::where('id', $id)->where('is_active', 0)->first();
            if ($notify) {
                $staff = User::where('role_id', STAFF_ROLE_ID)->get();
                $company = Company::where('on_system', 1)->get();
                return view('manager.notify.edit', compact('notify', 'staff', 'company'));
            } else {
                return redirect()->back()->with('fail', 'Vui lòng chọn đúng thông báo');
            }
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NotifyUpdateRequest $request, $id)
    {
        try {

            $input = $request->all();
            $input['id'] = $id;
            $notifyModel = new NotifyModel();
            $notifyModel->upsertNotify($input, true);
            if (!empty($input['is_active'])) {
                event(new NotificationEvent($request));
            }
            return redirect()->back()->with('success', 'Chỉnh thông báo thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $notify = Notify::where('id', $id)->where('is_active', 0)->first();
            if (!empty($notify)) {
                $notify->delete();
                return redirect()->route('manager.notify.index')->with('warning', 'Xóa thông báo thành công');
            } else {
                return redirect()->route('manager.notify.index')->with('warning', 'Xóa thông báo thất bại');
            }
        } catch (\Exception $ex) {
            abort(404);
        }
    }
}
