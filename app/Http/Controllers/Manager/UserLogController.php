<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserLog;
use Illuminate\Support\Facades\DB;

class UserLogController extends Controller
{
    public function index()
    {
        try {
            $userLogs = UserLog::all()->sortByDesc("id");
            return view('manager.userLog.index', compact('userLogs'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
    public function getLogByUser($id)
    {
        try {
            $userLogs = UserLog::where('created_by', $id)->orderBy('id', 'desc')->get();
            return view('manager.userLog.index', compact('userLogs'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
