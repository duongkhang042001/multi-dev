<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Timeline;
use App\Models\Semester;
use App\Models\ModelControllers\TimelineModel;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Manager\TimelineCreate;
use App\Http\Requests\Manager\TimelineEdit;


use function PHPUnit\Framework\returnSelf;

class TimelineController extends Controller
{
    /**g
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $timelines = Timeline::with('semester')->get();
            return view('manager.timeline.index', compact('timelines'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $semeters = Semester::all()->where('to', '>', now())->whereNull('timeline')->sortDesc();
            return view('manager.timeline.createTimeline', compact('semeters'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, TimelineCreate $timelineCreate)
    {

        $input = $request->all();
        $timelineCreate->validated($input);

        try {
            $semeter = Semester::find($input['semester_id']);
            if (date('Y-m-d', strtotime($semeter->from)) <= $input['start'] && $semeter->to > $input['start']) {
                try {
                    $timeline = new TimelineModel();
                    $timeline->createTimeline($input);
                } catch (\Exception $ex) {

                    abort(404);
                }
                return redirect()->route('manager.timeline.index')->with('success', 'Tạo thời gian thực tập thành công');
            } else {
                return redirect()->back()->with('fail', 'Vui lòng nhập thời gian thực tập đúng với kì');
            }
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $timeline = Timeline::find($id);
            $semester = Semester::find($timeline->semester_id);
            if (strtotime($semester->to) >= strtotime(date("Y-m-d H:i:s"))) {
                return view('manager.timeline.editTimeline', compact('timeline'));
            } else {
                return redirect()->route('manager.timeline.index')->with('fail', 'Các Kỳ đã qua không thể sửa');
            }
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TimelineEdit $request, $id)
    {
        $input = $request->all();
        $input['id'] = $id;
        try {
            $TimelineModel = new TimelineModel();
            $TimelineModel->updateTimeline($input);
        } catch (\Exception $ex) {
            abort(404);
        }
        return redirect()->route('manager.timeline.index')->with('success', 'Đã Sửa Thành Công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $timeLine = Timeline::find($id);
            $timeLine->delete();
            return redirect()->route('manager.timeline.index')->with('success', 'Xóa thành công');
        } catch (\Exception $ex) {
            abort(404);
        };
    }
}
