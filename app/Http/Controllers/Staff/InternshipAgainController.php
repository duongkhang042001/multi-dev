<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Jobs\SendInternshipFail;
use App\Jobs\SendInternshipPass;
use App\Models\Report;
use App\Models\Service;
use App\Models\User;
use App\Models\UserStudent;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InternshipAgainController extends Controller
{
    public function index()
    {
        try {
            $allInternshipAgain       = Service::where('type', SERVICE_TYPE_INTERNSHIP)->whereIn('status', [SERVICE_STATUS_PENDING, SERVICE_STATUS_APPROVED, SERVICE_STATUS_DENINED])->latest()->get();
            $waitingInternshipAgain   = Service::where('type', SERVICE_TYPE_INTERNSHIP)->where('status', SERVICE_STATUS_PENDING)->latest()->get();
            $approvedInternshipAgain  = Service::where('type', SERVICE_TYPE_INTERNSHIP)->whereIn('status', [SERVICE_STATUS_APPROVED, SERVICE_STATUS_DENINED])->latest()->get();
            $deninedInternshipAgain   = Service::where('type', SERVICE_TYPE_INTERNSHIP)->where('status', SERVICE_STATUS_CANCEL)->latest()->get();
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return view('staff.service.internshipAgain', compact('allInternshipAgain', 'waitingInternshipAgain', 'approvedInternshipAgain', 'deninedInternshipAgain'));
    }
    public function internshipPass($id)
    {
        try {
            DB::beginTransaction();
            $idUserStudent          = Service::find($id);
            $idUserStudent->status  = SERVICE_STATUS_APPROVED;
            $userStudent            = $idUserStudent->save();
            if ($userStudent) {
                $userStudent = UserStudent::where('user_id', $idUserStudent->user_id)->first();
                $userStudent->is_accept = 1;
                $userStudent->status    = NULL;
                $report = $userStudent->save();
                if ($report) {
                    $report = Report::where('user_id', $userStudent->user_id)->update(['is_active' => 0]);

                    if ($report) {
                        $staff = User::where('id', Auth::user()->id)->first();
                        $services = Service::find($id);
                        $users = [];
                        array_push($users, $services);
                        SendInternshipPass::dispatch($users, $staff);
                        $dataNoti = [
                            'title' =>  'Đăng ký thực tập lại thành công !',
                            'object' => NOTIFY_STUDENT,
                            'userId' => $services->user_id,
                            'url' => '/student/service-internship-again-student',
                        ];
                        $this->createNotification($dataNoti);
                    }
                }
            }
            DB::commit();
            return redirect()->back()->with('success', 'Xác nhận thực tập lại thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
    public function internshipFail(Request $request)
    {
        $input     = $request->all();
        try {
            DB::beginTransaction();
            $data      =  Arr::get($input, 'data', null);
            if ($data) {
                $id     = Arr::get($data, 'id', null);
                $reason = Arr::get($data, 'reason', null);
                $validator = Validator::make($data, [
                    'reason' => 'required',
                ], [
                    "reason.required"   => "Vui lòng nhập lý do từ chối đăng ký thực tập lại !",
                ]);
                if ($validator->fails()) {
                    return  response()->json(['error' => $validator->errors()]);
                }
                $idUser            = Auth::user()->id;
                $service           = Service::find($id);
                $service->status   = SERVICE_STATUS_DENINED;
                $service->reason   = $reason;
                $check             = $service->save();
                if ($check) {
                    $staff         = User::where('id', $idUser)->first();
                    $services       = Service::find($id);
                    $users         = [];
                    array_push($users, $services);
                    SendInternshipFail::dispatch($users, $staff);
                    $dataNoti = [
                        'title'  =>  'Đăng ký thực tập lại không được chấp thuận !',
                        'object' => NOTIFY_STUDENT,
                        'userId' => $services->user_id,
                        'url'    => '/student/service-internship-again-student',
                    ];
                    $this->createNotification($dataNoti);
                }
            }
            DB::commit();
            return $check;
            return redirect()->back()->with('success', 'Đã huỷ yêu cầu đăng ký thực tập lại');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
