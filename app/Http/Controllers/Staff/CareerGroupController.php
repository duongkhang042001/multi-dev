<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CareerGroup;
use App\Models\ModelControllers\CareerGroupModel;
use App\Http\Requests\Staff\CareerGroupRequest;
use App\Http\Requests\Staff\CareerGroupUpdateRequest;
use Illuminate\Support\Str;
use App\Supports\Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CareerGroupController extends Controller
{
    public function index()
    {
        try {
            $careerGroup = CareerGroup::all();
        } catch (\Exception $ex) {
          abort(404);
        }

        return view('staff.careerGroup.index', compact('careerGroup'));

    }

    public function create()
    {
        try {
            return view('staff.careerGroup.createCareerGroup');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
     
    }

    public function store(CareerGroupRequest $request)
    {
        $input=$request->all();
        try {
            $careerGroupModel = new CareerGroupModel;
            $newData = $careerGroupModel->upsertCareerGroup($input);
            $newData['_method'] =$request->method();
            Log::save('Nhóm Ngành',$request->method(),null,$newData);
        } catch (\Exception $ex) {
          abort(404);
        }
        return redirect()->route('staff.careerGroup.index')->with('success','Đã Thêm Thành Công');
    }
    public function edit($id)
    {
        $careerGroup = CareerGroup::find($id);
        return view('staff.careerGroup.editCareerGroup', compact('careerGroup'));
    }
    public function update(CareerGroupUpdateRequest $request, $id)
    {
        $input = $request->all();
        $input['id'] = $id;
        try {
            $oldData=CareerGroup::find($id);
            $careerGroupModel = new CareerGroupModel();
            $newData = $careerGroupModel->upsertCareerGroup($input,true);
            Log::save('Nhóm Ngành',$request->method(),$oldData,$newData);
            return redirect()->route('staff.careerGroup.index')->with('success','Đã Sửa Thành Công');
        } catch (\Exception $ex) {
          abort(404);
        }
    }
    public function destroy($id)
    {
        try {
            $careerGroup = CareerGroup::find($id);
            Log::save('Nhóm Ngành','DELETE',null,$careerGroup,null);
            $careerGroup->delete();
            return redirect()->route('staff.careerGroup.index')->with('warning','Xóa thành công');
        } catch (\Exception $ex) {
          abort(404);
        };
    }
}
