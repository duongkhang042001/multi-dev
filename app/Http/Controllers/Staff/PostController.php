<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Http\Requests\Staff\PostCreateRequest;
use App\Http\Requests\Staff\PostUpdateRequest;
use App\Models\ModelControllers\PostModel;
use App\Models\Post;
use App\Models\PostType;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\Mime\Encoder\Base64Encoder;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $postNormal  = Post::whereRelation('postType', 'slug', POST_TYPE_NORMAL_SLUG)->get();
            $postFAQ     = Post::whereRelation('postType', 'slug', POST_TYPE_FAQ_SLUG)->get();
            $postCompany = Post::whereRelation('postType', 'slug', POST_TYPE_COMPANY_SLUG)->get();
            return view('staff.post.index', compact('postNormal', 'postFAQ', 'postCompany'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $postType = PostType::whereIn('slug', [POST_TYPE_NORMAL_SLUG,POST_TYPE_FAQ_SLUG])->get();
            return view('staff.post.createPost', compact('postType'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request)
    {
        try {
            $input = $request->all();
            DB::beginTransaction();
            $getThumbnail = $request->file('thumbnail');
            $imageUpload = $this->upload($getThumbnail);
            if (!empty($imageUpload)) {
                $input['thumbnail'] = $imageUpload->code;
                $post = new PostModel();
                $post->upsertPost($input);
            }
            DB::commit();
            return redirect()->route('staff.post.index')->with('success', 'Thêm bài viết thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
            // dd($ex->getMessage());
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $post = Post::find($id);
            return view('staff.post.detailPost', compact('post'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $post      = Post::find($id);
            $postTypes = PostType::all();
            return view('staff.post.editPost', compact('post', 'postTypes'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, $id)
    {
        try {
            $input = $request->all();
            $input['id'] = $id;
            DB::beginTransaction();
            if (!empty($request->thumbnail)) {
                $getThumbnail       = $request->file('thumbnail');
                $imageUpload        = $this->upload($getThumbnail);
                $input['thumbnail'] = $imageUpload->code;
            }
            $post = new PostModel();
            $post->upsertPost($input, true);
            DB::commit();
            return redirect()->back()->with('status', 'Chỉnh Bài Viết Thành Công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $posts = Post::find($id);
            // $path = '../public/images';
            // if (file_exists($path)) {
            //     unlink($path . $posts->thumbnail);
            // } //tồn tại đường dẫn $path
            Post::find($id)->delete();
            return redirect()->route('staff.post.index')->with('warning', 'Xóa bài viết thành công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }
}
