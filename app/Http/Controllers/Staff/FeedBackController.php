<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Jobs\SendReplyStudent;
use App\Models\Feedback;
use App\Models\ModelControllers\FeedBackModel;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FeedBackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $feedbackAll     = Feedback::where('type', FEEDBACK_ALL)->orderby('created_at', 'desc')->get();
            $feedbackStudent = Feedback::where('type', FEEDBACK_STUDENT)->orderby('created_at', 'desc')->get();
            $feedbackCompany = Feedback::where('type', FEEDBACK_COMPANY)->orderby('created_at', 'desc')->get();
            return view('staff.feedback.index', compact('feedbackStudent', 'feedbackCompany', 'feedbackAll'));
        } catch (\Exception $ex) {
           abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function returnFeedback(Request $request, $id)
    {
        try {
            $input = $request->all();
            $input['id'] = $id;
            $feebackModel = new FeedBackModel();
            $feebackModel->feedbackReply($input);
            return redirect()->back()->with('success', 'Gửi phản hồi thành công');
        } catch (\Exception $ex) {
           abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $feedback = Feedback::find($id);
            if (!$feedback->is_active) {
                $feedback->is_active = 1;
                $feedback->save();
            }
            return view('staff.feedback.detailFeedBack', compact('feedback'));
        } catch (\Exception $ex) {
           abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $feedback = Feedback::find($id);
            $feedback->delete();
            return redirect()->route('staff.feedback.index')->with('warning', 'Xóa góp ý thành công');
        } catch (\Exception $ex) {
           abort(404);
        }
    }

    public function sendMailStudent(Request $request, $email)
    {
        try {
            $input = $request->all();
            $content = $input['content'];
            $email = $email;
            SendReplyStudent::dispatch($email, $content);
            return redirect()->back()->with('success', 'Gửi phản hồi thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
