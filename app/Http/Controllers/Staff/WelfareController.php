<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Http\Requests\Staff\WelfareCreateRequest;
use App\Http\Requests\Staff\WelfareUpdateRequest;
use App\Models\ModelControllers\WelfareModel;
use App\Models\Welfare;
use Illuminate\Http\Request;

class WelfareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $welfares = Welfare::all();
            return view('staff.welfare.index', compact('welfares'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('staff.welfare.createWelfare');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WelfareCreateRequest $request)
    {
        try {
            $input = $request->all();
            $welfareModel = new WelfareModel();
            $welfareModel->upsertWelfare($input);
            return redirect()->back()->with('success', 'Thêm phúc lợi thành công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $welfares = Welfare::find($id);
            return view('staff.welfare.editWelfare', compact('welfares'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WelfareUpdateRequest $request, $id)
    {
        try {
            $input = $request->all();
            $input['id'] = $id;
            $welfareModel = new WelfareModel();
            $welfareModel->upsertWelfare($input, true);
            return redirect()->back()->with('success', 'Chỉnh phúc lợi thành công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $welfares=Welfare::find($id);
            $welfares->delete();
            return redirect()->route('staff.welfare.index')->with('warning','Xóa phúc lợi thành công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }
}
