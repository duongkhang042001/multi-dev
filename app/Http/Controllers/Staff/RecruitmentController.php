<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Models\Careers;
use App\Models\ModelControllers\PostModel;
use App\Http\Requests\Staff\RecruimentCreateRequest;
use App\Http\Requests\Staff\RecruitmentUpdateRequest;
use App\Models\PostType;
use App\Models\Welfare;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ModelControllers\RecruitmentModel;
use App\Models\RecruitmentPost;
use App\Models\Company;
use App\Models\Post;

class RecruitmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $recruitments = RecruitmentPost::orderByDesc('id')->get();
            return view('staff.recruitment.index', compact('recruitments'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $companies = Company::all();
            $postType = PostType::Where('slug', POST_TYPE_RECRUITMENT_SLUG)->get();
            $welfares = Welfare::all();
            $careers = Careers::all();
            return view('staff.recruitment.createRecruitment', compact('postType', 'welfares', 'careers', 'companies'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecruimentCreateRequest $request)
    {
        try {
            $input = $request->all();
            DB::beginTransaction();
            $RecruitmentPost = new RecruitmentModel();
            $RecruitmentPost->upsertRecruitment($input);
            DB::commit();
            return redirect()->back()->with('success', 'Thêm bài đăng tuyển thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $recruitment    = RecruitmentPost::find($id);
            $nameCompany    = Company::find($recruitment->company_id);
            $nameCareer     = Careers::find($recruitment->career_id);
            $welfares       = Welfare::all();
            return view('staff.recruitment.detailRecruitment', compact('recruitment', 'nameCareer', 'nameCompany', 'welfares'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $recruitment = RecruitmentPost::with('post')->find($id);
            $postType = PostType::Where('slug', POST_TYPE_RECRUITMENT_SLUG)->get();
            $welfares = Welfare::all();
            $careers = Careers::all();
            $companies = Company::all();
            return view('staff.recruitment.editRecruitment', compact('recruitment', 'postType', 'welfares', 'careers', 'companies'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response RecruitmentUpdate
     */
    public function update(RecruitmentUpdateRequest $request, $id)
    {

        try {
            $input = $request->all();
            $input['id'] = $id;
            DB::beginTransaction();
            $RecruitmentPost = new RecruitmentModel();
            $RecruitmentPost->upsertRecruitment($input, true);
            DB::commit();
            return redirect()->back()->with('success', 'Sửa Bài Viết Thành Công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $recruitment = RecruitmentPost::find($id);
            $post = Post::find($recruitment->post_id);
            $post->delete();
            $recruitment->delete();
            return redirect()->route('staff.recruitment.index')->with('warning', 'Xóa Bài Viết Thành Công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }
}
