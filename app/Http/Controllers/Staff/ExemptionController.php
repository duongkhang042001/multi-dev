<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Jobs\SendExemptionFail;
use App\Jobs\SendExemptionSuccess;
use App\Models\Service;
use App\Models\User;
use App\Models\UserStudent;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use League\OAuth1\Client\Server\Server;
use PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions\F;

class ExemptionController extends Controller
{
    public function index()
    {
        try{
            DB::beginTransaction();
            $allExemption       = Service::where('type',SERVICE_TYPE_EXEMPTION)->whereIn('status',[SERVICE_STATUS_PENDING,SERVICE_STATUS_APPROVED,SERVICE_STATUS_DENINED,SERVICE_STATUS_CANCEL])->latest()->get();
            $waitingExemption   = Service::where('type',SERVICE_TYPE_EXEMPTION)->where('status',SERVICE_STATUS_PENDING)->latest()->get();
            $approvedExemption  = Service::where('type',SERVICE_TYPE_EXEMPTION)->whereIn('status',[SERVICE_STATUS_APPROVED,SERVICE_STATUS_DENINED])->latest()->get();
            $cancelExemption    = Service::where('type',SERVICE_TYPE_EXEMPTION)->where('status',SERVICE_STATUS_CANCEL)->latest()->get();
            DB::commit();
            return view('staff.service.exemption', compact('allExemption','waitingExemption','approvedExemption','cancelExemption')); 
        }catch(\Exception $ex)
        {
            DB::rollBack();
           abort(404);
        } 
     
    }
    public function updateSuccess($id)
    {
        try{
            DB::beginTransaction();
            $idUser               = Auth::user()->id;
            $service              = Service::find($id);
            $service->status      = SERVICE_STATUS_APPROVED;
            $userStudent          = $service->save();
            if($userStudent) {
                $userStudent              = UserStudent::where('user_id',$service->user_id)->first();
                $userStudent->status      = USERSTUDENT_PASS;
                $userStudent->is_accept   = 0;
                $checkEmail               = $userStudent->save();
                if($checkEmail) {
                    $staff = User::where('id',$idUser)->first();
                    $users = []; array_push($users,$service);
                    SendExemptionSuccess::dispatch($users,$staff);
    
                    $dataNoti = [
                        'title' =>  'Yêu cầu miễn giảm thực tập thành công !',
                        'object' => NOTIFY_STUDENT,
                        'userId' => $service->user_id,
                        'url' => '/student/service-exemption-student/create',
                    ];
                    $this->createNotification($dataNoti);
                }
            }
            DB::commit();
            return redirect()->back()->with('success', 'Xác nhận miễn thực tập sinh viên thành công');
        }catch(\Exception $ex)
        {
            DB::rollBack();
           abort(404);
        } 
        
    }
    public function exemptionFail(Request $request)
    {
        $input = $request->all();
        try{
            DB::beginTransaction();
            $data      =  Arr::get($input, 'data', null);
            if ($data) {
                $id     = Arr::get($data, 'id', null);
                $reason = Arr::get($data, 'reason', null);
                $validator = Validator::make($data, [
                    'reason' => 'required',
                ], [
                    "reason.required"   => "Vui lòng nhập lý do từ chối đăng ký thực tập lại !",
                ]);
                if ($validator->fails()) {
                    return  response()->json(['error' => $validator->errors()]);
                }
                $idUser             = Auth::user()->id;
                $service            = Service::find($id);
                $service->status    = SERVICE_STATUS_DENINED;
                $service->reason    = $reason;
                $checkEmail         = $service->save();
                if($checkEmail)
                {
                    $staff = User::where('id',$idUser)->first();
                    $users = []; array_push($users,$service);
                    SendExemptionFail::dispatch($users,$staff);

                    $dataNoti = [
                        'title' =>  'Yêu cầu miễn giảm thực tập không thành công !',
                        'object' => NOTIFY_STUDENT,
                        'userId' => $service->user_id,
                        'url' => '/student/service-exemption-student/create',
                    ];
                    $this->createNotification($dataNoti);
                }
            }
            DB::commit();
            return $checkEmail;
            return redirect()->back()->with('success', 'Đã huỷ miễn thực tập sinh viên !');
        }catch(\Exception $ex)
        {
            DB::rollBack();
           abort(404);
        } 
       
    }
}
