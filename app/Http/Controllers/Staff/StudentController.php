<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\City;
use App\Models\District;
use App\Models\Ward;
use App\Models\ModelControllers\UserModel;
use App\Http\Requests\Staff\StudentCreateRequest;
use App\Http\Requests\Staff\StudentUpdateRequest;
use App\Jobs\SendAccountStudent;
use App\Models\CareerGroup;
use App\Models\Careers;
use App\Models\Report;
use App\Models\Service;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class StudentController extends Controller
{

    public function index()
    {
        try {
            DB::beginTransaction();
            $date     = Carbon::now()->sub('1 days')->format('Y-m-d H:i:s');
            $longTime = Carbon::now()->sub('3 months')->format('Y-m-d H:i:s');
            //query
            $students = User::with('student')->whereRelation('role', 'code', STUDENT_ROLE_CODE)->latest()->get(); //all
            $newStudents      = User::whereRelation('role', 'code', STUDENT_ROLE_CODE)->where('created_at', '>=', $date)->latest()->get(); //create before 1 day
            $studentInterns   = User::with('report')->whereRelation('role', 'code', STUDENT_ROLE_CODE)
                ->where(function ($query) {
                    $query->whereRelation('student', 'status', '!=', NULL)
                        ->orwhereHas('reports', function ($query) {
                            $query->where('is_active', 1);
                        });
                })
                ->get();
//            dd($studentInterns);
            $studentsNoActive = User::whereRelation('role', 'code', STUDENT_ROLE_CODE)->where(function ($query) use ($longTime){
                $query->where('created_at', '<', $longTime)->whereNull('logged_at')
                    ->orWhere('is_active',0);
            })->get(); //account don't logged for 4 month
            $countPassStudents = User::withCount('reports')->with('reports')
                ->whereHas('student', function ($query) {
                    $query->where('current_semester', 7);
                })
                ->whereHas('reports', function ($query) {
                    $query->where('status', 'PASS')->where('is_active', 1);
                })
                ->get();
            $studentsActive = User::whereRelation('role', 'code', STUDENT_ROLE_CODE)
                ->whereHas('student', function ($query) {
                    $query->where('is_active', 1);
                })
                ->get();
            DB::commit();
            return view('staff.student.index', compact('students', 'newStudents', 'studentInterns', 'studentsNoActive','countPassStudents','studentsActive'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return redirect()->route('staff.student.index')->with('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    public function create()
    {
        try {
            return view('staff.student.createStudent');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function sendAccount($id)
    {
        try {
            $password = Str::random(8);
            $user     = User::findOrFail($id);
            $user->fill(['password' => Hash::make($password)])->save();
            $users = [];
            array_push($users, $user);
            $message = [
                'password' => $password,
            ];
            $sent    = SendAccountStudent::dispatch($users, $message)->delay(now()->addMinute(1));
            if ($sent) return redirect()->route('staff.student.index')->with('notify', 'Đã gửi email tài khoản cho sinh viên');
        } catch (\Exception $ex) {
            abort(404);
        }
        return redirect()->back()->with('fail', 'Đã xảy ra sự cố vui lòng thử lại sau');
    }

    public function store(StudentCreateRequest $request)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            $input['password'] = Str::random(8);
            $studentModel      = new UserModel;
            $student           = $studentModel->upsertStudent($input);
            /*if($request->file('avatar')){
                $imageAvatar = $this->upload($request->file('avatar'));
                $input['avatar'] = $imageAvatar->code;
            }*/ //chuẩn bị làm
            if (!empty($input['account'])) {
                $sendAccount = $input['account'];
                if ($sendAccount == 1) {
                    $users = [];
                    array_push($users, $student);
                    $message = [
                        'password' => $input['password'],
                    ];
                    SendAccountStudent::dispatch($users, $message)->delay(now()->addMinute(1));
                }
            }
            DB::commit();
            return redirect()->route('staff.student.index')->with('success', 'Thêm sinh viên thành công');
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return redirect()->route('staff.student.index')->with('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    public function show($id)
    {
        try {
            $user = User::where('id', $id)->whereRelation('role', 'code', STUDENT_ROLE_CODE)->first();
            $report = Report::with(['reportDetail' => function ($query) {
                $query->orderBy('date', 'ASC');
            }])->where('user_id', $id)->first();
            if($report)
            {
                $reportCount = Report::with(['reportDetail' => function ($query) {
                    $query->orderBy('date', 'ASC');
                }])->where('user_id', $id)->count();
                $reportAll = Report::where('user_id',$id)->get();
                $cv = [];
                $rateFile = [];
                if (empty($report))  abort(404);
                if (!empty($report->user->student->getCV())) {
                    $viewCV = $this->getFilePdf($report->user->student->getCV()->code);
                    $nameCV =  preg_replace('/\s+/', '',$this->vn_to_str($report->user->student->getCV()->name)).'.pdf';
                    $cv = ['view'=>$viewCV,'name'=>$nameCV];
                }
                if(!empty($report->rateFile)){
                    $viewRateFile = $this->getFilePdf($report->rate_file);
                    $nameRateFile =  preg_replace('/\s+/', '',$this->vn_to_str($report->user->name)).'.pdf';
                    $rateFile = ['view'=>$viewRateFile,'name'=>$nameRateFile];
                }
                $reportDetail = $report->reportDetail;
                $arrDate = Arr::pluck($reportDetail, 'date');
                $allWeek = $this->getAllWeek($report->start, $report->end);
                $service = Service::where('user_id',$user->id)->whereIn('type',[SERVICE_TYPE_EXEMPTION,SERVICE_TYPE_INTERNSHIP_CANCEL])->get();
            }
            else{
                return view('staff.student.detailStudent', compact('user'));
            }
            if ($user && $report)  return view('staff.student.detailStudent', compact('user','report', 'reportDetail', 'allWeek','cv','rateFile','arrDate','reportCount','reportAll','service'));
        } catch (\Exception $ex) {
            abort(404);
        }
        return redirect()->back()->with('fail', 'Tài khoản đang có sự cố!');
    }

    public function edit($id)
    {
        try {
            DB::beginTransaction();
            $careers = Careers::all();
            $user    = User::findOrFail($id);
            $cv      = [];
            if (!empty($user->student->getCV())) {
                $viewCV = $this->getFilePdf($user->student->getCV()->code);
                $nameCV = preg_replace('/\s+/', '', $this->vn_to_str($user->student->getCV()->name)) . '.pdf';
                $cv     = ['view' => $viewCV, 'name' => $nameCV];
            }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return view('staff.student.editStudent', compact('user', 'careers', 'cv'));
    }

    public function update($id, StudentUpdateRequest $request)
    {
        $input       = $request->all();
        $input['id'] = $id;

        try {
            DB::beginTransaction();
            $userModel = new UserModel();
            $userModel->upsertStudent($input, true);
            DB::commit();
            return redirect()->route('staff.student.index')->with('success', 'Chỉnh sửa sinh viên thành công');
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return redirect()->route('staff.student.index')->with('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $user    = User::with('profile')->find($id);
            $name    = $user->name;
            $profile = $user->profile->delete();
            if ($profile) {
                $user->delete();
            }
            DB::commit();
            return redirect()->route('staff.student.index')->with('warning', 'Sinh viên ' . $name . ' đã được xóa');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return redirect()->route('staff.student.index')->with('fail', 'Hệ thống đã xảy ra sự cố !');
    }
    public function studentSerivce($id)
    {
        try{
            DB::beginTransaction();
            $service = Service::find($id);
            if($service->type == SERVICE_TYPE_EXEMPTION)
            {
                $allExemption       = Service::where('type',SERVICE_TYPE_EXEMPTION)->whereIn('status',[SERVICE_STATUS_PENDING,SERVICE_STATUS_APPROVED,SERVICE_STATUS_DENINED,SERVICE_STATUS_CANCEL])->latest()->get();
                $waitingExemption   = Service::where('type',SERVICE_TYPE_EXEMPTION)->where('status',SERVICE_STATUS_PENDING)->latest()->get();
                $approvedExemption  = Service::where('type',SERVICE_TYPE_EXEMPTION)->whereIn('status',[SERVICE_STATUS_APPROVED,SERVICE_STATUS_DENINED])->latest()->get();
                $cancelExemption    = Service::where('type',SERVICE_TYPE_EXEMPTION)->where('status',SERVICE_STATUS_CANCEL)->latest()->get();
                return view('staff.service.exemption', compact('allExemption','waitingExemption','approvedExemption','cancelExemption'));
            }
            else
            {
                $service = Service::where('type',SERVICE_TYPE_INTERNSHIP_CANCEL)->get();
                $serviceStudent = Service::where('type',SERVICE_TYPE_INTERNSHIP_CANCEL)->whereRelation('roleUser', 'roles.code', STUDENT_ROLE_CODE)->get();
                $serviceCompany = Service::where('type',SERVICE_TYPE_INTERNSHIP_CANCEL)->whereRelation('roleUser', 'roles.code', COMPANY_ROLE_CODE)->get();
                return view('staff.request.requestReport', compact('service', 'serviceStudent', 'serviceCompany'));
            }
            DB::commit();
        }catch(\Exception $ex)
        {
            DB::rollBack();
            abort(404);
        }
    }
}
