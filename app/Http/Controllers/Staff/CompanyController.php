<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Jobs\SendRegisterCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Company;
use App\Models\City;
use App\Models\District;
use App\Models\Ward;
use App\Models\ModelControllers\CompanyModel;
use App\Http\Requests\Staff\CompanyCreateRequest;
use App\Http\Requests\Staff\CompanyUpdateRequest;
use App\Http\Requests\Staff\PostUpdateRequest;
use App\Http\Requests\Staff\StaffCompanyCreateRequest;
use App\Http\Requests\Staff\StaffCompanyUpdateRequest;
use App\Http\Requests\Staff\StudentUpdateRequest;
use App\Models\ModelControllers\UserModel;
use App\Models\Post;
use App\Models\PostType;
use App\Models\RecruitmentPost;
use App\Models\Report;
use App\Models\Role;
use App\Models\UserStudent;
use Illuminate\Support\Str;

class CompanyController extends Controller
{

    public function index()
    {
        try {
            $company = Company::where('status', COMPANY_STATUS_APPROVED)->get();
            $companyDenied = Company::where('status', COMPANY_STATUS_DENIED)->get();
        } catch (\Exception $ex) {
            abort(404);
        }
        return view('staff.company.index', compact('company', 'companyDenied'));
    }

    public function create()
    {
        try {
            DB::beginTransaction();
            $city     = City::all();
            $district = District::all();
            $ward     = Ward::all();
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return view('staff.company.createCompany', compact('city', 'district', 'ward'));
    }

    public function store(CompanyCreateRequest $request)
    {
        $input = $request->all();
        try {

            DB::beginTransaction();
            $getBanner   = $request->file('banner');
            $getAvatar   = $request->file('avatar');
            $imageBanner = $this->upload($getBanner);
            $imageAvatar = $this->upload($getAvatar);

            if ($imageBanner && $imageAvatar) {
                $company         = new CompanyModel();
                $input['banner'] = $imageBanner->code;
                $input['avatar'] = $imageAvatar->code;
                $company->upsertCompany($input);
            }
            DB::commit();
            return redirect()->route('staff.company.index')->with('success', 'Thêm doanh nghiệp thành công');
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return redirect()->route('staff.company.index')->withErrors('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();
            $company     = Company::find($id);
            $users       = User::with('profile')->where('company_id', $company->id)->get();
            $postCompany = Post::where('post_type_id')->where('is_active', 1)->get();
            $student     = Report::where('is_active', 1)->where('company_id', $company->id)->get();
            if ($company) {
                return view('staff.company.detailCompany', compact('company', 'users', 'postCompany', 'student'));
            }
            return redirect()->back()->withErrors('Tài khoản đang có sự cố!');
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
    }

    public function edit($id)
    {
        try {
            DB::beginTransaction();
            $company = Company::find($id);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return view('staff.company.editCompany', compact('company'));
    }

    public function update($id, CompanyUpdateRequest $request)
    {
        $input       = $request->all();
        $input['id'] = $id;
        try {
            DB::beginTransaction();
            if (!empty($request->banner)) {
                $getBanner       = $request->file('banner');
                $imageBanner     = $this->upload($getBanner);
                $input['banner'] = $imageBanner->code;
            }
            if (!empty($request->avatar)) {
                $getAvatar       = $request->file('avatar');
                $imageAvatar     = $this->upload($getAvatar);
                $input['avatar'] = $imageAvatar->code;
            }
            $companyModel = new CompanyModel();
            $companyModel->upsertCompany($input, true);
            DB::commit();
            return redirect()->route('staff.company.index')->with('success', 'Chỉnh sửa doanh nghiệp thành công');
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return redirect()->route('staff.company.index')->with('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $company = Company::find($id);
            // $users = User::with('profile')->where('company_id', $company->id)->get();
            // if ($company && $users) {
            //     foreach ($users as $key => $row) {
            //         $profile = $row->profile->delete();
            //         if ($profile) {
            //             $row->delete();
            //         }
            //     }
            $company->delete();
            // }
            DB::commit();
            return redirect()->route('staff.company.index')->with('warning', 'Doanh nghiệp đã được xóa');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return redirect()->route('staff.company.index')->withErrors('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    /* RESOURCE STAFF COMPANY */
    public function showStaffCompany($id)
    {
        try {
            DB::beginTransaction();
            $user = User::with('profile')->find($id);
            if ($user) return view('staff.company.detailStaffCompany', compact('user'));
            DB::commit();
            return redirect()->back()->withErrors('Tài khoản đang có sự cố!');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function createStaffCompany(Request $request)
    {
        try {
            $input     = $request->all();
            $companyId = $input['company_id'];
            DB::beginTransaction();
            $city     = City::all();
            $district = District::all();
            $ward     = Ward::all();
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return view('staff.company.createStaffCompany', compact('city', 'district', 'ward', 'companyId'));
    }

    public function storeStaffCompany(StaffCompanyCreateRequest $request)
    {
        $input     = $request->all();
        $companyId = $input['company_id'];
        try {
            DB::beginTransaction();
            $staffCompanyModel = new CompanyModel;
            $staffCompanyModel->upsertStaffCompany($input);
            DB::commit();
            return redirect()->route('staff.company.show', $companyId)->with('success', 'Thêm nhân viên thành công');
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return redirect()->back()->withErrors('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    public function editStaffCompany($id)
    {
        try {
            DB::beginTransaction();
            $user    = User::find($id);
            $profile = $user->profile;
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return view('staff.company.editStaffCompany', compact('user', 'profile'));
    }

    public function updateStaffCompany(StaffCompanyUpdateRequest $request, $id)
    {
        $input       = $request->all();
        $input['id'] = $id;
        try {
            $user      = User::find($id);
            $companyId = $user->company_id;
            DB::beginTransaction();
            $staffCompanyModel = new CompanyModel();
            $staffCompanyModel->upsertStaffCompany($input, true);
            DB::commit();
            return redirect()->route('staff.company.show', $companyId)->with('success', 'Chỉnh sửa nhân viên thành công');
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return redirect()->route('staff.company.show', $companyId)->with('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    public function destroyStaffCompany($id)
    {
        try {
            DB::beginTransaction();
            $user    = User::with('profile')->find($id);
            $name    = $user->name;
            $profile = $user->profile->delete();
            if ($profile) {
                $user->delete();
            }
            DB::commit();
            return redirect()->back()->with('warning', 'Nhân viên ' . $name . ' đã được xóa');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return redirect()->back()->withErrors('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    /* RESOURCE POST COMPANY */
    public function showPostCompany($id)
    {
        try {
            $post = Post::where('post_type_id', 3)->where('is_active', 1)->get();
            return view('staff.company.detailPostCompany', compact('post'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function editPostCompany($id)
    {
        try {
            $post      = Post::find($id);
            $postTypes = PostType::all();
            return view('staff.company.editPostCompany', compact('post', 'postTypes'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function updatePostCompany(PostUpdateRequest $request, $id)
    {
        try {
            $input       = $request->all();
            $input['id'] = $id;
            DB::beginTransaction();
            if (!empty($request->thumbnail)) {
                $getThumbnail       = $request->file('thumbnail');
                $imageUpload        = $this->upload($getThumbnail);
                $input['thumbnail'] = $imageUpload->code;
            }
            $post = new CompanyModel();
            $post->upsertPost($input, true);
            DB::commit();
            return redirect()->back()->with('success', 'Chỉnh Bài Viết Thành Công');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return redirect()->back()->withErrors('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    public function destroyPostCompany($id)
    {
        try {
            Post::find($id)->delete();
            return redirect()->back()->with('warning', 'Bài viết đã được xóa');
        } catch (\Exception $ex) {
            abort(404);
        }
        return redirect()->back()->withErrors('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    /* RESOUCE STUDENT COMPANY */
    public function showStudentCompany($id)
    {
        try {
            DB::beginTransaction();
            $user    = User::find($id);
            $profile = $user->profile;
            $student = $user->student;
            if ($user && $profile && $student) return view('staff.company.detailStudentCompany', compact('user', 'profile', 'student'));
            DB::commit();
            return redirect()->back()->withErrors('Tài khoản đang có sự cố!');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function editStudentCompany($id)
    {
        try {
            DB::beginTransaction();
            $user     = User::find($id);
            $profile  = $user->profile;
            $students = $user->student;
            DB::commit();
            return view('staff.company.editStudentCompany', compact('user', 'students', 'profile'));
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
    }

    public function updateStudentCompany($id, StudentUpdateRequest $request)
    {
        $input       = $request->all();
        $input['id'] = $id;
        try {
            DB::beginTransaction();
            $user      = User::find($id);
            $companyId = $user->company_id;
            $userModel = new UserModel();
            $userModel->upsertStudent($input, true);
            DB::commit();
            return redirect()->route('staff.company.show', $companyId)->with('success', 'Chỉnh sửa sinh viên thành công');
        } catch (\Exception $ex) {
            DB::rollback();
            abort(404);
        }
        return redirect()->route('staff.student.index')->withErrors('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    public function destroyStudentCompany($id)
    {
        try {
            DB::beginTransaction();
            $user    = User::with('profile')->find($id);
            $name    = $user->name;
            $profile = $user->profile->delete();
            if ($profile) {
                $user->delete();
            }
            DB::commit();
            return redirect()->back()->with('warning', 'Sinh viên ' . $name . ' đã được xóa');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return redirect()->route('staff.student.index')->withErrors('fail', 'Hệ thống đã xảy ra sự cố !');
    }

    #### Pending-Company---------->

    public function pendingCompany()
    {
        try {
            $pendingCompany = Company::where('status', 'WAITING')->get();
            return view('staff.company.showPendingCompany', compact('pendingCompany'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    public function approveCompany(Request $request, $id)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            $company = Company::where('id', $id)->first();
            $status = "";
            $approvedCompany = false;
            if (!empty($input['approved'])) $status = COMPANY_STATUS_APPROVED;
            if (!empty($input['denied'])) $status = COMPANY_STATUS_DENIED;
            if (!empty($company->status == 'WAITING') && !empty($status)) {
                switch ($status) {
                    case COMPANY_STATUS_APPROVED:
                        $company->status    = COMPANY_STATUS_APPROVED;
                        $company->code      = 'DN_' . time();
                        $company->is_active = 1;
                        $approvedCompany = true;
                        if ($company->userRegister->role->code == STUDENT_ROLE_CODE) {
                            $input = [
                                'title' =>  'Yêu cầu đăng ký thực tập của bạn đã được phê duyệt!',
                                'object' => NOTIFY_STUDENT,
                                'userId' => $company->created_by,
                            ];
                        } else {
                            $input = [
                                'title' =>  'Chúc mừng doanh nghiệp ' . $company->short_name . ' tham gia vào hệ thống!',
                                'object' => NOTIFY_COMPANY,
                                'object_detail' => $company->id,
                            ];
                        }
                        break;
                    case COMPANY_STATUS_DENIED:
                        $company->status    = COMPANY_STATUS_DENIED;
                        if ($company->userRegister->role->code == STUDENT_ROLE_CODE) {
                            UserStudent::where('user_id', $company->created_by)->update(['is_accept' => 1]);
                            $input = [
                                'title' =>  'Yêu cầu đăng ký thực tập của bạn đã bị từ chối, vui lòng đăng ký lại!!',
                                'object' => NOTIFY_STUDENT,
                                'userId' => $company->created_by,
                            ];
                        } else {
                            $input = [
                                'title' =>  'Thông tin Doanh nghiệp ' . $company->short_name . ' không đúng, vui lòng kiểm tra lại!!',
                                'object' => NOTIFY_COMPANY,
                                'object_detail' => $company->id,
                            ];
                        }
                        break;
                    default:
                        return redirect()->back()->with('fail', 'Vui lòng thử lại sau !');
                }
                $this->createNotification($input);

                $getLatLong = $this->getLatLong($company->getAddress());
                if ($getLatLong) {
                    $company->lat = $getLatLong->latitude;
                    $company->lng = $getLatLong->longitude;
                }
                $company->save();
                $data = [
                    'company' => $company,
                    'status' => $status,
                ];
                $sent =  SendRegisterCompany::dispatch($company->userRegister, $data);
            }
            if ($approvedCompany) {

                DB::commit();
                return redirect()->back()->with('success', 'Duyệt doanh nghiệp thành công');
            }
            if ($company->userRegister->role->code == STUDENT_ROLE_CODE) {
                $report = Report::where('user_id', $company->created_by)->where('is_active', 1)->first();
                UserStudent::where('user_id', $company->created_by)->update(['is_accept' => 1]);
                $report->delete();
                $company->delete();
            }
            DB::commit();
            return redirect()->back()->with('warning', 'Doanh nghiệp đã bị từ chối');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
        return redirect()->route('staff.company.index')->with('fail', 'Vui lòng thử lại sau !');
    }
}
