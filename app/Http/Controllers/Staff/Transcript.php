<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\UserStudent;
use Illuminate\Http\Request;

class Transcript extends Controller
{
    function index(){
        $transcript        = Service::where('deleted','!=',1)->where('type',SERVICE_TYPE_TRANSCRIPT)->whereIn('status',[SERVICE_STATUS_PENDING,SERVICE_STATUS_APPROVED,SERVICE_STATUS_DENINED])->get();
        $transcriptRequest = Service::where('deleted','!=',1)->where('type',SERVICE_TYPE_TRANSCRIPT)->where('status',SERVICE_STATUS_PENDING)->get();
        $transcriptRespone = Service::where('deleted','!=',1)->where('type',SERVICE_TYPE_TRANSCRIPT)->where('status',SERVICE_STATUS_APPROVED)->get();
        return view('staff.transcript.index',compact('transcript','transcriptRequest','transcriptRespone'));
    }
    function store(Request $request,$id){
        $input = $request->all();
        $file = $request->file('file');
        if (!empty($file)) {
            $fileUpload = $this->upload($file, 'pdf');
        }
        $transcript = Service::where('user_id',$id)->where('type',SERVICE_TYPE_TRANSCRIPT)->latest()->first();
        $transcript->file = $fileUpload->code;
        $transcript->status = SERVICE_STATUS_APPROVED;
        $dataNoti = [
            'title' => 'Yêu cầu cấp bảng điểm của sinh viên đã được cấp thành công!',
            'object' => NOTIFY_STUDENT,
            'userId' => $transcript->user_id,
            'url' => '/student/transcript',
        ];
        $this->createNotification($dataNoti);
        $transcript->save();
        return redirect()->route('staff.transcript.index')->with('success','Cấp bảng điểm thành công');
    }
}
