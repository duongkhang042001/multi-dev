<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Http\Requests\Staff\PostTypeCreateRequest;
use App\Http\Requests\Staff\PostTypeUpdateRequest;
use App\Models\ModelControllers\PostModel;
use App\Models\PostType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $postType = PostType::all();
            return view('staff.postType.index', compact('postType'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('staff.postType.createPostType');
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostTypeCreateRequest $request)
    {
        try {
            $input = $request->all();
            $postTypeModel = new PostModel();
            $postTypeModel->upsertPostType($input);
            return redirect()->back()->with('success', 'Tạo loại bài viết thành công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $postType = PostType::find($id);
            return view('staff.postType.detailPostType', compact('postType'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $postType = PostType::find($id);
            return view('staff.postType.editPostType', compact('postType'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostTypeUpdateRequest $request, $id)
    {
        try {
            $input = $request->all();
            $input['id'] = $id;
            $postTypeModel = new PostModel();
            $postTypeModel->upsertPostType($input, true);
            return redirect()->back()->with('success', 'Chỉnh loại bài viết thành công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $postTypes = PostType::find($id);
            $postTypes->delete();
            return redirect()->route('staff.post-type.index') - with('warning', 'Xóa loại bài viết thành công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }
}
