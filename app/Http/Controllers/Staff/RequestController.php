<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Models\RecruitmentPostDetail;
use App\Models\Report;
use App\Models\Service;
use App\Models\UserStudent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RequestController extends Controller
{
    public function showRequestReport()
    {
        try {
            $service        = Service::where('type',SERVICE_TYPE_INTERNSHIP_CANCEL)->get();
            $serviceStudent = Service::where('type',SERVICE_TYPE_INTERNSHIP_CANCEL)->whereRelation('roleUser', 'roles.code', STUDENT_ROLE_CODE)->get();
            $serviceCompany = Service::where('type',SERVICE_TYPE_INTERNSHIP_CANCEL)->whereRelation('roleUser', 'roles.code', COMPANY_ROLE_CODE)->get();
            return view('staff.request.requestReport', compact('service', 'serviceStudent', 'serviceCompany'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function updateRequestCompany(Request $request, $id)
    {
        $input = $request->all();
        try {
            $timeline = $this->checkTimeLine('find');
            $service = Service::findOrFail($id);
            $report =  Report::where('id',$service->report_id)->where('is_active',1)->first();
            $type = !empty($input['appoved']) ? 'appoved' : 'cancel';
            switch ($type) {
                case 'appoved':
                    $service->status = SERVICE_STATUS_APPROVED;
                    if($timeline && $report->semester->is_current){
                        $report->status = REPORT_CANCEL;
                        $report->is_accept = 0;
                        $student = UserStudent::where('user_id', $service->user_id)->first();
                        $student->is_accept = 1;
                    }
                    else {
                        $report->status = REPORT_FAIL;
                        $report->is_accept = 0;
                        $student = UserStudent::where('user_id', $service->user_id)->first();
                        $student->is_accept = 0;
                    }
                    break;
                case 'cancel':
                    $service->status = SERVICE_STATUS_DENINED;
                    break;
                default: return redirect()->route('staff')->with('fail','Đã xảy ra sự cố, vui lòng quay lại sao');
            }
            $dataNoti = [
                'title' => 'Yêu cầu hủy thực tập sinh viên của doanh nghiệp đã được chấp thuận!',
                'object' => NOTIFY_COMPANY,
                'object_detail' => $service->report->company_id,
                'url' => '/company/intern-student',
            ];
            $this->createNotification($dataNoti);
            $student->save();
            $service->save();
            $report->save();
            DB::commit();
            return redirect()->back()->with('success', 'Duyệt xử lý thành công');
        } catch (\Exception $ex) {
//            dd($ex->getMessage());
            abort(404);
        }
    }

    public function updateRequestStudent(Request $request,$id)
    {
     $input = $request->all();
        try {
            $timeline   = $this->checkTimeLine('find');
            $service    = Service::findOrFail($id);
            $report     =  Report::where('id',$service->report_id)->where('is_active',1)->first();
            $type       = !empty($input['appoved']) ? 'appoved' : 'cancel';
            switch ($type) {
                case 'appoved':
                    $service->status = SERVICE_STATUS_APPROVED;
                    if($timeline && $report->semester->is_current){
                        $report->status = REPORT_CANCEL;
                        $report->is_accept = 0;
                        $student = UserStudent::where('user_id', $report->user_id)->first();
                        $student->is_accept = 1;
                    }
                    else {
                        $report->status = REPORT_FAIL;
                        $report->is_accept = 0;
                        $student = UserStudent::where('user_id', $report->user_id)->first();
                        $student->is_accept = 0;
                    }
                    break;
                case 'cancel':
                    $service->status = SERVICE_STATUS_DENINED;
                    break;
                default: return redirect()->route('staff')->with('fail','Đã xảy ra sự cố, vui lòng quay lại sao');
            }
            $dataNoti = [
                'title' => 'Yêu cầu hủy thực tập của sinh viên đã được chấp thuận!',
                'object' => NOTIFY_STUDENT,
                'userId' => $service->user_id,
                'url' => '/student/report',
            ];
            $this->createNotification($dataNoti);
            $student->save();
            $service->save();
            $report->save();
            DB::commit();
            return redirect()->back()->with('success', 'Duyệt xử lý thành công');
        } catch (\Exception $ex) {
//            dd($ex->getMessage());
            abort(404);
        }
    }

    public function listRequestResetReport()
    {
        try {
            $requestResetReportAll      = Report::whereNotNull('request_reset')
                ->whereIn('status', [REPORT_DONE, REPORT_PENDING])
                ->get();
            $requestResetReportPending  = Report::whereNotNull('request_reset')
                ->whereIn('status', [REPORT_DONE, REPORT_PENDING])
                ->whereJsonContains('request_reset->status', 0)
                ->get();
            $requestResetReportSuscess  = Report::whereNotNull('request_reset')
                ->whereIn('status', [REPORT_DONE, REPORT_PENDING])
                ->whereJsonContains('request_reset->status', 1)
                ->get();
        } catch (\Exception $ex) {
            abort(404);
        }
        return view('staff.report.requestResetReport', compact('requestResetReportAll', 'requestResetReportPending', 'requestResetReportSuscess'));
    }

    public function handleRequestResetReport($id)
    {
        try {
            $report = Report::where('id', $id)->where('is_accept', 0)->whereNotNull('request_reset')->first();
            $requestReset = json_decode($report->request_reset);
            switch ($requestReset->request) {
                case PLAN_RESET_REPORT[$requestReset->request] === PLAN_RESET_REPORT[1]:
                    $report->is_accept          = 1;
                    $requestReset->status       = 1;
                    $report->request_reset      = json_encode($requestReset);
                    break;
                case PLAN_RESET_REPORT[$requestReset->request] === PLAN_RESET_REPORT[2]:
                    $userStudent                = UserStudent::where('user_id', $report->user_id)->first();
                    $userStudent->is_accept     = 1;
                    $userStudent->save();
                    RecruitmentPostDetail::where('user_id', $report->user_id)->update(['is_active' => 0]);
                    $report->is_active          = 0;
                    $requestReset->status       = 1;
                    $report->request_reset      = json_encode($requestReset);
                    break;
                default:
                    break;
            }
            $report->save();
            $dataNoti = [
                'title' =>  'Yêu cầu hổ trợ của bạn đã được nhà trường xử lý.',
                'object' => NOTIFY_STUDENT,
                'userId' => $report->user_id,
                'url' => '/student/service-exemption-student',
            ];
            $this->createNotification($dataNoti);
        } catch (\Exception $ex) {
            abort(404);
        }
        return redirect()->back()->with('notify', 'Xét duyệt cho sinh viên thành công!');
    }
}
