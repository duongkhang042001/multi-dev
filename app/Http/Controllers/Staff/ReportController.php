<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Jobs\SendInternshipFinished;
use App\Models\Report;
use App\Models\UserStudent;
use Exception;
use http\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function index()
    {
        try {
            $report     = Report::where('is_active', 1)->get();
            $reportDone = Report::where('is_active', 1)->where('status', REPORT_FINISHED)->get();
            $reportPass = Report::where('is_active', 1)->where('status', REPORT_PASS)->get();
            $reportFail = Report::where('is_active', 1)->where('status', REPORT_FAIL)->get();
            return view('staff.report.index', compact('report', 'reportDone', 'reportPass', 'reportFail'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        try {
            $report = Report::with(['reportDetail' => function ($query) {
                $query->orderBy('date', 'ASC');
            }])->where('id', $id)->first();
            $cv = [];
            $rateFile = [];
            if (empty($report))  abort(404);
            if (!empty($report->user->student->getCV())) {
                $viewCV = $this->getFilePdf($report->user->student->getCV()->code);
                $nameCV =  preg_replace('/\s+/', '',$this->vn_to_str($report->user->student->getCV()->name)).'.pdf';
                $cv = ['view'=>$viewCV,'name'=>$nameCV];
            }
            if(!empty($report->rateFile)){
                $viewRateFile = $this->getFilePdf($report->rate_file);
                $nameRateFile =  preg_replace('/\s+/', '',$this->vn_to_str($report->user->name)).'.pdf';
                $rateFile = ['view'=>$viewRateFile,'name'=>$nameRateFile];
            }
            $reportDetail = $report->reportDetail;
            $arrDate = Arr::pluck($reportDetail, 'date');
            $allWeek = $this->getAllWeek($report->start, $report->end);


            return view('staff.report.detail', compact('report', 'reportDetail', 'allWeek','cv','rateFile','arrDate'));
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['id'] = $id;
        if(empty($input['status']))  return redirect()->back()->with('fail', 'Đã xảy ra sự cố !');
        try {
            DB::beginTransaction();
            $report = Report::where('id',$id)->where('is_active',1)->first();
            $student = UserStudent::where('user_id',$report->user_id)->first();
            if(empty($report) && empty($student)){
                throw new Exception('Error');
            }
            $report->status   = $input['status'];
            $report->status_reason = !empty($input['status_reason']) ? $input['status_reason'] : null;
            $report->save();

            $student->status = STUDENT_STATUS_PASS;
            $student->save();
            $users = [];
            if($input['status'] == REPORT_FAIL){
                $dataNoti = [
                    'title' =>  'Bạn đã không đạt thực tập trong kỳ này. Vui lòng liên hệ với nhà trường để thực tập lại!',
                    'object' => NOTIFY_STUDENT,
                    'userId' => $report->user_id,
                    'url' => \Str::remove(env('APP_URL'),route('student.exemptionPass')),
                ];
                $data=[
                  'isSuccess' => false,
                  'company_name' => $report->company->name,
                  'reason'=> !empty($input['status_reason']) ? $input['status_reason'] : null
                ];
                array_push($users,$report->user);
                SendInternshipFinished::dispatch($users, $data);
            }

            if($input['status'] == REPORT_PASS){
                $dataNoti = [
                    'title' =>  'Chúc mừng bạn đã đạt thực tập!',
                    'object' => NOTIFY_STUDENT,
                    'userId' => $report->user_id,
                    'url' => \Str::remove(env('APP_URL'),route('student.exemptionPass')),
                ];
                $data=[
                    'isSuccess' => true,
                    'company_name' => $report->company->name
                ];
                array_push($users,$report->user);
                SendInternshipFinished::dispatch($users, $data);

            }

            $this->createNotification($dataNoti);
            DB::commit();
            return redirect()->route('staff.report.index')->with('success', 'Đánh giá thành công');
        } catch (\Exception $ex) {
            DB::rollBack();
//            dd($ex);
            abort(404);
        }
    }

    public function destroy($id)
    {
        //
    }
}
