<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Careers;
use App\Models\CareerGroup;
use App\Models\ModelControllers\CareersModel;
use App\Http\Requests\Staff\CareersRequest;
use App\Http\Requests\Staff\CareerUpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CareersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $careers = Careers::with('careerGroup')->orderByDesc('id')->get();
            $careerGroups = CareerGroup::all();
        } catch (\Exception $ex) {
            abort(404);
        }

        return view('staff.careers.index', compact('careers','careerGroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $careerGroups = CareerGroup::all();
            return view('staff.careers.createCareer', compact('careerGroups'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CareersRequest $request)
    {
        $input = $request->all();
        try {
            $careersModel = new CareersModel;
            $careersModel->upsertCareers($input);
            return redirect()->route('staff.careers.index')->with('success', 'Đã thêm ngành thành công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $career = Careers::find($id);
            $careerGroups = CareerGroup::all();
            return view('staff.careers.editCareer', compact('career', 'careerGroups'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CareerUpdateRequest $request, $id)
    {
        $input = $request->all();
        $input['id'] = $id;
        try {
            $careersModel = new CareersModel();
            $careersModel->upsertCareers($input, true);
            return redirect()->route('staff.careers.index')->with('success', 'Đã sửa thành công');
        } catch (\Exception $ex) {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $careers = Careers::find($id);
            $careers->delete();
            return redirect()->route('staff.careers.index')->with('warning', 'Xóa thành công');
        } catch (\Exception $ex) {
            abort(404);
        };
    }
}
