<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\RecruitmentPost;
use App\Models\RecruitmentPostDetail;
use App\Models\Semester;
use App\Models\UserStudent;
use App\Models\Timeline;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class HomeController extends Controller
{

    public function index()
    {
        try {
            $studentInSemester =   $staff = User::where('role_id', STUDENT_ROLE_ID)->count();;

            $companyActive = User::whereHas('company', function ($query) {
                $query->where('is_active', 1)->where('on_system', 1);
            })->count();
            $recruitmentPost = RecruitmentPost::count();
            $companyTopRecruitment = Company::withCount('recruitmentPostDetails')->with('recruitmentPostDetails')
                ->orderBy('recruitment_post_details_count', 'DESC')->limit(15)->get();

            $countPendingStudents = User::with('reports')
                ->whereHas('reports', function ($query) {
                    $query->where('status', 'PENDING')->where('is_active', 1);
                })
                ->get();
            $countPassStudents = User::withCount('reports')->with('reports')
                ->whereHas('reports', function ($query) {
                    $query->where('status', 'PASS')->where('is_active', 1);
                })
                ->get();

            $countFailStudents = User::withCount('reports')->with('reports')
                ->whereHas('reports', function ($query) {
                    $query->where('status', 'FAIL')->where('is_active', 1);
                })
                ->get();
            $countDoneStudents = User::withCount('reports')->with('reports')
                ->whereHas('reports', function ($query) {
                    $query->where('status', 'DONE')->where('is_active', 1);
                })
                ->get();
            $countCancelStudents = User::withCount('reports')->with('reports')
                ->whereHas('reports', function ($query) {
                    $query->where('status', 'CANCEL')->where('is_active', 1);
                })
                ->get();
            return view('staff.home', compact('studentInSemester', 'companyActive', 'recruitmentPost',
            'companyTopRecruitment', 'countPendingStudents', 'countPassStudents', 'countFailStudents','countDoneStudents','countCancelStudents'));
        } catch (\Exception $ex) {
            DB::rollBack();
            abort(404);
        }
    }
}
