<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class NotificationEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $object;
    public $userId;
    public $object_detail;

    public function __construct($request)
    {
        $this->object  = $request['object'];
        $this->userId  = !empty($request['userId']) ? $request['userId'] : '';
        $this->object_detail  = isset($request['object_detail']) ? $request['object_detail'] : '';
    }

    public function broadcastOn()
    {
        switch ($this->object) {
            case NOTIFY_STUDENT:
                if(isset($this->object_detail) &&  $this->object_detail == 0){
                    return ['all-student'];
                }else if (!empty($this->userId)){
                    return ['student'];
                }else{
                }
                break;
            case NOTIFY_STAFF:
                if(isset($this->object_detail) &&  $this->object_detail == 0){

                    return ['all-staff'];
                }else{
           
                    return ['staff'];
                }
                break;
            case NOTIFY_ALL:
                return ['all-user'];
                break;
            case NOTIFY_COMPANY_TO_STUDENT:

                return ['student-detail'];
                break;
            case NOTIFY_COMPANY:
                return ['all-company'];
                break;
            case NOTIFY_STUDENT_TO_COMPANY:
                return ['company-detail'];
                break;
        }
    }

    public function broadcastAs()
    {
       
        switch ($this->object) {
            case NOTIFY_STUDENT:
                if(isset($this->object_detail) &&  $this->object_detail == 0){
                    return 'all-student';
                }else if (!empty($this->userId)){
                    return 'student'.$this->userId;
                }else{
                }
                return 'all-student';
                break;
            case NOTIFY_STAFF:

                if(isset($this->object_detail) &&  $this->object_detail == 0){
           
                    return 'all-staff';
                }else{
                    return 'staff'.$this->object_detail;
                }
                break;
            case NOTIFY_ALL:
                return 'all-user';
                break;
            case NOTIFY_COMPANY_TO_STUDENT:
                return 'companyToStudent' . $this->userId;
                break;
            case NOTIFY_COMPANY:
                return 'all-company';
                break;
            case NOTIFY_STUDENT_TO_COMPANY:
                return 'studentToCompany' . $this->object_detail;
                break;
        }
    }
}
