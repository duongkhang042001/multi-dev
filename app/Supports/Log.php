<?php

/**
 * User: Sy Dai
 * Date: 04-Apr-17
 * Time: 14:59
 */

namespace App\Supports;

use App\TM;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Log
{
    static function save($target,$method,$old_data = null, $new_data = null,$description = null)
    {
        $user_id = Auth::user()->id;
        $now = date('Y-m-d H:i:s', time());
        DB::table('user_logs')->insert([
            'action'      => $method,
            'target'      => $target,
            'description' => $description,
            'old_data'    => !empty($old_data) ? json_encode($old_data) : null,
            'new_data'    => !empty($new_data) ? json_encode($new_data) : null,
            'created_at'  => $now,
            'created_by'  => $user_id,
            'updated_at'  => $now,
            'updated_by'  => $user_id,
        ]);
    }
}
