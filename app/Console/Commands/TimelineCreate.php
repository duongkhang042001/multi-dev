<?php

namespace App\Console\Commands;

use App\Models\CareerGroup;
use Illuminate\Console\Command;
use App\Models\Semester;
use App\Models\Timeline;
use Carbon\Carbon;
use Illuminate\Support\Str;

class TimelineCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timeline:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $presentSemesters = Semester::whereDate('from',Carbon::today())->first();
        if (date('Y-m-d',time()) == date('Y-m-d', strtotime($presentSemesters->from))) {
            $explodeCodeSemester = explode("-", $presentSemesters->code);
            if ($explodeCodeSemester[0] == 'spring') {
                $newNameSemester = 'SUMMER'.' '.$explodeCodeSemester[1];
            }else if ($explodeCodeSemester[0]=='summer') {
                $newNameSemester = 'FALL'.' '.$explodeCodeSemester[1];
            }else{
                $year=$explodeCodeSemester[1] + 1;
                $newNameSemester = 'SPRING'.' '.$year;
            }

            $semester= Semester::where('name','=',$newNameSemester)->where('deleted_at', NULL)->first();
            if (!$semester) {
                $semester          = new Semester();
                $semester->name    = $newNameSemester;
                $semester->code    = Str::slug($newNameSemester);
                $semester->from    = date('Y-m-d H:i:s', strtotime('+5 day', strtotime($presentSemesters->to)));
                $semester->to      = date('Y-m-d H:i:s', strtotime('+120 day +23 hour +59 minutes +59 seconds', strtotime($presentSemesters->to)));
                $semester->save();
            }


            $timeline = Timeline::where('semester_id', $semester->id)->where('deleted_at', NULL)->first();;
            if (!$timeline) {
                $timeline          = new Timeline();
                $timeline->start_find    = date('Y-m-d H:i:s', strtotime('-80 day ', strtotime($semester->from)));
                $timeline->end_find    =  date('Y-m-d H:i:s', strtotime('-15 day ', strtotime($semester->from)));
                $timeline->start_support   =  date('Y-m-d H:i:s', strtotime('-45 day ', strtotime($semester->from)));
                $timeline->end_support    =  date('Y-m-d H:i:s', strtotime('-30 day ', strtotime($semester->from)));
                $timeline->first_notify    =  date('Y-m-d H:i:s', strtotime('-2 day', strtotime($semester->from)));
                $timeline->second_notify    =  date('Y-m-d H:i:s', strtotime('+12 day', strtotime($semester->from)));
                $timeline->start    =  date('Y-m-d H:i:s', strtotime('-2 day', strtotime($semester->from)));
                $timeline->end    =  date('Y-m-d H:i:s', strtotime('+70 day', strtotime($semester->from)));
                $timeline->semester_id    =  $semester->id;
                $timeline->created_at = now();
                $timeline->save();
            }
        }
    }
}
