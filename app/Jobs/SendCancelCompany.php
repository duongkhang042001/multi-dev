<?php

namespace App\Jobs;

use App\Mail\MailCancelCompany;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
class SendCancelCompany implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    protected $users;
    protected $reason;
    protected $userStudent;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users ,$data = null,$reason,$userStudent)
    {
        $this->data = $data;
        $this->users = $users;
        $this->reason = $reason;
        $this->userStudent = $userStudent;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $user) {
            Mail::to($user->email)->send(new MailCancelCompany($user,$this->data,$this->reason,$this->userStudent));
        }
    }
}
