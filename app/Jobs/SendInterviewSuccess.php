<?php

namespace App\Jobs;

use App\Mail\MailInterviewSuccess;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
class SendInterviewSuccess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    protected $users;
    protected $wf;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users ,$data = null,$wf = null)
    {
        $this->data = $data;
        $this->users = $users;
        $this->wf = $wf;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $user) {
            Mail::to($user->getRecruitmentPostDetail()->user->email)->send(new MailInterviewSuccess($user,$this->data,$this->wf));
        }
    }
}
