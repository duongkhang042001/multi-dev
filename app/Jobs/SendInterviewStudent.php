<?php

namespace App\Jobs;

use App\Mail\MailInterviewStudent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
class SendInterviewStudent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    protected $users;
    protected $interview;
    protected $interviewDate;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users,$data = null,$interview = null,$interviewDate)
    {
        $this->data = $data;
        $this->users = $users;
        $this->interview = $interview;
        $this->interviewDate = $interviewDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $user) {
            Mail::to($user->email)->send(new MailInterviewStudent($user,$this->data,$this->interview,$this->interviewDate));
        }
    }
}
