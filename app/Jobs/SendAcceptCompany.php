<?php

namespace App\Jobs;

use App\Mail\MailAcceptCompany;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
class SendAcceptCompany implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    protected $users;
    protected $userStudent;
   
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users ,$data = null,$userStudent)
    {
        $this->data = $data;
        $this->users = $users;
        $this->userStudent = $userStudent;
    
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        foreach ($this->users as $user) {
            Mail::to($user->email)->send(new MailAcceptCompany($user,$this->data,$this->userStudent));
        }
    }
}
