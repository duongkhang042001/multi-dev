<?php

namespace App\Jobs;

use App\Mail\MailDenidRecruitmentStudent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendDenidRecruitmentStudent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    protected $users;
    protected $reason;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users ,$data = null,$reason)
    {
        $this->data = $data;
        $this->users = $users;
        $this->reason = $reason;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $user) {
            Mail::to($user->email)->send(new MailDenidRecruitmentStudent($user,$this->data,$this->reason));
        }
    }
}
