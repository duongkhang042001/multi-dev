<?php

namespace App\Rules;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class UniqueProfileRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $check;
    protected $msg;
    protected $idUser;
    protected $name;
    protected $idProfile;
    public function __construct($idUser = null,$name='người dùng')
    {
        $this->idUser=$idUser;
        $this->name=$name;

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {


        if ($this->idUser)
        {
            $user = User::findOrFail($this->idUser);
            $this->idProfile = $user->profile->id;
            $profile = Profile::where($attribute,$value)->where('id','!=',$this->idProfile)->whereNull('deleted_at')->exists();
        }else{
            $profile = Profile::where($attribute,$value)->whereNull('deleted_at')->exists();

        }

         //SWITCH CASE này để tạo message xuất ra và bắt thêm những trường khó xử lý đi kèm unique
        switch ($attribute)
        {
            case 'email_personal':
                $this->msg = 'Email này này đã tồn tại trên hệ thống!';
                if($this->idUser) $user = User::where('email',$value)->where('id','!=',$this->idUser)->whereNull('deleted_at')->exists();
                else $user = User::where('email',$value)->whereNull('deleted_at')->exists();
                if($user)   $this->check = true;
                break;
            case 'phone':
                $this->msg = 'Số điện thoại này này đã tồn tại trên hệ thống!';
                break;

            case 'indo':
                    $this->msg = 'CMND này này đã tồn tại trên hệ thống!';
                break;
        }
            if($profile || $this->check) return false;
            return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if(!empty($this->msg)) return $this->msg;
        return "Hệ thống đang gặp sự cố vui lòng thử lại sau!";
    }
}
