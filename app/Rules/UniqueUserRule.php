<?php

namespace App\Rules;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class UniqueUserRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $check;
    protected $msg;
    protected $idProfile;
    protected $name;
    public function __construct($idProfile = null,$name='người dùng')
    {
        $this->idProfile=$idProfile;
        $this->name=$name;

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if ($this->idProfile) {
            $user = User::where($attribute,$value)->where('id','!=',$this->idProfile)->whereNull('deleted_at')->exists();
        }else{

            $user = User::where($attribute,$value)->whereNull('deleted_at')->exists();
        }

         //SWITCH CASE này để tạo message xuất ra và bắt thêm những trường khó xử lý đi kèm unique
         switch ($attribute) {
            case 'code':
                $this->msg = 'Mã số '.$this->name.' này đã tồn tại trên hệ thống!';
                break;

            case 'email':
                $this->msg = 'Email này đã tồn tại trên hệ thống!';
                if($this->idProfile) $profile = Profile::where('email_personal',$value)->where('id','!=',$this->idProfile)->whereNull('deleted_at')->exists();
                else $profile = Profile::where('email_personal',$value)->whereNull('deleted_at')->exists();

                if($profile)   $this->check = true;
                break;

        }
        if($user || $this->check) return false;
        return true;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if(!empty($this->msg)) return $this->msg;
        return "Hệ thống đang gặp sự cố vui lòng thử lại sau!";
    }
}
