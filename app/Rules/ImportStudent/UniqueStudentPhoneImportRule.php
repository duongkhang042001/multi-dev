<?php

namespace App\Rules\ImportStudent;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class UniqueStudentPhoneImportRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    public function passes($attribute, $value)
    {
        $phoneProfile = Profile::where('phone',$value)->whereNull('deleted_at')->exists();
        return !$phoneProfile;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Số điện thoại đã tồn tại trong hệ thống';
    }
}
