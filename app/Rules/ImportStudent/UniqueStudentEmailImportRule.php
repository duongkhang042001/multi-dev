<?php

namespace App\Rules\ImportStudent;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class UniqueStudentEmailImportRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    public function passes($attribute, $value)
    {
        $user = User::where('email',$value)->whereNull('deleted_at')->exists();
        return !$user;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Email đã tồn tại trong hệ thống';
    }
}
