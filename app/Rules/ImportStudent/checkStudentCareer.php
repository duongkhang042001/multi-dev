<?php

namespace App\Rules\ImportStudent;

use App\Models\Careers;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class checkStudentCareer implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    public function passes($attribute, $value)
    {
        $Career = Careers::where('name',$value)->whereNull('deleted_at')->exists();
        return $Career;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Vui lòng chọn lại ngành';
    }
}
