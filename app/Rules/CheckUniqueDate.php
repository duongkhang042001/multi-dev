<?php

namespace App\Rules;

use App\Models\Report;
use App\Models\ReportDetail;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class CheckUniqueDate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public $date;
    public function __construct($date, $id = null)
    {
        $this->date = $date;
        $this->id   = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (empty($this->id)) {
            $userId = Auth::guard('student')->user()->id;
            $report = Report::where('user_id', $userId)->where('is_active', 1)->first();
            if ($userId) {
                $reportDetail = ReportDetail::where('date', '=', $this->date)->where('report_id', $report->id)->exists();
            }
            return !$reportDetail;
        } else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Bạn đã viết báo cáo cho ngày này rồi.';
    }
}
