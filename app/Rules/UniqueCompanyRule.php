<?php

namespace App\Rules;

use App\Models\Company;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class UniqueCompanyRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $check;
    protected $msg;
    protected $id;
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->id) {
            $company = Company::where($attribute,$value)->where('id','!=',$this->id)->whereNull('deleted_at')->exists();
        }else{
            $company = Company::where($attribute,$value)->whereNull('deleted_at')->exists();
        }
        //SWITCH CASE này để tạo message xuất ra và bắt thêm những trường khó xử lý đi kèm unique
        switch ($attribute) {
            case 'tax_number':
                $this->msg = 'Mã số thuế của doanh nghiệp đã tồn tại trên hệ thống!';
                if(strlen($value) != 13 && strlen($value) != 10){
                    $this->check = true;
                    $this->msg = 'Mã số thuế không đúng!';
                }
                break;

            case 'name':
                $this->msg = 'Tên doanh nghiệp này đã tồn tại trên hệ thống!';
                break;

            case 'phone':
                $this->msg = 'Số điện thoại này đã tồn tại trên hệ thống!';
                break;

            case 'slug':
                $this->msg = 'Đường dẫn này đã tồn tại trên hệ thống!';
                break;

            case 'code':
                $this->msg = 'Mã doanh nghiệp này đã tồn tại trên hệ thống!';
                break;

            case 'email':
                $this->msg = 'Email này đã tồn tại trên hệ thống!';
                if(User::where('email',$value)->whereNull('deleted_at')->exists())   $this->check = true;
                if(Profile::where('email_personal',$value)->whereNull('deleted_at')->exists())   $this->check = true;
                break;

        }
        if($company || $this->check) return false;
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if(!empty($this->msg)) return $this->msg;
        return "Hệ thống đang gặp sự cố vui lòng thử lại sau!";
    }
}
