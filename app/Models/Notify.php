<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'notifies';
    protected $primaryKey = 'id';

    protected $fillable = [
        'content',
        'title',
        'type',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by');
    }
   
}
