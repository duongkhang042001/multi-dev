<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'services';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'report_id',
        'description',
        'reason',
        'file',
        'type',
        'status',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function roleUser()
    {
        return $this->hasOneThrough(Role::class, User::class, 'id', 'id', 'user_id', 'role_id');
    }

    public function getFile()
    {
        return $this->hasOne(File::class, 'code', 'file');
    }

    public function report(){
        return $this->hasOne(Report::class, 'id', 'report_id');
    }
}
