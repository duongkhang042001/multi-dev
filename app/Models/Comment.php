<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory, SortDeleteHandle;


    protected $table = 'comments';
    protected $primaryKey = 'id';

    protected $fillable = [
        'content',
        'parent_id',
        'post_id',
        'type',
        'company_id',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by');
    }

    public function comment()
    {
        return $this->belongsTo(Comment::class,'id','parent_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class,'post_id','id');
    }

    public function company()
    {
        return $this->hasOne(Company::class,'id','company_id');
    }
}
