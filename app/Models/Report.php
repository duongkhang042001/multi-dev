<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Expr\FuncCall;

class Report extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'reports';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'tutor_name',
        'tutor_phone',
        'tutor_email',
        'tutor_position',
        'intern_position',
        'summary_activities',
        'about',
        'function_area_activities',
        'product_services',
        'organization',
        'strategy_future',
        'work',
        'result',
        'work_unfinished',
        'problem_develop',
        'general_comment',
        'company_id',
        'start',
        'end',
        'user_cancel_id',
        'status',
        'status_reason',
        'rate_id',
        'registed_at',
        'semester_id',
        'is_accept',
        'is_active',
        'is_outside',
        'rate_file',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];
    public function rateFile(){
        return $this->hasOne(File::class, 'code', 'rate_file');
    }
    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function userStudent()
    {
        return $this->belongsTo(UserStudent::class, 'id', 'report_id');
    }

    public function reportDetail()
    {
        return $this->hasMany(reportDetail::class, 'report_id', 'id');
    }

    public function reportRate()
    {
        return $this->hasOne(ReportRate::class, 'id', 'rate_id');
    }    
    public function roleCancel()
    {
        return $this->hasOneThrough(Role::class, User::class, 'id', 'id', 'user_cancel_id', 'role_id');
    }
    public function userCancel()
    {
        return $this->hasOne(User::class, 'id', 'user_cancel_id');
    }
    public function semester()
    {
        return $this->hasOne(Semester::class, 'id', 'semester_id');
    }
    public function semesterRegister()
    {
        return $this->hasOne(Semester::class, 'id', 'registed_at');
    }
    public function services()
    {
        return $this->hasMany(Service::class, 'report_id', 'id');
    }  
    public function serviceCancel(){
        return $this->belongsTo(Service::class, 'id', 'report_id')->where('type',SERVICE_TYPE_INTERNSHIP_CANCEL)->latest();
    }
    

}
