<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Careers extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'careers';
    protected $primaryKey = 'id';

    protected $fillable = [
        'code',
        'name',
        'career_group_id',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function careerGroup()
    {
        return $this->hasOne(CareerGroup::class,'id','career_group_id');
    }
}
