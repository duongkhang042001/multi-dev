<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use SortDeleteHandle;

    protected $table = 'profiles';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'email_personal',
        'short_name',
        'full_name',
        'first_name',
        'last_name',
        'phone',
        'description',
        'gender',
        'birthday',
        'address',
        'ward_id',
        'ward_name',
        'district_id',
        'district_name',
        'city_id',
        'city_name',
        'indo',
        'gallery_files',
        'status',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];
    public function users()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
    public function district()
    {
        return $this->hasOne(District::class, 'id', 'district_id');
    }
    public function ward()
    {
        return $this->hasOne(Ward::class, 'id', 'ward_id');
    }
    public function getAddress()
    {
        return $this->address . ', ' . $this->ward_name . ', ' . $this->district_name . ', ' . $this->city_name;
    }

    public function getAge()
    {
        $birthDate = date('m/d/Y', strtotime($this->birthday));
        $birthDate = explode("/", $birthDate);
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
            ? ((date("Y") - $birthDate[2]) - 1)
            : (date("Y") - $birthDate[2]));
        return $age;
    }
}
