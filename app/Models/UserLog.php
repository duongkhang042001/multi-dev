<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'user_logs';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'action',
        'target',
        'description',
        'old_data',
        'new_data',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
