<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'timelines';
    protected $primaryKey = 'id';

    protected $fillable = [
        'description',
        'start_find',
        'end_find',
        'start_support',
        'end_support',
        'first_notify',
        'second_notify',
        'semester_id',
        'is_current',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function semester()
    {
        return $this->hasOne(Semester::class,'id','semester_id');
    }
}
