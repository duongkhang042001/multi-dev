<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use HasFactory, SortDeleteHandle;


    protected $table = 'rates';
    protected $primaryKey = 'id';

    protected $fillable = [
        'code',
        'name',
        'user_id',
        'post_id',
        'percent',
        'company_id',
        'type',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function User()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function Post()
    {
        return $this->hasOne(Post::class,'id','post_id');
    }

    public function Company()
    {
        return $this->hasOne(Company::class,'id','company_id');
    }
}
