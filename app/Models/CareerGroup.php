<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CareerGroup extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'career_groups';
    protected $primaryKey = 'id';

    protected $fillable = [
        'code',
        'slug',
        'name',
        'is_education',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function recruitmentPosts()
    {
        return $this->hasManyThrough(RecruitmentPost::class,Careers::class,'career_group_id','career_id','id','id');
    }
}
