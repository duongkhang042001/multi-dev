<?php


namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

trait SortDeleteHandle
{
    use SoftDeletes;


    public static function boot()
    {
        parent::boot();
        $userIdCurrent = null;
        if (Auth::guard('manager')->check()) $userIdCurrent = Auth::guard('manager')->id();
        if (Auth::guard('staff')->check()) $userIdCurrent = Auth::guard('staff')->id();
        if (Auth::guard('student')->check()) $userIdCurrent = Auth::guard('student')->id();
        if (Auth::guard('company')->check()) $userIdCurrent = Auth::guard('company')->id();
        static::creating(function ($model) use ($userIdCurrent) {
            $user_id           = $userIdCurrent;
            $model->created_by = $user_id;
            $date              = date('Y-m-d H:i:s', time());
            $model->created_at = $date;
        });

        static::updating(function ($model) use ($userIdCurrent) {
            $user_id           = $userIdCurrent;
            $date              = date('Y-m-d H:i:s', time());
            $model->updated_by = $user_id;
            $model->updated_at = $date;
        });

        static::saving(function ($model) use ($userIdCurrent) {
            $user_id           = $userIdCurrent;
            $model->updated_by = $user_id;
            $model->updated_at = date('Y-m-d H:i:s', time());
        });

        static::deleting(function ($model) use ($userIdCurrent){
            $user_id           = $userIdCurrent;
            $model->deleted    = 1;
            $model->deleted_at = date('Y-m-d H:i:s', time());
            $model->deleted_by = $user_id;
            $model->save();
        });
    }

    public function userCreated()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function userUpdated()
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }
}
