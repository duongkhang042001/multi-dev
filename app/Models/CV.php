<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CV extends Model
{
    use HasFactory, SortDeleteHandle;


    protected $table = 'cv';
    protected $primaryKey = 'id';

    protected $fillable = [
        'introduce',
        'target',
        'education',
        'experience',
        'experience_year',
        'qualification',
        'activity',
        'skill',
        'certificate',
        'prize',
        'hobby',
        'url',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function User()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
