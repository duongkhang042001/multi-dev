<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'wards';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'code',
        'district_id',
        'district_code',
        'full_name',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function district()
    {
        return $this->hasOne(District::class,'id','district_id');
    }
}
