<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportRate extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'report_rates';
    protected $primaryKey = 'id';

    protected $fillable = [
        'report_id',
        'content',
        'advantages',
        'defect',
        'feedback',
        'attitude_point',
        'work_point',
        'status',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];
    public function Report()
    {
        return $this->hasOne(Report::class,'id','report_id');
    }
}
