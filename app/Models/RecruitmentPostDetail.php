<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecruitmentPostDetail extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'recruitment_post_details';
    protected $primaryKey = 'id';

    protected $fillable = [
        'recruitment_post_id',
        'user_id',
        'interview ',
        'time',
        'status',
        'wish',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];
    public function recruitmentPost()
    {
        return $this->hasOne(RecruitmentPost::class,'id','recruitment_post_id');
    }
    public function post()
    {
        return $this->hasOneThrough(Post::class,RecruitmentPost::class,'id','id','recruitment_post_id','post_id');
    }
    public function company()
    {
        return $this->hasOneThrough(Company::class,RecruitmentPost::class,'id','id','recruitment_post_id','company_id');
    }
    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}
