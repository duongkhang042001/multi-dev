<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use HasFactory, SortDeleteHandle;


    protected $table = 'feedbacks';
    protected $primaryKey = 'id';

    protected $fillable = [
        'full_name',
        'email',
        'phone',
        'title',
        'content',
        'content_return',
        'user_return',
        'type',
        'company_id',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function company()
    {
        return $this->hasOne(Company::class,'id','company_id');
    }

    public function user()
    {
        return $this->hasOne(User::class,'id','created_by');
    }

    public function userReturn()
    {
        return $this->hasOne(User::class,'id','user_return');
    }
    
}
