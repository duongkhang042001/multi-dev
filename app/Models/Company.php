<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'companies';
    protected $primaryKey = 'id';

    protected $fillable = [
        'code',
        'slug',
        'name',
        'phone',
        'email',
        'manager_id',
        'career_group_id',
        'url',
        'founded_at',
        'banner',
        'avatar',
        'tax_number',
        'address',
        'ward_code',
        'ward_name',
        'district_code',
        'district_name',
        'city_code',
        'city_name',
        'lat',
        'lng',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function manager()
    {
        return $this->hasOne(User::class,'id','manager_id');
    }
    public function userRegister()
    {
        return $this->hasOne(User::class,'id','created_by');
    }
    public function staffs()
    {
        return $this->hasMany(User::class,'company_id','id');
    }
    public function careerGroup()
    {
        return $this->hasOne(CareerGroup::class,'id','career_group_id');
    }
    public function recruitmentPosts()
    {
        return $this->hasMany(RecruitmentPost::class,'company_id','id');
    }
    public function recruitmentPost()
    {
        return $this->hasOne(RecruitmentPost::class,'company_id','id');
    }
    public function reports()
    {
        return $this->hasMany(Report::class,'company_id','id');
    }
    public function recruitmentPostDetails()
    {
        return $this->hasManyThrough(RecruitmentPostDetail::class,RecruitmentPost::class,'company_id','recruitment_post_id','id','id');
    }
    public function getAddress(){
        return $this->address.', '.$this->ward_name.', '.$this->district_name.', '.$this->city_name;
    }
    public function getAvatar(){
        return !empty($this->avatar) ? FILE_URL.$this->avatar : "";
    }
    public function getBanner()
    {
        return !empty($this->banner) ? FILE_URL.$this->banner : "";
    }
}
