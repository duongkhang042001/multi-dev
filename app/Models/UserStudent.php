<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserStudent extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'user_students';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'is_accept',
        'admission_at',
        'career_id',
        'current_semester',
        'report_id',
        'cv_file',
        'internship_status',
        'exemption_file', 
        'exemption_status', 
        'transcript',
        'transcript_issued',
        'transcript_status',
        'study_status',
        'status',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function reports()
    {
        return $this->hasMany(Report::class, 'user_id', 'id');
    }

    public function career()
    {
        return $this->hasOne(Careers::class, 'id', 'career_id');
    }

    public function getCV()
    {
        $listCV = json_decode($this->cv_file);
        foreach ($listCV as $cv) {
            $cvObject = json_decode($cv);
            if ($cvObject->is_active == 1) {
                return $cvObject;
            }
        }
        return false;
    }

    public function status($type){
        $now    = date('Y-m-d h:i:s', time());
        $currentSemester = Semester::where('from', '<=', $now)->where('to', '>=', $now)->first();
        $timeLine = $currentSemester->timeline;
        switch ($type) {
            case 'find':
                if (time() <= strtotime($timeLine->end_find) && time() >= strtotime($timeLine->start_find)) {
                    return true;
                } else {
                    return false;
                }
            case 'report':
                if (time() <= strtotime($timeLine->end) && time() >= strtotime($timeLine->start)) {
                    return true;
                } else {
                    return false;
                }
            default:
                break;
        }
    }
}
