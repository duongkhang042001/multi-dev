<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'messages';
    protected $primaryKey = 'id';

    protected $fillable = [
        'content',
        'sender_id',
        'responder_id',
        'type',
        'status',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function User()
    {
        return $this->hasOne(User::class,'id','sender_id');
    }
   
}
