<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'semesters';
    protected $primaryKey = 'id';

    protected $fillable = [
        'code',
        'name',
        'from',
        'to',
        'is_current',
        'description',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];
    public function timeline()
    {
        return $this->belongsTo(Timeline::class,'id','semester_id');
    }
}
