<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory, SortDeleteHandle;


    protected $table = 'districts';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'code',
        'city_id',
        'city_code',
        'full_name',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
}
