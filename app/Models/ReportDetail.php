<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportDetail extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'report_details';
    protected $primaryKey = 'id';

    protected $fillable = [
        'report_id',
        'content_ morning',
        'content_ afternoon',
        'date',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function report()
    {
        return $this->hasOne(Report::class,'id','report_id');
    }
}
