<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory, SortDeleteHandle;


    protected $table = 'posts';
    protected $primaryKey = 'id';

    protected $fillable = [
        'slug',
        'tag',
        'title',
        'post_type_id',
        'description',
        'short_description',
        'content',
        'thumbnail',
        'is_comment',
        'is_show',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function postType()
    {
        return $this->hasOne(PostType::class,'id','post_type_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function user()
    {
        return $this->hasOne(User::class,'id','created_by');
    }

    public function recruitmentPost()
    {
        return $this->hasOne(RecruitmentPost::class,'post_id','id');
    }
    public function getThumbnail(){
        return !empty($this->thumbnail) ? FILE_URL.$this->thumbnail : "";
    }
}
