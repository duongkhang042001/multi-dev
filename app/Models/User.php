<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, SortDeleteHandle;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'code',
        'name',
        'email',
        'password',
        'provider',
        'provider_id',
        'avatar',
        'report_id',
        'role_id',
        'work_at',
        'is_logged',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }
    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id', 'id');
    }
    public function service()
    {
        return $this->hasMany(Service::class, 'user_id', 'id');
    }
    public function getService($type, $latest = false)
    {
        switch ($type)
        {
            case SERVICE_TYPE_INTERNSHIP:
                    $service = !$latest ? Service::where('type',SERVICE_TYPE_INTERNSHIP)->where('user_id',Auth::id())->get()
                     : Service::where('type',SERVICE_TYPE_INTERNSHIP)->where('user_id',Auth::id())->latest()->first();
                     break;
            default : return false;
        }
        return $service;
    }
    public function student()
    {
        return $this->hasOne(UserStudent::class, 'user_id', 'id');
    }
    public function reports()
    {
        return $this->hasMany(Report::class, 'user_id', 'id');
    }
    public function report()
    {
        return $this->hasOne(Report::class, 'user_id', 'id')->where('is_active',1);
    }
    public function reportDetail()
    {
        return $this->hasOne(Report::class, 'user_id', 'id');
    }
    public function reportReset()
    {
        return $this->hasOne(Report::class, 'user_id', 'id')->where('is_active',1);
    }
    public function recruitmentPostDetails()
    {
        return $this->hasMany(RecruitmentPostDetail::class, 'user_id', 'id');
    }
    public function getRecruitmentPostDetail()
    {
        $recruitmentPostDetail = RecruitmentPostDetail::where('is_active',1)->first();
        return $recruitmentPostDetail;
    }
    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
    public function recruitmentPost()
    {
        return $this->belongsTo(RecruitmentPost::class, 'id', 'id');
    }
    public static function getFile($value, $type = null)
    {
        if (empty($value)) return null;
        if (strlen($value) != 9)  return $value;
        $file = File::where('code', $value)->first();
        if (!empty($file)) return FILE_URL . $value;
        else return null;
    }
    public function getAvatar(){
        return !empty($this->avatar) ? FILE_URL.$this->avatar : "";
    }
    public function serviceExemption(){
        return $this->belongsTo(Service::class, 'id', 'user_id')->where('type',SERVICE_TYPE_EXEMPTION)->latest();
    }
}
