<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory, SortDeleteHandle;


    protected $table = 'files';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'code',
        'extension',
        'type',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

}
