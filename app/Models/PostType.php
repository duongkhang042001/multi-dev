<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostType extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'post_types';
    protected $primaryKey = 'id';

    protected $fillable = [
        'code',
        'slug',
        'name',
        'description',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];
    
    public function posts()
    {
       return $this->hasMany(Post::class,'post_type_id','id');
    }
    
    public function user()
    {
        return $this->hasOne(User::class,'id','created_by');
    }
}
