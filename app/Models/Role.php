<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use SortDeleteHandle;

    protected $table = 'roles';
    protected $primaryKey = 'id';

    protected $fillable = [
        'code',
        'name',
    ];

    public function Users(){
        return $this->belongsToMany(User::class);
    }
}
