<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotifyDetail extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'notify_details';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'notify_id',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function notify()
    {
        return $this->hasOne(Notify::class,'id','notify_id');
    }

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
   
}
