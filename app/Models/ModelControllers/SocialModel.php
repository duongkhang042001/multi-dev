<?php


namespace App\Models\ModelControllers;


use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Str;

class SocialModel
{
    /**
     * @var User|null
     */
    protected $model;

    public function __construct(User $model = null)
    {
        $this->model=$model;
    }

    public function createUser($input, $provider)
    {
        $user = User::where('provider_id', $input['id'])->where('provider', $provider)->first();
        if (!empty($user) && empty($user->deleted) && $user->is_active == 1) {
            return $user;
        } else {
            $param = [
                'code'        => $provider . '_' . Str::random('6'),
                'name'        => $input['name'],
                'email'       => $input['email'],
                'avatar'      => $input['picture'],
                'provider_id' => $input['id'],
                'provider'    => $provider,
                'role_id'     => MANAGER_ROLE_ID,
                'is_avtive'   => 1
            ];
            $user  = new User();
            $user  = $user->create($param);
            if ($user) {
                $profile      = new Profile();
                $paramProfile = [
                    'user_id'        => $user->id,
                    'email_personal' => $input['email'],
                    'short_name'     => $input['name'],
                    'full_name'      => $input['name'],
                    'first_name'     => $input['given_name'],
                    'last_name'      => $input['family_name'],
                ];
                $profile      = $profile->create($paramProfile);
                if ($profile) return $user;
            }
        }
        return false;
    }
}
