<?php

namespace App\Models\ModelControllers;

use App\Models\Feedback;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FeedBackModel extends Model
{

   public function feedbackReply($input)
   {
    $feedback = Feedback::find($input['id']);
    $feedback->user_return    = Auth::user()->id;
    $feedback->content_return = $input['content'];
    $feedback->save();
    return $feedback;
   }

  
}
