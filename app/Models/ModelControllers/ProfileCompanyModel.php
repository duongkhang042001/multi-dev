<?php

namespace App\Models\ModelControllers;

use App\Models\City;
use App\Models\Company;
use App\Models\District;
use App\Models\Post;
use App\Models\Profile;
use App\Models\RecruitmentPost;
use App\Models\User;
use App\Models\Ward;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileCompanyModel extends Model
{
    function upsertProfile($input, $isUpdate = false)
    {
        if ($isUpdate) {
            $userId = Auth::user()->id;
            if (!empty($input['name'])) {
                $names = explode(" ", trim($input['name']));
                $first = $names[0];
                unset($names[0]);
                $last = !empty($names) ? implode(" ", $names) : null;
            }
            if (!empty($input['ward'])) {
                $ward = Ward::where('id', $input['ward'])->first();
            }
            if (!empty($input['city'])) {
                $city = City::where('id', $input['city'])->first();
            }
            if (!empty($input['district'])) {
                $district = District::where('id', $input['district'])->first();
            }
            $user         = User::find($userId);
            $user->name   = $input['name'] ?? $user->name;
            $user->avatar = !empty($input['avatar']) ? $input['avatar'] : $user->avatar;
            if (!empty($input['password'])) {
                if (Hash::check($input['password'], $user->password)) {
                    $user->password = Hash::make($input['password_new']);
                }
            }
            $profileUser = $user->save();
            if ($profileUser) {
                $profileUser                 = Profile::where('user_id', $user->id)->first();
                $profileUser->full_name      = !empty($input['name']) ? $input['name'] : $profileUser->full_name;
                $profileUser->short_name     = !empty($input['name']) ? $input['name'] : $profileUser->short_name;
                $profileUser->last_name      = $last;
                $profileUser->first_name     = $first;
                $profileUser->email_personal = $input['email_personal'];
                $profileUser->phone          = $input['phone'] ?? $profileUser->phone;
                $profileUser->birthday       = date('Y-m-d H:i:s', strtotime($input['birthday']));
                $profileUser->indo           = $input['indo'] ?? null;
                $profileUser->address        = $input['address'] ?? null;
                $profileUser->description    = $input['description'] ?? null;
                $profileUser->ward_id        = $input['ward'] ?? null;
                $profileUser->ward_name      = !empty($input['ward']) ? $ward->full_name : null;
                $profileUser->district_id    = $input['district'] ?? null;
                $profileUser->district_name  = !empty($input['district']) ? $district->full_name : null;
                $profileUser->city_id        = $input['city'] ?? null;
                $profileUser->city_name      = !empty($input['city']) ? $city->full_name : null;

                $profileUser->save();
            }
        } else {
            $userId = Auth::user()->id;
            if (!empty($input['full_name'])) {
                $names = explode(" ", trim($input['full_name']));
                $first = $names[0];
                unset($names[0]);
                $last = !empty($names) ? implode(" ", $names) : null;
            }
            if (!empty($input['ward'])) {
                $ward = Ward::where('id', $input['ward'])->first();
            }
            if (!empty($input['city'])) {
                $city = City::where('id', $input['city'])->first();
            }
            if (!empty($input['district'])) {
                $district = District::where('id', $input['district'])->first();
            }
            $profileUser                 = new Profile();
            $profileUser->user_id        = $userId;
            $profileUser->full_name      = $input['full_name'];
            $profileUser->short_name     = $input['full_name'];
            $profileUser->last_name      = $last;
            $profileUser->first_name     = $first;
            $profileUser->email_personal = $input['email_personal'];
            $profileUser->phone          = $input['phone'];
            $profileUser->birthday       = date('Y-m-d H:i:s', strtotime($input['birthday']));
            $profileUser->gender         = $input['gender'] ?? null;
            $profileUser->indo           = $input['indo'] ?? null;
            $profileUser->address        = $input['address'] ?? null;
            $profileUser->description    = $input['description'] ?? null;
            $profileUser->ward_id        = $input['ward'] ?? null;
            $profileUser->ward_name      = !empty($input['ward']) ? $ward->full_name : null;
            $profileUser->district_id    = $input['district'] ?? null;
            $profileUser->district_name  = !empty($input['district']) ? $district->full_name : null;
            $profileUser->city_id        = $input['city'] ?? null;
            $profileUser->city_name      = !empty($input['city']) ? $city->full_name : null;
            $user                        = $profileUser->save();
            if ($user) {
                $user       = User::find($userId);
                $user->name = $input['full_name'];
                if (!empty($input['avatar'])) {
                    $user->avatar = $input['avatar'];
                }
                $user->is_logged = 1;
                $user->save();
            }
        }
    }

    function upsertCompany($input, $isUpdate = false)
    {
        if ($isUpdate) {
            $userId = Auth::user()->id;
            if (!empty($input['ward'])) {
                $ward = Ward::where('id', $input['ward'])->first();
            }
            if (!empty($input['city'])) {
                $city = City::where('id', $input['city'])->first();
            }
            if (!empty($input['district'])) {
                $district = District::where('id', $input['district'])->first();
            }
            $company        = Company::find($input['id']);
            $url            = json_decode($company->url) ?? null;
            $url->url       = !empty($input['url']) ? $input['url'] : $url->url;
            $url->twitter   = !empty($input['twitter']) ? $input['twitter'] : $url->twitter;
            $url->facebook  = !empty($input['facebook']) ? $input['facebook'] : $url->facebook;
            $url->linkedin  = !empty($input['linkedin']) ? $input['linkedin'] : $url->linkedin;
            $url->instagram = !empty($input['instagram']) ? $input['instagram'] : $url->instagram;

            $company->manager_id      = $userId;
            $company->career_group_id = $input['careerGroup'] ?? $company->career_group_id;
            $company->careers         = json_encode($input['careers']) ?? $company->careers;
            $company->name            = $input['name'] ?? $company->name;
            $company->short_name      = $input['short_name'] ?? $company->short_name;
            $company->slug            = Str::slug($input['name']) ?? $company->slug;
            $company->email           = $input['email'] ?? $company->email;
            $company->description     = $input['description'] ?? $company->description;
            $company->phone           = $input['phone'] ?? $company->phone;
            $company->url             = json_encode($url) ?? $company->url;
            $company->founded_at      = date('Y-m-d H:i:s', strtotime($input['founded_at'])) ?? $company->founded_at;
            $company->tax_number      = $input['tax_number'] ?? $company->tax_number;
            $company->banner          = $input['banner'] ?? $company->banner;
            $company->avatar          = $input['avatar'] ?? $company->avatar;
            $company->address         = $input['address'] ?? $company->address;
            $company->ward_id         = $input['ward'] ?? $company->ward_id;
            $company->ward_name       = !empty($input['ward']) ? $ward->full_name : $company->ward_name;
            $company->district_id     = $input['district'] ?? $company->district_id;
            $company->district_name   = !empty($input['district']) ? $district->full_name : $company->district_name;
            $company->city_id         = $input['city'] ?? $company->city_id;
            $company->city_name       = !empty($input['city']) ? $city->full_name : $company->city_name;
            $company->save();
            $userCompany             = User::find($userId);
            $userCompany->company_id = $company->id;
            $userCompany->save();
        }
        else {
            $userId = Auth::id();
            if (!empty($input['ward'])) {
                $ward = Ward::where('id', $input['ward'])->first();
            }
            if (!empty($input['city'])) {
                $city = City::where('id', $input['city'])->first();
            }
            if (!empty($input['district'])) {
                $district = District::where('id', $input['district'])->first();
            }
            $url                      = [
                'url'       => Arr::get($input,'url',null),
                'facebook'  => Arr::get($input,'facebook',null),
                'twitter'   => Arr::get($input,'twitter',null),
                'linkedin'  => Arr::get($input,'linkedin',null),
                'instagram' => Arr::get($input,'instagram',null),
            ];
            $company                  = new Company();
            $company->manager_id      = Auth::guard('company')->check() ? $userId : null;
            $company->career_group_id = $input['careerGroup'] ?? null;
            $company->careers         = !empty($input['careers']) ? json_encode($input['careers']) : null;
            $company->name            = $input['name'];
            $company->short_name      = $input['short_name'];
            $company->slug            = Str::slug($input['name']);
            $company->email           = $input['email'];
            $company->description     = !empty($input['description']) ? $input['description'] : null;
            $company->phone           = $input['phone'] ?? null;
            $company->url             = json_encode($url) ?? null;
            $company->founded_at      = date('Y-m-d H:i:s', strtotime($input['founded_at']));
            $company->tax_number      = $input['tax_number'];
            $company->banner          = !empty($input['banner']) ? $input['banner'] : null;
            $company->avatar          = !empty($input['avatar']) ? $input['avatar'] : null;
            $company->address         = $input['address'];
            $company->ward_id         = $input['ward'] ?? null;
            $company->ward_name       = !empty($input['ward']) ? $ward->full_name : null;
            $company->district_id     = $input['district'] ?? null;
            $company->district_name   = !empty($input['district']) ? $district->full_name : null;
            $company->city_id         = $input['city'] ?? null;
            $company->city_name       = !empty($input['city']) ? $city->full_name : null;
            $company->on_system       = Auth::guard('company')->check() ? 1 : 0;
            $company->save();
            if (Auth::guard('company')->check()) {
                $userCompany             = User::find($userId);
                $userCompany->company_id = $company->id;
                $userCompany->save();
            }
        }
        return $company;
    }

    function upsertRecruitmentPost($input, $isUpdate = false)
    {

        if (!empty($input['ward'])) {
            $ward = Ward::where('id', $input['ward'])->first();
        }
        if (!empty($input['city'])) {
            $city = City::where('id', $input['city'])->first();
        }
        if (!empty($input['district'])) {
            $district = District::where('id', $input['district'])->first();
        }
        if ($isUpdate) {
            $recruitmentPost         = RecruitmentPost::find($input['id']);
            $post                    = Post::find($recruitmentPost->post_id);
            $post->title             = !empty($input['title']) ? $input['title'] : $post->title;
            $post->slug              = Str::slug($input['title']) ?? $post->slug;
            $post->post_type_id      = 4;
            $post->short_description = $input['short_description'] ?? $post->short_description;
            $post->description       = $input['short_description'] ?? $post->description;
            $post->content           = $input['content'] ?? $post->content;

            $recruitment = $post->save();
            if ($recruitment) {
                $company                        = Auth::user()->company->id;
                $recruitmentPost->post_id       = $post->id;
                $recruitmentPost->company_id    = $company;
                $recruitmentPost->gender        = $input['gender'] ?? $recruitmentPost->gender;
                $recruitmentPost->salary_min    = $input['salary_min'] ?? $recruitmentPost->salary_min;
                $recruitmentPost->salary_max    = $input['salary_max'] ?? $recruitmentPost->salary_max;
                $recruitmentPost->welfares      = json_encode($input['welfares']) ?? $recruitmentPost->welfares;
                $recruitmentPost->career_id     = $input['careers'] ?? $recruitmentPost->career_id;
                $recruitmentPost->quantity      = $input['quantity'] ?? $recruitmentPost->quantity;
                $recruitmentPost->requirement   = $input['requirement'] ?? $recruitmentPost->requirement;
                $recruitmentPost->working_form  = $input['working_form'] ?? $recruitmentPost->working_form;
                $recruitmentPost->date_start    = date('Y-m-d H:i:s', strtotime($input['date_start'])) ?? $recruitmentPost->date_start;
                $recruitmentPost->date_end      = date('Y-m-d H:i:s', strtotime($input['date_end'])) ?? $recruitmentPost->date_end;
                $recruitmentPost->ward_id       = $input['ward'] ?? null;
                $recruitmentPost->ward_name     = !empty($input['ward']) ? $ward->full_name : null;
                $recruitmentPost->district_id   = $input['district'] ?? null;
                $recruitmentPost->district_name = !empty($input['district']) ? $district->full_name : null;
                $recruitmentPost->city_id       = $input['city'] ?? null;
                $recruitmentPost->city_name     = !empty($input['city']) ? $city->full_name : null;
                $recruitmentPost->save();
            }
        } else {

            $post                    = new Post();
            $post->title             = $input['title'];
            $post->slug              = Post::where('slug', Str::slug($input['title']))->exists() ? Str::slug($input['title']) . '-' . time() : Str::slug($input['title']);
            $post->post_type_id      = 4;
            $post->short_description = $input['short_description'];
            $post->description       = $input['short_description'];
            $post->content           = $input['content'];
            $recruitmentPost         = $post->save();
            if ($recruitmentPost) {
                $company                            = Auth::user()->company->id;
                $recruitmentPost                    = new RecruitmentPost();
                $recruitmentPost->post_id           = $post->id;
                $recruitmentPost->company_id        = $company;
                $recruitmentPost->gender            = $input['gender'];
                $recruitmentPost->salary_min        = $input['salary_min'] ?? 0;
                $recruitmentPost->salary_max        = $input['salary_max'] ?? 0;
                $recruitmentPost->welfares          = json_encode($input['welfares']) ?? null;
                $recruitmentPost->career_id         = $input['careers'];
                $recruitmentPost->quantity          = $input['quantity'];
                $recruitmentPost->requirement       = $input['requirement'];
                $recruitmentPost->working_form      = $input['working_form'] ?? null;
                $recruitmentPost->date_start        = date('Y-m-d H:i:s', strtotime($input['date_start']));
                $recruitmentPost->date_end          = date('Y-m-d H:i:s', strtotime($input['date_end']));
                $recruitmentPost->address           = $input['address'] ?? $company->address;
                $recruitmentPost->ward_id           = $input['ward'] ?? null;
                $recruitmentPost->ward_name         = !empty($input['ward']) ? $ward->full_name : null;
                $recruitmentPost->district_id       = $input['district'] ?? null;
                $recruitmentPost->district_name     = !empty($input['district']) ? $district->full_name : null;
                $recruitmentPost->city_id           = $input['city'] ?? null;
                $recruitmentPost->city_name         = !empty($input['city']) ? $city->full_name : null;
                $recruitmentPost->save();
            }
        }
    }

    function upsertRecruitmentPostDetail($input, $isUpdate = false)
    {
        if ($isUpdate) {
            if (!empty($input['ward'])) {
                $ward = Ward::where('id', $input['ward'])->first();
            }
            if (!empty($input['city'])) {
                $city = City::where('id', $input['city'])->first();
            }
            if (!empty($input['district'])) {
                $district = District::where('id', $input['district'])->first();
            }
            $recruitmentPost = RecruitmentPost::find($input['id']);
            // $post                               = Post::find($recruitmentPost->post_id);
            // $company                            = Auth::user()->company->id;
            // $recruitmentPost->post_id           = $post->id;
            // $recruitmentPost->company_id        = $company;
            $recruitmentPost->working_form  = $input['working_form'] ?? $recruitmentPost->working_form;
            $recruitmentPost->address       = $input['address'] ?? $recruitmentPost->address;
            $recruitmentPost->salary_min    = $input['salary_min'] ?? $recruitmentPost->salary_min;
            $recruitmentPost->salary_max    = $input['salary_max'] ?? $recruitmentPost->salary_max;
            $recruitmentPost->ward_id       = $input['ward'] ?? null;
            $recruitmentPost->ward_name     = !empty($input['ward']) ? $ward->full_name : null;
            $recruitmentPost->district_id   = $input['district'] ?? null;
            $recruitmentPost->district_name = !empty($input['district']) ? $district->full_name : null;
            $recruitmentPost->city_id       = $input['city'] ?? null;
            $recruitmentPost->city_name     = !empty($input['city']) ? $city->full_name : null;
            $recruitmentPost->save();
        }
    }

    
    function upsertCompanyProfile($input, $isUpdate = false)
    {
        if ($isUpdate) {
            $userId = Auth::user()->id;
            if (!empty($input['ward'])) {
                $ward = Ward::where('id', $input['ward'])->first();
            }
            if (!empty($input['city'])) {
                $city = City::where('id', $input['city'])->first();
            }
            if (!empty($input['district'])) {
                $district = District::where('id', $input['district'])->first();
            }
            $company        = Company::find($input['id']);
            $url            = json_decode($company->url) ?? null;
            $url->url       = !empty($input['url']) ? $input['url'] : $url->url;
            $url->twitter   = !empty($input['twitter']) ? $input['twitter'] : $url->twitter;
            $url->facebook  = !empty($input['facebook']) ? $input['facebook'] : $url->facebook;
            $url->linkedin  = !empty($input['linkedin']) ? $input['linkedin'] : $url->linkedin;
            $url->instagram = !empty($input['instagram']) ? $input['instagram'] : $url->instagram;

            $company->manager_id      = $userId;
            $company->career_group_id = $input['careerGroup'] ?? $company->career_group_id;
            $company->careers         = json_encode($input['careers']) ?? $company->careers;
            $company->name            = $input['name'] ?? $company->name;
            $company->short_name      = $input['short_name'] ?? $company->short_name;
            $company->slug            = Str::slug($input['name']) ?? $company->slug;
            $company->email           = $input['email'] ?? $company->email;
            $company->description     = $input['description'] ?? $company->description;
            $company->phone           = $input['phone'] ?? $company->phone;
            $company->url             = json_encode($url) ?? $company->url;
            $company->founded_at      = date('Y-m-d H:i:s', strtotime($input['founded_at'])) ?? $company->founded_at;
            $company->tax_number      = $input['tax_number'] ?? $company->tax_number;
            $company->banner          = $input['banner'] ?? $company->banner;
            $company->avatar          = $input['avatar'] ?? $company->avatar;
            $company->address         = $input['address'] ?? $company->address;
            $company->ward_id         = $input['ward'] ?? $company->ward_id;
            $company->ward_name       = !empty($input['ward']) ? $ward->full_name : $company->ward_name;
            $company->district_id     = $input['district'] ?? $company->district_id;
            $company->district_name   = !empty($input['district']) ? $district->full_name : $company->district_name;
            $company->city_id         = $input['city'] ?? $company->city_id;
            $company->city_name       = !empty($input['city']) ? $city->full_name : $company->city_name;
            $company->status          = COMPANY_STATUS_WAITING;  
            $company->save();
        }else{
            
        }
        
    }

    
}
