<?php

namespace App\Models\ModelControllers;

use App\Models\CV;
use App\Models\UserStudent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class ResumeModel extends Model
{
    use HasFactory;
    public function createResume($input)
    {
        if (!empty($input)) {
            $resume = new CV();
            $resume->introduce          = Arr::get($input, 'introduce', null);
            $resume->target             = Arr::get($input, 'target', null);
            $resume->education          = json_encode(Arr::get($input, 'education', []));
            $resume->experience         = json_encode(Arr::get($input, 'exp', []));
            $resume->experience_year    = Arr::get($input, 'experience-year', null);
            $resume->qualification      = Arr::get($input, 'qualification', null);
            $resume->activity           = Arr::get($input, 'activity', null);
            $resume->skill              = json_encode(Arr::get($input, 'skill', []));
            $resume->certificate        = Arr::get($input, 'certificate', null);
            $resume->prize              = Arr::get($input, 'prize', null);
            $resume->hobby              = Arr::get($input, 'hobby', null);
            $resume->url                = json_encode(Arr::get($input, 'url', null));
            $resume->save();
        }
        return true;
    }

    public function upLoadResume($input)
    {
        if (!empty($input)) {
            $idUser = Auth::user()->id;
            $now    = date('Y-m-d h:i:s', time());
            $userStudent = UserStudent::where('user_id', $idUser)->first();
            $cvFile = !empty($userStudent->cv_file) ? json_decode($userStudent->cv_file) : [];
            $arrayCV = [
                'code'          => $input['code'],
                'name'          => $input['name'],
                'is_active'     => count($cvFile) == 0 ? 1 : 0,
                'type'          => 'personal',
                'created_at'    => $now,
            ];
            array_push($cvFile, json_encode($arrayCV));
            $userStudent->cv_file = json_encode($cvFile);
            $check = $userStudent->save();
        }
        return $check;
    }
}

// 'type' : "custom | system",
