<?php

namespace App\Models\ModelControllers;

use App\Models\Report;
use App\Models\ReportRate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportRateModel extends Model
{
    public function __construct(ReportRate $model = null)
    {
        $this->model = $model;
    }

    function upsertReportRateCompany($input, $isUpdate = false)
    {
        if ($isUpdate) {
            $report = Report::find($input['id']);
            $reportRate = ReportRate::find($report->rate_id);
            $reportRate->advantages             = $input['advantages'] ?? null;
            $reportRate->defect                 = $input['defect'] ?? null;
            $reportRate->content                = $input['content'] ?? null;
            $reportRate->attitude_point         = $input['attitude_point'] ?? null;
            $reportRate->work_point             = $input['work_point'] ?? null;
            $reportRate->status                 = $input['status'] ?? null;
            $reportRate->save();
        } else {
            $reportRate = new ReportRate();
            $reportRate->report_id              = $input['id'];
            $reportRate->advantages             = $input['advantages'] ?? null;
            $reportRate->defect                 = $input['defect'] ?? null;
            $reportRate->content                = $input['content'] ?? null;
            $reportRate->attitude_point         = $input['attitude_point'] ?? null;
            $reportRate->work_point             = $input['work_point'] ?? null;
            $reportRate->status                 = $input['status']??null;
            $reports=$reportRate->save();
            if($reports){
                $reports                        = Report::find($input['id']);
                $reports->rate_id               = $reportRate->id;
                $reports->status                = REPORT_FINISHED;
                $reports->save();
            }
           
        }
    }
}
