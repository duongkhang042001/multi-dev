<?php

namespace App\Models\ModelControllers;

use App\Models\File;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileModel extends Model
{
    use HasFactory;

    public function createFile($input)
    {
        if (!empty($input)) {
            $file = new File();
            $file->code         = $input['id'];
            $file->name         = $input['name'];
            $file->type         = $input['mimeType'];
            $file->extension    = pathinfo($input['name'], PATHINFO_EXTENSION);
            $file->save();
        }
        return $file;
    }
}
