<?php

namespace App\Models\ModelControllers;

use App\Models\Welfare;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WelfareModel extends Model
{
    public function upsertWelfare($input, $isUpdate = false)
    {
        if ($isUpdate) {
            $welfares = Welfare::find($input['id']);
            $welfares->code = $input['code'] ??   $welfares->code;
            $welfares->name = $input['name'] ??   $welfares->name;
            $welfares->save();
        } else {
            $welfares = new Welfare();
            $welfares->code = $input['code'];
            $welfares->name = $input['name'];
            $welfares->save();
        }
    }
}
