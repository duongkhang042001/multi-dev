<?php

namespace App\Models\ModelControllers;

use App\Models\City;
use App\Models\District;
use App\Models\Profile;
use App\Models\UserStudent;
use App\Models\User;
use App\Models\Ward;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;


class UserModel
{
    public function __construct(User $model = null)
    {
        $this->model = $model;
    }

    function upsertStaff($input, $isUpdate = false)
    {
        if ($isUpdate) {
            $user        = User::find($input['id']);
            $user->name  = $input['name'] ?? $user->name;
            $user->code  = !empty($input['code']) ? Str::upper($input['code']) : $user->code;
            $user->email = $input['email'] ?? $user->email;
            $userUpdate = $user->save();
            if ($userUpdate) {
                $profileUpdate = Profile::where('user_id', $user->id)->first();
                if (!empty($input['name'])) {
                    $names = explode(" ", trim($input['name']));
                    $first = $names[0];
                    unset($names[0]);
                    $last = !empty($names) ? implode(" ", $names) : null;
                }
                if (!empty($input['ward'])) {
                    $ward = Ward::where('id', $input['ward'])->first();
                }
                if (!empty($input['city'])) {
                    $city = City::where('id', $input['city'])->first();
                }
                if (!empty($input['district'])) {
                    $district = District::where('id', $input['district'])->first();
                }
                $profileUpdate->user_id        = $user->id;
                $profileUpdate->short_name     = $input['name'] ??   $profileUpdate->short_name;
                $profileUpdate->full_name      = $input['name'] ??   $profileUpdate->full_name;
                $profileUpdate->first_name     = $first;
                $profileUpdate->last_name      = $last;
                $profileUpdate->email_personal = $input['email_personal'] ?? $profileUpdate->email_personal;
                $profileUpdate->phone          = $input['phone'] ??           $profileUpdate->phone;
                $profileUpdate->gender         = $input['gender'] ??          $profileUpdate->gender;
                $profileUpdate->birthday       = date('Y-m-d H:i:s', strtotime($input['birthday'])) ??  $profileUpdate->birthday;
                $profileUpdate->address        = $input['address'] ??         $profileUpdate->address;
                $profileUpdate->indo           = $input['indo'] ??            $profileUpdate->indo;
                $profileUpdate->ward_id        = $input['ward'] ??             $profileUpdate->ward_id;
                $profileUpdate->ward_name      = !empty($input['ward']) ? $ward->full_name : $profileUpdate->ward_name;
                $profileUpdate->district_id    = $input['district'] ??         $profileUpdate->dictrict_id;
                $profileUpdate->district_name  = !empty($input['district']) ? $district->full_name : $profileUpdate->dictrict_name;
                $profileUpdate->city_id        = $input['city'] ??             $profileUpdate->city_id;
                $profileUpdate->city_name      = !empty($input['city']) ? $city->full_name : $profileUpdate->city_name;
                $profileUpdate->save();
            }
        } else {
            $user               = new User();
            $user->code         = Str::upper($input['code']);
            $user->name         = $input['name'];
            $user->email        = $input['email'];
            $user->password     = Hash::make('12345678');
            $user->role_id      = STAFF_ROLE_ID;
            $user->is_active    = 1;
            $userCreate = $user->save();
            if ($userCreate) {
                $userCreate = new Profile();
                if (!empty($input['name'])) {
                    $names = explode(" ", trim($input['name']));
                    $first = $names[0];
                    unset($names[0]);
                    $last = !empty($names) ? implode(" ", $names) : null;
                }
                if (!empty($input['ward'])) {
                    $ward = Ward::where('id', $input['ward'])->first();
                }
                if (!empty($input['city'])) {
                    $city = City::where('id', $input['city'])->first();
                }
                if (!empty($input['district'])) {
                    $district = District::where('id', $input['district'])->first();
                }
                $userCreate->user_id        = $user->id;
                $userCreate->short_name     = $input['name'];
                $userCreate->full_name      = $input['name'];
                $userCreate->first_name     = $first;
                $userCreate->last_name      = $last;
                $userCreate->email_personal = $input['email_personal'] ?? null;
                $userCreate->phone          = $input['phone'];
                $userCreate->gender         = $input['gender'];
                $userCreate->birthday       = date('Y-m-d H:i:s', strtotime($input['birthday']));
                $userCreate->address        = $input['address'];
                $userCreate->indo           = $input['indo'];
                $userCreate->address        = $input['address'];
                $userCreate->ward_id        = $input['ward'] ?? null;
                $userCreate->ward_name      = !empty($input['ward']) ? $ward->full_name : null;
                $userCreate->district_id    = $input['district'] ?? null;
                $userCreate->district_name  = !empty($input['district']) ? $district->full_name : null;
                $userCreate->city_id        = $input['city'] ?? null;
                $userCreate->city_name      = !empty($input['city']) ? $city->full_name : null;
                $userCreate->save();
            }
        }
    }

    function upsertStudent($input, $isUpdate = false)
    {
        if ($isUpdate) {
            $student            = User::find($input['id']);
            $student->name      = $input['name'] ?? $student->name;
            $student->email     = $student->is_logged ?  $student->email :$input['email'];
            $student->code      = !empty($input['code']) ? Str::upper($input['code']) : $student->code;
            $student->is_active = 1;
            $profileUpdate      = $student->save();
            if ($profileUpdate) {
                $profileUpdate = Profile::where('user_id', $student->id)->first();
                if (!empty($input['name'])) {
                    $names = explode(" ", trim($input['name']));
                    $first = $names[0];
                    unset($names[0]);
                    $last = !empty($names) ? implode(" ", $names) : null;
                }
                if (!empty($input['ward'])) {
                    $ward = Ward::where('id', $input['ward'])->first();
                }
                if (!empty($input['city'])) {
                    $city = City::where('id', $input['city'])->first();
                }
                if (!empty($input['district'])) {
                    $district = District::where('id', $input['district'])->first();
                }
                $profileUpdate->user_id        = $student->id;
                $profileUpdate->short_name     = $input['name'] ??            $profileUpdate->short_name;
                $profileUpdate->full_name      = $input['name'] ??            $profileUpdate->full_name;
                $profileUpdate->first_name     = $first;
                $profileUpdate->last_name      = $last;
                $profileUpdate->email_personal = $input['email_personal'] ??  $profileUpdate->email_personal;
                $profileUpdate->phone          = $input['phone'] ??           $profileUpdate->phone;
                $profileUpdate->gender         = $input['gender'] ??          $profileUpdate->gender;
                $profileUpdate->birthday       = date('Y-m-d H:i:s', strtotime($input['birthday'])) ?? $profileUpdate->birthday;
                $profileUpdate->address        = $input['address'] ??         $profileUpdate->address;
                $profileUpdate->indo           = $input['indo'] ??            $profileUpdate->indo;
                $profileUpdate->ward_id        = $input['ward'] ??            $profileUpdate->ward_id;
                $profileUpdate->ward_name      = !empty($input['ward'])     ? $ward->full_name : $profileUpdate->ward_name;
                $profileUpdate->district_id    = $input['district'] ??        $profileUpdate->district_id;
                $profileUpdate->district_name  = !empty($input['district']) ? $district->full_name : $profileUpdate->district_name;
                $profileUpdate->city_id        = $input['city'] ??            $profileUpdate->city_id;
                $profileUpdate->city_name      = !empty($input['city'])     ? $city->full_name : $profileUpdate->city_name;
                $userStudentUpdate             = $profileUpdate->save();
                if ($userStudentUpdate) {
                    $userStudentUpdate = UserStudent::where('user_id', $student->id)->first();
                    $userStudentUpdate->user_id             = $student->id;
                    $userStudentUpdate->current_semester    = $input['semester']  ?? $userStudentUpdate->current_semester;
                    $userStudentUpdate->transcript_status   = TRANSCRIPT_HAVE_NEVER ?? $userStudentUpdate->transcript_status;
                    $userStudentUpdate->study_status        = HOCDI ?? $userStudentUpdate->study_status;
                    $userStudentUpdate->career_id           = $input['career'] ?? $userStudentUpdate->career_id;
                    $userStudentUpdate->save();
                }
            }
        } else {
            $student          = new User();
            $student->name    = $input['name'];
            $student->code    = Str::upper($input['code']);
            $student->email   = $input['email'];
            $student->role_id = STUDENT_ROLE_ID;
            $student->password  = bcrypt($input['password']);
            $profileCreate = $student->save();
            if ($profileCreate) {
                $profileCreate = new Profile();
                if (!empty($input['name'])) {
                    $names = explode(" ", trim($input['name']));
                    $first = $names[0];
                    unset($names[0]);
                    $last = !empty($names) ? implode(" ", $names) : null;
                }
                if (!empty($input['ward'])) {
                    $ward = Ward::where('id', $input['ward'])->first();
                }
                if (!empty($input['city'])) {
                    $city = City::where('id', $input['city'])->first();
                }
                if (!empty($input['district'])) {
                    $district = District::where('id', $input['district'])->first();
                }
                $profileCreate->user_id        = $student->id;
                $profileCreate->short_name     = $input['name'];
                $profileCreate->full_name      = $input['name'];
                $profileCreate->first_name     = $first;
                $profileCreate->last_name      = $last;
                $profileCreate->email_personal = $input['email_personal'] ?? null;
                $profileCreate->phone          = $input['phone'];
                $profileCreate->gender         = $input['gender'];
                $profileCreate->birthday       = date('Y-m-d H:i:s', strtotime($input['birthday']));
                $profileCreate->address        = $input['address'];
                $profileCreate->indo           = $input['indo'];
                $profileCreate->address        = $input['address'];
                $profileCreate->ward_id        = $input['ward'] ?? null;
                $profileCreate->ward_name      = !empty($input['ward']) ? $ward->full_name : null;
                $profileCreate->district_id    = $input['district'] ?? null;
                $profileCreate->district_name  = !empty($input['district']) ? $district->full_name : null;
                $profileCreate->city_id        = $input['city'] ?? null;
                $profileCreate->city_name      = !empty($input['city']) ? $city->full_name : null;
                $userStudent = $profileCreate->save();
                if ($userStudent) {
                    $userStudent = new UserStudent();
                    $userStudent->user_id             = $student->id;
                    $userStudent->current_semester    = $input['semester'];
                    $userStudent->transcript_status   = TRANSCRIPT_HAVE_NEVER;
                    $userStudent->study_status        = HOCDI;
                    $userStudent->career_id           = $input['career'] ?? null;
                    $userStudent->save();
                }
            }
        }
        return $student;
    }
    function updateProfileStaff($input)
    {
        if (!empty($input['full_name'])) {
            $names = explode(" ", trim($input['full_name']));
            $firstName = $names[0];
            unset($names[0]);
            $lastName = !empty($names) ? implode(" ", $names) : null;
        }
        $ward = Ward::where('id', $input['ward'])->first();
        $district = District::where('id', $input['district'])->first();
        $city = City::where('id', $input['city'])->first();
        $profileUpdate = Profile::where('user_id',$input['id'])->whereNull('deleted_at')->first();
        $profileUpdate->short_name     = $input['full_name'];
        $profileUpdate->full_name      = $input['full_name'];
        $profileUpdate->first_name     = $firstName;
        $profileUpdate->last_name      = $lastName;
        $profileUpdate->email_personal = $input['email_personal'];
        $profileUpdate->phone          = $input['phone'];
        $profileUpdate->gender         = $input['gender'];
        $profileUpdate->birthday       = date('Y-m-d H:i:s', strtotime($input['birthday']));
        $profileUpdate->address        = $input['address'];
        $profileUpdate->indo           = $input['indo'];
        $profileUpdate->ward_id        = $input['ward'];
        $profileUpdate->ward_name      = $ward->full_name;
        $profileUpdate->district_id    = $input['district'];
        $profileUpdate->district_name  = $district->full_name;
        $profileUpdate->city_id        = $input['city'];
        $profileUpdate->city_name      = $city->full_name;
        $profileUpdate->description      = !empty($input['description']) ? $input['description'] : '';
        $profile = $profileUpdate->save();
        if ($profile) {
            $user               = User::find($profileUpdate->user_id);
            $user->avatar       = $input['avatar'] ?? $user->avatar;
            $user->is_logged    = 1;
            $user->save();
        }
    }

}
