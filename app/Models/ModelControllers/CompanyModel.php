<?php

namespace App\Models\ModelControllers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Company;
use App\Models\User;
use Illuminate\Support\Str;
use App\Models\City;
use App\Models\District;
use App\Models\File;
use App\Models\Post;
use App\Models\Ward;
use App\Models\Profile;
use Illuminate\Support\Arr;

class CompanyModel extends Model
{
   use HasFactory;
   public function __construct(User $model = null)
   {
      $this->model = $model;
   }
   function upsertCompany($input, $isUpdate = false)
   {
      if ($isUpdate) {
         $companyUpdate     = Company::find($input['id']);
         if (!empty($input['ward'])) {
            $ward = Ward::where('id', $input['ward'])->first();
         }
         if (!empty($input['city'])) {
            $city = Ward::where('id', $input['city'])->first();
         }
         if (!empty($input['district'])) {
            $district = Ward::where('id', $input['district'])->first();
         }
         $url                = json_decode($companyUpdate->url) ?? null;
         $url->url       = !empty($input['url']) ? $input['url'] : $url->url;
         $url->twitter   = !empty($input['twitter']) ? $input['twitter'] : $url->twitter;
         $url->facebook  = !empty($input['facebook']) ? $input['facebook'] : $url->facebook;
         $url->linkedin  = !empty($input['linkedin']) ? $input['linkedin'] : $url->linkedin;
         $url->instagram = !empty($input['instagram']) ? $input['instagram'] : $url->instagram;
         $companyUpdate->code           = $input['code'] ?? $companyUpdate->code;
         $companyUpdate->name           = $input['name'] ?? $companyUpdate->name;
         $companyUpdate->slug           = !empty($input['name']) ? Str::slug($input['name']) : $companyUpdate->slug;
         $companyUpdate->email          = $input['email'] ?? $companyUpdate->email;
         $companyUpdate->phone          = $input['phone'] ?? $companyUpdate->phone;
         $companyUpdate->url            = json_encode($url) ?? $companyUpdate->url;
         $companyUpdate->founded_at     = $input['founded_at'] ?? $companyUpdate->founded_at;
         $companyUpdate->banner         = $input['banner'] ?? $companyUpdate->banner;
         $companyUpdate->avatar         = $input['avatar'] ?? $companyUpdate->avatar;
         $companyUpdate->tax_number     = $input['tax_number'] ?? $companyUpdate->tax_number;
         $companyUpdate->address        = $input['address'] ?? $companyUpdate->address;
         $companyUpdate->ward_id        = $input['ward'] ??     $companyUpdate->ward_id;
         $companyUpdate->ward_name      = !empty($input['ward']) ? $ward->full_name : $companyUpdate->ward_name;
         $companyUpdate->district_id    = $input['district'] ?? $companyUpdate->district_id;
         $companyUpdate->district_name  = !empty($input['district']) ? $district->full_name : $companyUpdate->district_name;
         $companyUpdate->city_id        = $input['city'] ??     $companyUpdate->city_id;
         $companyUpdate->city_name      = !empty($input['city']) ? $city->full_name : $companyUpdate->city_name;
         $companyUpdate->save();
      } else {
         $companyCreate = new Company();
         if (!empty($input['ward'])) {
            $ward = Ward::where('id', $input['ward'])->first();
         }
         if (!empty($input['city'])) {
            $city = City::where('id', $input['city'])->first();
         }
         if (!empty($input['district'])) {
            $district = District::where('id', $input['district'])->first();
         }
         $url                      = [
            'url'       => Arr::get($input,'url',null),
            'facebook'  => Arr::get($input,'facebook',null),
            'twitter'   => Arr::get($input,'twitter',null),
            'linkedin'  => Arr::get($input,'linkedin',null),
            'instagram' => Arr::get($input,'instagram',null),
        ];
         $companyCreate->code           = $input['code'];
         $companyCreate->name           = $input['name'];
         $companyCreate->slug           = Str::slug($input['name']);
         $companyCreate->email          = $input['email'];
         $companyCreate->phone          = $input['phone'] ?? null;
         $companyCreate->url            = json_encode($url) ?? null;
         $companyCreate->founded_at     = date('Y-m-d H:i:s', strtotime($input['founded_at']));
         $companyCreate->tax_number     = $input['tax_number'];
         $companyCreate->banner         = $input['banner'];
         $companyCreate->avatar         = $input['avatar'];
         $companyCreate->address        = $input['address'];
         $companyCreate->ward_id        = $input['ward'] ?? null;
         $companyCreate->ward_name      = !empty($input['ward']) ? $ward->full_name : null;
         $companyCreate->district_id    = $input['district'] ?? null;
         $companyCreate->district_name  = !empty($input['district']) ? $district->full_name : null;
         $companyCreate->city_id        = $input['city'] ?? null;
         $companyCreate->city_name      = !empty($input['city']) ? $city->full_name : null;
         $companyCreate = $companyCreate->save();
      }
   }
   function upsertStaffCompany($input, $isUpdate = false)
   {
      if ($isUpdate) {
         $user        = User::find($input['id']);
         $user->name  = $input['name'] ?? $user->name;
         $user->code  = !empty($input['code']) ? Str::upper($input['code']) : $user->code;
         $user->email = $input['email'] ?? $user->email;
         $userUpdate = $user->save();
         if ($userUpdate) {
            $profileUpdate = Profile::where('user_id', $user->id)->first();
            if (!empty($input['name'])) {
               $names = explode(" ", trim($input['name']));
               $first = $names[0];
               unset($names[0]);
               $last = !empty($names) ? implode(" ", $names) : null;
            }
            if (!empty($input['ward'])) {
               $ward = Ward::where('id', $input['ward'])->first();
            }
            if (!empty($input['city'])) {
               $city = Ward::where('id', $input['city'])->first();
            }
            if (!empty($input['district'])) {
               $district = Ward::where('id', $input['district'])->first();
            }
            $profileUpdate->user_id        = $user->id;
            $profileUpdate->short_name     = $input['name'] ??   $profileUpdate->short_name;
            $profileUpdate->full_name      = $input['name'] ??   $profileUpdate->full_name;
            $profileUpdate->first_name     = $first;
            $profileUpdate->last_name      = $last;
            $profileUpdate->email_personal = $input['email_personal'] ?? $profileUpdate->email_personal;
            $profileUpdate->phone          = $input['phone'] ??           $profileUpdate->phone;
            $profileUpdate->gender         = $input['gender'] ??          $profileUpdate->gender;
            $profileUpdate->birthday       = date('Y-m-d H:i:s', strtotime($input['birthday'])) ??  $profileUpdate->birthday;
            $profileUpdate->address        = $input['address'] ??         $profileUpdate->address;
            $profileUpdate->indo           = $input['indo'] ??            $profileUpdate->indo;
            $profileUpdate->ward_id        = $input['ward'] ??             $profileUpdate->ward_id;
            $profileUpdate->ward_name      = !empty($input['ward']) ? $ward->full_name : $profileUpdate->ward_name;
            $profileUpdate->district_id    = $input['district'] ??         $profileUpdate->district_id;
            $profileUpdate->district_name  = !empty($input['district']) ? $district->full_name : $profileUpdate->district_name;
            $profileUpdate->city_id        = $input['city'] ??             $profileUpdate->city_id;
            $profileUpdate->city_name      = !empty($input['city']) ? $city->full_name : $profileUpdate->city_name;
            $profileUpdate->save();
         }
      } else {
         $user             = new User();
         $user->code       = Str::upper($input['code']);
         $user->name       = $input['name'];
         $user->email      = $input['email'];
         $user->role_id    = COMPANY_ROLE_ID;
         $user->company_id = $input['company_id'];
         $user->is_active  = 0;
         $staffCompanyCreate = $user->save();
         if ($staffCompanyCreate) {
            $staffCompanyCreate = new Profile();
            if (!empty($input['name'])) {
               $names = explode(" ", trim($input['name']));
               $first = $names[0];
               unset($names[0]);
               $last = !empty($names) ? implode(" ", $names) : null;
            }
            if (!empty($input['ward'])) {
               $ward = Ward::where('id', $input['ward'])->first();
            }
            if (!empty($input['city'])) {
               $city = City::where('id', $input['city'])->first();
            }
            if (!empty($input['district'])) {
               $district = District::where('id', $input['district'])->first();
            }
            $staffCompanyCreate->user_id        = $user->id;
            $staffCompanyCreate->short_name     = $input['name'];
            $staffCompanyCreate->full_name      = $input['name'];
            $staffCompanyCreate->first_name     = $first;
            $staffCompanyCreate->last_name      = $last;
            $staffCompanyCreate->email_personal = $input['email_personal'] ?? null;
            $staffCompanyCreate->phone          = $input['phone'];
            $staffCompanyCreate->gender         = $input['gender'];
            $staffCompanyCreate->birthday       = date('Y-m-d H:i:s', strtotime($input['birthday']));
            $staffCompanyCreate->address        = $input['address'];
            $staffCompanyCreate->indo           = $input['indo'];
            $staffCompanyCreate->ward_id        = $input['ward'] ?? null;
            $staffCompanyCreate->ward_name      = !empty($input['ward']) ? $ward->full_name : null;
            $staffCompanyCreate->district_id    = $input['district'] ?? null;
            $staffCompanyCreate->district_name  = !empty($input['district']) ? $district->full_name : null;
            $staffCompanyCreate->city_id        = $input['city'] ?? null;
            $staffCompanyCreate->city_name      = !empty($input['city']) ? $city->full_name : null;
            $staffCompany = $staffCompanyCreate->save();
            if ($staffCompany) {
               $idStaff =  $user->id;
               $company = Company::where('id', $input['company_id'])->where('deleted_at',Null)->first();
               if (!empty($company) && $company->manager_id == NULL) {
                  $company->manager_id = $idStaff;
                  $company->save();
               }
            }
         }
      }
   }
   public function upsertPost($input, $isUpdate = false)
   {
       if ($isUpdate) {
           $post = Post::find($input['id']);
           $post->title             = $input['title'] ??               $post->title;
           $post->slug              = Str::slug($input['slug']) ??     $post->slug;
           $post->content           = $input['content'] ??             $post->content;
           $post->short_description = !empty($input['description']) ? strip_tags($input['description']) : $post->short_description;
           $post->description       = $input['description'] ??         $post->description;
           $post->thumbnail         = $input['thumbnail'] ??           $post->thumbnail;
           $post->post_type_id      = $input['post-type'] ??           $post->post_type_id;
           $post->save();
       } else {
           $post = new Post();
           $post->title             = $input['title'];
           $post->slug              = Str::slug($input['slug']);
           $post->content           = $input['content'];
           $post->short_description = strip_tags($input['description']);
           $post->description       = $input['description'] ?? null;
           $post->thumbnail         = $input['thumbnail'];
           $post->post_type_id      = $input['post-type'];
           $post = $post->save();
       }
   }

   public function approveCompany($input){
      $pendingCompany = Company::where('id',$input['id'])->first();
      if(!empty($pendingCompany->status == 'WAITING')){
         $pendingCompany->status = 'APPROVED';
         $pendingCompany->code = 'DN_'.time();
         $pendingCompany->save();
      }
   }

}
