<?php

namespace App\Models\ModelControllers;

use App\Models\Careers;
use Illuminate\Support\Str;
class CareersModel
{
   public function __construct(Careers $model = null)
   {
      $this->model=$model;
   }
   function checkCareer($request){

   }
   function upsertCareers($request,$isUpdate=false){
if(!$isUpdate){


        $career                     = new Careers();
        $career->name               = $request['name'];
        $career->slug               = Str::slug($request['name']);
        $career->career_group_id    = $request['career_group_id'];
        $career->is_education       = (!empty($request['is_education']) && $request['is_education']=='on' ) ? 1 : 0;
        $career->save();
}else{
        $career     = Careers::find($request['id']);
        $career->name               = $request['name'] ?? $career->name;
        $career->slug               = Str::slug($request['name']) ?? $career->slug;
        $career->career_group_id    = $request['career_group_id']?? $career->career_group_id;
        $career->is_education       = (!empty($request['is_education']) && $request['is_education']=='on' ) ? 1 : 0;
        $career->save();
}

   }

}
