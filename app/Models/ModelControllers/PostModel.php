<?php

namespace App\Models\ModelControllers;

use App\Models\File;
use App\Models\Post;
use App\Models\PostType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PostModel extends Model
{
    public function upsertPostType($input, $isUpdate = false)
    {
        if ($isUpdate) {
            $postType = PostType::find($input['id']);
            $postType->slug = Str::slug($input['slug']) ??      $postType->slug;
            $postType->name = $input['name'] ??                 $postType->name;
            $postType->description = $input['description'] ??   $postType->description;
            $postType->save();
        } else {
            $postType = new PostType();
            $postType->slug = Str::slug($input['slug']);
            $postType->name = $input['name'];
            $postType->description = $input['description'];
            $postType->save();
        }
    }

    public function upsertPost($input, $isUpdate = false)
    {
        if ($isUpdate) {
            $post = Post::find($input['id']);
            $post->title             = $input['title'] ??               $post->title;
            $post->slug              = Str::slug($input['slug']) ??     $post->slug;
            $post->content           = $input['content'] ??             $post->content;
            $post->short_description = !empty($input['description']) ? strip_tags($input['description']) : $post->short_description;
            $post->description       = $input['description'] ??         $post->description;
            $post->thumbnail         = $input['thumbnail'] ??           $post->thumbnail;
            $post->post_type_id      = $input['post-type'] ??           $post->post_type_id;
            $post->save();
        } else {
            $post = new Post();
            $post->title             = $input['title'];
            $post->slug              = Str::slug($input['slug']);
            $post->content           = $input['content'];
            $post->short_description = strip_tags($input['description']);
            $post->description       = $input['description'] ?? null;
            $post->thumbnail         = $input['thumbnail'];
            $post->post_type_id      = $input['post-type'];
            $post = $post->save();
        }
    }
}
