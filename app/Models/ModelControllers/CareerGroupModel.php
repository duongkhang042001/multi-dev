<?php

namespace App\Models\ModelControllers;

use App\Models\CareerGroup;
use Illuminate\Support\Str;

class CareerGroupModel
{
    public function __construct(CareerGroup $model = null)
    {
        $this->model = $model;
    }
    function checkCareerGroup($request)
    {
    }


    function upsertCareerGroup($request, $isUpdate = false)
    {
        if (!$isUpdate) {

            $careerGroup          = new CareerGroup();
            $careerGroup->name    = $request['name'];
            $careerGroup->slug    = Str::slug($request['name']);
            $careerGroup->is_education = (!empty($request['is_education']) && $request['is_education'] == 'on') ? 1 : 0;
            $careerGroup->is_active = (!empty($request['is_active']) && $request['is_active'] == 'on') ? 1 : 0;
            $newCareerGroup = $careerGroup->save();
        } else {
            $careerGroup     = CareerGroup::find($request['id']);
            $careerGroup->name               = $request['name'] ?? $careerGroup->name;
            $careerGroup->slug               = Str::slug($request['name']) ?? $careerGroup->slug;
            $careerGroup->is_education       = (!empty($request['is_education']) && $request['is_education'] == 'on') ? 1 : 0;
            $careerGroup->is_active = (!empty($request['is_active']) && $request['is_active'] == 'on') ? 1 : 0;
            $newCareerGroup = $careerGroup->save();
        }
        return $careerGroup;
    }
}
