<?php

namespace App\Models\ModelControllers;

use App\Models\City;
use App\Models\District;
use App\Models\Profile;
use App\Models\RecruitmentPost;
use App\Models\RecruitmentPostDetail;
use App\Models\User;
use App\Models\UserStudent;
use App\Models\Ward;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class StudentModel extends Model
{
    use HasFactory;

    public function updateStudent($input)
    {
        if (!empty($input['ward'])) {
            $ward = Ward::where('id', $input['ward'])->first();
        }
        if (!empty($input['city'])) {
            $city = City::where('id', $input['city'])->first();
        }
        if (!empty($input['district'])) {
            $district = District::where('id', $input['district'])->first();
        }
        $userId = Auth::user()->id;
        $profileUser = Profile::where('user_id', $userId)->first();
        $profileUser->email_personal        = $input['email_personal'] ?? $profileUser->email_personal;
        $profileUser->short_name            = $this->explodeFullName($input['full_name'])->first_name ?? $profileUser->short_name;
        $profileUser->full_name             = $input['full_name'] ?? $profileUser->full_name;
        $profileUser->first_name            = $this->explodeFullName($input['full_name'])->first_name ?? $profileUser->first_name;
        $profileUser->last_name             = $this->explodeFullName($input['full_name'])->last_name ?? $profileUser->last_name;
        $profileUser->phone                 = $input['phone'] ?? $profileUser->phone;
        $profileUser->description           = $input['description'] ?? $profileUser->description;
        $profileUser->gender                = $input['gender'] ?? $profileUser->gender;
        $profileUser->birthday              = $input['birthday'] ?? $profileUser->birthday;
        $profileUser->address               = $input['address'] ?? $profileUser->address;
        $profileUser->ward_id               = $ward->id ?? $profileUser->ward_id;
        $profileUser->ward_name             = $ward->full_name ?? $profileUser->ward_name;
        $profileUser->district_id           = $district->id ?? $profileUser->district_id;
        $profileUser->district_name         = $district->full_name ?? $profileUser->district_name;
        $profileUser->city_id               = $city->id ?? $profileUser->city_id;
        $profileUser->city_name             = $city->full_name ?? $profileUser->city_name;
        $profileUser->indo                  = $input['indo'] ?? $profileUser->indo;
        $profileUser->phone                 = $input['phone'] ?? $profileUser->phone;
        $checkUpdateProfile                 = $profileUser->save();
        if ($checkUpdateProfile) {
            $user                   = User::find($userId);
            if (!empty($user)) {
                $user->name         = $input['full_name'] ?? $user->name;
                $user->avatar       = $input['avatar'] ?? $user->avatar;
                $user->is_logged    = 1;
                if (!empty($input['password'])) {
                    if (Hash::check($input['password'], $user->password)) {
                        $user->password = Hash::make($input['password_new']);
                    }
                }
                $user->save();
            }
            $userStudent                        = UserStudent::where('user_id', $userId)->first();
            if (!empty($userStudent)) {
                $userStudent->career_id         = $input['career'] ?? $userStudent->career_id;
                $userStudent->save();
            }
        }
        return true;
    }
    public function upsertStatusRecruimentPost($input, $isUpdate = false)
    {
        if ($isUpdate) {
            $recruitmentPostDetail = RecruitmentPostDetail::where('recruitment_post_id', $input['id'])->first();
            $recruitmentPostDetail->deleted  = 1;
            $recruitmentPostDetail->deleted_at = Carbon::now();
            $recruitmentPostDetail->save();
        }
    }
    private function explodeFullName($fullName)
    {
        if (!empty($fullName)) {
            $names = explode(" ", trim($fullName));
            $first = $names[0];
            unset($names[0]);
            $last = !empty($names) ? implode(" ", $names) : null;
        }
        return response()->json([
            'first_name'    => $first,
            'last_name'     => $last,
        ]);
    }
}
