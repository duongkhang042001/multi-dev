<?php

namespace App\Models\ModelControllers;

use App\Models\Semester;
use Illuminate\Support\Str;


class SemesterModel
{
   public function __construct(Semester $model = null)
   {
      $this->model=$model;
   }

   function upsertSemester($request, $isUpdate = false)
   {
       if (!$isUpdate) {
           $semester          = new Semester();
           $semester->name    = $request['name'];
           $semester->code    = Str::slug($request['name']);
           $semester->description = $request['description'];
           $semester->from    = $request['from'];
           $semester->to    = $request['to'];
           $newSemester = $semester->save();
       } else {
           $semester     = Semester::find($request['id']);
           $semester->name               = $request['name'] ?? $semester->name;
           $semester->code               = Str::slug($request['name']) ?? $semester->code;
           $semester->description = $request['description'] ?? $semester->description;
           $semester->from    = $request['from']??$semester->from;
           $semester->to    = $request['to']??$semester->to;
           $newSemester = $semester->save();
       }
       return $semester;
   }

}
