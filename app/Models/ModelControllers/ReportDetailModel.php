<?php

namespace App\Models\ModelControllers;

use App\Models\Report;
use App\Models\ReportDetail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ReportDetailModel extends Model
{
    public function upsertReportDetail($input)
    {
        $reportDetail = ReportDetail::where('id',$input['id'])->first();
        if ($reportDetail) {
            $reportDetail->content_morning      = $input['content_morning'] ?? $reportDetail->content_morning;
            $reportDetail->content_afternoon    = $input['content_afternoon'] ?? $reportDetail->content_afternoon;
        } else {
            $userId = Auth::user()->id;
            $report = Report::where('user_id', $userId)->where('is_active',1)->first();
            $reportDetail                       = new ReportDetail();
            $reportDetail->report_id            = $report->id ?? NULL;
            $reportDetail->content_morning      = $input['content_morning'] ?? NULL;
            $reportDetail->content_afternoon    = $input['content_afternoon'] ?? NULL;
            $reportDetail->date                 = $input['date'] ?? NULL;
        }
        $check = $reportDetail->save();
        return $check;
    }
}
