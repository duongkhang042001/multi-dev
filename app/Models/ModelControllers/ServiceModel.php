<?php

namespace App\Models\ModelControllers;

use App\Models\Service;
use App\Models\UserStudent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ServiceModel extends Model
{
    use HasFactory;
    public function upLoadService($input)
    {
        if (!empty($input)) {
            $now    = date('Y-m-d h:i:s', time());
            $userStudent = new Service();
           
            $arrayPersonalFile = [
                'code'          => $input['code'],
                'name'          => $input['name'],
                'created_at'    => $now,
            ];
            // array_push($personalFile, json_encode($arrayPersonalFile));
            $userStudent->description = $input['description'];
            $userStudent->file      = json_encode($arrayPersonalFile);
            $userStudent->user_id   = Auth::user()->id;
            $userStudent->type      = SERVICE_TYPE_EXEMPTION;
            $userStudent->status    = SERVICE_STATUS_WAITING;
            $check                  = $userStudent->save();
        }
        return $check;
    }
    public function updateService($input,$id)
    {
        if (!empty($input)) 
        {
            $now    = date('Y-m-d h:i:s', time());
            $userStudent = Service::where('id', $id)->first();
            $item = json_decode($userStudent->file);
            $arrayPersonalFile = [
                'code'          => $input['code'] ?? $item->code,
                'name'          => $input['name'] ?? $item->name,
                'created_at'    => $now,
            ];
            $userStudent->description = $input['description'] ?? $userStudent->description;
            $userStudent->file = json_encode($arrayPersonalFile);
            $check = $userStudent->save();
        }
        return $check;
    }
}
