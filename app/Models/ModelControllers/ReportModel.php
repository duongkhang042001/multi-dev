<?php

namespace App\Models\ModelControllers;

use App\Models\Report;
use App\Models\Semester;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ReportModel extends Model
{
    use HasFactory;
    function updateReport($input)
    {

        $idUser = Auth::user()->id;
        $now    = date('Y-m-d h:i:s', time());
        $report = Report::where('user_id', $idUser)->where('is_active', 1)->first();
        if (!empty($input) && !empty($report)) {
            if (!empty($input['advantage_comment']) && !empty($input['general_comment']) && !empty($input['difficult_comment'])) {
                $general_comment                           = ["advantage" => $input['advantage_comment'], "general" => $input['general_comment'], "difficult" => $input['difficult_comment']];
            } else {
                $general_comment                           = ["advantage" => null, "general" => null, "difficult" => null];
            }
            $report->start                                 = date('Y-m-d h:i:s', strtotime($input['start_date'])) ?? $report->start;
            if (!empty($input['end_date'])) {
                $report->end                               = date('Y-m-d h:i:s', strtotime($input['end_date'])) ?? $report->end;
            }
            $report->tutor_name                            = $input['tutor_name'] ?? $report->tutor_name;
            $report->tutor_phone                           = $input['tutor_phone'] ?? $report->tutor_phone;
            $report->tutor_email                           = $input['tutor_email'] ?? $report->tutor_email;
            $report->tutor_position                        = $input['tutor_position'] ?? $report->tutor_position;
            $report->intern_position                       = $input['intern_position'] ?? $report->intern_position;
            $report->about                                 = !empty($input['about_content']) ? trim($input['about_content']) : $report->about;
            $report->function_area_activities              = !empty($input['function_area_activities']) ? trim($input['function_area_activities']) : $report->function_area_activities;
            $report->product_services                      = !empty($input['product_services']) ? trim($input['product_services']) : $report->product_services;
            $report->organization                          = !empty($input['organization']) ? trim($input['organization']) : $report->organization;
            $report->strategy_future                       = !empty($input['strategy_future']) ? trim($input['strategy_future']) : $report->strategy_future;
            $report->summary_activities                    = !empty($input['summary_activities']) ? trim($input['summary_activities']) : $report->summary_activities;
            $report->work                                  = !empty($input['work']) ? trim($input['work']) : $report->work;
            $report->result                                = !empty($input['result']) ? trim($input['result']) : $report->result;
            $report->work_unfinished                       = !empty($input['work_unfinished']) ? trim($input['work_unfinished']) : $report->work_unfinished;
            $report->problem_develop                       = !empty($input['problem_develop']) ? trim($input['problem_develop']) : $report->problem_develop;
            $report->general_comment                       = json_encode($general_comment) ?? $report->general_comment;
            $report->propose                               = !empty($input['propose']) ? trim($input['propose']) : $report->propose;
            $check                                         = $report->save();
        }
        return $check;
    }

    function storeReport($career, $company, $checkTimeline)
    {
        $idUser                  = Auth::user()->id;
        $now                     = date('Y-m-d h:i:s', time());
        $semesterId              = $this->getSemester($checkTimeline);
        $report                  = new Report;
        $report->user_id         = $idUser;
        $report->semester_id     = $semesterId;
        $report->company_id      = $company;
        $report->is_accept       = $checkTimeline ? 1 : 0;
        $report->intern_position = $career;
        $report->registed_at     = $now;
        $report->save();
    }

    public function getSemester($check)
    {
        $now    = date('Y-m-d h:i:s', time());
        $currentSemester = Semester::where('from', '<=', $now)->where('to', '>=', $now)->first();
        if ($check) {
            return $currentSemester->id;
        } else {
            $dayNextSemester = Carbon::parse($currentSemester->to)->addDay(10);
            $nextSemester = Semester::where('from', '<=', $dayNextSemester)->where('to', '>=', $dayNextSemester)->first();
            return $nextSemester->id;
        }
    }
}
