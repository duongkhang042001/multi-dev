<?php

namespace App\Models\ModelControllers;

use App\Models\File;
use App\Models\Post;
use App\Models\RecruitmentPost;
use App\Models\RecruitmentPostDetail;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class RecruitmentModel
{
    public function __construct(RecruitmentPost $model = null)
    {
        $this->model = $model;
    }

    function upsertRecruitment($input, $isUpdate = false)
    {
        if (!$isUpdate) {
            $post = new Post();
            $post->title             = $input['title'];
            $post->slug              = Str::slug($input['slug']);
            $post->content           = $input['content'];
            $post->short_description = strip_tags($input['description']);
            $post->description       = $input['description'];
            $post->post_type_id      = $input['post_type'];
            $post->is_comment        = (!empty($input['comment'])) ? 1 : 0;
            $post->is_active         = (!empty($input['active']))  ? 1 : 0;
            $postCreate = $post->save();
            if ($postCreate) {
                $recruitment = new RecruitmentPost();
                $recruitment->post_id = $post->id;
                $recruitment->company_id = $input['company_id'];
                $recruitment->gender = $input['gender'];
                $recruitment->salary_min = $input['salary_min'];
                $recruitment->salary_max = $input['salary_max'];
                $recruitment->welfares = json_encode($input['welfare']);
                $recruitment->career_id = $input['career'];
                $recruitment->quantity = $input['quantity'];
                $recruitment->requirement = $input['requirement'];
                $recruitment->date_start = Carbon::parse($input['date_start'])->format('Y-m-d 00:00:00');
                $recruitment->date_end = Carbon::parse($input['date_end'])->format('Y-m-d 23:59:59');
                $recruitment->save();
            }
        } else {
            $recruitment = RecruitmentPost::find($input['id']);
            $post = Post::find($recruitment->post_id);
            $post->title             = $input['title'];
            $post->slug              = Str::slug($input['slug']);
            $post->content           = $input['content'];
            $post->short_description = strip_tags($input['description']);
            $post->description       = $input['description'];
            $post->post_type_id      = $input['post_type'];
            $post->is_comment       = (!empty($input['comment'])) ? 1 : 0;
            $post->is_active        = (!empty($input['active'])) ? 1 : 0;
            $postUpdate            = $post->save();
            if ($postUpdate) {
                $recruitment->company_id = $input['company_id'];
                $recruitment->gender = $input['gender'];
                $recruitment->salary_min = $input['salary_min'];
                $recruitment->salary_max = $input['salary_max'];
                $recruitment->welfares = json_encode($input['welfare']);
                $recruitment->career_id = $input['career'];
                $recruitment->quantity = $input['quantity'];
                $recruitment->requirement = $input['requirement'];
                $recruitment->date_start = Carbon::parse($input['date_start'])->format('Y-m-d 00:00:00');
                $recruitment->date_end = Carbon::parse($input['date_end'])->format('Y-m-d 23:59:59');
                $recruitment->save();
            }
        }
    }

    function storeRecruitment($id, $wish)
    {
        $recruitment = new RecruitmentPostDetail();
        $recruitment->recruitment_post_id = $id;
        $recruitment->user_id             = Auth::user()->id;
        $recruitment->wish                = $wish;
        $recruitment->status              = RECRUITMENT_WAITING;
        $recruitment->save();
    }

    function storeViewPost($postId)
    {
        
        $post = Post::findOrFail($postId);
        $post->view = $post->view + 1;
        $post->save();

    }
}
