<?php

namespace App\Models\ModelControllers;

use App\Events\NotificationEvent;
use App\Models\Notify;
use App\Models\NotifyDetail;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotifyModel extends Model
{
    public function __construct(Notify $model = null)
    {
        $this->model = $model;
    }
    public function upsertNotify($input, $isUpdate = false)
    {
        if ($isUpdate) {
            $notify = Notify::where('id', $input['id'])->where('is_active', 0)->first();
            $notify->title     = $input['title'];
            $notify->content   = isset($input['content']) ? $input['content'] : '';
            $notify->object      = $input['object'];
            $notify->object_detail      = isset($input['object_detail']) ? $input['object_detail'] : '';
            $notify->url     = $input['url'];
            $notify->is_active      = !empty($input['is_active']) ? 1 : 0;
            $notification = $notify->save();
            if (!empty($input['is_active'])) {
                if ($input['object'] == NOTIFY_ALL) {
                    $userID = User::all();
                }
                if ($input['object'] == NOTIFY_STAFF && $input['object_detail'] == 0) {
                    $userID = User::where('role_id', STAFF_ROLE_ID)->get();
                } else if (isset($input['object_detail'])) {
                    $userID = User::find($input['object_detail']);
                }
                if ($input['object'] == NOTIFY_STUDENT && $input['object_detail'] == 0) {
                    $userID = User::where('role_id', STUDENT_ROLE_ID)->get();
                } else if (isset($this->$input['userId'])) {
                    $userID = User::where('id', $input['userId'])->get();
                }
                if ($input['object'] == NOTIFY_COMPANY && $input['object_detail'] == 0) {
                    $userID = User::where('role_id', COMPANY_ROLE_ID)->get();
                } else if (isset($input['object_detail'])) {
                    $userID = User::where('company_id', $input['object_detail'])->get();
                }
                if ($input['object'] == NOTIFY_COMPANY_TO_STUDENT) {
                    $userID = User::where('id', $input['userId'])->get();
                }
                if ($input['object'] == NOTIFY_STUDENT_TO_COMPANY) {
                    $userID = User::where('company_id', $input['object_detail'])->get();
                }
                if ($notification) {
                    foreach ($userID as $row) {
                        $notifyDetail = new NotifyDetail();
                        $notifyDetail->user_id = $row->id;
                        $notifyDetail->notify_id = $notify->id;
                        $notifyDetail->save();
                    }
                }
            }
        } else {
            $notify = new Notify();
            $notify->title     = $input['title'];
            $notify->content   = !empty($input['content']) ? $input['content'] : null;
            $notify->object      = $input['object'];
            $notify->object_detail      = isset($input['object_detail']) ? $input['object_detail'] : null;
            $notify->is_active      = !empty($input['is_active']) ? 1 : 0;
            if (!empty($input['url'])) {
                if ($input['url'] == '/notify-detail') {
                    $notify->save();
                    $notify->url     = '/notify-detail/' . $notify->id;
                } else {
                    $notify->url     = $input['url'];
                }
            }
            $notification = $notify->save();
            if (!empty($input['is_active'])) {
                switch ($input['object']) {
                    case NOTIFY_ALL:
                        $userID = User::all();
                        break;
                    case NOTIFY_STAFF:
                        if ($input['object_detail'] == 0) {
                            $userID = User::where('role_id', STAFF_ROLE_ID)->get();
                        } else if (isset($input['object_detail'])) {
                            $userID = User::where('id', $input['object_detail'])->get();
                        }
                        break;
                    case NOTIFY_STUDENT:
                        if(isset($input['object_detail']) && $input['object_detail'] == 0) {
                            $userID = User::where('role_id', STUDENT_ROLE_ID)->get();
                        } else if (isset($input['userId'])) {
                            $userID = User::where('id', $input['userId'])->get();
                        }
                        break;
                    case NOTIFY_COMPANY:
                        if ($input['object_detail'] == 0) {
                            $userID = User::where('role_id', COMPANY_ROLE_ID)->get();
                        } else if (isset($input['object_detail'])) {
                            $userID = User::where('company_id', $input['object_detail'])->get();
                        }
                        break;
                    case NOTIFY_COMPANY_TO_STUDENT:
                        if ($input['object'] == NOTIFY_COMPANY_TO_STUDENT) {
                            $userID = User::where('id', $input['userId'])->get();
                        }
                        break;
                    case NOTIFY_STUDENT_TO_COMPANY:
                        if ($input['object'] == NOTIFY_STUDENT_TO_COMPANY) {
                            $userID = User::where('company_id', $input['object_detail'])->get();
                        }
                        break;
                }
                if ($notification) {
                    foreach ($userID as $row) {
                        $notifyDetail = new NotifyDetail();
                        $notifyDetail->user_id = $row->id;
                        $notifyDetail->notify_id = $notify->id;
                        $notifyDetail->save();
                    }
                }
            }
        }
    }
    public function sendNotifications($input)
    {
        $notify = Notify::find($input['id']);
        $notify->is_active = 1;
        $notification = $notify->save();
        if ($notification) {
            if ($notify->object == NOTIFY_ALL) {
                $userID = User::all();
            }
            if ($notify->object == NOTIFY_STAFF && $notify->object_detail == 0) {
                $userID = User::where('role_id', STAFF_ROLE_ID)->get();
            } else if (isset($notify->object_detail)) {
                $userID = User::find($notify->object_detail);
            }
            if ($notify->object == NOTIFY_STUDENT && $notify->object_detail == 0) {
                $userID = User::where('role_id', STUDENT_ROLE_ID)->get();
            }
            if ($notify->object == NOTIFY_COMPANY && $notify->object_detail == 0) {
                $userID = User::where('role_id', COMPANY_ROLE_ID)->get();
            } else if (isset($notify->object_detail)) {
                $userID = User::where('company_id', $notify->object_detail)->get();
            }
            if ($notify->object == NOTIFY_COMPANY_TO_STUDENT) {
                $userID = User::where('id', $input['userId'])->get();
            }
            if ($notify->object == NOTIFY_STUDENT_TO_COMPANY) {
                $userID = User::where('company_id', $notify->object_detail)->get();
            }
            foreach ($userID as $row) {
                $notifyDetail = new NotifyDetail();
                $notifyDetail->user_id = $row->id;
                $notifyDetail->notify_id = $notify->id;
                $notifyDetail->save();
            }
            event(new NotificationEvent($input));
        }
    }
}
