<?php

namespace App\Models\ModelControllers;

use Carbon\Carbon;
use App\Models\Timeline;

class TimelineModel
{
    public function __construct(Timeline $model = null)
    {
        $this->model = $model;
    }

    function createTimeline($request)
    {

        $timeline          = new Timeline();
        $timeline->start_find    = $request['start_find'];
        $timeline->end_find    = $request['end_find'];
        $timeline->start_support   = $request['start_support'];
        $timeline->end_support    = $request['end_support'];
        $timeline->first_notify    = $request['first_notify'];
        $timeline->second_notify    = $request['second_notify'];
        $timeline->start    = $request['start'];
        $timeline->end    = $request['end'];
        $timeline->semester_id    = $request['semester_id'];
        $timeline->created_at = now();

        $timeline->start_find = Carbon::parse($timeline->start_find)->format('Y-m-d 00:00:00');
        $timeline->end_find = Carbon::parse($timeline->end_find)->format('Y-m-d 23:59:59');
        $timeline->start_support = Carbon::parse($timeline->start_support)->format('Y-m-d 00:00:00');
        $timeline->end_support = Carbon::parse($timeline->end_support)->format('Y-m-d 23:59:59');
        $timeline->first_notify = Carbon::parse($timeline->start_support)->format('Y-m-d 10:00:00');
        $timeline->second_notify = Carbon::parse($timeline->end_support)->format('Y-m-d 10:00:00');
        $timeline->start = Carbon::parse($timeline->start_support)->format('Y-m-d 00:00:00');
        $timeline->end = Carbon::parse($timeline->end_support)->format('Y-m-d 23:59:59');

        $timeline->save();
    }
    function updateTimeline($request)
    {
        $timeline     = Timeline::find($request['id']);
        $timeline->start_find = !empty($request['start_find']) ? $request['start_find'] : $timeline->start_find;
        $timeline->end_find = !empty($request['end_find']) ? $request['end_find'] : $timeline->end_find;
        $timeline->start_support = !empty($request['start_support']) ? $request['start_support'] : $timeline->start_support;
        $timeline->end_support = !empty($request['end_support']) ? $request['end_support'] : $timeline->end_support;
        $timeline->first_notify = !empty($request['first_notify']) ? $request['first_notify'] : $timeline->first_notify;
        $timeline->second_notify = !empty($request['second_notify']) ? $request['second_notify'] : $timeline->second_notify;
        $timeline->start = !empty($request['start']) ? $request['start'] : $timeline->start;
        $timeline->end = !empty($request['end']) ? $request['end'] : $timeline->end;


        $timeline->start_find = Carbon::parse($timeline->start_find)->format('Y-m-d 00:00:00');
        $timeline->end_find = Carbon::parse($timeline->end_find)->format('Y-m-d 23:59:59');
        $timeline->start_support = Carbon::parse($timeline->start_support)->format('Y-m-d 00:00:00');
        $timeline->end_support = Carbon::parse($timeline->end_support)->format('Y-m-d 23:59:59');
        $timeline->first_notify = Carbon::parse($timeline->start_support)->format('Y-m-d 10:00:00');
        $timeline->second_notify = Carbon::parse($timeline->end_support)->format('Y-m-d 10:00:00');
        $timeline->start = Carbon::parse($timeline->start_support)->format('Y-m-d 00:00:00');
        $timeline->end = Carbon::parse($timeline->end_support)->format('Y-m-d 23:59:59');

        $timeline->save();
    }
}
