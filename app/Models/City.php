<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'cities';
    protected $primaryKey = 'id';

    protected $fillable = [
        'code',
        'name',
        'full_name',
        'is_active',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function recruitmentPosts()
    {
        return $this->hasMany(RecruitmentPost::class,'city_id','id');
    }
}
