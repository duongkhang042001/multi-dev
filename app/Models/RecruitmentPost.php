<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecruitmentPost extends Model
{
    use HasFactory, SortDeleteHandle;

    protected $table = 'recruitment_post';
    protected $primaryKey = 'id';

    protected $fillable = [
        'post_id',
        'company_id',
        'gender',
        'salary_min',
        'salary_max',
        'welfares',
        'career_id',
        'quantity',
        'date_start',
        'date_end',
        'is_active',
        'address',
        'ward_code',
        'ward_name',
        'district_code',
        'district_name',
        'city_code',
        'city_name',
        'deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
    ];

    public function post()
    {
        return $this->hasOne(Post::class,'id','post_id');
    }


    public function career()
    {
        return $this->hasOne(Careers::class,'id','career_id');
    }

    public function getAddress(){
        return $this->address.', '.$this->ward_name.', '.$this->district_name.', '.$this->city_name;
    }
    public function company()
    {
        return $this->hasOne(Company::class,'id','company_id');
    }
    public function recruitmentPostDetails()
    {
        return $this->hasMany(RecruitmentPostDetail::class,'recruitment_post_id','id');
    }
    public function scopeGender($query, $term)
    {
        $term = $term == -1 ? $query->get() : $query->where('gender',$term)->get();
    }
    public function scopeJobType($query, $term)
    {
        $query->where(function($query) use ($term){
            $query->where('working_form',$term);
        });
    }
    public function scopeCity($query, $term)
    {
        $term = "%$term%";
        $query->where(function($query) use ($term){
            $query->where('city_name','like',$term);
        });
    }
    public function scopeSearch($query, $term)
    {
        $term = "%$term%";
        $query->where(function($query) use ($term){
            $query->whereRelation('post','title','like',$term);
        });
    }
}
