<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailDeniedStudent extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
        ->subject("DOANH NGHIỆP TỪ CHỐI PHỎNG VẤN SINH VIÊN")
        ->view('mail.sendDeniedStudent', ['data' => $this->data,'user' => $this->user]);
    }
}
