<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailExemptionSuccess extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $staff;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$staff)
    {
        $this->user = $user;
        $this->staff = $staff;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
        ->subject("HOÀN THÀNH QUÁ TRÌNH THỰC TẬP")
        ->view('mail.sendExemptionSuccess', ['user' => $this->user,'staff' => $this->staff]);
    }
}
