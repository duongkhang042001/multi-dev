<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailInternshipPass extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $staff;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$staff)
    {
        $this->user = $user;
        $this->staff = $staff;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
        ->subject("Chúc mừng bạn đã đăng ký thực tập lại thành công")
        ->view('mail.sendInternshipAgainPass', ['user' => $this->user,'staff' => $this->staff]);
    }
}
