<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailInternshipFail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $staff;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$staff)
    {
        $this->user = $user;
        $this->staff = $staff;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
        ->subject("Đăng ký thực tập lại không không thành công")
        ->view('mail.sendInternshipAgainFail', ['user' => $this->user,'staff' => $this->staff]);
    }
}
