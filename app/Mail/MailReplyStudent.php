<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailReplyStudent extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $content;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$content)
    {
        $this->email = $email;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
        ->subject("PHẢN HỒI TỪ NHÀ TRƯỜNG")
        ->view('mail.sendReplyStudent', ['email' => $this->email,'content' => $this->content]);
    }
}
