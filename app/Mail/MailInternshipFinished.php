<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailInternshipFinished extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    protected $data;
    protected $type;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$data)
    {
        $this->user = $user;
        $this->data = $data;
        $this->type = $data['isSuccess'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->type){
            return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
                ->subject("Hoàn thành quá trình thực tập")
                ->view('mail.sendInternshipPass', ['user' => $this->user,'data' => $this->data]);
        }
        else{
            return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
                ->subject("Không hoàn thành thực tập")
                ->view('mail.sendInternshipFail', ['user' => $this->user,'data' => $this->data]);
        }
    }
}
