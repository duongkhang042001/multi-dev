<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailCancelCompany extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $user;
    public $reason;
    public $userStudent;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$data,$reason,$userStudent)
    {
        $this->user = $user;
        $this->data = $data;
        $this->reason = $reason;
        $this->userStudent = $userStudent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
        ->subject("SINH VIÊN TỪ CHỐI THỰC TẬP DOANH NGHIỆP")
        ->view('mail.sendCancelCompany', ['data' => $this->data,'user' => $this->user,'reason' => $this->reason,'userStudent' => $this->userStudent]);
    }
}
