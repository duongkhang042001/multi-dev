<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailAcceptCompany extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $user;
    public $userStudent;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$data,$userStudent)
    {
        $this->user = $user;
        $this->data = $data;
        $this->userStudent = $userStudent;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->user->role->code == STUDENT_ROLE_CODE)
            return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
            ->subject("SINH VIÊN ĐĂNG KÝ THỰC TẬP - FPT INTERNSHIP")
            ->view('mail.sendAcceptCompany', ['data' => $this->data,'user' => $this->user]);
        else
        {
            return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
            ->subject("DOANH NGHIỆP ĐĂNG KÝ HOẠT ĐỘNG - FPT INTERNSHIP")
            ->view('mail.sendNotificationCompany', ['data' => $this->data,'user' => $this->user,'userStudent' => $this->userStudent]);
        }
    }
}
