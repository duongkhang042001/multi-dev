<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailRegisterCompany extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
            ->subject($this->user->role->code == STUDENT_ROLE_CODE ? "ĐĂNG KÝ THỰC TẬP" : "ĐĂNG KÝ DOANH NGHIỆP")
            ->view('mail.registerCompany', ['data' => $this->data,'user' => $this->user]);
    }
}
