<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailDenidRecruitmentStudent extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $user;
    public $reason;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$data,$reason)
    {
        $this->user = $user;
        $this->data = $data;
        $this->reason = $reason;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
        ->subject("DOANH NGHIỆP TỪ CHỐI ỨNG TUYỂN SINH VIÊN")
        ->view('mail.sendDenidRecruitmentStudent', ['reason' => $this->reason,'data' => $this->data,'user' => $this->user]);
    }
}
