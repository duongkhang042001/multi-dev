<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailInterviewStudent extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $user;
    public $interview;
    public $interviewDate;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$data,$interview,$interviewDate)
    {
        $this->user = $user;
        $this->data = $data;
        $this->interview = $interview;
        $this->interviewDate = $interviewDate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
        ->subject("LỊCH PHỎNG VẤN TỪ DOANH NHIỆP ĐẾN SINH VIÊN")
        ->view('mail.sendInterviewStudent', ['interview' => $this->interview,'data' => $this->data,'user' => $this->user,'interviewDate' => $this->interviewDate]);
    }
}
