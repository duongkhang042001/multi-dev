<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailInterviewSuccess extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $user;
    public $wf;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$data,$wf)
    {
        $this->user = $user;
        $this->data = $data;
        $this->wf = $wf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'), 'FPT INTERNSHIP')
        ->subject("PHỎNG VẤN THÀNH CÔNG")
        ->view('mail.sendInterviewSuccess', ['wf' => $this->wf,'data' => $this->data,'user' => $this->user]);
    }
}
