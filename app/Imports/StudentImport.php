<?php

namespace App\Imports;

use App\Models\Careers;
use App\Models\Profile;
use App\Models\User;
use App\Models\UserStudent;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Throwable;

class StudentImport implements ToCollection, WithValidation, SkipsOnFailure, WithStartRow, WithMapping
{
    use Importable, SkipsFailures;

    protected $_header;
    protected $career;

    public function __construct()
    {
        $this->_header = [
            0 => "STT",
            1 => "MSSV",
            2 => "Họ và Tên",
            3 => "Giới tính",
            4 => "Ngày tháng năm sinh",
            5 => "Email",
            6 => "Số điện thoại",
            7 => "Kỳ hiện tại",
            8 => "Ngành học",
            9 => "Trạng thái học",
        ];
    }
    public function collection(Collection $collection)
    {
        $email = [];
        foreach ($collection->toArray() as $index => $row) {
            if(array_search(strtolower($row[5]),$email) != false) continue;
            if (!empty($row[2])) {
                $names     = explode(" ", trim($row[2]));
                $firstName = $names[0];
                unset($names[0]);
                $lastName = !empty($names) ? implode(" ", $names) : null;
            }
            $value   = [
                'code'             => $row[1],
                'password'         => Hash::make('12345678'),
                'email'            => strtolower($row[5]),
                'name'             => $row[2],
                'full_name'        => $row[2],
                'short_name'       => $row[2],
                'first_name'       => !empty($firstName) ? $firstName : null,
                'last_name'        => !empty($lastName) ? $lastName : null,
                'gender'           => ($row[3] == 'Nam') ? 1 : 0,
                'birthday'         => date('Y-m-d H:i:s', strtotime($row[4])),
                'phone'            => preg_replace('/[^0-9.]/', '', $row[6]),
                'current_semester' => $row[7],
                'career_id'        => $row[8],
                'study_status'     => $row[9] == 'HOCDI' ? 1 : 0,
                'is_accept'        => 1,
            ];
            $user    = User::create([
                'name'      => $value['name'],
                'code'      => $value['code'],
                'email'     => $value['email'],
                'password'  => $value['password'],
                'role_id'   => STUDENT_ROLE_ID,
                'is_active' => 1,
            ]);
            $profile = Profile::create([
                'user_id'    => $user->id,
                'full_name'  => $value['full_name'],
                'short_name' => $value['short_name'],
                'first_name' => $value['short_name'],
                'last_name'  => $value['last_name'],
                'gender'     => $value['gender'],
                'birthday'   => $value['birthday'],
                'phone'      => $value['phone'],
            ]);
            $student = UserStudent::create([
                'user_id'          => $user->id,
                'is_accept'        => 1,
                'career_id'        => $value['career_id'],
                'study_status'     => $value['study_status'],
                'current_semester' => $value['current_semester'],
            ]);
            array_push($email,$value['email']);
        }
    }
    public function startRow(): int
    {
        return 2;
    }
    public function batchSize(): int
    {
        return 1000;
    }
    public function prepareForValidation($data, $index)
    {
        if (!empty($data[4]) && is_numeric($data[4])) {
            $data[4] = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($data[4]))->format('d/m/Y');
        }
        if (!empty($data[8])) {
            $career = Careers::where('name', 'like', $data[8])->where('is_active', 1)->where('is_education', 1)->first();
            if (!empty($career)) $data[8] = $career->id;
        }
        return $data;
    }
    public function rules(): array
    {
        return [
            '1' => ['required', 'unique:users,code', 'max:20'],
            '2' => ['required', 'string', 'max:50'],
            '3' => ['required', 'in:Nam,Nữ'],
            '4' => ['required', 'date_format:d/m/Y'],
            '5' => ['email', 'required', 'unique:users,email', 'ends_with:@fpt.edu.vn'],
            '6' => ['required', 'numeric', 'unique:profiles,phone'],
            '7' => ['required', 'numeric', 'in:1,2,3,4,5,6,7,8,9,10,11'],
            '8' => ['required', 'numeric', 'exists:careers,id'],
            '9' => ['required', 'in:HOCDI,NGHIHOC,BAOLUU'],
        ];

    }
    public function customValidationMessages()
    {
        return [
            '1.required'    => 'Vui lòng nhập mã số sinh viên',
            '1.unique'      => 'Mã số sinh viên đã tồn tại',
            '2.required'    => 'Vui lòng nhập họ và tên',
            '3.required'    => 'Vui lòng nhập giới tính',
            '3.in'          => 'Giới tính phải là Nam hoặc Nữ',
            '4.required'    => 'Vui lòng nhập ngày tháng năm sinh',
            '4.date_format' => 'Ngày tháng năm sinh phải đúng định dạng ngày tháng năm',
            '5.email'       => 'Email chưa đúng định dạng',
            '5.required'    => 'Vui lòng nhập email',
            '5.unique'      => 'Email đã tồn tại',
            '5.ends_with'   => 'Email sinh viên phải có đuôi @fpt.edu.vn',
            '6.required'    => 'Vui lòng nhập số điện thoại',
            '6.numeric'     => 'Số điện thoại phải là số',
            '6.unique'      => 'Số điện thoại đã tồn tại',
            '7.required'    => 'Vui lòng nhập kỳ học',
            '7.numeric'     => 'Kỳ học phải là số',
            '7.in'          => 'Kỳ học của sinh viên nằm trong khoảng từ 1 đến 11',
            '8.required'    => 'Vui lòng nhập ngành học',
            '8.unique'      => 'Ngành học không tồn tại',
            '9.required'    => 'Vui lòng trạng thái học',
            '9.in'          => 'Trạng thái học không đúng (HOCDI, NGHIHOC, BAOLUU)',
        ];
    }
    public function map($row): array
    {
        return array_map('trim', $row);
    }
    public function columnFormats(): array
    {
        return [
            '4' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];

    }
    public function onFailure(Failure ...$failures)
    {
        $row      = [];
        $errors = [];
        foreach ($failures as $key => $failure){
            $search = array_search($failure->row(),$row);
            if(is_numeric($search)) $errors[$search]['errors'] = array_merge($errors[$search]['errors'],$failure->errors());
            else {
                $row[$key] =$failure->row();
                $errors[$key] = [
                    'row' => $failure->row(),
                    'errors' => $failure->errors(),
                    'values' => $failure->values(),
                ];
            }
            $total[$key] =$failure->row();
        }
        throw ValidationException::withMessages(['errors' => $errors]);
    }
}
