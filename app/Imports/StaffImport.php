<?php

namespace App\Imports;

use App\Models\Careers;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;
use Throwable;

class StaffImport implements ToCollection, WithValidation, SkipsOnFailure, WithStartRow, WithMapping
{
    use Importable, SkipsFailures;

    protected $_header;

    public function __construct()
    {
        $this->_header       = [
            0 => "STT",
            1 => "Mã nhân viên",
            2 => "Họ và Tên",
            3 => "Ngày tháng năm sinh",
            4 => "Email",
            5 => "Số điện thoại",
            6 => "CMND/CCCD",
        ];
    }

    public function collection(Collection $collection)
    {
        $email = [];
        foreach ($collection->toArray() as $index => $row) {
            if(array_search(strtolower($row[4]),$email) != false) continue;
            $user = User::create([
                'code'     => $row[1],
                'name'     => $row[2],
                'email'    => strtolower($row[4]),
                'password' => Hash::make('12345678'),
                'role_id'   => STAFF_ROLE_ID
            ]);
            if (!empty($row[2])) {
                $names     = explode(" ", trim($row[2]));
                $firstName = $names[0];
                unset($names[0]);
                $lastName = !empty($names) ? implode(" ", $names) : null;
            }
            Profile::create([
                'user_id'    => $user->id,
                'short_name' => $row[2],
                'full_name'  => $row[2],
                'first_name' => $firstName ?? null,
                'last_name'  => $lastName ?? null,
                'phone'      => preg_replace('/[^0-9.]/', '', $row[5]),
                'gender'     => 1,
                'birthday'   => date('Y-m-d H:i:s', strtotime($row[3])),
                'indo'       => $row[6],
            ]);
            array_push($email,strtolower($row[4]));
        }
    }

    public function rules(): array
    {
        return [
            '1' => ['required', 'unique:users,code', 'max:20'],
            '2' => ['required', 'string', 'max:50'],
            '3' => ['required', 'date_format:d/m/Y'],
            '4' => ['email', 'required', 'unique:users,email'],
            '5' => ['required', 'numeric','digits:10', 'unique:profiles,phone'],
            '6' => ['required', 'numeric','digits:9', 'unique:profiles,indo'],
        ];
    }
    public function prepareForValidation($data, $index)
    {
        if (!empty($data[3]) && is_numeric($data[3])) {
            $data[3] = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($data[3]))->format('d/m/Y');
        }
        return $data;
    }
    public function customValidationMessages()
    {
        return [
            '1.required'    => 'Mã nhân viên không được bỏ trống',
            '1.unique'      => 'Mã nhân viên đã tồn tại',
            '1.max'         => 'Mã nhân viên tối đa 20 ký tự',
            '2.required'    => 'Họ và tên không được bỏ trống',
            '2.string'      => 'Họ và tên phải là chữ',
            '2.max'         => 'Họ và tên tối đa 50 ký tự',
            '3.required'    => 'Ngày sinh không được bỏ trống',
            '3.date_format' => 'Ngày sinh phải định dạng theo mẫu d/m/Y',
            '4.required'    => 'Email nhân viên không được bỏ trống',
            '4.email'       => 'Email cần đúng định dạng',
            '4.unique'      => 'Email đã tồn tại',
            '5.required'    => 'Số điện thoại không được bỏ trống',
            '5.numeric'     => 'Số điện thoại phải là số',
            '5.unique'      => 'Số điện thoại đã tồn tại',
            '5.digits'      => 'Số điện thoại phải có độ dài bằng 10 kí tự',
            '6.required'    => 'CCCD/CMND không được bỏ trống',
            '6.numeric'     => 'CCCD/CMND phải là số',
            '6.unique'      => 'CCCD/CMND đã tồn tại',
            '6.digits'      => 'CCCD/CMND phải có độ dài bằng 9 kí tự',
        ];
    }

    public function onFailure(Failure ...$failures)
    {
        $row = [];
        $errors = [];
        foreach ($failures as $key => $failure){
            $search = array_search($failure->row(),$row);
            if(is_numeric($search)) $errors[$search]['errors'] = array_merge($errors[$search]['errors'],$failure->errors());
            else {
                $row[$key] =$failure->row();
                $errors[$key] = [
                    'row' => $failure->row(),
                    'errors' => $failure->errors(),
                    'values' => $failure->values(),
                ];
            }
            $total[$key] =$failure->row();
        }
        throw ValidationException::withMessages(['errors' => $errors]);
    }

    public function map($row): array
    {
        return array_map('trim', $row);
    }

    public function startRow(): int
    {
        return 2;
    }
    public function batchSize(): int
    {
        return 1000;
    }
}
